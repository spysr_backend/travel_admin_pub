<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
<!--************************************
        MAIN ADMIN AREA
    ****************************************-->
<style>
.ms-parent {
    width: 100% !important;
}
.btn-bs-file{
    position:relative;
}
.ms-choice {
    height: 30px !important;
    font-size: 12px !important;
    }
hr.profile-hr {
    margin-top: 10px;
    margin-bottom: -2px;
    border: 0;
    border-top: 2px solid #eeeeee;
    margin-left: 110px;
}
</style>
<!--ADMIN AREA BEGINS-->
<section class="wrapper container-fluid">
	<div class=" ">
		<div class=" ">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.employee_profile" />
					</h5>
					<div class="set pull-right">
                      <div class="dropdown">
					  <div class="btn-group">
						<a class="btn btn-xs btn-outline-primary" href="userList"><span class="fa fa-arrow-left"></span> Back To List </a>
					</div>
				    </div>
                           </div>
				</div>
				<div class="row">
				<div class="col-md-12 mt-2">
					<div class="profile-timeline-card" style="margin-top: 40px;">
						<div class="" style="">
						<c:choose>
							<c:when test="${userProfile.imagePath != null && userProfile.imagePath != '' }">
								<img class="timeline-img-user pull-left" src="<s:url action='getImageByPath?imageId=%{userProfile.imagePath}'/>">
							</c:when>
							<c:otherwise>
								<i class="fa fa-user header-icons timeline-img-user pull-left" style="box-shadow: none;"></i>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="dropdown pull-right">
							<button class="btn btn-xs btn-outline-primary" type="button" data-toggle="dropdown">
								<i class="fa fa-chevron-down"></i>
							</button>
							<ul class="dropdown-menu">
								<s:if test="%{((#session.User.userRoleId.isSuperUser()||#session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
									<li class=""><a href="companyDetails?id=${userProfile.companyId}" class=""> Company Detail
									</a></li>
								</s:if>
								<li class="">
								<a href="#" class="" data-toggle="modal" data-target="#reset-user-password"> <s:text name="tgi.label.reset_password" />
								</a></li>
								<li class="">
								<a href="add_user_wallet?userId=${userProfile.id}" class=""> Add Wallet
								</a></li>
							</ul>
						</div>
						<form id="imageUploadForm" method="post" enctype="multipart/form-data">
						<label class="btn-bs-file btn btn-xs btn-outline-primary pull-right mr-1" data-toggle="tooltip" data-placement="top" title="Change Profile Image">Browse
		                	<input type="file" name="uploadUserFile" onchange="userProfileUploadFn(${userProfile.id});" />
	            		</label>
	            		</form>
						<s:if test="%{((#session.User.userRoleId.isSuperUser()|| #session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
						<a class="btn btn-xs btn-outline-danger pull-right mr-1" data-event="duplicate" data-toggle="modal" data-target="#edit-user">
							<i class="fa fa-pencil"></i> Edit User
						</a>
						<a class="btn btn-xs btn-outline-primary pull-right mr-1" href="userRegister">
							<i class="fa fa-plus"></i> New User
						</a>
						</s:if>
						<div class="">
							<h4 class="mt-2 mb-4">
								 <b class="blue-grey-text">${userProfile.firstName} ${userProfile.lastName}</b>
							</h4>
						</div>
						<hr class="profile-hr">
						<div class="timeline-footer row" style="    margin-left: 80px;">
							<div class="col-md-8">
								<div class="col-md-2 col-xs-6 mb-2 mt-2">
									<span class="touroxy-form-element_label">User Name <br> <span class="spysr-form-element_data"><s:property value="userProfile.userName" /></span> </span>
								</div>
								<div class="col-md-2 col-xs-6 mb-2 mt-2">
									<span class="touroxy-form-element_label">Mail <br> <span class="spysr-form-element_data"><s:property value="userProfile.email" /></span> </span>
								</div>
								<div class="col-md-2 col-xs-6 mb-2 mt-2">
									<span class="touroxy-form-element_label"> Phone  <br> <span class="spysr-form-element_data"> <s:property value="userProfile.mobile" /> </span></span>
								</div>
								<div class="col-md-2 col-xs-6 mb-2 mt-2">
									<span class="touroxy-form-element_label"> Country  <br> <span class="spysr-form-element_data"><s:property value="userProfile.userDetail.countryName" /> </span></span>
								</div>
							</div>
						</div>
					</div>
					</div>
						<div class="col-md-8">
							<div class="profile-timeline-card">
								<div class="dropdown pull-right">
									<button class="btn btn-xs btn-outline-primary" type="button" data-toggle="modal" data-target="#edit-user-profile"> <i class="fa fa-pencil"></i> Edit User Profile
									</button>
								</div>
								<div class="">
									<p class="mt-2 mb-4">
										<span class="font-icon"><i class="fa fa-user"></i></span> <span> <b class="blue-grey-text">User Details</b></span>
									</p>
								</div>
								<hr>
								
								<div class="timeline-footer row">
									<div class="col-md-6 col-sm-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">First Name :<br>
												<span class="spysr-form-element_data"><s:property value="userProfile.firstName" /></span>
											</span>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Last Name :  <br>
											<span class="spysr-form-element_data"> ${userProfile.lastName != null && userProfile.lastName != '' ?userProfile.lastName : 'N/A'}</span>
											</span>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Email :  <br>
												<span class="spysr-form-element_data"><a href=""><s:property value="userProfile.email" /></a></span>
											</span>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Date of Birth  <br> 
											<span class="spysr-form-element_data"> ${userProfile.userDetail.dateOfBirth != null && userProfile.userDetail.dateOfBirth != '' ? userProfile.userDetail.dateOfBirth : 'N/A'}</span>
											</span>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Phone 1  <br>
												<span class="spysr-form-element_data"> <s:property value="userProfile.mobile" /></span>
											</span>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Phone 2  <br>
											<span class="spysr-form-element_data"> ${userProfile.userDetail.phone != null && userProfile.userDetail.phone != '' ? userProfile.userDetail.phone : 'N/A'}</span>
											</span>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Language  <br>
											<span class="spysr-form-element_data">	${userProfile.userDetail.language != null && userProfile.userDetail.language != '' ? userProfile.userDetail.language:'N/A'}
											</span></span>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Address  <br>
											<span class="spysr-form-element_data">	${userProfile.userDetail.address != null && userProfile.userDetail.address != '' ? userProfile.userDetail.address:'N/A'}
												</span></span>
										</div>
									</div>
									<div class="col-md-12 col-sm-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">About Me  <br>
											<span class="spysr-form-element_data">	${userProfile.userDetail.description != null && userProfile.userDetail.description != '' ? userProfile.userDetail.description : 'None'} </span></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="profile-timeline-card">
								<!-- <div class="pull-right">
									<button class="btn btn-xs btn-outline-primary" type="button" > <i class="fa fa-key"></i> Reset Password</button>
									</div> -->
								<div class="">
									<p class="mt-2 mb-4">
										<span class="font-icon"><i class="fa fa-user"></i></span> <span> <b class="blue-grey-text">User Details</b></span>
									</p>
								</div>
								<hr>
								<div class="timeline-footer row">
								
								
								<c:choose>
								<c:when test="${userProfile.userWallet != null}">
								<div class="row">	
								<div class="col-md-12 col-xs-12 mb-2 mt-2">
									<div class="pb-2">
									<div class="font-sm">
										<p class="pull-left text-left"><label class="form-control-label">Oxy Pay Balance</label></p> 
										<p class="pull-right text-right text-success"><i class="fa fa-inr" style="font-size:12px"></i>  <fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userProfile.userWallet.walletBalance}" /></p>
									</div>
									</div>
								</div>
								<div class="col-md-12 col-xs-12">
									<div class="">
										<p class="touroxy-form-element_label"><span class="test-danger">Oxy Money</span></p> <p class="pull-right text-right"><i class="fa fa-inr" style="font-size:12px"></i> 
										<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userProfile.userWallet.depositBalance}" /></p>
									</div>
								</div>
								<div class="col-md-12 col-xs-12">
									<div class="">
										<p class="touroxy-form-element_label"><span class="test-success">Oxy Back</span></p> <p class="pull-right text-right"><i class="fa fa-inr" style="font-size:12px"></i> 
										<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userProfile.userWallet.cashbackBalance}" /></p>
									</div>
								</div>
								<div class="col-md-12 col-xs-12">
									<div class="">
										<p class="touroxy-form-element_label"><span class="test-primary">Oxy Credit</span></p> <p class="pull-right text-right"> <span>Pt.</span>
										<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userProfile.userWallet.creditBalance}" />
										</p>
									</div>
								</div>
								</div>
							</c:when>
							<c:otherwise>
							<div class="col-md-12 col-xs-12 mb-2 mt-2">
									<div class="pb-2">
									<div class="font-sm">
										<p class=" text-center">No Wallet Found</p>
									</div>
									</div>
								</div>
							</c:otherwise>
							</c:choose>
									<div class="col-md-12 col-xs-12 mb-2 mt-2">
										<div class="">
										<c:forEach items="${userRoleMap}" var="userRole" varStatus="j">
										<c:if test="${userRole.value!=null && userRole.value.size()>0}">
											<div class="touroxy-form-element_label"><b>${userRole.key}</b> <br>
												<ul class="inline-list">
													<c:forEach items="${userRole.value}" var="userRoleInner" varStatus="j">
														<li class="roles_css"><span class="spysr-form-element_data" ><i class="fa fa-check chack-list"></i> &nbsp; ${userRoleInner}</span></li>
													</c:forEach>
												</ul>
											</div>
										</c:if>
										</c:forEach>
										</div>
									</div>
									<div class="col-md-12 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Parent Company Id  <br>
												<span class="spysr-form-element_data">
												${userProfile.companyUserId}
												</span></span>
										</div>
									</div>
									<div class="col-md-12 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Created On :  <br>
												<span class="spysr-form-element_data">
												${spysr:formatDate(userProfile.createdDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy - hh:mm a')}
												</span> </span>
										</div>
									</div>
									<div class="col-md-12 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Modified On :  <br>
												<span class="spysr-form-element_data">
												${spysr:formatDate(userProfile.updatedDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy - hh:mm a')}
											</span></span>
										</div>
									</div>
									<div class="col-md-12 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Email Sent Count :  <br>
												<c:choose>
								                <c:when test="${emailCountStatusList.size() > 0}">
								                ${emailCountStatusList.size()} 
								                <span class="link">
								               <span class="spysr-form-element_data"> <a data-email-content="popover-content" data-placement="left" data-toggle="popover" 
								               title="Email Sent History" data-container="body" data-html="true" href="#!" id="login" style="cursor: pointer;" class="">times
								                </a>
								                </span></span>
								                </c:when>
								                <c:otherwise>
													0 times														                
								                </c:otherwise>
								                </c:choose>
											 </span>
										</div>
										<!-- email history popover -->
										<div id="popover-content" class="hidden">
								        <div class="row">
								        <table class="table table-bordered table-striped-column table-hover mb-0">
						                               <thead>
						                               <tr class="small-trip-table">
											<th>SNo</th>
											<th>Mail Status</th>
											<th>Send At</th>
											</tr>
										</thead>
											<tbody>
											<c:forEach items="${emailCountStatusList}" var="emailData" varStatus="estatus">
											<tr class="small-trip-table">
											<td>${estatus.count}</td>
											<td>
											<c:choose>
											<c:when test="${emailData.mailStatus == 1}">
											<span class="text-success">Success</span>
											</c:when>
											<c:when test="${emailData.mailStatus == 0}">
											<span class="text-warning">Pending</span>
											</c:when>
											<c:when test="${emailData.mailStatus == -1}">
											<span class="text-danger">Failed</span>
											</c:when>
											</c:choose>
											</td>
											<td>
											${spysr:formatDate(emailData.createdDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy hh:mm a')}
											</td>
											</tr>
											</c:forEach>
										</tbody></table>
								        </div>
				</div>
										
									</div>
								</div>
							</div>
						</div>
						
				</div>


				<div id="user_edit_detail">
					<div class="modal fade" id="edit-user" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Edit User Field</h4>
									<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="14" alt="Copy to clipboard">
									</button>
								</div>
								<form id="updateUserForm" name="updateUserForm" method="post" class="form-horizontal">
									<input type="hidden" name="id" value="<s:property value="userProfile.id"/>">
									<input type="hidden" name="userRoleId.roleid"	value="<s:property value="userRoleId.roleid"/>">
									<div class="modal-body">
										<div class="row">
											<div class="form-group">
												<label class="form-control-label col-sm-4" for="username"><span class="text-danger"></span>User Name</label>
												<div class="col-md-8">
												<div class="controls">
														<input type="text" class="form-control input-sm" id="username" name="userName" value="<s:property value="userProfile.userName"/>" readonly="readonly">
													</div>
												</div>
											</div>
													<div class="form-group">
												<label class="form-control-label col-sm-4" for="first-name"><span class="text-danger"></span>First Name</label>
											<div class="col-md-8">
												<div class="controls">
														<input type="text" class="form-control input-sm" id="first-name" name="firstName" value="<s:property value="userProfile.firstName"/>" placeholder="First Name" autocomplete="off" required>
													</div>
												</div>
											</div>
													<div class="form-group">
												<label class="form-control-label col-sm-4" for="last-name"><span
													class="text-danger"></span>Last Name</label>
											<div class="col-md-8">
												<div class="controls">
														<input type="text" class="form-control input-sm" id="last-name" name="lastName" value="<s:property value="userProfile.lastName"/>" autocomplete="off">
													</div>
												</div>
											</div>
													<div class="form-group">
												<label class="form-control-label col-sm-4" for="email"><span
													class="text-danger"></span>Email</label>
											<div class="col-md-8">
												<div class="controls">
														<input type="email" class="form-control input-sm" name="email" id="email" value="<s:property value="userProfile.email"/>" autocomplete="off" readonly="readonly" >
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-sm-4" for="mobile"><span class="text-danger"></span>Phone</label>
											<div class="col-md-8">
												<div class="controls">
														<input type="tel" class="form-control input-sm" name="mobile" id="mobile" value="<s:property value="userProfile.mobile"/>" autocomplete="off" required>
													</div>
												</div>
											</div>
											<s:if test="%{#session.User.userRoleId.isSuperUser()}">
											<div class="form-group">
													<label class="form-control-label col-sm-4" for="userroletype"><span class="text-danger"></span>Employee Role</label>
													<div class="col-md-8">
														<select class="" id="userroletype"name="userRoleTypeArray" multiple="multiple">
															<c:forEach items="${userRolesMap}" var="user_role" varStatus="rowStatus">
															<option value="${user_role.key}">${user_role.value}</option>
															</c:forEach>
														</select>
													</div>
											</div>
											</s:if>
											<div class="form-group">
												<label class="form-control-label col-sm-4" for="verify-status">Email Verify Status</label>
												<div class="col-md-8">
												<div class="checkbox checkbox-success">
								                        <input type="checkbox" name="verifyStatus" id="verify-status"  value="${userProfile.verifyStatus}" class="mail-status" ${userProfile.verifyStatus == true ? 'checked': ''}>
								                        <label for="verify-status">&nbsp;&nbsp;Verify</label>
							                   		</div>
							                   </div>
											</div>
										</div>
									</div>
									<div class="modal-simple-footer">
											<button type="submit" class="btn btn-sm btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
											<button type="submit" class="btn btn-sm btn-success" id="edit-user-btn">Save</button>
										</div>
								</form>
							</div>

						</div>
					</div>
					<div class="modal fade" id="edit-user-profile" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Edit User Profile Field</h4>
									<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="14" alt="Copy to clipboard">
									</button>
								</div>
								<form id="updateUserDetailForm" name="updateUserDetailForm" method="post" class="form-horizontal">
									<input type="hidden" name="id" value="<s:property value="userProfile.userDetail.id"/>">
									<input type="hidden" name="userId" value="<s:property value="userProfile.id"/>">
									<div class="modal-body">
										<div class="row mt-3">
											<div class="form-group">
												<label class="form-control-label col-sm-4" for="telphone"><span class="text-danger"></span>Alternate Number</label>
											<div class="col-md-8">
												<div class="controls">
														<input type="tel" class="form-control input-sm" name="phone" id="telphone" value="${userProfile.userDetail.phone}" autocomplete="off" required>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-sm-4" for="dateOfBirth"><span class="text-danger"></span>Date Of Birth</label>
											<div class="col-md-8">
												<div class="controls">
														<input type="text" class="form-control " name="dateOfBirth" id="dateOfBirth" value="${userProfile.userDetail.dateOfBirth}" autocomplete="off" >
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="description"><span
													class="text-danger"></span>About Me</label>
											<div class="col-md-8">
												<div class="controls">
														<textarea class="form-control input-sm" id="description" name="description" autocomplete="off" rows="4">${userProfile.userDetail.description}</textarea>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="address"><span
													class="text-danger"></span>Address</label>
											<div class="col-md-8">
												<div class="controls">
														<textarea class="form-control input-sm" id="address" name="address" placeholder="Address" autocomplete="off" required>${userProfile.userDetail.address}</textarea>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="city"><span
													class="text-danger"></span>City</label>
												<div class="col-md-8">
												<div class="controls">
														<input type="tel" class="form-control input-sm" name="city" id="city" value="${userProfile.userDetail.city}"autocomplete="off">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="zip_code"><span
													class="text-danger"></span>Zip</label>
												<div class="col-md-8">
												<div class="controls">
														<input type="tel" class="form-control input-sm" name="zipCode" id="zip_code" value="${userProfile.userDetail.zipCode}"autocomplete="off">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="state"><span
													class="text-danger"></span>State</label>
												<div class="col-md-8">
												<div class="controls">
														<select class="form-control input-sm" name="state" id="state" >
															<option selected  value="">Select State</option>
															<c:forEach items="${statesList}" var="states">
																<option value="${states.state}" ${states.state == userProfile.userDetail.state}>${states.state}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="country-name"><span
													class="text-danger"></span>Country</label>
											     <div class="col-md-8">
												<div class="controls">
														<select class="form-control input-sm" name="countryName" id="country-name" required>
															<s:iterator value="CountryList">
																<option value="<s:property value="c_name"/>" ${c_name == userProfile.userDetail.countryName ? 'selected' : ''}><s:property value="c_name"></s:property></option>
															</s:iterator>

														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-simple-footer">
											
											<button type="submit" class="btn btn-sm btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
											<button type="submit" class="btn btn-sm btn-success" id="edit-user-profile-btn">Save</button>
										</div>
								</form>
							</div>

						</div>
					</div>
					<div class="modal fade" id="reset-user-password" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Reset Password Field</h4>
									<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="14" alt="Copy to clipboard">
									</button>
								</div>
								<form id="changeUserPasswordForm" name="changeUserPasswordForm" method="post" class="form-horizontal">
									<input type="hidden" name="id" value="<s:property value="userProfile.id"/>">
									<div class="modal-body">
										<div class="row mt-3">
											<div class="form-group">
												<label class="form-control-label col-sm-4" for="telphone"> Old Password <span class="text-danger">*</span></label>
											<div class="col-md-8">
													<div class="controls">
														<input type="password" name="oldPassword" id="old-password" autocomplete="off" class="form-control input-sm" required data-user-id="${userProfile.id}" >
														<span id="passwd-message"></span>
													</div>
														<span id="reset-password"></span>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-sm-4" for="telphone"> New Password <span class="text-danger">*</span></label>
											<div class="col-md-8">
												<div class="controls">
														<input type="password" name="newPassword" id="new-password" autocomplete="off" class="form-control input-sm" required>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-sm-4" for="confirmPwd"><s:text name="tgi.label.confirm_password" /> <span class="text-danger">*</span></label>
											<div class="col-md-8">
												<div class="controls">
														<input type="password" name="confirmPassword" id="confirmPwd"  autocomplete="off" class="form-control input-sm" required>
														<span id="error-message"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-simple-footer">
											
											<button type="submit" class="btn btn-sm btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
											<div id="remove-save-button" style="display:block;">
												<button type="submit" class="btn btn-sm btn-success" id="reset-user-password-btn">Save</button>
											</div>
											<div id="reset-password"></div>
										</div>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--ADMIN AREA ENDS-->
<script src="admin/js/admin/password-validate.js"></script>
<script type="text/javascript">
$(function() {
	  $('#dateOfBirth').daterangepicker({
	    singleDatePicker: true,
	    showDropdowns: true,
	    maxDate: new Date()
	  }, function(start, end, label) {
		  
	  });
	});
/*------------------------------------------------------------*/
	$(document).ready(function() {
		$("#userroletype").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a User Role",
		});

		var arrayUserRoleType = '${userRoleTypeView}'.split(',');
		console.debug(arrayUserRoleType);
		$("#userroletype").multipleSelect("setSelects", arrayUserRoleType);

	});
/*--------------------------------------------------------------------*/
	$("#updateUserForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			url : "userProfileUpdate",
			type : "POST",
			dataType : 'json',
			data : $("#updateUserForm").serialize(),
			success : function(jsonData) {
				if(jsonData.message.status == 'success'){
					alertify.success(jsonData.message.message)
					setTimeout(location.reload.bind(location), 1000);
				}
				else if(jsonData.message.status == 'error'){
					 alertify.error(jsonData.message.message);
				}
			},
			error : function(request, status, error) {
				alertify.error("user can't be updated,please try again");
			}
		});
	}); 
/*-----------------------------------------------*/ //update user detail 
	$("#updateUserDetailForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			url : "userDetailUpdate",
			type : "POST",
			dataType : 'json',
			data : $("#updateUserDetailForm").serialize(),
			success : function(jsonData) {
				if(jsonData.message.status == 'success'){
					alertify.success(jsonData.message.message)
					setTimeout(location.reload.bind(location), 1000);
				}
				else if(jsonData.message.status == 'error'){
					 alertify.error(jsonData.message.message);
				}
			},
			error : function(request, status, error) {
				alertify.error("user details can't be updated,please try again");
			}
		});
	}); 
	/*-------------------------------------------*/
	function userProfileUploadFn(userId){
			 var formData = new FormData($("#imageUploadForm")[0]);
			    $.ajax({
			        url: "uploadUserProfileImage?userId="+userId,
			        type: 'POST',
			        data: formData,
			        async: false,
			        dataType: 'json',
			        success: function (jsonData) 
			        {
			        	if(jsonData.json.status == 'success'){
							$.notify({icon: 'fa fa-check',message: jsonData.json.message},{type: 'success'});
							window.location.reload();
							}
		  				else if(jsonData.message.status == 'error')
		  				{
		  					$.notify({icon: 'fa fa-times',message: jsonData.json.message},{type: 'danger'});
		  					}
			        	
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });	
		}
	/*-------------------------------------------*/ //change password ajax
	$(document).ready(function() { 
		$('#changeUserPasswordForm').bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				newPassword : {
					validators : {
						notEmpty : {
							message : 'The new password is required and cannot be empty'
						},
						callback : {
							message : 'The password is not valid',
							callback : function( value, validator, $field) {
								if (value === '') {
									return true;
								}
								// Check the password strength
								if (value.length < 8) {
									return {
										valid : false,
										message : 'It must be more than 8 characters long'
									};
								}
								// The password doesn't contain any uppercase character
								if (value === value
										.toLowerCase()) {
									return {
										valid : false,
										message : 'It must contain at least one upper case character'
									}
								}
								// The password doesn't contain any uppercase character
								if (value === value
										.toUpperCase()) {
									return {
										valid : false,
										message : 'It must contain at least one lower case character'
									}
								}
								// The password doesn't contain any digit
								if (value
										.search(/[0-9]/) < 0) {
									return {
										valid : false,
										message : 'It must contain at least one digit'
									}
								}
								// The password doesn't contain any special Charter
								if (value
										.search(/[0-9]/) < 0) {
									return {
										valid : false,
										message : 'It must contain at least one digit'
									}
								}
								return true;
							}
						}
					}
				},
				oldPassword : {
					validators : {
						notEmpty : {
							message : 'The old password is required and cannot be empty'
						}
					}
				},
				confirmPassword : {
					validators : {
						notEmpty : {
							message : 'The confirm password is required and cannot be empty'
						},
						identical : {
							field : 'newPassword',
							message : 'The password and its confirm are not the same'
						}
					}
				}
			}
		})
.on('error.form.bv', function(e) {
})
.on('success.form.bv',function(e) {
			e.preventDefault();
			$.ajax({
					url : "update_user_password",
					type : "POST",
					dataType : 'json',
					data : $("#changeUserPasswordForm").serialize(),
					success : function(jsonData) {
							if(jsonData.json.status == 'success'){
								alertify.success(jsonData.json.message);
								window.location.reload();
							}
							else if(jsonData.json.status == 'error')
			  				{
								$("#error-message").html("<img class='' src='admin/img/svg/close.svg' width='8'>&nbsp;<span class='text-danger' style='font-size: 12px;'>"+jsonData.json.message+"</span>");
			  				}
							$("#reset-passwordBtn").removeAttr("disabled");
							var $form = $(e.target), validator = $form
							.data('bootstrapValidator');
							$form.bootstrapValidator('change_passwordBtn',false)
							.bootstrapValidator('resetForm',true); // Reset the form
					},
					error : function(request,status, error) {
						alertify.error("Somthing Problem, Try Again.");
					}
				});
			})
			.on('status.field.bv', function(e, data) {
				if (data.bv.getSubmitButton()) {
					console.debug("button disabled ");
					data.bv.disableSubmitButtons(false);
				}
			});
});
</script>
<script>
$(document).ready(function(){
	$("#old-password").change(function() { 
	var oldPassword = $("#old-password").val();
	var userId = $(this).data('user-id');
	if(oldPassword.length >= 4)
	{
		$("#passwd-message").html('<img src="admin/img/loading.gif" align="absmiddle">&nbsp;Checking availability...');
	    $.ajax({  
	    url: "verify_user_password?password="+oldPassword+"&id="+userId,  
	    type: "POST",  
	    dataType: 'json',  
	    success: function(jsonResponse){  
	  		if(jsonResponse.message.status == 'success')
	  		{
	  			$("#old-password").removeClass('object_error'); // if necessary
				$("#old-password").addClass("object_ok");
				$("#passwd-message").html("<img class='' src='admin/img/svg/checked_1.svg' width='10'>&nbsp;<span class='' style='font-size: 12px;color: #3cc341;'>"+jsonResponse.message.message+"</span>");
				$("#reset-user-password-btn").prop("disabled", false);
				
			}
			else if(jsonResponse.message.status == 'error')
			{
				$("#old-password").removeClass('object_ok'); // if necessary
				$("#old-password").addClass("object_error");
				$("#passwd-message").html("<img class='' src='admin/img/svg/close.svg' width='8'>&nbsp;<span class='text-danger' style='font-size: 12px;'>"+jsonResponse.message.message+"</span>");
				$("#reset-password").html('<a href="email_forget_password" class="pull-right" id="forget-password-btn">Forget Password</a>');
				$("#reset-user-password-btn").prop("disabled", true);
			}
	   //});
	 } 
	   
}); 
}
else
	{
	$("#passwd-message").html('<font color="red" style="font-size: 12px;">The password should have at least <strong>4</strong> characters.</font>');
	$("#old-password").removeClass('object_ok'); // if necessary
	$("#old-password").addClass("object_error");
	}

	});
	});
</script>