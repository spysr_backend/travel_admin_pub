<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

   
<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.show_date_to_user {
    padding: 0px 15px;
    position: relative;
    width: 93%;
    top: -25px;
    left: 5px;
    pointer-events: none;
    display: inherit;
    color: #000;
}
</style>

       
<script type="text/javascript">

function deletePopupCountryInfo(id,version)
{
	$('#alert_box_delete').modal({
  	    show:true,
  	    keyboard: false
	    } );
	$('#alert_box_delete_body').text("Are your sure want to delete ?");
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	var finalUrl = newUrl+"deleteMarkupList.action?markupId="+id+"&version="+version;
	  $("#deleteItem").val(finalUrl);
}

$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"flightMarkupList";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	$('#alert_box_delete_ok').click(function() {
		window.location.assign($("#deleteItem").val()); 
			$('#alert_box_delete').hide();
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 <script type="text/javascript">
	$(document).ready(
			function() {

				var company_list = [];
				var agents_list = [];
				  var user_list = []; 
				 

				$.ajax({
					//Action Name
					url :"CompanyListUnderSuperUser",
					dataType : "json",
					data : {
					 parent_company_user_id : $("#companyUserId").val(),
						email : $("#email").val()
					},

					success : function(data, textStatus, jqXHR) {

						var items = data.records;
						for (var i = 0; i < data.records.length; i++) {
							company_list.push(data.records[i].companyname + ","
									+ data.records[i].company_userid);
						}
						console.log(company_list);
						//response(items);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus);
					}
				});

				$("#search").autocomplete(
						{

							source : function(request, response) {
								var matcher = new RegExp('^'
										+ $.ui.autocomplete
												.escapeRegex(request.term),
										"i");
								response($.grep(company_list, function(item) {
									return matcher.test(item);

								}));
							},
							select : function(event, ui) {
								var label = ui.item.label;
								var company_userid = ui.item.value;
								console.log(company_userid);
							if(company_userid!=null){ 
								$.ajax({
									//Action Name
									url : "UserListUnderCompany",
									dataType : "json",
									data : {
										 company_user_id : company_userid 
									},

									success : function(data, textStatus, jqXHR) {
										 user_list=[];
										console.log("--data---------"+data);
										
										var items = data.user_records;
										
										for (var i = 0; i < data.user_records.length; i++) {
											user_list.push(data.user_records[i].username + "("+data.user_records[i].company_userid+")"  + ","
													+ data.user_records[i].id);
										}
										console.log(user_list);
										userlist(user_list);
									},
									error : function(jqXHR, textStatus, errorThrown) {
										console.log(textStatus);
									}
								});
							}
							
							 
							 }
						});
				
				
				$.ajax({
					//Action Name
					url :"AgentsListUnderSuperUser",
					dataType : "json",
					data : {
					 parent_company_user_id : $("#companyUserId").val(),
						email : $("#email").val()
					},

					success : function(data, textStatus, jqXHR) {

						var items = data.agentList;
						for (var i = 0; i < data.agentList.length; i++) {
							agents_list.push(data.agentList[i].username + "("+data.agentList[i].company_userid+")"  + ","
									+ data.agentList[i].id);
						}
						console.log("------agents_list------"+agents_list);
						 user_list=agents_list;
							console.log("------user_list------"+user_list);
						 userlist(user_list);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus);
					}
				});
				
			  });
	
	 function userlist(userlist)
			{
		 if(userlist.length>0){
				$("#userIdSearch").autocomplete(
						{
		 				source : function(request, response) {
								var matcher = new RegExp('^'
										+ $.ui.autocomplete
												.escapeRegex(request.term),
										"i");
								response($.grep(userlist, function(item) {
									return matcher.test(item);

								}));
							}
						});	 
		 }
		  
	}
 </script>
 
     <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->

            <section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                        <div class="card1">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.flight_markup_list" /></h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                                <div class="set pull-right">
                                  
                                  
                                    <a id="add_button" class="btn btn-xs btn-success " href="addMarkup"><i
							class="fa fa-plus fa-lg" aria-hidden="true"
							style="margin-right: 6px;"></i>Create<span class="none-xs">
								<s:text name="tgi.label.flight_markup" /></span></a>
                             
                               </div>
                                  <div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
                               </div>
                                <br>
                                <form action="flightMarkupList" class="filter-form">
                                <div class="" id="filterDiv" style="display:none;" >
                                
                                <s:if test="%{#session.Company!=null}">
									<s:if test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
                                 <div class="col-md-2 col-sm-6">
                                 <div class="form-group">
                                <input type="hidden" id="HdncompanyTypeShowData" value="" />
                                <select name="companyTypeShowData" id="companyTypeShowData" class="form-control input-sm">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="my"><s:text name="tgi.label.my_self" /></option>
                                <s:if test="%{#session.Company!=null}">
									<s:if test="%{#session.Company.companyRole.isSuperUser()}">
	                                <option value="distributor"><s:text name="tgi.label.distributor" /></option>
	                                </s:if>
                                </s:if>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div></div>
                                </s:if>
                                </s:if>
                                <div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="startDate" id="startDate"
												placeholder="Created Date From........"
												class="form-control search-query date1 input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="endDate" id="endDate"
												placeholder="Created Date To...."
												class="form-control search-query date2 input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="deptDate" id="deptDate"
												placeholder="Departure Date...."
												class="form-control search-query date5 input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="arrvDate" id="arrvDate"
												placeholder="Arrival Date........"
												class="form-control search-query date6 input-sm " />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="promofareStartDate" id="promofareStartDate"
												placeholder="PromoStart Date ...."
												class="form-control search-query date3 input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="promofareEndDate" id="promofareEndDate"
												placeholder="PromoStart Date ...."
												class="form-control search-query date4 input-sm" />
								</div>
								</div>
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                <input type="text" name="name" id="markupname-json"
												placeholder="Markup Name...."
												class="form-control search-query input-sm" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="configname" id="configname-json"
												placeholder="Config Name...."
												class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
                                <input type="text" name="airline" id="airline-json"
												placeholder="Airline...."
												class="form-control search-query input-sm" />
                               	</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
                                <input type="text" name="origin" id="origin-json"
												placeholder="Search By Origin...."
												class="form-control search-query input-sm" />
                                	</div>
								</div>
								<div class="row">
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
                                <input type="text" name="destination" id="destination-json"
												placeholder="Search By Destination...."
												class="form-control search-query input-sm" />
                                	</div>
								</div>
                               </div>
							<div class="col-md-1">
								<div class="form-group">
									<input type="reset" class="btn btn-danger" id="configreset"
										value="Clear">
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<input type="submit" class="btn btn-info" value="Search" />
								</div>
							</div>
						</div>
                                </form>
									

							<input type="hidden" value="" id="companyUserId">
							<input type="hidden" value="yogesh@spysr.in" id="email">
							<input type="hidden" value="" id="user_id">
					
					 


<div class="cnt cnt-table">
<div class="dash-table scroller">
			<div class="content mt-0 pt-0">
				<div id="example_wrapper" class="dataTables_wrapper ">
										<table id="example" class="display dataTable responsive nowrap"
									role="grid" aria-describedby="example_info"
									style="width: 100%;" cellspacing="0">
                                        <!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
                                        <thead>
										<tr class="border-radius border-color" role="row">
                                                    <th data-priority="1"><s:text name="tgi.label.s_no" /></th>
                                                    <th data-priority="2"><s:text name="tgi.label.markupname" /></th>
                                                    <th data-priority="3"><s:text name="tgi.label.configname" /> </th>
                                                   <%--  <th><s:text name="tgi.label.fixedamount" /></th> --%>
                                                    <th data-priority="4"><s:text name="tgi.label.airline" /></th>
                                                    <th data-priority="5"><s:text name="tgi.label.origin" /></th>
                                                    <th data-priority="6"><s:text name="tgi.label.destination" /></th>
                                                     <th data-priority="7"><s:text name="tgi.label.departure_date" /></th>
                                                    <th data-priority="8"><s:text name="tgi.label.return_date" /></th>
                                                   
                                                    <th data-priority="9"><s:text name="tgi.label.promostarts" /></th>
                                                    <th data-priority="10"><s:text name="tgi.label.promoends" /></th>
													<th data-priority="11"><s:text name="tgi.label.amount" /></th>
													<th data-priority="12"><s:text name="tgi.label.action" /></th>
													<th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
							<s:iterator value="flightMarkupList" status="rowCount">
								<tr>
									<td data-title="S.No" style="width:30px;"><s:property value="%{#rowCount.count}" /></td>
									<td data-title="MarkupName"><s:property value="name" /></td>
									<td data-title="ConfigName"><s:property value="configname" /></td>
									<%-- <td data-title="FixedAmount"><c:choose> <c:when test="${fixedAmount == true}"> <s:text name="tgi.label.yes" /></c:when><c:otherwise><s:text name="tgi.label.no" /></c:otherwise></c:choose></td> --%>
									<td data-title="Airline"><s:property value="airline" /></td>
									<%-- <td data-title="<s:text name="tgi.label.origin" />">${fn:substring(origin,0,3)}</td>
									<td data-title="<s:text name="tgi.label.destination" />">${fn:substring(destination,0,3)}</td> --%>
									<td data-title="<s:text name="tgi.label.origin" />"><s:property value="origin" /></td>
									<td data-title="<s:text name="tgi.label.origin" />"><s:property value="destination" /></td>
									
									<td data-title="<s:text name="tgi.label.departure_date" />">${spysr:formatDate(deptDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}</td>
									<td data-title="<s:text name="tgi.label.arrival_date" />">${spysr:formatDate(arrvDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}</td>
									
									<td data-title="<s:text name="tgi.label.promostarts" />">${spysr:formatDate(promofareStartDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}</td>
									<td data-title="<s:text name="tgi.label.promoends" />">${spysr:formatDate(promofareEndDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}</td>
									<td data-title="<s:text name="tgi.label.amount" />"><s:property value="markup" /></td>
									<td data-title="<s:text name="tgi.label.action" />">
										<div class="btn-group">
											<a class="btn btn-xs btn-success dropdown-toggle"
												data-toggle="dropdown" style="padding: 3px 6px;"> <i
												class="fa fa-cog fa-lg" aria-hidden="true"></i>
											</a>
											<ul class="dropdown-menu dropdown-info pull-right align-left">
												<li class=""><a
												href="flightMarkupEdit?markupId=<s:property value="markupId"/>"
												class="btn  btn-xs" data-toggle="modal"><span
												class="glyphicon glyphicon-edit"></span> <s:text name="tgi.label.edit" /></a></li>
												<li class="divider"></li>
												<li class=""><input type="hidden" id="deleteItem"> <a
										href="#"
										onclick="deletePopupCountryInfo('${markupId}','${version}')"
										class="btn btn-xs "><span
											class="glyphicon glyphicon-trash"></span><s:text name="tgi.label.delete" /> </a></li>
											</ul>
										</div>
									</td>
									<td></td>
								</tr>
							</s:iterator>
						</tbody>
                                        </table>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        
                                    </div>
								
							</div>
                            </div>
                        </div>
            </section>
        <!--ADMIN AREA ENDS-->
  
  <%--  <script type="text/javascript">
	$(document).ready(
			function() {
				$('.container').perfectScrollbar();
			 });
</script> --%>
<script type="text/javascript" class="init">
$(document).ready(function() {
		$('#filterBtn').click(function() {
	    $('#filterDiv').toggle();
	});
		$('div.easy-autocomplete').removeAttr('style');
} );

$('.faq-links').click(function() {
	var collapsed = $(this).find('i').hasClass('fa-compress');

	$('.faq-links').find('i').removeClass('fa-expand');

	$('.faq-links').find('i').addClass('fa-compress');
	if (collapsed)
		$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
});
	</script>	

 <%-- <script type="text/javascript">
      $(function () {
      
        $('#mytable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
             "info": true,
             "autoWidth": false,
             "search": {
          	    "regex": true,
          	  },
          "pagingType": "full_numbers",
          "lengthMenu": [10, 25, 50, 75, 100, 500 ],
          
            });
      });
    </script>
    
    <script>
    
    $(document).ready( function() {
        $('.ddd').dataTable( {
          "aLengthMenu": [[1, 5, 10, 20, -1], [1, 5, 10, 20, "All"]]
        } );
      } );
    </script>
   --%>  
    

 
 <link href="admin/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	   	 <script src="admin/js/jquery-ui.js" ></script>
         <script>
			$(document).ready(function() {
				var date1="${spysr:formatDate(startDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
				var date2="${spysr:formatDate(endDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
				 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
				 
				 var date3="${spysr:formatDate(startDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
					var date4="${spysr:formatDate(endDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
					 spysr_date_custom_plugin('filter-form','date3','date4','mm/dd/yyyy','MMM DD YYYY',date3 ,date4);
					 
				 var date5="${spysr:formatDate(startDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
					var date6="${spysr:formatDate(endDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
					 spysr_date_custom_plugin('filter-form','date5','date6','mm/dd/yyyy','MMM DD YYYY',date5 ,date6);
			});
		</script>

 
                   	   <s:if test="message != null && message  != ''">
						<%--  <script src="admin/js/jquery.min.js"></script> --%>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'deleted')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
						
						<script type="text/javascript" class="init">
	$(document).ready(function() {
			$('#filterBtn').click(function() {
		    $('#filterDiv').toggle();
		});

		$('div.easy-autocomplete').removeAttr('style');
	} );
	
	
	$('.faq-links').click(function() {
		var collapsed = $(this).find('i').hasClass('fa-compress');

		$('.faq-links').find('i').removeClass('fa-expand');

		$('.faq-links').find('i').addClass('fa-compress');
		if (collapsed)
			$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
	});
		</script>	
						<script>
//markup name
	var MarkupName = {
		url: "getFlightMarkupJson.action?data=markupName",
		getValue: "markupName",
		
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#markupname-json").easyAutocomplete(MarkupName);
	
	// config name
	var ConfigName = {
		url: "getFlightMarkupJson.action?data=configName",
		getValue: "configName",
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#configname-json").easyAutocomplete(ConfigName);
	
	var Airline = {
			url: "getFlightMarkupJson.action?data=airline",
			getValue: "airline",
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#airline-json").easyAutocomplete(Airline);
		
	var Origin = {
			url: "getFlightMarkupJson.action?data=origin",
			getValue: "origin",
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#origin-json").easyAutocomplete(Origin);
		
	var Destination = {
			url: "getFlightMarkupJson.action?data=destination",
			getValue: "destination",
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#destination-json").easyAutocomplete(Destination);
		
		
		$(document).ready(function(){$(".filterBtn").click(function(){$("#filterDiv").toggle(500)}),
			 $("div.easy-autocomplete").removeAttr("style"),$("#configreset").click(function(){$("#resetform")[0].reset()})}),
			 $(".filter-link").click(function(){var e=$(this).find("i").hasClass("fa-angle-up");$(".filter-link").find("i").removeClass("fa-angle-down"),
			 $(".filter-link").find("i").addClass("fa-angle-up"),e&&$(this).find("i").toggleClass("fa-angle-up fa-2x fa-angle-down fa-2x")});
	</script>	