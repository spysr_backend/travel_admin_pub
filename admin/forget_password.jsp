<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link rel="stylesheet" href="admin/css/bootstrap-datepicker3.css">
<link rel="stylesheet" href="admin/css/bootstrap.min.css">  
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script>
</script>
<br><br><br><br><br><br>
  <main>
   <div class="container">
   
   <section class="pageregister">
   
   <div class="panel panel-default pannewdef">
  <div class="panel-heading pannewhead">
    <h3 class="panel-title"><s:text name="tgi.label.verify_email" /></h3>
  </div>
  <div class="panel-body">
 
 <div id="checkemail">
 <form action="email_verify_email" class="home-login-form"> 
  <div class="form-group col-md-offset-2 newfrmgrp">
   <label class="col-sm-2 control-label" for="inputEmail3"><s:text name="tgi.label.email" /></label>
    <div class="col-sm-10">
      <input type="text" name="email" placeholder="Enter your email" id="inputEmail3" class="form-control">
    </div>
  </div>
<br><br>
  <div class="form-group col-md-offset-4">
  <input type="submit" value="Verify Email" id="submit-btn" class="btn btn1new">
   </div>
  </form>
  </div>
</div>
</div>
</section>
</div>
   </main>
