<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"editFlightDeals?id="+"${flightDeals.id}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                         <div class="hd clearfix">
                                <h5 class="pull-left">Add Flight Deals 
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="metatagpage_list"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.meta_dat_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div>
                             <div class="hd clearfix">
                            </div>
                              <div class="cnt cnt-table">
					<div class="table-responsive no-table-css">
						<form action="updateFlightDeals.action" method="post">
							<table class="table table-form">
								<thead>
									<tr>
										<th></th>
										<th>&nbsp;</th>
									</tr>
								</thead>
								<tbody>
								<tr>
										<td>
											<div class="field-name">
												Deal-Location
											</div>
										</td>
										<td>
										<select name="dealLocation" class="form-control">
										<option value='Fly to Seoul'  ${flightDeals.dealLocation == 'Fly to Seoul' ?'selected' :''}  >
										Fly to Seoul
										</option>
										<option value='Fly to Beijing' ${flightDeals.dealLocation == 'Fly to Beijing' ?'selected' :''} >
										Fly to Beijing
										</option>
										<option value='Fly Shanghai'  ${flightDeals.dealLocation == 'Fly Shanghai' ?'selected' :''} >
										Fly Shanghai
										</option>
										<option value='Fly to Manila'   ${flightDeals.dealLocation == 'Fly to Manila' ?'selected' :''}>
										Fly to Manila
										</option>
										</select>
										</td>
										<td>
											<div class="field-name">
												Priority
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="priority" value="${flightDeals.priority}" placeholder="Priority" autocomplete="off"
											required></td>


									</tr>
									<tr>
										<td>
											<div class="field-name">
												Origin
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="origin" value="${flightDeals.origin}" placeholder="Origin" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Destination
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="destination" value="${flightDeals.destination}" placeholder="Destination" autocomplete="off"
											required></td>


									</tr>

									<tr>
									<td>
											<div class="field-name">
												Crrier Code
											</div>
										</td>
										<td><input type="text" name="carrierCode" value="${flightDeals.carrierCode}" placeholder="Carrier Code" class="form-control" /></td>
										<td>
											<div class="field-name">
												Trip Type
											</div>
										</td>
 										<td><select name="tripType" class="form-control">
										<option value="O" ${flightDeals.tripType == 'O' ?'selected' :''} >One-Way</option>
										<option value="R" ${flightDeals.tripType == 'R' ?'selected' :''} >Round-Trip</option>
										</select> </td>
									</tr>
									<tr>
									<td>
											<div class="field-name">
												Base Fare
											</div>
										</td>
										<td><input type="text" name="baseFare" value="${flightDeals.baseFare}" placeholder="Base Fare" class="form-control" /></td>
										<td>
											<div class="field-name">
												Tax
											</div>
										</td>
										<td><input type="text" name="tax" value="${flightDeals.tax}" placeholder="Tax" class="form-control" /></td>
									</tr>
									<tr>
									<td>
											<div class="field-name">
												Price
											</div>
										</td>
										<td><input type="text" name="price" value="${flightDeals.price}" placeholder="Price" class="form-control" /></td>
									<td>
											<div class="field-name">
												Service Charge
											</div>
										</td>
										<td><input type="text" name="serviceCharge" value="${flightDeals.serviceCharge}" placeholder="ServiceCharge" class="form-control" /></td>
									</tr>	
									<tr>
										<td>
											<div class="field-name">
												Cash Back
											</div>
										</td>
										<td><input type="text" name="cashBack" value="${flightDeals.cashBack}" placeholder="Cash Back" class="form-control" /> </td>
									<td>
											<div class="field-name">
												Currency Code
											</div>
										</td>
										<td><input type="text" name="currency" value="${flightDeals.currency}" placeholder="Currency Code" class="form-control" /> </td>
									</tr>
										<tr>
										<td><div class="field-name">
												Classes
											</div>
										</td>
										<td><input type="text" name="classes" value="${flightDeals.classes}" placeholder="Classes" class="form-control" /> </td>
										<td>
											<div class="field-name">
												Advance Purchased
											</div>
										</td>
										<td><input type="text" name="advancePurchased" value="${flightDeals.advancePurchased}" placeholder="Advance Purchased" class="form-control" /> </td>
									</tr>
										<tr>
										<td>
											<div class="field-name">
												Flight Application
											</div>
										</td>
										<td><input type="text" name="flightApplication" value="${flightDeals.flightApplication}" placeholder="Flight Application" class="form-control" /> </td>	
										<td>
											<div class="field-name">
												Minimum Stay
											</div>
										</td>
										<td><input type="text" name="minimumStay" value="${flightDeals.minimumStay}" placeholder="Minimum Stay" class="form-control" /></td>
									</tr>
							 		<tr>
										<td>
											<div class="field-name">
												Maximum Stay
											</div>
										</td>
										<td><input type="text" name="maximumStay" value="${flightDeals.maximumStay}" placeholder="Maximum Stay" class="form-control" /> </td>
										<td>
											<div class="field-name">
												Ticketing Date
											</div>
										</td>
										<td><input type="text" name="ticketingDate" value="${flightDeals.ticketingDate}" id="ticketingDate" placeholder="Ticketing Date" class="form-control" /></td>
									</tr>
									
																		<tr>
										<td>
											<div class="field-name">
												Departure Date From
											</div>
										</td>
										<td><input type="text" name="departureDateFrom" value="${flightDeals.departureDateFrom}" id="departureDateFrom" placeholder="Departure Date From" class="form-control" /> </td>
										<td>
											<div class="field-name">
												Departure Date Till
											</div>
										</td>
										<td><input type="text" name="departureDateTill" value="${flightDeals.departureDateTill}" id="departureDateTill" placeholder="Departure Date Till" class="form-control" /></td>
									</tr>
 									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td>
										<input type="hidden" name="id" value="${flightDeals.id}" /> 
										<input type="hidden" name="version" value="${flightDeals.version}" /> 
										<input type="submit" class="btn btn-info"></td>
									</tr>
								</tbody>
							</table>

						</form>
					</div>
				</div>
</div></div></div>
                            </section>        
                                    			<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	 <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_body').text("${message}");
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
						
 <script>
 $(document).ready(function() 
 { 
	 $("#ticketingDate").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 $("#departureDateFrom").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 $("#departureDateTill").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 }); 
 </script>
							