<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --%>
<script src="admin/js/jspdf.min.js"></script>
<%-- <script src="admin/js/jspdf.debug.js"></script> --%>
<script>
	$(function() {

		$('#pdf').click(function() {
			var doc = new jsPDF();
			doc.addHTML($('#invoice')[0], 15, 15, {
				'background' : '#fff',
			}, function() {
				doc.save('sample-file.pdf');
			});
		});
	});
</script>

<title><s:text name="tgi.label.commission_invoice" /></title>
<!--  -->
<section class="wrapper container-fluid">
	<div class="row">
		<div class="">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.hotel_agent_invoice" />
					</h5>
					<div class="set pull-right">
						<button class="btn btn-success btn-xs"
							onclick="sendCustomerInvoiceToCustomer();">
							<s:text name="tgi.label.send_invoice" />
							<i class="fa fa-arrow-circle-right"></i>
						</button>
						<button type="button" class="btn btn-success btn-xs" id="h4"
							style="display: none">
							<s:text name="tgi.label.sending" />
							<i class="fa fa-arrow-circle-right"></i>
						</button>
					</div>
					<div class="set pull-right">
						<a class="btn btn btn-primary btn-xs"
							href="hotelAgentCommInvoiceList"><span class="pull-right">
								<span class="glyphicon glyphicon-step-backward"></span> <s:text
									name="tgi.label.back" />
						</span></a>

					</div>
					
					<div class="set pull-right">
						<a href="<%=request.getContextPath()%>/hotelAgentInvoice?orderId=${hotelInvObj.yourRef}" target="blank" class="popup btn btn-info btn-xs">
						<span class="pull-right">
							Print
						</span></a>
					</div> 
				</div>
				
				<div class="col-md-12 mt-2"> 
				<div class="profile-timeline-card">   
								<i class="fa fa-user header-icons timeline-img-user pull-left" style="box-shadow: none;"></i> 
						<div class="">
							<p class="mt-2 mb-4">
								<span> <b class="blue-grey-text">${hotelInvObj.company.companyName}</b></span>
							</p>

						</div>
						<hr>   
						<div class="timeline-footer row row-minus"> 
<div class="col-md-4"> 
<h4 class="text-left"><span>From</span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b>Address</b></td>
            <td> ${hotelInvObj.company.address}</td>
          </tr>
          <tr>
            <td><b>Tel</b></td>
            <td>${hotelInvObj.company.phone}</td>
          </tr> 
          <tr>
            <td><b>Email</b></td>
            <td>${hotelInvObj.company.email}</td>
          </tr> 
          <tr>
            <td><b>Website</b></td>
            <td>${hotelInvObj.company.website}</td>
          </tr>
          </tbody>
        </table>
     </div>				  
 <%-- <s:if test="hotelInvObj.userDetails.size>0">
 <s:iterator value="hotelInvObj.userDetails"> --%>
 <c:if test="${hotelInvObj.companyTo!=null}">
     <div class="col-md-4">
     <h4 class="text-left"><span><s:text name="tgi.label.to" /></span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b><s:text name="tgi.label.to" /></b></td>
            <td>${hotelInvObj.companyTo.companyName}</td>
          </tr>
          <tr>
            <td><b><s:text name="tgi.label.address" /></b></td>
            <td>${hotelInvObj.companyTo.address}</td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.tel" /></b></td>
            <td>${hotelInvObj.companyTo.phone}</td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.email" /></b></td>
            <td>${hotelInvObj.companyTo.email}</td>
          </tr>
          </tbody>
        </table>
     </div>		
     </c:if>
 <%-- 	</s:iterator>
 </s:if> --%>	 								
     <div class="col-md-4">
     <h4 class="text-left"><span><s:text name="tgi.label.tax_invoice" /></span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b><s:text name="tgi.label.invoice_no" /></b></td>
            <td> <s:property  value="invoiceData.invNo"/></td>
          </tr>
          <tr>
            <td><b><s:text name="tgi.label.consultant" /></b></td>
            <td><s:property  value="invoiceData.consultant"/></td>
          </tr>
          <tr>
			 <td><b><s:text name="tgi.label.book_no" /></b></td>
			 <td><s:property value="hotelInvObj.bookNo" /></td>
		  </tr>
          <tr>
            <td><b><s:text name="tgi.label.date" /></b></td>
            <td><s:property  value="hotelInvObj.bookingDate"/></td>
          </tr>
          </tbody>
        </table>
     </div>
						</div>
					</div>
					</div>
				
				
				
				<!-- Main content -->
				<!-- Main content -->
				<div class="sccuss-full-updated" id="success-alert"
					style="display: none">
					<div class="succfully-updated clearfix">

						<div class="col-sm-2">
							<i class="fa fa-check fa-3x"></i>
						</div>

						<div id="message" class="col-sm-10"></div>
						<button type="button" id="success" class="btn btn-primary">
							<s:text name="tgi.label.ok" />
						</button>
					</div>

				</div>
				<!-- Small boxes (Stat box) -->
				<div class="row">
					<div class="col-sm-12">
						<%-- <div id="butns" class="clearfix">
   <input type="hidden" value="<s:property  value="hotelInvObj.yourRef"/>" id="orderId"> 
         <s:if test="%{#session.User.userRoleId.isSuperUser()}">
          <input type="hidden" value="super" id="companyType"> 
          <button class=" btn btn-primary but no-print" onclick="sendCustomerInvoiceToCustomer();"><s:text name="tgi.label.send_mail" /> <i class="fa fa-arrow-circle-right"></i> </button>
          </s:if>
          <s:elseif test="%{#session.Company.companyRole.isDistributor()}">
           <button class=" btn btn-primary but no-print" onclick="sendCustomerInvoiceToCustomer();"><s:text name="tgi.label.send_mail" /><i class="fa fa-arrow-circle-right"></i> </button>
           <input type="hidden" value="dis" id="companyType"> 
          </s:elseif>
            <s:elseif test="%{#session.Company.companyRole.isAgent()}">
          <!--  <button class=" btn btn-primary but no-print" onclick="sendCustomerInvoiceToCustomer();">Send Mail <i class="fa fa-arrow-circle-right"></i> </button> -->
          </s:elseif>
          <button type="button" class="btn btn-primary but no-print" id="h4"  style="display: none" ><s:text name="tgi.label.sending" /><i class="fa fa-arrow-circle-right"></i></button>
         </div>   --%>
						<!-- 
        <button  type="button"    class="btn btn-primary but no-print" onclick="jQuery('invoice').print()" >Print <i class="fa fa-arrow-circle-right"></i> </button>
 
           <button type="button"  id="pdf"  class="btn btn-primary but no-print">PDF <i class="fa fa-arrow-circle-right"></i></button>

              <button type="button" class="btn btn-primary but no-print" >Mail   <i class="fa fa-arrow-circle-right"></i></button>	
         <input type="email" name="customerMail"  required="required" id="mail" style="width: 300px" placeholder="mail"   class="btn btn-primary but no-print" >  <button class=" btn btn-primary but no-print" onclick="sendCustomerInvoiceToCustomer();">Send <i class="fa fa-arrow-circle-right"></i> </button>
                       <button type="button" class="btn btn-primary but no-print" id="h4"  style="display: none" >sending-------<i class="fa fa-arrow-circle-right"></i></button>
         
          -->


					</div>
				</div> 
			</div>
		</div>
	</div>
</section>


<section class="col-md-12">
					<div id="invoice" class="clearfix mt-0">
						<div class="profile-timeline-card">
							<div class="">
								<%-- <div class="row">
										<div class="col-xs-4">
											<div class="text-center pro-img">

												<img
													src="<s:url action='getImageByPath?imageId=%{hotelInvObj.company.imagePath}'/>"
													class="avatar img-circle img-thumbnail img-responsive"
													alt="profile image">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="lint-invoice-adde text-right">
												<h2>${hotelInvObj.company.companyName}</h2>
												<p>
													<br>${hotelInvObj.company.address} <br> Tel: ${hotelInvObj.company.phone}<br>
													<b>Email:</b> ${hotelInvObj.company.email}<b> <br>Website:
													</b> ${hotelInvObj.company.website}
												</p>
											</div>
										</div>
									</div> --%>


								<div class="row">
									<%-- <div class="in-head">
										<h4 class="text-center">
											<span><s:text name="tgi.label.tax_invoice" /></span>
										</h4>
									</div>
									<div class="invoice-addres clearfix">
										<div class="col-md-5">
											<div class="panel panel-default ">
												<s:if test="hotelInvObj.userDetails.size>0">
													<s:iterator value="hotelInvObj.userDetails">
													<c:if test="${hotelInvObj.companyTo!=null}">
														<div class="panel-body">
															<h4>
																<s:text name="tgi.label.to" />
																:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<s:property value="hotelInvObj.attn" />
																${hotelInvObj.companyTo.companyName}
															</h4>
															<p>
																<b> <s:text name="tgi.label.address" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																</b>
																<s:property value="hotelInvObj.cusAddress" />

																${hotelInvObj.companyTo.address}
															</p>
															<p>
																<b><s:text name="tgi.label.tel" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
																<s:property value="hotelInvObj.mobile" />

																${hotelInvObj.companyTo.phone}
															</p>
															<!--  <p>Fax: 99876544</p> -->
															<p>
																<b> <s:text name="tgi.label.email" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																</b>
																<s:property value="hotelInvObj.mailId" />
																${hotelInvObj.companyTo.email}
															</p>

														</div>
														</c:if>
													</s:iterator>
												</s:if>
											</div>
										</div>


										<div class="col-md-6">
											<table class="table inn-num table-bordered">
												<tbody>
													<tr>
														<td><s:text name="tgi.label.invoice_no" /></td>
														<td><s:property value="hotelInvObj.invNo" /></td>
													</tr>
														 <tr>
										<td><s:text name="tgi.label.acct_code" /></td>
										<td><s:property value="hotelInvObj.ActCode" /></td>
									</tr>
													<tr>
														<td><s:text name="tgi.label.consultant" /></td>
														<td><s:property value="hotelInvObj.consultant" /></td>
													</tr>
													<tr>
														<td><s:text name="tgi.label.book_no" /></td>
														<td><s:property value="hotelInvObj.bookNo" /></td>
													</tr>

													<tr>
														<td><s:text name="tgi.label.date" /></td>
														<td><s:property value="hotelInvObj.bookingDate" /></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div> --%> 
									<!-- / end client details section -->

									<div class="">
										<table
											class="table table-bordered in-table-border no-table-css">
											<thead>
												<tr class="info">
													<th><h4>
															<s:text name="tgi.label.gst_type" />
														</h4></th>
													<th><h4>
															<s:text name="tgi.label.hotel_room_guest_particulars" />
														</h4></th>
													<th><h4>
															<s:text name="tgi.label.qty" />
														</h4></th>
													<th><h4>
															<s:text name="tgi.label.price" />
														</h4></th>
													<th><h4>
															<s:text name="tgi.label.total" />
															(USD)
														</h4></th>
												</tr>
											</thead>
											<tbody>
												<s:iterator value="hotelInvObj.hotelOrderGuest"
													status="rowCount">
													<tr>
														<td>ZR</td>
														<td>(<s:property value="%{#rowCount.count}" />) <s:property value="firstName" /> <s:property value="lastName" /> 
														<td>1</td>
														<td>0</td>
														<td>0</td>
													</tr>
												</s:iterator>
												<s:iterator value="hotelInvObj.hotelOrderRoomInfo">

												<tr>
													<td><s:text name="tgi.label.sr" /></td>
													<td><s:property value="name" /> <s:property
															value="hotelInvObj.hotelOrderRow.checkInDate" /> <b>-</b> <s:property
															value="hotelInvObj.hotelOrderRow.checkOutDate" /></td>
													<td>1</td>
													<td><s:property value="hotelInvObj.basePrice" /></td>
													<td><s:property value="hotelInvObj.basePrice" /></td>
												</tr>
													</s:iterator>
												<tr>
													<td>SR</td>
													<td>Total Before Markup</td>
													<td>1</td>
													<td><s:property value="hotelInvObj.basePrice" /></td>
													<td><s:property value="hotelInvObj.basePrice" /></td>
												</tr>
												<tr>
													<td>SR</td>
													<td>Self Markup</td>
													<td>1</td>
													<td><s:property value="hotelInvObj.markup" /></td>
													<td><s:property value="hotelInvObj.markup" /></td>
												</tr>
												<s:if test="logedInCompany == 'superuser'">
												<c:if test="${hotelInvObj.distributorMarkup!=null}">
													<tr>
														<td>SR</td>
														<td>Wholeseller Markup</td>
														<td>1</td>
														<td><s:property value="hotelInvObj.distributorMarkup" /></td>
														<td><s:property value="hotelInvObj.distributorMarkup" /></td>
													</tr>
												</c:if>	
												</s:if>
												<s:if test="logedInCompany != 'agent'">
												<c:if test="${hotelInvObj.childMarkup!=null}">
													<tr>
														<td>SR</td>
														<td>Agent Markup</td>
														<td>1</td>
														<td><s:property value="hotelInvObj.childMarkup" /></td>
														<td><s:property value="hotelInvObj.childMarkup" /></td>
													</tr>
												</c:if>	
												</s:if>
												<tr>
													<td>SR</td>
													<td>Total After Markup</td>
													<td>1</td>
													<td><s:property value="hotelInvObj.totAmount" /></td>
													<td><s:property value="hotelInvObj.totAmount" /></td>
												</tr>
												
												<tr>
													<td>SR</td>
													<td>Tax<span> </span>
													</td>
													<td>1</td>
													<td><s:property value="hotelInvObj.tax" /></td>
													<td><s:property value="hotelInvObj.tax" /></td>
												</tr>
												<tr>
													<td><s:text name="tgi.label.sr" /></td>
													<td><s:text name="tgi.label.processing_fees" /> <span>
													</span></td>
													<td>1</td>
													<td><s:property value="hotelInvObj.processingFees" /></td>
													<td><s:property value="hotelInvObj.processingFees" /></td>

												</tr>
												<tr>
													<td><s:text name="tgi.label.sr" /></td>
													<td><s:text name="tgi.label.insurance_fee" /> <span>
													</span></td>
													<td>1</td>
													<td>
													<c:choose>
													<c:when test="${hotelInvObj.hotelOrderRow.hotelOrderInsurance.price != null}">
													${hotelInvObj.hotelOrderRow.hotelOrderInsurance.price}
													</c:when>
													<c:otherwise>
													<s:text name="tgi.label.n_a" />
													</c:otherwise>
													</c:choose>
													</td>
													<td>
													<c:choose>
													<c:when test="${hotelInvObj.hotelOrderRow.hotelOrderInsurance.price != null}">
													${hotelInvObj.hotelOrderRow.hotelOrderInsurance.price}
													</c:when>
													<c:otherwise>
													<s:text name="tgi.label.n_a" />
													</c:otherwise>
													</c:choose>
													</td>

												</tr>
												<tr>
													<th></th>
													<th></th>
													<th><s:text name="tgi.label.calculation" /></th>
													<th></th>
													<th></th>
												</tr>

												<tr>
													<td>
														<!-- 6% -->
													</td>
													<td>
														<!-- SR -->
													</td>
													<td>
														<%-- <s:property  value="invoiceData.totGst"/> --%>
													</td>
													<td><s:text name="tgi.label.booking_amount" /></td>
													<td colspan="4">${hotelInvObj.price}<br> <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         	 <s:text name="tgi.label.totamount" /> <s:property  value="invoiceData.totWithGst"/> --%>
													</td>

												</tr>

												<tr>
													<td>
														<!-- 6% -->
													</td>
													<td>
														<!-- SR -->
													</td>
													<td>
														<%-- <s:property  value="invoiceData.totGst"/> --%>
													</td>
													<td><s:text name="tgi.label.discount_amount" /></td>
													<td>${hotelInvObj.discountAmount}<br> <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         	 <s:text name="tgi.label.totamount" />  <s:property  value="invoiceData.totWithGst"/> --%>
													</td>

												</tr>

												<tr>
													<td>
														<!-- 6% -->
													</td>
													<td>
														<!-- SR -->
													</td>
													<td>
														<%-- <s:property  value="invoiceData.totGst"/> --%>
													</td>
													<td><s:text name="tgi.label.total_paid" /></td>
													<td colspan="4">${hotelInvObj.totPrice}<br> <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         <s:text name="tgi.label.totamount"/>   <s:property  value="invoiceData.totWithGst"/> --%>
													</td>

												</tr>

												<tr>
													<th></th>
													<th>Commision Details<%-- <s:text name="tgi.label.commision_details" /> --%></th>
													<th></th>
													<th></th>
													<th></th>
												</tr>

												<c:if test="${hotelInvObj.myCommission!=null}">
													<tr>
														<td>SR</td>
														<td>My Commision</td>
														<td>1</td>
														<td><s:property value="hotelInvObj.myCommission" /></td>
														<td><s:property value="hotelInvObj.myCommission" /></td>
													</tr>
												</c:if>
												<s:if test="logedInCompany != 'agent'">
													<c:if test="${hotelInvObj.sharedCommission!=null}">
														<tr>
															<td>SR</td>
															<td>Shared Commision</td>
															<td>1</td>
															<td><s:property value="hotelInvObj.sharedCommission" /></td>
															<td><s:property value="hotelInvObj.sharedCommission" /></td>
														</tr>
													</c:if>
												</s:if>
												<tr>
													<td>SR</td>
													<td>My Revenue</td>
													<td>1</td>
													<td><s:property value="hotelInvObj.myProfit" /></td>
													<td><s:property value="hotelInvObj.myProfit" /></td>
												</tr>

												<tr>
														<td>SR</td>
														<td>Total Price Afetr My Revenue</td>
														<td>1</td>
														<td><s:property value="hotelInvObj.totalPriceAfterMyRevenue" /></td>
														<td><s:property value="hotelInvObj.totalPriceAfterMyRevenue" /></td>
													</tr> 
												
											</tbody>
										</table>
									</div>
									<br>
									<div class="clearfix">
										<div class="payment-in">
											<div class="panel panel-info">
												<!--   <div class="panel-heading">
    <h4>Payment details</h4>
  </div> -->

												<h4 class="pl-2">
													<s:text name="tgi.label.payment_details" />
												</h4>

												<!-- <div class="panel-body"> -->
												<table
													class="table table-bordered in-table-border no-table-css">
													<tr class="info">
														<th><s:text name="tgi.label.receipt" /></th>
														<th><s:text name="tgi.label.date" /></th>
														<th><s:text name="tgi.label.remark" /></th>
														<th><s:text name="tgi.label.payment_type" /></th>
														<th><s:text name="tgi.label.payable_amount" /></th>
															<th><s:text name="tgi.label.paid_amount" /></th>
														<!-- 
<th>
AgentComm
</th> -->
													</tr>

													<s:iterator value="hotelInvObj.txDetails">
														<tr>
															<td data-title="<s:text name="tgi.label.receipt" />"><s:property
																	value="transactionId" /></td>

															<td data-title="Date"><s:property
																	value="convertDate" /></td>

															<td data-title="Remark"><s:property
																	value="responseMessage" /></td>
															<td data-title="Payment Type"><s:property
																	value="paymentMethod" /></td>
															<td data-title="<s:text name="tgi.label.payable_amount" />"><s:property
																	value="amount" />
															<td data-title="<s:text name="tgi.label.paid_amount" />"><s:property
																	value="amount" />
														</tr>
													</s:iterator>

													<%-- <s:if test="agentWalletTxHistory.size()>0"> --%>
													<%-- <s:iterator value="agentWalletTxHistory">
											<tr>
												<td data-title="<s:text name="tgi.label.receipt" />"><s:text name="tgi.label.done" /></td>

												<td data-title="<s:text name="tgi.label.date" />"><s:property value="key.convertDate" /></td>

												<td data-title="<s:text name="tgi.label.remark" />"><s:property value="responseMessage" /></td>
												<td data-title="<s:text name="tgi.label.payment_type" />"><s:property value="paymentMethod" /></td>
												<td data-title="<s:text name="tgi.label.amount" />"><s:property value="key.amount" /></td>
												<td><s:property  value="%{#session.totalAgentCommission}"/></td>
											</tr>
										</s:iterator> --%>
													<%-- </s:if> --%>
													<%-- <s:else>
										<tr>
											<td colspan="5"><s:text
													name="tgi.label.payment_details_not_available" /></td>
										</tr>
									</s:else>
 --%>
												</table>
												<br>
											</div>
											<div class="panel panel-info">
												<h4 class="pl-2">
													<s:text name="tgi.label.wallet_details" />
												</h4>
												<table
													class="table table-bordered in-table-border no-table-css">
													<tr class="info">
														<th><s:text name="tgi.label.created_at" /></th>
														<th><s:text name="tgi.label.action" /></th>
														<th><s:text name="tgi.label.amount" /></th>
														<th><s:text name="tgi.label.openingbal" /></th>
														<th><s:text name="tgi.label.closingbal" /></th>
														<!-- <th>
Currency
</th> -->
													</tr>

													<s:if test="agentWalletTxHistory.size()">
														<s:iterator value="agentWalletTxHistory">
															<tr>
																<td data-title="<s:text name="tgi.label.created_at" />"><s:property
																		value="value.convertDate" /></td>

																<td data-title="<s:text name="tgi.label.action" />"><s:property
																		value="value.action" /></td>

																<td data-title="<s:text name="tgi.label.amount" />"><s:property
																		value="value.amount" /></td>
																<td data-title="<s:text name="tgi.label.openingbal" />"><s:property
																		value="value.openingBalance" /></td>
																<td data-title="<s:text name="tgi.label.closingbal" />"><s:property
																		value="value.closingBalance" /></td>
																<%-- <td data-title=""><s:property  value="value.currency"/></td> --%>
															</tr>
														</s:iterator>
													</s:if>
													<s:else>
														<tr>
															<td colspan="5"><s:text
																	name="tgi.label.wallet_details_not_available" /></td>
														</tr>
													</s:else>
												</table>
											</div>
										</div>
									</div>

									<div class="clearfix signature-lint no-table-css">

										<div class="col-sm-6">
											<h4>
												<s:text name="tgi.label.recived_by" />
											</h4>
											<br> <br>
											<p>
												<span style="border-top: 1px solid #adadad; padding: 5px;"><s:text
														name="tgi.label.customers_signature" /> &amp; <s:text
														name="tgi.label.chop" /></span>
											</p>
										</div>

										<div class="col-sm-6 pull-right">
											<div class="pull-right">
												<h4>TGI</h4>
												<br> <br>
												<p>
													<span style="border-top: 1px solid #adadad; padding: 5px;"><s:text
															name="tgi.label.autorised_signature" /></span>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="editor"></div>
						</div>
					</div>
					<!-- /.row -->
					<!-- Main row -->
				</section>
<!-- table-responsive -->
<!-- Main row -->



<!-- /.content -->
	<script type="text/javascript">
	function sendCustomerInvoiceToCustomer() {
		var companyType = $("#companyType").val();
		console.log("companyType..." + companyType);
		var orderId = $("#orderId").val();
		console.log("orderId..." + orderId);
		//var invoice = "<html><body style='border:2px solid;padding:10px 10px 10px 10px'>"+$("#invoice").html()+"</body></html>";
		/*   var htmlMessage=$('#invoice').html(); */
		//console.log("--htmlMessage..."+invoice);
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "sendHotelInvoiceToMail";
		//console.log("finalUrl..."+finalUrl);
		$('#h4').show();
		$.ajax({
			method : "POST",
			url : finalUrl,
			data : {
				orderId : $("#orderId").val(),
				companyType : $("#companyType").val()
			},
			success : function(data, status) {
				$.each(data, function(index, element) {
					console.log("data-------" + element.status);

					if (element.status == "success") {
						$('#h4').hide();
						$('#success-alert').show();
						$('#message').text("Successfully sent mail.");
						$('#success').click(function() {
							$('#success-alert').hide();
							window.location.assign($(location).attr('href'));
						});

					} else if (element.status == "fail") {
						$('#h4').hide();
						$('#success-alert').show();
						$('#message').text("Failed.Try again.");
						$('#success').click(function() {
							$('#success-alert').hide();

						});
					}

				});

			},
			error : function(xhr, status, error) {
				$('#h4').hide();
				$('#success-alert').show();
				$('#message').text(error);
				$('#success').click(function() {
					$('#success-alert').hide();
				});
				console.log("Error----------" + error);
			}
		});
	}
</script>
