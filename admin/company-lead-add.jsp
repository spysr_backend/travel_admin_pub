<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  <link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="admin/js/admin/panel-app.js"></script>
      <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid"> 

                <div class="row" style="margin-left: 0px;margin-right: 0px; margin-top: 50px;">
                <form class="from-horizontal" id="save_company_lead_form" enctype="multipart/form-data" method="post">
                <div class="col-md-12">
                <nav aria-label="breadcrumb">
				  <div class="own-bradcum">
				  			<div class="pull-right mt-2">
							  <a class="btn btn-sm btn-outline-primary" href="listCompanyLead"><i class="fa fa-bars" aria-hidden="true"></i> Lead List</a>
							</div>
				    <h4 class=""> Company Lead Profile </h4>    
				  </div>
				</nav>
				</div>
					<div class="col-md-12">
					<div class="container-detached">
						<div class="content-detached">

									<!-- Profile info -->
									<div class="panel panel-flat">
										<div class="panel-heading">
											<h6 class="panel-title">Profile information<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
							                	</ul>
						                	</div>
										</div>
										<hr>
										<div class="panel-body" style="display: block;">
													<div class="row">
														<div class="col-md-4 form-group">
															<label class="input-sm-label" for="firstName">First Name</label>
															<input type="text" name="firstName" value="" class="form-control">
														</div>
														<div class="col-md-4 form-group">
															<label>Last Name</label>
															<input type="text" name="lastName" value="" class="form-control">
														</div>
														<div class="col-md-4 form-group">
																<label>Email</label>
																<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
																<input type="email" name="email" value="" class="form-control">
																</div>
															</div>
															
													</div>
													<div class="row">
														<div class="col-md-4 form-group">
																<label>Phone #</label>
																<input type="text" name="phone" value="" class="form-control">
															</div>
														<div class="col-md-4 form-group">
															<label>Company</label>
															<input type="text" name="companyName" value="" class="form-control">
														</div>
														<div class="col-md-4 form-group">
																<label>Website</label>
																<input type="text" name="website" value="https://www." class="form-control">
															</div>
														<div class="col-md-12 form-group">
																<label>Description</label>
																<textarea name="companyLeadDetailData.description" class="form-control mb-15" rows="2" cols="1" placeholder="What's on your mind?"></textarea>
															</div>
															
													</div>
														<div class="row">
														<div class="col-md-4 form-group">
															<label>Address</label>
															<textarea name="companyLeadDetailData.address" rows="1" class="form-control"></textarea>
														</div>
														<div class="col-md-2 form-group">
															<label>City</label>
															<input type="text" name="companyLeadDetailData.city" value="" class="form-control">
														</div>
														<div class="col-md-2 form-group">
															<label>Zip/Postel Code</label>
															<input type="text" name="companyLeadDetailData.zip_code" value="" class="form-control">
														</div>
														<div class="col-md-2 form-group">
															<label>State</label>
															<select class="form-control" name="billingState" id="billing-state" >
																<option selected  value="">Select State</option>
																<c:forEach items="${statesList}" var="states">
																	<option value="${states.state}">${states.state}</option>
																</c:forEach>
															</select>
														</div>
														<div class="col-md-2 form-group">
															<label>Country</label>
															<select name="companyLeadDetailData.country" id="country" class="form-control usst" data-live-search="true">
															    <option value="">Select Country</option>
																		<c:forEach items="${countryList}" var="country">
																			<option value="${country.countryName}" ${country.countryName == 'India' ? 'selected' : ''} >${country.countryName}</option>
																</c:forEach>
															</select> 
														</div>
														</div>
														<br>
										</div>
									</div>
									<!-- /profile info -->

									<!-- Account settings -->
									<div class="panel panel-flat">
										<div class="panel-heading">
											<h6 class="panel-title">Company Detail<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
							                	</ul>
						                	</div>
										</div>
										<hr>
										<div class="panel-body" style="display: block;">
												<div class="form-group">
													<div class="row">
														<div class="col-md-2 col-sm-6 col-xs-6">
															<div class="checkbox checkbox-default">
																<input name="companyLeadDetailData.tour" id="tour" type="checkbox" value="false">
																<label for="tour">&nbsp;&nbsp;Tour </label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="companyLeadDetailData.flight" id="flight" type="checkbox" value="false" >
																<label for="flight">&nbsp;&nbsp;Flight</label>
															</div>
														</div>
														<div class="col-md-2 col-sm-6 col-xs-6">
															<div class="checkbox checkbox-default">
																<input name="companyLeadDetailData.hotel" id="hotel" type="checkbox" value="false">
																<label for="hotel">&nbsp;&nbsp;Hotel</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="companyLeadDetailData.car" id="car" type="checkbox" value="false" >
																<label for="car">&nbsp;&nbsp;Car</label>
															</div>
														</div>
														<div class="col-md-2 col-sm-6 col-xs-6">
															<div class="checkbox checkbox-default">
																<input name="companyLeadDetailData.bus" id="bus" type="checkbox" value="false" >
																<label for="bus">&nbsp;&nbsp;Bus</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="companyLeadDetailData.train" id="train" type="checkbox" value="false">
																<label for="train">&nbsp;&nbsp;Train</label>
															</div>
														</div>
														<div class="col-md-2 col-sm-6 col-xs-6">
															<div class="checkbox checkbox-default">
																<input name="companyLeadDetailData.visa" id="visa" type="checkbox" value="false">
																<label for="visa">&nbsp;&nbsp;Visa</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="companyLeadDetailData.travelInsurance" id="travelInsurance" type="checkbox" value="false">
																<label for="travelInsurance">&nbsp;&nbsp;Travel Insurance</label>
															</div>
														</div>
														<div class="col-md-2 col-sm-6 col-xs-6">
															<div class="checkbox checkbox-default">
																<input name="companyLeadDetailData.cruise" id="cruise" type="checkbox" value="false">
																<label for="cruise">&nbsp;&nbsp;Cruise</label>
															</div>
														</div>
													</div>
													<div class="row">
													<div class="col-md-6">
														<label class="form-control-label" for="appendedInput">Comapny Memberships </label>
														<div class="controls">
														<div class="form-group"><div class="">
														<select class="input-md" id="company-mambership" name="companyLeadDetailData.companyMambership" multiple="multiple" >
															<option value="IATA">International Air Transport Association</option>
															<option value="IATO">Indian Association of Tour Operators</option>
															<option value="DTGI">Dept. of Tourism, Government of India</option>
															<option value="ASTA">American Society of Travel Agents</option>
															<option value="DST">Dept. of State Tourism</option>
															<option value="PATA">Pacific Asia Travel Association</option>
															<option value="TAAI">Travel Agents Association of India </option>
															<option value="JATA">Japan Association of Travel Agents</option>
															<option value="TAFI"> Travel Agents Federation of India</option>
															<option value="Other"> Others</option>
														</select> 
												</div></div>
										</div>
														</div>
													</div>
													
												</div>
												<!-- <div class="form-group">
													<div class="row">
														<div class="col-md-3">
															<label>Company Memberships</label>
															
															<div class="checkbox checkbox-default">
																<input name="tour" id="tour" type="checkbox" value="false">
																<label for="tour">&nbsp;&nbsp;International Air Transport Association (IATA) </label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="flight" id="flight" type="checkbox" value="false" >
																<label for="flight">&nbsp;&nbsp;Dept. of Tourism, Government of India
</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="hotel" id="hotel" type="checkbox" value="false">
																<label for="hotel">&nbsp;&nbsp;Dept. of State Tourism</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="car" id="car" type="checkbox" value="false" >
																<label for="car">&nbsp;&nbsp;Travel Agents Association of India (TAAI)</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="bus" id="bus" type="checkbox" value="false" >
																<label for="bus">&nbsp;&nbsp;Travel Agents Federation of India (TAFI)</label>
															</div>
															</div>

														<div class="col-md-6">
															<label>&nbsp;</label>

															<div class="checkbox checkbox-default">
																<input name="train" id="train" type="checkbox" value="false">
																<label for="train">&nbsp;&nbsp;Indian Association of Tour Operators (IATO)</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="cruise" id="cruise" type="checkbox" value="false">
																<label for="cruise">&nbsp;&nbsp;American Society of Travel Agents (ASTA)</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="visa" id="visa" type="checkbox" value="false">
																<label for="visa">&nbsp;&nbsp;Pacific Asia Travel Association (PATA)
</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="travelInsurance" id="travelInsurance" type="checkbox" value="false">
																<label for="travelInsurance">&nbsp;&nbsp;Japan Association of Travel Agents (JATA)</label>
															</div>
															<div class="checkbox checkbox-default">
																<input name="others" id="others" type="checkbox" value="false">
																<label for="others">&nbsp;&nbsp;Other (Please Specify)</label>
															</div>

														</div>
													</div>
												</div> -->
										</div>
									</div>
									<!-- /account settings -->
									<!-- Tab content -->
								<div class="tab-content">
								<div class="tab-pane fade active in" id="profile">


									<!-- Share your thoughts -->
									<div class="panel panel-flat">
										<div class="panel-heading">
											<h6 class="panel-title">Company Documents <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
							                	</ul>
						                	</div>
										</div>
										<hr>
										<div class="panel-body">
										<div class="row">
										<div class="col-md-3">
										<label class="form-control-label" for="appendedInput">Company Logo</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input name="uploadFile" type="file" class="form-control"></div></div>
											</div>
										</div>
										</div>
										<!-- <div class="col-md-3">
										<label class="form-control-label" for="appendedInput">Company Document1</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input name="uploadDocument1File" type="file" class="form-control"></div></div>
											</div>
										</div>
										</div>
										<div class="col-md-3">
										<label class="form-control-label" for="appendedInput">Company Document2</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input name="uploadDocument2File" type="file" class="form-control"></div></div>
											</div>
										</div>
										</div> -->
										</div>
				                    	</div>
									</div>
									<!-- /share your thoughts -->
								</div>
							</div>
							<!-- /tab content -->
						</div>
					</div>
				</div>
				<div class="col-md-12" style="margin-bottom:20px;">
				<div class="text-right">
                     <button type="submit" class="btn btn-primary save-company-lead-btn" id="">Save Changes <b><i class="icon-circle-right2"></i></b></button>
            	</div>
				</div>
</form>
				</div>
			</section>
        <!--ADMIN AREA ENDS-->
  <script>
  $(document).ready(function() {
	  $('#save_company_lead_form').bootstrapValidator(
 		{ // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
 			feedbackIcons : {
 				valid : 'fa fa-check',
 				invalid : 'fa fa-remove',
 				validating : 'fa fa-refresh'
 			},
 			fields :{firstName :{message :'Name is not valid',validators :{notEmpty :{message:'First Name is required and cannot be empty'},
 				    stringLength :{min:1,message:'First Name must be more than 3 and less than 100 characters long'}}},
 				    lastName :{message :'Name is not valid',validators :{notEmpty :{message:'Last Name is required and cannot be empty'},
 					stringLength :{min:1,message:'Last Name must be more than 3 and less than 100 characters long'}}},
 					phone : {message : 'Phone  is not valid',validators : {notEmpty : {message : 'Please enter a Phone'},integer: {message: 'The value is not a Phone Number'},
					 stringLength : {
							min : 4,
							max : 12,
							message : 'Phone must be more than 4 and less than 12 characters long'
					}  
 						}
 					},
 				    email:{validators:{notEmpty :{message:'Email is required and cannot be empty'},emailAddress:{message:'The value is not a valid email address'}}},}
 		}).on('error.form.bv', function(e) {
	  	// do something if you want to check error 
	    }).on('success.form.bv', function(e) {
	  	notifySuccess();
	  	e.preventDefault();	
	    var form = document.forms[0];
	    var formData = new FormData(form);
	  		$.ajax({
	  			type: "POST",
	  			url : 'saveCompanyLeadProfile',
	  			data : formData,
	  			cache: false,
                processData: false,
	  			contentType: false,
	  			dataType: 'json',
	  			success : function(jsonData) {
	  				if(jsonData.message.status == 'success'){
	  					showModalPopUp(jsonData.message.message,"s");
	  					var url = '<%=request.getContextPath()%>/listCompanyLead';
						window.location.replace(url);
	  				}
	  				else if(jsonData.message.status == 'input'){
	  					showModalPopUp(jsonData.message.message,"w");}
	  				else if(jsonData.message.status == 'error'){
	  					showModalPopUp(jsonData.message.message,"e");}
	  				
	  				$(".save-company-lead-btn").removeAttr("disabled");
	  				//-----------------------------------------------
	  				// required for validator plugin
	  				//-----------------------------------------------
	  				 console.debug("button disabled ");
	  				 var $form     = $(e.target),
	                 validator = $form.data('bootstrapValidator');
	  				 $form
	                  .bootstrapValidator('disableSubmitButtons', false)  // Enable the submit buttons
	                  .bootstrapValidator('resetForm', true);             // Reset the form
	  	},
	  	error: function (request, status, error) {
	  		showModalPopUp("Company Lead can not be Saved.","e");
	      }
	  		});

	  }).on('status.field.bv', function(e, data) {
	  	if (data.bv.getSubmitButton()) {
	  		console.debug("button disabled ");
	  		data.bv.disableSubmitButtons(false);
	  	}
	  });
	  });
  /* -----------------------------------------------------*/
  $(document).ready(function() {
		$("#company-mambership").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a Company CMS",
		});
		
		var arrayuserroleType = '${company-mambership}'.split(',');
		console.debug(arrayuserroleType);
		$("#company-mambership").multipleSelect("setSelects", arrayuserroleType);
	
	}); 

  </script>