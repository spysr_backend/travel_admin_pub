<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="admin/js/jquery.min.js"></script>
<script type="text/javascript" src="admin/js/jspdf.min.js"></script>
<script type="text/javascript" src="admin/js/html2canvas.min.js"></script>
<%-- <script src="admin/js/jspdf.debug.js"></script> --%>
<script type="text/javascript">
	var form;
	var totalwidth;
	function clickpdf() {
		totalwidth = $('#invoice').width();

		form = $('#form'), cache_width = form.width(), a4 = [ 595.28, 841.89 ]; // for a4 size paper width and height
		$('body').scrollTop(0);
		createPDF();
	}

	//create pdf
	function createPDF() {
		getCanvas().then(function(canvas) {
			var img = canvas.toDataURL("image/png"),

			doc = new jsPDF({
				unit : 'px',
				format : 'a4'
			});
			doc.addImage(img, 'JPEG', 20, 20);

			doc.save('FlightInvoice.pdf');
			form.width(totalwidth).css('width', 'none');
		});
	}

	// create canvas object
	function getCanvas() {
		form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');
		return html2canvas(form, {
			imageTimeout : 2000,
		//removeContainer:true
		});
	}

	(function() {
		console.log('hi');
		var totalwidth = $('#invoice').width()
		var form = $('#form'), cache_width = form.width(), a4 = [ 595.28,
				841.89 ]; // for a4 size paper width and height

		// $('#createpdf').on('click',function(){

	}());
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
	<section class="wrapper container-fluid">
		<div class="row">
			<div class="">
			<div class="card1">
				<div class="pnl">
					<div class="hd clearfix">
						<h5 class="pull-left">
							<s:text name="tgi.label.flight_agent_invoice" />
						</h5>
						<div class="set pull-right">

							<button class=" btn btn-success btn-xs"
								onclick="sendCustomerInvoiceToCustomer();">
								<s:text name="tgi.label.send_invoice" />
								<span class="glyphicon glyphicon-send"></span>
							</button>
							<button type="button" class="btn btn-success btn-xs" id="h4"
								style="display: none">
								<s:text name="tgi.label.sending" />
								<i class="fa fa-arrow-circle-right"></i>
							</button>
						</div>
						<div class="set pull-right">

							<a class="btn btn btn-primary btn-xs"
								href="flightAgentInvoiceList"><span class="pull-right">
									<span class="glyphicon glyphicon-step-backward"></span> <s:text
										name="tgi.label.back" />
							</span></a>

						</div>
						
						<div class="set pull-right">
						<a href="<%=request.getContextPath()%>/flightAgentInvoice?orderId=${orderId}" target="blank" class="popup btn btn-info btn-xs">
						<span class="pull-right">
							<i class="fa fa-print" aria-hidden="true"></i> Print
						</span></a>
					</div> 
					</div>
					<div class="col-md-12 mt-2"> 
				<div class="profile-timeline-card">   
								<i class="fa fa-user header-icons timeline-img-user pull-left" style="box-shadow: none;"></i> 
						<div class="">
							<p class="mt-2 mb-4">
								<span> <b class="blue-grey-text">${invObj.company.companyName}</b></span>
							</p> 
						</div>
						<hr>  
						<div class="timeline-footer row row-minus"> 
<div class="col-md-4"> 
<h4 class="text-left"><span>From</span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b>Address</b></td>
            <td> ${invObj.company.address}</td>
          </tr>
          <tr>
            <td><b>Tel</b></td>
            <td>${invObj.company.phone}</td>
          </tr> 
          <tr>
            <td><b>Email</b></td>
            <td>${invObj.company.email}</td>
          </tr> 
          <tr>
            <td><b>Website</b></td>
            <td>${invObj.company.website}</td>
          </tr>
          </tbody>
        </table>
     </div>   
     <s:if test="%{invObj.userDetails.size()>0}">
	 <s:iterator value="invObj.userDetails">
	 <c:if test="${invObj.companyTo!=null}">
     <div class="col-md-4">
     <h4 class="text-left"><span><s:text name="tgi.label.to" /></span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b><s:text name="tgi.label.to" /></b></td>
            <td>${invObj.companyTo.companyName}</td>
          </tr>
          <tr>
            <td><b><s:text name="tgi.label.address" /></b></td>
            <td>${invObj.companyTo.address}</td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.tel" /></b></td>
            <td>${invObj.companyTo.phone}</td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.email" /></b></td>
            <td>${invObj.companyTo.email}</td>
          </tr>
          </tbody>
        </table>
     </div>  
     </c:if>
     </s:iterator>
     </s:if> 
     <div class="col-md-4">
     <h4 class="text-left"><span><s:text name="tgi.label.tax_invoice" /></span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b><s:text name="tgi.label.invoice_no" /></b></td>
            <td> <s:property  value="invObj.invNo"/></td>
          </tr>
          <tr>
            <td><b><s:text name="tgi.label.consultant" /></b></td>
            <td><s:property  value="invObj.consultant"/></td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.book_no" /></b></td>
            <td><s:property  value="invObj.bookNo"/></td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.date" /></b></td>
            <td><s:property  value="invObj.bookedDate"/></td>
          </tr>
          </tbody>
        </table>
     </div>
						</div>
					</div>
					</div> 
					<!-- Main content -->
					<!-- Main content --> 
				</div>
			</div>
		</div>
		</div>
		<!-- table-responsive -->
	</section>
	
	<section class="col-md-12">


						<div class="sccuss-full-updated" id="success-alert"
							style="display: none">
							<div class="succfully-updated clearfix">

								<div class="col-sm-2">
									<i class="fa fa-check fa-3x"></i>
								</div>

								<div id="message" class="col-sm-10"></div>
								<button type="button" id="success" class="btn btn-primary">
									<s:text name="tgi.label.ok" />
								</button>
							</div>

						</div>

						<!-- Small boxes (Stat box) -->
						<div class="profile-timeline-card">
						<div class="">
								<div id="invoice" class="clearfix mt-0">
									<%-- <div class="row">
										<div class="col-xs-4">
											<div class="text-center pro-img">

												<img src="<s:url action='getImageByPath?imageId=%{invObj.company.imagePath}'/>"
													class="avatar img-circle img-thumbnail img-responsive"
													alt="profile image">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="lint-invoice-adde text-right">
												<h2>${invObj.company.companyName}</h2>
												<p>
													<br>${invObj.company.address} <br> Tel: ${invObj.company.phone}<br>
													<b>Email:</b> ${invObj.company.email}<b> <br>Website:
													</b> ${invObj.company.website}
												</p>
											</div>
										</div> 
									</div> --%>

									<div class="row">
										<%-- <div class="in-head">
											<h4 class="text-center">
												<span><s:text name="tgi.label.tax_invoice" /></span>
											</h4>
										</div>

										<div class="invoice-addres clearfix">
											<div class="col-md-5">
												<div class="panel ">
													<s:if test="%{invObj.userDetails.size()>0}">
														<s:iterator value="invObj.userDetails">
															<c:if test="${invObj.companyTo!=null}">
															<div class="panel-body">
																<h4>
																	<s:text name="tgi.label.to" />
																	:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	${invObj.companyTo.companyName}
																</h4>
																<p>
																	<b><s:text name="tgi.label.address" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><a
																		href="#">${invObj.companyTo.address}</a>

																</p>
																<p>
																	<b><s:text name="tgi.label.tel" />
																		:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	${invObj.companyTo.phone}
																</p>
																<p>
																	<b> <s:text name="tgi.label.email" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	</b>
																	${invObj.companyTo.email}
																</p>

															</div>
															</c:if>
														</s:iterator>
													</s:if>
												</div>
											</div>

											<div class="col-md-6">

												<table class="table inn-num table-bordered">

													<tbody>
														<tr>
															<td><s:text name="tgi.label.invoice_no" /></td>
															<td><s:property value="invObj.invNo" /></td>


														</tr>
														<tr>
													<td><s:text name="tgi.label.acct_code" /></td>
													<td><a href="#"> <s:property
																value="invObj.ActCode" /></a></td>
												</tr>
														<tr>
															<td><s:text name="tgi.label.consultant" /></td>
															<td><s:property value="invObj.consultant" /></td>
														</tr>
														<tr>
															<td><s:text name="tgi.label.book_no" /></td>
															<td><s:property value="invObj.bookNo" /></td>
														</tr>

														<tr>
															<td><s:text name="tgi.label.your_ref" /></td>
															<td><s:property value="invObj.yourRef" /></td>
														</tr>
														<tr>
            <td><s:text name="tgi.label.page" /></td>
            <td><a href="#"><s:text name="tgi.label.page 1 of 1" /></a></td>
          </tr>
														<tr>
															<td><s:text name="tgi.label.date" /></td>
															<td><s:property value="invObj.bookedDate" /></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div> --%>
										<!-- / end client details section -->

										<div class="">
											<table
												class="table table-bordered in-table-border no-table-css">
												<thead>
													<tr class="info">
														<th><h4>
																<s:text name="tgi.label.gst_type" />
															</h4></th>
														<th><h4>
																<s:text name="tgi.label.pertycular" />
															</h4></th>
														<th><h4>
																<s:text name="tgi.label.qty" />
															</h4></th>
														<th><h4>
																<s:text name="tgi.label.price" />
															</h4></th>
														<th><h4>
																<s:text name="tgi.label.total" />
																(
																<s:property value="invObj.currency" />
																)
															</h4></th>
													</tr>
												</thead>
												<tbody>

													<s:iterator value="invObj.flightOrderRow.flightOrderCustomers" status="rowCount">

														<tr>
															<td>ZRE</td>
															<td>(<s:property value="%{#rowCount.count}" />) <s:property
																	value="firstName" />&nbsp;<s:property
																	value="lastName" /></td>
															<td>1</td>
															<td>0</td>
															<td>0</td>
														</tr>
													</s:iterator>
													<tr>
														<td>SR</td>
														<td><s:iterator value="invObj.tripParticulars">

																<s:property value="operatedByCode" /> &nbsp;&nbsp;/&nbsp;&nbsp;<s:property
																	value="flightNumber" />&nbsp;&nbsp;/
													&nbsp;&nbsp;<s:property value="originCode" />&nbsp;&nbsp; -&nbsp;&nbsp; <s:property
																	value="destinationCode" />&nbsp;&nbsp;
																<s:property value="convertDate" />&nbsp;&nbsp;
																<s:property value="departureTime" />&nbsp;&nbsp;
																<s:property value="arrivalTime" />
																<br>
															</s:iterator></td>
														<td>1</td>
														<td><s:property value="invObj.basePrice" /></td>
														<td><s:property value="invObj.basePrice" /></td>
													</tr>
													<tr>
														<td>SR</td>
														<td>Total Before Markup</td>
														<td>1</td>
														<td><s:property value="invObj.basePrice" /></td>
														<td><s:property value="invObj.basePrice" /></td>
													</tr>
													<tr>
														<td>SR</td>
														<td>Self Markup</td>
														<td>1</td>
														<td><s:property value="invObj.markup" /></td>
														<td><s:property value="invObj.markup" /></td>
													</tr>
													<s:if test="logedInCompany == 'superuser'">
													<c:if test="${invObj.distributorMarkup!=null}">
													<tr>
														<td>SR</td>
														<td>Wholeseller Markup</td>
														<td>1</td>
														<td><s:property value="invObj.distributorMarkup" /></td>
														<td><s:property value="invObj.distributorMarkup" /></td>
													</tr>
													</c:if>
													</s:if>
													<s:if test="logedInCompany != 'agent'">
													<c:if test="${invObj.childMarkup!=null}">
													<tr>
														<td>SR</td>
														<td>Agent Markup</td>
														<td>1</td>
														<td><s:property value="invObj.childMarkup" /></td>
														<td><s:property value="invObj.childMarkup" /></td>
													</tr>
													</c:if>
													</s:if>
													<tr>
														<td>SR</td>
														<td>Total After Markup</td>
														<td>1</td>
														<td><s:property value="invObj.totAmount" /></td>
														<td><s:property value="invObj.totAmount" /></td>
													</tr>
													<tr>
														<td>SR</td>
														<td>Tax<span> </span>
														</td>
														<td>1</td>
														<td><s:property value="invObj.tax" /></td>
														<td><s:property value="invObj.tax" /></td>
													</tr>
													<tr>
														<td><s:text name="tgi.label.sr" /></td>
														<td><s:text name="tgi.label.processing_fees" /> <span>
														</span></td>
														<td>1</td>
														<td><s:property value="invObj.processingFees" /></td>
														<td><s:property value="invObj.processingFees" /></td>

													</tr>
													 <tr>
											            <td><s:text name="tgi.label.sr" /></td>
											            <td>  <s:text name="tgi.label.insurance_fee" /> <span> </span>  </td>
											            <td> </td>
											               <td>
											              	<c:choose>
															<c:when test="${invObj.insurancePrice != null}">
															${invObj.insurancePrice}
															</c:when>
															<c:otherwise>
															<s:text name="tgi.label.n_a" />
															</c:otherwise>
															</c:choose>
											              </td>
											               <td>
											              	<c:choose>
															<c:when test="${invObj.insurancePrice != null}">
															${invObj.insurancePrice}
															</c:when>
															<c:otherwise>
															<s:text name="tgi.label.n_a" />
															</c:otherwise>
															</c:choose>
											              </td>
											              
											           </tr>
													<tr>
														<th></th>
														<th></th>
														<th><s:text name="tgi.label.calculation" /></th>
														<th></th>
														<th></th>
													</tr>

													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td><s:text name="tgi.label.booking_amount" /></td>
														<td colspan="4">${invObj.price}<br> <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         	 <s:text name="tgi.label.totamount" /> <s:property  value="invoiceData.totWithGst"/> --%>
														</td>
													</tr>

													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td><s:text name="tgi.label.discount_amount" /></td>
														<td>${invObj.discountAmount}<br> <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         	 <s:text name="tgi.label.totamount" />  <s:property  value="invoiceData.totWithGst"/> --%>
														</td>

													</tr>

													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td><s:text name="tgi.label.total_paid" /></td>
														<td colspan="4">${invObj.totPrice}<br> <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         <s:text name="tgi.label.totamount"/>   <s:property  value="invoiceData.totWithGst"/> --%>
														</td>
													</tr>
													
													<tr>
														<th></th>
														<th>Commision Details</th>
														<th></th>
														<th></th>
														<th></th>
													</tr>
													
													<c:if test="${invObj.myCommission!=null}">
													<tr>
														<td>SR</td>
														<td>My Commision</td>
														<td>1</td>
														<td><s:property value="invObj.myCommission" /></td>
														<td><s:property value="invObj.myCommission" /></td>
													</tr>
													</c:if>
													<s:if test="logedInCompany != 'agent'">
													<c:if test="${invObj.sharedCommission!=null}">
													<tr>
														<td>SR</td>
														<td>Shared Commision</td>
														<td>1</td>
														<td><s:property value="invObj.sharedCommission" /></td>
														<td><s:property value="invObj.sharedCommission" /></td>
													</tr>
													</c:if>
													</s:if>
													<tr>
														<td>SR</td>
														<td>My Revenue</td>
														<td>1</td>
														<td><s:property value="invObj.myProfit" /></td>
														<td><s:property value="invObj.myProfit" /></td>
													</tr> 
													<tr>
														<td>SR</td>
														<td>Total Price Afetr My Revenue</td>
														<td>1</td>
														<td><s:property value="invObj.totalPriceAfterMyRevenue" /></td>
														<td><s:property value="invObj.totalPriceAfterMyRevenue" /></td>
													</tr> 
													
												</tbody>
											</table>
										</div>

										<div class="clearfix">
											<div class="payment-in">
												<div class="panel panel-info">
													<!--   <div class="panel-heading">
    <h4>Payment details</h4>
  </div> -->

													<h4 class="pl-2">
														<s:text name="tgi.label.payment_details" />
													</h4>
													<!-- <div class="panel-body"> -->
													<table
														class="table table-bordered in-table-border no-table-css">
														<tr class="info">
															<th><s:text name="tgi.label.receipt" /></th>
															<th><s:text name="tgi.label.date" /></th>
															<th><s:text name="tgi.label.remark" /></th>
															<th><s:text name="tgi.label.payment_type" /></th>
															<th><s:text name="tgi.label.payable_amount" /></th>
															<th><s:text name="tgi.label.paid_amount" /></th>
															<!-- 
<th>
AgentComm
</th> -->
														</tr>
														<s:if test="invObj.txDetails.size()>0">
															<s:iterator value="invObj.txDetails">
																<tr>
																	<td data-title="<s:text name="tgi.label.receipt" />"><s:property value="apiTransactionId"/></td>
																	<td data-title="<s:text name="tgi.label.date" />"><s:property
																			value="convertDate" /></td>
																	<td data-title="<s:text name="tgi.label.remark" />"><s:property
																			value="responseMessage" /></td>
																	<td
																		data-title="<s:text name="tgi.label.payment_type" />"><s:property
																			value="paymentMethod" /></td>
																	<td
																		data-title="<s:text name="tgi.label.processing.fees" />"><s:property value="totalPaidAmount"/></td>
																	<td
																		data-title="<s:text name="tgi.label.processing.fees" />"><s:property value="totalPaidAmount"/></td>
																	<!-- <td><s:property  value="%{#session.agentCommInvoiceObj.totMarkup}"/></td>-->
																</tr>
															</s:iterator>

														</s:if>
														<s:else>
															<tr>
																<td colspan="6"><s:text
																		name="tgi.label.payment_details_not_available" /></td>
															</tr>
														</s:else>

													</table>
												</div>
												<div class="panel panel-info">
													<h4 class="pl-2">
														<s:text name="tgi.label.wallet_details" />
													</h4>
													<table
														class="table table-bordered in-table-border no-table-css">
														<tr class="info">
															<th><s:text name="tgi.label.created_at" /></th>
															<th><s:text name="tgi.label.action" /></th>
															<th><s:text name="tgi.label.amount" /></th>
															<th><s:text name="tgi.label.openingbal" /></th>
															<th><s:text name="tgi.label.closingbal" /></th>
															<th><s:text name="tgi.label.currency" /></th>

														</tr>
														<s:if test="invObj.agentWalletTxDetails.size()>0">
															<s:iterator value="invObj.agentWalletTxDetails">
																<tr>
																	<td data-title="<s:text name="tgi.label.created_at" />"><s:property
																			value="invObj.convertDate" /></td>
																	<td data-title="<s:text name="tgi.label.action" />"><s:property
																			value="invObj.action" /></td>
																	<td data-title="<s:text name="tgi.label.amount" />"><s:property
																			value="invObj.amount" /></td>
																	<td data-title="<s:text name="tgi.label.openingbal" />"><s:property
																			value="invObj.openingBalance" /></td>
																	<td data-title="<s:text name="tgi.label.closingbal" />"><s:property
																			value="invObj.closingBalance" /></td>
																	<td data-title="<s:text name="tgi.label.currency" />"><s:property
																			value="invObj.currency" /></td>
																</tr>
															</s:iterator>
														</s:if>
														<s:else>
															<tr>
																<td colspan="5"><s:text
																		name="tgi.label.wallet_details_not_available" /></td>
															</tr>


														</s:else>

													</table>

												</div>
											</div>
										</div>

										<div class="clearfix signature-lint no-table-css">

											<div class="col-sm-6">
												<h4>Recived By:</h4>
												<br> <br>
												<p>
													<span style="border-top: 1px solid #adadad; padding: 5px;">Customers
														Signature &amp; Chop</span>
												</p>
											</div>

											<div class="col-sm-6 pull-right">
												<div class="pull-right">
													<h4>TGI</h4>
													<br> <br>
													<p>
														<span style="border-top: 1px solid #adadad; padding: 5px;">Autorised
															Signature</span>
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="editor"></div>
							</div>
							<!-- /.row -->
							<!-- Main row -->
						</div>
					</section>

	<!-- Main row -->
	<!--Start Scripting  -->
	<script type="text/javascript">
		function sendCustomerInvoiceToCustomer() {
			var mail = $("#mail").val();
			console.log("mail..." + mail);
			var invoice = "<html><body style='border:2px solid;padding:10px 10px 10px 10px'>"
					+ $("#invoice").html() + "</body></html>";
			/*   var htmlMessage=$('#invoice').html(); */
			console.log("--htmlMessage..." + invoice);
			var totUrl = $(location).attr('href');
			var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
			var finalUrl = newUrl + "sendInvoiceToMail";
			$('#h4').show();
			$.ajax({
				method : "POST",
				url : finalUrl,
				data : {
					customerMail : mail,
					htmlMessage : invoice
				},
				success : function(data, status) {
					$.each(data, function(index, element) {
						console.log("data-------" + element.status);

						if (element.status == "success") {
							$('#h4').hide();
							$('#success-alert').show();
							$('#message').text("Successfully sent mail.");
							$('#success').click(
									function() {
										$('#success-alert').hide();
										window.location.assign($(location)
												.attr('href'));
									});

						} else if (element.status == "fail") {
							$('#h4').hide();
							$('#success-alert').show();
							$('#message').text("Failed.Try again.");
							$('#success').click(function() {
								$('#success-alert').hide();

							});
						}

					});

				},
				error : function(xhr, status, error) {
					$('#h4').hide();
					$('#success-alert').show();
					$('#message').text(error);
					$('#success').click(function() {
						$('#success-alert').hide();
					});
					console.log("Error----------" + error);
				}
			});

		}
	</script>
</body>
</html>
