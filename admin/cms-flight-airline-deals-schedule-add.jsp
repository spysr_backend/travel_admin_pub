<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addFlightAirlineDealsSchedule";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                    <div class="card1">
                        <div class="pnl">
                         <div class="hd clearfix">
                                <h5 class="pull-left">Add Flight Deals From </h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="metatagpage_list"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.meta_dat_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div> 
</div></div>
 <div class="container">
					<div class="row">
					<h4 class="text-center p-4 hidden-xs">Add Flight Deals From </h4> 
                   <div class="spy-form-horizontal mb-0"> 
					<div class="">
						<div class="">
						<form action="insertFlightAirlineDealsSchedule.action" method="post">
							
							<div class="row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Deal Page Number
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="dealPageNumber" placeholder="Deal Page Number" autocomplete="off"
											required> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Flight Number
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="flightNumber" placeholder="Flight Number" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div>
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Airline Code
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="airlineCode" placeholder="Airline Code" autocomplete="off"
											required> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Frequency
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="frequency" placeholder="Frequency" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div>
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Depart Date
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="departDate" id="departDate" placeholder="Depart Date" autocomplete="off"
											required></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Return Date
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="returnDate" id="returnDate" placeholder="Return Date" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div>
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Duration
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="duration" placeholder="Duration" autocomplete="off"
											required></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Equipment
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="equipment" placeholder="Equipment" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div> 
							
							
							<!-- <table class="table table-form">
								<thead>
									<tr>
										<th></th>
										<th>&nbsp;</th>


									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<div class="field-name">
												Deal Page Number
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="dealPageNumber" placeholder="Deal Page Number" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Flight Number
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="flightNumber" placeholder="Flight Number" autocomplete="off"
											required></td>
									</tr>
																		<tr>
										<td>
											<div class="field-name">
												Airline Code
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="airlineCode" placeholder="Airline Code" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Frequency
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="frequency" placeholder="Frequency" autocomplete="off"
											required></td>
									</tr>
																		<tr>
										<td>
											<div class="field-name">
												Depart Date
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="departDate" id="departDate" placeholder="Depart Date" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Return Date
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="returnDate" id="returnDate" placeholder="Return Date" autocomplete="off"
											required></td>
									</tr>
																		<tr>
										<td>
											<div class="field-name">
												Duration
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="duration" placeholder="Duration" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Equipment
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="equipment" placeholder="Equipment" autocomplete="off"
											required></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td><input type="submit" class="btn btn-info"></td>
										<td></td>
									</tr>
								</tbody>
							</table> -->

						</form>
					</div>
				</div>
</div>
</div></div>
</div></div>
                            </section>        
                                    			<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	 <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_body').text("${message}");
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
						

						<script>
 $(document).ready(function() 
 { 
	 $("#departDate").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 $("#returnDate").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 }); 
 </script>
						