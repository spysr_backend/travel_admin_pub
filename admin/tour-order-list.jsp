<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  
	
<style>
table.dataTable tbody th, table.dataTable tbody td {
    padding: 4px 4px;
}
</style>

   <!--************** MAIN ADMIN AREA ***********************-->
            <section class="wrapper container-fluid">
                        <div class="card1">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.tour_order_list" /></h5>
                                 <div class="set pull-right">
                                 <div class="dropdown">
								 <div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" style="padding: 3px 6px;"> <i class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="tourOrderList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.order_list" /> </a></li>
									<li class=""><a class="" href="tourCommissionReport"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_report" /></a></li>
									<li class=""><a class="" href="tourAgentCommInvoiceList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_invoice" /></a></li>
								</ul>
								</div>
								</div>
                                </div>
                                 <div class="set pull-right" style="font-size: 16px;">
									<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
									<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
								</div>		
                                 </div>
                                 
                                 <div class="row mt-4">
	                         		<form action="tourOrderList" class="filter-form" id="resetform">
									<div class="" id="filterDiv" style="display: none;">
									 <s:if test="%{#session.Company!=null}">
	         						<s:if test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
	                                 <div class="col-md-2">
	                                 <div class="form-group">
	                                <input type="hidden" id="HdncompanyTypeShowData" value="" />
	                                <select name="companyTypeShowData" id="companyTypeShowData" class="form-control input-sm">
	                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
	                                <option value="my"><s:text name="tgi.label.my_self" /></option>
	                                <s:if test="%{#session.Company!=null}">
	         						<s:if test="%{#session.Company.companyRole.isSuperUser()}">
	                                 <option value="distributor"><s:text name="tgi.label.distributor" /></option>
	                                 </s:if>
	                                </s:if>
	                                <option value="agency"><s:text name="tgi.label.agency" /></option>
	                                </select>
	                                </div></div>
	                                </s:if>
	                                </s:if>
									<div class="col-md-2 col-sm-6">
									<div class="form-group">
									<input type="text" name="orderId" id="orderid-json" placeholder="Search By Order Id...." class="form-control search-query input-sm" />
								   </div></div>
								   <div class="col-md-2 col-sm-6">
								   <div class="form-group">
									<input type="text" name="bookingDateRange" id="bookingStartDate" placeholder="Start Booking Date...." class="form-control input-sm" />
									 </div></div>
								   <div class="col-md-2 col-sm-6">
								   <div class="form-group">
									<input type="text" name="travelStartEndDateRange" id="travelStartDate" placeholder="Tour Start Date...." class="form-control input-sm" />
									 </div></div>
								   <div class="col-md-2 col-sm-6">
								   <div class="form-group">
									<input type="text" name="pnr" id="hotelName-json" placeholder="Tour PNR" class="form-control input-sm" />
								 </div></div>
								   <div class="col-md-2 col-sm-6">
								   <div class="form-group">
									<input type="text" name="title" id="hotelName-json"
										placeholder="Tour Title" class="form-control search-query input-sm" />
								 </div></div>
									<div class="col-md-2 col-sm-6">
									<div class="form-group">
									<input type="text" name="firstName" id="customerFirstName"
													placeholder="Passenger FirstName..."
													class="form-control search-query input-sm" />
									</div>
									</div>
									<div class="col-md-2 col-sm-6">
									<div class="form-group">
									<input type="text" name="lastName" id="customerLastName"
													placeholder="Passenger LastName..."
													class="form-control search-query input-sm" />
									</div>
									</div>
									<div class="col-md-2 col-sm-6">
									<div class="form-group">
									<input type="text" name="email" id="customerEmail"
													placeholder="Passenger Email"
													class="form-control search-query input-sm" />
									</div>
									</div>
									<div class="col-md-2 col-sm-6">
									<div class="form-group">
									<input type="text" name="mobile" id="customerMobile"
													placeholder="Passenger Mobile"
													class="form-control search-query input-sm" />
									 </div></div>
								   <div class="col-md-2 col-sm-6">
								   <div class="form-group">
									<input type="text" name="supplier" id="hotelName-json"
										placeholder="Tour Supplier" class="form-control search-query input-sm" />
									 </div></div>
								   <div class="col-md-2 col-sm-6">
								   <div class="form-group">
									<input type="text" name="createdBy" id="agency-json"
										placeholder="Agency..." class="form-control search-query input-sm" />
									 </div></div>
								   <div class="col-md-2 col-sm-6">
								   <div class="form-group">
									<select name="paymentMode" id="paymentMode" class="form-control input-sm">
									<option value="" selected="selected"><s:text name="tgi.label.select_payment_method" /></option>
	                                <option value="ALL"><s:text name="tgi.label.all" /></option>
	                             	<option value="CHECK"><s:text name="tgi.label.CHECK" /></option>
	                                <option value="CREDIT_CARD"><s:text name="tgi.label.CREDIT_CARD" /></option>
	                                <option value="DEBIT_CARD"><s:text name="tgi.label.DEBIT_CARD" /></option>
	                                <option value="WALLET"><s:text name="tgi.label.WALLET" /></option>
	                                </select>
									 </div></div>
								   <div class="col-md-2 col-sm-6">
								   <div class="form-group">
									<select name="paymentModeType" id="paymentModeType" class="form-control input-sm">
									<option value="" selected="selected"><s:text name="tgi.label.select_payment_mode" /></option>
									<option value="ALL"><s:text name="tgi.label.all" /></option>
	                                <option value="SQUAREUP" ><s:text name="tgi.label.squareUp" /></option>
	                             	<option value="EXTERNALLINK_TEST"><s:text name="tgi.label.external" /></option>
	                                <option value="AUTHORIZE"><s:text name="tgi.label.authorize" /></option>
	                                <option value="WALLET"><s:text name="tgi.label.WALLET" /></option>
	                                <option value="WIRECARD"><s:text name="tgi.label.wirecard" /></option>
	                                <option value="STRIPE"><s:text name="tgi.label.stripe" /></option>
	                                </select>
									 </div></div>
								   <div class="col-md-2 col-sm-6">
								   <div class="form-group">
									<select name="paymentStatus" id="paymentStatus" class="form-control input-sm">
									<option value="" selected="selected"><s:text name="tgi.label.select_payment_status" /></option>
									<option value="ALL"><s:text name="tgi.label.all" /></option>
	                                <option value="PENDING" ><s:text name="tgi.label.PENDING" /></option>
	                             	<option value="SUCCESS"><s:text name="tgi.label.SUCCESS" /></option>
	                                <option value="FAILED"><s:text name="tgi.label.FAILED" /></option>
	                                <option value="PROCESSING"><s:text name="tgi.label.PROCESSING" /></option>
	                                <option value="DECLINED"><s:text name="tgi.label.DECLINED" /></option>
	                                <option value="REFUND"><s:text name="tgi.label.REFUND" /></option>
	                                </select>
									</div> 
									</div>
									<div class="col-md-2 col-sm-6">
									<select name="bookingStatus" id="bookingStatus" class="form-control input-sm">
									<option value="" selected="selected"><s:text name="tgi.label.select_booking_status" /></option>
									<option value="ALL"><s:text name="tgi.label.all" /></option>
	                                <option value="PENDING" ><s:text name="tgi.label.PENDING" /></option>
	                             	<option value="CONFIRM"><s:text name="tgi.label.CONFIRM" /></option>
	                                <option value="FAILED"><s:text name="tgi.label.FAILED" /></option>
	                                <option value="PROCESSING"><s:text name="tgi.label.PROCESSING" /></option>
	                                <option value="CANCELLED"><s:text name="tgi.label.CANCELLED" /></option>
	                                </select>
									 </div>
								   <div class="col-md-1">
									<div class="form-group">
									<input type="reset" class="btn btn-sm btn-danger btn-block" id="configreset" value="Clear">
								</div>
								</div>
									<div class="col-md-1">
									<div class="form-group">
									<input type="submit" class="btn btn-sm btn-info btn-block" value="Search" />
								</div>
								</div>
							</div>
					</form> 
					</div>
                            
                                   
			<div class="dash-table">
			<div class="content">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"
										role="grid" aria-describedby="example_info"
										style="width: 100%;" >
                                        <thead>
										<tr class="border-radius border-color" role="row">
                                                    <th data-priority="1" style="width: 20px"></th>
                                                    <th data-priority="3" class="col-md-1"><s:text name="tgi.label.booking_date" /></th>
                                                    <th data-priority="2"><s:text name="tgi.label.order_id" /></th>
                                                      <th data-priority="5"><s:text name="tgi.label.taveller_name" /></th>
                                                      <th data-priority="5">Taveller Info</th>
                                                      <th data-priority="5" style="width: 54px;">Guest</th>
                                                      <th data-priority="5">From - To</th>
                                                      <th data-priority="6"><s:text name="tgi.label.start_date" /></th>
                                                    <th data-priority="7"><s:text name="tgi.label.end_date" /></th>
                                                  
                                                     <th data-priority="8"><s:text name="tgi.label.price" /></th>
                                                     <th data-priority="10"style="width: 120px;"><s:text name="tgi.label.booking_status" /></th>
                                                     <th data-priority="9" style="width: 118px;"><s:text name="tgi.label.payment_status" /></th>
                                                      <th data-priority="11" style="width: 107px;">Booking Source</th>
													<th data-priority="12" style="width: 47px"><img class="clippy" src="admin/img/svg/settings.svg" width="16" alt="Settings" style="margin-left: 6px;"></th>
													<th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
   											<s:if test="tourOrderList.size > 0">
                                           <s:iterator value="tourOrderList" status="rowCount">
                                                <tr>
                                                    <td scope="row" data-title="S.No"><div class="center"><s:property value="%{#rowCount.count}" /></div></td>
                                                    <td data-title="OrderId">${spysr:formatDate(bookingDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM/dd/yyyy hh:mm:ss a')}</td>
                                                    <td data-title="OrderId"><a href="tourPassengerOrderDetails?id=<s:property value="id"/>&orderId=<s:property value="orderId"/>">${orderId}</a></td>
                                                    <td >${orderCustomer.firstName} ${orderCustomer.lastName}</td>
                                                    <td class="">
                                                    		<span class="oxy_pd_tp"><img class="" src="admin/img/svg/envelope-g.svg" width="12"> &nbsp;${orderCustomer.email}</span> <br>
                                                    		<c:choose>
                                                    		<c:when test="${orderCustomer.mobile != null && orderCustomer.mobile != ''}">
                                                    	 	<span class="oxy_pd_bt"><img class="clippy" src="admin/img/svg/telephone-g.svg" width="12"> &nbsp;${orderCustomer.mobile}</span>
                                                    		</c:when>
                                                    		<c:otherwise>
                                                    		</c:otherwise>
                                                    		</c:choose>
                                                    </td>
                                                    <td ><span class="oxy_number_bg_style">${totalGuest}</span></td>
                                                    <td >${origin} - ${destination}</td>
                                                    <td >${spysr:formatDate(tourStartDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM/dd/yyyy')}</td>
                                                    <td >${spysr:formatDate(tourEndDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM/dd/yyyy')}</td>
     												<td data-title="Price">${finalPrice}</td>
     												<td data-title="Booking Status">
     												<c:choose>
     												<c:when test="${bookingStatus == 'CONFIRM'}">
 														<div class="oxy_status_bg_success">${bookingStatus}</div>										
     												</c:when>
     												<c:when test="${bookingStatus == 'PENDING'}">
     													<div class="oxy_status_bg_warning">${bookingStatus}</div>	
     												</c:when>
     												<c:when test="${bookingStatus == 'FAILED'}">
     													<div class="oxy_status_bg_danger">${bookingStatus}</div>	
     												</c:when>
     												<c:when test="${bookingStatus == 'CANCELLED'}">
     													<div class="oxy_status_bg_danger">${bookingStatus}</div>	
     												</c:when>
     												<c:when test="${bookingStatus == 'PROCESSING'}">
     													<div class="oxy_status_bg_active">${bookingStatus}</div>	
     												</c:when>
     												</c:choose>
     												</td>
     												<td data-title="Payment Status">
     												<c:choose>
     												<c:when test="${paymentStatus == 'SUCCESS'}">
 														<div class="oxy_status_bg_success">${paymentStatus}</div>										
     												</c:when>
     												<c:when test="${paymentStatus == 'PENDING'}">
     													<div class="oxy_status_bg_warning">${paymentStatus}</div>	
     												</c:when>
     												<c:when test="${paymentStatus == 'FAILED'}">
     													<div class="oxy_status_bg_danger">${paymentStatus}</div>	
     												</c:when>
     												<c:when test="${paymentStatus == 'REFUND'}">
     													<div class="oxy_status_bg_active">${paymentStatus}</div>	
     												</c:when>
     												</c:choose>
     												
     												</td>
     												<td data-title="Created By">${bookingSource != null && bookingSource != ''?bookingSource:'N/A'}</td>
													<td data-title="Action">
													<div class="dropdown pull-right" style="    margin-right: 20px;">
													  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
													  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
													  <ul class="dropdown-menu">
													    <li class="action"><a href="tourPassengerOrderDetails?id=<s:property value="id"/>&orderId=<s:property value="orderId"/>">Order Detail</a></li>
													    <li class="action"><a href="generateTourCustomerInvoice?id=<s:property value="id"/>&orderId=<s:property value="orderId"/>&companyId=<s:property value="companyId"/>">Invoice</a></li>
													    <li class="action"><a href="userDelete?id=${id}">Delete</a></li>
													  </ul>
													</div>
													</td> 
														<td></td>
														 </tr>
                                                </s:iterator></s:if>
                                            </tbody>
											
                                        </table></div></div></div></div>
										</div>
            </section>
        <!--ADMIN AREA ENDS-->

<script>

$(function() {
	  $('input[name="bookingDateRange"],input[name="travelStartEndDateRange"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: {
	          cancelLabel: 'Clear'
	      },
	      "showDropdowns": true,
	      ranges: {
	           /* 'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')], */
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        },
	        "cancelClass": "btn-danger"
	  });

	  $('input[name="bookingDateRange"],input[name="travelStartEndDateRange"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
	  });

	  $('input[name="bookingDateRange"],input[name="travelStartEndDateRange"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });
	});
<!--AutoCompleteter  -->
     //order id
		var OrderId = {
			url: "getTourReportsJson.action?data=orderId",
			getValue: "orderId",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#orderid-json").easyAutocomplete(OrderId);
	
		
       //airine
		var PaymentStatus = {
			url: "getTourReportsJson.action?data=paymentStatus",
			getValue: "status",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#pymtstatus-json").easyAutocomplete(PaymentStatus);

		var Agency = {
			url: "getTourReportsJson.action?data=agencyName",
			getValue: "agencyName",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#agency-json").easyAutocomplete(Agency);
		
			
	</script>
	<script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>	