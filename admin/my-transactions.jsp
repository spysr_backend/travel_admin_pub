<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>

  
<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}

.show_date_to_user {
    padding: 0px 15px;
    position: relative;
    width: 93%;
    top: -25px;
    left: 5px;
    pointer-events: none;
    display: inherit;
    color: #000;
}





</style>
        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
        
            <section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                        <div class="pnl">
                                 <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.my_transactions" /></h5>
                                <div class="set pull-left">
                                  <!--   <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                            <div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
                              
                               </div> 
                                <form action="listTransactionFilter" class="filter-form" id="resetform">
							    <div class="profile-timeline-card mt-0 mb-0 pt-4 p-0 row row-minus" id="filterDiv" style="display:none;" >
                                <div class="col-md-2 col-sm-6">
								<div class="form-group">
                                <input type="text" name="startDate" id="datepicker_startDate" value="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MM/dd/yyyy')}"
												placeholder="Start Date...."
												class="form-control search-query date1 input-sm" />
												
                                </div>
								</div>
                                <div class="col-md-2 col-sm-6">
								<div class="form-group">
                                <input type="text" name="endDate" id="datepicker_endDate" value="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MM/dd/yyyy')}"
												placeholder="End Date...."
												class="form-control hasdatepicker date2 input-sm" />
                               </div>
								</div>
                                <div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="agencyName" id="agency-json"
												placeholder="Search By Agency Name...."
												class="form-control search-query input-sm" value="${agencyName}" />
								</div></div>
								<%-- <div class="col-md-2">
								<input type="text" name="transactionType" id="transactiontype-json"
												placeholder="Search By Transaction Type...."
												class="form-control search-query" value="${transactionType}" />
								</div> --%>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<select name="transactionType" id="paymentMode" class="form-control input-sm" value="${transactionType}">
								<option value="" selected="selected"><s:text name="tgi.label.select" /> <s:text name="tgi.label.transaction_type" /></option>
                             	<option value="Debit"><s:text name="tgi.label.debit" /></option>
                                <option value="Credit"><s:text name="tgi.label.credit" /></option>
                                </select>
								</div>
								</div>
                                <div class="col-md-1 col-sm-6">
								<div class="form-group">
								<input type="reset" class="btn btn-danger btn-block" id="configreset" value="Clear">
								</div>
								</div>
									<div class="col-md-1 col-sm-6">
									<div class="form-group">
									<input type="submit" class="btn btn-info btn-block" value="Search" />
								</div>
								</div>
								</div>
                                </form> 
                              <%--   <div class="set pull-right">
                                    <div class="dropdown">
                                      <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <s:text name="tgi.label.dropdown" />
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#"><s:text name="tgi.label.action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.another_action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.something_else_here" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.separated_link" /></a></li>
                                      </ul>
                                    </div>
                               </div> --%>
                           


					 <div class="cnt cnt-table">
						    <div class="no-table-css">
		<div class="fw-container">
		<div class="fw-body">
			<div class="content mt-0 pt-2">
				<!-- <h1 class="page_title">File export</h1> -->
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display nowrap dataTable table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                                        <!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
                                            <thead>
                                                <tr class="border-radius border-color">
                                                    <th><s:text name="tgi.label.s_no" /></th>
                                                    <th><s:text name="tgi.label.alloted_at" /></th>
                                                    <th><s:text name="tgi.label.amount" /></th>
                                                    <th><s:text name="tgi.label.openingbal" /></th>
                                                    <th><s:text name="tgi.label.closingbal" /></th>
                                                    <th><s:text name="tgi.label.currency" /></th>
                                                    <th><s:text name="tgi.label.transaction_type" /></th>
                                                    <th><s:text name="tgi.label.agency" /></th>
													
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                	<s:iterator value="superTx" status="rowCount">

												<tr>
												<td data-title="S.No"><s:property value="#rowCount.count"/></td>
													<td data-title="Alloted_at"><s:property value="convertDate" /></td>
													<td data-title="Amount"><s:property value="amount"/></td>
													<td data-title="OpeningBal"><s:property value="parentOpeningBalance" /></td>
													<td data-title="ClosingBal"><s:property value="parentClosingBalance" /></td>
													<td data-title="Currency"><s:property value="currency" /></td>
													<td data-title="<s:text name="tgi.label.transaction_type" />"><s:property value="transactionType" /></td>
												 <td data-title="Agency"><s:property value="agencyName" /></td>
												 </tr>

											</s:iterator>
                                            </tbody>
                                        </table>
                                        
                                    </div>
								</div></div></div></div></div>
                            </div>
                        </div>
                </div>
            </section>
        <!--ADMIN AREA ENDS-->

        
<script type="text/javascript" class="init">
$(document).ready(function() {
		$('#filterBtn').click(function() {
	    $('#filterDiv').toggle();
	});
		
		$('div.easy-autocomplete').removeAttr('style');
		
		$('#configreset').click(function(){
            $('#resetform')[0].reset(); 
            });
} );

$('.faq-links').click(function() {
	var collapsed = $(this).find('i').hasClass('fa-compress');

	$('.faq-links').find('i').removeClass('fa-expand');

	$('.faq-links').find('i').addClass('fa-compress');
	if (collapsed)
		$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
});
	</script>	
<script>
$(document).ready(function() {
	var date1="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
	var date2="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
	 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
	 
});
</script>
	
	<script>
		//agency name
		var Agency = {
			url: "getTransactionJson.action?data=agencyName",
			getValue: "agencyName",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#agency-json").easyAutocomplete(Agency);
		
		//transaction type
		var TransactionType = {
			url: "getTransactionJson.action?data=transactionType",
			getValue: "transactionType",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#transactiontype-json").easyAutocomplete(TransactionType);
		
	</script>	
	<script>
	$(document).ready(function(){$(".filterBtn").click(function(){$("#filterDiv").toggle(500)}),
	 $("div.easy-autocomplete").removeAttr("style"),$("#configreset").click(function(){$("#resetform")[0].reset()})}),
	 $(".filter-link").click(function(){var e=$(this).find("i").hasClass("fa-angle-up");$(".filter-link").find("i").removeClass("fa-angle-down"),
	 $(".filter-link").find("i").addClass("fa-angle-up"),e&&$(this).find("i").toggleClass("fa-angle-up fa-2x fa-angle-down fa-2x")});
	 </script>