<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<link href="admin/css/jquery-ui.css" rel="stylesheet" type="text/css"/>  
   <script src= https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js> </script>  
   <script src= https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js> </script>
 
 <!-- <link href="admin/css/jquerydarkness-ui.min.css" rel="stylesheet" type="text/css"/> 
 <link href="admin/css/slider.css" rel="stylesheet" type="text/css"/>  
   -->
   <!--  <link href="admin/css/slider.css" rel="stylesheet" type="text/css"/>   -->
  
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addMarkup";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script> 

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"markupList";
	/* $('#success').click(function() {
	 window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	}); */
	$('#alert_box_info_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box_info').hide();
			
		});
	$('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
  
    
        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->




<section class="wrapper container-fluid">

	<div class="">
		<div class="">
		<div class="card1">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.new_flight_markup_configuration" />
					</h5>
					<!-- <div class="set pull-left">
						 <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button>
					</div> -->
					<div class="set pull-right">
						<div class="dropdown">
							<div class="btn-group">
								<a class="btn btn-xs btn-success" href="flightMarkupList"><span
											class="glyphicon glyphicon-list-alt"></span>&nbsp;Flight
											<s:text name="tgi.label.markup_list" /> </a> 
							</div>
						</div>
					</div>
				</div> 
		</div> </div>
		
		
		<div class="container">
		<div class="row">
		<h4 class="text-center p-4 hidden-xs">New Flight Markup Configuration</h4> 
			<div class="spy-form-horizontal mb-0">  
						<div class="">
						<form action="setMarkup" id="flight-markup-add" method="post" class="filter-form" >
							
							<div class="row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.config_number" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control" name="configData"
														id="configData" autocomplete="off" required>
														<option selected value=""><s:text
																name="tgi.label.select_company_confignumber" /></option>
														<s:if
															test="%{#session.Company.companyRole.isDistributor()}">
															<s:iterator value="companyConfigIdsList">
																<option
																	value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>/<s:property value="companyName"/>"><s:property
																		value="config_number" /> -
																	<s:property value="configname" /> (
																	<s:property value="companyName" />-
																	<s:property value="companyUserId" />)
																</option>
															</s:iterator>
															<s:iterator value="AgencyConfiglist">
																<option
																	value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>/<s:property value="companyName"/>"><s:property
																		value="config_number" /> -
																	<s:property value="configname" /> (
																	<s:property value="companyName" />-
																	<s:property value="companyUserId" />)
																</option>
															</s:iterator>
														</s:if>
														<s:elseif test="%{#session.Company.companyRole.isAgent()}">
															<s:iterator value="AgencyConfiglist">
																<option
																	value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>/<s:property value="companyName"/>"><s:property
																		value="config_number" /> -
																	<s:property value="configname" /> (
																	<s:property value="companyName" />-
																	<s:property value="companyUserId" />)
																</option>
															</s:iterator>
														</s:elseif>

														<s:else>
															<s:iterator value="companyConfigIdsList">
																<option
																	value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>/<s:property value="companyName"/>"><s:property
																		value="config_number" /> -
																	<s:property value="configname" /> (
																	<s:property value="companyName" />-
																	<s:property value="companyUserId" />)
																</option>
															</s:iterator>
														</s:else>


													</select></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_name" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control"
														id="username" name="name"
														value='<s:property value="name" />'
														placeholder="Markup Name" autocomplete="off" required></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.is_accumulative" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control" name="accumulative"
														autocomplete="off" required>
														<option value="true"><s:text name="tgi.label.yes" /></option>
														<option value="false" selected="selected"><s:text
																name="tgi.label.no" /></option>

													</select></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.lable.is_fixedamount" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control" name="fixedAmount" required>
														<option value="true"><s:text name="tgi.label.yes" /></option>
														<option value="false"><s:text name="tgi.label.no" /></option>

													</select></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.markup_amount" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control"
														id="last-name" name="markup"
														value='<s:property value="markup" />' placeholder="Amount"
														autocomplete="off" required></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.position_of_markup" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="number" class="form-control"
														name="positionOfMarkup" id="country" autocomplete="off"
														required></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-4">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.airline" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control" name="airline" id="country" autocomplete="off" required>
														<option value="ALL" selected="selected"><s:text
																name="tgi.label.all" /></option>
														<s:iterator value="allAirlineList">
															<option value="<s:property value="airlinename"/>"><s:property
																	value="airlinename"></s:property></option>

														</s:iterator>
													</select></div></div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.departure_date" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" id="datepicker_dep"
														class="form-control date1 " name="deptDateTr"
														value='<s:property value="deptDate" />' autocomplete="off"
														placeholder="Departure Date"></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.return_date" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" id="datepicker_arr"
														class="form-control date2"
														value='<s:property value="arrvDate" />' name="arrvDateTr"
														autocomplete="off" placeholder="Arrival Date"></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.promofare_startdate" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" id="datepicker_PromofareStart"
														class="form-control date3" name="promofareStartDateTr"
														value='<s:property value="promofareStartDate" />'
														autocomplete="off" placeholder="Promofare Start Date"></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_enddate" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" id="datepicker_PromofareEnd"
														class="form-control date4" name="promofareEndDateTr"
														value='<s:property value="promofareEndDate" />'
														autocomplete="off" placeholder="Promofare EndDate"></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.origin" />
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" id="origin"
															class="form-control airportList" name="origin"
															value='<s:property value="origin" />' autocomplete="off"
															placeholder="ALL" auto-complete-directive ui-items="names"></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.destination" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" id="dest"
														class="form-control airportList"
														name="destination"
														value='<s:property value="destination" />'
														autocomplete="off" placeholder="ALL"
														auto-complete-directive ui-items="names"></div></div>
											</div>
										</div>
									</div>
										
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.country" />
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<select class="form-control" name="country"
														id="country" autocomplete="off" required>
														<option selected value="ALL"><s:text
																name="tgi.label.all" /></option>
														<s:iterator value="countryList">
															<option value="<s:property value="c_name"/>"><s:property
																	value="c_name"></s:property></option>
														</s:iterator>

													</select></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.class_of_service" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
													<select class="form-control" name="classOfService"
														id="country" autocomplete="off" required>
														<option selected="selected" value="ALL"><s:text
																name="tgi.label.all" /></option>
														<option value="Economy"><s:text
																name="tgi.label.economy" /></option>
														<option value="Premium"><s:text
																name="tgi.label.premium_economy" /></option>
														<option value="Business"><s:text
																name="tgi.label.business" /></option>
														<option value="First"><s:text
																name="tgi.label.first" /></option>

													</select>
											</div></div>
										</div>
									</div>  
								</div>	 
						 <div class=" row">
						 <div class="col-md-12">
							<div class="set pull-right">
										<div class="form-actions">
											<button class="btn btn-default btn-danger" id="tourCancelBtn" type="reset"><s:text name="tgi.label.reset" /></button>
											<button type="submit" class="btn btn-primary" id=""><s:text name="tgi.label.submit" /></button>
										</div></div>
										</div></div>
						</form>
					</div>
				</div>
			</div></div>
		</div>
	</div>
<!--Script start  -->


						<script>
	                	  $(".airportList").autocomplete({
	                          minChars: 1,
	                          source: "city_autocomplete"
	          										});
						</script>

	<s:if test="message != null && message  != ''">
		<script src="admin/js/jquery.min.js"></script>
		<script src="admin/js/bootstrap.min.js"></script>
		<c:choose>
			<c:when test="${fn:contains(message, 'successfully')}">
				<script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
			</c:when>
			<c:otherwise>
				<script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
			</c:otherwise>
		</c:choose>
	</s:if>
</section>

 <link href="admin/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	   	 <script src="admin/js/jquery-ui.js" ></script>
         <script>
			$(document).ready(function() {
				var date1="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				var date2="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
				 
				 var date3="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					var date4="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					 spysr_date_custom_plugin('filter-form','date3','date4','mm/dd/yyyy','MMM DD YYYY',date3 ,date4);
			});
		</script>
<%--  <!--bootstrap vadidator  -->
                 <script type="text/javascript">
$(document).ready(function() {
$('#flight-markup-add')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				
				/* configData : {
					message : 'ConfigData Role is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a ConfigData  '
						},
					}
				},
				name : {
					message : 'MarkUp Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a MarkUp Name'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'MarkUp Name  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				hotelChain : {
					message : 'HotelChain is not valid',
					validators : {
						 notEmpty : {
							message : 'Select enter a HotelChain'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'HotelChain  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				hotelName : {
					message : 'HotelName is not valid',
					validators : {
						 notEmpty : {
							message : 'Select enter a HotelName'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'HotelName  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				hotelCity : {
					message : 'City is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a City'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'City  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				
				hotelCountry : {
					message : 'Country Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a Country'
						},
					}
				},
				isaccumulative : {
					message : 'isaccumulative  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a isaccumulative'
						},
					}
				},
				isfixedAmount : {
					message : 'IsfixedAmount  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a IsfixedAmount'
						},
					}
				},
				markupAmount : {
					message : 'Amount Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Address'
						},
						  numeric: {
	                            message: 'The value is not a number',
	                            // The default separators
	                            thousandsSeparator: '',
	                            decimalSeparator: '.'
	                        }
						
					}
				},
				positionMarkup : {
					message : 'positionMarkup Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a position markup'
						},
					}
				},
				/* eventDate: {
                    validators: {
                        notEmpty: {
                            message: 'The date is required'
                        },
                        date: {
                            format: 'MM/DD/YYYY',
                            message: 'The date is not a valid'
                        }
                    }
                } */ 
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {
	/* notifySuccess(); */
	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script> --%>
<!--ADMIN AREA ENDS-->

    