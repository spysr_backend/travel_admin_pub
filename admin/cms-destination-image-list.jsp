<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs/fn.tld" prefix="fn" %>
<title>Insert title here</title>
<style type="text/css">
<%--img
{
width : 300px;
height : 200px;
} --%>
td #destpadding
{
padding: 3px 20px 3px 3px;
}
</style>
 
 <input type="hidden" id="seturlcountrycode" value="${param.countryCode}">
 <input type="hidden" id="seturlcityname" value="${param.cityName}">

 <script type="text/javascript">
//when page load
$(document).ready(function()
{
	$("#countryCode").val($("#seturlcountrycode").val());
	 createSelectBoxFromUrl('countryCode','cityName','Select City','<%=request.getContextPath()%>/destinationCityListByCountryCode?countryCode');
 });
 </script>
 

	<script>
		function getImageBySelectCity() {
			$("#cCode").val($("#countryCode").val());
			$("#ctName").val($("#cityName").val());
			$("#getCity").submit();
		}
 	</script> 
	<form action="destinationimage_list" id="getCity">
		<input type="hidden" name="countryCode" id="cCode" /> 
		<input type="hidden" name="cityName" id="ctName" />
	</form>
	
 <div class="alert alert-info alert-dismissable" style="text-align:center;">
<div class="row">
<div class="col-md-6">
<%-- <s:select list="countryList" listKey="c_code" headerKey="TR" headerValue="Turkey"
								listValue="c_name" name="countryCode"
								value="TR" cssStyle="color:black;" id="country"
								cssClass="form-control"/>
 --%>
 											<c:if test="${tourDestinationCountryMap != null}">
													<select class="form-control" name="countryCode"
														id="countryCode"
														onchange="javascript:createSelectBoxFromUrl('countryCode','cityName','Select City','<%=request.getContextPath()%>/destinationCityListByCountryCode?countryCode')">
														<option value =""><s:text name="tgi.label.select_country" /></option>
														<c:forEach items="${tourDestinationCountryMap}"
															var="countryObj">
															<option value="${countryObj.key}">${countryObj.value}</option>
														</c:forEach>
													</select>
												</c:if>
 </div>
<div class="col-md-6">
<%--  <s:select headerKey="-1" headerValue="select city" id="city" onchange="getImageBySelectCity();"
		list="#{'ankara':'ankara', 'Antalya':'Antalya', 'Bodrum':'Bodrum', 'Canakkale':'Canakkale','Chicago':'Chicago','Boston':'Boston','Orlando':'Orlando'}" 
		name="role" cssClass="form-control"
		value="1" />
 --%>	
 											<select id="cityName" name="cityName" class="form-control" onchange="getImageBySelectCity();">
											<option value ="">Select City</option>
											</select> 
 </div></div>				
 </div>
 				<s:if test="message != '' && message != null">
                 <c:if test="${(fn:indexOf(message, 'Failed') != null || fn:indexOf(message, 'Failed') != '') && fn:indexOf(message, 'Failed') == 0}">
                 <div class="alert alert-success" style="text-align:center;">
                ${message} 
                </div>
                </c:if>
                <c:if test="${(fn:indexOf(message, 'Delete') != null || fn:indexOf(message, 'Delete') != '') && fn:indexOf(message, 'Delete') == 0}">
                 <div class="alert alert-danger" style="text-align:center;">
                ${message} 
                </div>
                </c:if>
                </s:if>
                <table>
                
                
                <s:if test="cityImagePathList.size == 0">
                <div class="alert alert-danger" style="text-align:center;">
                seleted city image not found
                </div>
                </s:if>
                
<s:iterator value="cityImagePathList" begin="0" end="cityImagePathList.size-1" status="i">
<c:if test="${i.count == 1 || i.count%3 == 1}">
<tr>
</c:if>
<td id="destpadding">
<img height="200px" width="300px" alt="" src="get_image_content?imgpath=<s:property />" />
<a href="destinationimage_delete?countryCode=${param.countryCode}&cityName=${param.cityName}&imgpath=<s:property />">
<button style="text-align:center;" class="btn btn-info">Delete</button>
</a>
</td>
<c:if test="${i.count == 3}">
</tr>
</c:if>
</s:iterator>

</table>
