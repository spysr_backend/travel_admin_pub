<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/c.tld" prefix="c"%>

<title>Insert title here</title>
	<section class="wrapper container-fluid">

		<div class="row">
			<div class="col-md-12">
				<div class="pnl">
					<div class="hd clearfix">
						<h5 class="pull-left"><s:text name="tgi.label.add_image" /></h5>
						<div class="set pull-left"></div>
					</div>
					<div class="hd clearfix">
						<h5 class="pull-left"></h5>
					</div>


					<div class="cnt cnt-table">
						<div class="table-responsive">
							<form action="destinationimage_insert"
								enctype="multipart/form-data" method="post"
								class="form-horizontal">
								<table class="table table-form">
									<thead>
										<tr>
											<th style="color:green;font-weight:bold;">${message}</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<div class="field-name"><s:text name="tgi.label.country" /></div>
											</td>
											<td>
											<c:if test="${tourDestinationCountryMap != null}">
													<select class="form-control" name="countryCode"
														id="countryCode"
														onchange="javascript:createSelectBoxFromUrl('countryCode','cityName','Select City','<%=request.getContextPath()%>/destinationCityListByCountryCode?countryCode')">
														<option value =""><s:text name="tgi.label.select_country" /></option>
														<c:forEach items="${tourDestinationCountryMap}"
															var="countryObj">
															<option value="${countryObj.key}">${countryObj.value}</option>
														</c:forEach>
													</select>
												</c:if>
											</td>
											<td>
												<div class="field-name"><s:text name="tgi.label.city" /></div>
											</td>
											<td>
											<select id="cityName" name="cityName" class="form-control">
											<option value =""><s:text name="tgi.label.select_city" /></option>
											</select> 
											<!-- <input type="text" name="cityName"
												class="form-control input-sm" style="display: none" /> --></td>
										</tr>
										<tr>
											<td colspan=1>
												<div class="field-name"><s:text name="tgi.label.image" /></div>
											</td>
											<td colspan=3><input type="file" name="destinationImage"
												class="form-control input-sm"></td>
										</tr>
									</tbody>

								</table>
								<div class="form-group table-btn">
									<div class="col-sm-10 col-sm-offset-2 ">

										<div class="pull-right">
											<button type="submit" onclick="javascript:showPopUp();"
												class="btn btn-primary btn-lg"><s:text name="tgi.label.add" /></button>
										</div>
									</div>
								</div>
							</form>
							<br> <br>
						</div>
					</div>


				</div>
			</div>
		</div>
	</section>

	<%--  <s:select list="countryList" listKey="c_code"
								listValue="c_name" name="countryCode"
								value="TR" cssStyle="color:black;"
								cssClass="form-control" data-live-search="true" />
								</td>
                                      --%>
