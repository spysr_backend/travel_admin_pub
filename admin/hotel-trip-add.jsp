
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<style>
.inner-addon .glyphicon {
    position: absolute;
    padding: 5px 5px 5px 5px;
    pointer-events: none;
    margin-top: 25px;
}
.right-addon .glyphicon {
    right: 20px;
}
</style>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addCompany";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<script type="text/javascript">
$(document).ready(function()
		{
			/*
				assigning keyup event to password field
				so everytime user type code will execute
			*/

			$('#password').keyup(function()
			{
				$('#result').html(checkStrength($('#password').val()))
			})	
			
			/*
				checkStrength is function which will do the 
				main password strength checking for us
			*/
			
			function checkStrength(password)
			{
				//initial strength
				var strength = 0
				
				//if the password length is less than 6, return message.
				if (password.length < 6) { 
					$('#result').removeClass()
					$('#result').addClass('short')
					return 'Too short' 
				}
				
				//length is ok, lets continue.
				
				//if length is 8 characters or more, increase strength value
				if (password.length > 7) strength += 1
				
				//if password contains both lower and uppercase characters, increase strength value
				if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
				
				//if it has numbers and characters, increase strength value
				if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
				
				//if it has one special character, increase strength value
				if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
				
				//if it has two special characters, increase strength value
				if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
				
				//now we have calculated strength value, we can return messages
				
				//if value is less than 2
				if (strength < 2 )
				{
					$('#result').removeClass()
					$('#result').addClass('weak')
					return 'Weak'			
				}
				else if (strength == 2 )
				{
					$('#result').removeClass()
					$('#result').addClass('good')
					return 'Good'		
				}
				else
				{
					$('#result').removeClass()
					$('#result').addClass('strong')
					return 'Strong'
				}
			}
		});
</script>
<script type="text/javascript">
 

 function isNumberKey(evt,obj){
	    var charCode = (evt.which) ? evt.which : event.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57))   
	    
	        return false;
	   
	}
	function InvalidMsg(textbox) {
	    
	    if(textbox.validity.patternMismatch){
	       textbox.setCustomValidity('Mobile number not vaild.');
	   }    
	   else {
	       textbox.setCustomValidity('');
	   }
	   return true;
	}
	function InvalidEmailMsg(textbox) {
	    
	    if(textbox.validity.patternMismatch){
	       textbox.setCustomValidity('Email address not vaild.');
	   }    
	   else {
	       textbox.setCustomValidity('');
	   }
	   return true;
	}

	function onlyAlphabets(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                return true;
            else
                return false;
        }
        catch (err) {
            alert(err.Description);
        }}

 </script>

 
<script type="text/javascript">
	$(document).ready(
			function() {
 			var currency_list = [];
 			 $('#country').change(function(){
 				var country=$('#country').val();
 				 document.getElementById('Billingcountry').value =country;
				/*  $.ajax({
					//Action Name
					url :"currencyJson",
					dataType : "json",
					success : function(data, textStatus, jqXHR) {
						$.each(data.currencyMap, function( key, value ) {
							if(country!="0"){
							if(key==country){
								console.log( key + ": " + value );
								 document.getElementById('currency').value =value;
							
							 
							}
							}
							else{
								 document.getElementById('currency').value ="0";
								 document.getElementById('Billingcountry').value ="0";
							}
							
							});
 					 
					 console.log(data.currencyMap);  
						 
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log("-------Error-----status-----------"+textStatus);
						console.log("-------jqXHR--- -----------"+jqXHR);
						console.log("-----errorThrown-----------"+errorThrown);
					}
				});*/

			   });
 		        });
	 
 </script>

 

<script type="text/javascript">

        $(document).ready(function(){
 			 $('#bilAdress').click(function(){
 					if($(this).is(":checked")){
                	if ($('textarea#address') != undefined) {
                		   var message = $('textarea#address').val();
                		   document.getElementById('Billingaddress').value =message;
                		 }
 				  }

                else if($(this).is(":not(:checked)")){
                	 document.getElementById('Billingaddress').value ="";
                }

            });

        });

    </script>



<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"getApprovalCompaniesList";
	  
	$('#success').click(function() {
	 window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 
 

            <section class="wrapper container-fluid"> 
                <div class="">
                    <div class="">
                    	<div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left">Add Hotel Trip</h5>
                        	  <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-outline-danger" href="companyList"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;<s:text name="tgi.label.company_list" /></a>
									</div>
									</div>
                               </div>
								 <div class="set pull-center">        
						<div class="col-sm-9 clearfix " data-placement="top">
						<a href="addTourTrip" class="btn btn-top-link btn-xs"> All Trips </a>
						<a href="addTourTrip" class="btn btn-top-link btn-xs"> Tour Trips </a>
						<a href="addHotelTrip" class="btn btn-top-link btn-xs"> Hotel Trips </a> 
						<a href="addFlightTrip" class="btn btn-top-link btn-xs"> Flight Trips </a> 
						<a href="addCarTrip" class="btn btn-top-link btn-xs"> Car Trips </a> 
						<a href="addMiscellaneousTrip" class="btn btn-top-link btn-xs"> Miscellaneous Trips </a>
					</div>
					 </div>
                            </div>
 				<div class=""> 
 <div class="col-md-12">
 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse01" aria-expanded="false" aria-controls="collapseOne">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Customer Details</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse01" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="">
                                        <div class="panel-body">
												<div class="col-sm-1">
							<div class="form-group has-feedback has-success">
								<label for="adults" class="control-label">Adult(s)</label> 
								<input type="number" class="form-control input-sm valid" name="hotelTravelRequestQuotation.adultCount" id="adultCount" placeholder="Adult Count" autocomplete="off" value="1" required="required">
														<span class="form-control-feedback glyphicon glyphicon-ok" style="display: inline;"></span>
							 				</div>
							</div>
											<div class="col-sm-2">
							     				<div class="form-group has-feedback">
													<label for="City" class="control-label">City </label> <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" autocomplete="off" id="hotelCitySearch" name="orderCustomer.city" class="form-control input-sm ui-autocomplete-input" required="required" placeholder="City">
													 

													</div>
												</div>
						<div class="col-sm-2">
							<div class="form-group has-feedback">
								<label for="checkInDate" class="control-label">CheckIn Date </label> <input type="text" autocomplete="off" id="checkInDate" onchange="validateDateFormat(this)" name="hotelTravelRequest.checkIn" class="form-control input-sm hasDatepicker" required="required" placeholder="checkInDate" readonly="">
									 
							</div>
						</div>
						
						<div class="col-sm-2">
							<div class="form-group has-feedback">
								<label for="checkOutDate" class="control-label">CheckOut Date </label> <input type="text" autocomplete="off" id="checkOutDate" name="hotelTravelRequest.checkOut" class="form-control input-sm hasDatepicker" onchange="validateDateFormat(this)" required="required" placeholder="checkOutDate" readonly="">
									 
							</div>
							</div>
							
							<div class="col-sm-2">
							<div class="form-group has-feedback">
							<label for="checkinTime" class="control-label">Check In Time</label>
									<input type="text" class="form-control input-sm hasDatepicker" name="hotelTravelRequestQuotation.checkInTime" id="checkintime" placeholder="CheckIn Time" autocomplete="off" required="required" value="12:00" readonly="">
											 
												</div>
												</div>
								<div class="col-sm-2">
									<div class="form-group has-feedback">
									<label for="checkinTime" class="control-label">Check Out Time</label>
											<input type="text" class="form-control input-sm hasDatepicker" name="hotelTravelRequestQuotation.checkOutTime" id="checkouttime" placeholder="CheckIn Time" autocomplete="off" required="required" value="12:00" readonly="">
												 
												</div>
											</div> 
							<div class="col-sm-2">
							<div class="form-group has-feedback">
								<label for="childs" class="control-label">Child(s)</label> 
								<input type="number" class="form-control input-sm" name="hotelTravelRequestQuotation.childCount" id="childCount" placeholder="Child Count" autocomplete="off" value="0" required="required">
							 
							</div>
							</div>
							<div class="col-sm-2">
							<div class="form-group has-feedback">
							<label for="paymentOption" class="control-label">Payment Option(s)</label>
											<select class="form-control input-sm" name="hotelTravelRequestQuotation.availablePaymentOption" required="required">
														<option value="PrePaid" selected="selected">PrePaid</option>
														<option value="PayAtHotel">PayAtHotel</option>
													</select>
													 
												</div>
							</div>
										<div class="col-sm-2">
												<div class="form-group has-feedback">
													<label for="City" class="control-label">Booking
														Date</label> <input readonly="readonly" type="text" autocomplete="off" name="hotelOrderRow.bookingDate" class="form-control input-sm hasDatepicker" id="bookingDate" placeholder="Booking Date" required="required">
														 
												</div>
											</div>
											<div class="col-sm-2">
											<div class="form-group has-feedback">
											<label for="flightOrderRow.managementFeesdummy" class="control-label">Product Type<span id="mandatory"> * </span></label> 
											<select class="form-control input-sm required productTypeVal" id="managementFeesForSend" name="hotelOrderRow.managementFeesdummy" required="" onchange="getmangmentfee(this)"> 
												<option value="00.00" selected="selected">Select Product Type</option>
												<option value="110.000">Domestic</option>
												<option value="90.000">International</option>
											</select>
											 
													</div>
											</div> 
										</div>
                                    </div>
                                </div>
                                </div>
                                </div>
                                <div class="col-md-12">
 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse02" aria-expanded="false" aria-controls="collapseTwo">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Hotel Details</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="">
                                        <div class="panel-body">
											<div class="col-sm-2">
												<div class="form-group has-feedback">
													<label for="hotelName" class=" control-label">Hotel
														Name <span id="mandatory"> * </span>
													</label> <input type="text" autocomplete="off" required="required" name="hotelOrderHotelData.name" id="hotelname" class="form-control input-sm " placeholder="Hotel Name" value="">
														 

												</div>
											</div>
											
											<div class="col-sm-2">
												<div class="form-group has-feedback">
													<label for="hotelCategory" class=" control-label">Hotel
														Category <span id="mandatory"> * </span>
													</label> <input type="text" autocomplete="off" required="required" name="hotelOrderHotelData.hotelCategory" id="hotelcategory" class="form-control input-sm" placeholder="Hotel Category" value="">
														 

												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group has-feedback">
													<label for="hotel address1" class="control-label">Hotel
														Address1 <span id="mandatory"> * </span>
													</label> <input type="text" autocomplete="off" required="required" name="hotelOrderHotelData.address1" id="hoteladdress" class="form-control input-sm " placeholder="Hotel Address1" value="">
														 

												</div>
											</div>
											<div class="col-sm-2">

												<div class="form-group has-feedback">
													<label for="hotel telephone" class=" control-label"><span id="mandatory"> * </span>Telephone </label> <input type="text" autocomplete="off" name="hotelOrderHotelData.telephone" required="required" class="form-control input-sm" placeholder="telephone">
														 

												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group has-feedback">
													<label for="hotelLocation" class=" control-label">Hotel
														Property Amenities <span id="mandatory"> * </span>
													</label>

													<textarea rows="1" cols="4" name="hotelOrderHotelData.propertyAmenities" id="propertyAmenities" required="required" class="form-control input-sm" placeholder="Hotel Property Amenities"></textarea>
												 
												</div>
											</div>
											<div class="col-sm-12 clearfix more-details">
												<div class="row">
													<a class="btn btn-primary btn-xs" role="button" data-toggle="collapse" href="#filters-hotel" aria-expanded="true" aria-controls="filters-hotel">
														More
													</a>
												</div>
											</div>
											<div class="col-sm-12 clearfix">
												<div class="row"> 
													<div class="collapse" id="filters-hotel" aria-expanded="true" style="">
														<div class="panel-body">
															<!-- Filter of main info -->
															<div class="row clearfix">
																<div class="col-sm-2">

																	<div class="form-group ">
																		<label for="hotelChain" class=" control-label">Hotel
																			Chain </label> <input type="text" autocomplete="off" name="hotelOrderHotelData.chain" class="form-control input-sm" placeholder="Hotel Chain">
																	</div>
																</div>

																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotelName" class=" control-label">Hotel
																			Type </label> <input type="text" autocomplete="off" name="hotelOrderHotelData.type" class="form-control input-sm" placeholder="Hotel Type">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotelName" class=" control-label">Hotel
																			Stars </label> <input type="number" autocomplete="off" name="hotelOrderHotelData.stars" class="form-control input-sm" placeholder="Hotel Stars">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotelrank" class=" control-label">Rank
																		</label> <input type="number" autocomplete="off" name="hotelOrderHotelData.rank" class="form-control input-sm" placeholder="Hotel Rank">
																	</div>
																</div>

																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotelrank" class="  control-label">Region
																			Name </label> <input type="text" autocomplete="off" name="hotelOrderHotelData.regionName" class="form-control input-sm" placeholder="Hotel Region Name">
																	</div>
																</div>
																<div class="col-sm-2">

																	<div class="form-group ">
																		<label for="hotelregionID" class=" control-label">RegionID
																		</label> <input type="text" autocomplete="off" name="hotelOrderHotelData.regionID" class="form-control input-sm" placeholder="Hotel RegionID">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotelregionCityID" class=" control-label">Region
																			CityID </label> <input type="number" autocomplete="off" name="hotelOrderHotelData.regionCityID" class="form-control input-sm" placeholder="Hotel Region CityID">
																	</div>
																</div>

																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotel address2" class=" control-label">Hotel
																			Address2 </label> <input type="text" autocomplete="off" name="hotelOrderHotelData.address2" class="form-control input-sm" placeholder="Hotel Address2">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotel address3" class=" control-label">Hotel
																			Address3 </label> <input type="text" autocomplete="off" name="hotelOrderHotelData.address3" class="form-control input-sm" placeholder="Hotel Address3">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotel state" class=" control-label">Hotel
																			State </label> <input type="text" autocomplete="off" name="hotelOrderHotelData.state" class="form-control input-sm" placeholder="Hotel state">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotel Country" class="  control-label">Hotel
																			Country </label> <select class="form-control input-sm" name="hotelOrderHotelData.country">
																			<option value="">Select Country</option>
																				<option value="Afghanistan">Afghanistan</option>
																				<option value="Albania">Albania</option>
																				<option value="Algeria">Algeria</option>
																				<option value="American Samoa">American Samoa</option>
																				<option value="Andorra">Andorra</option>
																				<option value="Angola">Angola</option>
																				<option value="Anguilla">Anguilla</option>
																				<option value="Antarctica">Antarctica</option>
																				<option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
																				<option value="Argentina">Argentina</option>
																				<option value="Armenia">Armenia</option>
																				<option value="Aruba">Aruba</option>
																				<option value="Australia">Australia</option>
																				<option value="Austria">Austria</option>
																				<option value="Azerbaijan">Azerbaijan</option>
																				<option value="Bahamas">Bahamas</option>
																				<option value="Bahrain">Bahrain</option>
																				<option value="Bangladesh">Bangladesh</option>
																				<option value="Barbados">Barbados</option>
																				<option value="Belarus">Belarus</option>
																				<option value="Belgium">Belgium</option>
																				<option value="Belize">Belize</option>
																				<option value="Benin">Benin</option>
																				<option value="Bermuda">Bermuda</option>
																				<option value="Bhutan">Bhutan</option>
																				<option value="Bolivia">Bolivia</option>
																				<option value="Bosnia">Bosnia</option>
																				<option value="Botswana">Botswana</option>
																				<option value="Bouvet Island">Bouvet Island</option>
																				<option value="Brazil">Brazil</option>
																				<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
																				<option value="British Virgin Islands">British Virgin Islands</option>
																				<option value="Brunei">Brunei</option>
																				<option value="Bulgaria">Bulgaria</option>
																				<option value="Burkina Faso">Burkina Faso</option>
																				<option value="Burundi">Burundi</option>
																				<option value="Cambodia">Cambodia</option>
																				<option value="Cameroon">Cameroon</option>
																				<option value="Canada">Canada</option>
																				<option value="Cape Verde">Cape Verde</option>
																				<option value="Caribbean Netherlands">Caribbean Netherlands</option>
																				<option value="Cayman Islands">Cayman Islands</option>
																				<option value="Central African Republic">Central African Republic</option>
																				<option value="Chad">Chad</option>
																				<option value="Chile">Chile</option>
																				<option value="China">China</option>
																				<option value="Christmas Island">Christmas Island</option>
																				<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
																				<option value="Colombia">Colombia</option>
																				<option value="Comoros">Comoros</option>
																				<option value="Congo - Brazzaville">Congo - Brazzaville</option>
																				<option value="Congo - Kinshasa">Congo - Kinshasa</option>
																				<option value="Cook Islands">Cook Islands</option>
																				<option value="Costa Rica">Costa Rica</option>
																				<option value="Croatia">Croatia</option>
																				<option value="Cuba">Cuba</option>
																				<option value="Curaçao">Curaçao</option>
																				<option value="Cyprus">Cyprus</option>
																				<option value="Czech Republic">Czech Republic</option>
																				<option value="Côte d’Ivoire">Côte d’Ivoire</option>
																				<option value="Denmark">Denmark</option>
																				<option value="Djibouti">Djibouti</option>
																				<option value="Dominica">Dominica</option>
																				<option value="Dominican Republic">Dominican Republic</option>
																				<option value="Ecuador">Ecuador</option>
																				<option value="Egypt">Egypt</option>
																				<option value="El Salvador">El Salvador</option>
																				<option value="Equatorial Guinea">Equatorial Guinea</option>
																				<option value="Eritrea">Eritrea</option>
																				<option value="Estonia">Estonia</option>
																				<option value="Ethiopia">Ethiopia</option>
																				<option value="Falkland Islands">Falkland Islands</option>
																				<option value="Faroe Islands">Faroe Islands</option>
																				<option value="Fiji">Fiji</option>
																				<option value="Finland">Finland</option>
																				<option value="France">France</option>
																				<option value="French Guiana">French Guiana</option>
																				<option value="French Polynesia">French Polynesia</option>
																				<option value="French Southern Territories">French Southern Territories</option>
																				<option value="Gabon">Gabon</option>
																				<option value="Gambia">Gambia</option>
																				<option value="Georgia">Georgia</option>
																				<option value="Germany">Germany</option>
																				<option value="Ghana">Ghana</option>
																				<option value="Gibraltar">Gibraltar</option>
																				<option value="Greece">Greece</option>
																				<option value="Greenland">Greenland</option>
																				<option value="Grenada">Grenada</option>
																				<option value="Guadeloupe">Guadeloupe</option>
																				<option value="Guam">Guam</option>
																				<option value="Guatemala">Guatemala</option>
																				<option value="Guernsey">Guernsey</option>
																				<option value="Guinea">Guinea</option>
																				<option value="Guinea-Bissau">Guinea-Bissau</option>
																				<option value="Guyana">Guyana</option>
																				<option value="Haiti">Haiti</option>
																				<option value="Heard &amp; McDonald Islands">Heard &amp; McDonald Islands</option>
																				<option value="Honduras">Honduras</option>
																			
																				<option value="Hong Kong">Hong Kong</option>
																			
																				<option value="Hungary">Hungary</option>
																			
																				<option value="Iceland">Iceland</option>
																			
																				<option value="India">India</option>
																			
																				<option value="Indonesia">Indonesia</option>
																			
																				<option value="Iran">Iran</option>
																			
																				<option value="Iraq">Iraq</option>
																			
																				<option value="Ireland">Ireland</option>
																			
																				<option value="Isle of Man">Isle of Man</option>
																			
																				<option value="Israel">Israel</option>
																			
																				<option value="Italy">Italy</option>
																			
																				<option value="Jamaica">Jamaica</option>
																			
																				<option value="Japan">Japan</option>
																			
																				<option value="Jersey">Jersey</option>
																			
																				<option value="Jordan">Jordan</option>
																			
																				<option value="Kazakhstan">Kazakhstan</option>
																			
																				<option value="Kenya">Kenya</option>
																			
																				<option value="Kiribati">Kiribati</option>
																			
																				<option value="Kuwait">Kuwait</option>
																			
																				<option value="Kyrgyzstan">Kyrgyzstan</option>
																			
																				<option value="Laos">Laos</option>
																			
																				<option value="Latvia">Latvia</option>
																			
																				<option value="Lebanon">Lebanon</option>
																			
																				<option value="Lesotho">Lesotho</option>
																			
																				<option value="Liberia">Liberia</option>
																			
																				<option value="Libya">Libya</option>
																			
																				<option value="Liechtenstein">Liechtenstein</option>
																			
																				<option value="Lithuania">Lithuania</option>
																			
																				<option value="Luxembourg">Luxembourg</option>
																			
																				<option value="Macau">Macau</option>
																			
																				<option value="Macedonia">Macedonia</option>
																			
																				<option value="Madagascar">Madagascar</option>
																			
																				<option value="Malawi">Malawi</option>
																			
																				<option value="Malaysia">Malaysia</option>
																			
																				<option value="Maldives">Maldives</option>
																			
																				<option value="Mali">Mali</option>
																			
																				<option value="Malta">Malta</option>
																			
																				<option value="Marshall Islands">Marshall Islands</option>
																			
																				<option value="Martinique">Martinique</option>
																			
																				<option value="Mauritania">Mauritania</option>
																			
																				<option value="Mauritius">Mauritius</option>
																			
																				<option value="Mayotte">Mayotte</option>
																			
																				<option value="Mexico">Mexico</option>
																			
																				<option value="Micronesia">Micronesia</option>
																			
																				<option value="Moldova">Moldova</option>
																			
																				<option value="Monaco">Monaco</option>
																			
																				<option value="Mongolia">Mongolia</option>
																			
																				<option value="Montenegro">Montenegro</option>
																			
																				<option value="Montserrat">Montserrat</option>
																			
																				<option value="Morocco">Morocco</option>
																			
																				<option value="Mozambique">Mozambique</option>
																			
																				<option value="Myanmar">Myanmar</option>
																			
																				<option value="Namibia">Namibia</option>
																			
																				<option value="Nauru">Nauru</option>
																			
																				<option value="Nepal">Nepal</option>
																			
																				<option value="Netherlands">Netherlands</option>
																			
																				<option value="New Caledonia">New Caledonia</option>
																			
																				<option value="New Zealand">New Zealand</option>
																			
																				<option value="Nicaragua">Nicaragua</option>
																			
																				<option value="Niger">Niger</option>
																			
																				<option value="Nigeria">Nigeria</option>
																			
																				<option value="Niue">Niue</option>
																			
																				<option value="Norfolk Island">Norfolk Island</option>
																			
																				<option value="North Korea">North Korea</option>
																			
																				<option value="Northern Mariana Islands">Northern Mariana Islands</option>
																			
																				<option value="Norway">Norway</option>
																			
																				<option value="Oman">Oman</option>
																			
																				<option value="Pakistan">Pakistan</option>
																			
																				<option value="Palau">Palau</option>
																			
																				<option value="Palestine">Palestine</option>
																			
																				<option value="Panama">Panama</option>
																			
																				<option value="Papua New Guinea">Papua New Guinea</option>
																			
																				<option value="Paraguay">Paraguay</option>
																			
																				<option value="Peru">Peru</option>
																			
																				<option value="Philippines">Philippines</option>
																			
																				<option value="Pitcairn Islands">Pitcairn Islands</option>
																			
																				<option value="Poland">Poland</option>
																			
																				<option value="Portugal">Portugal</option>
																			
																				<option value="Puerto Rico">Puerto Rico</option>
																			
																				<option value="Qatar">Qatar</option>
																			
																				<option value="Romania">Romania</option>
																			
																				<option value="Russia">Russia</option>
																			
																				<option value="Rwanda">Rwanda</option>
																			
																				<option value="Réunion">Réunion</option>
																			
																				<option value="Samoa">Samoa</option>
																			
																				<option value="San Marino">San Marino</option>
																			
																				<option value="Saudi Arabia">Saudi Arabia</option>
																			
																				<option value="Senegal">Senegal</option>
																			
																				<option value="Serbia">Serbia</option>
																			
																				<option value="Seychelles">Seychelles</option>
																			
																				<option value="Sierra Leone">Sierra Leone</option>
																			
																				<option value="Singapore">Singapore</option>
																			
																				<option value="Sint Maarten">Sint Maarten</option>
																			
																				<option value="Slovakia">Slovakia</option>
																			
																				<option value="Slovenia">Slovenia</option>
																			
																				<option value="Solomon Islands">Solomon Islands</option>
																			
																				<option value="Somalia">Somalia</option>
																			
																				<option value="South Africa">South Africa</option>
																			
																				<option value="South Georgia &amp; South Sandwich Islands">South Georgia &amp; South Sandwich Islands</option>
																			
																				<option value="South Korea">South Korea</option>
																			
																				<option value="South Sudan">South Sudan</option>
																			
																				<option value="Spain">Spain</option>
																			
																				<option value="Sri Lanka">Sri Lanka</option>
																				<option value="St. Barthélemy">St. Barthélemy</option>
																				<option value="St. Helena">St. Helena</option>
																				<option value="St. Kitts &amp; Nevis">St. Kitts &amp; Nevis</option>
																				<option value="St. Lucia">St. Lucia</option>
																				<option value="St. Martin">St. Martin</option>
																				<option value="St. Pierre &amp; Miquelon">St. Pierre &amp; Miquelon</option>
																				<option value="St. Vincent &amp; Grenadines">St. Vincent &amp; Grenadines</option>
																				<option value="Sudan">Sudan</option>
																				<option value="Svalbard &amp; Jan Mayen">Svalbard &amp; Jan Mayen</option>
																				<option value="Swaziland">Swaziland</option>
																				<option value="Sweden">Sweden</option>
																				<option value="Switzerland">Switzerland</option>
																				<option value="Syria">Syria</option>
																				<option value="São Tomé &amp; Príncipe">São Tomé &amp; Príncipe</option>
																				<option value="Taiwan">Taiwan</option>
																				<option value="Tajikistan">Tajikistan</option>
																				<option value="Tanzania">Tanzania</option>
																				<option value="Thailand">Thailand</option>
																				<option value="Timor-Leste">Timor-Leste</option>
																				<option value="Togo">Togo</option>
																				<option value="Tokelau">Tokelau</option>
																				<option value="Tonga">Tonga</option>
																				<option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
																				<option value="Tunisia">Tunisia</option>
																				<option value="Turkey">Turkey</option>
																				<option value="Turkmenistan">Turkmenistan</option>
																				<option value="Turks &amp; Caicos Islands">Turks &amp; Caicos Islands</option>
																				<option value="Tuvalu">Tuvalu</option>
																				<option value="U.S. Outlying Islands">U.S. Outlying Islands</option>
																				<option value="U.S. Virgin Islands">U.S. Virgin Islands</option>
																				<option value="UK">UK</option>
																				<option value="US">US</option>
																				<option value="Uganda">Uganda</option>
																				<option value="Ukraine">Ukraine</option>
																				<option value="United Arab Emirates">United Arab Emirates</option>
																				<option value="Uruguay">Uruguay</option>
																				<option value="Uzbekistan">Uzbekistan</option>
																				<option value="Vanuatu">Vanuatu</option>
																				<option value="Vatican City">Vatican City</option>
																				<option value="Venezuela">Venezuela</option>
																				<option value="Vietnam">Vietnam</option>
																				<option value="Wallis &amp; Futuna">Wallis &amp; Futuna</option>
																				<option value="Western Sahara">Western Sahara</option>
																				<option value="Yemen">Yemen</option>
																				<option value="Zambia">Zambia</option>
																				<option value="Zimbabwe">Zimbabwe</option>
																				<option value="Åland Islands">Åland Islands</option>
																		</select>
																	</div>
																</div>

																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotel email" class=" control-label">Email
																		</label> <input type="text" autocomplete="off" name="hotelOrderHotelData.email" class="form-control input-sm" placeholder="Email">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="url" class=" control-label">Url </label> <input type="text" autocomplete="off" name="hotelOrderHotelData.url" class="form-control input-sm" placeholder="url">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="url" class=" control-label">ZipCode
																		</label> <input type="text" autocomplete="off" name="hotelOrderHotelData.zip" class="form-control input-sm" placeholder="zip code">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="latitude" class=" control-label">Latitude
																		</label> <input type="text" autocomplete="off" name="hotelOrderHotelData.latitude" class="form-control input-sm" placeholder="Latitude">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="longitude" class=" control-label">Longitude
																		</label> <input type="text" autocomplete="off" name="hotelOrderHotelData.longitude" class="form-control input-sm" placeholder="Longitude">
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="hotelLocation" class=" control-label">Hotel Location </label>
																		<textarea rows="2" cols="2" name="hotelOrderHotelData.hotelLocation" class="form-control input-sm" placeholder="Hotel Location"></textarea>
																	</div>
																</div>
																<div class="col-sm-2">
																	<div class="form-group ">
																		<label for="Hotel Room Amenities" class=" control-label">Hotel Room Amenities </label>
																		<textarea rows="4" cols="4" name="hotelOrderHotelData.roomAmenities" class="form-control input-sm" placeholder="Hotel Room Amenities"></textarea>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
                                </div>
                                </div>
                                <div class="col-md-12">
 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse03" aria-expanded="false" aria-controls="collapseThree">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingThree">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Room Details</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="">
                                        <div class="panel-body"> 
                        <div class="panel-group" id="roomnested">
                            <div class="panel panel-default" style="border: 1px solid #ccc;margin-bottom: 5px" id="roomslength">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#roomnested" href="#nested-room"><b>Room 1</b>
                                        </a>
                                    </h4>
                                </div>
                                <div id="nested-room" class="panel-collapse">
                                    <div class="panel-body">
                                    	<div class="first-block-rooms clearfix">
												<div class="col-sm-4">

													<div class="form-group has-feedback">
														<label for="checkInDate" class=" control-label">Room
															Type <span id="mandatory"> * </span>
														</label> <input type="text" autocomplete="off" name="hotelOrderRoomInfoList[0].roomType" id="roomtype" class="form-control input-sm" value="" required="required" placeholder="Room Type">
															

													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group has-feedback">
														<label for="checkInDate" class="  control-label">Meal
															Type 
														</label> <input type="text" autocomplete="off" name="hotelOrderRoomInfoList[0].mealType" id="mealtype" class="form-control input-sm " placeholder="Meal Type"> 
															
													</div>
												</div>

												<div class="col-sm-4">
													<div class="form-group has-feedback">
														<label for="checkInDate" class=" control-label">Inclusions
															<span id="mandatory"> * </span>
														</label> <input type="text" autocomplete="off" required="required" name="hotelOrderRoomInfoList[0].inclusions" id="inclusions" class="form-control input-sm " placeholder="Inclusions" value="">
															


													</div>
												</div>

											</div>
                                  
                                  
                                  											<!-- nested -->

											<div class="panel-group" id="nested">

												<div class="panel panel-default " style="margin-bottom: 5px;margin-top: 10px;" id="mainpanel">
												
													<div class="panel-heading">
														<h4 class="panel-title">
															<a class="collapsed" data-toggle="collapse" data-parent="#nested" href="#nestedPerson"> Lead Person Details </a>
														</h4>
													</div>
													<!--/.panel-heading -->
													<div id="nestedPerson" class="panel-collapse ">
														<div class="panel-body">
														
														<div class="detailswithRM">
															<div class="col-sm-1">
																<div class="form-group has-feedback">
																	<label for="checkInDate" class="control-label">Title
																		<span id="mandatory"> * </span>
																	</label> <select name="hotelOrderRoomInfoList[0].hotelOrderGuests[0].title" class="form-control input-sm" id="title1">
																		<option value="Mr" selected="selected">Mr</option>
													<option value="Mrs">Mrs</option>
													<option value="Miss">Miss</option>
													<option value="Ms">Ms</option>
													<option value="Master">Master</option>
																		
																		</select>
																		

																</div>
															</div>
															<div class="col-sm-2">

																<div class="form-group has-feedback">
																	<label for="checkInDate" class=" control-label">First
																		Name <span id="mandatory"> * </span>
																	</label> <input type="text" autocomplete="off" name="hotelOrderRoomInfoList[0].hotelOrderGuests[0].firstName" class="form-control input-sm firstName" required="required" value="" placeholder="First Name" id="firstName"> <input type="hidden" name="hotelOrderRoomInfoList[0].hotelOrderGuests[0].leader" value="true"> 
																		
																</div>


															</div>
															<input type="hidden" id="totalNumberOfPassenger" value="2">
															<div class="col-sm-2"> 
																<div class="form-group has-feedback">
																	<label for="checkInDate" class=" control-label">Last
																		Name <span id="mandatory"> * </span>
																	</label> <input type="text" autocomplete="off" name="hotelOrderRoomInfoList[0].hotelOrderGuests[0].lastName" class="form-control input-sm " placeholder="Last Name" required="required" value="" id="lastName">
																		

																</div>
															</div>
															<div class="col-sm-1"> 
																<div class="form-group has-feedback">
																	<label for="checkInDate" class=" control-label">GuestType
																		<span id="mandatory"> * </span>
																	</label> <select name="hotelOrderRoomInfoList[0].hotelOrderGuests[0].paxType" class="form-control input-sm">
																		<option selected="selected" value="Adult">Adult</option>
																		<option value="Infant">Infant</option>
																		<option value="Children">Children</option>

																	</select>
																	

																</div>
															</div>

															<div class="col-sm-2">
																<div class="form-group has-feedback">
																	<label for="checkInDate" class=" control-label">Nationality
																		<span id="mandatory"> * </span>
																	</label> <select name="hotelOrderRoomInfoList[0].hotelOrderGuests[0].nationality" class="form-control input-sm" required="">
																		
																			
																			<option value="Afghanistan">Afghanistan</option>
																		
																			
																			<option value="Albania">Albania</option>
																		
																			
																			<option value="Algeria">Algeria</option>
																		
																			
																			<option value="American Samoa">American Samoa</option>
																		
																			
																			<option value="Andorra">Andorra</option>
																		
																			
																			<option value="Angola">Angola</option>
																		
																			
																			<option value="Anguilla">Anguilla</option>
																		
																			
																			<option value="Antarctica">Antarctica</option>
																		
																			
																			<option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
																		
																			
																			<option value="Argentina">Argentina</option>
																		
																			
																			<option value="Armenia">Armenia</option>
																		
																			
																			<option value="Aruba">Aruba</option>
																		
																			
																			<option value="Australia">Australia</option>
																		
																			
																			<option value="Austria">Austria</option>
																		
																			
																			<option value="Azerbaijan">Azerbaijan</option>
																		
																			
																			<option value="Bahamas">Bahamas</option>
																		
																			
																			<option value="Bahrain">Bahrain</option>
																		
																			
																			<option value="Bangladesh">Bangladesh</option>
																		
																			
																			<option value="Barbados">Barbados</option>
																		
																			
																			<option value="Belarus">Belarus</option>
																		
																			
																			<option value="Belgium">Belgium</option>
																		
																			
																			<option value="Belize">Belize</option>
																		
																			
																			<option value="Benin">Benin</option>
																		
																			
																			<option value="Bermuda">Bermuda</option>
																		
																			
																			<option value="Bhutan">Bhutan</option>
																		
																			
																			<option value="Bolivia">Bolivia</option>
																		
																			
																			<option value="Bosnia">Bosnia</option>
																		
																			
																			<option value="Botswana">Botswana</option>
																		
																			
																			<option value="Bouvet Island">Bouvet Island</option>
																		
																			
																			<option value="Brazil">Brazil</option>
																		
																			
																			<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
																		
																			
																			<option value="British Virgin Islands">British Virgin Islands</option>
																		
																			
																			<option value="Brunei">Brunei</option>
																		
																			
																			<option value="Bulgaria">Bulgaria</option>
																		
																			
																			<option value="Burkina Faso">Burkina Faso</option>
																		
																			
																			<option value="Burundi">Burundi</option>
																		
																			
																			<option value="Cambodia">Cambodia</option>
																		
																			
																			<option value="Cameroon">Cameroon</option>
																		
																			
																			<option value="Canada">Canada</option>
																		
																			
																			<option value="Cape Verde">Cape Verde</option>
																		
																			
																			<option value="Caribbean Netherlands">Caribbean Netherlands</option>
																		
																			
																			<option value="Cayman Islands">Cayman Islands</option>
																		
																			
																			<option value="Central African Republic">Central African Republic</option>
																		
																			
																			<option value="Chad">Chad</option>
																		
																			
																			<option value="Chile">Chile</option>
																		
																			
																			<option value="China">China</option>
																		
																			
																			<option value="Christmas Island">Christmas Island</option>
																		
																			
																			<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
																		
																			
																			<option value="Colombia">Colombia</option>
																		
																			
																			<option value="Comoros">Comoros</option>
																		
																			
																			<option value="Congo - Brazzaville">Congo - Brazzaville</option>
																		
																			
																			<option value="Congo - Kinshasa">Congo - Kinshasa</option>
																		
																			
																			<option value="Cook Islands">Cook Islands</option>
																		
																			
																			<option value="Costa Rica">Costa Rica</option>
																		
																			
																			<option value="Croatia">Croatia</option>
																		
																			
																			<option value="Cuba">Cuba</option>
																		
																			
																			<option value="Curaçao">Curaçao</option>
																		
																			
																			<option value="Cyprus">Cyprus</option>
																		
																			
																			<option value="Czech Republic">Czech Republic</option>
																		
																			
																			<option value="Côte d’Ivoire">Côte d’Ivoire</option>
																		
																			
																			<option value="Denmark">Denmark</option>
																		
																			
																			<option value="Djibouti">Djibouti</option>
																		
																			
																			<option value="Dominica">Dominica</option>
																		
																			
																			<option value="Dominican Republic">Dominican Republic</option>
																		
																			
																			<option value="Ecuador">Ecuador</option>
																		
																			
																			<option value="Egypt">Egypt</option>
																		
																			
																			<option value="El Salvador">El Salvador</option>
																		
																			
																			<option value="Equatorial Guinea">Equatorial Guinea</option>
																		
																			
																			<option value="Eritrea">Eritrea</option>
																		
																			
																			<option value="Estonia">Estonia</option>
																		
																			
																			<option value="Ethiopia">Ethiopia</option>
																		
																			
																			<option value="Falkland Islands">Falkland Islands</option>
																		
																			
																			<option value="Faroe Islands">Faroe Islands</option>
																		
																			
																			<option value="Fiji">Fiji</option>
																		
																			
																			<option value="Finland">Finland</option>
																		
																			
																			<option value="France">France</option>
																		
																			
																			<option value="French Guiana">French Guiana</option>
																		
																			
																			<option value="French Polynesia">French Polynesia</option>
																		
																			
																			<option value="French Southern Territories">French Southern Territories</option>
																		
																			
																			<option value="Gabon">Gabon</option>
																		
																			
																			<option value="Gambia">Gambia</option>
																		
																			
																			<option value="Georgia">Georgia</option>
																		
																			
																			<option value="Germany">Germany</option>
																		
																			
																			<option value="Ghana">Ghana</option>
																		
																			
																			<option value="Gibraltar">Gibraltar</option>
																		
																			
																			<option value="Greece">Greece</option>
																		
																			
																			<option value="Greenland">Greenland</option>
																		
																			
																			<option value="Grenada">Grenada</option>
																		
																			
																			<option value="Guadeloupe">Guadeloupe</option>
																		
																			
																			<option value="Guam">Guam</option>
																		
																			
																			<option value="Guatemala">Guatemala</option>
																		
																			
																			<option value="Guernsey">Guernsey</option>
																		
																			
																			<option value="Guinea">Guinea</option>
																		
																			
																			<option value="Guinea-Bissau">Guinea-Bissau</option>
																		
																			
																			<option value="Guyana">Guyana</option>
																		
																			
																			<option value="Haiti">Haiti</option>
																		
																			
																			<option value="Heard &amp; McDonald Islands">Heard &amp; McDonald Islands</option>
																		
																			
																			<option value="Honduras">Honduras</option>
																		
																			
																			<option value="Hong Kong">Hong Kong</option>
																		
																			
																			<option value="Hungary">Hungary</option>
																		
																			
																			<option value="Iceland">Iceland</option>
																		
																			
																				<option selected="selected" value="India">India</option>
																			
																			<option value="India">India</option>
																		
																			
																			<option value="Indonesia">Indonesia</option>
																		
																			
																			<option value="Iran">Iran</option>
																		
																			
																			<option value="Iraq">Iraq</option>
																		
																			
																			<option value="Ireland">Ireland</option>
																		
																			
																			<option value="Isle of Man">Isle of Man</option>
																		
																			
																			<option value="Israel">Israel</option>
																		
																			
																			<option value="Italy">Italy</option>
																		
																			
																			<option value="Jamaica">Jamaica</option>
																		
																			
																			<option value="Japan">Japan</option>
																		
																			
																			<option value="Jersey">Jersey</option>
																		
																			
																			<option value="Jordan">Jordan</option>
																		
																			
																			<option value="Kazakhstan">Kazakhstan</option>
																		
																			
																			<option value="Kenya">Kenya</option>
																		
																			
																			<option value="Kiribati">Kiribati</option>
																		
																			
																			<option value="Kuwait">Kuwait</option>
																		
																			
																			<option value="Kyrgyzstan">Kyrgyzstan</option>
																		
																			
																			<option value="Laos">Laos</option>
																		
																			
																			<option value="Latvia">Latvia</option>
																		
																			
																			<option value="Lebanon">Lebanon</option>
																		
																			
																			<option value="Lesotho">Lesotho</option>
																		
																			
																			<option value="Liberia">Liberia</option>
																		
																			
																			<option value="Libya">Libya</option>
																		
																			
																			<option value="Liechtenstein">Liechtenstein</option>
																		
																			
																			<option value="Lithuania">Lithuania</option>
																		
																			
																			<option value="Luxembourg">Luxembourg</option>
																		
																			
																			<option value="Macau">Macau</option>
																		
																			
																			<option value="Macedonia">Macedonia</option>
																		
																			
																			<option value="Madagascar">Madagascar</option>
																		
																			
																			<option value="Malawi">Malawi</option>
																		
																			
																			<option value="Malaysia">Malaysia</option>
																		
																			
																			<option value="Maldives">Maldives</option>
																		
																			
																			<option value="Mali">Mali</option>
																		
																			
																			<option value="Malta">Malta</option>
																		
																			
																			<option value="Marshall Islands">Marshall Islands</option>
																		
																			
																			<option value="Martinique">Martinique</option>
																		
																			
																			<option value="Mauritania">Mauritania</option>
																		
																			
																			<option value="Mauritius">Mauritius</option>
																		
																			
																			<option value="Mayotte">Mayotte</option>
																		
																			
																			<option value="Mexico">Mexico</option>
																		
																			
																			<option value="Micronesia">Micronesia</option>
																		
																			
																			<option value="Moldova">Moldova</option>
																		
																			
																			<option value="Monaco">Monaco</option>
																		
																			
																			<option value="Mongolia">Mongolia</option>
																		
																			
																			<option value="Montenegro">Montenegro</option>
																		
																			
																			<option value="Montserrat">Montserrat</option>
																		
																			
																			<option value="Morocco">Morocco</option>
																		
																			
																			<option value="Mozambique">Mozambique</option>
																		
																			
																			<option value="Myanmar">Myanmar</option>
																		
																			
																			<option value="Namibia">Namibia</option>
																		
																			
																			<option value="Nauru">Nauru</option>
																		
																			
																			<option value="Nepal">Nepal</option>
																		
																			
																			<option value="Netherlands">Netherlands</option>
																		
																			
																			<option value="New Caledonia">New Caledonia</option>
																		
																			
																			<option value="New Zealand">New Zealand</option>
																		
																			
																			<option value="Nicaragua">Nicaragua</option>
																		
																			
																			<option value="Niger">Niger</option>
																		
																			
																			<option value="Nigeria">Nigeria</option>
																		
																			
																			<option value="Niue">Niue</option>
																		
																			
																			<option value="Norfolk Island">Norfolk Island</option>
																		
																			
																			<option value="North Korea">North Korea</option>
																		
																			
																			<option value="Northern Mariana Islands">Northern Mariana Islands</option>
																		
																			
																			<option value="Norway">Norway</option>
																		
																			
																			<option value="Oman">Oman</option>
																		
																			
																			<option value="Pakistan">Pakistan</option>
																		
																			
																			<option value="Palau">Palau</option>
																		
																			
																			<option value="Palestine">Palestine</option>
																		
																			
																			<option value="Panama">Panama</option>
																		
																			
																			<option value="Papua New Guinea">Papua New Guinea</option>
																		
																			
																			<option value="Paraguay">Paraguay</option>
																		
																			
																			<option value="Peru">Peru</option>
																		
																			
																			<option value="Philippines">Philippines</option>
																		
																			
																			<option value="Pitcairn Islands">Pitcairn Islands</option>
																		
																			
																			<option value="Poland">Poland</option>
																		
																			
																			<option value="Portugal">Portugal</option>
																		
																			
																			<option value="Puerto Rico">Puerto Rico</option>
																		
																			
																			<option value="Qatar">Qatar</option>
																		
																			
																			<option value="Romania">Romania</option>
																		
																			
																			<option value="Russia">Russia</option>
																		
																			
																			<option value="Rwanda">Rwanda</option>
																		
																			
																			<option value="Réunion">Réunion</option>
																		
																			
																			<option value="Samoa">Samoa</option>
																		
																			
																			<option value="San Marino">San Marino</option>
																		
																			
																			<option value="Saudi Arabia">Saudi Arabia</option>
																		
																			
																			<option value="Senegal">Senegal</option>
																		
																			
																			<option value="Serbia">Serbia</option>
																		
																			
																			<option value="Seychelles">Seychelles</option>
																		
																			
																			<option value="Sierra Leone">Sierra Leone</option>
																		
																			
																			<option value="Singapore">Singapore</option>
																		
																			
																			<option value="Sint Maarten">Sint Maarten</option>
																		
																			
																			<option value="Slovakia">Slovakia</option>
																		
																			
																			<option value="Slovenia">Slovenia</option>
																		
																			
																			<option value="Solomon Islands">Solomon Islands</option>
																		
																			
																			<option value="Somalia">Somalia</option>
																		
																			
																			<option value="South Africa">South Africa</option>
																		
																			
																			<option value="South Georgia &amp; South Sandwich Islands">South Georgia &amp; South Sandwich Islands</option>
																		
																			
																			<option value="South Korea">South Korea</option>
																		
																			
																			<option value="South Sudan">South Sudan</option>
																		
																			
																			<option value="Spain">Spain</option>
																		
																			
																			<option value="Sri Lanka">Sri Lanka</option>
																		
																			
																			<option value="St. Barthélemy">St. Barthélemy</option>
																		
																			
																			<option value="St. Helena">St. Helena</option>
																		
																			
																			<option value="St. Kitts &amp; Nevis">St. Kitts &amp; Nevis</option>
																		
																			
																			<option value="St. Lucia">St. Lucia</option>
																		
																			
																			<option value="St. Martin">St. Martin</option>
																		
																			
																			<option value="St. Pierre &amp; Miquelon">St. Pierre &amp; Miquelon</option>
																		
																			
																			<option value="St. Vincent &amp; Grenadines">St. Vincent &amp; Grenadines</option>
																		
																			
																			<option value="Sudan">Sudan</option>
																		
																			
																			<option value="Suriname">Suriname</option>
																		
																			
																			<option value="Svalbard &amp; Jan Mayen">Svalbard &amp; Jan Mayen</option>
																		
																			
																			<option value="Swaziland">Swaziland</option>
																		
																			
																			<option value="Sweden">Sweden</option>
																		
																			
																			<option value="Switzerland">Switzerland</option>
																		
																			
																			<option value="Syria">Syria</option>
																		
																			
																			<option value="São Tomé &amp; Príncipe">São Tomé &amp; Príncipe</option>
																		
																			
																			<option value="Taiwan">Taiwan</option>
																		
																			
																			<option value="Tajikistan">Tajikistan</option>
																		
																			
																			<option value="Tanzania">Tanzania</option>
																		
																			
																			<option value="Thailand">Thailand</option>
																		
																			
																			<option value="Timor-Leste">Timor-Leste</option>
																		
																			
																			<option value="Togo">Togo</option>
																		
																			
																			<option value="Tokelau">Tokelau</option>
																		
																			
																			<option value="Tonga">Tonga</option>
																		
																			
																			<option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
																		
																			
																			<option value="Tunisia">Tunisia</option>
																		
																			
																			<option value="Turkey">Turkey</option>
																		
																			
																			<option value="Turkmenistan">Turkmenistan</option>
																		
																			
																			<option value="Turks &amp; Caicos Islands">Turks &amp; Caicos Islands</option>
																		
																			
																			<option value="Tuvalu">Tuvalu</option>
																		
																			
																			<option value="U.S. Outlying Islands">U.S. Outlying Islands</option>
																		
																			
																			<option value="U.S. Virgin Islands">U.S. Virgin Islands</option>
																		
																			
																			<option value="UK">UK</option>
																		
																			
																			<option value="US">US</option>
																		
																			
																			<option value="Uganda">Uganda</option>
																		
																			
																			<option value="Ukraine">Ukraine</option>
																		
																			
																			<option value="United Arab Emirates">United Arab Emirates</option>
																		
																			
																			<option value="Uruguay">Uruguay</option>
																		
																			
																			<option value="Uzbekistan">Uzbekistan</option>
																		
																			
																			<option value="Vanuatu">Vanuatu</option>
																		
																			
																			<option value="Vatican City">Vatican City</option>
																		
																			
																			<option value="Venezuela">Venezuela</option>
																		
																			
																			<option value="Vietnam">Vietnam</option>
																		
																			
																			<option value="Wallis &amp; Futuna">Wallis &amp; Futuna</option>
																		
																			
																			<option value="Western Sahara">Western Sahara</option>
																		
																			
																			<option value="Yemen">Yemen</option>
																		
																			
																			<option value="Zambia">Zambia</option>
																		
																			
																			<option value="Zimbabwe">Zimbabwe</option>
																		
																			
																			<option value="Åland Islands">Åland Islands</option>
																		

																	</select>
																	


																</div>
															</div>



															<div class="col-sm-2">
																<div class="form-group has-feedback">
																	<label for="checkInDate" class="col-sm-2 control-label">Age
																	</label> <input type="number" autocomplete="off" name="hotelOrderRoomInfoList[0].hotelOrderGuests[0].age" maxlength="3" class="form-control input-sm cusValidationforprice" placeholder="Age">
																		

																</div>
															</div>

															<div class="col-sm-2">

																<div class="form-group has-feedback">
																	<label for="checkInDate" class="col-sm-2 control-label">Email
																	</label> <input type="email" autocomplete="off" id="emailId" name="hotelOrderRoomInfoList[0].hotelOrderGuests[0].email" value="" class="form-control input-sm" placeholder="Email">
																		

																</div>
															</div>
															
															
						
						
						
						

					
					
											
											
											
											
											
											
											
											 
											
														</div>
														<!--/.panel-body -->
													</div>
													<!--/.panel-collapse -->
												</div>
												<!-- /.panel -->
													</div> 
											<!-- nested --> 
											
											
											
                                    </div>
                                    <!-- panel body ends here  -->
												<div class="additionalpackage"></div>
												
														<!-- /.panel-group -->
												<div class="clearfix add-remove text-right">
													<a class="btn btn-primary btn-xs" role="button" id="addroom" onclick="add(this);"> 
														Add Guest
													</a> <a class="btn btn-danger  btn-xs remove_field" id="removeroom" role="button" onclick="remove_field(this)" disabled="">
														Remove Guest </a>

												</div>
												
                                  

                                    
                                </div>
                            </div> 
                             <!-- add rooms here -->
                        
				</div><div class="additionalrooms"><div class="panel panel-default" style="border: 1px solid #ccc;margin-bottom: 5px" id="roomslength">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#roomnested" href="#nested-room1">Room2</a>
                                    </h4>
                                </div>
                                <div id="nested-room1" class="panel-collapse">
                                    <div class="panel-body">
                                    	<div class="first-block-rooms clearfix">
												<div class="col-sm-4">

													<div class="form-group has-feedback">
														<label for="checkInDate" class=" control-label">Room
															Type <span id="mandatory"> * </span>
														</label> <input type="text" autocomplete="off" name="hotelOrderRoomInfoList[1].roomType" id="roomtype" class="form-control input-sm" value="" required="required" placeholder="Room Type">
															

													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group has-feedback">
														<label for="checkInDate" class="  control-label">Meal
															Type 
														</label> <input type="text" autocomplete="off" name="hotelOrderRoomInfoList[1].mealType" id="mealtype" class="form-control input-sm " placeholder="Meal Type"> 
															
													</div>
												</div>

												<div class="col-sm-4">
													<div class="form-group has-feedback">
														<label for="checkInDate" class=" control-label">Inclusions
															<span id="mandatory"> * </span>
														</label> <input type="text" autocomplete="off" required="required" name="hotelOrderRoomInfoList[1].inclusions" id="inclusions" class="form-control input-sm " placeholder="Inclusions" value="">
															


													</div>
												</div>

											</div>
                                  
                                  
                                  											<!-- nested -->

											<div class="panel-group" id="nested1">

												<div class="panel panel-default " style="margin-bottom: 5px;margin-top: 10px;" id="mainpanel">
												
													<div class="panel-heading">
														<h4 class="panel-title">
															<a class="collapsed" data-toggle="collapse" data-parent="#nested" href="#nestedPerson1" id="nested-collapseOne1"> Lead Person Details </a>
														</h4>
													</div>
													<!--/.panel-heading -->
													<div id="nestedPerson1" class="panel-collapse ">
														<div class="panel-body">
														
														<div class="detailswithRM">
															<div class="col-sm-1">
																<div class="form-group has-feedback">
																	<label for="checkInDate" class="control-label">Title
																		<span id="mandatory"> * </span>
																	</label> <select name="hotelOrderRoomInfoList[1].hotelOrderGuests[0].title" class="form-control input-sm" id="title1">
																		<option value="Mr" selected="selected">Mr</option>
													<option value="Mrs">Mrs</option>
													<option value="Miss">Miss</option>
													<option value="Ms">Ms</option>
													<option value="Master">Master</option>
																		
																		</select>
																		

																</div>
															</div>
															<div class="col-sm-2">

																<div class="form-group has-feedback">
																	<label for="checkInDate" class=" control-label">First
																		Name <span id="mandatory"> * </span>
																	</label> <input type="text" autocomplete="off" name="hotelOrderRoomInfoList[1].hotelOrderGuests[0].firstName" class="form-control input-sm firstName" required="required" value="" placeholder="First Name" id="firstName1"> <input type="hidden" name="hotelOrderRoomInfoList[1].hotelOrderGuests[0].leader" value="" id="NaN"> 
																		
																</div>


															</div>
															<input type="hidden" id="totalNumberOfPassenger" value="">
															<div class="col-sm-2"> 
																<div class="form-group has-feedback">
																	<label for="checkInDate" class=" control-label">Last
																		Name <span id="mandatory"> * </span>
																	</label> <input type="text" autocomplete="off" name="hotelOrderRoomInfoList[1].hotelOrderGuests[0].lastName" class="form-control input-sm " placeholder="Last Name" required="required" value="" id="lastName1">
																		

																</div>
															</div>
															<div class="col-sm-1"> 
																<div class="form-group has-feedback">
																	<label for="checkInDate" class=" control-label">GuestType
																		<span id="mandatory"> * </span>
																	</label> <select name="hotelOrderRoomInfoList[1].hotelOrderGuests[0].paxType" class="form-control input-sm">
																		<option selected="selected" value="Adult">Adult</option>
																		<option value="Infant">Infant</option>
																		<option value="Children">Children</option>

																	</select>
																	

																</div>
															</div>

															<div class="col-sm-2">
																<div class="form-group has-feedback">
																	<label for="checkInDate" class=" control-label">Nationality
																		<span id="mandatory"> * </span>
																	</label> <select name="hotelOrderRoomInfoList[1].hotelOrderGuests[0].nationality" class="form-control input-sm" required="">
																		
																			
																			<option value="Afghanistan">Afghanistan</option>
																		
																			
																			<option value="Albania">Albania</option>
																		
																			
																			<option value="Algeria">Algeria</option>
																		
																			
																			<option value="American Samoa">American Samoa</option>
																		
																			
																			<option value="Andorra">Andorra</option>
																		
																			
																			<option value="Angola">Angola</option>
																		
																			
																			<option value="Anguilla">Anguilla</option>
																		
																			
																			<option value="Antarctica">Antarctica</option>
																		
																			
																			<option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
																		
																			
																			<option value="Argentina">Argentina</option>
																		
																			
																			<option value="Armenia">Armenia</option>
																		
																			
																			<option value="Aruba">Aruba</option>
																		
																			
																			<option value="Australia">Australia</option>
																		
																			
																			<option value="Austria">Austria</option>
																		
																			
																			<option value="Azerbaijan">Azerbaijan</option>
																		
																			
																			<option value="Bahamas">Bahamas</option>
																		
																			
																			<option value="Bahrain">Bahrain</option>
																		
																			
																			<option value="Bangladesh">Bangladesh</option>
																		
																			
																			<option value="Barbados">Barbados</option>
																		
																			
																			<option value="Belarus">Belarus</option>
																		
																			
																			<option value="Belgium">Belgium</option>
																		
																			
																			<option value="Belize">Belize</option>
																		
																			
																			<option value="Benin">Benin</option>
																		
																			
																			<option value="Bermuda">Bermuda</option>
																		
																			
																			<option value="Bhutan">Bhutan</option>
																		
																			
																			<option value="Bolivia">Bolivia</option>
																		
																			
																			<option value="Bosnia">Bosnia</option>
																		
																			
																			<option value="Botswana">Botswana</option>
																		
																			
																			<option value="Bouvet Island">Bouvet Island</option>
																		
																			
																			<option value="Brazil">Brazil</option>
																		
																			
																			<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
																		
																			
																			<option value="British Virgin Islands">British Virgin Islands</option>
																		
																			
																			<option value="Brunei">Brunei</option>
																		
																			
																			<option value="Bulgaria">Bulgaria</option>
																		
																			
																			<option value="Burkina Faso">Burkina Faso</option>
																		
																			
																			<option value="Burundi">Burundi</option>
																		
																			
																			<option value="Cambodia">Cambodia</option>
																		
																			
																			<option value="Cameroon">Cameroon</option>
																		
																			
																			<option value="Canada">Canada</option>
																		
																			
																			<option value="Cape Verde">Cape Verde</option>
																		
																			
																			<option value="Caribbean Netherlands">Caribbean Netherlands</option>
																		
																			
																			<option value="Cayman Islands">Cayman Islands</option>
																		
																			
																			<option value="Central African Republic">Central African Republic</option>
																		
																			
																			<option value="Chad">Chad</option>
																		
																			
																			<option value="Chile">Chile</option>
																		
																			
																			<option value="China">China</option>
																		
																			
																			<option value="Christmas Island">Christmas Island</option>
																		
																			
																			<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
																		
																			
																			<option value="Colombia">Colombia</option>
																		
																			
																			<option value="Comoros">Comoros</option>
																		
																			
																			<option value="Congo - Brazzaville">Congo - Brazzaville</option>
																		
																			
																			<option value="Congo - Kinshasa">Congo - Kinshasa</option>
																		
																			
																			<option value="Cook Islands">Cook Islands</option>
																		
																			
																			<option value="Costa Rica">Costa Rica</option>
																		
																			
																			<option value="Croatia">Croatia</option>
																		
																			
																			<option value="Cuba">Cuba</option>
																		
																			
																			<option value="Curaçao">Curaçao</option>
																		
																			
																			<option value="Cyprus">Cyprus</option>
																		
																			
																			<option value="Czech Republic">Czech Republic</option>
																		
																			
																			<option value="Côte d’Ivoire">Côte d’Ivoire</option>
																		
																			
																			<option value="Denmark">Denmark</option>
																		
																			
																			<option value="Djibouti">Djibouti</option>
																		
																			
																			<option value="Dominica">Dominica</option>
																		
																			
																			<option value="Dominican Republic">Dominican Republic</option>
																		
																			
																			<option value="Ecuador">Ecuador</option>
																		
																			
																			<option value="Egypt">Egypt</option>
																		
																			
																			<option value="El Salvador">El Salvador</option>
																		
																			
																			<option value="Equatorial Guinea">Equatorial Guinea</option>
																		
																			
																			<option value="Eritrea">Eritrea</option>
																		
																			
																			<option value="Estonia">Estonia</option>
																		
																			
																			<option value="Ethiopia">Ethiopia</option>
																		
																			
																			<option value="Falkland Islands">Falkland Islands</option>
																		
																			
																			<option value="Faroe Islands">Faroe Islands</option>
																		
																			
																			<option value="Fiji">Fiji</option>
																		
																			
																			<option value="Finland">Finland</option>
																		
																			
																			<option value="France">France</option>
																		
																			
																			<option value="French Guiana">French Guiana</option>
																		
																			
																			<option value="French Polynesia">French Polynesia</option>
																		
																			
																			<option value="French Southern Territories">French Southern Territories</option>
																		
																			
																			<option value="Gabon">Gabon</option>
																		
																			
																			<option value="Gambia">Gambia</option>
																		
																			
																			<option value="Georgia">Georgia</option>
																		
																			
																			<option value="Germany">Germany</option>
																		
																			
																			<option value="Ghana">Ghana</option>
																		
																			
																			<option value="Gibraltar">Gibraltar</option>
																		
																			
																			<option value="Greece">Greece</option>
																		
																			
																			<option value="Greenland">Greenland</option>
																		
																			
																			<option value="Grenada">Grenada</option>
																		
																			
																			<option value="Guadeloupe">Guadeloupe</option>
																		
																			
																			<option value="Guam">Guam</option>
																		
																			
																			<option value="Guatemala">Guatemala</option>
																		
																			
																			<option value="Guernsey">Guernsey</option>
																		
																			
																			<option value="Guinea">Guinea</option>
																		
																			
																			<option value="Guinea-Bissau">Guinea-Bissau</option>
																		
																			
																			<option value="Guyana">Guyana</option>
																		
																			
																			<option value="Haiti">Haiti</option>
																		
																			
																			<option value="Heard &amp; McDonald Islands">Heard &amp; McDonald Islands</option>
																		
																			
																			<option value="Honduras">Honduras</option>
																		
																			
																			<option value="Hong Kong">Hong Kong</option>
																		
																			
																			<option value="Hungary">Hungary</option>
																		
																			
																			<option value="Iceland">Iceland</option>
																		
																			
																				<option selected="selected" value="India">India</option>
																			
																			<option value="India">India</option>
																		
																			
																			<option value="Indonesia">Indonesia</option>
																		
																			
																			<option value="Iran">Iran</option>
																		
																			
																			<option value="Iraq">Iraq</option>
																		
																			
																			<option value="Ireland">Ireland</option>
																		
																			
																			<option value="Isle of Man">Isle of Man</option>
																		
																			
																			<option value="Israel">Israel</option>
																		
																			
																			<option value="Italy">Italy</option>
																		
																			
																			<option value="Jamaica">Jamaica</option>
																		
																			
																			<option value="Japan">Japan</option>
																		
																			
																			<option value="Jersey">Jersey</option>
																		
																			
																			<option value="Jordan">Jordan</option>
																		
																			
																			<option value="Kazakhstan">Kazakhstan</option>
																		
																			
																			<option value="Kenya">Kenya</option>
																		
																			
																			<option value="Kiribati">Kiribati</option>
																		
																			
																			<option value="Kuwait">Kuwait</option>
																		
																			
																			<option value="Kyrgyzstan">Kyrgyzstan</option>
																		
																			
																			<option value="Laos">Laos</option>
																		
																			
																			<option value="Latvia">Latvia</option>
																		
																			
																			<option value="Lebanon">Lebanon</option>
																		
																			
																			<option value="Lesotho">Lesotho</option>
																		
																			
																			<option value="Liberia">Liberia</option>
																		
																			
																			<option value="Libya">Libya</option>
																		
																			
																			<option value="Liechtenstein">Liechtenstein</option>
																		
																			
																			<option value="Lithuania">Lithuania</option>
																		
																			
																			<option value="Luxembourg">Luxembourg</option>
																		
																			
																			<option value="Macau">Macau</option>
																		
																			
																			<option value="Macedonia">Macedonia</option>
																		
																			
																			<option value="Madagascar">Madagascar</option>
																		
																			
																			<option value="Malawi">Malawi</option>
																		
																			
																			<option value="Malaysia">Malaysia</option>
																		
																			
																			<option value="Maldives">Maldives</option>
																		
																			
																			<option value="Mali">Mali</option>
																		
																			
																			<option value="Malta">Malta</option>
																		
																			
																			<option value="Marshall Islands">Marshall Islands</option>
																		
																			
																			<option value="Martinique">Martinique</option>
																		
																			
																			<option value="Mauritania">Mauritania</option>
																		
																			
																			<option value="Mauritius">Mauritius</option>
																		
																			
																			<option value="Mayotte">Mayotte</option>
																		
																			
																			<option value="Mexico">Mexico</option>
																		
																			
																			<option value="Micronesia">Micronesia</option>
																		
																			
																			<option value="Moldova">Moldova</option>
																		
																			
																			<option value="Monaco">Monaco</option>
																		
																			
																			<option value="Mongolia">Mongolia</option>
																		
																			
																			<option value="Montenegro">Montenegro</option>
																		
																			
																			<option value="Montserrat">Montserrat</option>
																		
																			
																			<option value="Morocco">Morocco</option>
																		
																			
																			<option value="Mozambique">Mozambique</option>
																		
																			
																			<option value="Myanmar">Myanmar</option>
																		
																			
																			<option value="Namibia">Namibia</option>
																		
																			
																			<option value="Nauru">Nauru</option>
																		
																			
																			<option value="Nepal">Nepal</option>
																		
																			
																			<option value="Netherlands">Netherlands</option>
																		
																			
																			<option value="New Caledonia">New Caledonia</option>
																		
																			
																			<option value="New Zealand">New Zealand</option>
																		
																			
																			<option value="Nicaragua">Nicaragua</option>
																		
																			
																			<option value="Niger">Niger</option>
																		
																			
																			<option value="Nigeria">Nigeria</option>
																		
																			
																			<option value="Niue">Niue</option>
																		
																			
																			<option value="Norfolk Island">Norfolk Island</option>
																		
																			
																			<option value="North Korea">North Korea</option>
																		
																			
																			<option value="Northern Mariana Islands">Northern Mariana Islands</option>
																		
																			
																			<option value="Norway">Norway</option>
																		
																			
																			<option value="Oman">Oman</option>
																		
																			
																			<option value="Pakistan">Pakistan</option>
																		
																			
																			<option value="Palau">Palau</option>
																		
																			
																			<option value="Palestine">Palestine</option>
																		
																			
																			<option value="Panama">Panama</option>
																		
																			
																			<option value="Papua New Guinea">Papua New Guinea</option>
																		
																			
																			<option value="Paraguay">Paraguay</option>
																		
																			
																			<option value="Peru">Peru</option>
																		
																			
																			<option value="Philippines">Philippines</option>
																		
																			
																			<option value="Pitcairn Islands">Pitcairn Islands</option>
																		
																			
																			<option value="Poland">Poland</option>
																		
																			
																			<option value="Portugal">Portugal</option>
																		
																			
																			<option value="Puerto Rico">Puerto Rico</option>
																		
																			
																			<option value="Qatar">Qatar</option>
																		
																			
																			<option value="Romania">Romania</option>
																		
																			
																			<option value="Russia">Russia</option>
																		
																			
																			<option value="Rwanda">Rwanda</option>
																		
																			
																			<option value="Réunion">Réunion</option>
																		
																			
																			<option value="Samoa">Samoa</option>
																		
																			
																			<option value="San Marino">San Marino</option>
																		
																			
																			<option value="Saudi Arabia">Saudi Arabia</option>
																		
																			
																			<option value="Senegal">Senegal</option>
																		
																			
																			<option value="Serbia">Serbia</option>
																		
																			
																			<option value="Seychelles">Seychelles</option>
																		
																			
																			<option value="Sierra Leone">Sierra Leone</option>
																		
																			
																			<option value="Singapore">Singapore</option>
																		
																			
																			<option value="Sint Maarten">Sint Maarten</option>
																		
																			
																			<option value="Slovakia">Slovakia</option>
																		
																			
																			<option value="Slovenia">Slovenia</option>
																		
																			
																			<option value="Solomon Islands">Solomon Islands</option>
																		
																			
																			<option value="Somalia">Somalia</option>
																		
																			
																			<option value="South Africa">South Africa</option>
																		
																			
																			<option value="South Georgia &amp; South Sandwich Islands">South Georgia &amp; South Sandwich Islands</option>
																		
																			
																			<option value="South Korea">South Korea</option>
																		
																			
																			<option value="South Sudan">South Sudan</option>
																		
																			
																			<option value="Spain">Spain</option>
																		
																			
																			<option value="Sri Lanka">Sri Lanka</option>
																		
																			
																			<option value="St. Barthélemy">St. Barthélemy</option>
																		
																			
																			<option value="St. Helena">St. Helena</option>
																		
																			
																			<option value="St. Kitts &amp; Nevis">St. Kitts &amp; Nevis</option>
																		
																			
																			<option value="St. Lucia">St. Lucia</option>
																		
																			
																			<option value="St. Martin">St. Martin</option>
																		
																			
																			<option value="St. Pierre &amp; Miquelon">St. Pierre &amp; Miquelon</option>
																		
																			
																			<option value="St. Vincent &amp; Grenadines">St. Vincent &amp; Grenadines</option>
																		
																			
																			<option value="Sudan">Sudan</option>
																		
																			
																			<option value="Suriname">Suriname</option>
																		
																			
																			<option value="Svalbard &amp; Jan Mayen">Svalbard &amp; Jan Mayen</option>
																		
																			
																			<option value="Swaziland">Swaziland</option>
																		
																			
																			<option value="Sweden">Sweden</option>
																		
																			
																			<option value="Switzerland">Switzerland</option>
																		
																			
																			<option value="Syria">Syria</option>
																		
																			
																			<option value="São Tomé &amp; Príncipe">São Tomé &amp; Príncipe</option>
																		
																			
																			<option value="Taiwan">Taiwan</option>
																		
																			
																			<option value="Tajikistan">Tajikistan</option>
																		
																			
																			<option value="Tanzania">Tanzania</option>
																		
																			
																			<option value="Thailand">Thailand</option>
																		
																			
																			<option value="Timor-Leste">Timor-Leste</option>
																		
																			
																			<option value="Togo">Togo</option>
																		
																			
																			<option value="Tokelau">Tokelau</option>
																		
																			
																			<option value="Tonga">Tonga</option>
																		
																			
																			<option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
																		
																			
																			<option value="Tunisia">Tunisia</option>
																		
																			
																			<option value="Turkey">Turkey</option>
																		
																			
																			<option value="Turkmenistan">Turkmenistan</option>
																		
																			
																			<option value="Turks &amp; Caicos Islands">Turks &amp; Caicos Islands</option>
																		
																			
																			<option value="Tuvalu">Tuvalu</option>
																		
																			
																			<option value="U.S. Outlying Islands">U.S. Outlying Islands</option>
																		
																			
																			<option value="U.S. Virgin Islands">U.S. Virgin Islands</option>
																		
																			
																			<option value="UK">UK</option>
																		
																			
																			<option value="US">US</option>
																		
																			
																			<option value="Uganda">Uganda</option>
																		
																			
																			<option value="Ukraine">Ukraine</option>
																		
																			
																			<option value="United Arab Emirates">United Arab Emirates</option>
																		
																			
																			<option value="Uruguay">Uruguay</option>
																		
																			
																			<option value="Uzbekistan">Uzbekistan</option>
																		
																			
																			<option value="Vanuatu">Vanuatu</option>
																		
																			
																			<option value="Vatican City">Vatican City</option>
																		
																			
																			<option value="Venezuela">Venezuela</option>
																		
																			
																			<option value="Vietnam">Vietnam</option>
																		
																			
																			<option value="Wallis &amp; Futuna">Wallis &amp; Futuna</option>
																		
																			
																			<option value="Western Sahara">Western Sahara</option>
																		
																			
																			<option value="Yemen">Yemen</option>
																		
																			
																			<option value="Zambia">Zambia</option>
																		
																			
																			<option value="Zimbabwe">Zimbabwe</option>
																		
																			
																			<option value="Åland Islands">Åland Islands</option>
 																	</select>
																</div>
															</div>



															<div class="col-sm-2">
																<div class="form-group has-feedback">
																	<label for="checkInDate" class="col-sm-2 control-label">Age
																	</label> <input type="number" autocomplete="off" name="hotelOrderRoomInfoList[1].hotelOrderGuests[0].age" maxlength="3" class="form-control input-sm cusValidationforprice" placeholder="Age" id="NaN">
																</div>
															</div>

															<div class="col-sm-2">

																<div class="form-group has-feedback">
																	<label for="checkInDate" class="col-sm-2 control-label">Email
																	</label> <input type="email" autocomplete="off" id="emailId1" name="hotelOrderRoomInfoList[1].hotelOrderGuests[0].email" value="" class="form-control input-sm" placeholder="Email">
																</div>
															</div> 
														</div>
														<!--/.panel-body -->
													</div>
													<!--/.panel-collapse -->
												</div>
												<!-- /.panel -->
													</div> 
											<!-- nested --> 
											 
                                    </div>
                                    <!-- panel body ends here  -->
												<div class="additionalpackage1"></div>
												
														<!-- /.panel-group -->
												<div class="clearfix add-remove text-right">
													<a class="btn btn-primary btn-xs" role="button" id="addroom1" onclick="add(this);"> 
														Add Guest
													</a> <a class="btn btn-danger  btn-xs remove_field" id="removeroom1" role="button" onclick="remove_field(this)" disabled="">
														Remove Guest </a>
												</div>
                                </div>
                            </div> 
                             <!-- add rooms here -->
				</div></div> 
				<div class="clearfix add-remove">
					<a class="btn btn-primary  btn-xs" role="button" id="addrooms" onclick="addrooms(this);"> <!-- onclick="add() onclick="remove_field()" -->
						Add rooms
					</a> <a class="btn btn-danger  btn-xs remove_room" id="removerooms" role="button" onclick="remove_rooms(this)" disabled="">
						Remove Room </a>
				</div>
				</div> 
                </div>
                                    </div>
                                </div>
                                </div>
                                </div> 
                                <div class="col-md-12">
 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse04" aria-expanded="false" aria-controls="collapseFour">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingFour">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Supplier Amount Payable</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse04" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="">
                                        <div class="panel-body">
										<div class="col-sm-2">
										<div class="form-group">
												<label for="NetRate" class=" control-label" style="color: red;"><b>Selected ManagementFee</b> </label> 
													<input type="text" style="color: red; font-size: 18px" disabled="disabled" id="quotedAmount" value="" class="form-control input-sm">
												</div>
											</div>
										
										
											

										<div class="col-sm-2">
											<div class="form-group has-feedback">
												<label for="Vendor" class=" control-label">Supplier
													Name <span id="mandatory"> * </span>
												</label>
												
													<select class="form-control input-sm" name="hotelOrderRow.apiProvoder" id="supplierName" required="">
														<option value="" selected="selected">select
															Supplier</option>
														
															<option value="Desia">Desia</option>
														
															<option value="TBO">TBO</option>
														
													</select>
													 
													
												</div>
											</div>
											
											<div class="col-sm-2"> 
											<div class="form-group has-feedback">
												<label for="NetRate" class=" control-label">Supplier
													Order Reference <span id="mandatory"> * </span>
												</label>
												 
													<input type="text" autocomplete="off" name="hotelOrderRow.orderReference" class="form-control input-sm " placeholder="Type Order Refernce" id="orderReference" required="">
														 
												</div>
											</div>
											
										<div class="col-sm-2"> 
											<div class="form-group has-feedback">
												<label for="cancelPolicy" class=" control-label">Cancelation Policy<span id="mandatory"> * </span>
												</label>
												 
													<input type="text" autocomplete="off" name="hotelOrderRow.cancelPolicy" class="form-control input-sm " placeholder="Type Cancellation Policy" id="cancelPolicy" required="required">
														 
												</div>
											</div>
										<div class="">
										<div class="col-sm-2"> 
											<div class="form-group has-feedback ">
												<label for="NetRate" class=" control-label">Base
													Price <span id="mandatory"> * </span>
												</label>
												 
													<input type="text" autocomplete="off" name="hotelOrderRow.apiPrice" class="form-control input-sm cusValidationforprice required baseFareprice digitOnly" required="required" placeholder="netRate" id="basePrice" value="0" onchange="numbersonly(this);">
														 
												</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group has-feedback">
												<label for="TxnCharge" class=" control-label">Tax
													<span id="mandatory"> * </span>
												</label>
												 
													<input type="text" autocomplete="off" name="hotelOrderRow.taxes" class="form-control input-sm cusValidationforprice digitOnly" required="required" placeholder="Tax" id="tax" value="0" onchange="numbersonly(this);">
														 
												</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group has-feedback">
												<label for="NetRate" class="  control-label">Mark
													up <span id="mandatory"> * </span>
												</label>
												 
													<input type="text" autocomplete="off" value="0" name="hotelOrderRow.markupAmount" class="form-control input-sm  cusValidationforprice required digitOnly" id="markup" required="required" placeholder="mark up" onchange="numbersonly(this);">
														 
												</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="NetRate" class=" control-label">Management Fee <span id="mandatory"> * </span>
												</label>
												 
													<input type="text" autocomplete="off" value="0" name="hotelOrderRow.tempManagementFees" class="form-control input-sm" id="managementFee" required="required" placeholder="Management Fee" readonly="readonly">
												</div>
											</div>
				
 
												 
													<input type="hidden" autocomplete="off" name="hotelOrderRow.discountAmount" value="0" class="form-control input-sm" id="discountAmount" required="required" placeholder="Discount Amount">
													<input type="hidden" autocomplete="off" name="hotelOrderRow.feeAmount" class="form-control input-sm" value="0">
												</div>
											 
											
											
											 
											 
												<div class="col-sm-12 clearfix">
												<div class="form-group" style="color: red;display: none" id="balanceCheck">
												<h5 style="color: red">Total Booking Amount is more than Deposit Or Wallet Balances.Please Check.</h5>
											</div>
											</div>
									<div class="col-sm-2"> 
										<div class="form-group has-feedback">
												<label for="currency" class="  control-label">Supplier
													Currency <span id="mandatory"> * </span>
												</label>
												 
													<select class="form-control input-sm" required="required" name="hotelOrderRow.apiCurrency">
														<option selected="selected" value=""></option>
														
										
										<option value="AFN">AFN</option>
									
										
										<option value="ALL">ALL</option>
									
										
										<option value="DZD">DZD</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="AOA">AOA</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value=""></option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="ARS">ARS</option>
									
										
										<option value="AMD">AMD</option>
									
										
										<option value="AWG">AWG</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="AZN">AZN</option>
									
										
										<option value="BSD">BSD</option>
									
										
										<option value="BHD">BHD</option>
									
										
										<option value="BDT">BDT</option>
									
										
										<option value="BBD">BBD</option>
									
										
										<option value="BYR">BYR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="BZD">BZD</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="BMD">BMD</option>
									
										
											<option selected="selected" value="INR">INR</option>
										
										<option value="INR">INR</option>
									
										
										<option value="BOB">BOB</option>
									
										
										<option value="BAM">BAM</option>
									
										
										<option value="BWP">BWP</option>
									
										
										<option value=""></option>
									
										
										<option value="BRL">BRL</option>
									
										
										<option value=""></option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="BND">BND</option>
									
										
										<option value="BGN">BGN</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="BIF">BIF</option>
									
										
										<option value="KHR">KHR</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="CAD">CAD</option>
									
										
										<option value="CVE">CVE</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="KYD">KYD</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="CLP">CLP</option>
									
										
										<option value="CNY">CNY</option>
									
										
										<option value=""></option>
									
										
										<option value=""></option>
									
										
										<option value="COP">COP</option>
									
										
										<option value="KMF">KMF</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value=""></option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="CRC">CRC</option>
									
										
										<option value="HRK">HRK</option>
									
										
										<option value="CUP">CUP</option>
									
										
										<option value="ANG">ANG</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value=""></option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="DKK">DKK</option>
									
										
										<option value="DJF">DJF</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="DOP">DOP</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="EGP">EGP</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="ERN">ERN</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="ETB">ETB</option>
									
										
										<option value="FKP">FKP</option>
									
										
										<option value=""></option>
									
										
										<option value="FJD">FJD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="XPF">XPF</option>
									
										
										<option value=""></option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="GMD">GMD</option>
									
										
										<option value="GEL">GEL</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="GHS">GHS</option>
									
										
										<option value="GIP">GIP</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="DKK">DKK</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="GTQ">GTQ</option>
									
										
										<option value="GBP">GBP</option>
									
										
										<option value="GNF">GNF</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="GYD">GYD</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value=""></option>
									
										
										<option value="HNL">HNL</option>
									
										
										<option value=""></option>
									
										
										<option value="HUF">HUF</option>
									
										
										<option value="ISK">ISK</option>
									
										
											<option selected="selected" value="INR">INR</option>
										
										<option value="INR">INR</option>
									
										
										<option value="IDR">IDR</option>
									
										
										<option value="IRR">IRR</option>
									
										
										<option value="IQD">IQD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="GBP">GBP</option>
									
										
										<option value="ILS">ILS</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="JMD">JMD</option>
									
										
										<option value="JPY">JPY</option>
									
										
										<option value="GBP">GBP</option>
									
										
										<option value="JOD">JOD</option>
									
										
										<option value="KZT">KZT</option>
									
										
										<option value="KES">KES</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value="KWD">KWD</option>
									
										
										<option value="KGS">KGS</option>
									
										
										<option value="LAK">LAK</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="LBP">LBP</option>
									
										
										<option value="ZAR">ZAR</option>
									
										
										<option value="LRD">LRD</option>
									
										
										<option value="LYD">LYD</option>
									
										
										<option value="CHF">CHF</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="MOP">MOP</option>
									
										
										<option value="MKD">MKD</option>
									
										
										<option value="MGA">MGA</option>
									
										
										<option value="MWK">MWK</option>
									
										
										<option value="MYR">MYR</option>
									
										
										<option value="MVR">MVR</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="MRO">MRO</option>
									
										
										<option value="MUR">MUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="MXN">MXN</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="MDL">MDL</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="MNT">MNT</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="MAD">MAD</option>
									
										
										<option value="MZN">MZN</option>
									
										
										<option value="MMK">MMK</option>
									
										
										<option value="ZAR">ZAR</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value="NPR">NPR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="XPF">XPF</option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="NIO">NIO</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="NGN">NGN</option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value="KPW">KPW</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="NOK">NOK</option>
									
										
										<option value="OMR">OMR</option>
									
										
										<option value="PKR">PKR</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value=""></option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="PGK">PGK</option>
									
										
										<option value="PYG">PYG</option>
									
										
										<option value="PEN">PEN</option>
									
										
										<option value="PHP">PHP</option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="PLN">PLN</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="QAR">QAR</option>
									
										
										<option value="RON">RON</option>
									
										
										<option value="RUB">RUB</option>
									
										
										<option value="RWF">RWF</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="WST">WST</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="SAR">SAR</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="RSD">RSD</option>
									
										
										<option value="SCR">SCR</option>
									
										
										<option value="SLL">SLL</option>
									
										
										<option value="SGD">SGD</option>
									
										
										<option value="ANG">ANG</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="SBD">SBD</option>
									
										
										<option value="SOS">SOS</option>
									
										
										<option value="ZAR">ZAR</option>
									
										
										<option value=""></option>
									
										
										<option value="KRW">KRW</option>
									
										
										<option value="SSP">SSP</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="LKR">LKR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="SHP">SHP</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="SDG">SDG</option>
									
										
										<option value="SRD">SRD</option>
									
										
										<option value="NOK">NOK</option>
									
										
										<option value="SZL">SZL</option>
									
										
										<option value="SEK">SEK</option>
									
										
										<option value="CHF">CHF</option>
									
										
										<option value="SYP">SYP</option>
									
										
										<option value="STD">STD</option>
									
										
										<option value=""></option>
									
										
										<option value="TJS">TJS</option>
									
										
										<option value="TZS">TZS</option>
									
										
										<option value="THB">THB</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="TOP">TOP</option>
									
										
										<option value="TTD">TTD</option>
									
										
										<option value="TND">TND</option>
									
										
										<option value="TRY">TRY</option>
									
										
										<option value="TMT">TMT</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value=""></option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="GBP">GBP</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="UGX">UGX</option>
									
										
										<option value="UAH">UAH</option>
									
										
										<option value="AED">AED</option>
									
										
										<option value="UYU">UYU</option>
									
										
										<option value="UZS">UZS</option>
									
										
										<option value="VUV">VUV</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="VEF">VEF</option>
									
										
										<option value="VND">VND</option>
									
										
										<option value="XPF">XPF</option>
									
										
										<option value="MAD">MAD</option>
									
										
										<option value="YER">YER</option>
									
										
										<option value="ZMW">ZMW</option>
									
										
										<option value="ZWL">ZWL</option>
									
										
										<option value="EUR">EUR</option>
									

													</select>
													 
												</div>
									</div>
										
											<!-- <div class="form-group"> -->
												
												<!-- <div class="col-sm-8"> -->
													<input type="hidden" class="form-control input-sm" required="required" placeholder="Conversion Rate" name="hotelOrderRow.apiToBaseExchangeRate" value="1">
												<!-- </div>
											</div> -->
<div class="col-sm-2"> 
											<div class="form-group ">
												<label for="currency" class="  control-label">Supplier
													PaymentType <span id="mandatory"> * </span>
												</label>
												 
													<select class="form-control input-sm" id="supplierPaymentType" name="supplierPaymentType">
														<option value="Full" selected="selected">Full Payment</option>
														<option value="Partial">Partial Payment</option>
														<option selected="selected" value="Zero">No Payment</option>
													</select>
												</div> 
											</div>
											
											<div class="col-sm-1" style="margin-top: 25px;">
													<a class="btn btn-info btn-sm btn-block" data-toggle="modal" href="#suppliermyNotification"><i class="fa fa-bell-o fa-sm"></i> Reminder</a>
												</div>
											<div class="col-sm-2 supplierPayment" style="display: none"> 
											<div class="form-group  ">
												<label for="currency" class="  control-label">Amount</label>
												 
													<input type="text" class="form-control input-sm" placeholder="amount" name="paymentTransaction.apiProviderAmount" id="supplierAmount">
												</div>

											</div>
											<div class="col-sm-2 supplierPayment" style="display: none"> 
											<div class="form-group ">
												<label for="currency" class=" control-label">Paid
													By</label>
												 
													<input type="text" class="form-control input-sm" placeholder="Paid By" name="paymentTransaction.paidBySupplier">
												</div>

											</div> 
<div class="col-sm-2 no-payment-mode" style="display: none;"> 
											<div class="form-group   ">
												<label for="currency" class="  control-label">Payment
													Mode <span id="mandatory"> * </span>
												</label>
													<select class="form-control input-sm" id="supplierPaymentMethod" name="apiProviderPaymentTransaction.payment_method">
														<option value="card">CARD</option>
														<option value="cash">CASH</option>
														<option value="online">ONLINE</option>
													</select>
												</div>
											</div>

							<div class="col-sm-2 "> 
											<div class="form-group ">
												<label for="currency" class="  control-label">Comments
												</label>
													<textarea class="form-control input-sm" placeholder="Type text" name="apiProviderPaymentTransaction.response_message"></textarea>
												</div>
											</div>

							<div class="col-sm-2"> 
											<div class="form-group has-feedback card-details" style="display: none;">
												<label for="currency" class=" control-label">CardHolderName
													<span id="mandatory"> * </span>
												</label>
													<select class="form-control input-sm" name="paymentTransaction.supplierCardHolderId">
														<option value="">Select card holder name</option>
													</select>
													 
												</div>
											</div>

										</div>
                                    </div>
                                </div>
                                </div>
                                </div> 
                                <div class="col-md-12">
 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse05" aria-expanded="false" aria-controls="collapseFive">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingFive">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Client Amount Receivables</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse05" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive" aria-expanded="false" style="">
                                        <div class="panel-body">

						
										
										
					<div class="col-sm-2">				
										
						<div class="form-group">
												<label for="NetRate" class="  control-label" style="color: red; font-size: 12px"><b>Total 
														Amount</b> </label>
												 
													<input type="text" style="color: red; font-size: 18px" disabled="disabled" id="quotedAmount1" value="" class="form-control input-sm">
												</div>
											</div>
											<div class="col-sm-2"> 
											<input type="hidden" id="taxType" value="GST">
											<input type="hidden" autocomplete="off" name="totalGstTax" onchange="numbersonly(this)" class="form-control input-sm" id="gstTax" value="0.0">
											<div class="form-group">
												<label for="totalGstTax">
													Gst Tax Amount</label> 
													<div class="inner-addon right-addon">
      												<span class="glyphicon">@ <span id="gstTaxPer"></span>%</span>
													<input type="text" autocomplete="off" name="totalGstTaxAmount" onchange="numbersonly(this)" class="form-control input-sm" id="gstTaxAmount" value="0.0" required="" readonly="readonly">
													</div>
											</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="totInvoiceAmount">Invoice
													Amount </label> <input type="text" autocomplete="off" onclick="numbersonly(this);addtax();" name="totInvoiceAmount" id="invoiceamount" class="form-control input-sm" placeholder="Invoice Amount" value="0" required="" readonly="readonly">
											</div>
											</div>
											<!-- <div class="col-sm-2"> 
											<div class="form-group">
												<label for="" >
													CGST  </label> <input type="text" autocomplete="off"
													name="" id="cgstamount"
													class="form-control input-sm" placeholder="CGST"
													value="0" required readonly="readonly">
											</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="" >
													IGST  </label> <input type="text" autocomplete="off"
													name="" id="igstamount"
													class="form-control input-sm" placeholder="CGST"
													value="0" required readonly="readonly">
											</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="" >
													SGST  </label> <input type="text" autocomplete="off"
													name="" id="sgstamount"
													class="form-control input-sm" placeholder="CGST"
													value="0" required readonly="readonly">
											</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="" >
													UGST  </label> <input type="text" autocomplete="off"
													name="" id="ugstamount"
													class="form-control input-sm" placeholder="UGST"
													value="0" required readonly="readonly">
											</div>
											</div> -->
											<div class="col-sm-2">
												<div class="form-group has-feedback">
												<label for="currency" class=" control-label">Booking
													Currency <span id="mandatory"> * </span>
												</label>
												 
													<select class="form-control input-sm" required="required" name="hotelOrderRow.bookingCurrency">
														
										
										<option value="AFN">AFN</option>
									
										
										<option value="ALL">ALL</option>
									
										
										<option value="DZD">DZD</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="AOA">AOA</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value=""></option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="ARS">ARS</option>
									
										
										<option value="AMD">AMD</option>
									
										
										<option value="AWG">AWG</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="AZN">AZN</option>
									
										
										<option value="BSD">BSD</option>
									
										
										<option value="BHD">BHD</option>
									
										
										<option value="BDT">BDT</option>
									
										
										<option value="BBD">BBD</option>
									
										
										<option value="BYR">BYR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="BZD">BZD</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="BMD">BMD</option>
									
										
											<option selected="selected" value="INR">INR</option>
										
										<option value="INR">INR</option>
									
										
										<option value="BOB">BOB</option>
									
										
										<option value="BAM">BAM</option>
									
										
										<option value="BWP">BWP</option>
									
										
										<option value=""></option>
									
										
										<option value="BRL">BRL</option>
									
										
										<option value=""></option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="BND">BND</option>
									
										
										<option value="BGN">BGN</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="BIF">BIF</option>
									
										
										<option value="KHR">KHR</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="CAD">CAD</option>
									
										
										<option value="CVE">CVE</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="KYD">KYD</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="CLP">CLP</option>
									
										
										<option value="CNY">CNY</option>
									
										
										<option value=""></option>
									
										
										<option value=""></option>
									
										
										<option value="COP">COP</option>
									
										
										<option value="KMF">KMF</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value=""></option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="CRC">CRC</option>
									
										
										<option value="HRK">HRK</option>
									
										
										<option value="CUP">CUP</option>
									
										
										<option value="ANG">ANG</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value=""></option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="DKK">DKK</option>
									
										
										<option value="DJF">DJF</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="DOP">DOP</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="EGP">EGP</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="ERN">ERN</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="ETB">ETB</option>
									
										
										<option value="FKP">FKP</option>
									
										
										<option value=""></option>
									
										
										<option value="FJD">FJD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="XPF">XPF</option>
									
										
										<option value=""></option>
									
										
										<option value="XAF">XAF</option>
									
										
										<option value="GMD">GMD</option>
									
										
										<option value="GEL">GEL</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="GHS">GHS</option>
									
										
										<option value="GIP">GIP</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="DKK">DKK</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="GTQ">GTQ</option>
									
										
										<option value="GBP">GBP</option>
									
										
										<option value="GNF">GNF</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="GYD">GYD</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value=""></option>
									
										
										<option value="HNL">HNL</option>
									
										
										<option value=""></option>
									
										
										<option value="HUF">HUF</option>
									
										
										<option value="ISK">ISK</option>
									
										
											<option selected="selected" value="INR">INR</option>
										
										<option value="INR">INR</option>
									
										
										<option value="IDR">IDR</option>
									
										
										<option value="IRR">IRR</option>
									
										
										<option value="IQD">IQD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="GBP">GBP</option>
									
										
										<option value="ILS">ILS</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="JMD">JMD</option>
									
										
										<option value="JPY">JPY</option>
									
										
										<option value="GBP">GBP</option>
									
										
										<option value="JOD">JOD</option>
									
										
										<option value="KZT">KZT</option>
									
										
										<option value="KES">KES</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value="KWD">KWD</option>
									
										
										<option value="KGS">KGS</option>
									
										
										<option value="LAK">LAK</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="LBP">LBP</option>
									
										
										<option value="ZAR">ZAR</option>
									
										
										<option value="LRD">LRD</option>
									
										
										<option value="LYD">LYD</option>
									
										
										<option value="CHF">CHF</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="MOP">MOP</option>
									
										
										<option value="MKD">MKD</option>
									
										
										<option value="MGA">MGA</option>
									
										
										<option value="MWK">MWK</option>
									
										
										<option value="MYR">MYR</option>
									
										
										<option value="MVR">MVR</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="MRO">MRO</option>
									
										
										<option value="MUR">MUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="MXN">MXN</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="MDL">MDL</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="MNT">MNT</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="MAD">MAD</option>
									
										
										<option value="MZN">MZN</option>
									
										
										<option value="MMK">MMK</option>
									
										
										<option value="ZAR">ZAR</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value="NPR">NPR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="XPF">XPF</option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="NIO">NIO</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="NGN">NGN</option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value="KPW">KPW</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="NOK">NOK</option>
									
										
										<option value="OMR">OMR</option>
									
										
										<option value="PKR">PKR</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value=""></option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="PGK">PGK</option>
									
										
										<option value="PYG">PYG</option>
									
										
										<option value="PEN">PEN</option>
									
										
										<option value="PHP">PHP</option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="PLN">PLN</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="QAR">QAR</option>
									
										
										<option value="RON">RON</option>
									
										
										<option value="RUB">RUB</option>
									
										
										<option value="RWF">RWF</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="WST">WST</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="SAR">SAR</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="RSD">RSD</option>
									
										
										<option value="SCR">SCR</option>
									
										
										<option value="SLL">SLL</option>
									
										
										<option value="SGD">SGD</option>
									
										
										<option value="ANG">ANG</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="SBD">SBD</option>
									
										
										<option value="SOS">SOS</option>
									
										
										<option value="ZAR">ZAR</option>
									
										
										<option value=""></option>
									
										
										<option value="KRW">KRW</option>
									
										
										<option value="SSP">SSP</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="LKR">LKR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="SHP">SHP</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="XCD">XCD</option>
									
										
										<option value="SDG">SDG</option>
									
										
										<option value="SRD">SRD</option>
									
										
										<option value="NOK">NOK</option>
									
										
										<option value="SZL">SZL</option>
									
										
										<option value="SEK">SEK</option>
									
										
										<option value="CHF">CHF</option>
									
										
										<option value="SYP">SYP</option>
									
										
										<option value="STD">STD</option>
									
										
										<option value=""></option>
									
										
										<option value="TJS">TJS</option>
									
										
										<option value="TZS">TZS</option>
									
										
										<option value="THB">THB</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="XOF">XOF</option>
									
										
										<option value="NZD">NZD</option>
									
										
										<option value="TOP">TOP</option>
									
										
										<option value="TTD">TTD</option>
									
										
										<option value="TND">TND</option>
									
										
										<option value="TRY">TRY</option>
									
										
										<option value="TMT">TMT</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="AUD">AUD</option>
									
										
										<option value=""></option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="GBP">GBP</option>
									
										
										<option value="USD">USD</option>
									
										
										<option value="UGX">UGX</option>
									
										
										<option value="UAH">UAH</option>
									
										
										<option value="AED">AED</option>
									
										
										<option value="UYU">UYU</option>
									
										
										<option value="UZS">UZS</option>
									
										
										<option value="VUV">VUV</option>
									
										
										<option value="EUR">EUR</option>
									
										
										<option value="VEF">VEF</option>
									
										
										<option value="VND">VND</option>
									
										
										<option value="XPF">XPF</option>
									
										
										<option value="MAD">MAD</option>
									
										
										<option value="YER">YER</option>
									
										
										<option value="ZMW">ZMW</option>
									
										
										<option value="ZWL">ZWL</option>
									
										
										<option value="EUR">EUR</option>
									
													</select>
													<span class="form-control-feedback glyphicon glyphicon-ok"></span>
												</div>
													<input type="hidden" value="1" class="form-control input-sm" required="required" placeholder="Conversion Rate" name="hotelOrderRow.baseToBookingExchangeRate">
											</div> 
											
												<!-- <div class="col-sm-8"> -->
													<!-- <input type="hidden" value="1" class="form-control input-sm"
														required="required" placeholder="Conversion Rate"
														name="hotelOrderRow.baseToBookingExchangeRate"> -->
												<!-- </div>
											</div> -->
										<div class="col-sm-2">
											<div class="form-group has-feedback">
												<label for="NetRate" class=" control-label">Booking
													Status <span id="mandatory"> * </span>
												</label>
												 
													<select class="form-control input-sm" required="required" name="hotelOrderRow.statusAction">
														<option value="Confirmed">Confirmed</option>
														<!-- <option value="Pending">Pending</option>  @yogesh told to do this
														<option value="Reserved">Reserved</option>
														<option value="Failed">Failed</option> -->
													</select>
													<span class="form-control-feedback glyphicon glyphicon-ok"></span>
												</div>
											</div>
											<div class="col-sm-2">
											<div class="form-group has-feedback">
												<label for="currency" class=" control-label">Customer
													PaymentType <span id="mandatory"> * </span>
												</label>
												 
													<select class="form-control input-sm" name="customerPaymentType" id="clientPaymentType">
														<option value="Full" selected="selected">Full Payment</option>
														<option value="Partial">Partial Payment</option>
														<option selected="selected" value="Zero">No Payment</option>
													</select>
													<span class="form-control-feedback glyphicon glyphicon-ok"></span>
												</div>
												
											</div>
											
											<div class="col-sm-2">
											<div class="form-group has-feedback">
												<label for="discount" class=" control-label">Discount<span id="mandatory"> * </span>
												</label>
													<input type="text" autocomplete="off" value="0.0" name="hotelOrderRow.discount" class="form-control input-sm" id="discount" required="required" placeholder="Discount" readonly="readonly"> 
													<span class="form-control-feedback glyphicon glyphicon-ok"></span>
												</div>
												
											</div>
											
											<div class="col-sm-1" style="margin-top: 25px;">
													<a class="btn btn-info btn-sm btn-block" data-toggle="modal" href="#clientmyNotification"><i class="fa fa-bell-o fa-sm"></i>Reminder</a>
												</div>
											<div class="col-sm-2 clientPayment" style="display: none;">
												<div class="form-group has-feedback ">
													<label for="currency" class=" control-label">Amount</label>
													 
														<input type="text" class="form-control input-sm" placeholder="amount" name="paymentTransaction.clientAmount" id="clientAmount">
															<span class="form-control-feedback glyphicon glyphicon-ok"></span>
													</div>
												</div>
											 
										<div class="col-sm-2 clientPayment" style="display: none;">
											<div class="form-group has-feedback ">
												<label for="currency" class=" control-label">Paid
													By</label>
												 
													<input type="text" class="form-control input-sm" placeholder="Paid By" name="paymentTransaction.paidByClient">
														<span class="form-control-feedback glyphicon glyphicon-ok"></span>
												</div>
											</div> 
											<div class="col-sm-2 no-client-payment-mode" style="display: none;">
												<div class="form-group has-feedback  ">
													<label for="currency" class=" control-label">Payment
														Mode <span id="mandatory"> * </span>
													</label> 
													<select class="form-control input-sm" id="clientPaymentMethod" name="paymentTransaction.payment_method">
														<option value="card">CARD</option>
														<option value="cash" selected="selected">CASH</option>
														<option value="online">ONLINE</option>

													</select>
													<span class="form-control-feedback glyphicon glyphicon-ok"></span>
												</div>
											</div>
											<div class="col-sm-2 client-comments" style="">
											<div class="form-group has-feedback">
												<label for="currency" class=" control-label">Comments
													
												</label>
												 
													<textarea rows="1" class="form-control input-sm" placeholder="Type text" id="comments" name="paymentTransaction.response_message"></textarea>
														<span class="form-control-feedback glyphicon glyphicon-ok"></span>
												</div>
											</div>

											<div class="col-sm-2 client-card-details" style="display: none;">
												<div class="form-group has-feedback ">
												<label for="currency" class=" control-label">CardHolderName
													<span id="mandatory"> * </span>
												</label> 
													<select class="form-control input-sm" name="paymentTransaction.clientCardHolderId">
														<option value="">Select card holder name</option>
														 
													</select>
													<span class="form-control-feedback glyphicon glyphicon-ok"></span>
												</div>
											</div>


											<!--/.panel-body -->
										</div>
                                    </div>
                                </div>
                                </div>
                                </div> 
                                <div class="row">
					<div class="col-md-12">
									<div class="set pull-right" style="margin-bottom: 20px;margin-top: 7px;">
										<div class="form-actions">
											<input type="hidden" name="leadId" value="${param.companyLeadId}">
											<button class="btn btn-default btn-danger" id="tourCancelBtn" type="reset">Reset</button>
											<button type="submit" class="btn btn-primary user-save-btn" id="tourEditBtn">Submit</button>
										</div>
										</div>	
										</div>
					</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </section>
            
            
            
            
            
            
            
            
            
            
            <div id="suppliermyNotification" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
									</button>
							<h4 class="modal-title">Supplier Payment Notification Alert
								!</h4>
						</div>

						<div class="modal-body">
							<span style="color: red; font-size: 11px; display: block">
								(* Please separate emails with semicolon) </span>
							<form action="" method="post" class="form-horizontal ng-pristine ng-valid" name="myForm" id="supplierForm">
								<div class="row">
									<div class="col-sm-12">

										<div class="col-sm-12  ">
											<div class="form-group">
												<label for="comment">To Mail(s):</label>
												<textarea class="form-control" rows="1" id="supplierToEmails"></textarea>
											</div>
										</div>

										<div class="col-sm-12">
											<div class="form-group">
												<label for="comment">CC Mail(s):</label>
												<textarea class="form-control" rows="1" id="supplierCcEmails"></textarea>
											</div>
										</div>

										<div class="col-sm-6 fd">
											<div class="form-group">
												<label for="currency" class=" control-label"> From
													Date </label> <input type="text" class="form-control input-sm hasDatepicker" value="" required="" id="supplierFromDate" placeholder="FromDate">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="currency" class=" control-label"> To
													Date <font size="1" color="red">(Selected time is
														preferred)</font>
												</label> <input type="text" class="form-control input-sm hasDatepicker" value="" required="" id="supplierToDate" name="" placeholder="date with preferable time">
											</div>
										</div>
										<div class="col-sm-6 fd">
											<div class="form-group">
												<label for="currency" class=" control-label"> Due
													Date </label> <input type="text" class="form-control input-sm hasDatepicker" value="" required="" id="supplierDueDate" name="" placeholder="FromDate">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="currency" class=" control-label"> Paid
													Date </label> <input type="text" class="form-control input-sm hasDatepicker" value="" required="" id="supplierPaidDate" name="" placeholder="FromDate">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<label for="comment">Comment:</label>
												<textarea class="form-control" rows="1" id="supplierComment"></textarea>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<div class="col-sm-12">
								<button type="button" id="paymentNotificationAlert" onclick="sendSupplierAndClientNotification('supplier');" class="btn btn-primary btn-sm">Set Supplier Reminder</button>
								<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div id="clientmyNotification" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
									</button>
							<h4 class="modal-title">Client Payment Notification Alert !</h4>
						</div>

						<div class="modal-body">
							<span style="color: red; font-size: 11px">(* Please
								separate mails with semicolon) </span>
							<form action="" method="post" class="form-horizontal ng-pristine ng-valid" name="myForm" id="clientForm">
								<div class="row">
									<div class="col-sm-12">

										<div class="col-sm-12 ">
											<div class="form-group">
												<label for="comment">To Mail(s):</label>
												<textarea class="form-control" rows="1" id="clientToEmails"></textarea>
											</div>
										</div>

										<div class="col-sm-12">
											<div class="form-group">
												<label for="comment">CC Mail(s):</label>
												<textarea class="form-control" rows="1" id="clientCcEmails"></textarea>
											</div>
										</div>

										<div class="col-sm-6 fd">
											<div class="form-group">
												<label for="currency" class=" control-label"> From
													Date </label> <input type="text" class="form-control input-sm hasDatepicker" value="" required="" id="clientFromDate" placeholder="FromDate">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="currency" class=" control-label"> To
													Date <font size="1" color="red">(Selected time is
														preferred)</font>
												</label> <input type="text" class="form-control input-sm hasDatepicker" value="" required="" id="clientToDate" name="" placeholder="date with preferable time">
											</div>
										</div>
										<div class="col-sm-6 fd">
											<div class="form-group">
												<label for="currency" class=" control-label"> Due
													Date </label> <input type="text" class="form-control input-sm hasDatepicker" value="" required="" id="clientDueDate" name="" placeholder="FromDate">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="currency" class=" control-label"> Paid
													Date </label> <input type="text" class="form-control input-sm hasDatepicker" value="" required="" id="clientPaidDate" name="" placeholder="FromDate">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<label for="comment">Comment:</label>
												<textarea class="form-control" rows="1" id="clientComment"></textarea>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<div class="col-sm-12">
								<button type="button" id="paymentNotificationAlert" onclick="sendSupplierAndClientNotification('client');" class="btn btn-primary btn-sm">Set Client Reminder</button>
								<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>

				</div>
			</div>
            
            
<script src="admin/js/admin/user-validators.js"></script>            
           <script type="text/javascript">
	$(document).ready(function() {
		$("#companyServiceType").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a Company Services",
		});
		
		var arrayuserroleType = '${companyServiceTypeView}'.split(',');
		console.debug(arrayuserroleType);
		$("#companyServiceType").multipleSelect("setSelects", arrayuserroleType);
	
	});
	
 $(document).ready(function() {
		$("#companyCMSType").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a Company CMS",
		});
		
		var arrayuserroleType = '${companyCMSTypeView}'.split(',');
		console.debug(arrayuserroleType);
		$("#companyCMSType").multipleSelect("setSelects", arrayuserroleType);
	
	}); 
</script> 
            <script type="text/javascript">
$(document).ready(function() {
$('#company-register')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
		companyName : {
					message : 'Companyname is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Company Name'
						},
						stringLength : {
							min : 3,
							max : 50,
							
							message : 'Companyname must be more than 3 and less than 100 characters long'
						} 
					}
				},
				companyType : {
					message : 'companyType is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Company Type'
						},
					}
				},
				distributorType : {
					message : 'Distributor is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Distributor Type'
						},
					}
				},
				typeOfWallet : {
					message : 'typeOfWallet is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Wallet Type'
						},
					}
				},
				postAmount : {
					message : 'typeOfWallet is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Enter a Amount'
						},
					}
				},
				currencyCode : {
					message : 'Currency  is not valid',
					validators : {
						 notEmpty : {
							message : 'select Enter a Currency'
						},
					}
				},
				website: {
		                validators: {
		                	notEmpty : {
								message : 'Please enter a website'
							},
		                    uri: {
		                        message: 'The website address is not valid'
		                    }
		                }
		            },
		        userName : {
					message : 'username is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a user name'
						},
						stringLength : {
							min : 3,
							max : 50,
							
							message : 'UserName must be more than 3 and less than 100 characters long'
						} 
					}
				},
				password : {
					message : 'password is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a password'
						},
						/* stringLength : {
							min : 6,
							max : 50,
							
							message : 'Password must be more than 6 and less than 100 characters long'
						}  */
					}
				},
				email : {
					message : 'Email  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a email'
						},
					}
				},
				CountryName : {
					message : 'Countryname  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a country'
						},
					}
				},
				/* Language : {
					message : 'Language  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a language'
						},
					}
				}, */
				phone : {
					message : 'Phone  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Phone'
						},
						integer: {
	                        message: 'The value is not a Phone Number'
	                    },
						 stringLength : {
								min : 4,
								max : 18,
								message : 'Phone must be more than 4 and less than 15 characters long'
						}  
					}
				},
				companyDescription : {
					message : 'Description  is not valid',
					validators : {
						/*  notEmpty : {
							message : 'Please enter a Description '
						}, */
					}
				},
				billingCompany : {
					message : 'Company Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Company Name'
						},
						stringLength : {
							min : 3,
							max : 50,
							
							message : 'Companyname must be more than 3 and less than 50 characters long'
						} 
					}
				},
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {

	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>
        
	<script type="text/javascript">
$(function() {
  
    $('#companyType').change(function(){
    	 if($('#companyType').val()== 'distributor') {
            $('.distributor-type-div').show(); 
        } 
        else if($('#companyType').val() == 'agent') {
        	 $('.distributor-type-div').hide(); 
          
       } 
        
    });
    
    $('#typeOfWallet').change(function(){
   	 if($('#typeOfWallet').val()== 'Postpaid') {
           $('.Wallet-type-div').show();
           
        } 
       else if($('#typeOfWallet').val() == 'Prepaid') {
       	 $('.Wallet-type-div').hide(); 
     
         
      } 
       
   });
    
    
    
   
 });
 </script>
<script src="admin/js/multiple-select.js"></script>	
		
						<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if> 