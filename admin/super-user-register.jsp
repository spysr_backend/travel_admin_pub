<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
	<section class="wrapper container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="pnl">
					<div class="hd clearfix">
						<h5 class="pull-left">
							<s:text name="tgi.label.add_super_company" />
						</h5>
					</div>
					<div class="cnt cnt-table">
						<div class="table-responsive">
							<form action="companyRegisterSuperUser" method="post"
								class="form-horizontal">
								<table class="table table-form">
									<tbody>
										<tr>
											<td>
												<div class="field-name">
													<s:text name="tgi.label.company_name" />
												</div>
											</td>
											<td><input type="text" class="form-control input-sm"
												id="companyName" name="companyName"
												placeholder="Company Name" autocomplete="off" required>
											</td>
											
											<td>
												<div class="field-name">Company 
													<s:text name="tgi.label.email" />
												</div>
											</td>
											<td><input type="email" class="form-control input-sm"
												name="email" id="email" placeholder="Email"
												autocomplete="off" required></td>
										<tr>
			
											<td>
												<div class="field-name">Mobile</div>
											</td>
											<td><input type="tel" class="form-control input-sm"
												name="mobile" id="telphone" placeholder="8105979291"
												autocomplete="off" required></td>
										<td>
												<div class=" field-name">
													<s:text name="tgi.label.upload_images" />
												</div>
											</td>
											<td>
												<div class="col-sm-12">
													<input type="file" id="lefile" accept="image/*"> <input
														type="hidden" name="imagePath" ng-model="Imagepath"
														value="{{imagePath}}">
												</div>

											</td>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
										</tr>
										<tr>
											<td>
												<div class="field-name">
													<s:text name="tgi.label.language" />
												</div>
											</td>
											<td><select class="form-control input-sm"
												name="language">
													<s:iterator value="languageList">
														<option value='${language}' ${id=='EN'? 'selected':''}>${language}</option>
													</s:iterator>
											</select></td>
											<td>
												<div class="field-name">
													<s:text name="tgi.label.currency" />
												</div>
											</td>
											<td><select class="form-control input-sm"
												name="currencyCode" id="currency">
													<option value="0" selected="selected"><s:text
															name="tgi.label.select_currency" /></option>
													<%-- <s:iterator value="#session.Country"> --%>
													<s:iterator value="countryList">
														<option value='${currencyCode}' ${currencyCode=='INR'? 'selected':''}>${currencyName}</option>
													</s:iterator>
											</select></td>
											</tr>
										
										<!--  Company User Detail-->
										<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										</tr>
										<tr>
											<td>
												<div class="field-name">
													Email
												</div>
											</td>
											<td><input type="email" class="form-control input-sm"
												name="user.email" id="email" placeholder=""
												autocomplete="off" required></td>
											<td>
												<div class="field-name">
													<s:text name="tgi.label.password" />
												</div>
											</td>
											<td><input type="password" class="form-control input-sm"
												name="user.password" id="password" placeholder=""
												autocomplete="off" required></td>
										</tr>
									</tbody>
								</table>
								<div class="form-group table-btn">
									<div class="col-sm-12 ">
										<div class="pull-right">
											<input type="hidden" name="superCompany" value="true">
											<button class="btn btn-default" type="submit"><s:text name="tgi.label.cancel" /></button>
											<button class="btn btn-primary " type="submit"><s:text name="tgi.label.register" /></button>
										</div>
									</div>
									<br/>
									<br/>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
