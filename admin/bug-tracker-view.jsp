<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
  <link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
  <link href="admin/css/inner-css/bug-tracker.css" rel="stylesheet" type="text/css">
  <link href="admin/css/inner-css/custom-button.css" rel="stylesheet" type="text/css">
<style>
.panel-body {
    padding: 20px;
} 
.bug-status-btn{
	border-radius: 4px !important;
}
.nav-tabs {
    background: #d8dbde;
    border: 0;
    -moz-border-radius: 3px 3px 0 0;
    -webkit-border-radius: 3px 3px 0 0;
    border-radius: 3px 3px 0 0;
}
.nav-tabs>li>a {
    border-radius: 0 0 0 0;
    border: 0;
    padding: 14px 20px;
    color: #353a40;
    font-weight: bold;
}
.comment-list .media-object {
    width: 60px; 
}
.comment-list .media {
    border-bottom: 1px solid #ddd;
}
.comment-list .media-body {
    font-size: 13px;
    position: relative;
}
.comment-list h4 {
    margin: 0;
    color: #333;
    font-size: 16px;
}
.headding-panel{
    background: #e6e6e6;
    padding: 4px 0px 4px 0px;
    margin-bottom: 5px;
    border-radius: 2px;
}
.clippy {
    margin-bottom: 2px;
}
span.status-label-info {
    background-color: #03A9F4;
    border-color: #3F51B5;
    padding: 1px 10px 1px 9px;
    color: #ffffff;
    border-radius: 2px;
    display: inline-block;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<i class="fa fa-bug"></i>Bug Issues
		</h1>
		<div class="breadcrumb-wrapper">
			<%-- <span class="label">You are here:</span> --%>
			<ol class="breadcrumb mb-0 mt-0">
				<li><a href="addBugTracker">Add Bug</a></li>
				<li><a href="goBugTrackerList">Bug List</a></li>
				<li><a href="viewTestCases?bugId=${id}">Test Case List</a></li>
				<li class="active">View Issue</li>
				
			</ol>
		</div>
	</section>
	<!-- Main content -->
	<section class="col-md-12">
		<div class="panel mt-3">
			<div class="panel-heading">
				<div class="pull-right">
				<h5 class="bug-key-title">${bugTracker.referenceNo} &nbsp;&nbsp;<small class="text-muted">
					<fmt:formatDate value="${bugTracker.createdAt}" pattern="E, MMM dd, yyyy h:m a" /></small>
				</h5>
				</div>
				<div class="panel-title">${bugTracker.title}</div>
			</div>
			<div class="panel-body">
				
				<div class="btn-group pull-right">
					<a href="#" class="btn btn-primary btn-sm" data-event="duplicate" data-toggle="modal" data-target="#update_bug_traker" ><i class="fa fa-pencil mr5"></i> Edit</a>
				</div>
			<!-- update bug tricker details -->
				<div class="modal fade" id="update_bug_traker" role="dialog">
										<div class="modal-dialog modal-lg">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title text-center">Edit Call Log</h4>
													<button type="button" class="close slds-modal__close" data-dismiss="modal">
													<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="18" alt="Copy to clipboard">
													</button>
												</div>
												<form id="updateBugTrackerForm" name="updateBugTrackerForm" method="post" class="bv-form">
													<input type="hidden" name="id" value="<s:property value="bugTracker.id"/>"> 
													<div class="modal-body scroll-modal-content">
													 <div class="row">
													 <h4 class="slds-section__title slds-theme--shade" style="font-weight: 500">
																<span class="section-header-title slds-p-horizontal--small slds-truncate">Basic Information</span></h4>
																<div class="col-md-6">
																<div class="col-md-12">
																<label class="form-control-label" for="comment"><span class="text-danger">*</span> Title</label>
																	<div class="controls">
																			<div class="form-group">
																			<input type="text" name="title" id="title" class="form-control input-sm"  autocomplete="off" value="<s:property value="bugTracker.title"/>" required="required">
																			</div>
																	</div>
																</div>
																<div class="col-md-12">
																<label class="form-control-label" for="comment">Description</label>
																	<div class="controls">
																		<div class="form-group">
																		<textarea cols="3" rows="3" class="form-control input-sm" name="bugTracker.description"><s:property value="bugTracker.description"  /></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-12">
																<label class="form-control-label" for="comment">Bug Type</label>
																	<div class="controls">
																		<div class="form-group">
																		<select class="form-control input-sm" id="level" name="bugTracker.bugType" required="required">
																		<%-- <option value="${bugTracker.bugType}" selected="selected">${bugTracker.bugType}</option> --%>
																		<c:forEach var="statusItem" items="${bugTrackerUtility.bugType}">
																			<c:choose>
																				<c:when test="${statusItem==bugTracker.bugType}">
																					<option value="${statusItem}" selected="selected">${statusItem}</option>
																				</c:when>
																				<c:otherwise>
																					<option value="${statusItem}">${statusItem}</option>
																				</c:otherwise>
																			</c:choose>
																		</c:forEach>
																	</select>
																		</div>
																	</div>
																</div>
																<div class="col-md-12">
																<label class="form-control-label" for="comment">Bug Priority</label>
																	<div class="controls">
																		<div class="form-group">
																		<select class="form-control input-sm" id="level" name="bugTracker.level" required="required">
																			<c:forEach var="statusItem" items="${bugTrackerUtility.bugPriority}">
																				<c:choose>
																					<c:when test="${statusItem==bugTracker.level}">
																						<option value="${statusItem}" selected="selected">${statusItem}</option>
																					</c:when>
																					<c:otherwise>
																						<option value="${statusItem}">${statusItem}</option>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																		</select>
																		</div>
																	</div>
																</div>
																<div class="col-md-12">
																<label class="form-control-label" for="comment"><span class="text-danger">*</span> Url</label>
																	<div class="controls">
																			<div class="form-group">
																			<input type="text" class="form-control input-sm" name="url" placeholder="URL" autocomplete="off" value="${bugTracker.url}">
																			</div>
																	</div>
																</div>
																<%-- <div class="col-md-12">
																<label class="form-control-label" for="comment">Status</label>
																	<div class="controls">
																		<div class="form-group">
																		<select class="form-control input-sm" id="status" name="bugTracker.status" required="required">
																			<c:forEach var="statusItem" items="${bugTrackerUtility.bugStatus}">
																				<c:choose>
																					<c:when test="${statusItem==bugTracker.status}">
																						<option value="${statusItem}" selected="selected">${statusItem}</option>
																					</c:when>
																					<c:otherwise>
																						<option value="${statusItem}">${statusItem}</option>
																					</c:otherwise>
																				</c:choose>
										
																			</c:forEach>
										
																		</select>
																		</div>
																	</div>
																</div> --%>
																</div>
																<div class="col-md-6">
																<div class="form-group">
																<label for="uploadimage" class="col-sm-4 control-label">Upload File </label>
																<div class="col-sm-8">
																	<div class="row">
																		<div class="col-sm-6 file-upload">
									
																			<input type="file" id="filePath"
																				accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,image/*"
																				ng-file-select="onFileSelect($files)" name="filePath">
																		</div>
																		<div class="col-sm-6 ">
																			<div id="fileinfo">
																				<div id="fileError"></div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
																</div>
														</div>
														
														<div class="row">
														<div class="col-md-12">
																<h4 class="slds-section__title slds-theme--shade" style="font-weight: 500">
																<span class="section-header-title slds-p-horizontal--small slds-truncate">Additional Information</span></h4>
																<div class="row">
																	<c:choose>
																	<c:when test="${session.User.userRoleId.techHead==true}">
																	<div class="col-md-6">
																	<label class="form-control-label" for="comment"><span class="text-danger">*</span> Assigned Date</label>
																		<div class="controls">
																				<div class="form-group">
																				<input type="text" class="form-control input-sm datetimepicker1" name="bugTracker.assignDate" value="<fmt:formatDate value="${bugTracker.assignDate}" pattern="MM/dd/yyyy" />" >
																				</div>
																		</div>
																	</div>
																	</c:when>
																	<c:otherwise>
																	<input type="hidden" class="form-control input-sm datetimepicker1" name="bugTracker.assignDate" value="<fmt:formatDate value="${bugTracker.assignDate}" pattern="MM/dd/yyyy" />"  readonly>
																	</c:otherwise>
																	</c:choose>
																</div>
																<div class="row">
																<c:choose>
																<c:when test="${session.User.userRoleId.techHead==true}">
																<div class="col-md-6">
																	<label class="form-control-label" for="comment">Work Start At</label>
																		<div class="controls">
																		<div class="form-group">
																		<input type="text" class="form-control input-sm datetimepicker1" name="bugTracker.startToWorkDate" value="<fmt:formatDate value="${bugTracker.startToWorkDate}" pattern="MM/dd/yyyy" />" >
																		</div>
																		</div>
																	</div>
																</c:when>
																<c:otherwise>
																<div class="col-md-6">
																	<label class="form-control-label" for="comment">Work Start At</label>
																		<div class="controls">
																		<div class="form-group">
																		<input type="text" class="form-control input-sm datetimepicker1" name="bugTracker.startToWorkDate" value="<fmt:formatDate value="${bugTracker.startToWorkDate}" pattern="MM/dd/yyyy" />" readonly="readonly">
																		</div>
																		</div>
																	</div>
																</c:otherwise>
																</c:choose>
																<c:choose>
																<c:when test="${session.User.userRoleId.techHead==true}">
																<div class="col-md-6">
																	<label class="control-label">Work Finish At</label>
																	<div class="controls">
																		<div class="form-group">
																		<input type="text" class="form-control input-sm datetimepicker1" name="bugTracker.workFinishDate" value="<fmt:formatDate value="${bugTracker.workFinishDate}" pattern="MM/dd/yyyy" />">
																	</div>
																	</div>
																</div>
																</c:when>
																<c:otherwise>
																<div class="col-md-6">
																	<label class="control-label">Work Finish At</label>
																	<div class="controls">
																		<div class="form-group">
																		<input type="text" class="form-control input-sm datetimepicker1" name="bugTracker.workFinishDate" value="<fmt:formatDate value="${bugTracker.workFinishDate}" pattern="MM/dd/yyyy" />" readonly="readonly">
																	</div>
																	</div>
																</div>
																</c:otherwise>
																</c:choose>
																<c:choose>
																<c:when test="${session.User.userRoleId.techHead==true}">
																<div class="col-md-6">
																	<label class="form-control-label" for="comment">Estimated Hours</label>
																	<div class="controls">
																		<div class="form-group">
																		<input type="number" class="form-control input-sm" name="bugTracker.totalEstimatedHours" value="<s:property value="bugTracker.totalEstimatedHours"/>">
																	</div>
																	</div>
																</div>
																</c:when>
																<c:when test="${session.User.userRoleId.techSupport==true }">
																		<div class="col-md-6">
																	<label class="form-control-label" for="comment">Estimated Hours</label>
																	<div class="controls">
																		<div class="form-group">
																		<input type="number" class="form-control input-sm" name="bugTracker.totalEstimatedHours" value="<s:property value="bugTracker.totalEstimatedHours"/>" readonly>
																	</div>
																	</div>
																</div>
																</c:when>
																<c:otherwise>
																<input type="hidden" name="bugTracker.totalEstimatedHours" value="<s:property value="bugTracker.totalEstimatedHours"/>" readonly>
																</c:otherwise>
																</c:choose>
																
																<c:choose>
																	<c:when test="${session.User.userRoleId.techHead==true}">
																	<div class="col-md-6">
																		<label class="form-control-label" for="comment">Developer Estimated Hours</label>
																		<div class="controls">
																		<div class="form-group">
																			<input type="number" class="form-control input-sm" name="bugTracker.totalDeveloperEstimatedHours" value="<s:property value="bugTracker.totalDeveloperEstimatedHours"/>">
																		</div>
																		</div>
																	</div>
																	</c:when>
																	<c:otherwise>
																	<div class="col-md-6">
																		<label class="form-control-label" for="comment">Developer Estimated Hours</label>
																		<div class="controls">
																		<div class="form-group">
																			<input type="number" class="form-control input-sm" name="bugTracker.totalDeveloperEstimatedHours" value="<s:property value="bugTracker.totalDeveloperEstimatedHours"/>" readonly="readonly">
																		</div>
																		</div>
																	</div>
																	</c:otherwise>
																</c:choose>
																
																<c:choose>
																	<c:when test="${session.User.userRoleId.techHead==true}">
																	<div class="col-md-6">
																		<label class="form-control-label" for="comment">Working Hours</label>
																		<div class="controls">
																		<div class="form-group">
																			<input type="number" class="form-control input-sm" name="bugTracker.totalWorkingHours" value="<s:property value="bugTracker.totalWorkingHours"/>" >
																		</div>
																		</div>
																	</div>
																	</c:when>
																	<c:otherwise>
																	<div class="col-md-6">
																		<label class="form-control-label" for="comment">Working Hours</label>
																		<div class="controls">
																		<div class="form-group">
																			<input type="number" class="form-control input-sm" name="bugTracker.totalWorkingHours" value="<s:property value="bugTracker.totalWorkingHours"/>" readonly="readonly">
																		</div>
																		</div>
																	</div>
																	</c:otherwise>
																	</c:choose>
																	
																<c:choose>
																	<c:when test="${session.User.userRoleId.techHead==true }">
																	<div class="col-md-6">
																		<label class="form-control-label" for="comment">Extra Hours</label>
																	<div class="controls">
																		<div class="form-group">
																			<input type="number" class="form-control input-sm" name="bugTracker.totalExtraHours" value="<s:property value="bugTracker.totalExtraHours"/>">
																		</div>
																		</div>
																	</div>
																	</c:when>
																	<c:when test="${session.User.userRoleId.techSupport==true }">
																	<div class="col-md-6">
																		<label class="form-control-label" for="comment">ExtraHours</label>
																		<div class="controls">
																		<div class="form-group">
																			<input type="number" class="form-control input-sm" name="bugTracker.totalExtraHours" value="<s:property value="bugTracker.totalExtraHours"/>">
																		</div>
																		</div>
																	</div>
																	</c:when>
																	<c:otherwise>
																			<input type="hidden" name="bugTracker.totalExtraHours" value="<s:property value="bugTracker.totalExtraHours"/>" readonly>
																	</c:otherwise>
																	</c:choose>
																
															</div>
																
														</div>
														</div>
														<div class="row">
																<h4 class="slds-section__title slds-theme--shade" style="font-weight: 500">
																<span class="section-header-title slds-p-horizontal--small slds-truncate">Owner Information</span></h4>
														
														<c:choose>
														<c:when test="${session.User.userRoleId.techHead==true}">
														<div class="col-md-6">
														<label class="form-control-label" for="comment">Assigned By</label>
														<div class="controls">
														<div class="form-group">
														<select name="bugTracker.assignedBy" id="assign-by" class="form-control">
															<option value="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="assign">
																		<option value="${assign.userId}" ${assign.userId == bugTracker.assignedBy ? 'selected': ''} >${assign.firstName}</option>
															</c:forEach>
														</select>
														</div>
														</div>
														</div>
														</c:when>
														<c:otherwise>
														</c:otherwise>
														</c:choose>
														
														<div class="col-md-6">
														<label class="form-control-label" for="comment">Assigned To</label>
														<div class="controls">
														<div class="form-group">
														<select name="bugTracker.assignTo" id="assign-to" class="form-control">
															<option value="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="assign">
																		<option value="${assign.userId}" ${assign.userId == bugTracker.assignTo ? 'selected': ''} >${assign.firstName}</option>
															</c:forEach>
														</select> 
														</div>
														</div>
														</div>
														</div>
												</div>
													<div class="modal-footer">
														<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
														<button type="submit" class="btn btn-success" id="update_bug_trakerBtn" data-modal-id="update_bug_traker" data-form-name="updateBugTrackerForm">Save</button>
													</div>
												</form>
											</div>
					
										</div>
								</div>
			<!--end-- update bug tricker details -->
			
				<div class="btn-group mr-1">
					<button class="btn <c:choose><c:when test="${bugTracker.status == 'New'}">btn-primary</c:when><c:otherwise>btn-default</c:otherwise></c:choose> btn-arrow-right bug-status-btn" 
							data-status="New" data-bug-id="${bugTracker.id}">  New
					</button>
					<button class="btn <c:choose><c:when test="${bugTracker.status == 'Pending'}">btn-warning</c:when><c:otherwise>btn-default</c:otherwise></c:choose> btn-arrow-right bug-status-btn" 
							data-status="Pending" data-bug-id="${bugTracker.id}">  Mark Pending
					</button>
					<c:choose>
					<c:when test="${bugTracker.status != 'WorkInProgress'}">
					<button class="btn <c:choose><c:when test="${bugTracker.status == 'Pause'}">btn-info</c:when><c:otherwise>btn-default</c:otherwise></c:choose> btn-arrow-right bug-status-btn" 
					data-status="WorkInProgress" data-bug-id="${bugTracker.id}"> <img class="clippy" src="admin/img/svg/play-button.svg" width="14" alt="Progress">  Work In Progress
					</button>
					</c:when>
					<c:otherwise>
					<button class="btn <c:choose><c:when test="${bugTracker.status == 'WorkInProgress'}">btn-warning</c:when><c:otherwise>btn-default</c:otherwise></c:choose> btn-arrow-right bug-status-btn" 
							data-status="Pause" data-bug-id="${bugTracker.id}"> <img class="clippy" src="admin/img/svg/pause-button.svg" width="14" alt="Pause"> Pause &nbsp;&nbsp;&nbsp;&nbsp;
					</button>
					</c:otherwise>
					</c:choose>
					<button class="btn <c:choose><c:when test="${bugTracker.status == 'Review'}">btn-danger</c:when><c:otherwise>btn-default</c:otherwise></c:choose> btn-arrow-right" 
							data-event="duplicate" data-toggle="modal" data-target="#ask_review_bug_status"> <img class="clippy" src="admin/img/svg/consulting-message.svg" width="16" alt="Review"> Ask Review
					</button>
					<button class="btn <c:choose><c:when test="${bugTracker.status == 'Closed'}">btn-danger</c:when><c:otherwise>btn-default</c:otherwise></c:choose> btn-arrow-right bug-status-btn" 
					data-status="Closed" data-bug-id="${bugTracker.id}"> <img class="clippy" src="admin/img/svg/cancel-bl.svg" width="12" alt="Close"> Close Issue
					</button>
				</div>
				<div class="row mt-4">
					<div class="">
						<h5 class="subtitle subtitle-lined">Details</h5>
						<div class="row row-minus">
							<div class="col-sm-6">
								<div class="row row-minus">
									<div class="col-xs-6 subTitleBold">Type</div>
									<div class="col-xs-6">${bugTracker.bugType}</div>
								</div>
								<div class="row row-minus">
									<div class="col-xs-6 subTitleBold">Priority</div>
									<div class="col-xs-6">${bugTracker.level}</div>
								</div>
								<div class="row row-minus">
									<div class="col-xs-6 subTitleBold">Status</div>
									<div class="col-xs-6"><span class="status-label">${bugTracker.status}</span></div>
								</div>
								<c:choose>
									<c:when test="${session.User.userRoleId.techHead==true || session.User.userRoleId.techSupport==true }">
										<div class="row row-minus">
											<div class="col-xs-6 subTitleBold">Estimated Hours</div>
											<div class="col-xs-6">
												<c:choose>
													<c:when test="${bugTracker.totalEstimatedHours != null}">
										${bugTracker.totalEstimatedHours} hrs <span data-toggle="tooltip" data-placement="top" title="Developer Hours">(${bugTracker.totalDeveloperEstimatedHours} hrs)</span>
											</c:when>
													<c:otherwise>
										<span class="text-warning">N/A</span> 
										</c:otherwise>
												</c:choose>
											</div>
										</div>
										<div class="row row-minus">
											<div class="col-xs-6 subTitleBold">WorkingHours</div>
											<div class="col-xs-6">
												<c:choose>
													<c:when test="${bugTracker.totalWorkingHours!=null}">
										${bugTracker.totalWorkingHours} hrs
									</c:when>
													<c:otherwise>
										 <span class="text-warning">N/A</span> 
										</c:otherwise>
												</c:choose>
											</div>
										</div>
										<div class="row row-minus">
											<div class="col-xs-6 subTitleBold">ExtraHours</div>
											<div class="col-xs-6">
												<c:choose>
													<c:when test="${bugTracker.totalExtraHours!=null}">
												${bugTracker.totalExtraHours} hrs
									</c:when>
													<c:otherwise>
										 <span class="text-warning">N/A</span> 
										</c:otherwise>
												</c:choose>
											</div>
										</div>
									</c:when>
								</c:choose>
							</div>
							<!-- col-sm-6 -->
							<div class="col-sm-6">
								<div class="row row-minus">
									<div class="col-xs-6 subTitleBold">Assignee By</div>
									<div class="col-xs-6">${bugTracker.assignedByName}</div>
								</div>
								<div class="row row-minus">
									<div class="col-xs-6 subTitleBold">Assigned To</div>
									<div class="col-xs-6">${bugTracker.assignedToName}</div>
								</div>
								<div class="row row-minus">
									<div class="col-xs-6 subTitleBold">Assigned Date</div>
									<div class="col-xs-6">
										<c:choose>
											<c:when test="${bugTracker.assignDate!=null}">
												<fmt:formatDate value="${bugTracker.assignDate}" pattern="MMM/dd/yyyy" />
											</c:when>
											<c:otherwise>
										 <span class="text-warning">N/A</span> 
										</c:otherwise>
										</c:choose>
									</div>
								</div>
								<div class="row row-minus">
									<div class="col-xs-6 subTitleBold">Work Start Date</div>
									<div class="col-xs-6">
										<c:choose>
											<c:when test="${bugTracker.startToWorkDate!=null}">
												<fmt:formatDate value="${bugTracker.startToWorkDate}" pattern="MMM/dd/yyyy" />
											</c:when>
											<c:otherwise>
										 <span class="text-warning">N/A</span> 
										</c:otherwise>
										</c:choose>
									</div>
								</div>
								<div class="row row-minus">
									<div class="col-xs-6 subTitleBold">Work Finish Date</div>
									<div class="col-xs-6">
										<c:choose>
											<c:when test="${bugTracker.workFinishDate!=null}">
												<fmt:formatDate value="${bugTracker.workFinishDate}" pattern="MMM/dd/yyyy" />
											</c:when>
											<c:otherwise>
										 <span class="text-warning">N/A</span> 
										</c:otherwise>
										</c:choose>
									</div>
								</div>

								<div class="row row-minus">
									<div class="col-xs-6 subTitleBold">Reporter</div>
									<div class="col-xs-6">${bugTracker.createdByUserName}</div>
								</div>
							</div>
							<!-- col-sm-6 -->
						</div>
						<!-- row -->
						<div class="mt-5"></div>
						<h5 class="subtitle subtitle-lined">Description</h5>
						<p>${fn:replace(bugTracker.description, newLineChar, "</br>")}</p>
						<br> <br>
						<ul class="nav nav-tabs nav-tabs-line" role="tablist">
				              <li class="nav-item active">
				              <a class="nav-link" data-toggle="tab" href="#history" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>History</a>
				              </li>
				              <li class="nav-item">
				              <a class="nav-link" data-toggle="tab" href="#comments" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>Comment</a>
				              </li>
				              <li class="nav-item">
				              <a class="nav-link" data-toggle="tab" href="#testCases" aria-controls="updates" role="tab" aria-selected="false"><i class="icon wb-cloud" aria-hidden="true"></i>Test Cases</a>
				              </li>
				              
			            </ul>
			            <div class="panel-tab-body">
						<div class="tab-content noshadow">
							<div class="tab-pane" id="comments" role="tabpanel"> 
							<div class="row comment-box mt-10">
							<div class="col-md-8">
							<div class="card">
							  <div class="card-header">Add Comment</div>
							  <div class="card-body">
							  <div class="row">
										<form action="saveBugTrackerComment" method="post" class="form-horizontal" name="myForm" id="myForm" enctype="multipart/form-data">
											 <input type="hidden" name="bugTrackerId" id="bugTrackerId" value="${bugTracker.id}" />
												<div class="col-md-12">
												
												<label class="">To &nbsp;&nbsp;<span class="text-primary">this lead</span></label>
													<div class="form-group mt-10">
															<textarea rows="3" cols="2" class="form-control" name="comments" placeholder="comments" required="required"></textarea>
													</div>
													<div class="form-group">
														<label for="uploadimage" class="col-sm-2 control-label">Upload
															File </label>
														<div class="col-sm-8">
															<div class="row">
																<div class="col-sm-6 file-upload">
																	<input type="file" id="filePath" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,image/*"
																		ng-file-select="onFileSelect($files)" name="bugTrackerHistory.filePath">
																</div>
																<div class="col-sm-6 ">
																	<div id="fileinfo">
																		<div id="fileError"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-4">
													<div class="form-group pull-right">
														<div class="col-xs-12">
															<button type="submit" id="save" class="btn btn-success">Save</button>
														</div>
													</div>
												</div>
										</form>
								</div>
								</div>  
								</div>
								</div>
								</div>
											<s:iterator value="bugTrackerCommentDataList" status="rowstatus">
											<ul class="media-list comment-list mt-10">
															<li class="media">
															<a href="#" class="pull-left">
                                                        <c:choose>
														<c:when test="${createdByImagePath != null}">
															<img alt="" src="<s:url action='getImageByPath?imageId=%{createdByImagePath}'/>" class="media-object">
														</c:when>
														<c:otherwise>
															<img alt="" src="admin/img/team4.png" class="media-object">
														</c:otherwise>
													</c:choose>
                                                    </a>
																<div class="media-body">
																<c:if test="${session.User.userRoleId.techHead==true}">
																	<div class="dropdown pull-right mt-10">
																	  <button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
																	  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
																	  <ul class="dropdown-menu">
																	    <li class="action"><a href="#" data-event="duplicate" data-toggle="modal" data-target="#update_comment_${rowstatus.count}">Edit</a></li>
																	    <li class="action"><a href="#" class="delete-follow-up-comment" data-comment-id="${id}">Delete</a></li>
																	  </ul>
																	</div>
																</c:if>
																	<h4 style="">${createdByName}</h4>
																	<small class="text-muted">
																	Created At :<em><fmt:formatDate value="${createdAt}" pattern="E, MMM dd, yyyy h:m a" /></em> 
																	<c:choose>
																	<c:when test="${updatedAt != null}">
																	-- Updated At:<em><fmt:formatDate value="${updatedAt}" pattern="E, MMM dd, yyyy h:m a" /></em>
																	</c:when>
																	<c:otherwise>
																	
																	</c:otherwise>
																	</c:choose>
																	
																	</small>
																	<p class="">${comments}.</p>
																	<div class="row">
																		<div class=" ">
																			<c:if test="${filePath !=null}">
																				<div class="download">
																					<p>
																						<b>Download</b> : <a href="downloadBugTrackerCommentFile?fileName=${filePath}" class="btn btn-success btn-xs">${filePath} </a>
																					</p>
																				</div>
																			</c:if>
																		</div>
																	</div>
																</div> 
															</li>
													</ul>
	<!--update follow up comment model-->
									<div class="modal fade" id="update_comment_${rowstatus.count}" role="dialog">
										<div class="modal-dialog modal-dialog-centered">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title text-center">Edit Comment</h4>
													<button type="button" class="close slds-modal__close" data-dismiss="modal">
													<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
													</button>
												</div>
												<form id="update_commentForm_${rowstatus.count}" method="post" class="bv-form">
												<input type="hidden" name="id" id="comment-id" value="${id}">
												 <input type="hidden" name="bugTrackerId" id="bugTrackerId" value="${bugTracker.id}" />
													<div class="modal-body">
													 <div class="row">
																<div class="col-md-12 mt-20">
																	<div class="controls">
																			<div class="form-group">
																			<textarea rows="5" cols="" name="comments" class="form-control">${comments}</textarea>
																			</div>
																	</div>
																</div>
															</div>
												</div>
													<div class="modal-footer">
														<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
														<button type="submit" class="btn btn-success update-followup-comment-btn" id="" data-modal-id="update_comment_${rowstatus.count}" data-form-name="update_commentForm_${rowstatus.count}">Save</button>
													</div>
												</form>
											</div>
					
										</div>
								</div>
									<!-- // update follow up comment-->
											</s:iterator>
											
											<button class="btn btn-primary add-comment-btn mt-4">
										<i class="fa fa-comments pr-1"></i> Add Comment
									</button>
					                </div>
<!-- history tab-pane -->
							<div id="history" class="tab-pane active">

								<div class="row">
									<div class="bug-tracker">
										<div class="bug-section">
											<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
												<s:iterator value="bugHistoryList" status="rowCount">
													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="headingOne">
															<h5 class="panel-title">
																<a role="button" class="bugtracker-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true">${rowCount.count} : 
																	<span><span class="text-muted">${status}</span></span>
																	<span class="pull-right"><small class="text-muted" style="font-size: 12px;"><fmt:formatDate value="${createdAt}" pattern="MMM/dd/yyyy hh:mm a" /></small></span>
																</a>
															</h5>
														</div>
														<div id="collapse" class="panel-collapse collapse"
															role="tabpanel">
															<div class="panel-body">

																<div class="row">
																	<div class="col-sm-6">
																		<div class="row">
																			<div class="col-xs-6">Type</div>
																			<div class="col-xs-6">${bugTracker.bugType}</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-6">Priority</div>
																			<div class="col-xs-6">${level}</div>
																		</div>
																		<c:choose>
																			<c:when
																				test="${session.User.userRoleId.techHead==true || session.User.userRoleId.techSupport==true }">
																				<div class="row">
																					<div class="col-xs-6">Estimated Hours</div>
																					<div class="col-xs-6">
																					<c:choose>
																					<c:when test="${estimatedHours != null}">
																					<span>${estimatedHours} hrs</span> <span data-toggle="tooltip" data-placement="top" title="Developer Hours">(${developerEstimatedHours} hrs)</span>
																					</c:when>
																					<c:otherwise>
																					<span class="text-warning">N/A</span>
																					</c:otherwise>
																					</c:choose>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-xs-6">WorkingHours</div>
																					<div class="col-xs-6">
																					<c:choose>
																					<c:when test="${workingHours != null}">
																					<span>${workingHours}</span>
																					</c:when>
																					<c:otherwise>
																					<span class="text-warning">N/A</span>
																					</c:otherwise>
																					</c:choose>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-xs-6">ExtraHours</div>
																					<div class="col-xs-6">
																					<c:choose>
																					<c:when test="${extraHours != null}">
																					<span>${extraHours}</span>
																					</c:when>
																					<c:otherwise>
																					<span class="text-warning">N/A</span>
																					</c:otherwise>
																					</c:choose>
																					</div>
																				</div>
																			</c:when>
																		</c:choose>
																	</div>
																	<!-- col-sm-6 -->
																	<div class="col-sm-6">
																		<div class="row">
																			<div class="col-xs-6">Status</div>
																			<div class="col-xs-6"><span class="text-success">${status}</span></div>
																		</div>
																		<div class="row">
																			<div class="col-xs-6">Assignee</div>
																			<div class="col-xs-6">${assignedByName}</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-6">Assigned To</div>
																			<div class="col-xs-6">${assignedToName}</div>
																		</div>
																		<div class="row">
																		<div class="col-xs-6 subTitleBold">Assigned Date</div>
																		<div class="col-xs-6">
																			<c:choose>
																				<c:when test="${assignDate!=null}">
																					<fmt:formatDate value="${assignDate}" pattern="MMM/dd/yyyy hh:mm a" />
																				</c:when>
																				<c:otherwise>
																			 <span class="text-warning">N/A</span> 
																			</c:otherwise>
																			</c:choose>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-xs-6 subTitleBold">Work Start Date</div>
																		<div class="col-xs-6">
																			<c:choose>
																				<c:when test="${startToWorkDate!=null}">
																					<fmt:formatDate value="${startToWorkDate}" pattern="MMM/dd/yyyy hh:mm a" />
																				</c:when>
																				<c:otherwise>
																			 <span class="text-warning">N/A</span> 
																			</c:otherwise>
																			</c:choose>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-xs-6 subTitleBold">Work Finish Date</div>
																		<div class="col-xs-6">
																			<c:choose>
																				<c:when test="${workFinishDate!=null}">
																					<fmt:formatDate value="${workFinishDate}" pattern="MMM/dd/yyyy hh:mm a" />
																				</c:when>
																				<c:otherwise>
																			 <span class="text-warning">N/A</span> 
																			</c:otherwise>
																			</c:choose>
																		</div>
																	</div>
																	</div>
																	<!-- col-sm-6 -->
																</div>
																<div class="row">
																	<div class="col-sm-12 ">
																		<c:if test="comments!=null">
																			<div class="b-comments">
																				<p>
																					<b>Comments : </b> ${comments}
																				</p>
																			</div>
																		</c:if>
																		<c:if test="${filePath !=null}">
																			<div class="download">
																				<p>Download</p>
																				<p>
																					<a
																						href="downloadBugTrackerHistoryFile?fileName=${filePath}"
																						class="btn btn-success btn-xs">${filePath} </a>
																				</p>
																			</div>
																		</c:if>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</s:iterator>
											</div>
										</div>
									</div>
								</div>

							</div>
<!--test cases panel  -->
							<div id="testCases" class="tab-pane">
								<div class="form-group">
									<label class="col-sm-2 control-label"> </label>
									<div class="col-sm-12">
										<div class="support">
											<div class="level1">
																<ul class="media-list comment-list mt-10">
															<s:iterator value="bugTestCasesList" status="rowstatus">
																<li class="media">
																<div class="media-body">
																<div class="dropdown pull-right mt-10">
																		<s:if test="verify!=true">
																				<a href="#!" data-test-case-id="${id}" class=" btn btn-primary btn-xs verify_test_case">Verify</a> 
																				<a href="#" data-event="duplicate" data-toggle="modal" data-target="#update_test_case_${rowstatus.count}" class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit</a> 
																				<c:if test="${session.User.userRoleId.techHead==true}">
																				<button type="submit" class="btn btn-danger btn-xs delete-test-cases" data-test-case-id="${id}"><i class="fa fa-thrash"></i> Delete</button>
																				</c:if>
																		</s:if>
																		<s:else>
																				<a href="#" class="btn btn-success btn-xs" id="verify" style="cursor: pointer !important;"><i class="fa fa-check ">Verified</i> </a>
																				<c:if test="${session.User.userRoleId.techHead==true}">
																				<a href="#" data-event="duplicate" data-toggle="modal" data-target="#update_test_case_${rowstatus.count}" class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit</a> 
																				<button type="submit" class="btn btn-danger btn-xs delete-test-cases" data-test-case-id="${id}"><i class="fa fa-thrash"></i> Delete</button>
																				</c:if>
																		</s:else>
																</div>
																<c:if test="${session.User.userRoleId.techHead==true}">
																</c:if>
																	<h4 style="">${createdByName} <small>Created At: <fmt:formatDate value="${createdAt}" pattern="MMM/dd/yyyy hh:mm a" /></small></h4>
																	<%-- <small class="text-muted">
																	Created At :<em><fmt:formatDate value="${createdAt}" pattern="E, MMM dd, yyyy h:m a" /></em> 
																	<c:choose>
																	<c:when test="${updatedAt != null}">
																	-- Updated At:<em><fmt:formatDate value="${updatedAt}" pattern="E, MMM dd, yyyy h:m a" /></em>
																	</c:when>
																	<c:otherwise>
																	
																	</c:otherwise>
																	</c:choose>
																	
																	</small> --%>
																	<p class="mt-2"><b>Title :</b> ${testCaseTitle}.</p>
																</div>
																<!--update bug test case  -->
																<div class="modal fade" id="update_test_case_${rowstatus.count}" role="dialog">
																<div class="modal-dialog modal-dialog-centered">
																	<!-- Modal content-->
																	<div class="modal-content">
																		<div class="modal-header">
																			<h4 class="modal-title text-center">Edit Test Case</h4>
																			<button type="button" class="close slds-modal__close" data-dismiss="modal">
																			<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
																			</button>
																		</div>
																		<form id="update_testCaseForm_${rowstatus.count}" method="post" class="bv-form">
																		<input type="hidden" name="id" id="comment-id" value="${id}">
																		 <input type="hidden" name="bugTestCase.id" id="test-case-id" value="${id}" />
																			<div class="modal-body">
																	 <div class="row">
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Title</label>
																					<div class="controls">
																							<div class="form-group">
																								<input type="text" name="testCaseTitle" id="testCaseTitle"  class="form-control" value="${testCaseTitle}"/>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Description</label>
																					<div class="controls">
																							<div class="form-group">
																								<textarea name="description" id="description" class="form-control" rows="2" cols="">${description}</textarea>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Module Name</label>
																					<div class="controls">
																							<div class="form-group">
																								<input type="text" name="module" id="module" class="form-control" value="${module}"/>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Test Priority</label>
																					<div class="controls">
																							<div class="form-group">
																								<select class="form-control" id="testPriority" name="testPriority">
																									<option value="" selected="selected">Select Priority</option>
																									<c:forEach var="priority" items="${testCaseUtility.testPriority}">
																										 <option value="${priority}" ${priority == testPriority ? 'selected': ''}>${priority}</option>
																										 </c:forEach>
																								 </select>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Test Status</label>
																					<div class="controls">
																							<div class="form-group">
																								<select class="form-control" id="testStatus" name="testStatus">
																									<option value="" selected="selected">Select Status</option>
																									<c:forEach var="status" items="${testCaseUtility.testStatus}">
																										 <option value="${status}" ${status == testStatus ? 'selected': ''}>${status}</option>
																										 </c:forEach>
																								 </select>
																							</div>
																					</div>
																				</div>
																			</div>
																</div>
																			<div class="modal-footer">
																				<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
																				<button type="submit" class="btn btn-success update-test-cases-btn" id="" data-modal-id="update_test_case_${rowstatus.count}" data-form-name="update_testCaseForm_${rowstatus.count}">Save</button>
																			</div>
																		</form>
																	</div>
											
																</div>
														</div>
																</li>
															</s:iterator>
															</ul>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						</div>
						</div>
					</div>
				</div>
			
			</div>
		</section>
		
	<div class="modal fade" id="ask_review_bug_status" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title text-center">Ask Review To</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
								</button>
							</div>
								<div class="modal-body">
								 <div class="row">
											<div class="col-md-12 mt-20">
												<div class="controls">
														<div class="form-group">
															<select name="bugTracker.assignTo" id="assign-to-id" class="form-control">
															<option value="0" data-assign-id="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="assign">
																		<option value="${assign.userId}" data-assign-id="${assign.userId}">${assign.firstName} ${assign.lastName}</option>
															</c:forEach>
															</select>
														</div>
												</div>
											</div>
						                    <div class="col-md-12 mt-10">
						                    <p class="text-warning">Assign this bug to <strong>Review</strong> for testing.</p>
						                    </div>
										</div>
								<input type="hidden" name="travelLeadId" id="" value="${id}">
							</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-success bug-status-btn" data-bug-id="${bugTracker.id}" data-status="Review">Save</button>
								</div>
						</div>

					</div>
				</div>
	<div class="modal fade" id="email_bug_status" role="dialog">
					<div class="modal-dialog modal-side modal-bottom-right">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title text-center">Email Send</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
								</button>
							</div>
							<form method="post" class="form-horizontal" name="myForm" id="follow-up-email-from" enctype="multipart/form-data">
								<div class="modal-body">
					                 <div class="row">
					                 <div class="form-group">
								      <label class="control-label col-sm-2 text-left" for="from-email">From</label>
								      <div class="col-sm-10">
								        <input type="text" class="form-control" id="from-email" name="fromEmail" value="<s:property value="%{#session.User.email}"/>">
								      </div>
								    </div>
								    <div class="form-group">
								      <label class="control-label col-sm-2 text-left" for="to-email">To</label>
								      <div class="col-sm-10">          
								        <input type="text" name="toEmail" class="form-control" id="to-email">
								      </div>
								    </div>
								    <div class="form-group">
								      <label class="control-label col-sm-2 text-left" for="bcc-email">Bcc</label>
								      <div class="col-sm-10">          
								        <input type="text"  name="bccEmail" class="form-control" id="bcc-email">
								      </div>
								    </div>
								    <div class="form-group">
								      <label class="control-label col-sm-2 text-left" for="subject">Subject</label>
								      <div class="col-sm-10">          
								        <input type="text" name="emailSubject" class="form-control" id="subject" placeholder="Enter Subject..">
								      </div>
								    </div>
								    <div class="form-group">
								     <label class="control-label col-sm-2 text-left" for="emailBody">&nbsp;</label>
								      <div class="col-sm-10">          
								        <textarea name="emailBody" cols="" rows="8" class="form-control" id="emailBody" placeholder="Email Body"></textarea>
								      </div>
								    </div>
					                 </div>
							</div>
								<div class="modal-footer">
									 <input type="hidden" name="bugTrackerId" id="bugTrackerId" value="${bugTracker.id}" />
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-success" data-bug-id="${bugTracker.id}" data-status="Review">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
								</div>
							</form>
						</div>

					</div>
				</div>
	<div class="modal fade" id="emailModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Alert !</h4>
				</div>
				<div class="modal-body">
					<p id="desc"></p>

				</div>

			</div>
		</div>
	</div>
</div>
<script>
	var i = 0;
	var j = 0;
	$('.panel-collapse ').each(function() {
		i++;
		var newID = 'collapse' + i;
		$(this).attr('id', newID);
		$(this).val(i);
	});

	$('.bugtracker-toggle').each(function() {
		j++;
		var newIDS = '#collapse' + j;
		$(this).attr('href', newIDS);

		$(this).val(j);
	});

	$(".add-comment-btn").click(function() {
		$(".comment-box").show();
	});
	$(document).ready(function() {
		var noOfBugTracker = $(".panel-group").find('.panel-default').length;
		//alert(noOfBugTracker);
		$(".panel-default:first-child").find(".panel-collapse").addClass("in");
	});
	/*  */
	$(document).ready(function() {
			$("#asssignDateedit , #startToWorkDateedit ,#workFinishDateedit ").datepicker({
										dateFormat : "dd-mm-yy"

							});

	});
/*0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-*/
$(function () {
    $('.datetimepicker1').datetimepicker({
    	format: 'MM/DD/YYYY'
    });
});

</script>

<script type="text/javascript">
	$(document).ready(function() {
			$('.bug-status-btn').click(function(e) {
						var bugId = $(this).data("bug-id");
						var bugStatus = $(this).data("status");
						var userId = $('select#assign-to-id').find(':selected').data('assign-id');
							$.ajax({
									url : "updateBugTrackerStatus?status="+bugStatus+"&id="+bugId+"&statusChanged=true&userId="+userId,
									type : "GET",
									dataType : 'json',
									success : function(jsonData) {
										if(jsonData.message.status == 'success'){
											alertify.success(jsonData.message.message);
											setTimeout(location.reload.bind(location), 1000);
											}
						  				else if(jsonData.message.status == 'error'){
						  					alertify.success(jsonData.message.message);
						  					}
									},
									error : function(request,status, error) {
										alertify.alert("<span class='text-danger'>Error, Contact Support with Bug Id</span>");
									}
								});
					});
			
		/*--update bug tracker--*/
			$("#update_bug_trakerBtn").click(function(e) {
				e.preventDefault();
				var formDeleteName = $(this).attr("data-form-name");
				var modalId = $(this).attr("data-modal-id");
				$.ajax({
					url : "updateBugTracker",
					type : "POST",
					dataType : 'json',
					data : $("#" + formDeleteName).serialize(),
					success : function(jsonData) {
						if(jsonData.message.status == 'success'){
							$("#" + modalId).hide();
							$.notify({icon: 'fa fa-check',message: jsonData.message.message },{type: 'success'});
							setTimeout(location.reload.bind(location), 1000);
							}
		  				else if(jsonData.message.status == 'error'){
		  					$("#" + modalId).hide();
		  					$.notify({icon: 'fa fa-exclamation-triangle',message: jsonData.message.message },{type: 'success'});
		  					}
					},
					error : function(request, status, error) {
						alertify.error("follow up comment can't be updated,please try again");
					}
				});
			});
		
		
			$(".update-followup-comment-btn").click(function(e) {
				e.preventDefault();
				var formDeleteName = $(this).attr("data-form-name");
				var modalId = $(this).attr("data-modal-id");
				$.ajax({
					url : "updateBugTrackerComment",
					type : "POST",
					dataType : 'json',
					data : $("#" + formDeleteName).serialize(),
					success : function(jsonData) {
						if(jsonData.message.status == 'success'){
							$("#" + modalId).hide();
							alertify.success(jsonData.message.message);
							}
		  				else if(jsonData.message.status == 'error'){
		  					$("#" + modalId).hide();
		  					alertify.error(jsonData.message.message);}
						setTimeout(location.reload.bind(location), 1000);
					},
					error : function(request, status, error) {
						alertify.error("follow up call can't be updated,please try again");
					}
				});
			});
			$(".delete-follow-up-comment").click(function(e) {
					// confirm dialog
					var commentId = $(this).data("comment-id");
					 alertify.confirm("Are you sure delete this comment", function () {
							$.ajax({
								url : "deleteBugTrackerComment?id="+commentId,
								type : "GET",
								dataType: 'json',
								success : function(jsonData) {
									if(jsonData.message.status == 'success'){
										alertify.success(jsonData.message.message);}
					  				else if(jsonData.message.status == 'error'){
					  					alertify.error(jsonData.message.message);}
									setTimeout(location.reload.bind(location), 1000);
								},
								error: function (request, status, error) {
									alertify.error("Comment  can not be deleted.");
								}
							});
					 }, function() {
						 alertify.error("You've clicked Cancel");
					 });
			});
			
			  $(".verify_test_case").click(function(e) {
					e.preventDefault();	
					var testCaseId = $(this).attr("data-test-case-id");
						$.ajax({
							url : "bugTestCaseVerify?id="+testCaseId,
							type : "POST",
							dataType: 'json',
							success : function(jsonData) {
									alertify.success("Test Case has been verfied successfully");
							},
							error: function (request, status, error) {
								alertify.error("Bug Test Cases can not be saved.","e");
							}
						});
					});
			  
			$(".update-test-cases-btn").click(function(e) {
				e.preventDefault();
				var formDeleteName = $(this).attr("data-form-name");
				var modalId = $(this).attr("data-modal-id");
				$.ajax({
					url : "updateBugTestCase",
					type : "POST",
					dataType : 'json',
					data : $("#" + formDeleteName).serialize(),
					success : function(jsonData) {
						if(jsonData.json.status == 'success'){
							$("#" + modalId).hide();
							alertify.success(jsonData.json.message);
							}
		  				else if(jsonData.json.status == 'error'){
		  					$("#" + modalId).hide();
		  					alertify.error(jsonData.json.message);}
						setTimeout(location.reload.bind(location), 1000);
					},
					error : function(request, status, error) {
						alertify.error("test cases can't be updated,please try again");
					}
				});
			});
			
			$(".delete-test-cases").click(function(e) {
				// confirm dialog
				var testId = $(this).data("test-case-id");
				 alertify.confirm("Are you sure delete this comment", function () {
						$.ajax({
							url : "deleteBugTestCaseById?id="+testId,
							type : "GET",
							dataType: 'json',
							success : function(jsonData) {
								if(jsonData.message.status == 'success'){
									alertify.success(jsonData.message.message);}
				  				else if(jsonData.message.status == 'error'){
				  					alertify.error(jsonData.message.message);}
								setTimeout(location.reload.bind(location), 1000);
							},
							error: function (request, status, error) {
								alertify.error("test case  can not be deleted.");
							}
						});
				 }, function() {
					 alertify.error("You've clicked Cancel");
				 });
		});
			
			
});
</script>




