<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
	
<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.show_date_to_user {
	padding: 0px 15px;
	position: relative;
	width: 93%;
	top: -25px;
	left: 5px;
	pointer-events: none;
	display: inherit;
	color: #000;
}
</style>
        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->

            <section class="wrapper container-fluid">
                <div class="">
                    <div class="">
                        <div class="pnl">
                        <div class="hd clearfix">
					<h5 class="pull-left">Approval Company List</h5>
					<div class="set pull-right hidden-xs" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
				</div>
                             <form action="listCompanyFilter" class="filter-form">
							    <div class="profile-timeline-card mt-0 mb-0" id="filterDiv" style="display:none;" >
							    <div class="row row-minus"> 
                                <div class="col-md-2 col-sm-6">
                                <div class=" form-group">
                                <input type="hidden" id="HdncompanyShowData" value="${param.companyShowData}" />
                                <select name="companyShowData" id="companyShowData" class="form-control">
                               	<option value="" selected="selected">Select Self Company</option>
                                <option value="all"><s:text name="tgi.label.all" /></option>
                                <option value="myself"><s:text name="tgi.label.my_self" /></option> 
                                </select>
                                </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                <input type="hidden" id="HdncompanyTypeShowData" value="${param.companyType}" />
                                <select name="companyType" id="companyTypeShowData" class="form-control">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="distributor"><s:text name="tgi.label.distributor" /></option>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div>
                                </div>
                                  <div class="col-md-2 col-sm-6">
                                  <div class="form-group">
                                <input type="text" name="startDate" id="datepicker_startDate" value="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MM/dd/yyyy')}"
												placeholder="Start Date...."
												class="form-control search-query date1" />
												
                                </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                <input type="text" name="endDate" id="datepicker_endDate" value="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MM/dd/yyyy')}" placeholder="End Date...." class="form-control hasdatepicker date2" />
                                </div>
                                </div>
                             <!--   <div class="col-md-2">
								<input type="text" name="companyType" id="companytype-json"
												placeholder="Company Type...."
												class="form-control search-query" />
								</div> -->
                                 <div class="col-md-2 col-sm-6">
                                 <div class="form-group"> 
								<input type="text" name="userName" id="username-json"
												placeholder="User Name...."
												class="form-control search-query" />
								</div> 
								</div> 
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="companyName" id="companyname-json"
													placeholder="Company Name...."
													class="form-control search-query" />
									</div>
									</div> 
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                <input type="text" name="email" id="email-json"
												placeholder="Search By Email...."
												class="form-control search-query" />
                                </div>
                                </div>
                                
                                
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                <input type="text" name="phone" id="phone-json"
												placeholder="Search By Phone...."
												class="form-control hasdatepicker" />
                                </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                <input type="text" name="city" id="city-json"
												placeholder="Search By City...."
												class="form-control hasdatepicker" />
                                </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                <input type="text" name="countryName" id="country-json"
												placeholder="Search By Country...."
												class="form-control hasdatepicker" />
                                </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                <input type="text" name="companyUserId" id="companyId-json"
												placeholder="Search By Company Id...."
												class="form-control hasdatepicker" />
                                </div>
                                </div>
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">                                 
                                <select name="flagTypeStatus" id="flagTypeStatus" class="form-control">
                               	<option value="" selected="selected">Select Company Status</option>
                                <option value="true">Active</option>
                                <option value="false">Inactive</option> 
                                </select>
                                </div>
                                </div>  
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                <select name="flagTypeLock" id="flagTypeLock" class="form-control">
                               	<option value="" selected="selected">Select Locked &amp; Unlocked</option>
                                <option value="false">Unlocked</option>
                                <option value="true">Locked</option> 
                                </select>
                                </div> 
                                </div> 
                                <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                 <button type="submit" class="btn btn-info btn-block">
      							<span class="glyphicon glyphicon-search"></span> <s:text name="tgi.label.search" />
    							</button>
    							</div>
    							</div>
    							</div>
                                </div> 
                                </form>
							  <div class="cnt cnt-table">
						    <div class=" ">
		<div class="fw-container">
		<div class="fw-body">
			<div class="content mt-0 pt-2">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"  role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
                                            <thead>
                                            <tr class="table-sub-header">
                                        	<th colspan="10" class="noborder-xs">
                                        	<div class="pull-right" style="padding:4px;margin-top: -6px;margin-bottom: -10px;">   
                                        	<div class="btn-group"> 
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Company">
											  <a class="btn btn-sm btn-default" href="addCompany" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class="">&nbsp;New&nbsp;</span></a> 
											</div>
											<div class="dropdown pull-left" style="margin-right:5px;">
											  <a class="btn btn-sm btn-default " href="#" >&nbsp;Import&nbsp;</a> 
											</div>
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a  class="btn btn-sm btn-default filterBtn" data-toggle="dropdown"  style="margin-bottom: 2px;margin-right: 3px;" title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
                                                <tr class="border-radius border-color" role="row">
 							 <th><s:text name="tgi.label.sno" /></th>
							<th><s:text name="tgi.label.company_type" /></th>
							<th><s:text name="tgi.label.company" /></th>
							<th><s:text name="tgi.label.email" /></th>
							<th><s:text name="tgi.label.phone" /></th>
							<th><s:text name="tgi.label.city" /></th>
							<th><s:text name="tgi.label.country" /></th>
							<th>Mail Status</th>
							<th>Verify Status</th>
						  	<th style="width:150px"><div class="center"><s:text name="tgi.label.approval" /></div></th>
						 	<th></th>
						</tr>
					</thead>
                                            <tbody>
                              <s:iterator value="AllApprovalcompanylist"  status="rowStatus" var="company">
							<tr>
								<td data-title="S.No"><s:property value="#rowStatus.count"/></td>
								<td data-title="Company Type">
								<c:choose>
								<c:when test="${companyRole.agent==true}">
									Agency
								</c:when>
								<c:when test="${companyRole.distributor==true}">
									Wholesaler
								</c:when>
								<c:when test="${companyRole.corporate==true}">
									Corporate
								</c:when>
								<c:otherwise>
								Superuser
								</c:otherwise>
								</c:choose>
								</td>
								<td data-title="Company"><s:property value="companyName"/></td>
								<td data-title="Email"><s:property value="email" /></td>
								<td data-title="Phone"><s:property value="phone" /></td>
								<td data-title="City"><s:property value="city"/></td>
								<td data-title="Country"><s:property value="countryName" /></td>
								
								<td>
								<c:choose>
									<c:when test="${company.mailStatus == 1}"><span class="text-success"><i class="fa fa-check-circle"></i> Success</span>
									</c:when>
									<c:otherwise><span class="text-danger"><i class="fa fa-times-circle"></i> Failed </span>
									</c:otherwise>
									 </c:choose>
								</td>
								<td data-title="Country">
								<c:choose>
								<c:when test="${company.verifyStatus != false}">
								<span class="text-success"><i class="fa fa-check-circle"></i> Verify</span>
								</c:when>
								<c:otherwise>
								<span class="text-danger"><i class="fa fa-times-circle"></i> Not Verify</span>
								</c:otherwise>
								</c:choose>
								</td>
								<td data-title="Approval">
											<form action="compnayApproval" method="POST">
										 	<input type="hidden" name="status" value="<s:property value="status"/>" />
										 	<input type="hidden" name="companyId" value="<s:property value="companyId"/>" />
										 	<button type="submit" class="btn btn-xs btn-outline-danger" value="" >Approve Now</button>
											</form>
											
									<%-- <form action="compnayApproval" method="post"  id="statusForm<s:property value="companyid"/>">
										<input type="hidden" name="companyid"
											value="<s:property value="companyid"/>" id="uniqueId">
											 
											 <input type="hidden" name="Status"  value="<s:property value="Status"/>" id="Status">
											  <span id="load<s:property value="companyid"/>" style="display: none;" ><img src="images/loading.GIF" class="load-gif"> </span>
										 	<s:if test="Status == true">
										 	<input type="button" class="btn-primary" value="Approved" onclick="activateOrDeactivateFormsubmit(<s:property value="companyid"/>)"	id="stat<s:property value="companyid"/>"> 
											</s:if>
											<s:if test="Status == false">
										 	<input type="button" class="btn-primary" value="NotApproved" onclick="activateOrDeactivateFormsubmit(<s:property value="companyid"/>)"	id="stat<s:property value="companyid"/>"> 
											</s:if>
											<div id="load<s:property value="companyid"/>" style="display:block;"><img src="images/loading.GIF"> </div>
											 
									</form>
						  --%>
										</td>
										<%-- <td data-title="Action">
										<div class="btn-group">
											<a class="btn btn-xs btn-success dropdown-toggle"
												data-toggle="dropdown" style="padding: 3px 6px;"> <i
												class="fa fa-cog fa-lg" aria-hidden="true"></i>
											</a>
											<ul class="dropdown-menu dropdown-info pull-right align-left">
											<li class=""><a
											href="companyDetails?companyId=<s:property value="companyId"/>"
											class="btn btn-xs" data-title="Update"> <span
											data-placement="top" class="fa fa-plus-circle"></span> <s:text name="tgi.label.view_profile" /></a></li>
											</ul>
										</div>
										</td> --%>
									<td></td>

							</tr>
						</s:iterator>
                                            </tbody>
                                        </table>
                                    </div>
								</div></div></div>
                            </div>
                        </div>
                    </div></div>
                </div>
            </section>
        <!--ADMIN AREA ENDS-->

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"getApprovalCompaniesList";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 <script src="admin/js/jquery-ui.js" ></script>
  <script>
$(document).ready(function() {
	var date1="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
	var date2="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
	 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
});
</script>
 
 
<script>
	  $(function(){
		  var totUrl=$(location).attr('href');
			var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
			  var finalUrl = newUrl+"getApprovalCompaniesList";
		  
		  
	      var t  = $('.alert').text();
	   if(t.length>0){
	    	   alert(t); 
	    	 window.location.assign(finalUrl)
	     }
	   });
</script>
<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
						
  <script>
//user name
	var UserName = {
		url: "getCompanyJson.action?data=userName",
		getValue: "userName",
		
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#username-json").easyAutocomplete(UserName);
	
	// company name
	var CompanyName = {
		url: "getCompanyJson.action?data=companyName",
		getValue: "companyName",
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#companyname-json").easyAutocomplete(CompanyName);
	
	// company type
	var CompanyType = {
		url: "getCompanyJson.action?data=companyType",
		getValue: "companyType",
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#companytype-json").easyAutocomplete(CompanyType);
	
	
 // email
		var Email = {
			url: "getCompanyJson.action?data=email",
			getValue: "email",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#email-json").easyAutocomplete(Email);
		
		// phone
		var Phone = {
			url: "getCompanyJson.action?data=phone",
			getValue: "phone",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#phone-json").easyAutocomplete(Phone);
		
		// country name
		var CountryName = {
			url: "getCompanyJson.action?data=countryName",
			getValue: "countryName",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#countryname-json").easyAutocomplete(CountryName);
		
		// city name
		var CityName = {
			url: "getCompanyJson.action?data=cityName",
			getValue: "cityName",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#cityname-json").easyAutocomplete(CityName);
		
	</script>	
	
	<%-- <script type="text/javascript" class="init">
$(document).ready(function() {
		$('.filterBtn').click(function() {
	    $('#filterDiv').toggle();
	});
		
		$('div.easy-autocomplete').removeAttr('style');	
} );

$('.faq-links').click(function() {
	var collapsed = $(this).find('i').hasClass('fa-compress');

	$('.faq-links').find('i').removeClass('fa-expand');

	$('.faq-links').find('i').addClass('fa-compress');
	if (collapsed)
		$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
}); 
	</script> --%>
	
	<script>
  
 $(document).ready(function(){$(".filterBtn").click(function(){$("#filterDiv").toggle(500)}),
	 $("div.easy-autocomplete").removeAttr("style"),$("#configreset").click(function(){$("#resetform")[0].reset()})}),
	 $(".filter-link").click(function(){var e=$(this).find("i").hasClass("fa-angle-up");$(".filter-link").find("i").removeClass("fa-angle-down"),
	 $(".filter-link").find("i").addClass("fa-angle-up"),e&&$(this).find("i").toggleClass("fa-angle-up fa-2x fa-angle-down fa-2x")});
  </script>