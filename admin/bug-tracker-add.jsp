<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="dj" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="admin/css/inner-css/bug-tracker.css" rel="stylesheet" type="text/css">

<style>
.hidden {
	display: none;
}

.error {
	color: red;
}

.valid {
	color: green;
}
</style>

 <section class="wrapper container-fluid"> 

	<section class="content-header">
		<h1>
			<i class="fa fa-plus"></i>Create Bug
		</h1>
		<div class="breadcrumb-wrapper">
			<%-- <span class="label">You are here:</span> --%>
			<ol class="breadcrumb">
				<li><a href="goBugTrackerList">Bug List</a></li>
				<li class="active">Add Bug</li>
			</ol>
		</div>
	</section>
		<!-- Main content -->
		<section class="">
		<div class="container">
					<div class="row">
					<h4 class="text-center p-4 hidden-xs">Create Bug</h4> 
					<div class="col-md-2"></div>
					<div class="col-md-8">
                   <div class="spy-form-horizontal mb-0"> 
			<form method="post" class="form-horizontal" name="bugTrackerForm" id="bugTrackerForm" enctype="multipart/form-data"> 
								<input type="hidden" id="notiCreatedUserId" name="userId" value="<s:property value="#session.User.id" />"> 
								<input  name="companyId" type="hidden" id="notiCreatedCompanyId" value="<s:property value="#session.User.Companyid" />">
						<div class="row">
						<div class="form-group">
								<label class="col-sm-4 control-label text-left">Title</label>
								<div class="col-sm-8">
									<input type="text" class="form-control input-sm" name="title" id="title" placeholder="Bug title" autocomplete="off" required>
								</div>
						</div>
					<!-- 	<div class="form-group">
							<label class="col-sm-4 control-label text-left">Bug Tag</label>
							<div class="col-sm-8">
							<textarea rows="2" cols="2"  class="form-control input-sm" 	name="tag" placeholder="bug tag"  
									required></textarea>
							 </div>
						</div> -->

 					<div class="form-group">
							<label class="col-sm-4 control-label text-left">Description</label>
							<div class="col-sm-8">
							<textarea rows="4" cols="4"  class="form-control input-sm" 	name="description" placeholder="Description..."  
									required></textarea>
							 </div>
						</div>
						 
  						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Bug Type</label>
							<div class="col-sm-8">
								<select class="form-control input-sm" id="level" name="bugType">
										<c:forEach var="statusItem" items="${bugTrackerUtility.bugType}">
											 <option value="${statusItem}">${statusItem}</option>
											 </c:forEach>
									 </select>
							</div>
						</div> 
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Bug Priority</label>
							<div class="col-sm-8">
								<select class="form-control input-sm" id="level" name="level">
									<c:forEach var="statusItem" items="${bugTrackerUtility.bugPriority}">
											 <option value="${statusItem}">${statusItem}</option>
											 </c:forEach>
									 
								</select>
							</div>
						</div> 
						<c:choose>
						<c:when test="${session.User.userRoleId.techHead==true}">
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Status</label>
							<div class="col-sm-8">
								<select class="form-control input-sm" id="status" name="status" required>
										 <c:forEach var="statusItem" items="${bugTrackerUtility.bugStatus}">
										 <c:choose>
											 <c:when
												  test="${statusItem=='Created'}">
																		<option value="${statusItem}" selected="selected">${statusItem}</option>
																	</c:when>
																	<c:otherwise>
																		<option value="${statusItem}">${statusItem}</option>
																	</c:otherwise>
																</c:choose>
											 </c:forEach>
								</select>
							</div>
						</div>
						</c:when>
						<c:otherwise>
							<input type="hidden" name="status" id="status" value="New">
						</c:otherwise>
						
						</c:choose>
						
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Assigned To</label>
							<div class="col-sm-8">
								  	<select name="assignTo" id="assign-to" class="form-control input-sm">
									<option value="0" selected="selected">Select User</option>
									<c:forEach items="${userVos}" var="assign">
												<option value="${assign.userId}" >${assign.firstName} ${assign.lastName}</option>
									</c:forEach>
									</select> 
							</div>
						</div>
						
						<c:choose>
						<c:when test="${session.User.userRoleId.techHead==true}">
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Assigned Date</label>
							<div class="col-sm-8">
								<input type="text" class="form-control input-sm datetimepicker1" name="assignDate" placeholder="Assigned Date" autocomplete="off">
							</div>
						</div>
						</c:when>
						</c:choose>
						<c:choose>
						<c:when test="${session.User.userRoleId.techHead==true}">
							<div class="form-group">
							<label class="col-sm-4 control-label text-left">Work Start Date</label>
							<div class="col-sm-8">
								<input type="text" class="form-control input-sm datetimepicker1" name="startToWorkDate" placeholder="Start To Work Date" autocomplete="off">
							</div>
						</div>
						</c:when>
						</c:choose>
						<c:choose>
						<c:when test="${session.User.userRoleId.techHead==true}">
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Work Finish Date</label>
							<div class="col-sm-8">
								<input type="text" class="form-control input-sm datetimepicker1" name="workFinishDate" placeholder="Work Finish Date" autocomplete="off">
							</div>
						</div>
						</c:when>
						</c:choose>
						
						<c:choose>
						<c:when test="${session.User.userRoleId.techHead==true}">
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Estimated Hours</label>
							<div class="col-sm-8">
								<input type="number" class="form-control input-sm" name="totalEstimatedHours" placeholder="Estimated Hours" autocomplete="off">
							</div>
						</div>
						</c:when>
						</c:choose>
						<c:choose>
						<c:when test="${session.User.userRoleId.techHead==true}">
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Developer Estimated Hours</label>
							<div class="col-sm-8">
								<input type="number" class="form-control input-sm" name="totalDeveloperEstimatedHours" placeholder="developer Estimated Hours" autocomplete="off">
							</div>
						</div>
						</c:when>
						</c:choose>
						
						<c:choose>
						<c:when test="${session.User.userRoleId.techHead==true}">
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Working Hours</label>
							<div class="col-sm-8">
								<input type="number" class="form-control input-sm" name="totalWorkingHours" placeholder="working hours" autocomplete="off"  >
							</div>
						</div>
						</c:when>
						</c:choose>
						<c:choose>
						<c:when test="${session.User.userRoleId.techHead==true}">
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">Extra Hours</label>
							<div class="col-sm-8">
								<input type="number" class="form-control input-sm" name="totalExtraHours" placeholder="Extra Hours" autocomplete="off">
							</div>
						</div>
						</c:when>
						</c:choose>
						<div class="form-group">
							<label class="col-sm-4 control-label text-left">URL</label>
							<div class="col-sm-8">
								<input type="text" class="form-control input-sm" name="url" placeholder="URL" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="uploadimage" class="col-sm-4 control-label text-left">Upload File </label>
							<div class="col-sm-8">
								<div class="">
									<div class="file-upload">
										<input type="file" id="filePath" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,image/*" 
											ng-file-select="onFileSelect($files)" name="bugTrackerHistory.filePath" class="form-control">
									</div>
									<!-- 	ng-file-select="onFileSelect($files)" -->
									<div class="col-sm-6 ">
										<div id="fileinfo">
 									<div id="fileError"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group text-center">
					<div class="col-xs-12 submitWrap text-center">
						<input type="hidden" name="bugActiveStatus" value="1">
						<button type="submit" id="bug-tracker-saveBtn" class="btn btn-success pull-right">Create Bug</button>
					</div>
				</div>
			 </div> 
			</form>
			</div></div></div></div>
		</section>
</section> 
<script type="text/javascript">

$(function () {
    $('.datetimepicker1').datetimepicker({
    	format: 'MM/DD/YYYY'
    });
});

/* $(function(){
	  $('bugTrackerForm').on('keypress', function(event){
	    if(event.which === 13 && $(event.target).is(':input')){
	        event.preventDefault();
	        $('#bug-tracker-saveBtn').trigger('click');
	    }
	  });
	}); */
	
/* save */

/* $("#bug-tracker-saveBtn").click(function(e) {
			e.preventDefault();
			var formDeleteName = $(this).attr("data-form-name");
			$.ajax({
				url : "saveBugTracker",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						$.notify({icon: 'fa fa-check-square',message: jsonData.message.message },{type: 'success'});
					}
	  				else if(jsonData.message.status == 'error'){
	  					$.notify({icon: 'fas fa-exclamation-triangle',message: jsonData.message.message },{type: 'danger'});
	  					}
					setTimeout(location.reload.bind(location), 1000);
				},
				error : function(request, status, error) {
					alertify.error("follow up call can't be updated,please try again");
				}
			});
		}); */
		$(document).ready(function() {
			$("#asssignDate ,#startToWorkDate,#workFinishDate").datepicker({
										autoclose : true,
										format : 'mm/dd/yyyy',
										todayHighlight : true
			});

	});
		
$(document).ready(function() {
    $("#bugTrackerForm").bootstrapValidator({
        feedbackIcons: {
            valid: "fa fa-check",
            invalid: "fa fa-remove",
            validating: "fa fa-refresh"
        },
        fields:{title:{message:"Title is not valid",validators:{notEmpty:{message:"Title is required field"},stringLength:{min:1,message:"Name must be more than 3 and less than 100 characters long"}}},
        		description:{message:"Description is not valid",validators:{notEmpty:{message:"Description is a required field"}}}}
        
    }).on("error.form.bv", function(e) {}).on("success.form.bv", function(e) {
            e.preventDefault();
            var form = $('form')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            $.ajax({
                url: "saveBugTracker",
                type: "POST",
                dataType: "json",
                data: formData,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false,
                success: function(jsonData) {
                	
                	if(jsonData != null)
                	{
                		if(jsonData.message.status == 'success'){
    						$.notify({icon: 'fa fa-check-square',message: jsonData.message.message },{type: 'success'});
    						var url = '<%=request.getContextPath()%>/goBugTrackerList';
    						window.location.replace(url);
    						
    					}
    	  				else if(jsonData.message.status == 'error'){
    	  					$.notify({icon: 'fas fa-exclamation-triangle',message: jsonData.message.message },{type: 'danger'});
    	  					}
                	}
                	else
                	{
                		$.notify({icon: 'fas fa-exclamation-triangle',message: "Somthing Wrong Json Data is null" },{type: 'danger'});
                	}
                	
                    $(".save_travel_sales_leadBtn").removeAttr("disabled"), console.debug("button disabled ");
                    var s = $(e.target);
                    s.data("bootstrapValidator");
                    s.bootstrapValidator("disableSubmitButtons", !1).bootstrapValidator("resetForm", !0)
                },
                error: function(e, a, s) {
                    showModalPopUp("Travel Lead can not be Saved.", "e")
                }
            })
    }).on("status.field.bv", function(e, a) {
        a.bv.getSubmitButton() && (console.debug("button disabled "), a.bv.disableSubmitButtons(!1))
    })
});
</script>
