<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%--  <script src= https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js> </script>
    <script src= https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js> </script> --%>

 <title><s:property value="%{#session.ReportData.agencyUsername}" /></title> 

		<!-- Content Header (Page header) -->
		 <s:if test="hasActionErrors()">
						<div class="succfully-updated clearfix" id="error-alert">

							<div class="col-sm-2">
								<i class="fa fa-check fa-3x"></i>
							</div>
							<div class="col-sm-10">
								<p>
									<s:actionerror />
								</p>
								<button type="button" id="cancel" class="btn btn-primary">Ok</button>
							</div>
						</div>
					</s:if>
					<s:if test="hasActionMessages()">
						<div class="sccuss-full-updated" id="success-alert">
							<div class="succfully-updated clearfix">

								<div class="col-sm-2">
									<i class="fa fa-check fa-3x"></i>
								</div>

								<div class="col-sm-10">
									<s:actionmessage />
									<button type="button" id="success" class="btn btn-primary">Ok</button>
								</div>
							</div>
						</div>
					</s:if>
		
 <section class="wrapper container-fluid">
                <div class="row">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.hotel_passenger_order_details" /></h5>
                                 <div class="set pull-right">
								 <a class="btn btn-success btn-xs" href="hotelOrdersList"><span
								class="pull-right">
									<span class="glyphicon glyphicon-step-backward"></span><s:text name="tgi.label.back" /></span></a>
                                     </div></div>

		<!-- Main content -->
		<section class="col-md-12">
			<!-- Small boxes (Stat box) -->
			 
			 <div class="row">
				<div class="profile-timeline-card"> 
				<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-h-square" aria-hidden="true"></i></span> <span> <b class="blue-grey-text"></b></span>
									</p>
										
									<%--  <div class="col-md-2" style="border-right:1px solid #ddd"><b><s:text name="tgi.label.created_by" /></b> <br><br> <p>${CurrentHotelReport.agencyUsername}</p> </div>
									 <div class="col-md-2" style="border-right:1px solid #ddd"><b>Order Id</b> <br><br> <p style="color: #ff3100;"><s:property value="CurrentHotelReport.orderRef" /></p> </div>
									 <div class="col-md-2" style="border-right:1px solid #ddd"><b>Reference Code</b> <br><br> <p> ${CurrentHotelReport.referenceCode}</p> </div>
									 <div class="col-md-2" style="border-right:1px solid #ddd"><b><s:text name="tgi.label.supplier" /></b> <br><br> <p> ${CurrentHotelReport.apiProvider.vendorName}</p> </div> --%>
									 
									 <div class="table-responsive no-table-css clearfix">
									<table class="table table-striped table-bordered">
									<thead>
                                   <tr class="border-radius border-color">	
									<tr>
										<th><b><s:text name="tgi.label.created_by" /></b></th>
										<th><b><s:text name="tgi.label.order_id" /></b></th>
										<th><b><s:text name="tgi.label.reference_code" /></b></th>
										<th><b><s:text name="tgi.label.booking_status" /></b></th>
										<th><b><s:text name="tgi.label.supplier" /></b></th>
									</tr></thead>
									<tr>
										<td data-title="<s:text name="tgi.label.created_by" />"><s:property value="CurrentHotelReport.agencyUsername" /></td>
										<td data-title="Order Id"><s:property value="CurrentHotelReport.orderRef" /></td>
										<td data-title="Reference Code"><s:property value="CurrentHotelReport.referenceCode" /></td>
										<td data-title="Booking Status"><s:property value="CurrentHotelReport.statusAction" /></td>
										<td data-title="<s:text name="tgi.label.supplier" />">${CurrentHotelReport.apiProvider.vendorName}</td>
									</tr>
			</table></div>
			</div></div>
									 
										<%-- 	<th colspan="1"><s:text name="tgi.label.pnr" /> :
											<td>${tourOrderRow.orderReference}</td>
											</th>
											<th colspan="1"><s:text name="tgi.label.status" /> : 
											<td>${tourOrderRow.statusAction}</td>
											</th>
											<th colspan="1"><s:text name="tgi.label.supplier" /> :
											<td>${tourOrderRow.providerAPI}</td>
											</th> --%>
								  
								<div class="row">
				<div class="profile-timeline-card"> 
				<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-h-square" aria-hidden="true"></i></span> <span> <b class="blue-grey-text">Hotel Info</b></span>
									</p>
									<div class="table-responsive no-table-css clearfix">
									<table class="table table-striped table-bordered">
									<thead>
                                   <tr class="border-radius border-color">	
									<tr>
										<th><s:text name="tgi.label.hotel_name" /></th>
										<th><s:text name="tgi.label.location" /></th>
								        <th><s:text name="tgi.label.checkin" /></th>
										<th><s:text name="tgi.label.checkout" /></th> 
										<th><s:text name="tgi.label.city" /></th>
										<th><s:text name="tgi.label.country" /></th>
										<!-- <th>Hotel_type</th> -->
										<th><s:text name="tgi.label.hotel_category" /></th>

									</tr></thead>
									<tr>
										<td data-title="Hotel_name"><s:property value="CurrentHotelReport.hotelName" /></td>
										<td data-title="Location"><s:property value="CurrentHotelReport.hotel_loc" /></td>
										<td data-title="CheckIn"><s:property value="CurrentHotelReport.checkInDate" /></td>
										<td data-title="CheckOut"><s:property value="CurrentHotelReport.checkOutDate" /></td>
										<%-- <td data-title="Location"><s:property value="CurrentHotelReport.hotel_loc" /></td> --%>
										<td data-title="State"><s:property value="CurrentHotelReport.city" /></td>
										<td data-title="Country"><s:property value="CurrentHotelReport.country" /></td>
									<%-- 	<td data-title="Hotel_type"><s:property value="CurrentHotelReport.hotelType" /></td> --%>
										<td data-title="Hotel Category"><s:property value="CurrentHotelReport.hotel_cat" /></td>
									</tr>
			</table></div>
			</div></div>
			

					<!-- 		<form class="form-inline" action="" method="post"> -->


						<!-- testing -->

							<!-- <div class="box-body"> -->

							<%-- <label for="Country"><h4>
									<b><s:property value="user" /></b>
								</h4></label> --%>
							
						
									<%-- <tr>
									<th colspan="4"><big>Created By</big> : ${CurrentHotelReport.agencyUsername}</th>
									
										<th colspan="4"><big>Order Id</big> :<s:property
												value="CurrentHotelReport.orderRef" /></th>
												<th colspan="4"><big>Reference Code </big> : ${CurrentHotelReport.referenceCode}</th>
												<th colspan="4"><big>Supplier</big> : ${CurrentHotelReport.apiProvider.vendorName}</th>

									</tr> --%>
									<%-- <tr>
										<th><h5>
												<b><u>HOTEL INFO</u></b>
											</h5></th>


									</tr>
									<tr>
										<th><s:text name="tgi.label.hotel_name" /></th>
										<th><s:text name="tgi.label.location" /></th>
										<th><s:text name="tgi.label.state" /></th>
										<th><s:text name="tgi.label.country" /></th>
										<th><s:text name="tgi.label.hotel_type" /></th>
										<th><s:text name="tgi.label.hotel_category" /></th>

									</tr>
									<tr>
										<th><s:property value="CurrentHotelReport.hotelName" /></th>
										<th><s:property value="CurrentHotelReport.hotel_loc" /></th>
										<th><s:property value="CurrentHotelReport.state" /></th>
										<th><s:property value="CurrentHotelReport.country" /></th>
										<th><s:property value="CurrentHotelReport.hotelType" /></th>
										<th><s:property value="CurrentHotelReport.hotel_cat" /></th>


									</tr> --%>
								<div class="row">
											<div class="profile-timeline-card"> 
											<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-bed" aria-hidden="true"></i></span> <span> <b class="blue-grey-text">Room Info</b></span>
									</p>
									<div class="table-responsive no-table-css clearfix">
									<table class="table table-bordered table-striped-column table-hover mb-0">
									<thead>
                                   <tr class="border-radius border-color">
										<th><s:text name="tgi.label.s.no" /></th>
										<th><s:text name="tgi.label.booking_date" /></th>
										<th><s:text name="tgi.label.meal_type" /></th>
										<th><s:text name="tgi.label.hotel_name" /></th>

									</thead>
									<s:iterator value="roomInfo" status="serial">
										<tr>
											<td data-title="S.No"><s:property value="%{#serial.count}" /></td>
											
											<%-- <td data-title="Created_at">${spysr:formatDate(createdDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy hh:mm')}</td> --%>
								 <td data-title="Created_at"><s:property value="createdDate" /></td> 
											<td data-title="MealType"><s:property value="mealType" /></td>
											<td data-title="Hotel_name"><s:property value="CurrentHotelReport.hotelName" /></td>

										</tr>
									</s:iterator>
									</table></div>
									</div></div>
									
									
									
									
										<div class="row">
											<div class="profile-timeline-card"> 
											<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-user" aria-hidden="true"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.guest_info" /></b></span>
									</p>
									<div class="table-responsive no-table-css clearfix">
									<table class="table table-bordered table-striped-column table-hover mb-0">
									<thead>
                                   <tr class="border-radius border-color">
										<th><s:text name="tgi.label.s.no" /></th>
										<th><s:text name="tgi.label.gender" /></th>
										<th><s:text name="tgi.label.firstname" /></th>
										<th><s:text name="tgi.label.lastname" /></th>
										<!-- <th>Email</th> -->
										<th><s:text name="tgi.label.date_of_birth" /></th>

									</tr>
									</thead>
									<s:iterator value="roomGuestInfos" status="serial">
										<tr>

											<td data-title="S.No"><s:property value="%{#serial.count}" /></td>
											<td data-title="Gender"><s:property value="gender" /></td>
											<td data-title="Firstname"><s:property value="firstname" /></td>
											<td data-title="Lastname"><s:property value="lastname" /></td>
											<%-- <td data-title="Email"><s:property value="email" /></td> --%>
											<td data-title="DOB">${spysr:formatDate(DOB,'MM-dd-yyyy', 'MMM-dd-yyyy')}</td>

										</tr>
									</s:iterator>
									</table></div>
									</div></div>
									
									<div class="row">
											<div class="profile-timeline-card"> 
											<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-phone" aria-hidden="true"></i></span> <span> <b class="blue-grey-text">Contact Info</b></span>
									</p>
									
									 <div class="table-responsive no-table-css clearfix">
									<table class="table table-bordered table-striped-column table-hover mb-0">
									<thead>
                                   <tr class="border-radius border-color">
										<th><s:text name="tgi.label.title" /></th>
										<th><s:text name="tgi.label.name" /></th>
										<!-- <th>Lastname</th> -->
										<th><s:text name="tgi.label.mobile" /></th>
										<th><s:text name="tgi.label.address" /></th>
										<th><s:text name="tgi.label.email" /></th>
									</tr>
									</thead>
										<tr>
											<td data-title="Title">${hotelOrderRowData.orderCustomer.title}</td>
											 <td data-title="Name">${hotelOrderRowData.orderCustomer.firstName} ${hotelOrderRowData.orderCustomer.lastName}</td>
											<td data-title="Mobile">${hotelOrderRowData.orderCustomer.mobile}</td>
											<td data-title="Address">${hotelOrderRowData.orderCustomer.address}</td>
											<td data-title="Email">${hotelOrderRowData.orderCustomer.email}</td> 
										</tr>
									</table></div>
									</div></div>
									
									
									<div class="row">
										<div class="profile-timeline-card"> 
										<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-times-circle" aria-hidden="true"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.cancellation_info" /></b></span>
									</p>
									 
									<div class="table-responsive no-table-css clearfix">
									<table class="table table-bordered table-striped-column table-hover mb-0">
									<thead>
                                   <tr class="border-radius border-color">
										<th><s:text name="tgi.label.s.no" /></th>
										<th><s:text name="tgi.label.start" /></th>
										<th><s:text name="tgi.label.end" /></th>
										<th><s:text name="tgi.label.fee_type" /></th>
										<th><s:text name="tgi.label.fee_amount" /></th>
										
										<th><s:text name="tgi.label.amount" /></th>
										<th><s:text name="tgi.label.currency" /></th>
										<th><s:text name="tgi.label.remarks" /></th>
									</tr></thead>
									<s:iterator value="cancellationPoliciesInfo"
										status="serial">
										<tr>

											<td data-title="S.No"><s:property value="%{#serial.count}" /></td>
											<td data-title="Start"><s:property value="startDate" /></td>
											<td data-title="End"><s:property value="endDate" /></td>
											<td data-title="Fee_amount"><s:property value="formattedFeeAmount" /></td>
											<td data-title="Amount"><s:property value="feeAmount" /></td>
											<td data-title="Fee_type"><s:property value="feeType" /></td>
											
											<td data-title="Currency"><s:property value="curCode" /></td>
											<td data-title="Remarks"><s:property value="remarks" /></td>
										</tr>
									</s:iterator>
									</table></div>
									</div></div>
									
									<s:if test="insuranceDataList.size()>0"> 
									<div class="row">
											<div class="profile-timeline-card"> 
											<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-user-plus" aria-hidden="true"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.flight_insurance" /></b></span>
									</p>
									
									<div class="table-responsive no-table-css clearfix">
									<table class="table table-bordered table-striped-column table-hover mb-0">
									<thead>
                                   <tr class="border-radius border-color">
										<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
										
									</tr>
									</thead>
									<s:iterator value="insuranceDataList">
										<tr>
											<td data-title="><s:text name="tgi.label.insurance_id" />">${insuranceId}</td>
											<td data-title="<s:text name="tgi.label.name" />">${insuranceTitle}</td>
											<td data-title="<s:text name="tgi.label.price" />">${insurancePrice}</td>
											<td data-title="<s:text name="tgi.label.description" />">${description}</td>
											
										</tr></s:iterator>
									</table> 
									</div>
									</div></div>
									</s:if>
									<s:else>
									<div class="row">
										<div class="profile-timeline-card"> 
										<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-user-plus" aria-hidden="true"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.flight_insurance" /></b></span>
									</p> 
											<table class="table table-bordered in-table-border no-table-css mb-0">
												 <tr class="border-radius border-color">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.insurance_not_available" /></td>
												</tr>
											</table>
											</div></div>
										</s:else>
 
									
									<div class="row">
										<div class="profile-timeline-card"> 
										<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-list-alt" aria-hidden="true"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.order_summary" /></b></span>
									</p> 
									<div class="table-responsive no-table-css clearfix mb-0">
									<table class="table table-bordered table-striped-column table-hover mb-0">
									<thead>
                                   <tr class="border-radius border-color">
										<th><s:text name="tgi.label.guests" /></th>
										<th><s:text name="tgi.label.base_price" /></th>
										<th><s:text name="tgi.label.processing_fee" /></th>
										<th><s:text name="tgi.label.insurance_fee" /></th>
										<th><s:text name="tgi.label.total_price" /></th>
									</tr></thead>
										<tr>
										<td data-title="<s:text name="tgi.label.guests" />"><s:property value="CurrentHotelReport.guests" /></td>
										<td data-title="<s:text name="tgi.label.base_price" />"><s:property value="CurrentHotelReport.base_price" /></td>
										<td data-title="<s:text name="tgi.label.processing_fee" />"><s:property value="CurrentHotelReport.fee_amount" /></td>
										<td data-title="<s:text name="tgi.label.insurance_fee" />">
										<c:choose>
										<c:when test="${CurrentHotelReport.insurancePrice != null}">
										<s:property value="CurrentHotelReport.insurancePrice" />
										</c:when>
										<c:otherwise>
										<s:text name="tgi.label.n_a" />
										</c:otherwise>
										</c:choose>
										</td>
										
										
										<td data-title="<s:text name="tgi.label.total_price" />"><s:property value="CurrentHotelReport.total" />&nbsp;<s:property value="CurrentHotelReport.curCode" /></td>
										</tr>
								
								<%-- 	<tr>
									
										<th colspan="5"><h4>
												<b><s:text name="tgi.label.Guests_:" /> <s:property
														value="CurrentHotelReport.guests" /></b>
											</h4></th>

										<th align="right"><h4>
												<b><s:text name="tgi.label.Total:" /><s:property value="CurrentHotelReport.total" />
													<s:property value="CurrentHotelReport.curCode" /></b>
											</h4></th>
									</tr>
									<tr> --%>


									<!-- <tr>

										<th colspan="5"><button type="button" id="orderNow"
												class="btn btn-primary">Alter Order Now</button></th>
								</tr>  -->
									 </table>
									</div>
									</div></div>


							<!-- /.box -->

						
							<form action="insertHotelOrderModifiedInfo"  method="post"  >
								<s:if test="hasActionErrors()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionerror />
						</div>  
					</s:if>
					<s:if test="hasActionMessages()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionmessage />
						</div>
					</s:if>
							<table id="editOrder" style="border: 2px solid #2283E1;display: none"
								class="table table-bordered table-striped-column table-hover mb-0">


								<tr>

									<td><h4><s:text name="tgi.label.booking_status" /></h4> <select name="statusAction"
										id="statusAction" autocomplete="off" required>
											<option selected="selected" selected="selected" value=""><s:text name="tgi.label.select_booking_status" /></option>
											  <option value="failed"><s:text name="tgi.label.failed" /></option>
											  <option value="reserved"><s:text name="tgi.label.reserved" /></option>
											<option value="confirmed"><s:text name="tgi.label.confirmed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option>
									</select>
								 
											<input type="hidden"  name="flightOrderRowId"  value="<s:property value="CurrentHotelReport.id"/>">
											<input type="hidden"  name="userId"  value="<s:property value="CurrentHotelReport.userId"/>">
											<input type="hidden"  name="orderId"  value="<s:property value="CurrentHotelReport.orderRef"/>">
								  <input type="hidden"  name="totalBookingAmount" value="<s:property
									 value="CurrentHotelReport.totAmountSpent"/>">
									   <input type="hidden"  name="updatedBy" value="<s:property
									 value="%{#session.User.Username}"/>">
										 </td>

									<td><h4><s:text name="tgi.label.payment_status" /></h4> <select name="paymentStatus"
										id="paymentStatus"    autocomplete="off" required> 
											<option selected="selected" value=""><s:text name="tgi.label.select_payment_status" /></option>
											<option value="success"><s:text name="tgi.label.success" /></option>
											<option value="failed"><s:text name="tgi.label.failed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option>
											<option value="refund"><s:text name="tgi.label.refund" /></option>

									</select></td>
									<td><h4><s:text name="tgi.label.conveniencefees" /></h4> <input type="text" name="convenienceFees"
										id="convenienceFees"    autocomplete="off" required> 
										 </td>
										 <td><h4><s:text name="tgi.label.cancellationfees" /></h4> <input type="text" name="cancellationFees"
										id="cancellationFees"    autocomplete="off" required> 
										 </td>
									<td>
										<h4><s:text name="tgi.label.emp_comments" /></h4> <textarea name="employeeComments" > </textarea>

									</td>

									<td>
									<%-- 	<h4>Api Comments</h4> <textarea  disabled="disabled"><s:property
												value="%{#session.ReportData.apiComments}" /></textarea>
 --%>
									<%-- <input type="hidden" name="apiComments" value="<s:property
												value="%{#session.ReportData.apiComments}" />"> --%>
									
									
									
									</td>

									<td><h4><s:text name="tgi.label.action" /></h4>
										<button type="submit" class="btn btn-primary"><s:text name="tgi.label.order_now" /></button></td>
								</tr>

							</table>
						</form>
						<!-- table-responsive -->
					<!-- </form> -->
					</section>
					</div></div>
</div>
</section>
			<!-- /.row -->
	<script>
		$(document).ready(function() {
			$("#twodpd2").datepicker({
				dateFormat : "yy-mm-dd"
			});
			$("#twodpd1").datepicker({
				dateFormat : "yy-mm-dd"
			/*  changeMonth: true,
			 changeYear: true */
			});
		});
	</script>
	<script type="text/javascript">
		/* $(document).ready(
				/* function() {
					var table = $('#example').DataTable({
						lengthChange : false,
						   "searching": false,
						   "ordering": false,  
						   "paging": false,
						   "info": false,
						/* "pagingType" : "full_numbers",
						"lengthMenu" : [ 10, 25, 50, 75, 100, 500 ], */
						/* buttons : [ 'excel', 'pdf', 'print' ] */
					/* });

					table.buttons().container().appendTo(
							'#example_wrapper .col-sm-6:eq(0)');

				});   */

		/*  $(function () {
		 	$('#example').DataTable({
		    	 "paging": true,
		         "lengthChange": true,
		        "searching": true,
		        "ordering": true,  
		           "info": true,
		         "autoWidth": false,  
		        "search": {
		      	    "regex": true,
		      	  }, 
		      	 
		      "pagingType": "full_numbers",
		      "lengthMenu": [10, 25, 50, 75, 100, 500 ],
		     
		      
		     });  
		  
		   });   */
	</script>

<script type="text/javascript">
		$(document).ready(function() {
			$("#orderNow").click(function() {
				$("#editOrder").toggle("slow", function() {

				});
			});
		});
	</script>
	
	
	<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"";
	$('#success').click(function() {
	// window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
	
	
	<%-- <script type="text/javascript">
   $(function(){
	   var txt = $('.success-alert').text();
	   var protocol=location.protocol;
   	   var host=location.host;
   	   //var url=protocol+"//"+host+"/LintasTravelAdmin/searchSuperUserHotelOrderList";
	   if(txt.length>0){
    	   alert(txt);
    	  // window.location.assign(url);
    	 
    	   
     }
    });
 </script> --%>
	
	<!-- 
 
  $(document).ready(function() 
    	 { 
    		 $("#twodpd1").datepicker({
    			 dateFormat: "yy-mm-dd"  
    			/*  changeMonth: true,
    			 changeYear: true */
    		 }); 
    		 }); -->

