<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.show_date_to_user {
    padding: 0px 15px;
    position: relative;
    width: 93%;
    top: -25px;
    left: 5px;
    pointer-events: none;
    display: block;
    color: #000;
}

.scrollbar
{
	margin-left: 30px;
	float: left;
	height: 300px;
	width: 65px;
	background: #F5F5F5;
	overflow-y: scroll;
	margin-bottom: 25px;
}

.force-overflow
{
	min-height: 450px;
}

#wrapper
{
	text-align: center;
	width: 500px;
	margin: auto;
}


#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}


</style>
    <!--************************************
        MAIN ADMIN AREA
    ****************************************-->
        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">
                        <div class="card1">
                        <div class="pnl">
                    <div class="hd clearfix">
					<h5 class="pull-left">Company List</h5>
					<div class="set pull-right hidden-xs" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
				</div>
                               <br>
                            <form action="listCompanyFilter" class="filter-form" method="post">
							    <div class="" id="filterDiv" style="display:none;" >
                                <div class="row">
                               <s:if test="%{#session.User.userRoleId.isSuperUser()}">
                                <div class="col-md-2 form-group">
                                <select name="companyShowData" id="companyShowData" class="form-control input-sm">
	                                <option value="" selected="selected">Search By Created From</option>
	                                <s:if test="%{#session.User.userRoleId.isSuperUser()}">
	                                	<option value="all"><s:text name="tgi.label.all" /></option>
	                                </s:if> 
	                                <option value="myself"><s:text name="tgi.label.my_self" /></option> 
	                                <option value="distributor"><s:text name="tgi.label.distributor" /></option> 
                                </select>
                                </div>
                               </s:if>
                                <div class="col-md-2 col-sm-6 form-group">
                                <select name="companyType" id="company-role" class="form-control input-sm">
                                <option value="" selected="selected"><s:text name="tgi.label.select" /> <s:text name="tgi.label.agency" /></option>
                                <s:if test="%{#session.User.userRoleId.isSuperUser()}">
                                <option value="all"><s:text name="tgi.label.all" /></option>
                                </s:if>
                                <option value="distributor"><s:text name="tgi.label.distributor" /></option>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div>
	                               <div class="col-md-2 col-sm-6">
								   <div class="form-group">
								<input type="text" name="createdDateRange" id="created-date-range" placeholder="Search by Created Date..." class="form-control input-sm" />
								 </div></div>
                                <div class="col-md-2 col-sm-6 form-group">
									<input type="text" name="companyName" id="company-name-json" placeholder="Company Name...." class="form-control search-query input-sm" />
                                </div>
                                <div class="col-md-2 col-sm-6 form-group">
                                 
                                <input type="text" name="email" id="email-json" placeholder="Search By Email...." class="form-control search-query input-sm" />
                                </div>
                                <div class="col-md-2 col-sm-6 form-group">
                                 
                                <input type="text" name="mobile" id="phone-json" placeholder="Search By Phone...." class="form-control input-sm" />
                                </div>
                                </div>
                                <div class="col-md-2 col-sm-6 form-group">
                                 
                                <input type="text" name="companyDetail.city" id="city-json" placeholder="Search By City...." class="form-control hasdatepicker input-sm" />
                                </div>
                                <div class="col-md-2 col-sm-6 form-group">
                                 
                                <input type="text" name="companyDetail.countryName" id="country-json" placeholder="Search By Country...." class="form-control hasdatepicker input-sm" />
                                </div>
                                <div class="col-md-2 col-sm-6 form-group">
                                <input type="text" name="companyUserId" id="companyId-json"
												placeholder="Search By Company Id...."
												class="form-control hasdatepicker input-sm" />
                                </div>
                                <div class="col-md-2 col-sm-6 form-group">
                      					<select class="form-control input-sm" name="companyVisibilityLevelData">
                                               <option value="">Select Company Level</option>
                                               <option value="b2b">B2B</option>
                                               <option value="b2c">B2C</option>
                                               <option value="b2b2c">B2B2C</option>
                                               <option value="b2b2b2c">B2B2B2C</option>
                                        </select>
                                </div>
                                <div class="col-md-2 col-sm-6 form-group">
                                 
                                <select name="flagTypeStatus" id="flagTypeStatus" class="form-control input-sm">
                               	<option value="" selected="selected">Select Company Status</option>
                                <option value="true">Active</option>
                                <option value="false">Inactive</option> 
                                </select>
                                </div>
                                <div class="row">
                               <div class="col-md-1"> 
                               <div class="form-group"> 
								<input type="reset" class="btn btn-sm btn-danger btn-block" id="configreset" value="Clear">
								</div>
								</div>
								<div class="col-md-1">
								<div class="form-group">
								<input type="submit" class="btn btn-sm btn-info btn-block" value="Search" />
								</div>
                                </div>
                                </div>
                                </div>
                                </form>
				 <div class="cnt cnt-table">
				<div class="dash-table horizontal">
							<div class="content pt-0 mt-0">
								<div id="example_wrapper" class="dataTables_wrapper ">
										<table id="example" class="display dataTable responsive nowrap"  role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
                                        <thead>
                                        <tr class="table-sub-header">
                                        	<th colspan="13" class="noborder-xs">
                                        	<div class="pull-right">   
                                        	<div class="btn-group"> 
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Company">
											  <a class="btn btn-sm btn-default" href="addCompany" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class="">&nbsp;New&nbsp;</span></a> 
											</div>
											<div class="dropdown pull-left" style="margin-right:5px;">
											  <a class="btn btn-sm btn-default " href="#" >&nbsp;Import&nbsp;</a> 
											</div>
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a  class="btn btn-sm btn-default filterBtn" data-toggle="dropdown"  style="margin-bottom: 2px;margin-right: 3px;" title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
                                        <tr class="border-radius border-color" role="row">
                                                    <th style="width: 16px;" data-priority="1"></th>
                                                   	<th style="width: 50px;" data-priority="2">
															<div class="center">
															<div data-toggle="tooltip" data-placement="top" title="Select All">
															<div class="checkbox checkbox-default">
										                        <input id="master_check" data-action="all" type="checkbox" >
										                        <label for="master_check">&nbsp;</label>
										                    </div>
										                    </div>
										                    </div>
													</th>
                                                    <th data-priority="3">Company Name</th>
                                                    <th data-priority="4">Company Type</th>
                                                    <th data-priority="5">Email</th>
                                                    <th data-priority="6">Mobile</th>
                                                    <th data-priority="7">City</th>
                                                    <th data-priority="8">Country</th>
                                                    <th data-priority="9" style="width: 90px;">Company Id</th>
                                                    <th data-priority="9" style="width:85px">Mail Status</th>
                                                    <th data-priority="10" style="width:133px">Created Date</th>
                                                    <th data-priority="10">Status</th>
													<th data-priority="12" style="width: 30px"><img class="clippy" src="admin/img/svg/settings.svg" width="16" alt="Settings"></th>
                                                     <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              <s:iterator value="companyList" status="rowstatus" var="company">
                                                <tr>
                                               <td scope="row" data-title="S.No"><div class="center"><s:property value="%{#rowstatus.count}" /></div></td>
                                                <td data-title="Select" class="select-checkbox">
																<div class="center">
																<div class="checkbox checkbox-default">
										                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${id}" class="check_row">
										                        <label for="checkbox1_sd_${rowstatus.count}"></label>
										                   		</div>
										                   		</div>
														</td>
														<td class="link"><a href="companyDetails?id=${id}" class="plus-icon"  ><s:property value="companyName" /></a> </td>
														
														<td data-title="Company Type">
														<c:choose>
														<c:when test="${company.companyRole.agent==true}">
															Agency
														</c:when>
														<c:when test="${company.companyRole.distributor==true}">
															Wholesaler
														</c:when>
														<c:when test="${company.companyRole.corporate==true}">
															Corporate
														</c:when>
														<c:otherwise>
														Superuser
														</c:otherwise>
														</c:choose>
														</td>
														<td data-title="Email"><s:property value="email" /> </td>
														<td data-title="Mobile"><s:property value="mobile" /></td>
														<td data-title="User">${companyDetail.city}</td>
														<td data-title="Country">${companyDetail.countryName}</td>
														<td data-title="Company Id"><s:property value="companyUserId" /></td>
														<td data-title="User">
														<c:choose>
														<c:when test="${company.verifyStatus != false}">
														<span class="text-success"><i class="fa fa-check-circle"></i> Verify</span>
														</c:when>
														<c:otherwise>
														<span class="text-danger"><i class="fa fa-times-circle"></i> Not Verify</span>
														</c:otherwise>
														</c:choose>
														</td>
														<td>${spysr:formatDate(createdDate,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}</td>
														<td class="block-highlight" > <label class="switch"> ${status}<input type="checkbox" data-id="${id}" data-flag-type="STATUS" class="company_status_toggle" ${status ==true?'checked':''}>
															<span class="slider"></span>
															</label>
														</td>
														<td>
														<div class="center">
															<div class="dropdown pull-right" style="margin-right: 10px;">
															  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
															  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
															  <ul class="dropdown-menu">
															    <li class="action">
															    <a href="companyDetails?id=${id}" class="plus-icon"  >Company Details </a>
															    </li>
															    <li class="action">
															    <a  href="getApprovalCompaniesList"  class="plus-icon">Company Approval List</a>
															    </li>
															    <li class="action"><a href="deleteWholeCompanyData?id=${id}">Delete</a></li>
															  </ul>
															</div>
														</div>
														</td>
														<td></td>
                                                		</tr>
                                                </s:iterator>
                                            </tbody>
                                        </table></div>
                                    </div>
								</div></div></div></div>
            </section>
       
<script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>	
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"superUserCompanyList";
	$('#success').click(function() {
	 window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
  <script>
	
	// company name
	var CompanyName = {
		url: "getCompanyJson.action?data=companyName",
		getValue: "companyName",
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#company-name-json").easyAutocomplete(CompanyName);
	
 // email
		var Email = {
			url: "getCompanyJson.action?data=email",
			getValue: "email",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#email-json").easyAutocomplete(Email);
		
		// phone
		var Phone = {
			url: "getCompanyJson.action?data=mobile",
			getValue: "mobile",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#phone-json").easyAutocomplete(Phone);
		
		// country name
		var CountryName = {
			url: "getCompanyJson.action?data=countryName",
			getValue: "countryName",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#country-json").easyAutocomplete(CountryName);
		
		// city name
		var City = {
			url: "getCompanyJson.action?data=city",
			getValue: "city",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#city-json").easyAutocomplete(City);
		
		// companyId
		var CompanyUserId = {
			url: "getCompanyJson.action?data=companyUserId",
			getValue: "companyUserId",
			list: {
				match: {
					enabled: true
				}
			}
		};
		console.debug(CompanyUserId);
		$("#companyId-json").easyAutocomplete(CompanyUserId);
		
	</script>	
  
 <script>
 $('.company_status_toggle').change(function () {
    var status = false;
    var id = $(this).data("id");
    var flagType = $(this).data("flag-type");
    if($(this).is(":checked")==false)
     {
      status= false;
      $(this).attr('checked', false);
     }
    else{
      status= true;
      $(this).attr('checked', true);
     }
    $.ajax({
    	url : "update_company_profile_status?statusFlag="+status+"&flagType="+flagType+"&id="+id,
		type : "POST",
		dataType : 'json',
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
			}
			else if(jsonData.message.status == 'error'){
				 //alertify.set('notifier','position', 'top-right');
				 alertify.error(jsonData.message.message);
			}
			else if(jsonData.message.status == 'input'){
				 alertify.error(jsonData.message.message);
			}
			
		},
		error : function(request, status, error) {
			alertify.error("user status can't be changed ,please try again");
		}
	});
    });

</script>