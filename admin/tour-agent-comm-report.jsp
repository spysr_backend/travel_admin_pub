<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
    
    
	
<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.show_date_to_user {
    padding: 0px 15px;
    position: relative;
    width: 93%;
    top: -25px;
    left: 5px;
    pointer-events: none;
    display: inherit;
    color: #000;
}
</style>      
   <!--************* MAIN ADMIN AREA **********************-->
            <section class="wrapper container-fluid">
                        <div class="pnl">
                        <div class="card1">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.tour_commission_report" /></h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
                                       <div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="tourReportList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.report_list" /> </a></li>
									<li class=""><a class="" href="tourOrderList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.order_list" /> </a></li>
									<li class=""><a class="" href="tourCommissionReport"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_report" /></a></li>
									<li class=""><a class="" href="tourCustomerInvoiceList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.custommer_invoice" /> </a></li>
									<li class=""><a class="" href="tourAgentCommInvoiceList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_invoice" /></a></li>
								</ul>
								</div>
                                    </div> 
                               </div>
                                <div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
                               </div> <br>
                              <form action="tourCommissionReport" class="filter-form" id="filterBtn">
							<div class="" id="filterDiv" style="display: none;">
							<div class="form-group">
							  <s:if test="%{#session.Company!=null}">
         						<s:if test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
                                 <div class="col-md-2">
                                 <div class="form-group">
                                <input type="hidden" id="HdncompanyTypeShowData" value="" />
                                <select name="companyTypeShowData" id="companyTypeShowData" class="form-control input-sm">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="my"><s:text name="tgi.label.my_self" /></option>
                                <s:if test="%{#session.Company!=null}">
         						<s:if test="%{#session.Company.companyRole.isSuperUser()}">
                                 <option value="distributor"><s:text name="tgi.label.distributor" /></option>
                                 </s:if>
                                </s:if>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div></div>
                                </s:if>
                                </s:if>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="orderId" id="orderid-json"
									placeholder="Search By Order Id...."
									class="form-control search-query input-sm" />
							   </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="bookingStartDate" id="bookingStartDate"
												placeholder="Start Booking Date...."
												class="form-control search-query date1 input-sm " />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="bookingEndDate" id="bookingEndDate"
												placeholder="End Booking Date...."
												class="form-control search-query date2 input-sm" />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="travelStartDate" id="travelStartDate"
												placeholder="Tour Start Date...."
												class="form-control search-query date3 input-sm" />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="travelEndDate" id="travelEndDate"
												placeholder="Tour End Date........"
												class="form-control search-query date4 input-sm" />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="pnr" id="hotelName-json"
									placeholder="Tour PNR" class="form-control search-query input-sm" />
							 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="tourTitle" id="hotelName-json"
									placeholder="Tour Title" class="form-control search-query input-sm" />
							 </div></div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="firstName" id="customerFirstName"
												placeholder="Passenger FirstName..."
												class="form-control search-query input-sm" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="lastName" id="customerLastName"
												placeholder="Passenger LastName..."
												class="form-control search-query input-sm" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="email" id="customerEmail"
												placeholder="Passenger Email"
												class="form-control search-query input-sm" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="mobile" id="customerMobile"
												placeholder="Passenger Mobile"
												class="form-control search-query input-sm" />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="supplier" id="hotelName-json"
									placeholder="Tour Supplier" class="form-control search-query input-sm" />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="createdBy" id="agency-json"
									placeholder="Agency..." class="form-control search-query input-sm" />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select name="paymentMode" id="paymentMode" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_payment_method" /></option>
                                <option value="ALL"><s:text name="tgi.label.all" /></option>
                             	<option value="CHECK"><s:text name="tgi.label.CHECK" /></option>
                                <option value="CREDIT_CARD"><s:text name="tgi.label.CREDIT_CARD" /></option>
                                <option value="DEBIT_CARD"><s:text name="tgi.label.DEBIT_CARD" /></option>
                                <option value="WALLET"><s:text name="tgi.label.WALLET" /></option>
                                </select>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select name="paymentModeType" id="paymentModeType" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_payment_mode" /></option>
								<option value="ALL"><s:text name="tgi.label.all" /></option>
                                <option value="SQUAREUP" ><s:text name="tgi.label.squareUp" /></option>
                             	<option value="EXTERNALLINK_TEST"><s:text name="tgi.label.external" /></option>
                                <option value="AUTHORIZE"><s:text name="tgi.label.authorize" /></option>
                                <option value="WALLET"><s:text name="tgi.label.WALLET" /></option>
                                <option value="WIRECARD"><s:text name="tgi.label.wirecard" /></option>
                                <option value="STRIPE"><s:text name="tgi.label.stripe" /></option>
                                </select>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select name="paymentStatus" id="paymentStatus" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_payment_status" /></option>
								<option value="ALL"><s:text name="tgi.label.all" /></option>
                                <option value="PENDING" ><s:text name="tgi.label.PENDING" /></option>
                             	<option value="SUCCESS"><s:text name="tgi.label.SUCCESS" /></option>
                                <option value="FAILED"><s:text name="tgi.label.FAILED" /></option>
                                <option value="PROCESSING"><s:text name="tgi.label.PROCESSING" /></option>
                                <option value="DECLINED"><s:text name="tgi.label.DECLINED" /></option>
                                <option value="REFUND"><s:text name="tgi.label.REFUND" /></option>
                                </select>
								</div> 
								</div>
								<div class="row">
								<div class="col-md-2 col-sm-6">
								<select name="bookingStatus" id="bookingStatus" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_booking_status" /></option>
								<option value="ALL"><s:text name="tgi.label.all" /></option>
                                <option value="PENDING" ><s:text name="tgi.label.PENDING" /></option>
                             	<option value="CONFIRM"><s:text name="tgi.label.CONFIRM" /></option>
                                <option value="FAILED"><s:text name="tgi.label.FAILED" /></option>
                                <option value="PROCESSING"><s:text name="tgi.label.PROCESSING" /></option>
                                <option value="CANCELLED"><s:text name="tgi.label.CANCELLED" /></option>
                                </select>
								 </div></div>
							     <div class="col-md-1">
								<div class="form-group">
								<input type="reset" class="btn btn-danger btn-block" id="configreset" value="Clear">
							</div>
							</div>
								<div class="col-md-1">
								<div class="form-group">
								<input type="submit" class="btn btn-info btn-block" value="Search" />
							</div>
							</div>
						</div></div>
				</form>
						    <div class="cnt cnt-table">
			<div class="content mt-0 pt-0">
				<!-- <h1 class="page_title">File export</h1> -->
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap" role="grid" aria-describedby="example_info" style="width: 100%;">
                                            <thead>
                                            	 <tr class="border-radius border-color hidden-xs">
                                            	<th></th>
                                            	<th></th>
                                            	<th></th>
                                            	 <th><s:text name="tgi.label.markup" /></th>
                                            	 <s:if test="logedInCompany == 'superuser'">
                                            	 <th></th>
                                            	 </s:if>
                                            	  <s:if test="logedInCompany != 'agent'">
                                            	 <th></th>
                                            	 </s:if>
                                            	 <th><s:text name="tgi.label.amount" /></th>
                                            	 <th></th>
                                            	 <th></th>
                                            	  <s:if test="logedInCompany != 'agent'">
                                            	  <th></th>
                                            	  </s:if>
                                            	   <th><s:text name="tgi.label.commission" /></th>
                                            	   <th></th>
                                            	    <th></th>
                                            	    <th></th>
                                            	    <th></th>
                                            	 </tr>
                                                <tr class="border-radius border-color">
                                                    <th data-priority="1"><s:text name="tgi.label.s_no" /></th>
                                                      <th data-priority="3"><s:text name="tgi.label.booking_date" /></th>
                                                    <th data-priority="2"><s:text name="tgi.label.orderid" /></th>
                                                    <th data-priority="4"><s:text name="tgi.label.my_markup" /></th>
                                                    <s:if test="logedInCompany == 'superuser'">
                                                    <th data-priority="5"><s:text name="tgi.label.wholesaler_markup" /></th>
                                                    </s:if>
                                                    <s:if test="logedInCompany != 'agent'">
                                                 	<th data-priority="6"><s:text name="tgi.label.agency_markup" /></th>
                                                 	</s:if>
                                                	 <th data-priority="7"><s:text name="tgi.label.ticket" /></th>
													<th data-priority="8"><s:text name="tgi.label.total" /> </th>
													<th>My Commission</th>
													 <s:if test="logedInCompany != 'agent'">
                                                    <th data-priority="9"><s:text name="tgi.label.shared" /></th>
                                                    </s:if>
                                                    <th data-priority="10"><s:text name="tgi.label.my_revenue" /></th>
                                                    <th data-priority="11"><s:text name="tgi.label.payment_status" /></th>
                                                    <th data-priority="12"><s:text name="tgi.label.booking_status" /></th>
                                                    <th data-priority="13"><s:text name="tgi.label.supplier" /></th>
                                                    <th data-priority="14"><s:text name="tgi.label.agency" /></th>
                                                  	<th></th>
                                                   
                                               
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <s:if test="tourCommissionReportList.size>0">
												<s:iterator value="tourCommissionReportList"
													status="rowCount">
													<tr>
														<td><s:property value="%{#rowCount.count}" /></td>
														<td><s:property value="bookingDate" /></td>
														<td><s:property value="orderId" /></td>
														<td><s:property value="markup" /></td>
														<s:if test="logedInCompany == 'superuser'">
														<td><s:property value="distributorMarkup" /></td>
														</s:if>
														<s:if test="logedInCompany != 'agent'">
														<td><s:property value="childMarkup" /></td>
														</s:if>
														<td><s:property value="ticketPrice" /></td>
														<td><s:property value="finalPrice" /></td>
														<td><s:property value="myCommission" /></td>
														 <s:if test="logedInCompany != 'agent'">
														<td><s:property value="sharedCommission" /></td>
														</s:if>
														<td><s:property value="myProfit" /></td>
														<td><s:property value="paymentStatus" /></td>
														<td><s:property value="status" /></td>
														<td><s:property value="supplier" /></td>
														<td><s:property value="createdBy" /></td>
														<td></td>
														
													</tr>
												</s:iterator>
												</s:if>
                                            </tbody>
                                            <tbody class= "hidden-xs">
														<tr>
															<th></th>
															<th></th>
															<th></th>
															
															<th><s:text name="tgi.label.total_self" /></th>
															<s:if test="logedInCompany == 'superuser'">
															<th><s:text name="tgi.label.total_wholesaler" /></th>
															</s:if>
															<s:if test="logedInCompany != 'agent'">
															<th><s:text name="tgi.label.total_agency" /></th>
															</s:if>
															<th><s:text name="tgi.label.total_ticket" /></th>
															<th><s:text name="tgi.label.total_booking" /></th>
															<th><s:text name="tgi.label.total_my" /></th>
															 <s:if test="logedInCompany != 'agent'">
															  <th><s:text name="tgi.label.total_shared" /></th>
															  </s:if>
															 <th> 
															<s:text name="tgi.label.total_revenue" /> 
															   </th>
															  <th> 
															  </th>
															  <th> 
															  </th>
															 
														 </tr>
														<tr>
															<th></th>
															<th></th>
															<th></th>
															
 														<th><s:property
																value="tourAgentagentTotalReportCommObj.totMyMarkup"/></th>
														<s:if test="logedInCompany == 'superuser'">
														<th><s:property
																value="tourAgentagentTotalReportCommObj.totWholeSalerMarkup"/></th>
														</s:if>
														<s:if test="logedInCompany != 'agent'">
														<th><s:property
																value="tourAgentagentTotalReportCommObj.totAgencyMarkup"/></th>
														</s:if>
														<th><s:property
																value="tourAgentagentTotalReportCommObj.totTicketPrice"/></th>
														<th><s:property
																value="tourAgentagentTotalReportCommObj.totAmountSpent"/></th>
														 
														 <th><s:property
																value="tourAgentagentTotalReportCommObj.totMyCommission"/></th>
 														 <s:if test="logedInCompany != 'agent'">
 														<th><s:property
																value="tourAgentagentTotalReportCommObj.totSharedCommission"/></th>
														</s:if>
															 <th><s:property
																	value="tourAgentagentTotalReportCommObj.totAgentSComm" />
															</th>
															 
															<th></th>
															 <th></th>
														 </tr>
 														</tbody>
                                        </table>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
            </section>
	<script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>	