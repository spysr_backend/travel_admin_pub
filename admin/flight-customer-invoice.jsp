<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

	<section class="wrapper container-fluid">
                <div class="row">
                    <div class="">
                        <div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
					<s:text name="tgi.label.flight_customer_invoice" />
					</h5>
						<div class="set pull-right">
						<!-- <button class="btn btn-success btn-xs"
						onclick="sendCustomerInvoiceToCustomer();">
						Send <i class="fa fa-arrow-circle-right"></i>
					</button>
					<button type="button" class="btn btn-success btn-xs" id="h4"
						style="display: none">
						sending-------<i class="fa fa-arrow-circle-right"></i>
					</button> -->
					
					<button class=" btn btn-success btn-xs"
						onclick="sendCustomerInvoiceToCustomer();">
						<s:text name="tgi.label.send_invoice" /> <span class="glyphicon glyphicon-send"></span>
					</button>
					<button type="button" class="btn btn-success btn-xs" id="h4"
						style="display: none">
						<s:text name="tgi.label.sending" />-----<i class="fa fa-arrow-circle-right"></i>
					</button>
				</div>
					<div class="set pull-right">

						<a class="btn btn btn-primary btn-xs"
							href="flightCustomerInvoiceList"><span class="pull-right">
								<span class="glyphicon glyphicon-step-backward"></span>
							<s:text name="tgi.label.back" />
						</span></a>

					</div>
					
					<div class="set pull-right">
						<a href="<%=request.getContextPath()%>/flightInvoice?orderId=${orderId}" target="blank" class="popup btn btn-info btn-xs">
						<span class="pull-right">
						<i class="fa fa-print" aria-hidden="true"></i> Print
						</span></a>
					</div>
				</div>
				
				
				<div class="col-md-12 mt-2"> 
				<div class="profile-timeline-card">   
								<i class="fa fa-user header-icons timeline-img-user pull-left" style="box-shadow: none;"></i> 
						<div class="">
							<p class="mt-2 mb-4">
								<span> <b class="blue-grey-text">${invObj.company.companyName}</b></span>
							</p> 
						</div>
						<hr>  
						 
						<div class="timeline-footer row row-minus"> 
<div class="col-md-4"> 
<h4 class="text-left"><span>From</span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b>Address</b></td>
            <td> ${invObj.company.address}</td>
          </tr>
          <tr>
            <td><b>Tel</b></td>
            <td>${invObj.company.phone}</td>
          </tr> 
          <tr>
            <td><b>Email</b></td>
            <td>${invObj.company.email}</td>
          </tr> 
          <tr>
            <td><b>Website</b></td>
            <td>${invObj.company.website}</td>
          </tr>
          </tbody>
        </table>
     </div>     
     <div class="col-md-4">
     <h4 class="text-left"><span><s:text name="tgi.label.to" /></span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b><s:text name="tgi.label.to" /></b></td>
            <td>${invObj.flightOrderRow.flightCustomer.firstName} ${invObj.flightOrderRow.flightCustomer.lastName}</td>
          </tr>
          <tr>
            <td><b><s:text name="tgi.label.address" /></b></td>
            <td>${invObj.flightOrderRow.flightCustomer.address}</td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.tel" /></b></td>
            <td>${invObj.flightOrderRow.flightCustomer.mobile}</td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.email" /></b></td>
            <td>${invObj.flightOrderRow.flightCustomer.email}</td>
          </tr>
          </tbody>
        </table>
     </div>    
     <div class="col-md-4">
     <h4 class="text-left"><span><s:text name="tgi.label.tax_invoice" /></span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b><s:text name="tgi.label.invoice_no" /></b></td>
            <td> <s:property  value="invObj.invNo"/></td>
          </tr>
          <tr>
            <td><b><s:text name="tgi.label.consultant" /></b></td>
            <td><s:property  value="invObj.consultant"/></td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.book_no" /></b></td>
            <td><s:property  value="invObj.bookNo"/></td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.date" /></b></td>
            <td><s:property  value="invObj.bookedDate"/></td>
          </tr>
          </tbody>
        </table>
     </div>
						</div>
					</div>
					</div> 
	<!-- Main content -->
	<!-- Main content -->
	</div></div>
		</div>
		<!-- table-responsive -->
	</section>


<section class="col-md-12">
		<div class="sccuss-full-updated" id="success-alert"
			style="display: none">
			<div class="succfully-updated clearfix">

				<div class="col-sm-2">
					<i class="fa fa-check fa-3x"></i>
				</div>

				<div id="message" class="col-sm-10"></div>
				<button type="button" id="success" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>
			</div>

		</div>
		<!-- Small boxes (Stat box) -->
		<div class="profile-timeline-card">
						<div class="">
				<div id="invoice" class="clearfix mt-0">
				
						<%-- <div class="row">

						<div class="col-xs-4">
							<div class="pro-img">
						 
							<img src="<s:url action='getImageByPath?imageId=%{invObj.company.imagePath}'/>"
								class="avatar img-circle img-thumbnail img-responsive" alt="profile image">
						</div>
						</div>
						<div class="col-sm-8">
							<div class="lint-invoice-adde text-right">
								<h2>${invObj.company.companyName}</h2>
								<p>
									<br>${invObj.company.address} <br>
									<s:text name="tgi.label.tel" /> ${invObj.company.phone}<br> 
									<b><s:text name="tgi.label.email" /></b> ${invObj.company.email}<b>
									<br><s:text name="tgi.label.website" /></b> ${invObj.company.website}
								</p>
							</div>
						</div> 
					</div> --%>
						

						<div class="row">
							<%-- <div class="in-head">
								<h4 class="text-center">
									<span><s:text name="tgi.label.tax_invoice" /></span>
								</h4>
							</div>

							<div class="invoice-addres clearfix">
								<div class="col-md-5">
									<div class="panel panel-default ">

										<div class="panel-body">
											<h4>
												<s:text name="tgi.label.to" />
												:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <s:property value="invObj.attn" />${invObj.flightOrderRow.flightCustomer.firstName} ${invObj.flightOrderRow.flightCustomer.lastName}
											</h4>
											<p>
												<b> <s:text name="tgi.label.address" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</b>
												${invObj.flightOrderRow.flightCustomer.address}
											</p>
											<p>
												<b> <s:text name="tgi.label.tel" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</b>
												<s:property value="invObj.mobile" />${invObj.flightOrderRow.flightCustomer.mobile}
											</p>
											<!--  <p>Fax: 99876544</p> -->
											<p>
												<b> <s:text name="tgi.label.attn" /></b>
												<s:property value="invObj.attn" />
											</p>
										 <p>
												<b> <s:text name="tgi.label.email" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
												<s:property value="invObj.emailId" />${invObj.flightOrderRow.flightCustomer.email}
											</p>
					 
										</div>
									</div>
								</div>

								<div class="col-md-6">

									<table class="table inn-num table-bordered">

										<tbody>
											<tr>
												<td><s:text name="tgi.label.invoice_no" /></td>
												<td><s:property value="invObj.invNo" />
												</td>


											</tr>
											<tr>
												<td><s:text name="tgi.label.acct_code" /></td>
												<td><a href="#"> <s:property value="invObj.ActCode" /></a></td>
											</tr>
										 <tr>
												<td><s:text name="tgi.label.consultant" /></td>
												<td><s:property
															value="invObj.consultant" /></td>
											</tr> 
											<tr>
												<td><s:text name="tgi.label.book_no" /></td>
												<td><s:property value="invObj.bookNo" /></td>
											</tr>

											<tr>
												<td><s:text name="tgi.label.your_ref" /></td>
												<td><s:property value="invObj.yourRef" /></td>
											</tr>
											<tr>
													<td><s:text name="tgi.label.page" /></td>
													<td><a href="#">Page 1 of 1</a></td>
												</tr>
											<tr>
												<td><s:text name="tgi.label.date" /></td>
												<td><s:property
															value="invObj.bookedDate" /></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div> --%>
							<!-- / end client details section -->

							<div class="">
								<table class="table table-bordered in-table-border no-table-css">
									<thead>
										<tr class="info">
											<th><h4><s:text name="tgi.label.gst_type" /></h4></th>
											<th><h4>
													<s:text name="tgi.label.pertycular" />
												</h4></th>
											<th><h4>
													<s:text name="tgi.label.qty" />
												</h4></th>
											<th><h4>
													<s:text name="tgi.label.price" />
												</h4></th>
											<th><h4>
											<s:text name="tgi.label.total" />(<s:property value="invObj.currency" />)
												</h4></th>

										</tr>
									</thead>
									<tbody>
										<s:iterator value="invObj.tripParticulars">
											<tr>
											</tr>
										</s:iterator>
										<s:iterator value="invObj.flightOrderRow.flightOrderCustomers" status="rowCount">

											<tr>
													<td>ZRE</td>
												<td>(<s:property  value="%{#rowCount.count}"/>)  <s:property value="firstName" /></td>
												<td >1</td>
             									<td >0</td>
             									<td >0</td>
											</tr>
										</s:iterator>
										<tr>
											<td>SR</td>
											<td>
											<s:iterator value="invObj.tripParticulars">
												
													<s:property value="operatedByCode" />/ <s:property value="flightNumber" />/
													<s:property value="originCode" /> - <s:property value="destinationCode" /> 
													<s:property value="convertDate" /> <s:property value="departureTime" />
													<s:property value="arrivalTime" />
													<br>					
											</s:iterator></td>
											 <td >1</td>
          	   									<td ><s:property  value="invObj.basePrice"/></td>
             								    <td ><s:property  value="invObj.basePrice"/></td>
										</tr>
										<tr>
            <td></td>
            <td>Tax<span> </span>  </td>
            <td> </td>
             <td> </td>
              <td><s:property  value="invObj.tax"/></td>
          </tr>	
           <tr>
            <td><s:text name="tgi.label.sr" /></td>
            <td>  <s:text name="tgi.label.processing_fees" /> <span> </span>  </td>
            <td> </td>
             <td> </td>
              <td><s:property  value="invObj.processingFees"/></td>
              
           </tr>
           <tr>
            <td><s:text name="tgi.label.sr" /></td>
            <td>  <s:text name="tgi.label.insurance_fee" /> <span> </span>  </td>
            <td> </td>
             <td> </td>
               <td>
              	<c:choose>
				<c:when test="${invObj.insurancePrice != null}">
				${invObj.insurancePrice}
				</c:when>
				<c:otherwise>
				<s:text name="tgi.label.n_a" />
				</c:otherwise>
				</c:choose>
              </td>
              
           </tr>
            <tr>
            <th>
           
            </th>
            <th>
           
            </th>
            <th>
          <s:text name="tgi.label.calculation" /> 
            </th>
            <th>
          
            </th>
            <th >
        
            </th>
             </tr>
             
             <tr>
            <td>
          <!-- 6% -->
            </td>
            <td>
          <!-- SR -->
            </td>
            <td>
        <%-- <s:property  value="invoiceData.totGst"/> --%>
            </td>
            <td>
        	<s:text name="tgi.label.booking_amount" /> 
            </td>
            <td colspan="4">
            
            ${invObj.price}<br>
         	 <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         	 <s:text name="tgi.label.totamount" />   <s:property  value="invoiceData.totWithGst"/> --%> 
            </td>
            
            </tr>
            
              <tr>
            <td>
          <!-- 6% -->
            </td>
            <td>
          <!-- SR -->
            </td>
            <td>
        <%-- <s:property  value="invoiceData.totGst"/> --%>
            </td>
            <td>
         <s:text name="tgi.label.discount_amount" /> 
            </td>
            <td >
            
            ${invObj.discountAmount}<br>
         	 <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         	   <s:text name="tgi.label.totamount" />  <s:property  value="invoiceData.totWithGst"/> --%> 
            </td>
            
            </tr>
            
              <tr>
            <td>
          <!-- 6% -->
            </td>
            <td>
          <!-- SR -->
            </td>
            <td>
        <%-- <s:property  value="invoiceData.totGst"/> --%>
            </td>
            <td>
        	<s:text name="tgi.label.total_paid" />
            </td>
            <td colspan="4">
	            ${invObj.totAmount}<br>
         	 <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         	 <s:text name="tgi.label.totamount" />  <s:property  value="invoiceData.totWithGst"/> --%> 
            </td>
            
            </tr>
										
									</tbody>
								</table>
							</div>

							<div class="clearfix">
								<div class="payment-in">
									<div class="panel panel-info">
									<c:choose>
									<c:when test="${invObj != null}">
											<h4 class="pl-2">
												<s:text name="tgi.label.flight_insurance" />
											</h4>
<!-- <div class="panel-body"> -->
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="info">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
									<tr>
											<td data-title="><s:text name="tgi.label.insurance_id" />">${invObj.insuranceId}</td>
											<td data-title="<s:text name="tgi.label.name" />">${invObj.insuranceTitle}</td>
											<td data-title="<s:text name="tgi.label.price" />">${invObj.insurancePrice}</td>
											<td data-title="<s:text name="tgi.label.description" />">${invObj.description}</td>
										</tr>
											</table>
											</c:when>
											<c:otherwise>
										<h4 class="pl-2">
												<s:text name="tgi.label.flight_insurance" />
											</h4>
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="info">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.insurance_not_available" /></td>
												</tr></table>
										</c:otherwise>
										</c:choose>
											<%-- <s:else>
											<h4>
												<s:text name="tgi.label.flight_insurance" />
											</h4>
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="info">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.wallet_details_not_available" /></td>
												</tr>
											</table>
										</s:else> --%>
										
										</div>
										
								</div>
							</div>
							<div class="clearfix">
								<div class="payment-in">
									<div class="panel panel-info">
										<!--   <div class="panel-heading">
    <h4>Payment details</h4>
  </div> -->

										<s:if test="invObj.txDetails.size()>0">
											<h4 class="pl-2">
												<s:text name="tgi.label.payment_details" />
											</h4>
											<!-- <div class="panel-body"> -->
											<table class="table table-bordered in-table-border no-table-css">
												<tr class="info">
														<th><s:text name="tgi.label.receipt" /></th>
														<th><s:text name="tgi.label.date" /></th>
														<th><s:text name="tgi.label.remark" /></th>
														<th><s:text name="tgi.label.payment_method" /></th>
														<th><s:text name="tgi.label.payable_amount" /></th>
														<th><s:text name="tgi.label.paid_amount" /></th>
													</tr>
												<s:iterator value="invObj.txDetails">
													<tr>
															<td>${transactionId}</td>
															<td>${convertDate}</td>
															<td>${responseMessage}</td>
															<td>${paymentMethod}</td>
															<td>${totalPaidAmount}</td>
															<td><fmt:formatNumber type="number" pattern="#.00" value="${totalPaidAmount}" /></td>
														</tr>
												</s:iterator>
											</table>
										</s:if>
										
										</div>
									<%--  <div class="panel panel-info">
										<s:if test="invObj.agentWalletTxDetails.size()>0">
											<h4>
												<s:text name="tgi.label.wallet_details" />
											</h4>
											<table class="table table-bordered in-table-border">
												<tr class="info">

													<th><s:text name="tgi.label.date" /></th>
													<th><s:text name="tgi.label.remark" /></th>
													<th><s:text name="tgi.label.amount" /></th>
													<th><s:text name="tgi.label.opening_balance" /></th>
													<th><s:text name="tgi.label.closing_balance" /></th>
												</tr>
												<s:iterator value="invObj.agentWalletTxDetails">
													<tr>

														<td data-title="<s:text name="tgi.label.date" />"><s:property value="convertDate" /></td>

														<td data-title="<s:text name="tgi.label.remark" />"><s:property value="action" /></td>
														<td data-title="<s:text name="tgi.label.amount" />"><s:property value="amount" /></td>
														<td data-title="<s:text name="tgi.label.opening_balance" />"><s:property value="openingBalance" /></td>
														<td data-title="<s:text name="tgi.label.closing_balance" />"><s:property value="closingBalance" /></td>
														<td data-title="<s:text name="tgi.label.currency" />"><s:property value="currency" /></td>
													</tr>
												</s:iterator>
											</table>
										</s:if>
										<s:else>
											<h4>
												<s:text name="tgi.label.wallet_details" />
											</h4>
											<table class="table table-bordered in-table-border no-table-css">
												<tr class="info">

													<th><s:text name="tgi.label.date" /></th>
													<th><s:text name="tgi.label.remark" /></th>
													<th><s:text name="tgi.label.amount" /></th>
													<th><s:text name="tgi.label.opening_balance" /></th>
													<th><s:text name="tgi.label.closing_balance" /></th>

												</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.wallet_details_not_available" /></td>
												</tr>
											</table>
										</s:else>
									</div>  --%>
								</div>
							</div>

							<div class="clearfix signature-lint">

							<div class="col-sm-6">
								<h4> <s:text name="tgi.label.recived_by" /> </h4>
								<br> <br>
								<p>
									<span style="border-top: 1px solid #adadad; padding: 5px;">Customers
										Signature &amp; Chop</span>
								</p>
							</div>

							<div class="col-sm-6 pull-right">
								<div class="pull-right">
									<h4 class="text-capitalize">touroxy</h4>
									<br> <br>
									<p>
										<span style="border-top: 1px solid #adadad; padding: 5px;">Autorised
											Signature</span>
									</p>
								</div>
							</div>
						</div>
						</div>
				</div>
				<div id="editor"></div>
			</div>
			<!-- /.row -->
			<!-- Main row -->


</div></section>
<!-- /.row -->
<!-- Main row -->



<!-- /.content -->

<script type="text/javascript">
var orderId = '${orderId}';

<%-- function generatePdf()
{
	$.ajax({
	 	url: "<%=request.getContextPath()%>/pdfDownload?orderId="+orderId,
		type: "POST",
		});
} --%>

</script>

<input type="hidden" id="orderid" value="${param.orderId}">
<script type="text/javascript">
	function sendCustomerInvoiceToCustomer() {
		var orderid = $("#orderid").val();
		var mail = $("#mail").val();
		console.log("mail..." + mail);
		var invoice = "<html><body style='border:2px solid;padding:10px 10px 10px 10px'>"
				+ $("#invoice").html() + "</body></html>";
		/*   var htmlMessage=$('#invoice').html(); */
		console.log("--htmlMessage..." + invoice);
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "sendInvoiceToMail";
		$('#h4').show();
		$.ajax({
			method : "POST",
			url : finalUrl,
			data : {
				customerMail : mail,
				orderid : orderid,
				htmlMessage : invoice
			},
			success : function(data, status) {
				$.each(data, function(index, element) {
					console.log("data-------" + element.status);

					if (element.status == "success") {
						$('#h4').hide();
						$('#success-alert').show();
						$('#message').text("Successfully sent mail.");
						$('#success').click(function() {
							$('#success-alert').hide();
							window.location.assign($(location).attr('href'));
						});

					} else if (element.status == "fail") {
						$('#h4').hide();
						$('#success-alert').show();
						$('#message').text("Failed.Try again.");
						$('#success').click(function() {
							$('#success-alert').hide();

						});
					}

				});

			},
			error : function(xhr, status, error) {
				$('#h4').hide();
				$('#success-alert').show();
				$('#message').text(error);
				$('#success').click(function() {
					$('#success-alert').hide();
				});
				console.log("Error----------" + error);
			}
		});

	}
</script>
