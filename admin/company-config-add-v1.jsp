<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addNewCompanyConfig";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 
		<!--************************************
        MAIN ADMIN AREA
    ****************************************-->

		<!--ADMIN AREA BEGINS-->


		<section class="wrapper container-fluid">

			<div class="">
				<div class="">
					<div class="pnl">
						<div class="hd clearfix">
							<h5 class="pull-left"><s:text name="tgi.label.new_company_configuration" /></h5>
							<div class="set pull-left">
								<!--    <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
							</div>
						 <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="companyConfigList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.company_conf_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
						</div>
						<div class="hd clearfix">
							<h5 class="pull-left"></h5>
						</div>
						<div class="cnt cnt-table">
							<div class="table-responsive">
								<form action="AddCompanyConfigData" id="add-company-configData" method="post" class="form-horizontal">
									<input type="hidden" value="${sessionScope.Company.companyUserId}" class="form-control input-sm" id="companyUserId">
									<input type="hidden" value="<s:property value=" %{#session.Company.companyRole.isDistributor()} "/>" class="form-control input-sm" id="companyRoleType">
									<table class="table table-form">
										<thead>
											<tr>
												<th></th>
												<th>&nbsp;</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div class="field-name"><s:text name="tgi.label.company_UserId"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="configCompanyName" id="company_id" autocomplete="off" >
														<option selected value="0"><s:text name="tgi.label.select_company_userId" /></option>
														<s:if test="%{#session.Company.companyRole.isDistributor()}">
															<s:iterator value="companyUserIdsList">
																<option value="<s:property value=" companyId "/>,<s:property value="companyName "/>,<s:property value="companyUserId "/>"><s:property value="companyUserId" />(<s:property value="companyName" />)</option>
															</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="companyUserIdsList">
																<option value="<s:property value=" companyId "/>,<s:property value="companyName "/>,<s:property value="companyUserId "/>"><s:property value="companyUserId" />(<s:property value="companyName" />)</option>
															</s:iterator>
														</s:else>


													</select></div></div>
												</td>
												<td>
													<div class="field-name"><s:text name="tgi.label.config_name" /></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<input type="text" class="form-control input-sm" id="username" name="configName" placeholder="config name" autocomplete="off" ></div></div>
												</td>


											</tr>
											<tr>
											<td><s:text name="tgi.label.config_type" /></td>
											<td>
											<select class="form-control input-sm" name="configType" id="configType" required="">
        <option selected="" value=""><s:text name="tgi.label.select_config_type" /></option>
         <option value="B2C">B2C</option>
         <option value="B2B">B2B</option>
         <option value="API">API</option>
         <option value="WL"><s:text name="tgi.label.whitelable" /></option>
       </select>
											</td>
											<td><s:text name="tgi.label.parent_configId"/></td>
												<td><div class="form-group" id="parentconfigid_div" style="display:block;">
												
														<div class="col-sm-7"><div class="form-group"><div class="col-md-12">
															<select class="form-control input-sm" name="parentConfigIdSplit" id="parentConfigIdSplit" autocomplete="off" >
																<option selected value="0"><s:text name="tgi.label.select_parent_configid"/></option>
																<s:iterator value="%{configList}">
																	<option value="<s:property value="config_id "/>,<s:property value="companyUserId "/>">
																		<s:property value="config_id" /> - <s:property value="configName" /> (
																		<s:property value="companyUserId" />)
																	</option>
																</s:iterator>
															</select></div></div>
														</div>
													</div>
												</td>
											</tr>
											</tbody>
											<tbody>
											<tr>
											<%-- <td colspan="4">
											<table style="border:1px solid reg;">
											<tr>
											<td colspan="4" style="text-align:left;"><h2><s:text name="tgi.label.flight" /></h2></td>
											</tr>
											<tr>
												<td>
													<div class="field-name"><s:text name="tgi.label.rate_type" /></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="rateTypeFlight" id="rateTypeFlight" autocomplete="off" >
														<option value="NetFlight"><s:text name="tgi.label.net"/></option>
														<option value="CommissionFlight"><s:text name="tgi.label.commission"/></option>
													</select></div></div>
												</td>
												
												</tr>
												<tr class="commission-group-flight" style="display:none">
												<td>
													<div class="field-name"><s:text name="tgi.label.commision_type"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="commissionTypeFlight" id="commissionTypeFlight" autocomplete="off">
														<option value="Percentage"><s:text name="tgi.label.percentage"/></option>
														<option value="Fixed"><s:text name="tgi.label.fixed"/></option>
													</select></div></div>
												</td>
												<td>
													<div class="field-name"><s:text name="tgi.label.commision"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<input type="text" class="form-control input-sm" id="commission" name="commissionAmountFlight" placeholder="amount" autocomplete="off">
												</div></div></td>
											</tr>
												</table>
												</td></tr></tbody>
												<tbody>
												<tr>
											<td colspan="4">
											<table>
											<tr>
											<td colspan="4" style="text-align:left;"><h2><s:text name="tgi.label.hotel" /></h2></td>
											</tr>
												<tr>
												<td>
													<div class="field-name"><s:text name="tgi.label.rate_type" /></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="rateTypeHotel" id="rateTypeHotel" autocomplete="off" >
														<option value="NetHotel"><s:text name="tgi.label.net"/></option>
														<option value="CommissionHotel"><s:text name="tgi.label.commission"/></option>
													</select></div></div>
												</td>
												
												
											</tr>
												<tr class="commission-group-hotel" style="display:none">
												<td>
													<div class="field-name"><s:text name="tgi.label.commision_type"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="commissionTypeHotel" id="commissionTypeHotel" autocomplete="off">
														<option value="Percentage"><s:text name="tgi.label.percentage"/></option>
														<option value="Fixed"><s:text name="tgi.label.fixed"/></option>
													</select></div></div>
												</td>
												<td>
													<div class="field-name"><s:text name="tgi.label.commision"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<input type="text" class="form-control input-sm" id="commission" name="commissionAmountHotel" placeholder="amount" autocomplete="off">
												</div></div></td>
											</tr>
												</table>
												</td>
												</tr>
												</tbody>
												<tbody>
												<tr>
											<td colspan="4">
											<table>
											<tr>
											<td colspan="4" style="text-align:left;"><h2><s:text name="tgi.label.car" /></h2></td>
											</tr>
												<tr>
												<td>
													<div class="field-name"><s:text name="tgi.label.rate_type" /></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="rateTypeCar" id="rateTypeCar" autocomplete="off" >
														<option value="NetCar"><s:text name="tgi.label.net"/></option>
														<option value="CommissionCar"><s:text name="tgi.label.commission"/></option>
													</select></div></div>
												</td>
											</tr>
												<tr class="commission-group-car" style="display:none">
												<td>
													<div class="field-name"><s:text name="tgi.label.commision_type"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="commissionTypeCar" id="commissionTypeCar" autocomplete="off">
														<option value="Percentage"><s:text name="tgi.label.percentage"/></option>
														<option value="Fixed"><s:text name="tgi.label.fixed"/></option>
													</select></div></div>
												</td>
												<td>
													<div class="field-name"><s:text name="tgi.label.commision"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<input type="text" class="form-control input-sm" id="commission" name="commissionAmountCar" placeholder="amount" autocomplete="off">
												</div></div></td>
											</tr>
												</table>
												</td>
												</tr>
												</tbody>
												
												<tbody>
												<tr>
											<td colspan="4">
											<table>
											<tr>
											<td colspan="4" style="text-align:left;"><h2><s:text name="tgi.label.tour" /></h2></td>
											</tr>
												<tr>
												<td>
													<div class="field-name"><s:text name="tgi.label.rate_type" /></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="rateTypeTour" id="rateTypeTour" autocomplete="off" >
														<option value="NetTour"><s:text name="tgi.label.net"/></option>
														<option value="CommissionTour"><s:text name="tgi.label.commission"/></option>
													</select></div></div>
												</td>
											</tr>
												<tr class="commission-group-tour" style="display:none">
												<td>
													<div class="field-name"><s:text name="tgi.label.commision_type"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="commissionTypeTour" id="commissionTypeTour" autocomplete="off">
														<option value="Percentage"><s:text name="tgi.label.percentage"/></option>
														<option value="Fixed"><s:text name="tgi.label.fixed"/></option>
													</select></div></div>
												</td>
												<td>
													<div class="field-name"><s:text name="tgi.label.commision"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<input type="text" class="form-control input-sm" id="commission" name="commissionAmountTour" placeholder="amount" autocomplete="off">
												</div></div></td>
											</tr>
												</table>
												</td>
												</tr>
												</tbody>
											<tbody> --%>
											<tr>
												<td>
													<div class="field-name"><s:text name="tgi.label.status"/></div>
												</td>
												<td><div class="form-group"><div class="col-md-12">
													<select class="form-control input-sm" name="configStatus" id="configStatus" autocomplete="off" >
														<option value="yes"><s:text name="tgi.label.active"/></option>
														<option value="no"><s:text name="tgi.label.in_active"/></option>

													</select></div></div>
												</td>
												</tr>
												<tr>
												<%-- <td>
													<div class="form-group" id="parentconfigid_div" style="display: none">
												
														<label for="City" class="col-sm-2 control-label"><s:text name="tgi.label.parent_configId"/> </label>
													
														<div class="col-sm-7"><div class="form-group"><div class="col-md-12">
															<select class="form-control input-sm" name="parentConfigIdSplit" id="parentConfigIdSplit" autocomplete="off" >
																<option selected value="0"><s:text name="tgi.label.select_parent_configid"/></option>
																<s:iterator value="%{#session.parentConfigIds}">
																	
																	<option value="<s:property value="config_id "/>,<s:property value="companyUserId "/>,<s:property value="rateTypeFlight"/>,<s:property value="commissionTypeFlight"/>,<s:property value="commissionAmountFlight"/>">
																		<s:property value="config_id" /> (
																		<s:property value="companyUserId" />-
																		<s:property value="rateTypeFlight" />-
																		<s:property value="commissionTypeFlight" />-
																		<s:property value="commissionAmountFlight" />)
																	</option>
																	
																	<option value="<s:property value="config_id "/>,<s:property value="companyUserId "/>,<s:property value="rateTypeHotel"/>,<s:property value="commissionTypeHotel"/>,<s:property value="commissionAmountHotel"/>">
																		<s:property value="config_id" /> (
																		<s:property value="companyUserId" />-
																		<s:property value="rateTypeHotel" />-
																		<s:property value="commissionTypeHotel" />-
																		<s:property value="commissionAmountHotel" />)
																	</option>
																	
																	<option value="<s:property value="config_id "/>,<s:property value="companyUserId "/>,<s:property value="rateTypeCar"/>,<s:property value="commissionTypeCar"/>,<s:property value="commissionAmountCar"/>">
																		<s:property value="config_id" /> (
																		<s:property value="companyUserId" />-
																		<s:property value="rateTypeCar" />-
																		<s:property value="commissionTypeCar" />-
																		<s:property value="commissionAmountCar" />)
																	</option>
																	
																	<option value="<s:property value="config_id "/>,<s:property value="companyUserId "/>,<s:property value="rateTypeTour"/>,<s:property value="commissionTypeTour"/>,<s:property value="commissionAmountTour"/>">
																		<s:property value="config_id" /> (
																		<s:property value="companyUserId" />-
																		<s:property value="rateTypeTour" />-
																		<s:property value="commissionTypeTour" />-
																		<s:property value="commissionAmountTour" />)
																	</option>
																	
																</s:iterator>
															</select></div></div>
														</div>
													</div>
												</td> --%>
											</tr>
										</tbody>
									</table>
									<div class="form-group table-btn">
										<div class="col-sm-10 col-sm-offset-2 ">
										<%-- 	<div class="pull-left">
												<button class="btn btn-danger" type="submit"><s:text name="tgi.label.Clear_form"/></button>
											</div> --%>
											<div class="pull-right">

												<button class="btn btn-default" type="reset"><s:text name="tgi.label.cancel"/></button>
												<button class="btn btn-primary " type="submit"><s:text name="tgi.label.add"/></button>
											</div>
										</div>
									</div>
								</form>
								<br>
								<br>
								<br>
							</div>
						</div>
					</div>
				</div>
				</div>
	<!-- 		</div> -->

		</section>
		<!--ADMIN AREA ENDS-->
		<%-- <script type="text/javascript">
			$(function() {

				$('#rateType').change(function() {
					if ($('#rateType').val() == 'Commission') {
						$('.commission-group').show();
						if ($('#company_id').val() == "") {
							$('#commission_alert-div').hide();
						} else {
							$('#commission_alert-div').show();
						}

					} else {
						$('.commission-group').hide();
						$('#commission_alert-div').hide();

					}

				});

			});
		</script> --%>
		
		<script type="text/javascript">
			$(function() {

				$('#rateTypeFlight').change(function() {
					if ($('#rateTypeFlight').val() == 'CommissionFlight') {
						$('.commission-group-flight').show();
						if ($('#company_id').val() == "") {
							$('#commission_alert-div').hide();
						} else {
							$('#commission_alert-div').show();
						}

					} else {
						$('.commission-group-flight').hide();
						$('#commission_alert-div').hide();

					}

				});

			});
		</script>
		<script type="text/javascript">
			$(function() {

				$('#rateTypeHotel').change(function() {
					if ($('#rateTypeHotel').val() == 'CommissionHotel') {
						$('.commission-group-hotel').show();
						if ($('#company_id').val() == "") {
							$('#commission_alert-div').hide();
						} else {
							$('#commission_alert-div').show();
						}

					} else {
						$('.commission-group-hotel').hide();
						$('#commission_alert-div').hide();

					}

				});

			});
		</script>
		<script type="text/javascript">
			$(function() {

				$('#rateTypeCar').change(function() {
					if ($('#rateTypeCar').val() == 'CommissionCar') {
						$('.commission-group-car').show();
						if ($('#company_id').val() == "") {
							$('#commission_alert-div').hide();
						} else {
							$('#commission_alert-div').show();
						}

					} else {
						$('.commission-group-car').hide();
						$('#commission_alert-div').hide();

					}

				});

			});
		</script>
		
		<script type="text/javascript">
			$(function() {

				$('#rateTypeTour').change(function() {
					if ($('#rateTypeTour').val() == 'CommissionTour') {
						$('.commission-group-tour').show();
						if ($('#company_id').val() == "") {
							$('#commission_alert-div').hide();
						} else {
							$('#commission_alert-div').show();
						}

					} else {
						$('.commission-group-tour').hide();
						$('#commission_alert-div').hide();

					}

				});

			});
		</script>
		
		<script type="text/javascript">
			$(function() {
				var companyUserId1 = null;
				var rateType1 = null;
				var commissionType1 = null;
				var commissionAmount1 = "0.00";
				var parentCompanyUserId = null;
				var net = null;
				var comm = null;
				var percentage = null;
				var fixed = null;
				var parentConfigValidOption = null;
				$('#company_id').change(function() {
					var parentConfigValid = document.getElementById("parentConfigIdSplit");
					parentConfigValidOption = parentConfigValid.options[0].value;
					/* var type = document.getElementById("rateType");
					net = type.options[0].value;
					comm = type.options[1].value;
					var cType = document.getElementById("commissionType");
					percentage = cType.options[0].value;
					fixed = cType.options[1].value; */
					var array = $('#company_id').val().split(",");
						if(array[0] == 1)
						{
						$("#parentConfigIdSplit").hide();
						}
						else
							{
							$("#parentConfigIdSplit").show();
							}
					parentCompanyUserId = array[2];
					// alert(parentCompanyUserId);
					if ($('#configName').val() != "") {
						$('#configName').val("");
					}
					if ($('#rateType').val() == 'Commission') {
						$('.commission-group').hide();
						$('#rateType').val("Net");
						$('#commission_alert-div').hide();
					}
					if ($('#company_id').val() == "") {
						$('#parentconfigid_div').hide();
						$('#commission_alert-div').hide();
						$('#Whitelable_div').hide();
					} else {
						$('#parentconfigid_div').show();
						$('#commission_alert-div').show();
					}
					//alert(array[2]+"---"+$('#companyUserId').val());
					if (parentCompanyUserId != $('#companyUserId').val()) {
						//alert($('#parentConfigIdSplit').val());
						if ($('#company_id').val() == "") {
							$('#parentconfigid_div').hide();
							$('#commission_alert-div').hide();
						} else {
							$('#parentconfigid_div').show();
							$('#commission_alert-div').show();
							$('#Whitelable_div').show();
						}

						if ($('#parentConfigIdSplit').val() != "0") {
							// $('#parentConfigIdSplit').val(""); 

							$('#parentConfigIdSplit').val("0");
						} else {
							$('#commission_alert-div').hide();
						}
						if ($('#parentConfigIdSplit').val() == "0") {
							parentConfigValid.options[0].value = "";

						} else {
							$('#commission_alert-div').hide();
						}
						$('#parentConfigIdSplit').change(function() {
							var parentArray = $('#parentConfigIdSplit').val().split(",");
							companyUserId1 = parentArray[1];
							rateType1 = parentArray[2];
							commissionType1 = parentArray[3];
							commissionAmount1 = parentArray[4];
							$('.modelType').text(rateType1);
							$('.commType').text(commissionType1);
							$('.commAmnt').text(commissionAmount1);

							if ($('#company_id').val() == "") {
								$('#commission_alert-div').hide();
							} else {
								$('#commission_alert-div').show();
							}
							if ($('#configName').val() != "") {
								$('#configName').val("");
							}



							if (net == rateType1) {
								document.getElementById("rateType").value = net;
								document.getElementById("commissionType").value = "";
								document.getElementById("commission").value = "0.00";

								if ($('#companyRoleType').val() == "true") {
									$('#rateType option[value*="' + comm + '"]').prop('disabled', true);
								}
								$('#commission_amount_div').hide();
								$('.commission-group').hide();
								$('#commission_alert-div').hide();
								//document.getElementById("commissionType").value=commissionType1;
							} else if (comm == rateType1) {
								document.getElementById("rateType").value = comm;
								$('.commission-group').show();
								document.getElementById("commissionType").value = commissionType1;
								if ($('#companyRoleType').val() == "true") {
									$('#rateType option[value*="' + comm + '"]').prop('disabled', false);
								}
								if (commissionType1 == percentage) {
									$('#commissionType option[value*="' + fixed + '"]').prop('disabled', true);
								} else if (commissionType1 == fixed) {
									$('#commissionType option[value*="' + percentage + '"]').prop('disabled', true);
								}

							} else {
								$('.commission-group').hide();
								document.getElementById("commissionType").value = rateType1;
							}
						});
					} else {
						$('#parentconfigid_div').hide();
						$('#commission_alert-div').hide();
						$('.commission-group').hide();
						document.getElementById("commission").value = "0.00";
						document.getElementById("configName").value = "";
						document.getElementById("commissionType").value = "";
						document.getElementById("rateType").value = "Net";
						if ($('#parentConfigIdSplit').val() == "") {
							parentConfigValid.options[0].value = "0";

						}

					}
				});
				$('#rateType').change(function() {
					if ($('#rateType').val() == 'Commission') {
						if (parentCompanyUserId != $('#companyUserId').val()) {
							if ($('#company_id').val() == "") {
								$('#commission_alert-div').hide();
							} else {
								$('#commission_alert-div').show();
							}
						} else {
							$('#commission_alert-div').hide();
							$('#commissionType option[value*="' + fixed + '"]').prop('disabled', false);
							$('#commissionType option[value*="' + percentage + '"]').prop('disabled', false);
							document.getElementById("commissionType").value = "";
							document.getElementById("commissionType").value = "Percentage";
						}
					}
				});
				$("#commission").keyup(function() {
					//alert(Math.round(parseFloat(commissionAmount1)*100)/100);
					if (parentCompanyUserId == $('#companyUserId').val()) {
						$('#commission_amount_div').hide();
					} else if (Math.round(parseFloat(commissionAmount1) * 100) / 100 >= Math.round(parseFloat($('#commission').val()) * 100) / 100) {

						$('#commission_amount_div').hide();
					} else if ($('#commission').val() == "") {
						$('#commission_amount_div').hide();
					} else {

						if (companyUserId1 == $('#companyUserId').val()) {
							if (comm == rateType1) {
								if (Math.round(parseFloat(commissionAmount1) * 100) / 100 == 0) {
									$('#commission_alert-div').hide();
								} else {
									$('#commission_amount_div').show();
								}
							}
						} else {
							$('#commission_amount_div').show();
						}

					}
					var val = $(this).val();
					if (isNaN(val)) {
						val = val.replace(/[^0-9\.]/g, '');
						if (val.split('.').length > 0)
							val = val.replace(/\.+$/, "");
					}
					$(this).val(val);
					if ($('#commission').val().length == 0) {
						$('#commission_amount_div').hide();
					}
				});
			});
		</script>
					
						<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
<script type="text/javascript">
$(document).ready(function() {
$('#add-company-configData')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				configCompanyName : {
					message : 'Config CompanyName is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Config CompanyName'
						},
						
					}
				},
				configName : {
					message : 'Config Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Config Name '
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'Config Name must be more than 3 and less than 20 characters long'
						} 
					}
				},
				rateType : {
					message : 'rateType is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Rate Type'
						},
						
					}
				},
				configStatus : {
					message : 'ConfigStatus is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a ConfigStatus'
						},
						
					}
				},
				commissionType : {
					message : 'Commission Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Commission Type'
						},
						
					}
				},
				commissionAmount : {
					message : 'Amount Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Amount'
						},
						 numeric: {
	                         message: 'The value is not a number',
	                            // The default separators
	                            thousandsSeparator: '',
	                            decimalSeparator: '.'
	                        }
					}
				},
	
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {
	/* notifySuccess(); */
	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>


<!-- $("#company_id > option").each(function () {    
					    if (this.value.indexOf(parentCompanyUserId) >0) {
					        $("#company_id option[value='" + this.value +"']").remove();
					    }
					}); -->