<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src="admin/js/jquery-ui.js" ></script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addHotelMarkup";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 
<script type="text/javascript">
 	 $(function() {
		 loadstations();
	 });
 	function loadstations() {
		  $.getJSON("hotels.json",{
				format : "json"
			}).done(
					 function(jsonbusdata) {
					 
						  citylist = [];
						  names = [];
						 
						 $.each(jsonbusdata, function(i, station) {
							citylist.push(station.chain);
							 
						 });
						 $("#hotelChain").autocomplete({
					        source: citylist,
					         
					    });
						 
						 $('#hotelChain').on('autocompleteselect', function (e, ui) {
							    	 
			 }); 
					    $.ui.autocomplete.filter = function (array, term) {
					        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
					        return $.grep(array, function (value) {
					            return matcher.test(value.label || value.value || value);
					        });
					    };
					 });
 	}
 	
 	
 	
 	
 	
 	</script>



   <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->

            <section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                    <div class="card1">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.add_hotal_markup_configuration" /></h5>
                              <!--   <div class="set pull-left">
                                    <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button>
                                </div> -->
                               <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success" href="hotelMarkupList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.hotel_markUp_list"/> </a>
							</div>
						</div>
                               </div> 
                            </div> 
                    </div>
                </div>
                
                <div class="container">
		<div class="row">
		<h4 class="text-center p-4 hidden-xs"><s:text name="tgi.label.add_hotal_markup_configuration" /></h4> 
                   <div class="spy-form-horizontal mb-0">
					<div class="">
						<div class="">
                                <form action="markup" id="hotel-markup" method="post" class="form-horizontal filter-form">
                                   
                                   <div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.config_number" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control " name="configData"
								id="configData" autocomplete="off" >
								<option selected value="0"><s:text name="tgi.label.select_company_confignumber" /></option>
								<s:if test="%{#session.Company.companyRole.isDistributor()}">
								 <s:iterator value="AgencyConfiglist">
									<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>/<s:property value="companyName"/>"><s:property value="config_number"/> - <s:property value="configname"/>  (<s:property value="companyName"/>-<s:property value="companyUserId"/>)</option>
								  </s:iterator>
								  <s:iterator value="companyConfigIdsList">
								<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>/<s:property value="companyName"/>"><s:property value="config_number"/>  - <s:property value="configname"/> (<s:property value="companyName"/>-<s:property value="companyUserId"/>)</option>
								  </s:iterator>
 						 		</s:if>
 						 		<s:elseif test="%{#session.Company.companyRole.isAgent()}">
								  <s:iterator value="AgencyConfiglist">
									<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>/<s:property value="companyName"/>"><s:property value="config_number"/>  - <s:property value="configname"/> (<s:property value="companyName"/>-<s:property value="companyUserId"/>)</option>
								  </s:iterator>
 						 		</s:elseif>
 						 		
								<s:else>
								<s:iterator value="companyConfigIdsList">
							<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>/<s:property value="companyName"/>"><s:property value="config_number"/>  - <s:property value="configname"/> (<s:property value="companyName"/>-<s:property value="companyUserId"/>)</option>
								 </s:iterator>
 							</s:else>
						 

							</select></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_name" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control " id="name"
									name="name" placeholder="MarkUp Name" autocomplete="off"
									></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.hotel_chain_group" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control " id="hotelChain"
									name="hotelChain" placeholder="Hotel Group"  
									  value="All"   ></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.hotel_name" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control " id="hotelName"
									name="hotelName" placeholder="hotel Name" autocomplete="off"
									  value="All" ></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.city" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control " id="hotelCity"
									name="hotelCity" placeholder="City" autocomplete="off"
									 value="All"></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.country" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control " name="hotelCountry" id="hotelCountry"
													autocomplete="off" >
													<option selected value="ALL"><s:text name="tgi.label.all" /></option>
													<s:iterator value="CountryList">
														<option value="<s:property value="c_name"/>"><s:property
																value="c_name"></s:property></option>
													</s:iterator>

							</select></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-4">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.is_accumulative" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
														<select class="form-control " name="isaccumulative"
											  autocomplete="off" >
		 									<option value="1"><s:text name="tgi.label.yes" /></option>
											<option value="0" selected="selected"><s:text name="tgi.label.no" /></option>
											 
										</select></div></div>
												</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.lable.is_fixedamount" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control " name="isfixedAmount"
													  autocomplete="off" >
				 									<option value="1"><s:text name="tgi.label.yes" /></option>
													<option value="0"><s:text name="tgi.label.no" /></option>
												 </select></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_amount" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control " id="markupAmount"
									name="markupAmount" placeholder="Enter Amount" autocomplete="off" ></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.checkin_date" />
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" id="checkInTr"
								class="form-control  date1" name="checkInTr" autocomplete="off"
								placeholder="ALL"   ></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.checkout_date" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" id="datepicker_dep"
								class="form-control  date2" name="checkOutTr" autocomplete="off"
								placeholder="ALL"  ></div></div>
											</div>
										</div>
									</div>
										
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_startdate" />
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" id="datepicker_PromofareStart"
													class="form-control  date3" name="promofareStartDateTr"  value='<s:property value="promofareStartDate" />'  autocomplete="off"
													placeholder="ALL"   ></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_enddate" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
													<input type="text" id="datepicker_PromofareEnd"
								class="form-control  date4" name="promofareEndDateTr"  value='<s:property value="promofareEndDate" />'  autocomplete="off"
								placeholder="ALL" >
											</div></div>
										</div>
									</div>
										
									
					</div>
									<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.position_of_markup" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
															<input type="number" class="form-control " name="positionMarkup"
												id="country" autocomplete="off" ></div></div>
										</div>
									</div> 
									</div>	
                                   
                                    <div class=" row">
									<div class="col-md-12">
                                    <div class="set pull-right">
										<div class="form-actions">
											<button class="btn btn-default btn-danger" id="tourCancelBtn" type="reset"><s:text name="tgi.label.reset" /></button>
											<button type="submit" class="btn btn-primary" id=""><s:text name="tgi.label.set_markup" /></button>
										</div></div>
										</div></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                </div></div>
            </section>
      <!--bootstrap vadidator  -->
      
       <link href="admin/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	   	 <script src="admin/js/jquery-ui.js" ></script>
         <script>
			$(document).ready(function() {
				var date1="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				var date2="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
				 
				 var date3="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					var date4="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					 spysr_date_custom_plugin('filter-form','date3','date4','mm/dd/yyyy','MMM DD YYYY',date3 ,date4);
			});
		</script>
      
      
      
                 <script type="text/javascript">
$(document).ready(function() {
$('#hotel-markup')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				
				configData : {
					message : 'ConfigData Role is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a ConfigData  '
						},
					}
				},
				name : {
					message : 'MarkUp Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a MarkUp Name'
						},
						stringLength : {
							min : 3,
							max : 200,
							
							message : 'MarkUp Name  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				hotelChain : {
					message : 'HotelChain is not valid',
					validators : {
						 notEmpty : {
							message : 'Select enter a HotelChain'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'HotelChain  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				hotelName : {
					message : 'HotelName is not valid',
					validators : {
						 notEmpty : {
							message : 'Select enter a HotelName'
						},
						stringLength : {
							min : 3,
							max : 500,
							
							message : 'HotelName  must be more than 3 and less than 500 characters long'
						} 
						
					}
				},
				hotelCity : {
					message : 'City is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a City'
						},
						stringLength : {
							min : 3,
							max : 500,
							
							message : 'City  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				
				hotelCountry : {
					message : 'Country Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a Country'
						},
					}
				},
				isaccumulative : {
					message : 'isaccumulative  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a isaccumulative'
						},
					}
				},
				isfixedAmount : {
					message : 'IsfixedAmount  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a IsfixedAmount'
						},
					}
				},
				markupAmount : {
					message : 'Amount Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Amount'
						},
						  numeric: {
	                            message: 'The value is not a number',
	                            // The default separators
	                            thousandsSeparator: '',
	                            decimalSeparator: '.'
	                        }
						
					}
				},
				positionMarkup : {
					message : 'positionMarkup Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a position markup'
						},
					}
				},
				/* eventDate: {
                    validators: {
                        notEmpty: {
                            message: 'The date is required'
                        },
                        date: {
                            format: 'MM/DD/YYYY',
                            message: 'The date is not a valid'
                        }
                    }
                } */
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {
	/* notifySuccess(); */
	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>
        <!--ADMIN AREA ENDS-->
      

      		
						<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>