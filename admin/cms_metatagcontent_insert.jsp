<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"metatagpage";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                    <div class="card1">
                        <div class="pnl">
                         <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.add" /> <s:text name="tgi.label.meta_data" /></h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="metatagpage_list"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.meta_dat_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div> 
				</div>
</div>


					<div class="container">
					<div class="row">
					<h4 class="text-center p-4 hidden-xs"><s:text name="tgi.label.add" /> <s:text name="tgi.label.meta_data" /></h4> 
                   <div class="spy-form-horizontal mb-0"> 
					<div class="">
						<div class="">
						${message}
						<form action="metatagpage_insert.action">
							
							
							
							
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.page_name" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control input-sm" name="pageName"
											id="configStatus" autocomplete="off" required>
												<option value="flight_search"><s:text
														name="tgi.label.flight_search" /></option>
												<option value="hotel_search"><s:text
														name="tgi.label.hotel_search" /></option>
												<option value="car_search"><s:text
														name="tgi.label.car_search" /></option>
												<option value="cruise_search"><s:text
														name="tgi.label.cruise_search" /></option>
										</select></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.page_title" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="pageTitle" placeholder="Page Title" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div>	
					<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.meta_keywords" />
											</label>
										<div class="controls"><div class=""><div class="">
												<textarea name="metaKeyword"
												placeholder="Meta Keywords" style="width:100%;"></textarea></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.meta_description" />
											</label>
										<div class="controls">
											<div class=""><div class=""><div class="">
												<textarea name="metaDescription"
												placeholder="Meta Description" style="width:100%;"></textarea></div></div>
											</div>
										</div>
									</div>
									
					</div>	
					<br>
					<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Language
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select name="language" class="form-control">
										<option value="en" selected="selected">English</option>
										<option value="ko">Korean</option>
										<option value="tr">Turkish</option>
										</select></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">&nbsp;
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="submit" class="btn btn-info"></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							
							
							<%-- <table class="table table-form">
								<thead>
									<tr>
										<th></th>
										<th>&nbsp;</th>


									</tr>
								</thead>
								<tbody>
									<tr>

										<td>
											<div class="field-name">
												<s:text name="tgi.label.page_name" />
											</div>
										</td>
										<td><select class="form-control input-sm" name="pageName"
											id="configStatus" autocomplete="off" required>
												<option value="flight_search"><s:text
														name="tgi.label.flight_search" /></option>
												<option value="hotel_search"><s:text
														name="tgi.label.hotel_search" /></option>
												<option value="car_search"><s:text
														name="tgi.label.car_search" /></option>
												<option value="cruise_search"><s:text
														name="tgi.label.cruise_search" /></option>
										</select></td>
										<td>
											<div class="field-name">
												<s:text name="tgi.label.page_title" />
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="pageTitle" placeholder="Page Title" autocomplete="off"
											required></td>


									</tr>

									<tr>
										<td>
											<div class="field-name">
												<s:text name="tgi.label.meta_keywords" />
											</div>
										</td>
										<td><textarea rows="6" cols="30" name="metaKeyword"
												placeholder="Meta Keywords"></textarea></td>
										<td>
											<div class="field-name">
												<s:text name="tgi.label.meta_description" />
											</div>
										</td>
										<td><textarea rows="6" cols="30" name="metaDescription"
												placeholder="Meta Description"></textarea></td>
									<tr>
										<td>Language</td>
										<td><select name="language" class="form-control">
										<option value="en" selected="selected">English</option>
										<option value="ko">Korean</option>
										<option value="tr">Turkish</option>
										</select></td>
										<td><input type="submit" class="btn btn-info"></td>
										<td></td>
									</tr>
								</tbody>
							</table>
 --%>
						</form>
					</div>
				</div>
				</div>
				</div></div>
</div></div>
                            </section>        
                                    			<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
																				$(
																						document)
																						.ready(
																								function() {
																									$(
																											'#alert_box')
																											.modal(
																													{
																														show : true,
																														keyboard : false
																													});
																									$(
																											'#alert_box_body')
																											.text(
																													'${param.message}');
																								});
																			</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>