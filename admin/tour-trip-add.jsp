<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
  <link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="admin/css/less-plugins/awesome-bootstrap-checkbox.css">
  <script type="text/javascript" src="admin/js/admin/panel-app.js"></script>

<style>
	.input-group {
    position: sticky;
}
</style>

  <section class="wrapper container-fluid">
                  	<div class="">
                      <div class="pnl">
                      <div class="hd clearfix">
                              <h5 class="pull-left">Book Tour Trip</h5>
                        <div class="set pull-right">        
						<div class="clearfix" >
						<a href="addTourTrip" class="btn btn-xs btn-primary"> All Trips </a>
						<a href="addTourTrip" class="btn btn-xs btn-primary"> Tour Trips </a>
						<a href="addHotelTrip" class="btn btn-xs btn-primary"> Hotel Trips </a> 
						<a href="addFlightTrip" class="btn btn-xs btn-primary"> Flight Trips </a> 
						<a href="addCarTrip" class="btn btn-xs btn-primary"> Car Trips </a> 
						<a href="addMiscellaneousTrip" class="btn  btn-xs btn-primary"> Miscellaneous Trips </a>
						</div>
			 			</div>
                     </div>
			<form method="post" class="form-horizontal ng-pristine ng-valid" name="saveTourTripForm" id="saveTourTripForm"> 
			
				<input type="hidden" name="universalOrderDetail.emailId" id="front-user-email-id" value="${tourTripVo.email}">                     
				<input type="hidden" name="frontUserData.firstName" id="front-user-first-name" value="${tourTripVo.firstName}">                     
				<input type="hidden" name="frontUserData.lastName" id="front-user-last-name" value="${tourTripVo.lastName}">                     
				<input type="hidden" name="frontUserData.email" id="front-user-email" value="${tourTripVo.email}">                     
				<input type="hidden" name="frontUserData.mobile" id="front-user-mobile" value="${tourTripVo.mobile}">                     
				<input type="hidden" name="frontUserData.city" id="front-user-city" value="${tourTripVo.city}"> 
				<input type="hidden" name="quotationId" id="trip-quotation-id" value="${quotationId}"> 
				<input type="hidden" name="quotationNumber" id="quotation-number" value="${tripQuotationVo.quotationNumber}"> 
				                   
	  		<section class="tour_details mt-4" id="tour-trip-info">
				<div class="col-md-12 col-xs-12 col-sm-12">
				<div class="container-detached">
					<!-- Follow Up info -->
					<div class="panel panel-flat panel-bg-gray">
						<div class="panel-heading">
							<h6 class="panel-title">Tour Trip Detail<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview">
			                		<img class="clippy" src="admin/img/svg/down-arrow.svg" width="17" alt="Copy to clipboard" ></a></li>
			                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
			                	</ul>
		                	</div>
						</div>
						<hr >
						<div class="panel-body" style="display: block;">
						<div class="row">
						<div class="col-sm-2">
								<div class="form-group">
								<label for="tripType">Tour Listing Type<span class="text-danger" id="mandatory"> * </span></label>
								 <select name="tourType" class="form-control input-sm " id="tour-type" required="required">
									<!-- <option value="">Select Trip Type</option> -->
									<option value="O" selected="selected">Tours</option>
									<option value="R">Activity</option>
									 <option value="M">Events</option>
									 <option value="M">Trade Shows</option>
								</select>
							</div>
							</div>
							<div class="col-md-2 ">
									<div class="form-group">
										<label for="NetRate" class=" control-label">Tour Name </label> <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
										<input type="text" name=tourName autocomplete="off" class="form-control input-sm" required="required" placeholder="Trip Title" id="airline" value="${tripQuotationData.title}">
											 	
									</div>
								</div>
							    <!-- <input type="hidden" autocomplete="off" name="flightOrderRow.productType" class="form-control input-sm" required="required" id="productType" placeholder="Passenger Count" value=""> -->
								<!-- <input type="text" name="flightOrderTripDetailList[0].flightNumber" autocomplete="off" class="form-control input-sm " required="required" placeholder="Tour Number" id="flightNumber">	 -->
							<div class="col-sm-2">
							<div class="form-group">
								<label for="booking-date" class="control-label">Departure Date<span class="text-danger" id="mandatory"> * </span></label> 
								<input type="text" autocomplete="off" name="tourStartDateFlag" class="form-control input-sm datetimepicker1" id="bookingDate" placeholder="Booking Date" required="required" 
									   value="${spysr:formatDate(tripQuotationData.departureDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}">
							</div>
							</div>
							<div class="col-sm-2">
							<div class="form-group">
								<label for="booking-date" class="control-label">Arrival Date<span class="text-danger" id="mandatory"> * </span></label> 
								<input type="text" autocomplete="off" name="tourEndDateFlag" class="form-control input-sm datetimepicker1" id="bookingDate" placeholder="Booking Date" required="required"
									   value="${spysr:formatDate(tripQuotationData.arrivalDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}" >
							</div>
							</div>
							<%-- <div class="col-sm-3">
							<div class="form-group">
								<label for="booking-date" class="control-label">No. of Days<span class="text-danger" id="mandatory"> * </span></label> 
								<input type="text" autocomplete="off" name="flightOrderRow.bookingDate" class="form-control input-sm" id="bookingDate" placeholder="No. of Days" required="required" value="${tourTripVo.duration}" />
							</div>
							</div> --%>
							<div class="col-md-2">
								<div class="form-group">
									<label for="NetRate" class=" control-label">Origin </label>
									<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
									<input type="text" id="origin" name="origin" class="form-control input-sm ui-autocomplete-origin" required="required" placeholder="Origin Name" 
										   value="${tripQuotationData.destinationFrom}" autocomplete="off">
										 
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="NetRate" class=" control-label">Destination
									</label> <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
									<input type="text" name="destination" class="form-control input-sm ui-autocomplete-destination" id="destination" autocomplete="off" required="required" placeholder="Destination Name" 
									       value="${tripQuotationData.destinationTo}">
										 
								</div>
							</div>
							<div class="col-sm-2">
							<div class="form-group">
								<label for="booking-date" class="control-label">Duration (Days)<span class="text-danger" id="mandatory"> * </span></label> 
								<input type="text" name="duration" class="form-control input-sm" value="${tripQuotationVo.duration}">
							</div>
							</div>
							<div class="col-sm-2">
							<div class="form-group">
								<label for="booking-date" class="control-label">Total No. of Passanger(s)<span class="text-danger" id="mandatory"> * </span></label> 
								<select name="totalGuest" class="form-control input-sm" id="bookingDate" required="required">
								  <option value="0" selected="selected">Select Room</option>
								  <c:forEach var="i" begin="0" end="50" step="1">
									<option value="${i}" ${i == tripQuotationData.totalPassanger ? 'selected': ''}>${i}</option>
								</c:forEach>
								</select>
							</div>
							</div>
							<div class="col-sm-2">
							<div class="form-group">
								<label for="booking-date" class="control-label">No. of Rooms(s)<span class="text-danger" id="mandatory"> * </span></label> 
								<select aria-hidden="true" name="totalRoom" id="totalRoom" class="form-control input-sm">
								  <option value="0" selected="selected">Select Room</option>
								  <c:forEach var="i" begin="0" end="50" step="1">
									<option value="${i}" ${i == tourTripVo.numberOfRooms ? 'selected': ''}>${i}</option>
								</c:forEach>
								</select>
							</div>
							</div>
							<div class="col-sm-2">
							<div class="form-group">
								<label for="booking-date" class="control-label">Adult<span class="text-danger" id="mandatory"> * </span></label> 
								<div class="input-group">
								<select aria-hidden="true" name="adult" id="adult" class="form-control input-sm">
								  <option value="0" selected="selected">Adult</option>
								  <c:forEach var="i" begin="0" end="120" step="1">
									<option value="${i}" ${i==tourTripVo.totalAdult ? 'selected': ''}>${i}</option>
								</c:forEach>
								</select>
								<span class="input-group-addon" id="sizing-addon2"><i class="fa fa-angle-left" aria-hidden="true"></i> 10 years</span>
								</div>
							</div>
							</div>
							<div class="col-sm-2">
							<div class="form-group">
								<label for="booking-date" class="control-label">Kid's</label>
								<div class="input-group">
								<select aria-hidden="true" name="kid" id="kid" class="form-control input-sm">
								  <option value="0" selected="selected">Kid's</option>
								  <c:forEach var="i" begin="0" end="120" step="1">
									<option value="${i}" ${i==tourTripVo.totalChild ? 'selected': ''}>${i}</option>
								  </c:forEach>
								</select>
								<span class="input-group-addon" id="sizing-addon2">5 to 8 years</span>
								</div>
							</div>
							</div>
							<div class="col-sm-2">
							<div class="form-group">
								<label for="booking-date" class="control-label">Kid's</label> 
								<div class="input-group"> 
								<select aria-hidden="true" name="infant" id="infant" class="form-control input-sm">
								  <option value="0" selected="selected">New Born</option>
								  <c:forEach var="i" begin="0" end="5" step="1">
									<option value="${i}" ${i==tourTripVo.totalInfant ? 'selected': ''}>${i}</option>
								</c:forEach>
								</select>
								<span class="input-group-addon" id="sizing-addon2">0 to 5 years</span>
								</div>
							</div>
							</div>
							</div>
						</div>
                              </div>
                              </div>
                              </div>
  </section>
	<section class="customer_details mt-4" id="tour-passagner-detail">
			<div class="col-md-12 col-xs-12 col-sm-12">
			<div class="container-detached">
					<!-- Follow Up info -->
					<div class="panel panel-flat panel-bg-gray">
						<div class="panel-heading">
							<h6 class="panel-title">Passenger Detail<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="17" alt="Copy to clipboard" ></a></li>
			                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
			                	</ul>
		                	</div>
						</div>
						<hr >
						<div class="panel-body" style="display: block;">
						<div class="form_box_block ">
						<div class="row row-minus">
                            <div class="detailswithRM"> 	 
								<div class="col-sm-2">
									<div class="form-group">
										<label for="hotelName" class=" control-label">Title
										</label> <select name="orderCustomer.title" class="form-control input-sm" id="title1">
											<option value="Mr" selected="selected">Mr</option>
											<option value="Mrs">Mrs</option>
											<option value="Miss">Miss</option>
											<option value="Ms">Ms</option>
											<option value="Master">Master</option>

										</select>
										
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label for="hotelName" class=" control-label">FirstName <span class="text-danger" id="mandatory"> * </span></label> 
										<input type="text" autocomplete="off" name="orderCustomer.firstName" class="form-control input-sm firstName" required="required" id="firstName" placeholder="First Name" value="${tourTripVo.firstName}">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label for="hotelChain" class="  control-label">LastName
										</label> 
										<input type="text" autocomplete="off" required="required" name="orderCustomer.lastName" id="lastName" class="form-control input-sm " placeholder="Last Name" value="${tourTripVo.lastName}">

									</div>
								</div>
								<div class="col-sm-2">
								<div class="form-group">
									<label for="City" class=" control-label">Email <span class="text-danger" id="mandatory"> * </span> </label>
									<input type="email" autocomplete="off" name="orderCustomer.email" id="emailId" class="form-control input-sm" placeholder="Email" value="${tourTripVo.email}" required="required">
								</div>
								</div>
								<div class="col-sm-2">
								<div class="form-group">
									<label for="mobile" class=" control-label">Mobile Number <span class="text-danger" id="mandatory"> * </span> </label>
									<input type="text" autocomplete="off" name="orderCustomer.mobile" id="mobileNo" class="form-control input-sm"  placeholder="Mobile" value="${tourTripVo.mobile}">
										

								</div>
								 </div>
								 <div class="col-sm-2">
								<div class="form-group">
									<label for="City" class=" control-label">Date of Birth <span class="text-danger" id="mandatory"> * </span></label>
									<input type="text" autocomplete="off" name="orderCustomer.birthDate" id="date_of_birth" class="form-control input-sm date_of_birth" placeholder="DOB">
								</div>
												</div>
							<div class="more-option" style="display: none">
								<div class="col-sm-2">
								<div class="form-group">
									<label for="City" class=" control-label">City<span class="text-danger" id="mandatory"> * </span></label>
									<input type="text" autocomplete="off" name="orderCustomer.city" id="city" class="form-control input-sm" placeholder="City" value="${tourTripVo.city}">
								</div>
												</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label for="currency" class=" control-label">Nationality <span class="text-danger" id="mandatory"> * </span>
										</label> 
										<select class="form-control input-sm" name="orderCustomer.nationality" id="nationality" >
															<option selected  value=""><s:text name="tgi.label.select_country" />	</option>
															<c:forEach items="${CountryList}" var="country">
																<option value="${country.countryName}" ${country.countryName == 'India' ? 'selected': ''}>${country.countryName}</option>
															</c:forEach>
														</select>	
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label for="currency" class="  control-label">Passport Date of Issue</label> 
										<input type="text" name="orderCustomer.passportIssueDate" class="form-control input-sm passport_date_issue" id=passport_date_issue placeholder="Passport Date Of Issue ">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label for="currency" class="  control-label">Passport Expire Date </label> 
										<input type="text" name="orderCustomer.passportExpiryDate" class="form-control input-sm expiredate hasDatepicker" id="passport_date_expire" placeholder="Passport Expire Date">
											
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label for="currency" class="control-label">Passport
											Number </label> <input type="text" class="form-control input-sm " placeholder="Passport Number" name="orderCustomer.passportNumber" id="passportNumb1">
											
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label for="hotel Country" class=" control-label">Passport Issuing Country </label> 
										<select class="form-control input-sm" name="orderCustomer.passportIssuingCountry">
											<option selected  value=""><s:text name="tgi.label.select_country" />	</option>
											<c:forEach items="${CountryList}" var="country">
												<option value="${country.countryName}" ${country.countryName == showUserDetail.country ? 'selected': ''}>${country.countryName}</option>
											</c:forEach>
										</select>
										

									</div>
								</div>
								</div>  
							</div>
							 <div class="col-sm-2 mb-2 pull-right">
									<a class="btn btn-primary btn-xs pull-right" id="filmore"> More options <i class="fa fa-angle-up"></i></a>
							</div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12 col-sm-12 mb-2">
								<div class="pull-right">
									<a class="btn btn-primary btn-sm" id="add_trip">Add Room </a>
									<a class="btn btn-danger btn-sm" id="remove_trip">Remove Room</a>
								</div>

							</div>
                        </div>
           <!-- room guest info  -->
						<div class="form_box_block ">
						<div class="row row-minus">
							<div class="col-md-1 col-sm-1" style="margin-top: 22px">
								<h5><span class="room-no-count">Room 1 :</span></h5>
							</div>
							<div class="col-md-2 col-sm-2">
								<label for="room-name" class="form-control-label">Room Name</label>
									<div class="form-group">
									<input type="text" name="tourOrderRowRoomInfos[0].name" id="room-name" class="form-control input-sm" placeholder="" required="required"> 
									</div>
							</div>
							<div class="col-md-2 col-sm-2">
								<label for="meal-type" class="form-control-label">Meal Type</label>
									<div class="form-group">
									<select name="tourOrderRowRoomInfos[0].mealType" class="form-control input-sm" id="meal-type" required="required">
											<option value="" selected="selected">Select Plan</option>
											<option value="CP">CP</option>
											<option value="AP">AP</option>
											<option value="EP">EP</option>
											<option value="MAP">MAP</option>
										</select>
									</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label for="room-description" class="form-control-label">Room Description</label>
									<div class="form-group">
									<textarea rows="1" name="description" id="room-description" class="form-control input-sm" placeholder="Describe Something"></textarea>
									</div>
							</div>
						</div>
						<p class="" style="border-bottom: 1px solid #dadada;">
								 <span> <b class="blue-grey-text">&nbsp;Guest : 1</b></span>
						</p>
						<div class="row row-minus">
								<div class="col-md-1 col-sm-1">
									<div class="form-group">
										<label for="title1" class=" control-label">Title
										</label> 
										<select name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].title" class="form-control input-sm" id="title1">
											<option value="Mr" selected="selected">Mr</option>
											<option value="Mrs">Mrs</option>
											<option value="Miss">Miss</option>
											<option value="Ms">Ms</option>
											<option value="Master">Master</option>
										</select>
										
									</div>
								</div>
								<div class="col-md-2 col-sm-2">
									<div class="form-group">
										<label for="hotelName" class=" control-label">FirstName <span class="text-danger" id="mandatory"> * </span> </label> 
										<input type="text" autocomplete="off" name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].firstName" class="form-control input-sm firstName" 
												required="required" id="firstName" placeholder="First Name" value="${tourTripVo.firstName}">
											

									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label for="hotelChain" class="  control-label">LastName </label> 
										<input type="text" autocomplete="off" required="required" name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].lastName" 
											id="lastName" class="form-control input-sm" placeholder="Last Name" value="${tourTripVo.lastName}" />

									</div>
								</div>
								<div class="col-sm-1">
								<div class="form-group">
									<label for="City" class=" control-label">Date of Birth <span class="text-danger" id="mandatory"> * </span></label>
									<input type="text" autocomplete="off" name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].birthDate" id="date_of_birth" class="form-control input-sm date_of_birth" placeholder="DOB">
								</div>
							</div>
								<div class="col-sm-1">
								<div class="form-group">
									<label for="City" class=" control-label">Passenger Type<span class="text-danger" id="mandatory"> </span></label>
									<select name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].paxType" id="pax-type" class="form-control input-sm" required="required">
										<option value="" >Select Type</option>
										<option value="adult" >Adult</option>
										<option value="child">Child</option>
										<option value="senior">Senior</option>
										<option value="infant">Infant</option>
									</select>
								</div>
							</div>
							<div class="col-sm-1">
									<div class="form-group">
										<label for="currency" class=" control-label">Nationality <span class="text-danger" id="mandatory"> * </span>
										</label> 
										<select class="form-control input-sm" name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].nationality" id="nationality" >
															<option selected  value=""><s:text name="tgi.label.select_country" />	</option>
															<c:forEach items="${CountryList}" var="country">
																<option value="${country.countryName}" ${country.countryName == 'India' ? 'selected': ''}>${country.countryName}</option>
															</c:forEach>
										</select>	
									</div>
								</div>
							<!-- <div class="col-sm-1 mt-3">
									<div class="form-group">
										<a class="btn btn-xs btn-primary" id="guest-more-option"><i class="fa fa-plus"></i></a>	
									</div>
								</div> -->
							 <div class="show-guest-more-option" style="display: block">
								<div class="col-sm-1">
									<div class="form-group">
									<span data-toggle="tooltip" data-placement="top" title="Passport Date of Issue">
										<label for="currency" class="  control-label">PDI</label> 
										<input type="text" name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].issueDate" class="form-control input-sm passport_date_issue" id=passport_date_issue placeholder="Passport Date Of Issue ">
									</span>
									</div>
								</div>
								<div class="col-sm-1">
									<div class="form-group">
									<span data-toggle="tooltip" data-placement="top" title="Passport Expire Date ">
										<label for="currency" class="  control-label">PED</label> 
										<input type="text" name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].expiryDate" class="form-control input-sm expiredate" id="passport_date_expire" placeholder="Passport Expire Date">
									</span>
									</div>
								</div>
								<div class="col-sm-1">
									<div class="form-group">
									<span data-toggle="tooltip" data-placement="top" title="Passport Number ">
										<label for="currency" class="control-label">PN</label> 
										<input type="text" class="form-control input-sm " placeholder="Passport Number" name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].passportNumber" id="passportNumb1">
										</span>	
									</div>
								</div>
								<div class="col-sm-1">
									<div class="form-group">
									<span data-toggle="tooltip" data-placement="top" title="Passport Issuing Country  ">
										<label for="hotel Country" class=" control-label">PIC</label> 
										<select class="form-control input-sm" name="tourOrderRowRoomInfos[0].tourOrderRowGuests[0].passportIssuingCountry">
											<option selected  value=""><s:text name="tgi.label.select_country" />	</option>
											<c:forEach items="${CountryList}" var="country">
												<option value="${country.countryName}" ${country.countryName == showUserDetail.country ? 'selected': ''}>${country.countryName}</option>
											</c:forEach>
										</select>
										
									</span>
									</div>
								</div>
								</div>
							<div class="col-md-12 col-sm-12 mb-2">
								<div class="pull-right">
									<a class="btn btn-primary btn-xs" id="add_trip">Add Guest</a>
									<a class="btn btn-danger btn-xs" id="remove_trip">Remove Guest</a>
								</div>

							</div>
                        </div>
                        </div>
                        </div>
                        </div></div></div>
  </section>
 <section class="customer_details" id="trip-itinerary"> 
			<div class="col-md-12 col-xs-12 col-sm-12">
				<div class="content-detached">
					<!-- Follow Up info -->
					<div class="panel panel-flat panel-bg-gray panel-collapsed">
						<div class="panel-heading">
							<h6 class="panel-title">Trip Itinerary<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="17" alt="Copy to clipboard" ></a></li>
			                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
			                	</ul>
		                	</div>
						</div>
						<hr >
						<div class="panel-body" style="display: block;">
							<c:if test="${tripQuotationVo.itineraryMap != null && tripQuotationVo.itineraryMap.size()>0}">
							<c:forEach items="${tripQuotationVo.itineraryMap}" var="itineraryObj" varStatus="status">
							<div class="form_box_block">
                     				<div class="row row-minus">
									<div class="form-group row has-feedback">
				                      <label class="col-md-2 col-form-label"><span class="itin-day-count">Day <span id="day-to-add">${itineraryObj.key}</span>: </span></label>
				                      <input type="hidden" name="tourOrderRowItineraryDescription.tourOrderRowItineraryDayDescriptions[${status.index}].day" id="day_${status.count}" value="${itineraryObj.value.day}">
				                      <div class="col-md-10">
				                        <input type="text" class="form-control input-sm" name="tourOrderRowItineraryDescription.tourOrderRowItineraryDayDescriptions[${status.index}].title" placeholder="Day Title" autocomplete="off" required="required"  id="itinerary_title_${status.count}"
				                        data-bv-field="tourOrderRowItineraryDescription.tourOrderRowItineraryDayDescriptions[${status.index}].title" value="${itineraryObj.value.title}">
				                        <i class="form-control-feedback" data-bv-icon-for="tourOrderRowItineraryDescription.tourOrderRowItineraryDayDescriptions[${status.index}].title" style="display: none;"></i>
				                      <small class="help-block" data-bv-validator="notEmpty" data-bv-for="tourOrderRowItineraryDescription.tourOrderRowItineraryDayDescriptions[${status.index}].title" data-bv-result="NOT_VALIDATED" style="display: none;">Please enter a value</small></div>
				                    </div>
				                    <div class="col-md-12">
										<div class="controls">
											<div class=""><div class="form-group">
												<textarea class="form-control " id="description_${status.count}" name="tourOrderRowItineraryDescription.tourOrderRowItineraryDayDescriptions[${status.index}].description" autocomplete="off" rows="3" placeholder="Briefly describe...">${itineraryObj.value.description}</textarea>
												</div>
											</div>
										</div>
									</div>
									</div>
									<div class="row row-minus">
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput">Hotel on Day 1:</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm" id="hotelName_${status.count}" name="tourOrderRowItineraryDescription.tourOrderRowItineraryDayDescriptions[${status.index}].hotelName" autocomplete="off" value="${itineraryObj.value.hotelName}">
												</div></div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput">&nbsp;</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm" id="hotelCity_${status.count}" name="tourOrderRowItineraryDescription.tourOrderRowItineraryDayDescriptions[${status.index}].hotelCity" placeholder="Hotel City" autocomplete="off" value="${itineraryObj.value.hotelCity}">
												</div></div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput">&nbsp;</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control input-sm" name="tourOrderRowItineraryDescription.tourOrderRowItineraryDayDescriptions[${status.index}].hotelRating" id="hotel-rating_${status.count}" style="width: 100%;">
                                                        <option value="${itineraryObj.value.hotelRating}" selected="selected">${itineraryObj.value.hotelRating}</option>
                                                        <option value="1"> 1 Star</option>
                                                        <option value="2">2 Star</option>
                                                        <option value="3">3 Star</option>
                                                        <option value="4">4 Star</option>
                                                        <option value="5">5 Star</option>
                                                        <option value="5+">5+ Star</option>
                                                 </select>
												</div></div>
											</div>
										</div>
									</div>
									</div>	     
                                	</div>
                                	</c:forEach>
                                	</c:if>
						</div></div>
                </div>
                </div> 
  </section>
  <section class="customer_details" id="trip-inclusion">
			<div class="col-md-12 col-xs-12 col-sm-12">
			<div class="container-detached">
					<!-- Follow Up info -->
					<div class="panel panel-flat panel-bg-gray">
						<div class="panel-heading">
							<h6 class="panel-title">Trip Inclusion<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="17" alt="Copy to clipboard" ></a></li>
			                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
			                	</ul>
		                	</div>
						</div>
						<hr >
						<div class="panel-body" style="display: block;">
						<div id="inputsWrapperInclusions">
						<div class="col-md-12 col-sm-12 mb-2">
								<div class="pull-right">
									<a class="btn btn-sm btn-success addMoreBoxInner" id="addMoreBoxInclusions" data-inputs-wrapper-id="inputsWrapperInclusions" data-inputs-wrapper-inner-id="inputsWrapperInnerInclusions" 
									   data-add-more-box="addMoreBox" data-remove-box="removeBox" data-add-more-box-inner="addMoreBoxInner" data-add-box-id="addMoreBoxInclusions" data-add-btn-txt="Add More" data-field-count=1 
									   data-box-count="<c:choose><c:when test="${tripQuotationVo.tripQuotationInclusionsList != null && tripQuotationVo.tripQuotationInclusionsList.size() >0}">
									${tripQuotationVo.tripQuotationInclusionsList.size()}</c:when><c:otherwise>1</c:otherwise></c:choose>" 
									   data-max-count=2 data-max-flag=false data-type="inclusions" ><img class="clippy" src="admin/img/svg/plus.svg" width="15"> &nbsp;Add</a>
									<a class="btn btn-sm btn-danger removeBox" data-add-btn-id="addMoreBoxInclusions" ><i class="fa fa-minus"></i> Remove</a>
								</div>

						</div>
						
						<c:choose>
						<c:when test="${tripQuotationVo.tripQuotationInclusionsList != null && tripQuotationVo.tripQuotationInclusionsList.size()>0}">
						<c:forEach items="${tripQuotationVo.tripQuotationInclusionsList}" var="inclusionObj" varStatus="status">
						<div class="row inputsWrapperInnerInclusions">
									<div class="col-sm-5">
									<div class="form-group">
										 <label for="totalGstTax">Title</label> 
											<input type="text" name="tourOrderRowItineraryDescription.tourOrderRowItineraryInclusions[${status.index}].title" id="inclusion-title_${status.count}" value="${inclusionObj.title}" class="form-control input-sm">
										</div>
									</div>
									<div class="col-sm-6">
									<div class="form-group">
										 <label for="totalGstTax">Description</label> 
											<textarea name="tourOrderRowItineraryDescription.tourOrderRowItineraryInclusions[${status.index}].description" id="inclusion-description_${status.count}"  class="form-control input-sm" rows="1">${inclusionObj.description}</textarea>
										</div>
									</div>
						</div>
						</c:forEach>
						</c:when>
						<c:otherwise>
						<div class="row inputsWrapperInnerInclusions">
									<div class="col-sm-5">
									<div class="form-group">
										 <label for="totalGstTax">Title</label> 
											<input type="text" name="tourOrderRowItineraryDescription.tourOrderRowItineraryInclusions[0].title" id="inclusion-title" value="" class="form-control input-sm">
										</div>
									</div>
									<div class="col-sm-6">
									<div class="form-group">
										 <label for="totalGstTax">Description</label> 
											<textarea name="tourOrderRowItineraryDescription.tourOrderRowItineraryInclusions[0].description" id="inclusion-description"  class="form-control input-sm" rows="1"></textarea>
										</div>
									</div>
						</div>
						</c:otherwise>
						</c:choose>
                       </div>
                       </div>
                         </div>
                     </div>
                     </div> 
  </section>
  <section class="customer_details" id="trip-exclusion">
			<div class="col-md-12 col-xs-12 col-sm-12">
			<div class="container-detached">
					<!-- Follow Up info -->
					<div class="panel panel-flat panel-bg-gray">
						<div class="panel-heading">
							<h6 class="panel-title">Trip Exclusion<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="17" alt="Copy to clipboard" ></a></li>
			                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
			                	</ul>
		                	</div>
						</div>
						<hr >
						<div class="panel-body" style="display: block;">
						<div id="inputsWrapperExclusions">
						<div class="col-md-12 col-sm-12 mb-2">
								<div class="pull-right">
								<span class="controls addMoreBox" >
									<a class="btn btn-sm btn-success addMoreBoxInner" id="addMoreBoxExclusions" data-inputs-wrapper-id ="inputsWrapperExclusions" data-inputs-wrapper-inner-id="inputsWrapperInnerExclusions" 
									   data-add-more-box="addMoreBox" data-remove-box="removeBox" data-add-more-box-inner="addMoreBoxInner" data-add-box-id="addMoreBoxIncExclusions" data-add-btn-txt="Add More" data-field-count=1
									   data-box-count="<c:choose><c:when test="${tripQuotationVo.tripQuotationExclusionsList != null && tripQuotationVo.tripQuotationExclusionsList.size() >0}">
									${tripQuotationVo.tripQuotationExclusionsList.size()}</c:when><c:otherwise>1</c:otherwise></c:choose>" 
									   data-max-count=2 data-max-flag=false data-type="exclusions" ><img class="clippy" src="admin/img/svg/plus.svg" width="15">&nbsp; Add</a>
									<a class="btn btn-sm btn-danger removeBox" data-add-btn-id="addMoreBoxExclusions" ><i class="fa fa-minus"></i> Remove</a>
								</span>
								</div>

						</div>
						<c:choose>
						<c:when test="${tripQuotationVo.tripQuotationExclusionsList != null && tripQuotationVo.tripQuotationExclusionsList.size()>0}">
						<c:forEach items="${tripQuotationVo.tripQuotationExclusionsList}" var="exclusionObj" varStatus="status">
						<div class="row inputsWrapperInnerExclusions">
									<div class="col-sm-5">
									<div class="form-group">
										 <label for="totalGstTax">Title</label> 
											<input type="text" name="tourOrderRowItineraryDescription.tourOrderRowItineraryExclusions[${status.index}].title" id="exclusion-title_${status.count}" value="${exclusionObj.title}" class="form-control input-sm">
										</div>
									</div>
									<div class="col-sm-6">
									<div class="form-group">
										 <label for="totalGstTax">Description</label> 
											<textarea name="tourOrderRowItineraryDescription.tourOrderRowItineraryExclusions[${status.index}].description" id="exclusion-description_${status.count}"  class="form-control input-sm" rows="1">${exclusionObj.description}</textarea>
										</div>
									</div>
						</div>
						</c:forEach>
						</c:when>
						<c:otherwise>
						<div class="row inputsWrapperInnerExclusions">
									<div class="col-sm-5">
									<div class="form-group">
										 <label for="totalGstTax">Title</label> 
											<input type="text" name="tourOrderRowItineraryDescription.tourOrderRowItineraryExclusions[0].title" id="exclusion-title" value=" " class="form-control input-sm">
										</div>
									</div>
									<div class="col-sm-6">
									<div class="form-group">
										 <label for="totalGstTax">Description</label> 
											<textarea name="tourOrderRowItineraryDescription.tourOrderRowItineraryExclusions[0].description" id="exclusion-description"  class="form-control input-sm" rows="1"></textarea>
										</div>
									</div>
						</div>
						</c:otherwise>
						</c:choose>
						</div>
                       </div>
                         </div>
                     </div>
                     </div> 
  </section>
  <section class="customer_details" id="trip-cancellation-policy">
			<div class="col-md-12 col-xs-12 col-sm-12">
			<div class="container-detached">
					<!-- Follow Up info -->
					<div class="panel panel-flat panel-bg-gray">
						<div class="panel-heading">
							<h6 class="panel-title">Trip Cancellation Policy<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="17" alt="Copy to clipboard" ></a></li>
			                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
			                	</ul>
		                	</div>
						</div>
						<hr >
						<div class="panel-body" style="display: block;">
						<div id="inputsWrapperCancellationPolicy">
						<div class="col-md-12 col-sm-12 mb-2">
						<div class="pull-right">
						<span class="controls addMoreBox" >
							<a class="btn btn-sm btn-success addMoreBoxInner" id="addMoreBoxCancellationPolicy" data-inputs-wrapper-id ="inputsWrapperCancellationPolicy" data-inputs-wrapper-inner-id="inputsWrapperInnerCancellationPolicy" 
							data-add-more-box="addMoreBox" data-remove-box="removeBox" data-add-more-box-inner="addMoreBoxInner" data-add-box-id="addMoreBoxCancellationPolicy" data-add-btn-txt="Add More" data-field-count=1 
							data-box-count="<c:choose><c:when test="${tripQuotationVo.tripQuotationCancellationPolicyList != null && tripQuotationVo.tripQuotationCancellationPolicyList.size() >0}">
									${tripQuotationVo.tripQuotationCancellationPolicyList.size()}</c:when><c:otherwise>1</c:otherwise></c:choose>" 
							data-max-count=2 data-max-flag=false data-type="CancellationPolicy" ><img class="clippy" src="admin/img/svg/plus.svg" width="12">&nbsp; Add</a>
						</span>
							<a class="btn btn-sm btn-danger removeBox" data-add-btn-id="addMoreBoxCancellationPolicy" ><i class="fa fa-minus"></i> Remove</a>
						</div>

						</div>
						<c:choose>
						<c:when test="${tripQuotationVo.tripQuotationCancellationPolicyList != null && tripQuotationVo.tripQuotationCancellationPolicyList.size()>0}">
							<c:forEach items="${tripQuotationVo.tripQuotationCancellationPolicyList}" var="cancellationObj" varStatus="status">
							<div class="row inputsWrapperInnerCancellation">
									<div class="col-sm-3">
									<div class="form-group">
										 <label for="cancel-cancellation-day_${status.count}">Days</label> 
											<input type="text" name="tourOrderRowCancellationPolicies[${status.index}].days" id="cancel-cancellation-day_${status.count}" value="${cancellationObj.cancellationDay}" class="form-control input-sm">
										</div>
									</div>
									<div class="col-sm-3">
									<div class="form-group">
										 <label for="cancel-fee-amount_${status.count}">Amount</label> 
										 <div class="input-group">
											<input type="text" name="tourOrderRowCancellationPolicies[${status.index}].feeAmount" id="cancel-fee-amount_${status.count}" value="${cancellationObj.feeAmount}" class="form-control input-sm">
											<span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
										</div>
										</div>
									</div>
									<div class="col-sm-5">
									<div class="form-group">
										 <label for="cancel-remarks_${status.count}">Description</label> 
											<textarea name="tourOrderRowCancellationPolicies[${status.index}].remarks" id="cancel-remarks_${status.count}"  class="form-control input-sm" rows="1">${cancellationObj.remarks}</textarea>
										</div>
									</div>
							</div>
							</c:forEach>
						</c:when>
						<c:otherwise>
						<div class="row inputsWrapperInnerCancellation">
									<div class="col-sm-3">
									<div class="form-group">
										 <label for="cancel-cancellation-day">Days</label> 
											<input type="text" name="tourOrderRowCancellationPolicies[0].days" id="cancel-cancellation-day" value="" class="form-control input-sm">
										</div>
									</div>
									<div class="col-sm-3">
									<div class="form-group">
										 <label for="cancel-fee-amount">Amount</label> 
										 <div class="input-group">
											<input type="text" name="tourOrderRowCancellationPolicies[0].feeAmount" id="cancel-fee-amount" value="" class="form-control input-sm">
											<span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
										</div>
										</div>
									</div>
									<div class="col-sm-5">
									<div class="form-group">
										 <label for="cancel-remarks">Description</label> 
											<textarea name="tourOrderRowCancellationPolicies[0].remarks" id="cancel-remarks"  class="form-control input-sm" rows="1"></textarea>
										</div>
									</div>
							</div>
						</c:otherwise>
						</c:choose>
						</div>
                       </div>
                         </div>
                     </div>
                     </div> 
  </section>
  <section class="customer_details" id="trip-item-type">
			<div class="col-md-12 col-xs-12 col-sm-12">
			<div class="container-detached">
					<!-- Follow Up info -->
					<div class="panel panel-flat panel-bg-gray">
						<div class="panel-heading">
							<h6 class="panel-title">Trip Item Type<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="17" alt="Copy to clipboard" ></a></li>
			                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
			                	</ul>
		                	</div>
						</div>
						<hr >
						<div class="panel-body" style="display: block;">
						<div id="inputsWrapperItemType">
						<div class="col-md-12 col-sm-12 mb-2">
								<div class="pull-right">
								<span class="controls addMoreBox" >
									<a class="btn btn-sm btn-success addMoreBoxInner" id="addMoreBoxItemType" data-inputs-wrapper-id ="inputsWrapperItemType" data-inputs-wrapper-inner-id="inputsWrapperInnerItemType" 
									data-add-more-box="addMoreBox" data-remove-box="removeBox" data-add-more-box-inner="addMoreBoxInner" data-add-box-id="addMoreBoxItemType" data-add-btn-txt="Add More" data-field-count=1 
									data-box-count="<c:choose><c:when test="${tripQuotationVo.tripQuotationPaymentPolicyList != null && tripQuotationVo.tripQuotationPaymentPolicyList.size() >0}">
									${tripQuotationVo.tripQuotationPaymentPolicyList.size()}</c:when><c:otherwise>1</c:otherwise></c:choose>" 
									data-max-count=2 data-max-flag=false data-type=ItemType ><img class="clippy" src="admin/img/svg/plus.svg" width="12">&nbsp; Add</a>
								</span>
								
									<a class="btn btn-sm btn-danger removeBox" data-add-btn-id="addMoreBoxItemType" ><i class="fa fa-minus"></i> Remove</a>
								</div>

						</div>
						<c:choose>
						<c:when test="${tripQuotationVo.tripQuotationPaymentPolicyList != null && tripQuotationVo.tripQuotationPaymentPolicyList.size()>0}">
						<c:if test="${tripQuotationVo.tripQuotationPaymentPolicyList != null && tripQuotationVo.tripQuotationPaymentPolicyList.size()>0}">
						<c:forEach items="${tripQuotationVo.tripQuotationPaymentPolicyList}" var="paymentPolicyObj" varStatus="status">
						<div class="row inputsWrapperInnerItemType">
									<div class="col-md-3 col-sm-3">
									<div class="form-group">
										 <label for="item-type_${status.count}" class="form-control-label">Type</label> 
										 <input type="text" name="tourOrderRowItineraryDescription.tourOrderRowItineraryItem[${status.index}].type" id="item-type_${status.count}" value="Payment Policy" class="form-control input-sm" required="required">
										</div>
									</div>
									<div class="col-md-3 col-sm-3">
									<div class="form-group">
										 <label for="item-title_${status.count}" class="form-control-label">Title</label> 
										 <input type="text" name="tourOrderRowItineraryDescription.tourOrderRowItineraryItem[${status.index}].title" id="item-title_${status.count}" value=" " class="form-control input-sm">
										</div>
									</div>
									<div class="col-md-5 col-sm-5">
									<div class="form-group">
										 <label for="item-description_${status.count}" class="form-control-label">Description</label> 
											<textarea name="tourOrderRowItineraryDescription.tourOrderRowItineraryItem[${status.index}].description" id="item-description_${status.count}"  
													  class="form-control input-sm" rows="1">${paymentPolicyObj.remarks}</textarea>
										</div>
									</div>
						</div>
						</c:forEach>
						</c:if>
						</c:when>
						<c:otherwise>
						<div class="row inputsWrapperInnerItemType">
									<div class="col-md-3 col-sm-3">
									<div class="form-group">
										 <label for="totalGstTax">Type</label> 
										<input type="text" name="tourOrderRowItineraryDescription.tourOrderRowItineraryItem[0].type" id="item-type" value="" class="form-control input-sm" required="required">
										</div>
									</div>
									<div class="col-md-3 col-sm-3">
									<div class="form-group">
										 <label for="item-title" class="form-control-label">Title</label> 
										 <input type="text" name="tourOrderRowItineraryDescription.tourOrderRowItineraryItem[0].title" id="item-title" value="" class="form-control input-sm">
										</div>
									</div>
									<div class="col-md-5 col-sm-5">
									<div class="form-group">
										 <label for="totalGstTax">Description</label> 
											<textarea name="tourOrderRowItineraryDescription.tourOrderRowItineraryItem[0].description" id="item-description" class="form-control input-sm" rows="1"></textarea>
										</div>
									</div>
						</div>
						
						</c:otherwise>
						</c:choose>
						</div>
                       </div>
                         </div>
                     </div>
                     </div> 
  </section>
  <section class="customer_details" id="trip-payment-detail">
			<div class="col-md-12 col-xs-12 col-sm-12 mb-7">
			<div class="container-detached">
					<!-- Follow Up info -->
					<div class="panel panel-flat panel-bg-gray">
						<div class="panel-heading">
							<h6 class="panel-title">Tour Payment Detail<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="17" alt="Copy to clipboard" ></a></li>
			                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
			                	</ul>
		                	</div>
						</div>
						<hr >
						<div class="panel-body" style="display: block;">
						<div class="row">
									<div class="col-md-2 col-sm-2">
									<div class="form-group">
										 <label for="totalGstTax">Supplier Price </label> 
											<input type="text" name="apiPrice" id="supplier-price" value="${tripQuotationData.supplierPrice}" class="form-control input-sm">
										</div>
									</div>
									<div class="col-md-2 col-sm-2">
									<div class="form-group">
										 <label for="totalGstTax">Markup Price </label> 
											<input type="text" name="markupAmount" id="markup-amount" value="${tripQuotationData.markup}" class="form-control input-sm">
										</div>
									</div>
									<div class="col-md-1 col-sm-1 ">	
									<div class="form-group has-feedback ">
									<div data-toggle="tooltip" data-placement="top" title="D = (SP + MP) - D">
										<label for="discount">Discount</label>
											<input type="text" name="discountAmount" class="form-control input-sm " value="0" placeholder="Enter the discount amount" id="discount-fee" />
										</div>
									</div>
									</div>
									<div class="col-md-1 col-sm-1"> 
									<div class="form-group">
										<div data-toggle="tooltip" data-placement="top" title="Gst Tax Amount ( % )">
										<label for="totalGstTaxAmount">GST ( % )</label> 
										<div class="inner-addon right-addon">
   												<input type="text" autocomplete="off"  onchange="numbersonly(this)" class="form-control input-sm" id="gst-percentage" value="${tripQuotationData.gstPercentage}"> 
 										</div>
 										</div>
									</div>
									</div>
									<div class="col-md-1 col-sm-1"> 
									<div class="form-group">
										<div data-toggle="tooltip" data-placement="top" title="Gst Tax Amount">
										<label for="totalGstTaxAmount">GST Amount</label> 
										<div class="inner-addon right-addon">
   												<input type="text" autocomplete="off" name="gstTaxAmount" onchange="numbersonly(this)" class="form-control input-sm" id="gst-amount" value="${tripQuotationData.gst}"> 
 										</div>
 										</div>
									</div>
									</div>
									<div class="col-md-1 col-sm-2 ">	
									<div class="form-group has-feedback ">
										<label for="discount">Base Price</label>
											<input type="text" class="form-control input-sm " value="${tripQuotationVo.basePriceAfterGst}" required="required" id="base-fare" readonly="readonly" />
											
										</div>
									</div>
									<div class="col-sm-1">	
									<div class="form-group has-feedback ">
										<label for="discount">Processing Fee ( % )</label>
											<input type="text" class="form-control input-sm " value="${tripQuotationData.processingFeePercentage}" required="required" id="processing-fee-persentage" />
										</div>
									</div>
									<div class="col-sm-1">	
									<div class="form-group has-feedback ">
										<label for="discount">Processing Fee</label>
											<input type="text" name="processingFee" class="form-control input-sm " value="${tripQuotationData.processingFee}" required="required"  id="processing-fee" />
										</div>
									</div>
									
									<div class="col-md-2 col-sm-2">
									<div class="form-group">
										 <label for="totalGstTax" style="color: red;">Tour Price </label> 
											<input type="text" name="totalPrice" style="color: red; font-size: 16px" id="total-price" value="${tripQuotationData.totalAmount}" class="form-control input-sm" readonly="readonly">
										</div>
									</div>
									<div class="col-md-2 col-sm-2">
									<div class="form-group">
									<label for="currency">Booking Currency </label>
										<select class="form-control input-sm" required="required" name="baseCurrency" id="baseCurrency" >
										<option value="USD">USD</option>
										<option value="INR" selected>INR</option>
										<option value="EURO">EURO</option>
										<option value="YEN">YEN</option>
										<option value="DINA">DINA</option>
									</select>
										
									</div>
									</div>
									<div class="col-md-2 col-sm-2">	
									<div class="form-group">
										<label for="currency">INR To Booking </label>
											<input type="text" class="form-control input-sm" value="1.0" required="required" placeholder="Conversion Rate" name="baseToBookingExchangeRate" readonly="readonly">
										</div>
									</div>
							<div class="col-md-2 col-sm-2">
							<div class="form-group">
								<label for="NetRate" class=" control-label">Booking Status </label>
									<select class="form-control input-sm" required="required" name="bookingStatus" id="bookingStatus">
										<option value="CONFIRM">Confirmed</option>
										<option value="PENDING">Pending</option>
										<option value="FAILED">Failed</option>
									</select>
									
								</div>
							</div>
							</div>
							<div class="row">
							<hr>
							<div class="col-md-2 col-sm-2 " > 
							<div class="form-group">
								<label for="currency" class=" control-label">Payment Mode </label>
									<select class="form-control input-sm" id="clientPaymentMethod" name="paymentTransactionData.paymentMethod" required="required">
										<option value="card" >CARD</option>
										<option value="cash" selected>CASH</option>
										<option value="online">ONLINE</option>
									</select>
								</div>
							</div>
							<div class="col-md-2 col-sm-2"> 
							<div class="form-group">
								<label for="currency" class=" control-label">Customer Payment Type </label>
									<select class="form-control input-sm" name="paymentTransactionData.paymentType" id="paymentType">
										<option value="Full" selected="selected">Full Payment</option>
										<option value="Partial">Partial Payment</option>
										<option value="Zero">No Payment</option>
									</select>
								</div>
							</div>
							<div class="col-md-2 col-sm-2">
								<div class="form-group">
									<label for="NetRate">Payment Status </label>
										<select class="form-control input-sm" required="required" name="paymentTransactionData.paymentStatus" id="paymentStatus">
											<option value="SUCCESS">Success</option>
											<option value="PENDING">Pending</option>
										</select>
									</div>
							</div>
							<div class="col-md-2 col-sm-2" > 
							<div class="form-group">
								<label for="currency" class=" control-label">Paid By</label>
									<input type="text" class="form-control input-sm" placeholder="Paid By" name="paymentTransactionData.paidBy">
								</div>
							</div> 
							<!-- <div class="col-sm-2" > 
							<div class="form-group">
								<label for="currency" class=" control-label">Card Holder Name
								</label> 
									<input type="text" name="cardHolderName" id="card-holder-name" class="form-control input-sm">
									
								</div>
							</div>
							<div class="col-sm-2" > 
							<div class="form-group">
								<label for="currency" class=" control-label">Card Number
								</label> 
									<input type="text" name="cardNumber" id="card-number" class="form-control input-sm">
									
								</div>
							</div> -->
							<div class="col-md-4 col-sm-4"> 
							<div class="form-group has-feedback ">
								<label for="currency" class=" control-label">Comments
								</label>
									<textarea rows="1" class="form-control input-sm" placeholder="Type text" name="paymentTransactionData.responseMessage"></textarea>
								</div>
							</div>
							</div>
                       </div>
                         </div>
                     </div>
                     </div> 
                     </section>
							 	<div class="row">
								<div class="bottom-fixed-footer-btn">
									<div class="form-actions">
										<div class="pull-left">
										<button type="reset" class="btn btn-default" id="resetTourTripBtn">Reset</button>
										</div>
										<div class="pull-right">
										<button type="submit" class="btn btn-success" id="saveTourTripBtn" style="padding: 6px 28px;">Save</button>
										</div>
									</div>
								</div>
								</div>
                              </form>
                          </div>
                      </div>
            </section>
<script>
$(function () {
    $(function() {
    	  $('.date_of_birth').daterangepicker({
    	    singleDatePicker: true,
    	    showDropdowns: true,
    	    maxDate: new Date()
    	  }, function(start, end, label) {
    		  
    	  });
    	});
	
    $('.passport_date_issue').datetimepicker({	
    	format: 'MM/DD/YYYY',
    	widgetPositioning : {
            vertical: 'bottom'
         }
    	
    })
    .on('dp.hide', function() {
        $(this).blur();
    });
	
    $('.expiredate').datetimepicker({
    	format: 'MM/DD/YYYY',
    	minDate:new Date(),
    	disabledDates : [new Date()],
    	widgetPositioning : {
            vertical: 'bottom'
         }
    })
    .on('dp.hide', function() {
        $(this).blur();
    });
    
    $(function () {
        $('.datetimepicker1').datetimepicker({
        	format: 'MM/DD/YYYY hh:mm A'
        });
    });
 });

$("#filmore").click(function () {
        $(".more-option").toggle();
    });
$("#guest-more-option").click(function () {
        $(".show-guest-more-option").toggle();
    });
    
   /*-----------------------------------------------------------------------*/
    $(document).ready(function(){
		$('#supplier-price, #markup-amount, #gst-percentage , #processing-fee-persentage,#discount-fee').keyup(function() {
			var suuplierPriceInput= $("#supplier-price").val();
			var markUpInput = $("#markup-amount").val();
			var gstInput = $("#gst-percentage").val();
			var processingFeeInput = $("#processing-fee-persentage").val();
			var discountFeeInput = $("#discount-fee").val();
			
			var supplierPrice = parseFloat(suuplierPriceInput!=undefined && suuplierPriceInput!="" ?suuplierPriceInput:0);
			var markUp = parseFloat(markUpInput!=undefined && markUpInput!="" ?markUpInput:0);
			var gst = parseFloat(gstInput!=undefined && gstInput!="" ?gstInput:0);
			var processingFee = parseFloat(processingFeeInput!=undefined && processingFeeInput!="" ?processingFeeInput:0);
			var discountFeeInput = parseFloat(discountFeeInput!=undefined && discountFeeInput!="" ?discountFeeInput:0);
		
			
			var baseAmount = 0;
			var taxAmount = 0;
			var totalAmount = 0;
			var processingAmount = 0;
			var totalAmountAfterDiscount = 0;
			console.log(supplierPrice);
			var markUpAmount=(supplierPrice+markUp);
			markUpAmount = (markUpAmount-discountFeeInput)
			taxAmount = parseFloat((markUpAmount*gst)/100);
			baseAmount = parseFloat((markUpAmount*gst)/100)+markUpAmount;
			processingAmount = parseFloat((baseAmount*processingFee)/100);
			totalAmount = parseFloat((baseAmount*processingFee)/100)+baseAmount;
			//totalAmount = totalAmountAfterDiscount = parseFloat(totalAmount-discountFeeInput);
			
			// display all values
			 $("#gst-amount").val(taxAmount);
			 $("#base-fare").val(baseAmount);
			 $("#processing-fee").val(processingAmount);
			 $("#total-price").val(totalAmount);
	});
});
   /*-----------------------------------------------------------------------*/
   $(document).ready(function() {
    $("#saveTourTripForm").bootstrapValidator({
        feedbackIcons: {
            valid: "fa fa-check",
            invalid: "fa fa-remove",
            validating: "fa fa-refresh"
        },
        fields:{title:{message:"Title is not valid",validators:{notEmpty:{message:"Title is required field"},
        	stringLength:{min:1,message:"Title must be more than 3 and less than 100 characters long"}}},
        	destinationFrom:{message:"Destination from is not valid",validators:{notEmpty:{message:"Destination from is a required field"}}},
        	destinationTo:{message:"Destination to is not valid",validators:{notEmpty:{message:"Destination to is a required field"}}},
        	supplierPrice:{message:"Phone is not valid",validators:{notEmpty:{message:"Supplier Price is a required field"}}},
        	basePrice:{message:"Base Price is not valid",validators:{notEmpty:{message:"Base Price is a required field"}}}},
        
    }).on("error.form.bv", function(e) {}).on("success.form.bv", function(e) {
    		notifySuccess();
            e.preventDefault(), 
            $.ajax({
                url: "saveTourTrip",
                type: "POST",
                dataType: "json",
                data: $("#saveTourTripForm").serialize(),
                success: function(jsonData) {
                	if(jsonData.message.status == 'success')
                	{
	  					showModalPopUp(jsonData.message.message,"s");
	  					<%-- var url = '<%=request.getContextPath()%>/editTravelSalesLead';
						window.location.replace(url+'?id='+jsonData.message.id2); --%>	
                	}
	  				else if(jsonData.message.status == 'input')
	  				{
	  					showModalPopUp(jsonData.message.message,"w");
	  					
	  				}
	  				else if(jsonData.message.status == 'error')
	  				{
	  					showModalPopUp(jsonData.message.message,"e");
	  				}
                	
                    $("#saveTourTripBtn").removeAttr("disabled"), console.debug("button disabled ");
                    var s = $(e.target);
                    s.data("bootstrapValidator");
                    s.bootstrapValidator("disableSubmitButtons", !1).bootstrapValidator("resetForm", !0)
                },
                error: function(e, a, s) {
                    showModalPopUp("Tour Trip Bookibg can not be Saved.Please try again", "e")
                }
            })
    }).on("status.field.bv", function(e, a) {
        a.bv.getSubmitButton() && (console.debug("button disabled "), a.bv.disableSubmitButtons(!1))
    })
});
</script>
<script>
									
$(document).ready(function() {
	$(".addMoreBoxInner").click(function (e) {
			var InputsWrapper   = $(this).data("inputs-wrapper-id");
			var InputsWrapperInner   = $(this).data("inputs-wrapper-inner-id"); 
			var AddButtonBoxInner       = $(this).data("add-more-box-inner"); 
			var AddButtonBox       = $(this).data("add-more-box"); 
			var RemoveFileBox       = $(this).data("remove-box"); 
			var AddMoreFileBoxId      = $(this).data("add-box-id");
			var type      = $(this).data("type");
			var addBtnText=$(this).data("add-btn-txt");
			var FieldCount = $(this).data("field-count");
			var x = parseInt($(this).data("box-count"));
			var MaxInputs = parseInt($(this).data("max-count")); 
			var MaxInputFlag = Boolean($(this).data("max-flag"));
			
	        if(x <= MaxInputs || !MaxInputFlag) 
	        {
	            FieldCount++; //text box added ncrement
	            var body= "";
	            if(type=="inclusions")
	            {
	            	body = getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,0,1,5,'Title','text','title'+x,'tourOrderRowItineraryDescription.tourOrderRowItineraryInclusions['+x+'].title','off',0,0,'','')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,1,1,6,'Description','textarea','description'+x,'tourOrderRowItineraryDescription.tourOrderRowItineraryInclusions['+x+'].description','off',1,0,'','');
	            }
	            if(type=="exclusions")
	            {
	            	body = getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,0,1,5,'Title','text','title'+x,'tourOrderRowItineraryDescription.tourOrderRowItineraryExclusions['+x+'].title','off',0,0,'','')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,1,1,6,'Description','textarea','description'+x,'tourOrderRowItineraryDescription.tourOrderRowItineraryExclusions['+x+'].description','off',1,0,'','');
	            }
	            if(type=="CancellationPolicy")
	            {
	            	body = getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,0,2,3,'Days','text','days'+x,'tourOrderRowCancellationPolicies['+x+'].days','off',0,0,'','')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,1,2,3,'Amount','text','feeAmount'+x,'tourOrderRowCancellationPolicies['+x+'].feeAmount','off',0,0,'Y','%')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,2,2,5,'Remarks','textarea','remarks'+x,'tourOrderRowCancellationPolicies['+x+'].remarks','off',1,0,'','');
	            }
	            if(type=="ItemType")
	            {
	            	body = getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,0,2,3,'Type','text','type'+x,'tourOrderRowItineraryDescription.tourOrderRowItineraryItem['+x+'].type','off',0,0,'','')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,1,2,3,'Title','text','title'+x,'tourOrderRowItineraryDescription.tourOrderRowItineraryItem['+x+'].title','off',0,0,'','')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,2,2,5,'Remarks','textarea','description'+x,'tourOrderRowItineraryDescription.tourOrderRowItineraryItem['+x+'].description','off',1,0,'','');
	            }
	            
	            $("#"+InputsWrapper).append(body);
	            x++; 
	            $("."+AddButtonBox).show();
	            $("."+AddButtonBoxInner).html(addBtnText);
	            if(x == MaxInputs+1 && MaxInputFlag) {
	            	 $("."+AddButtonBox).hide();
	            }
	            $(this).data("field-count",FieldCount);
	            $(this).data("box-count",x);
	        }
	        return false;
	});
	
	
	$("body").on("click",".removeBox", function(e){
			var addBtnId= $(this).data("add-btn-id");	
			var x = parseInt($("#"+addBtnId).data("box-count"));
			var InputsWrapperInner = $("#"+addBtnId).data("inputs-wrapper-inner-id");
			var AddButtonBoxInner       = $("#"+addBtnId).data("add-more-box-inner"); //"addMoreFileBox"; //Add button ID
			var AddButtonBox       = $("#"+addBtnId).data("add-more-box");
			var addBtnText=$(this).data("add-btn-txt");
			
	        if( x > 1 ) {
	        		var removeXid = x-1;
	        		 $("#"+InputsWrapperInner+removeXid).remove();
	                 x--;
	                 $("#"+addBtnId).data("box-count",x);
	                 $("."+AddButtonBox).show();
	                $("."+AddButtonBoxInner).html(addBtnText);
	        }
		return false;
	}) 

	});
function getInputBlock(wrapper, wrapperInner, addMoreFileBoxId, wrapperId,colCountStart ,colCountEnd,colMdCount,label,type, id, name, autocomplete, areaRow,areaCol,formInputFlag, formInputSymbol)
{
	var divRow='<div class="row '+wrapper+'" id="'+wrapperInner+wrapperId+'" >';
	var divCol='<div class="col-md-'+colMdCount+'">';
	var divLabel='<label class="form-control-label" for="appendedInput">'+label+'</label>';
	var divFormControl='<div class="controls">';
	
	var divFromGroup='<div class="form-group">';
	
	var divFormInput='<div class="input-group">';
	var divFormInputSpan='<span class="input-group-addon" id="sizing-addon2"><b>'+formInputSymbol+'</b></span>';
	
	var formInput='<input type="text" class="form-control input-sm" id="'+id+'" name="'+name+'"  autocomplete="'+autocomplete+'">';
	var formTextArea='<textarea class="form-control input-sm" id="'+id+'" name="'+name+'" autocomplete="'+autocomplete+'" rows="'+areaRow+'"></textarea>';
	var divCloseTag='</div>';
	var aClose='<div class="mt-4"><a href="#" class="btn-sm  btn-danger removeBox" data-add-btn-id="'+addMoreFileBoxId+'">Remove</a></div>';
	
	var start='';
	if(colCountStart==0)
		start =divRow+divCol+divLabel+divFormControl;
	else
		start = divCol+divLabel+divFormControl;
	
	if(formInputFlag=="")
			start=start+divFromGroup;
	else
			start=start+divFormInput;
	
	var body='';
	if(type=="text")
		body=formInput;
	if(formInputSymbol!="")
		body=body+divFormInputSpan;
	if(type=="textarea")
		body=formTextArea;
	var end = divCloseTag+divCloseTag+divCloseTag;
	if(colCountStart==colCountEnd)
	{
		end =end+aClose+divCloseTag;
	}
	return start+body+end;
}
</script>	