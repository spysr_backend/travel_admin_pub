<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
    
    
    
    
    
    <!-- <link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/site-examples.css">-->
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/buttons.dataTables.min.css">
	
	 <link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/jquery.dataTables.min.css">
	
	
	<script type="text/javascript" async="" src="admin/print-pdf-excel/print-resource/ga.js.download"></script>
	<%-- <script type="text/javascript" src="admin/print-pdf-excel/print-resource/site.js.download">
	</script> --%>
	<%-- <script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/jquery-1.12.3.js.download">
	</script> --%>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/jquery.dataTables.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/dataTables.buttons.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.flash.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/jszip.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/pdfmake.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/vfs_fonts.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.html5.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.print.min.js.download">
	</script>
	<%-- <script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/demo.js.download">
	</script> --%>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.colVis.min.js">
	</script>
	<script type="text/javascript" class="init">
	$(document).ready(function() {
		var currentDate = "export-data_"+$.datepicker.formatDate('yyyy-mm-dd', new Date())+(new Date).getTime();
		$('#example').DataTable( {
			dom: 'Bfrtip',
			 
		buttons: [
		            {
		                extend: 'copyHtml5',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                }
		            },
		            {
		                extend: 'excelHtml5',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            {
		                extend: 'pdfHtml5',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            {
		                extend: 'print',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            {
		                extend: 'csv',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            'colvis'
		        ]
			        
		} );
		
	} );

		</script>   
<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}

</style>
        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->

            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.passport_details" /></h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <%--   <div class="set pull-right">
                                    <div class="dropdown">
                                      <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <s:text name="tgi.label.dropdown" />
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#"><s:text name="tgi.label.action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.another_action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.something_else_here" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.separated_link" /></a></li>
                                      </ul>
                                    </div>
                               </div> --%>
                            </div>
				<%-- 			<div class="cnt cnt-table">
							<div class="table-responsive">
                                 <form class="form-inline" action="searchCompanyHotelReportList"
							method="post">
                                    <table class="table table-form">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <tr>

                                                <td>
                                                    <div class="field-name"><s:text name="tgi.label.first_name" /></div>
                                                </td>
                                                <td>
                                                   <input type="text" class="form-control input-sm" id="twodpd2"
										placeholder="FirstName" name="emergencyfirstName" value='<s:property value="emergencyfirstName"/>'>
                                                </td>
												<td>
                                                    <div class="field-name"><s:text name="tgi.label.last_name" /></div>
                                                </td>
                                                <td>
                                                   <input type="text" class="form-control input-sm" id="twodpd2"
										placeholder="FirstName" name="emergencylastName" value='<s:property value="emergencylastName"/>'>
                                                </td>
                                            </tr>
                                            
                                            <tr>
											 <td>
                                                    <div class="field-name"><s:text name="tgi.label.email_address" /></div>
                                                </td>
                                               <td>
                                                   	<input type="text" class="form-control input-sm" id="twodpd2"
										placeholder="Email" name="emergencyemail" value='<s:property value="emergencyemail"/>'>
                                                </td>

												<th>&nbsp;</th>
												<th>&nbsp;</th>
												</tr>
                                           
                                         </tbody>
										
                                    </table>
									<div class="form-group table-btn">
                                        <div class="col-sm-10 col-sm-offset-2 ">
                                            
											<div class="pull-right">
                                           <button class="btn btn-primary " type="submit"><s:text name="tgi.label.show" /></button>
                                            </div>
                                        </div>
                                    </div></form><br><br><br>
 </div></div> --%>
                               
                          <div class="table-responsive dash-table no-table-css">

							<!-- testing -->
<div class="dash-table no-table-css">
		<div class="box clearfix">
		<div class="fw-container">
		<div class="fw-body">
			<div class="content">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display nowrap dataTable table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                                        <!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
                                        <thead>
										<tr class="border-radius border-color" role="row">
                                                    <th><s:text name="tgi.label.s.no" /></th>
                                                    <th><s:text name="tgi.label.first_name" /></th>
                                                    <th><s:text name="tgi.label.last_name" /></th>
                                                    <th><s:text name="tgi.label.email_address" /></th>
                                                    <th><s:text name="tgi.label.mobile" /></th>
                                                </tr>
                                                	<s:iterator value="reportData_list" status="serial" >
												<tr>
													<td data-title="S.No"><s:property value="%{#serial.count}" /></td>
													<td data-title="First Name"><s:property value="firstName" /></td>
													<td data-title="Last Name"><s:property value="lastName" /></td>													
													<td data-title="Email"><s:property value="email" /></td>
													<td data-title="Mobile"><s:property value="mobile" /></td>											
												</tr>
											</s:iterator>
                                            </thead>
                                            <tbody>
                                                <tr class="no-records-found"><td colspan="9"><s:text name="tgi.label.no_matching_records_found" /></td></tr>
                                               
                                                  
												
                                            </tbody>
                                        </table></div></div></div></div></div>
                                    </div>
								
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        <!--ADMIN AREA ENDS-->
    
    
    
    
    	<%-- <script type="text/javascript">
		$(document).ready(
				function() {
					var table = $('#example').DataTable({
						lengthChange : true,
						"pagingType" : "full_numbers",
						"lengthMenu" : [ 10, 25, 50, 75, 100, 500 ],
						buttons : [ 'excel', 'pdf', 'print' ]
					});

					table.buttons().container().appendTo(
							'#example_wrapper .col-sm-6:eq(0)');

				});

		<script type="text/javascript"> --%>
<%-- $(function() {
   $('#row_dim').hide(); 
    $('#user').change(function(){
    	//alert($('#user').val());
        if($('#user').val()== 'ALL') {
            $('#company_form-group').hide(); 
        } 
        else if($('#user').val() == '0') {
        	 $('#company_form-group').show(); 
          
       } 
        else {
            $('#company_form-group').hide(); 
        } 
    });
   
   
    $('#companyName').change(function(){
    	//alert($('#companyName').val());
        if($('#companyName').val() == 'ALL') {
            $('#user_form-group').hide(); 
        } else if($('#companyName').val() == '0') {
        	 $('#user_form-group').show(); 
           
        } 
        else{
        	 $('#user_form-group').hide(); 
        }
    });
   
   
   
   
});
 </script>
 --%>    