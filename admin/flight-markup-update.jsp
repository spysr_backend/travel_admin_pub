<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src="admin/js/jquery-ui.js" ></script>
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"flightMarkupEdit?markupId="+"${param.markupId}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
  <script type="text/javascript">
   
   $(function() {
		var id=$("#uniqueId").val();
		var accumulative=$("#accumulative").val();
		var fixedAmount=$("#fixedAmount").val();
		var positionOfMarkup=$("#positionOfMarkup").val();
		var airline=$("#airline").val();
		var country=$("#country").val();
		var classOfService=$("#classOfService").val();
		//alert(id+"\n"+country_id);
		 document.getElementById('accumulative'+id).value =accumulative;
		 document.getElementById('fixedAmount'+id).value =fixedAmount;
	/* 	 document.getElementById('positionOfMarkup'+id).value =positionOfMarkup; */
		 document.getElementById('airline'+id).value =airline;
		 document.getElementById('country'+id).value =country;
		 document.getElementById('classOfService'+id).value =classOfService;

		 
	}); 
 </script> 
 <script>
 $(document).ready(function() 
 { 
	 $("#datepicker_arr").datepicker({
		 format: "M-dd-yyyy"  
		/*  changeMonth: true,
		 changeYear: true */
	 }); 
	 });
	 
 </script> 
     <script>
 $(document).ready(function() 
 { 
	 $("#datepicker_dep").datepicker({
		 format: "M-dd-yyyy"
	 });
	 }); 
 </script>
  <script>
 $(document).ready(function() 
 { 
	 $("#datepicker_PromofareStart").datepicker({
		 format: "M-dd-yyyy"
	 });
	 }); 
 </script>
   <script>
 $(document).ready(function() 
 { 
	 $("#datepicker_PromofareEnd").datepicker({
		 format: "M-dd-yyyy"
	 });
	 }); 
 </script>
 

        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
     

            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                    <div class="card1">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.update_company_markup" /></h5>
                                <div class="set pull-left">
                                  <!--   <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                                <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="flightMarkupList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.flight_markup_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div>
                            
                           <div class="row">
					<div class="col-lg-12">
						<div class="card-block1">
                                    <form action="updateMarkupData" method="post" class="form-horizontal">
                                    <div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.markup_name" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control" id="username"
									name="name" placeholder="markup Name" autocomplete="off"
									  value="<s:property value="CurrentMarkupProfile.name"/>"
									 required></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.is_accumulative" />
											</label>
									<input type="hidden" value="<s:property value="CurrentMarkupProfile.markupId"/>" id="uniqueId">
									<input type="hidden" value="<s:property value="CurrentMarkupProfile.accumulative"/>" id="accumulative">
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control" name="accumulative" 
								 id="accumulative<s:property value="CurrentMarkupProfile.markupId"/>"
									  autocomplete="off" required>
									   <option value="true"><s:text name="tgi.label.yes" /></option>
									<option value="false" selected="selected"><s:text name="tgi.label.no" /></option>
									 
								</select></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.is_fixed_amount" />
											</label>
											<input type="hidden" value="<s:property value="CurrentMarkupProfile.fixedAmount"/>" id="fixedAmount">
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control" name="fixedAmount" 
										id="fixedAmount<s:property value="CurrentMarkupProfile.markupId"/>"
									  autocomplete="off" required>
 									<option value="true"><s:text name="tgi.label.yes" /></option>
									<option value="false"><s:text name="tgi.label.no" /></option>
									 
								</select></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control" id="last-name"
													name="markup" 
													value="<s:property value="CurrentMarkupProfile.markup"/>"
													placeholder="amount" autocomplete="off" required></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.position_of_markup" />
											</label>
										<input type="hidden" value="<s:property value="CurrentMarkupProfile.positionOfMarkup"/>" id="positionOfMarkup">
										<div class="controls"><div class="form-group"><div class="">
												<input type="number"   class="form-control" name="positionOfMarkup"
												value="<s:property value="CurrentMarkupProfile.positionOfMarkup"/>"  autocomplete="off" required></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.airline" />
											</label>
											<input type="hidden" value="<s:property value="CurrentMarkupProfile.airline"/>" id="airline">
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control"  name="airline" 
									id="airline<s:property value="CurrentMarkupProfile.markupId"/>"       autocomplete="off" required>
 										<option value="ALL" selected="selected"><s:text name="tgi.label.all" /></option>
									<s:iterator value="allAirlineList">
									<option value="<s:property value="airlinename"/>"><s:property
											value="airlinename"></s:property></option>
											 
								</s:iterator>
									 
								</select></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
								<div class="col-md-6">
									<label class="form-control-label" for="prependedInput"><s:text
											name="tgi.label.departure_date" /> </label>
									<div class="controls">
										<div class="form-group">
											<div class="">
												<input type="text" id="datepicker_dep"
													class="form-control date1" name="deptDateTr"
													autocomplete="off" placeholder="ALL"
													value="${spysr:formatDate(CurrentMarkupProfile.deptDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<label class="form-control-label" for="appendedInput"><s:text
											name="tgi.label.return_date" /> </label>
									<div class="controls">
										<div class="">
											<div class="form-group">
												<div class="">
													<input type="text" id="datepicker_arr"
														class="form-control date2" name="arrvDateTr"
														autocomplete="off" placeholder="ALL"
														value="${spysr:formatDate(CurrentMarkupProfile.arrvDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}">
												</div>
											</div>
										</div>
									</div>
								</div>
									
					</div>
							<div class=" row">
								<div class="col-md-6">
									<label class="form-control-label" for="prependedInput"><s:text
											name="tgi.label.promofare_startdate" /> </label>
									<div class="controls">
										<div class="form-group">
											<div class="">
												<input type="text" id="datepicker_PromofareStart"
													class="form-control date3"
													name="promofareStartDateTr"
													value="${spysr:formatDate(CurrentMarkupProfile.promofareStartDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"
													autocomplete="off" placeholder="ALL">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<label class="form-control-label" for="appendedInput"><s:text
											name="tgi.label.promofare_enddate" /> </label>
									<div class="controls">
										<div class="">
											<div class="form-group">
												<div class="">
													<input type="text" id="datepicker_PromofareEnd"
														class="form-control date4"
														name="promofareEndDateTr"
														value="${spysr:formatDate(CurrentMarkupProfile.promofareEndDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"
														autocomplete="off" placeholder="ALL">
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class=" row">
								<div class="col-md-6">
									<label class="form-control-label" for="appendedInput"><s:text
											name="tgi.label.origin" /> </label>
									<div class="controls">
										<div class="">
											<div class="form-group">
												<div class="">
													<input type="text" id="origin"
														class="form-control airportList" name="origin"
														value="<s:property value="CurrentMarkupProfile.origin"/>"
														autocomplete="off" placeholder="ALL"
														auto-complete-directive ui-items="names">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<label class="form-control-label" for="appendedInput"><s:text
											name="tgi.label.destination" /> </label>
									<div class="controls">
											<div class="form-group">
												<div class="">
													<input type="text" id="dest"
														class="form-control airportList"
														name="destination" autocomplete="off"
														value="<s:property value="CurrentMarkupProfile.destination"/>"
														placeholder="ALL" auto-complete-directive ui-items="names">
												</div>
											</div>
									</div>
								</div>
							</div>
							<div class=" row">
								<div class="col-md-6">
									<label class="form-control-label" for="appendedInput"><s:text
											name="tgi.label.country" /> </label> <input type="hidden"
										value="<s:property value="CurrentMarkupProfile.country"/>"
										id="country">
									<div class="controls">
										<div class="form-group">
											<div class="">
												<select class="form-control" name="country"
													id="country<s:property value="CurrentMarkupProfile.markupId"/>"
													autocomplete="off" required>
													<option selected value="ALL"><s:text
															name="tgi.label.all" /></option>
													<s:iterator value="countryList">
														<option value="<s:property value="c_name"/>"><s:property
																value="c_name"></s:property></option>
													</s:iterator>

												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<label class="form-control-label" for="appendedInput"><s:text
											name="tgi.label.class_of_service" /> </label> <input type="hidden"
										value="<s:property value="CurrentMarkupProfile.classOfService"/>"
										id="classOfService">
									<div class="controls">
										<div class="form-group">
											<select class="form-control" name="classOfService"
												id="classOfService<s:property value="CurrentMarkupProfile.markupId"/>"
												autocomplete="off" required>
												<option selected="selected" value="ALL"><s:text
														name="tgi.label.all" /></option>
												<option value="Economy"><s:text
														name="tgi.label.economy" /></option>
												<option value="Business"><s:text
														name="tgi.label.business" /></option>

											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group table-btn"></div>
                                        <div class="col-sm-10 col-sm-offset-2 "></div>
                                           <!--  <div class="pull-left"><button class="btn btn-danger" type="submit">Clear Form</button></div> -->
                                            <div class="pull-right">
                                               <div class="form-group"><div class="col-md-12"> <input type="hidden" name="markupId" value="${CurrentMarkupProfile.markupId}">
                                                <button class="btn btn-default" type="button"><s:text name="tgi.label.cancel" /></button>
                                                <button class="btn btn-primary " type="submit">
                                                    <s:text name="tgi.label.update" /></button>
                                            </div>
                                        </div>
                                    </div></form>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div></div>
<script>
                	  $(".airportList").autocomplete({
                          minChars: 3,
                          source: "city_autocomplete"
          										});
						</script>                
            </section>
        <!--ADMIN AREA ENDS-->
         	  
  	   <s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>