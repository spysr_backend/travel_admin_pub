<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"editFlightAirlineDealsSchedule?id="+"${flightAirlineDealsSchedule.id}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                         <div class="hd clearfix">
                                <h5 class="pull-left">Add Flight Deals From 
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="metatagpage_list"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.meta_dat_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div>
                             <div class="hd clearfix">
                            </div>
                              <div class="cnt cnt-table">
					<div class="table-responsive no-table-css">
						<form action="updateFlightAirlineDealsSchedule.action" method="post">
							<table class="table table-form">
								<thead>
									<tr>
										<th></th>
										<th>&nbsp;</th>


									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<div class="field-name">
												Deal Page Number
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="dealPageNumber" value="${flightAirlineDealsSchedule.dealPageNumber}" placeholder="Deal Page Number" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Flight Number
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="flightNumber" value="${flightAirlineDealsSchedule.flightNumber}" placeholder="Flight Number" autocomplete="off"
											required></td>
									</tr>
																		<tr>
										<td>
											<div class="field-name">
												Airline Code
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="airlineCode" value="${flightAirlineDealsSchedule.airlineCode}" placeholder="Airline Code" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Frequency
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="frequency" value="${flightAirlineDealsSchedule.frequency}" placeholder="Frequency" autocomplete="off"
											required></td>
									</tr>
																		<tr>
										<td>
											<div class="field-name">
												Depart Date
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="departDate" id="departDate" value="${flightAirlineDealsSchedule.departDate}" placeholder="Depart Date" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Return Date
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="returnDate" id="returnDate" value="${flightAirlineDealsSchedule.returnDate}" placeholder="Return Date" autocomplete="off"
											required></td>
									</tr>
																		<tr>
										<td>
											<div class="field-name">
												Duration
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="duration" value="${flightAirlineDealsSchedule.duration}" placeholder="Duration" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Equipment
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="equipment" value="${flightAirlineDealsSchedule.equipment}" placeholder="Equipment" autocomplete="off"
											required></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td>
										<input type="hidden" name="id" value="${flightAirlineDealsSchedule.id}" />
										<input type="hidden" name="version" value="${flightAirlineDealsSchedule.version}" />
										<input type="submit" class="btn btn-info"></td>
										<td></td>
									</tr>
								</tbody>
							</table>

						</form>
					</div>
				</div>
</div></div></div>
                            </section>        
                                    			<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	 <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_body').text("${message}");
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
						

						<script>
 $(document).ready(function() 
 { 
	 $("#departDate").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 $("#returnDate").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 }); 
 </script>
						