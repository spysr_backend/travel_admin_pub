<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<style>
#example_filter input {
	height: 25px;
}

#example_wrapper .dt-buttons {
	padding-top: 10px;
}

.show_date_to_user {
	padding: 0px 15px;
	position: relative;
	width: 93%;
	top: -25px;
	left: 5px;
	pointer-events: none;
	display: inherit;
	color: #000;
}
span.text-spysr {
    font-size: 15px;
}
</style>

<title><s:property value="user" /></title>

<section class="wrapper container-fluid">
	<div class="">
		<div class="">
			<div class="pnl">
				<div class="card1">
					<div class="hd clearfix">
						<h5 class="pull-left">
							<s:text name="tgi.label.tour_agent_invoice_list" />
						</h5>
						<div class="set pull-right">
							<div class="dropdown">
								<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" style="padding: 3px 6px;"> <i class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
										<li class=""><a class="" href="tourOrderList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.order_list" /> </a></li>
										<li class=""><a class="" href="tourCommissionReport"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_report" /></a></li>
										<li class=""><a class="" href="tourAgentCommInvoiceList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_invoice" /></a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="set pull-right">
							<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse" style="cursor: pointer;" data-toggle="collapse" href="filterDiv" aria-expanded="false">
								<span class="text-spysr"> <img class="clippy" src="admin/img/svg/filter.svg" width="11" alt="Copy to clipboard"> Filter <i class="fa fa-angle-down"></i></span>
							</a>
						</div>
					</div>
					<br>
					<form action="tourAgentCommInvoiceList" class="filter-form">
						<div class="" id="filterDiv" style="display: none;">
							<div class="">
								<s:if test="%{#session.Company!=null}">
									<s:if test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
										<div class="col-md-2">
											<div class="form-group">
												<input type="hidden" id="HdncompanyTypeShowData" value="" />
												<select name="companyTypeShowData" id="companyTypeShowData"
													class="form-control input-sm">
													<option value="all" selected="selected"><s:text
															name="tgi.label.all" /></option>
													<option value="my"><s:text
															name="tgi.label.my_self" /></option>
													<s:if test="%{#session.Company!=null}">
														<s:if test="%{#session.Company.companyRole.isSuperUser()}">
															<option value="distributor"><s:text
																	name="tgi.label.distributor" /></option>
														</s:if>
													</s:if>
													<option value="agency"><s:text
															name="tgi.label.agency" /></option>
												</select>
											</div>
										</div>
									</s:if>
								</s:if>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<input type="text" name="orderId" id="orderid-json"
											placeholder="Search By Order Id...."
											class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<input type="text" name="bookingStartDate"
											id="bookingStartDate" placeholder="Start Booking Date...."
											class="form-control search-query date1 input-sm " />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<input type="text" name="travelStartDate" id="travelStartDate"
											placeholder="Tour Start Date...."
											class="form-control search-query date3 input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<input type="text" name="invoiceNo" id="orderid-json"
											placeholder="Search By Invoice No...."
											class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<input type="text" name="pnr" id="hotelName-json"
											placeholder="Tour PNR"
											class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<input type="text" name="tourTitle" id="hotelName-json"
											placeholder="Tour Title"
											class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<input type="text" name="supplier" id="hotelName-json"
											placeholder="Tour Supplier"
											class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<input type="text" name="createdBy" id="agency-json"
											placeholder="Agency..."
											class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<select name="paymentMode" id="paymentMode"
											class="form-control input-sm">
											<option value="" selected="selected"><s:text
													name="tgi.label.select_payment_method" /></option>
											<option value="ALL"><s:text name="tgi.label.all" /></option>
											<option value="CHECK"><s:text name="tgi.label.CHECK" /></option>
											<option value="CREDIT_CARD"><s:text
													name="tgi.label.CREDIT_CARD" /></option>
											<option value="DEBIT_CARD"><s:text
													name="tgi.label.DEBIT_CARD" /></option>
											<option value="WALLET"><s:text
													name="tgi.label.WALLET" /></option>
										</select>
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<select name="paymentModeType" id="paymentModeType"
											class="form-control input-sm">
											<option value="" selected="selected"><s:text
													name="tgi.label.select_payment_mode" /></option>
											<option value="ALL"><s:text name="tgi.label.all" /></option>
											<option value="SQUAREUP"><s:text
													name="tgi.label.squareUp" /></option>
											<option value="EXTERNALLINK_TEST"><s:text
													name="tgi.label.external" /></option>
											<option value="AUTHORIZE"><s:text
													name="tgi.label.authorize" /></option>
											<option value="WALLET"><s:text
													name="tgi.label.WALLET" /></option>
											<option value="WIRECARD"><s:text
													name="tgi.label.wirecard" /></option>
											<option value="STRIPE"><s:text
													name="tgi.label.stripe" /></option>
										</select>
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<select name="paymentStatus" id="paymentStatus"
											class="form-control input-sm">
											<option value="" selected="selected"><s:text
													name="tgi.label.select_payment_status" /></option>
											<option value="ALL"><s:text name="tgi.label.all" /></option>
											<option value="PENDING"><s:text
													name="tgi.label.PENDING" /></option>
											<option value="SUCCESS"><s:text
													name="tgi.label.SUCCESS" /></option>
											<option value="FAILED"><s:text
													name="tgi.label.FAILED" /></option>
											<option value="PROCESSING"><s:text
													name="tgi.label.PROCESSING" /></option>
											<option value="DECLINED"><s:text
													name="tgi.label.DECLINED" /></option>
											<option value="REFUND"><s:text
													name="tgi.label.REFUND" /></option>
										</select>
									</div>
								</div>

								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<select name="bookingStatus" id="bookingStatus"
											class="form-control input-sm">
											<option value="" selected="selected"><s:text
													name="tgi.label.select_booking_status" /></option>
											<option value="ALL"><s:text name="tgi.label.all" /></option>
											<option value="PENDING"><s:text
													name="tgi.label.PENDING" /></option>
											<option value="CONFIRM"><s:text
													name="tgi.label.CONFIRM" /></option>
											<option value="FAILED"><s:text
													name="tgi.label.FAILED" /></option>
											<option value="PROCESSING"><s:text
													name="tgi.label.PROCESSING" /></option>
											<option value="CANCELLED"><s:text
													name="tgi.label.CANCELLED" /></option>
										</select>
									</div>
								</div>
								<div class="col-md-1">
								<div class="form-group">
								<input type="reset" class="btn btn-sm btn-danger btn-block" id="configreset" value="Clear">
							</div>
							</div>
								<div class="col-md-1">
								<div class="form-group">
								<input type="submit" class="btn btn-sm btn-info btn-block" value="Search" />
							</div>
							</div>
							</div>
						</div>
					</form>
		<!-- Tour My Invoice List  -->
					<div class="dash-table">
						<div class="content">
							<div id="example_wrapper" class="dataTables_wrapper">
								<table id="example" class="display dataTable responsive nowrap" role="grid" aria-describedby="example_info" style="width: 100%;">
									<thead>
										<tr class="border-radius border-color" role="row">
											<th data-priority="1" style="width: 20px"></th>
											<th data-priority="3" class="col-md-1"><s:text name="tgi.label.booking_date" /></th>
											<th data-priority="2"><s:text name="tgi.label.order_id" /></th>
											<th data-priority="4"><s:text name="tgi.label.invoice_no" /></th>
											<th data-priority="5">Customer</th>
											<th data-priority="5" style="width: 54px;">Guest</th>
											<th data-priority="9" style="width: 125px;">Base Amount</th>
											<th data-priority="9" style="width: 125px;">Markup Amount</th>
											<th data-priority="9" style="width: 125px;"><s:text name="tgi.label.total" /> Amount</th>
											<th data-priority="8" style="width: 120px;">Booking Status</th>
											<th data-priority="7" style="width: 120px;"><s:text name="tgi.label.payment" /> Status</th>
											<th data-priority="10"><s:text name="tgi.label.supplier" /></th>
											<th data-priority="12" style="width: 136px;"><img class="clippy" src="admin/img/svg/invoice.svg" width="16" alt="Settings" style="margin-left: 6px;"></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<s:if test="customerInvoiceList.size>0">
											<s:iterator value="customerInvoiceList" status="rowCount">
												<tr>
													<td><div class="center"><s:property value="%{#rowCount.count}" /></div></td>
													<td>${spysr:formatDate(bookingDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM/dd/yyyy hh:mm a')}</td>
													<td><a href="generateTourAgentCommissionInvoice?agentInvoice=true&orderId=${orderId}"><s:property value="orderId" /></a></td>
													<td><s:property value="invoiceNo" /></td>
													<td >${orderCustomer.firstName} ${orderCustomer.lastName}</td>
													<td ><span class="oxy_number_bg_style">${totalGuest}</span></td>
													<td><fmt:formatNumber type="number" pattern="#.00" value="${apiPrice}" /></td>
													<td><fmt:formatNumber type="number" pattern="#.00" value="${markupAmount}" /></td>
													<td><s:property value="finalPrice" /></td>
													<td>
     												<c:choose>
     												<c:when test="${bookingStatus == 'CONFIRM'}">
 														<div class="oxy_status_bg_success">${bookingStatus}</div>										
     												</c:when>
     												<c:when test="${bookingStatus == 'PENDING'}">
     													<div class="oxy_status_bg_warning">${bookingStatus}</div>	
     												</c:when>
     												<c:when test="${bookingStatus == 'FAILED'}">
     													<div class="oxy_status_bg_danger">${bookingStatus}</div>	
     												</c:when>
     												<c:when test="${bookingStatus == 'CANCELLED'}">
     													<div class="oxy_status_bg_danger">${bookingStatus}</div>	
     												</c:when>
     												<c:when test="${bookingStatus == 'PROCESSING'}">
     													<div class="oxy_status_bg_active">${bookingStatus}</div>	
     												</c:when>
     												</c:choose>
     												</td>
     												<td data-title="Payment Status">
     												<c:choose>
     												<c:when test="${paymentStatus == 'SUCCESS'}">
 														<div class="oxy_status_bg_success">${paymentStatus}</div>										
     												</c:when>
     												<c:when test="${paymentStatus == 'PENDING'}">
     													<div class="oxy_status_bg_warning">${paymentStatus}</div>	
     												</c:when>
     												<c:when test="${paymentStatus == 'FAILED'}">
     													<div class="oxy_status_bg_danger">${paymentStatus}</div>	
     												</c:when>
     												<c:when test="${paymentStatus == 'REFUND'}">
     													<div class="oxy_status_bg_active">${paymentStatus}</div>	
     												</c:when>
     												</c:choose>
     												
     												</td>
													<td >${apiProvider != null && apiProvider != '' ? apiProvider:''}</td>
													<td data-title="<s:text name="tgi.label.action" />">
														<p data-placement="top" title="Generate Invoice">
															<a href="generateTourAgentCommissionInvoice?agentInvoice=false&orderId=${id}"
																class="btn btn-success btn-xs" data-toggle="modal"> My Invoice
															</a>
															<a href="generateTourAgentCommissionInvoice?agentInvoice=true&orderId=${orderId}"
																class="btn btn-success btn-xs" data-toggle="modal"> Agent Invoice
															</a>
														</p>
													</td>
													<td></td>
												</tr>
											</s:iterator>
										</s:if>
									</tbody>
									<%-- <tbody class="hidden-xs">
										<tr class="border-radius border-color">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td><b><s:text name="tgi.label.total_spent" /></b></td>
											<td><b><s:property value="agentTotalCommObj.totAmountSpent" /></b></td>
										</tr>
									</tbody> --%>
								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$(function() {
		$('#period').change(function() {
			if ($('#period').val() == '0') {
				$('#fromDateDiv').show();
				$('#endDateDiv').show();
			} else {
				$('#fromDateDiv').hide();
				$('#endDateDiv').hide();
			}
		});

		$('#companyName').change(function() {
			if ($('#companyName').val() == 'ALL') {
				$('#user_form-group').show();
			} else {
				$('#user_form-group').hide();
			}
		});

	});
</script>
<!--AutoCompleteter  -->
<script>
	//order id
	var OrderId = {
		url : "getTourReportsJson.action?data=orderId",
		getValue : "orderId",

		list : {
			match : {
				enabled : true
			}
		}
	};

	$("#orderid-json").easyAutocomplete(OrderId);

	//airine
	var PaymentStatus = {
		url : "getTourReportsJson.action?data=paymentStatus",
		getValue : "status",

		list : {
			match : {
				enabled : true
			}
		}
	};

	$("#pymtstatus-json").easyAutocomplete(PaymentStatus);

	var Agency = {
		url : "getTourReportsJson.action?data=agencyName",
		getValue : "agencyName",

		list : {
			match : {
				enabled : true
			}
		}
	};
	$("#agency-json").easyAutocomplete(Agency);
</script>
</script>
	<script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>	