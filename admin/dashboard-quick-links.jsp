<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<!-- Placecomplete plugin -->

<link rel="stylesheet" href="admin/css/switchery.min.css">
<link rel="stylesheet" href="admin/css/fakeLoader.css">
<!-- Custom stylesheet in less-->
<link rel="stylesheet" href="admin/css/flexslider.css">
<link rel="stylesheet" href="admin/css/animate.css">
<link rel="stylesheet" href="admin/css/owl.carousel.css">
<link rel="stylesheet" href="admin/css/bootstrap-datepicker3.css">
<link rel="stylesheet" href="admin/css/clockpicker.css">
<link rel="stylesheet" href="admin/css/bootstrap-select.min.css">
<link rel="stylesheet" href="admin/css/jquery.bxslider.css">

<!-- <link rel="stylesheet" href="admin/css/style-search.css">    -->
<link rel="stylesheet" href="admin/css/jquery.auto-complete.css">
<link href="admin/css/chart.css" rel="stylesheet" type="text/css" />
<script src="admin/js/jquery.min.js"></script>
<script src="admin/js/owl.carousel.min.js"></script>
<script src="admin/js/modernizr.js"></script>
<script>
   
 function autoRefresh_div()
 {	 
    //a function which will load data from other file after x seconds
      var protocol=location.protocol;
  	   var host=location.host;
  	  /* var url=protocol+"//"+host+"/TGIAdmin/getHotelNamesJson"; */
  	  //var url = protocol+"//"+host+"/getDashboardDataJson";
  		var url ="getDashboardDataJson";
   				comfirmList= [];
 				$.ajax({
					    method: "GET",
					    url:url,
					   /*  data: {type:"today"}, */
					    success:function(data,status)
						{ 
					    	//console.log("id.."+data.jsonobj.FlightOrdercount);
					     $("#flightOrders").text(data.jsonobj.FlightOrdercount);
					     $("#flightpaymentcount").text(data.jsonobj.Flightpaymentordercount);
					     $("#flightconfirmCount").text(data.jsonobj.Flightconfirmordercount);
					    	 
					     $("#hotelOrders").text(data.jsonobj.HotelOrdercount);
					     $("#hotelconfirmCount").text(data.jsonobj.Hotelconfirmordercount);
					     $("#hotelpaymentcount").text(data.jsonobj.Hotelpaymentordercount);
					     
					     $("#carOrders").text(data.jsonobj.CarOrdercount);
					     $("#carconfirmCount").text(data.jsonobj.Carconfirmordercount);
					     $("#carpaymentcount").text(data.jsonobj.Carpaymentordercount);
					     
					     $("#tourOrders").text(data.jsonobj.TourOrdercount);
					     $("#tourconfirmCount").text(data.jsonobj.Tourconfirmordercount);
					     $("#tourpaymentcount").text(data.jsonobj.Tourpaymentordercount);
					     
					     $("#limoOrders").text(data.jsonobj.LimoOrdercount);
					     $("#limoconfirmCount").text(data.jsonobj.Limoconfirmordercount);
					     $("#limopaymentcount").text(data.jsonobj.Limopaymentordercount);
					     
					     $("#distributorlist").text(data.jsonobj.totaldistributorlist);		
					     $("#agentlist").text(data.jsonobj.totalagentlist);	
					     },
						error: function(xhr,status,error)
						{
							console.log(error);
						}
					});  
  }
  $(function() {
	 //autoRefresh_div();
	//getgraphdata();
	/*  setInterval('autoRefresh_div()',60000); // refresh div after 60 secs */
 }); 
  
     </script>

<style>
.nav-horizontal {
	padding: 5px 5px 10px;
	margin: 0;
	list-style: none;
	background: white;
	margin-bottom: 2rem;
	border-radius: 0px;
}

.nav-horizontal li {
	display: inline-block;
	margin: 0 3px 9px;
}

.nav-horizontal li {
	display: inline-block;
	margin: 0 3px 9px;
}

.nav-horizontal a:hover, .nav-horizontal li.active a {
	background-color: #353a40;
	text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.5);
	text-decoration: none;
	color: #fff;
	border-radius: 0px;
	transition: 0.40s;
}

.headtop .nav>li>a:hover, .nav>li>a:focus {
	background: #ffde01 !important;
	color: #353a40 !important;
}

.nav-horizontal a {
	display: block;
	min-width: 130px;
	min-height: 110px;
	border-radius: 0px;
	font-weight: 100;
	text-align: center;
	color: #353a40;
	padding: 8px 10px;
	background-color: #ffde01;
	transition: 0.40s;
}

@media screen and (max-width: 730px) {
	.nav-horizontal a {
		text-align: center;
		min-width: 130px;
		min-height: 10px;
		border-radius: 0px !important;
		padding: 12px 15px;
	}
}

@media screen and (max-width: 992px) {
	.nav-horizontal a {
		text-align: center;
		min-width: 110px;
		min-height: 10px;
		border-radius: 0px !important;
		padding: 12px 15px;
	}
}

.nav-horizontal a:hover i, .nav-horizontal li.active a i {
	color: #fff;
	opacity: 1;
}

@media screen and (min-width: 992px) {
	.nav-horizontal i {
		display: block;
		height: 55px;
		margin-right: 0;
		margin-bottom: 10px;
		color: #353a40;
		font-size: 35px;
		padding: 10px;
	}
}

.card.card-inverse {
	margin-bottom: 15px;
}

h3.quick-text {
	/* padding: 15px 0px 15px 0px; */
	color: #363a40;
	font-weight: 100;
}

.clippy {
	margin-top: -1px;
	position: relative;
	top: 1px;
}

img {
	border: 0;
}

.input-group {
	display: table;
}

.input-group-button {
	width: 1%;
	vertical-align: middle;
}

.input-group input, .input-group-button {
	display: table-cell;
}

.input-group-button:last-child .btn {
	margin-left: -1px;
}
</style>
<section class="content-header pt-4" style="text-align: center;">
	<div class="container" style="margin-top: 35px;">
		<div class="content-header company">
			<ul class="nav-horizontal text-center">
				<h3 class="quick-text">Lead Quick Links</h3>
				<s:if test="%{((#session.User.userRoleId.isSuperUser()||#session.Company.companyRole.isDistributor() || #session.Company.companyRole.isAgent()) && #session.User.userRoleId.isAdmin())}">
				<s:if test="%{((#session.User.userRoleId.isSuperUser()||#session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
					<li class="active"><a href="<s:url action="listCompanyLead"/>"><i class="fa fa-building-o"></i>Company Lead</a></li>
				</s:if>
					<li><a href="<s:url action="listTravelSalesLead"/>"><i class="fa fa-leaf"></i> Travel Lead</a></li>
					<%-- <li><a href="<s:url action="listTravelSalesLeadBuyNow"/>"><i class="fa fa-leaf"></i> Buy Travel Lead</a></li> --%>
					<li><a href="<s:url action="listTravelSalesMyLead"/>"><i class="fa fa-leaf"></i> My Lead</a></li>
				</s:if>
			</ul>
		</div>
		<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserMode()) && #session.User.userRoleId.isSuperUser())}">
		<div class="content-header company">
			<ul class="nav-horizontal text-center">
				<h3 class="quick-text">Company Quick Links</h3>
				<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserMode()) && #session.User.userRoleId.isSuperUser())}">
					<li class="active"><a
						href="<s:url action="walletTransactions"/>"><i
							class="fa fa-money"></i> Transactions List</a></li>
					<li><a href="<s:url action="goMyWallet"/>"><i
							class="fa fa-google-wallet"></i> Add My Wallet</a></li>
				</s:if>
				<s:if test="%{ #session.User.userRoleId.isAdmin() || #session.Company.companyRole.isDistributor()}">
					<li><a href="<s:url action="showWalletUsers"/>"><i class="fa fa-google-wallet"></i> Add User Wallet</a></li>
				</s:if>
				<s:if
					test="%{((#session.User.userRoleId.isSuperUser()||#session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
					<li><a href="<s:url action="addCompany"/>"><i
							class="fa fa-building-o"></i> Add Company</a></li>
					<li><a href="<s:url action="companyList"/>"><i
							class="fa fa-list-alt"></i> Company List</a></li>
					<li><a href="<s:url action="addNewCompanyConfig"/>"><i
							class="fa fa-plus-square-o"></i> Add Config</a></li>

					<li><a href="<s:url action="userRegister"/>"><i
							class="fa fa-user-plus"></i> Add Employee</a></li>
					<li><a href="<s:url action="userList"/>"><i
							class="fa fa-list-alt"></i>Employee List</a></li>
				</s:if>
			</ul>
		</div>
		</s:if>	

		<s:if
			test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserMode()) && #session.Company.companyCmsService.isFlight())}">
			<div class="content-header flight">
				<ul class="nav-horizontal text-center">
					<h3 class="quick-text">Flight Quick Links</h3>
					<li class="active"><a
						href="<s:url action="flightMarkupList"/>"><i
							class="fa fa-list-alt"></i>Markup List</a></li>
					<li><a href="<s:url action="flightReportList"/>"><i
							class="fa fa-list-alt"></i>Report List</a></li>
					<li><a href="<s:url action="flightOrderList"/>"><i
							class="fa fa-list-alt"></i>Order List</a></li>
					<li><a href="<s:url action="flightCommissionReportList"/>"><i
							class="fa fa-files-o"></i>Commision Report</a></li>
					<li><a href="<s:url action="flightCustomerInvoiceList"/>"><i
							class="fa fa-files-o"></i>Customer Invoice </a></li>
					<li><a href="<s:url action="flightAgentInvoiceList"/>"><i
							class="fa fa-files-o" aria-hidden="true"></i>Agent Invoice </a></li>
				</ul>
			</div>
		</s:if>
		<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserMode()) && #session.Company.companyCmsService.isHotel())}">
			<div class="content-header hotel">
				<ul class="nav-horizontal text-center">
					<h3 class="quick-text">Hotel Quick Links</h3>
					<li class="active"><a href="<s:url action="hotelMarkupList"/>"><i
							class="fa fa-list-alt"></i>Markup List</a></li>
					<li><a href="<s:url action="hotelReportList"/>"><i
							class="fa fa-list-alt"></i>Report List</a></li>
					<li><a href="<s:url action="hotelOrdersList"/>"><i
							class="fa fa-list-alt"></i>Order List</a></li>
					<li><a href="<s:url action="hotelCommissionReport"/>"><i
							class="fa fa-file-text-o"></i>Commision Report</a></li>
					<li><a href="<s:url action="hotelCustomerInvoiceList"/>"><i
							class="fa fa-file-text-o"></i>Customer Invoice </a></li>
					<li><a href="<s:url action="hotelAgentCommInvoiceList"/>"><i
							class="fa fa-file-text-o"></i>Agent Invoice </a></li>
				</ul>
			</div>
		</s:if>
		<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserMode()) && #session.Company.companyCmsService.isCar())}">
			<div class="content-header car">
				<ul class="nav-horizontal text-center">
					<h3 class="quick-text">Car Quick Links</h3>
					<li class="active"><a href="<s:url action="carMarkupList"/>"><i
							class="fa fa-list-alt"></i>Markup List</a></li>
					<li><a href="<s:url action="carReportList"/>"><i
							class="fa fa-list-alt"></i>Report List</a></li>
					<li><a href="<s:url action="carOrderList"/>"><i
							class="fa fa-list-alt"></i>Order List</a></li>
					<li><a href="<s:url action="carCommissionReport"/>"><i
							class="fa fa-files-o"></i>Commision Report</a></li>
					<li><a href="<s:url action="carCustomerInvoiceList"/>"><i
							class="fa fa-files-o"></i>Customer Invoice </a></li>
					<li><a href="<s:url action="carAgentCommInvoiceList"/>"><i
							class="fa fa-files-o"></i>Agent Invoice </a></li>
				</ul>
			</div>
		</s:if>
		<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserMode()) && #session.Company.companyCmsService.isTour())}">
			<div class="content-header tour">
				<ul class="nav-horizontal text-center">
					<h3 class="quick-text">Tour Quick Links</h3>
					<li class="active"><a href="<s:url action="tourMarkupList"/>"><i
							class="fa fa-list-alt"></i>Markup List</a></li>
					<li><a href="<s:url action="tourOrderList"/>"><i
							class="fa fa-list-alt"></i>Order List</a></li>
					<li><a href="<s:url action="tourCommissionReport"/>"><i
							class="fa fa-file-text-o"></i>Commision Report</a></li>
					<li><a href="<s:url action="tourAgentCommInvoiceList"/>"><i
							class="fa fa-file-text-o"></i>Agent Invoice </a></li>
				</ul>
			</div>
		</s:if>
		<s:if test="%{#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isTechSupport() || #session.User.userRoleId.isTechHead() || #session.User.userRoleId.isTravelDesk()}">
			<div class="content-header tour">
			<ul class="nav-horizontal text-center">
			<h3 class="quick-text">Task Management Quick Links </h3>
			<li class="active">
			<a href="<s:url action="addBugTracker"/>"><i class="fa fa-list-alt"></i>New Task/Bug</a>
			</li>
			<li>
			<a href="<s:url action="goBugTrackerList"/>"><i class="fa fa-list-alt"></i>Task/Bug List</a>
			</li>
			<li>
			<a href="<s:url action="getAssignBugList"/>"><i class="fa fa-list-alt"></i>Assign Task/Bug</a>
			</li>
			</ul>
			</div>
		 </s:if>

		<!-- Content Header (Page header) -->


		<!-- Main content Analytic Part -->
		<section class="content" style="display:none;" >
			<!-- Small boxes (Stat box) -->
			<div class="row">

				<div class="col-sm-12 col-md-12">

					<!--  flightorder row -->
					<s:if
						test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport()
                   					||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUserMode()) && #session.Company.companyCmsService.isFlight())}">
						<div class="row">
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-primary">
									<div class="card-block p-b-0">
										<!-- <div class="small-week">
               <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer"  > Today <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                          <a href="showFlightWeekOrderList?dateType=WEEK" class="small-box-footer"  >Week<i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm active">
                                <a href="showFlightWeekOrderList?dateType=MONTH" class="small-box-footer"  >Month <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div> -->

										<div class="inner">
											<h3 id="flightOrders">
												0<sup style="font-size: 20px"></sup>
											</h3>
											<p>
												<s:text name="tgi.label.flight_new_orders" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-bag"></i>
										</div>

										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showFlightWeekOrderList?dateType=TODAY"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a href="showFlightWeekOrderList?dateType=WEEK"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showFlightWeekOrderList?dateType=MONTH"
												class="small-box-footer"><s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>
									</div>

									<!--  <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
								</div>
							</div>
							<!-- ./col -->
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-primary">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="flightconfirmCount">
												0<sup style="font-size: 20px"></sup>
											</h3>
											<p>
												<s:text name="tgi.label.confirmed_order_counts" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-stats-bars"></i>
										</div>
										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showFlightWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showFlightWeekOrderList?dateType=WEEK&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showFlightWeekOrderList?dateType=MONTH&bookingStatus=CONFIRM"
												class="small-box-footer"><s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>

										<%--  <a href="showFlightWeekOrderList?type=flightconfirm" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>

							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-primary">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="flightpaymentcount">0</h3>
											<p>
												<s:text name="tgi.label.payment_done" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-person-add"></i>
										</div>
										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showFlightWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showFlightWeekOrderList?dateType=WEEK&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showFlightWeekOrderList?dateType=MONTH&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>
										<%--  <a href="showFlightWeekOrderList?type=flightpayment" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>
							<!-- ./col -->
						</div>
					</s:if>
					<br>
					<!--  flightorder row ends -->
					<!--  hotel order row -->

					<!-- Small boxes (Stat box) -->
					<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport() ||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUserMode()) && #session.Company.companyCmsService.isHotel())}">
						<div class="row">
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-warning">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="hotelOrders">0</h3>
											<p>
												<s:text name="tgi.label.hotel_new-orders" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-bag"></i>
										</div>

										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa  btn-sm ">
												<a href="showHotelWeekOrderList?dateType=TODAY" class="small-box-footer"> <s:text name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showHotelWeekOrderList?dateType=WEEK"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showHotelWeekOrderList?dateType=MONTH" class="small-box-footer">
												<s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>

										<!-- <a href="showHotelWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
									</div>
								</div>
							</div>
							<!-- ./col -->
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-warning">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="hotelconfirmCount">
												0<sup style="font-size: 20px"></sup>
											</h3>
											<p>
												<s:text name="tgi.label.confirmed_order_counts" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-stats-bars"></i>
										</div>
										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showHotelWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showHotelWeekOrderList?dateType=WEEK&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showHotelWeekOrderList?dateType=MONTH&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>
										<%--  <a href="showHotelWeekOrderList?type=hotelconfirm" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>
							<!-- ./col -->
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-warning">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="hotelpaymentcount">0</h3>
											<p>
												<s:text name="tgi.label.payment_done" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-person-add"></i>
										</div>
										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showHotelWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showHotelWeekOrderList?dateType=WEEK&paymentStatus=SUCCESS"
												class="small-box-footer"><s:text name="tgi.label.week" /><i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showHotelWeekOrderList?dateType=MONTH&paymentStatus=SUCCESS"
												class="small-box-footer"><s:text name="tgi.label.month" />
													<i class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>
										<%-- <a href="showHotelWeekOrderList?type=hotelpayment" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>
							<!-- ./col -->

						</div>
					</s:if>
					<br>
					<!--  hotel order row -->


					<!-- car order row -->
					<s:if
						test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport()
                   					||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUserMode()) && #session.Company.companyCmsService.isCar())}">
						<div class="row">
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-danger">
									<div class="card-block p-b-0">

										<!-- <div class="small-week">
               <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer"  > Today <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                          <a href="showFlightWeekOrderList?dateType=WEEK" class="small-box-footer"  >Week<i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm active">
                                <a href="showFlightWeekOrderList?dateType=MONTH" class="small-box-footer"  >Month <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div> -->

										<div class="inner">
											<h3 id="carOrders">
												0<sup style="font-size: 20px"></sup>
											</h3>
											<p>
												<s:text name="tgi.label.car_new_orders" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-bag"></i>
										</div>

										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showCarWeekOrderList?dateType=TODAY"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a href="showCarWeekOrderList?dateTyp=WEEK"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showCarWeekOrderList?dateTyp=MONTH"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>


										<!--  <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
									</div>
								</div>
							</div>
							<!-- ./col -->
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-danger">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="carconfirmCount">
												0<sup style="font-size: 20px"></sup>
											</h3>
											<p>
												<s:text name="tgi.label.confirmed_order_counts" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-stats-bars"></i>
										</div>
										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showCarWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showCarWeekOrderList?dateTyp=WEEK&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showCarWeekOrderList?dateTyp=MONTH&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>

										<%-- <a href="showCarWeekOrderList?bookingStatus=CONFIRM" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>
							<!-- ./col -->
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-danger">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="carpaymentcount">0</h3>
											<p>
												<s:text name="tgi.label.payment_done" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-person-add"></i>
										</div>
										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showCarWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showCarWeekOrderList?dateTyp=WEEK&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showCarWeekOrderList?dateTyp=MONTH&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>
										<%-- <a href="showCarWeekOrderList?paymentStatus=SUCCESS" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>
							<!-- ./col -->
						</div>
					</s:if>
					<br>
					<!-- end car order row -->


					<!-- tour order row -->
					<s:if
						test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport() ||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUserMode()) && #session.Company.companyCmsService.isLimo())}">
						<!-- limo order row -->
						<div class="row">
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-purple">
									<div class="card-block p-b-0">

										<!-- <div class="small-week">
               <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer"  > Today <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                          <a href="showFlightWeekOrderList?dateType=WEEK" class="small-box-footer"  >Week<i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm active">
                                <a href="showFlightWeekOrderList?dateType=MONTH" class="small-box-footer"  >Month <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div> -->

										<div class="inner">
											<h3 id="limoOrders">
												0<sup style="font-size: 20px"></sup>
											</h3>
											<p>
												<s:text name="tgi.label.limo_new_orders" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-bag"></i>
										</div>

										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showLimoWeekOrderList?dateType=TODAY"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a href="showLimoWeekOrderList?dateType=WEEK"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showLimoWeekOrderList?dateType=MONTH"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>


										<!--  <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
									</div>
								</div>
							</div>
							<!-- ./col -->
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-purple">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="limoconfirmCount">
												0<sup style="font-size: 20px"></sup>
											</h3>
											<p>
												<s:text name="tgi.label.confirmed_order_counts" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-stats-bars"></i>
										</div>
										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showLimoWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showLimoWeekOrderList?dateType=WEEK&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showLimoWeekOrderList?dateType=MONTH&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>

										<%-- <a href="showLimoWeekOrderList?bookingStatus=CONFIRM" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>
							<!-- ./col -->
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-purple">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="limopaymentcount">0</h3>
											<p>
												<s:text name="tgi.label.payment_done" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-person-add"></i>
										</div>
										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showLimoWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showLimoWeekOrderList?dateType=WEEK&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showLimoWeekOrderList?dateType=MONTH&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>
										<%-- <a href="showLimoWeekOrderList?paymentStatus=SUCCESS" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>
							<!-- ./col -->
						</div>
						<br>
					</s:if>
					<!-- end limo order row -->
					<s:if
						test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport()
                   					||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUserMode()) && #session.Company.companyCmsService.isTour())}">
						<div class="row">
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-success">
									<div class="card-block p-b-0">

										<!-- <div class="small-week">
               <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer"  > Today <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                          <a href="showFlightWeekOrderList?dateType=WEEK" class="small-box-footer"  >Week<i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm active">
                                <a href="showFlightWeekOrderList?dateType=MONTH" class="small-box-footer"  >Month <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div> -->

										<div class="inner">
											<h3 id="tourOrders">
												0<sup style="font-size: 20px"></sup>
											</h3>
											<p>
												<s:text name="tgi.label.tour_new_orders" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-bag"></i>
										</div>

										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showTourWeekOrderList?dateType=TODAY"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a href="showTourWeekOrderList?dateType=WEEK"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a href="showTourWeekOrderList?dateType=MONTH"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>


										<!--  <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
									</div>
								</div>
							</div>
							<!-- ./col -->
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-success">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="tourconfirmCount">
												0<sup style="font-size: 20px"></sup>
											</h3>
											<p>
												<s:text name="tgi.label.confirmed_order_counts" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-stats-bars"></i>
										</div>

										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showTourWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showTourWeekOrderList?dateType=WEEK&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showTourWeekOrderList?dateType=MONTH&bookingStatus=CONFIRM"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>
										<%-- <a href="showTourWeekOrderList?bookingStatus=CONFIRM" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>
							<!-- ./col -->
							<div class="col-sm-4">
								<!-- small box -->
								<div class="card card-inverse card-success">
									<div class="card-block p-b-0">
										<div class="inner">
											<h3 id="tourpaymentcount">0</h3>
											<p>
												<s:text name="tgi.label.payment_done" />
											</p>
										</div>
										<div class="icon">
											<i class="ion ion-person-add"></i>
										</div>
										<div class="small-week small-box-footer">
											<label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showTourWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.today" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa  btn-sm ">
												<a
												href="showTourWeekOrderList?dateType=WEEK&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.week" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label> <label class="btn btn-transparent grey-salsa btn-sm ">
												<a
												href="showTourWeekOrderList?dateType=MONTH&paymentStatus=SUCCESS"
												class="small-box-footer"> <s:text
														name="tgi.label.month" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</label>
										</div>
										<%-- <a href="showTourWeekOrderList?type=tourpayment" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
									</div>
								</div>
							</div>
							<!-- ./col -->
						</div>
					</s:if>
					<br>

				</div>
			</div>
			<!--  Number of agents and distributers ends -->

			<!--  Number of agents and distributers -->
			<div class="col-sm-12 col-md-12 onlyagents">
				<div class="row">
					<s:if test="%{#session.User.userRoleId.isSuperUser()}">
						<div class="col-sm-6">
							<div class="row">
								<!-- small box -->
								<div class="small-box bg-orange-active">
									<div class="inner clearfix">
										<div class="col-sm-7">
											<h3 id="distributorlist">0</h3>
										</div>
										<div class="col-sm-5 text-right">

											<p>
												<a href="getAllDistributors" class="small-box-footer"><s:text
														name="tgi.label.distributors" /> <i
													class="fa fa-arrow-circle-right"></i></a>
											</p>

										</div>

									</div>

								</div>
								<div class="icon">
									<i class="ion ion-pie-graph"></i>
								</div>

							</div>
						</div>
						<!-- ./col -->
					</s:if>

					<!--   Agents  -->
					<s:if
						test="%{(#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isDistributor())}">
						<div class="col-sm-6">
							<div class="row">
								<!-- small box -->
								<div class="small-box bg-lime-active">
									<div class="inner clearfix">
										<div class="inner">
											<div class="col-sm-6">
												<h3 id="agentlist">1</h3>
											</div>
											<div class="col-sm-5 text-right">
												<p>
													<a href="AllAgencyList" class="small-box-footer"> <s:text
															name="tgi.label.agents" /> <i
														class="fa fa-arrow-circle-right"></i></a>
												</p>
											</div>

										</div>

									</div>
									<div class="icon">
										<i class="ion ion-pie-graph"></i>
									</div>

								</div>
							</div>
						</div>
					</s:if>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 caption-subject">
					<h4>
						<span><s:text name="tgi.label.sales_summary" /></span>
					</h4>
				</div>
				<div class="col-md-4">
					<div class="pnl">
						<div class="hd clearfix">
							<h5>
								<s:text name="tgi.label.weekly_sales" />
							</h5>
							<div class="set pull-right">
								<a href="#" class="fa fa-refresh"></a> <a href="#"
									class="fa fa-expand"></a>
							</div>
						</div>
						<div class="cnt">
							<div class="row">
								<div class="col-md-12">
									<ul class="chart-dot-list">
										<li class="air"><s:text name="tgi.label.air" /></li>
										<li class="hotel"><s:text name="tgi.label.hotel" /></li>
										<!--  <li class="car">Car</li> -->
									</ul>
								</div>
								<div class="col-md-12">
									<div class="chart chart-doughnut">
										<canvas id="chart-area" width="262" height="196"
											style="width: 262px; height: 196px;">
                                    </canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-8">
					<div class="pnl ">
						<div class="hd clearfix">
							<h5>
								<s:text name="tgi.label.weekly_sales" />
							</h5>
							<div class="set pull-right">
								<a href="#" class="fa fa-refresh"></a> <a href="#"
									class="fa fa-expand"></a>
							</div>
						</div>
						<div class="cnt">
							<div class="chart">
								<canvas id="chart-bar" height="289" width="867"
									style="width: 867px; height: 289px;"></canvas>
							</div>
						</div>
					</div>
				</div>


				<div class="col-md-4">
					<div class="pnl">
						<div class="hd clearfix">
							<h5>
								<s:text name="tgi.label.monthly_sales" />
							</h5>
							<div class="set pull-right">
								<a href="#" class="fa fa-refresh"></a> <a href="#"
									class="fa fa-expand"></a>
							</div>
						</div>
						<div class="cnt">
							<div class="row">
								<div class="col-md-12">
									<ul class="chart-dot-list">
										<li class="air"><s:text name="tgi.label.air" /></li>
										<li class="hotel"><s:text name="tgi.label.hotel" /></li>
										<!--  <li class="car">Car</li> -->
									</ul>
								</div>
								<div class="col-md-12">
									<div class="chart chart-doughnut">
										<canvas id="chart-area-month" width="262" height="196"
											style="width: 262px; height: 196px;">
                                    </canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-8">
					<div class="pnl ">
						<div class="hd clearfix">
							<h5>
								<s:text name="tgi.label.monthly_sales" />
							</h5>
							<div class="set pull-right">
								<a href="#" class="fa fa-refresh"></a> <a href="#"
									class="fa fa-expand"></a>
							</div>
						</div>
						<div class="cnt">
							<div class="chart">
								<canvas id="chart-bar-month" height="289" width="867"
									style="width: 867px; height: 289px;"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>


		</section>
		<!-- /.content -->
	</div>
</section>
<!-- /.content-wrapper -->

<script src="admin/js/jquery.validate.min.js"></script>
<script src="admin/js/Chart.min.js"></script>
<script src="admin/js/chartDataSample.js"></script>
<!-- 2. Include library -->
<script src="admin/js/clipboard.min.js"></script>
<!-- 3. Instantiate clipboard by passing a string selector -->
<script>
    var clipboard = new Clipboard('.btn');
    clipboard.on('success', function(e) {
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
</script>
