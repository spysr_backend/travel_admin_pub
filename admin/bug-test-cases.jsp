<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="dj" uri="/struts-dojo-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <link href="admin/css/inner-css/bug-tracker.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
	$(function() {

		$('#success').click(function() {
			window.location.assign(document.referrer);
			$('#success-alert').hide();
		});
		$('#cancel').click(function() {
			$('#error-alert').hide();

		});
	});
</script>

		<section class="content-header">
		<h1>
			<i class="fa fa-list"></i>Bug Test Cases
		</h1>
		<div class="breadcrumb-wrapper">
			<%-- <span class="label">You are here:</span> --%>
			<ol class="breadcrumb">
				<li><a href="goBugTrackerList">Bug List</a></li>
				<li><a href="goTestCases?id=${param.bugId}">Create Test Case</a></li>
				<li class="active">View Test Cases</li>
			</ol>
		</div>
		</section>
		<!-- Main content -->
		<section class="content clearfix">
				<div class="row">
					<div class="support">
								<s:iterator value="bugTrackerDatas" status="rowCount">
										<div class="qotationlist clearfix">
											<div class="row">
												<div class="col-sm-4">
															<h4>${title}</h4>
												</div>
												<div class="col-sm-8">
												<h5 class="pull-right">${referenceNo} &nbsp;&nbsp;<small class="text-muted">
												<fmt:formatDate value="${createdAt}" pattern="E, MMM dd, yyyy h:m a" /></small>
												</h5>
												<p></p>
												</div>
											</div>
											</div>
									<div class="dash-table">
										<div class="content mt-0 pt-0">
											<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"  role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
                                        <thead>
                                        <tr class="table-sub-header">
                                        	<th colspan="13" class="noborder-xs">
                                        	<div class="pull-right" style="padding:4px;margin-bottom: -10px;">   
                                        	<div class="btn-group"> 
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Travel Sales Lead">
											  <a class="btn btn-sm btn-default" data-event="duplicate" data-toggle="modal" data-target="#save_new_testCase" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class=""> Create	</span></a> 
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${bugTrackerDatas.bugTestCases.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_row" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Delete</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
										<tr class="border-radius border-color" role="row">
                                                   	<th></th>
                                                   	<th style="width: 50px;" data-priority="1">
                                                   	<div class="center">
															<div data-toggle="tooltip" data-placement="top" title="Select All">
															<div class="checkbox checkbox-default">
										                        <input id="master_check" data-action="all" type="checkbox" >
										                        <label for="master_check">&nbsp;</label>
										                    </div>
										                    </div>
										                   </div>
													</th>
                                                    
                                                    <th data-priority="3">Test Case Id</th>
                                                    <th data-priority="2" class="col-md-2">Title</th>
                                                    <th data-priority="4">Module Name</th>
                                                    <th data-priority="5">Test Priority</th>
                                                    <th data-priority="6">Test Status</th>
                                                    <th data-priority="7">Prepared By</th>
                                                    <th data-priority="8">Tested By</th>
                                                    <th data-priority="8">Verify By</th>
                                                    <th class="col-md-1">Created Date</th>
                                                    <th class="col-md-1">Action</th>
                                                     <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
   											<s:if test="bugTrackerDatas.bugTestCases.size > 0">
                                           <s:iterator value="bugTrackerDatas.bugTestCases" status="rowstatus">
                                                <tr>
                                                <td scope="row" data-title="S.No"><div class="center"><s:property value="%{#rowstatus.count}" /></div></td>
                                                <td data-title="Select" class="select-checkbox">
                                                <div class="center">
																<div class="checkbox checkbox-default">
										                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${id}" class="check_row">
										                        <label for="checkbox1_sd_${rowstatus.count}"></label>
										                        </div>
										                   		</div>
														</td>
                                                    
                                                    <td>
                                                    <c:choose>
                                                    <c:when test="${testCaseId != null}">
                                                    <span>${testCaseId}</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                    <span>N/A</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                    <td>${testCaseTitle}</td>
                                                    <td>
                                                    <c:choose>
                                                    <c:when test="${module != null && module != ''}">
                                                    ${module}
                                                    </c:when>
                                                    <c:otherwise>
                                                    <span>N/A</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                    <td>
                                                    <c:choose>
													<c:when test="${testPriority == 'Medium'}">
													<span class="label label-warning">${testPriority}</span>
													</c:when>
													<c:when test="${testPriority == 'Low'}">
													<span class="label label-success">${testPriority}</span>
													</c:when>
													<c:when test="${testPriority == 'Critical'}">
													<span class="label label-dnager">${testPriority}</span>
													</c:when>
													<c:otherwise>
													<span class="text-warning">N/A</span>
													</c:otherwise>
													</c:choose>
                                                    </td>
                                                    <td class="">
                                                    <c:choose>
                                                    <c:when test="${testStatus == 'Pass'}">
                                                    <span class="label label-success">${testStatus}</span>
                                                    </c:when>
                                                    <c:when test="${testStatus == 'Fail'}">
                                                     <span class="label label-danger">${testStatus}</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                    <span>N/A</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                    <td class="">
                                                    <c:choose>
                                                    <c:when test="${testDesignedByName != null}">
                                                    <span>${testDesignedByName}</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                    <span class="">N/A</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                    <td class="">
                                                    <c:choose>
                                                    <c:when test="${testExecutedByName != null}">
                                                    <span>${testExecutedByName}</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                    <span class="">N/A</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                    <td class="">
                                                    <span>${verifyByName}</span>
                                                    </td>
                                                    <td><s:date name="createdAt" format="dd-MMM-YYYY hh:mm a"/></td>
                                        	        <td data-title="Action">
                                        	        <s:if test="verify!=true"> 
																		<a href="#!" data-test-case-id="${id}" class=" btn btn-primary btn-xs verify_test_case">Verify</a> 
																		<a href="#!" class="btn btn-success btn-xs" data-event="duplicate" data-toggle="modal" data-target="#edit_testCase_${rowstatus.count}"><i class="fa fa-edit"></i> Edit</a>
																			
													</s:if>
														<s:else>
																<a href="#" class="btn btn-success btn-xs" id="verify" style="cursor:pointer !important;"><i class="fa fa-check " > Verified</i>
																</a>
																<c:if test="${session.User.userRoleId.techHead==true}">
																<a href="#!" class="btn btn-success btn-xs" data-event="duplicate" data-toggle="modal" data-target="#edit_testCase_${rowstatus.count}"><i class="fa fa-edit"></i> Edit</a>
																</c:if>
														</s:else>
													</td>  
													<td></td>
														 </tr>
					<!--##################### update test cases ###################################-->
					<div class="modal fade" id="edit_testCase_${rowstatus.count}" role="dialog">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<h4 class="modal-title text-center">Update Test Cases</h4>
																	<button type="button" class="close slds-modal__close" data-dismiss="modal">
																	<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
																	</button>
																</div>
																<form id="updateBugTestCasesForm_${rowstatus.count}" method="post" class="bv-form">
																<input type="hidden" name="bugId" id="bug-id" value="${param.bugId}">
																<input type="hidden" name="id" id="id" value="${id}">
																	<div class="modal-body">
																	 <div class="row">
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Title</label>
																					<div class="controls">
																							<div class="form-group">
																								<input type="text" name="testCaseTitle" id="testCaseTitle"  class="form-control" value="${testCaseTitle}"/>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Description</label>
																					<div class="controls">
																							<div class="form-group">
																								<textarea name="description" id="description" class="form-control" rows="2" cols="">${description}</textarea>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Module Name</label>
																					<div class="controls">
																							<div class="form-group">
																								<input type="text" name="module" id="module" class="form-control" value="${module}"/>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Test Priority</label>
																					<div class="controls">
																							<div class="form-group">
																								<select class="form-control" id="testPriority" name="testPriority">
																									<option value="" selected="selected">Select Priority</option>
																									<c:forEach var="priority" items="${testCaseUtility.testPriority}">
																										 <option value="${priority}" ${priority == testPriority ? 'selected': ''}>${priority}</option>
																										 </c:forEach>
																								 </select>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Test Status</label>
																					<div class="controls">
																							<div class="form-group">
																								<select class="form-control" id="testStatus" name="testStatus">
																									<option value="" selected="selected">Select Status</option>
																									<c:forEach var="status" items="${testCaseUtility.testStatus}">
																										 <option value="${status}" ${status == testStatus ? 'selected': ''}>${status}</option>
																										 </c:forEach>
																								 </select>
																							</div>
																					</div>
																				</div>
																			</div>
																</div>
																	<div class="modal-footer">
																		<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
																		<button type="submit" class="btn btn-success update-bugTestCase-Btn" id="" data-form-name="updateBugTestCasesForm_${rowstatus.count}" data-modal-id="edit_testCase_${rowstatus.count}">Save</button>
																	</div>
																</form>
															</div>
									
														</div>
													</div>
                                                </s:iterator>
                                            </s:if>
                                            </tbody>
											
                                        </table></div></div></div>
											</s:iterator> 
							</div>
						</div>
				</section>
											<div class="test-cases-add-model">
											<div class="modal fade" id="save_new_testCase" role="dialog">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<h4 class="modal-title text-center">Test Cases</h4>
																	<button type="button" class="close slds-modal__close" data-dismiss="modal">
																	<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
																	</button>
																</div>
																<form id="saveBugTestCasesForm" method="post" class="bv-form">
																<input type="hidden" name="bugId" id="bug-id" value="${param.bugId}">
																	<div class="modal-body">
																	 <div class="row">
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Title</label>
																					<div class="controls">
																							<div class="form-group">
																								<input type="text" name="testCaseTitle" id="testCaseTitle"  class="form-control" value=""/>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Description</label>
																					<div class="controls">
																							<div class="form-group">
																								<textarea name="description" id="description" class="form-control" rows="2" cols=""></textarea>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Module Name</label>
																					<div class="controls">
																							<div class="form-group">
																								<input type="text" name="module" id="module" class="form-control" value=""/>
																							</div>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Test Priority</label>
																					<div class="controls">
																							<div class="form-group">
																								<select class="form-control" id="testPriority" name="testPriority">
																									<option value="" selected="selected">Select Priority</option>
																									<c:forEach var="priority" items="${testCaseUtility.testPriority}">
																										 <option value="${priority}">${priority}</option>
																										 </c:forEach>
																								 </select>
																							</div>
																					</div>
																				</div>
																				<%-- <div class="col-md-12">
																					<label class="form-control-label" style="font-size: 13px;color: #0f1010;" for="formGroupInputSmall">Test Status</label>
																					<div class="controls">
																							<div class="form-group">
																								<select class="form-control" id="testStatus" name="testStatus">
																									<option value="" selected="selected">Select Status</option>
																									<c:forEach var="status" items="${testCaseUtility.testStatus}">
																										 <option value="${status}">${status}</option>
																										 </c:forEach>
																								 </select>
																							</div>
																					</div>
																				</div> --%>
																			</div>
																</div>
																	<div class="modal-footer">
																		<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
																		<button type="submit" class="btn btn-success" id="save-bug-test-case">Save</button>
																	</div>
																</form>
															</div>
									
														</div>
													</div>
</div>

<script>
$(document).ready(function(){
		  $("#save-bug-test-case").click(function(e) {
			notifySuccess();
			e.preventDefault();	
				$.ajax({
					url : "saveBugTestCase",
					type : "POST",
					dataType: 'json',
					data : $("#saveBugTestCasesForm").serialize(),
					success : function(jsonData) {
						if(jsonData.json.status == 'success'){
							$.notify({icon: 'fa fa-check',message: jsonData.json.message},{type: 'success'});
							setTimeout(location.reload.bind(location), 1000);
							}
		  				else if(jsonData.json.status == 'error')
		  				{
		  					$.notify({icon: 'fa fa-times',message: jsonData.json.message},{type: 'danger'});
		  					setTimeout(location.reload.bind(location), 1000);
		  					}
					},
					error: function (request, status, error) {
						showModalPopUp("Bug Test Cases can not be saved.","e");
					}
				});
			});

		  $(".update-bugTestCase-Btn").click(function(e) {
			e.preventDefault();	
			notifySuccess();
			var formName = $(this).attr("data-form-name");
			var modalId = $(this).attr("data-modal-id");
				$.ajax({
					url : "updateBugTestCase",
					type : "POST",
					dataType: 'json',
					data : $("#" + formName).serialize(),
					success : function(jsonData) {
						if(jsonData.json.status == 'success'){
							$("#"+modalId).hide();
							$.notify({icon: 'fa fa-check',message: jsonData.json.message},{type: 'success'});
							setTimeout(location.reload.bind(location), 1000);
							}
		  				else if(jsonData.json.status == 'error')
		  				{
		  					$.notify({icon: 'fa fa-times',message: jsonData.json.message},{type: 'danger'});
		  					setTimeout(location.reload.bind(location), 1000);
		  					}
					},
					error: function (request, status, error) {
						showModalPopUp("Bug Test Cases can not be saved.","e");
					}
				});
			});
		  $(".verify_test_case").click(function(e) {
			e.preventDefault();	
			var testCaseId = $(this).attr("data-test-case-id");
				$.ajax({
					url : "bugTestCaseVerify?id="+testCaseId,
					type : "POST",
					dataType: 'json',
					success : function(jsonData) {
							alertify.success("Test Case has been verfied successfully");
					},
					error: function (request, status, error) {
						alertify.error("Bug Test Cases can not be saved.","e");
					}
				});
			});

		});

$('#delete_all_row').on("click", function(event){
	// confirm dialog
	 var allVals = [];
	 $(".check_row:checked").each(function() {  
		 allVals.push($(this).attr('data-id'));
	 });  
		 var check = confirm("Are you sure you want to delete selected data?");
		 if(check == true)
		 {
			 var join_selected_values = []; 
			 join_selected_values = allVals.join(",");
			 $.ajax({
						url : "deleteBugTestCase?testCaseIds="+join_selected_values,
						type : "POST",
						dataType: 'html',
						success : function(data) {
							alertify.success("You've clicked OK Your data have successfully deleted");
							setTimeout("window.location.reload();", 1000);
						},
						error: function (request, status, error) {
							alertify.error("Test case can not be deleted.","e");
						}
				}); 
		 }
		 else
		 {
			 return false;	 
		 }
});
</script>
<script type="text/javascript" src="admin/js/admin/multiple-delete.js"></script>