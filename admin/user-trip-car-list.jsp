<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>

<link href="admin/filter_resource/easy-autocomplete.min.css" rel="stylesheet"
	type="text/css">
<%-- <script src="admin/filter_resource/jquery-1.11.2.min.js"></script> --%>
<script src="admin/filter_resource/jquery.easy-autocomplete.min.js"
	type="text/javascript"></script>


<!-- <link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/site-examples.css">-->
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/buttons.dataTables.min.css">
	
	 <link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/jquery.dataTables.min.css">
	
	
	<script type="text/javascript" async="" src="admin/print-pdf-excel/print-resource/ga.js.download"></script>
	<%-- <script type="text/javascript" src="admin/print-pdf-excel/print-resource/site.js.download">
	</script> --%>
	<%-- <script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/jquery-1.12.3.js.download">
	</script> --%>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/jquery.dataTables.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/dataTables.buttons.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.flash.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/jszip.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/pdfmake.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/vfs_fonts.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.html5.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.print.min.js.download">
	</script>
	<%-- <script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/demo.js.download">
	</script>
 --%>	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.colVis.min.js">
	</script>
	<script type="text/javascript" class="init">
	$(document).ready(function() {
		var currentDate = "export-data_"+$.datepicker.formatDate('yyyy-mm-dd', new Date())+(new Date).getTime();
		$('#example').DataTable( {
			dom: 'Bfrtip',
			 
		buttons: [
		            {
		                extend: 'copyHtml5',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                }
		            },
		            {
		                extend: 'excelHtml5',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            {
		                extend: 'pdfHtml5',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            {
		                extend: 'print',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            {
		                extend: 'csv',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            'colvis'
		        ]
			        
		} );
		
	} );

		</script>   
<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.show_date_to_user {
    padding: 0px 15px;
    position: relative;
    width: 93%;
    top: -25px;
    left: 5px;
    pointer-events: none;
    display: inline-block;
    color: #000;
}
</style>


<!-- <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
 --><script src="admin/js/jquery-ui.js" ></script>
 <script type="text/javascript">
	$(document).ready(
			function() {
				if($("#HdncompanyShowData").val() != null && $("#HdncompanyShowData").val() != "")
				$("#companyShowData").val($("#HdncompanyShowData").val());
				if($("#HdncompanyTypeShowData").val() != null && $("#HdncompanyTypeShowData").val() != "")
				$("#companyTypeShowData").val($("#HdncompanyTypeShowData").val());
				
				var company_list = [];
				var agents_list = [];
				  var user_list = []; 
				 

				$.ajax({
					//Action Name
					url :"CompanyListUnderSuperUser",
					dataType : "json",
					data : {
					 parent_company_user_id : $("#companyUserId").val(),
						email : $("#email").val()
					},

					success : function(data, textStatus, jqXHR) {

						var items = data.records;
						for (var i = 0; i < data.records.length; i++) {
							company_list.push(data.records[i].companyname + ","
									+ data.records[i].company_userid);
						}
						console.log(company_list);
						//response(items);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus);
					}
				});

				$("#search").autocomplete(
						{

							source : function(request, response) {
								var matcher = new RegExp('^'
										+ $.ui.autocomplete
												.escapeRegex(request.term),
										"i");
								response($.grep(company_list, function(item) {
									return matcher.test(item);

								}));
							},
							select : function(event, ui) {
								var label = ui.item.label;
								var company_userid = ui.item.value;
								console.log(company_userid);
							if(company_userid!=null){ 
								$.ajax({
									//Action Name
									url : "UserListUnderCompany",
									dataType : "json",
									data : {
										 company_user_id : company_userid 
									},

									success : function(data, textStatus, jqXHR) {
										 user_list=[];
										console.log("--data---------"+data);
										
										var items = data.user_records;
										
										for (var i = 0; i < data.user_records.length; i++) {
											user_list.push(data.user_records[i].username + "("+data.user_records[i].company_userid+")"  + ","
													+ data.user_records[i].id);
										}
										console.log(user_list);
										userlist(user_list);
									},
									error : function(jqXHR, textStatus, errorThrown) {
										console.log(textStatus);
									}
								});
							}
							
							 
							 }
						});
				
				
				$.ajax({
					//Action Name
					url :"AgentsListUnderSuperUser",
					dataType : "json",
					data : {
					 parent_company_user_id : $("#companyUserId").val(),
						email : $("#email").val()
					},

					success : function(data, textStatus, jqXHR) {

						var items = data.agentList;
						for (var i = 0; i < data.agentList.length; i++) {
							agents_list.push(data.agentList[i].username + "("+data.agentList[i].company_userid+")"  + ","
									+ data.agentList[i].id);
						}
						console.log("------agents_list------"+agents_list);
						 user_list=agents_list;
							console.log("------user_list------"+user_list);
						 userlist(user_list);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus);
					}
				});
				
			  });
	
	 function userlist(userlist)
			{
		 if(userlist.length>0){
				$("#userIdSearch").autocomplete(
						{
		 				source : function(request, response) {
								var matcher = new RegExp('^'
										+ $.ui.autocomplete
												.escapeRegex(request.term),
										"i");
								response($.grep(userlist, function(item) {
									return matcher.test(item);

								}));
							}
						});	 
		 }
		  
	}
 </script>


    <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
     

            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left">Car Trip List</h5>
                            								<div class="set pull-right">
									<a href="userTourTripList?tripType=tour" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								Tour Trip
								</button></a>
								</div>
								<div class="set pull-right">
									<a href="userLimoTripList?tripType=limo" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								Limo Trip
								</button></a>
								</div>
                                <div class="set pull-right">
									<a href="userHotelTripList?tripType=hotel" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								Hotel Trip
								</button></a>
								</div>
								<div class="set pull-right">
									<a href="userFlightTripList?tripType=flight" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								Flight Trip
								</button></a>
								</div>
								<div class="set pull-right">
									<a href="userAllTripList?tripType=all" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								All Trip
								</button></a>
								</div>
                               </div>
                                <form action="listCompanyFilter" class="filter-form">
							    <div class="" id="filterDiv" style="display:none;" >
							     <div class="form-group" style="margin-top:15px;">
                                <div class="col-md-2">
                                <input type="hidden" id="HdncompanyShowData" value="${param.companyShowData}" />
                                <select name="companyShowData" id="companyShowData" class="form-control">
                               	<option value="" selected="selected">Select Self Company</option>
                                <option value="all"><s:text name="tgi.label.all" /></option>
                                <option value="myself"><s:text name="tgi.label.my_self" /></option> 
                                </select>
                                </div>
                                <div class="col-md-2">
                                <input type="hidden" id="HdncompanyTypeShowData" value="${param.companyType}" />
                                <select name="companyType" id="companyTypeShowData" class="form-control">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="distributor"><s:text name="tgi.label.distributor" /></option>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div>
                                  <div class="col-md-2">
                                <input type="text" name="startDate" id="datepicker_startDate" value="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MM/dd/yyyy')}"
												placeholder="Start Date...."
												class="form-control search-query date1" />
												
                                </div>
                                <div class="col-md-2">
                                <input type="text" name="endDate" id="datepicker_endDate" value="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MM/dd/yyyy')}"
												placeholder="End Date...."
												class="form-control hasdatepicker date2" />
                                </div>
                                 <div class="col-md-2">
									
								<input type="text" name="userName" id="username-json"
												placeholder="User Name...."
												class="form-control search-query" />
								</div>
								<br><br>
                                <br>
								<div class="col-md-2">
									<input type="text" name="companyName" id="companyname-json"
													placeholder="Company Name...."
													class="form-control search-query" />
									</div>
								
                                <div class="col-md-2">
                                <input type="text" name="email" id="email-json"
												placeholder="Search By Email...."
												class="form-control search-query" />
                                </div>
                                
                                
                                <div class="col-md-2">
                                <input type="text" name="phone" id="phone-json"
												placeholder="Search By Phone...."
												class="form-control hasdatepicker" />
                                </div>
                                <div class="col-md-2">
                                <input type="text" name="city" id="city-json"
												placeholder="Search By City...."
												class="form-control hasdatepicker" />
                                </div>
                                <div class="col-md-2">
                                <input type="text" name="countryName" id="country-json"
												placeholder="Search By Country...."
												class="form-control hasdatepicker" />
                                </div>
                                <div class="col-md-2">
                                <input type="text" name="companyUserId" id="companyId-json"
												placeholder="Search By Company Id...."
												class="form-control hasdatepicker" />
                                </div>
                                <div class="col-md-2">
                                
                                <select name="flagTypeStatus" id="flagTypeStatus" class="form-control">
                               	<option value="" selected="selected">Select Company Status</option>
                                <option value="true">Active</option>
                                <option value="false">Inactive</option> 
                                </select>
                                </div>
                                <div class="col-md-2">
                                <select name="flagTypeLock" id="flagTypeLock" class="form-control">
                               	<option value="" selected="selected">Select Locked &amp; Unlocked</option>
                                <option value="false">Unlocked</option>
                                <option value="true">Locked</option> 
                                </select>
                                </div>
								<br><br>
                                <br>
                                <div class="col-md-2"> <button type="submit" class="btn btn-info">
      							<span class="glyphicon glyphicon-search"></span> <s:text name="tgi.label.search" />
    							</button></div>
                                </div></div>
                                </form>
                                <br><br>
							<thead><tr>
			</tr></thead>

 <div class="cnt cnt-table">
<div class="dash-table no-table-css">
		<div class="box ">
		<div class="fw-container">
		<div class="fw-body">
			<div class="content">
				<div id="example_wrapper" class="dataTables_wrapper ">
										<table id="example" class="display nowrap dataTable table-bordered table-responsive" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                                        <!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
                                        <thead>
										<tr class="border-radius border-color" role="row">
                                                    <th><s:text name="tgi.label.s_no" /></th>
                                                    <th><s:text name="tgi.label.name" /></th>
                                                    <th>Order No.</th>
                                                    <th>Pick Up</th>
                                                    <!-- <th>Drop Off</th> -->
                                                    <th>Pick Up Date Time</th>
                                                    <th>Drop Off Date Time</th>
													 <th><s:text name="tgi.label.action" /></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <s:iterator value="carOrderList" status="rowCount">
                                                <tr>
                                                <td data-title="S.No"><s:property value="%{#rowCount.count}" /></td>
								<td data-title="Name">${customer.firstName} ${customer.lastName} </td>
								<td data-title="OrderNo">${orderId}</td>
								<td data-title="pickUpOfficeName">${carOrderInfo.pickUpOfficeName} </td>
								<%-- <td data-title="returnOfficeName">${carOrderInfo.returnOfficeName}  </td> --%>
								<td data-title="pickUpDateTime">${spysr:formatDate(carOrderInfo.pickUpDateTime,'yyyy-MM-dd', 'MM/dd/yyyy')}
								${spysr:formatDate(carOrderInfo.pickUpTime,'yyyy-MM-dd HH:mm:ss', 'HH:mm:ss')} </td>
								<td data-title="CheckOut">${spysr:formatDate(carOrderInfo.returnDateTime,'yyyy-MM-dd', 'MM/dd/yyyy')} 
								${spysr:formatDate(carOrderInfo.returnTime,'yyyy-MM-dd HH:mm:ss', 'HH:mm:ss')}
								</td>
								<td data-title="Action">
										<div class="btn-group">
										<div class="set pull-right">
											<a class="btn btn-xs btn-success dropdown-toggle"
												data-toggle="dropdown" style="padding: 3px 6px;"> <i
												class="fa fa-cog fa-lg" aria-hidden="true"></i>
											</a>
											<ul class="dropdown-menu dropdown-info pull-right align-left">
												<li class=""><a
											href="userCarTripDetail?id=${id}"
											class="btn btn-xs" data-title="Update"> <span
											data-placement="top" class="fa fa-plus-circle"></span> <s:text name="tgi.label.details" /><span></span></a></li>
											</ul>
										</div></div>
									</td>
                                                </tr></s:iterator></tbody>
                                        </table></div>
                                    </div>
								</div></div></div></div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
       


<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"superUserCompanyList";
	$('#success').click(function() {
	 window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 <script>
$(document).ready(function() {
	var date1="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
	var date2="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
	 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
});
</script>
	
