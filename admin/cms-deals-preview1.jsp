<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
 
 <style>
    .all-deal-details {
  color: #222;
  margin-top: 15px;
  margin-bottom: 30px;
}
.all-deal-details a {
  color: #f90f03;
}
.all-deal-details .item-detail {
  margin-bottom: 15px;
}
.all-deal-details .list-two {
  padding: 0;
}
.all-deal-details .list-two li {
  width: 49%;
  display: inline-block;
}
.all-deals {
  margin-top: 15px;
  padding: 15px 0;
}
.all-deals h2 {
  margin-top: 10px;
  text-transform: uppercase;
  font-weight: 400;
  letter-spacing: 2px;
  color: #fff;
  margin-bottom: 15px;
  display: inline-block;
  padding-bottom: 8px;
  background: #111;
  padding: 5px 10px;
  border-left: 4px solid #f90f03;
}
.deal-item {
  background: #111;
  overflow: hidden;
  margin-bottom: 15px;
}
.deal-item img {
  width: 100%;
  height: 140px;
}
.deal-item h3 {
  padding: 10px;
  font-size: 18px;
  color: #f90f03;
  margin-top: 10px;
  text-transform: uppercase;
  font-size: 13px;
  font-weight: bold;
}
.deal-item h3 small {
  display: block;
  font-size: 12px;
  margin-top: 10px;
  margin-bottom: 20px;
  text-transform: uppercase;
}
.deal-item .price {
  padding: 10px;
  color: #ccc;
  float: right;
}
.deal-item .price small {
  display: block;
  font-size: 8px;
  text-transform: uppercase;
}
.all-tickets .ticket-orange {
  margin-bottom: 15px;
}
.all-tickets .ticket-orange a:hover {
  color: #fff;
}
.all-tickets .ticket-orange h4 {
  margin: 0;
  background: #f90f03;
  text-align: center;
  padding: 15px;
  font-weight: normal;
}
</style>
<section class="wrapper container-fluid">

		<div class="row all-deals">
			<div class="col-md-12">
				<%-- <h2><s:text name="tgi.label.where_you_go" /></h2> --%>
				<h2><s:text name="tgi.label.where_you_go" /></h2>
			</div>
			<s:if test="dealsVO.catagory != null">
				<div class="col-md-3 col-sm-6">
					<div class="deal-item" style="${dealsVO.boxBgcolor}">
						<a href="#">
						<img style="width:300px;height:300px;" id="modal" src="get_image_content?imgpath=${dealsVO.imageUrl}" class="postthumb img-responsive" alt="undefined" />
							<div class="price">
									<s:if test="dealsVO.price != null">
										<small><s:text name="tgi.label.price:" /> </small> <span style="${dealsVO.styleCurrency}">${dealsVO.currency}</span><span style="${dealsVO.stylePrice}">${dealsVO.price}</span>
								</s:if>
									<s:else>
								<s:text name="tgi.label.book_now" />
								</s:else>
							</div>
							<h3>
								<span style="${dealsVO.styleTitle}">${dealsVO.title}</span> <small><span style="${dealsVO.styleDescription}">${dealsVO.description}</span></small>
							</h3>
						</a>
					</div>
				</div>
			</s:if>
		</div>
</section>

          	  <s:if test="errorMessage != null && errorMessage != ''">
                	   <div id="actionError">${errorMessage}</div>
                	         <script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
                	  <script>
                	  $(document).ready(function() 
                			  {   
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
                	  $("#actionError").hide();
	      			  $('#alert_box_body').text($("#actionError").text());
                			  });
							</script>
						</s:if>
						<s:if test="message != null && message != ''">
						  <div id="actionMessage">${message}</div>
						       <script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
                	  <script>
                	  $(document).ready(function() 
                			  { 
                		  $('#alert_box').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
                	  $("#actionMessage").hide();
  	      			  $('#alert_box_body').text($("#actionMessage").text());
                			  });
							</script>
						</s:if>