<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
	$(function() {
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "hotelMarkupEdit?id=" + "${param.id}";
		$('#alert_box_info_ok').click(function() {
			window.location.assign(finalUrl);
			$('#alert_box_info').hide();

		});
		$('#alert_box_ok').click(function() {
			window.location.assign(finalUrl);
			$('#alert_box').hide();

		});
		$('#cancel').click(function() {
			$('#error-alert').hide();

		});
	});
</script>


<script type="text/javascript">
	$(document).ready(function() {
		$("#countrySelect").val($("#country").val());
	});
</script>


<%-- <script type="text/javascript">
 	 $(function() {
		 loadstations();
	 });
 	function loadstations() {
		  $.getJSON("hotels.json",{
				format : "json"
			}).done(
					 function(jsonbusdata) {
					 
						  citylist = [];
						  names = [];
						 
						 $.each(jsonbusdata, function(i, station) {
							citylist.push(station.chain);
							 
						 });
						 $("#hotelChain").autocomplete({
					        source: citylist,
					         
					    });
						 
						 $('#hotelChain').on('autocompleteselect', function (e, ui) {
							    	 
			 }); 
					    $.ui.autocomplete.filter = function (array, term) {
					        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
					        return $.grep(array, function (value) {
					            return matcher.test(value.label || value.value || value);
					        });
					    };
					 });
 	}
 	
 	
 	
 	
 	
 	</script> --%>

<script>
	$(function() {
		var id = $("#uniqueId").val();
		var accumulative = $("#accumulative").val();
		var fixedAmount = $("#fixedAmount").val();
		/* 	var positionOfMarkup=$("#positionOfMarkup").val(); */
		//var airline=$("#airline").val();
		var country = $("#country").val();
		//var classOfService=$("#classOfService").val();
		//alert(id+"\n"+country_id);
		document.getElementById('accumulative' + id).value = accumulative;
		document.getElementById('fixedAmount' + id).value = fixedAmount;
		/* 	 document.getElementById('positionOfMarkup'+id).value =positionOfMarkup; */
		//document.getElementById('airline'+id).value =airline;
		/* document.getElementById('country'+id).value =country; */
		//document.getElementById('classOfService'+id).value =classOfService;

	});
</script>
<script>
	$(document).ready(function() {
		$("#datepicker_arr").datepicker({
			format : "M-dd-yyyy"
		/*  changeMonth: true,
		 changeYear: true */
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#datepicker_dep").datepicker({
			format : "M-dd-yyyy"
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#datepicker_PromofareStart").datepicker({
			format : "M-dd-yyyy"
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#datepicker_PromofareEnd").datepicker({
			format : "M-dd-yyyy"
		});
	});
</script>


<!--************************************
        MAIN ADMIN AREA
    ****************************************-->

<!--ADMIN AREA BEGINS-->

<section class="wrapper container-fluid">

	<div class="row">
		<div class="col-md-12">
			<div class="card1">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.edit_hotel_markup" />
					</h5>
					<div class="set pull-left">
						<!--   <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
					</div>
					<div class="set pull-right">
						<div class="dropdown">
							<div class="btn-group">
								<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
								<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="hotelMarkupList"><span
											class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text
												name="tgi.label.hotel_markup_list" /> </a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				 <div class="row">
					<div class="col-lg-12">
						<div class="card-block1">
						<form action="updateHotelMarkup" id="hotel-markup-update"
							method="post" class="form-horizontal">
							
							
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.markup_name" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm" id="name"
														name="name" placeholder="markup Name" autocomplete="off"
														 value="<s:property value="hotelMarkupCurrent.name"/>"></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.hotel_group" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
														id="hotelChain" name="hotelChain"
														placeholder="Hotel Group" required
														value="<s:property value="hotelMarkupCurrent.hotelChain"/>"></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.hotel_name" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
														id="hotelName" name="hotelName" placeholder="hotel Name"
														autocomplete="off" required
														value="<s:property value="hotelMarkupCurrent.hotelName"/>"></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.city" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
														id="hotelCity" name="hotelCity" placeholder="markup Name"
														autocomplete="off" required
														value="<s:property value="hotelMarkupCurrent.hotelCity"/>"></div></div>
											</div>
										</div>
									</div>
									
					</div>
									<div class=" row">
										<div class="col-md-6">
											<label class="form-control-label" for="prependedInput"><s:text
													name="tgi.label.country" /> </label> <input type="hidden"
												value="<s:property value="hotelMarkupCurrent.id"/>"
												id="uniqueId"> <input type="hidden"
												value="<s:property value="hotelMarkupCurrent.id"/>" name="id">
											<input type="hidden"
												value="<s:property value="hotelMarkupCurrent.hotelCountry"/>"
												id="country">
											<div class="controls">
												<div class="form-group">
													<div class="">
														<select class="form-control input-sm" name="hotelCountry"
															id="countrySelect" autocomplete="off" required>
															<option selected value="ALL"><s:text
																	name="tgi.label.all" /></option>
															<s:iterator value="CountryList">
																<option value="<s:property value="c_name"/>"><s:property
																		value="c_name"></s:property></option>
															</s:iterator>

														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text
													name="tgi.label.is_accumulative" /> </label> <input type="hidden"
												value="<s:property value="hotelMarkupCurrent.isaccumulative"/>"
												id="accumulative">
											<div class="controls">
												<div class="">
													<div class="form-group">
														<div class="">
															<select class="form-control input-sm"
																name="isaccumulative"
																id="accumulative<s:property value="hotelMarkupCurrent.id"/>"
																autocomplete="off" required>
																<option value="1"><s:text name="tgi.label.yes" /></option>
																<option value="0" selected="selected"><s:text
																		name="tgi.label.no" /></option>

															</select>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
									<div class=" row">
									<div class="col-md-4">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.is_fixedAmount" />
											</label>
											<input type="hidden"
											value="<s:property value="hotelMarkupCurrent.isfixedAmount"/>"
											id="fixedAmount">
										<div class="controls"><div class="form-group"><div class="">
														<select class="form-control input-sm" name="isfixedAmount"
														id="fixedAmount<s:property value="hotelMarkupCurrent.id"/>"
														autocomplete="off" required>
														<option value="1"><s:text name="tgi.label.yes" /></option>
														<option value="0"><s:text name="tgi.label.no" /></option>
													</select></div></div>
												</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_amount" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
														id="markupAmount" name="markupAmount" placeholder="amount"
														autocomplete="off" required
														value="<s:property value="hotelMarkupCurrent.markupAmount"/>"></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.position_of_markup" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="number" class="form-control input-sm"
														name="positionMarkup" id="country" autocomplete="off"
														required
														value="<s:property value="hotelMarkupCurrent.positionMarkup"/>"></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.checkin_date" />
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" id="datepicker_arr"
														class="form-control input-sm" name="checkInTr"
														autocomplete="off" placeholder="ALL" 
														value="${spysr:formatDate(hotelMarkupCurrent.checkIn,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.checkout_date" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" id="datepicker_dep"
														class="form-control input-sm" name="checkOutTr"
														autocomplete="off" placeholder="ALL"
														value="${spysr:formatDate(hotelMarkupCurrent.checkOut,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"></div></div>
											</div>
										</div>
									</div>
										
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_startdate" />
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" id="datepicker_PromofareStart"
														class="form-control input-sm" name="promofareStartDateTr"
														value="${spysr:formatDate(hotelMarkupCurrent.promofareStartDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"
														autocomplete="off" placeholder="ALL"></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_enddate" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
													<input type="text" id="datepicker_PromofareEnd"
														class="form-control input-sm" name="promofareEndDateTr"
														value="${spysr:formatDate(hotelMarkupCurrent.promofareEndDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"
														autocomplete="off" placeholder="ALL" >
											</div></div>
										</div>
									</div>
										
									
					</div>
							<div class="form-group table-btn">
								<div class="col-sm-10 col-sm-offset-2 ">

									<div class="pull-right">
										<button class="btn btn-default" type="submit">
											<s:text name="tgi.label.cancel" />
										</button>
										<button class="btn btn-primary " type="submit">
											<s:text name="tgi.label.update" />
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<br>

</div></div>
			</div>
		</div>
	</div>



</section>
<!--bootstrap vadidator  -->
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('#hotel-markup-update')
								.bootstrapValidator(
										{
											// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
											feedbackIcons : {
												valid : 'fa fa-check',
												invalid : 'fa fa-remove',
												validating : 'fa fa-refresh'
											},
											fields : {

												configData : {
													message : 'ConfigData Role is not valid',
													validators : {
														notEmpty : {
															message : 'Please select a ConfigData  '
														},
													}
												},
												name : {
													message : 'MarkUp Name is not valid',
													validators : {
														notEmpty : {
															message : 'Please enter a MarkUp Name'
														},
														stringLength : {
															min : 3,
															max : 200,

															message : 'MarkUp Name  must be more than 3 and less than 200 characters long'
														}

													}
												},
												hotelChain : {
													message : 'HotelChain is not valid',
													validators : {
														notEmpty : {
															message : 'Select enter a HotelChain'
														},
														stringLength : {
															min : 3,
															max : 200,

															message : 'HotelChain  must be more than 3 and less than 200 characters long'
														}

													}
												},
												hotelName : {
													message : 'HotelName is not valid',
													validators : {
														notEmpty : {
															message : 'Select enter a HotelName'
														},

													}
												},
												hotelCity : {
													message : 'City is not valid',
													validators : {
														notEmpty : {
															message : 'Please enter a City'
														},
														stringLength : {
															min : 3,
															max : 100,

															message : 'City  must be more than 3 and less than 200 characters long'
														}

													}
												},

												hotelCountry : {
													message : 'Country Type is not valid',
													validators : {
														notEmpty : {
															message : 'Please Select a Country'
														},
													}
												},
												isaccumulative : {
													message : 'isaccumulative  Type is not valid',
													validators : {
														notEmpty : {
															message : 'Please Select a isaccumulative'
														},
													}
												},
												isfixedAmount : {
													message : 'IsfixedAmount  Type is not valid',
													validators : {
														notEmpty : {
															message : 'Please Select a IsfixedAmount'
														},
													}
												},
												markupAmount : {
													message : 'Amount Type is not valid',
													validators : {
														notEmpty : {
															message : 'Please enter a Address'
														},
														numeric : {
															message : 'The value is not a number',
															// The default separators
															thousandsSeparator : '',
															decimalSeparator : '.'
														}

													}
												},
												positionMarkup : {
													message : 'positionMarkup Type is not valid',
													validators : {
														notEmpty : {
															message : 'Please enter a position markup'
														},
													}
												},
											/* eventDate: {
											    validators: {
											        notEmpty: {
											            message: 'The date is required'
											        },
											        date: {
											            format: 'MM/DD/YYYY',
											            message: 'The date is not a valid'
											        }
											    }
											} */
											}
										})
								.on('error.form.bv', function(e) {
									// do something if you want to check error 
								})
								.on(
										'success.form.bv',
										function(e) {
											/* notifySuccess(); */
											showModalPopUp(
													"Saving Details, Please wait ..",
													"i");

										})
								.on('status.field.bv', function(e, data) {
									if (data.bv.getSubmitButton()) {
										console.debug("button disabled ");
										data.bv.disableSubmitButtons(false);
									}
								});
					});
</script>
<!--ADMIN AREA ENDS-->

<s:if test="message != null && message  != ''">
	<script src="admin/js/jquery.min.js"></script>
	<script src="admin/js/bootstrap.min.js"></script>
	<c:choose>
		<c:when test="${fn:contains(message, 'successfully')}">
			<script>
				$(document).ready(function() {
					$('#alert_box').modal({
						show : true,
						keyboard : false
					});
					$('#alert_box_body').text('${param.message}');
				});
			</script>
		</c:when>
		<c:otherwise>
			<script>
				$(document).ready(function() {
					$('#alert_box_info').modal({
						show : true,
						keyboard : false
					});
					$('#alert_box_info_body').text("${param.message}");
				});
			</script>
		</c:otherwise>
	</c:choose>
</s:if>