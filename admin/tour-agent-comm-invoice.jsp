<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<section class="wrapper container-fluid">
	<div class="row">
		<div class="">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.tour_agent_invoice" />${company.companyDetail}
					</h5>
					<div class="set pull-right">
						<button class=" btn btn-success btn-xs "
							onclick="sendCustomerInvoiceToCustomer();">
							<s:text name="tgi.label.send_invoice" />
							<span class="glyphicon glyphicon-send"></span>
						</button>


						<button type="button" class="btn btn-primary but no-print" id="h4"
							style="display: none">
							<s:text name="tgi.label.sending" />
							<i class="fa fa-arrow-circle-right"></i>
						</button>
					</div>
					<div class="set pull-right">
						<a href="<%=request.getContextPath()%>/tourAgentInvoice?orderId=${invoiceData.yourRef}" target="blank" class="popup btn btn-info btn-xs">
						<span class="pull-right">
						<i class="fa fa-print" aria-hidden="true"></i> Print Invoice
						</span></a>
					</div> 
					<div class="set pull-right">

						<a class="btn btn btn-primary btn-xs"
							href="tourAgentCommInvoiceList"><span class="pull-right">
								<span class="glyphicon glyphicon-step-backward"></span> <s:text
									name="tgi.label.back" />
						</span></a>

					</div>
				</div> 
				   
				<div class="col-md-12 mt-2"> 
				<div class="profile-timeline-card">   
				<div class="row row-minus">			
					<div class="col-md-12">
				           <table class="table">
				         <tbody>
				          <tr>
                         	<td style="border-top: 0px solid #dddddd;">
                             <p>&nbsp;</p> <img src="https://i.imgur.com/kg7Lvd4.png" width="200" height="43" style="margin-left: 10px;"/>
                             <p>&nbsp;</p>
                             </td>
                             <td style="text-align: right;border-top: 0px solid #dddddd;">
                             <p>&nbsp;</p>
                             <p style="">
                                 <b> <span>
									${superCompanyVo.companyName}</span></b> <br />
									                                     <span style="">${superCompanyVo.address}</span><br /> <span style=""><b>State :</b> ${superCompanyVo.state != null && superCompanyVo.state != ''?superCompanyVo.state:'...'},${superCompanyVo.state != null && superCompanyVo.state != ''?superCompanyVo.zipCode:'...'}</span><br />
									<b>Phone :</b> <span style="">
									<span>${superCompanyVo.phone != null && superCompanyVo.phone != ''?superCompanyVo.phone :'N/A'}</span>
									</span><br /> 
									<b>Email :</b> <span style="">
									${superCompanyVo.email != null && superCompanyVo.email != ''?superCompanyVo.email:''}
									</span>
									<br />
									<b>Website :</b> <span style="">
									${superCompanyVo.website != null && superCompanyVo.website != '' ?superCompanyVo.website : 'N/A'}
									</span>
                             </p>
                         </td>
                     </tr>
				          </tbody>
				        </table>
				     </div> 
				     </div>
						<hr style="border-top: 3px solid #eeeeee;">
						<div class="timeline-footer row row-minus"> 
							<div class="col-md-3"> 
							<h4 class="text-left"><span>Bill To</span></h4>
							           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
							         <tbody>
							          <tr>
							            <td> <b>Address</b></td>
							            <td> ${companyVo.address}</td>
							          </tr>
							          <tr>
							            <td><b>Tel</b></td>
							            <td>${companyVo.phone}</td>
							          </tr> 
							          <tr>
							            <td><b>Email</b></td>
							            <td>${companyVo.email}</td>
							          </tr> 
							          <tr>
							            <td><b>Website</b></td>
							            <td>${companyVo.website != null && companyVo.website != '' ? companyVo.website :'N/A'}</td>
							          </tr>
							          </tbody>
							        </table>
							  </div>
							  <div class="col-md-6">&nbsp;</div> 
							     <%-- <s:if test="invoiceData.userDetails.size>0">
								<s:iterator value="invoiceData.userDetails">
								<c:if test="${invoiceData.company!=null}">
							     <div class="col-md-4">
							     <h4 class="text-left"><span><s:text name="tgi.label.to" /></span></h4>
							           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
							         <tbody>
							          <tr>
							            <td> <b><s:text name="tgi.label.to" /></b></td>
							            <td>${invoiceData.company.companyName}</td>
							          </tr>
							          <tr>
							            <td><b><s:text name="tgi.label.address" /></b></td>
							            <td>${invoiceData.company.companyDetail.address}</td>
							          </tr> 
							          <tr>
							            <td><b><s:text name="tgi.label.tel" /></b></td>
							            <td>${invoiceData.company.mobile}</td>
							          </tr> 
							          <tr>
							            <td><b><s:text name="tgi.label.email" /></b></td>
							            <td>${invoiceData.company.email}</td>
							          </tr>
							          </tbody>
							        </table>
							     </div> 
							     </c:if>	
								</s:iterator>
								</s:if>  --%>
							     <div class="col-md-3">
							     <h4 class="text-left"><span><s:text name="tgi.label.tax_invoice" /></span></h4>
							           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
							         <tbody>
							          <tr>
							            <td> <b><s:text name="tgi.label.invoice_no" /></b></td>
							            <td> <s:property  value="invoiceData.invNo"/></td>
							          </tr>
							          <tr>
							            <td><b><s:text name="tgi.label.consultant" /></b></td>
							            <td><s:property  value="invoiceData.consultant"/></td>
							          </tr> 
							          <tr>
							            <td><b><s:text name="tgi.label.your_ref" /></b></td>
							            <td><s:property  value="invoiceData.yourRef"/></td>
							          </tr> 
							          <tr>
							            <td><b><s:text name="tgi.label.date" /></b></td>
							            <td><s:property  value="invoiceData.date"/></td>
							          </tr>
							          </tbody>
							        </table>
							     </div>
						</div>
					</div>
					</div> 
			</div>
		</div>
	</div>
	<!-- table-responsive -->
</section>



<section class="col-md-12">
					<div class="profile-timeline-card">
						<div class="">

							<div id="invoice" class="clearfix mt-0">
								<div class="row"> 
									<div class="">
										<table
											class="table table-bordered in-table-border no-table-css">
											<thead>
												<tr class="info">
													<th><h4>
															S.No
														</h4></th>
													<th><h4>
															<s:text
																name="tgi.label.tour_details_and_guest_particulars" />
														</h4></th>
													<th><h4>
															<s:text name="tgi.label.qty" />
														</h4></th>
													<th><h4>
															<s:text name="tgi.label.price" />
														</h4></th>
													<th><h4>
															<s:text name="tgi.label.total" />
															(USD)
														</h4></th>
												</tr>
											</thead>
											<tbody>
													<tr>
														<td>1</td>
														<td><s:property value="invoiceData.orderCustomer.firstName" /> <s:property value="invoiceData.orderCustomer.lastName" />
														</td>
														<td>1</td>
														<td></td>
														<td></td>
													</tr>
												<tr>
													<td>2</td>
													<td>
														<p>${invoiceData.tourName}</p>
														<p>${invoiceData.origin} -- ${invoiceData.destination}</p>
													</td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td>3</td>
													<td></td>
													<td></td>
													<td>Total Before Markup</td>
													<td><s:property value="invoiceData.apiPrice" /></td>
												</tr>
												<tr>
													<td>4</td>
													<td></td>
													<td></td>
													<td>Self Markup</td>
													<td><s:property value="invoiceData.markup" /></td>
												</tr>
												<s:if test="logedInCompany == 'superuser'">
												<c:if test="${invoiceData.distributorMarkup!=null}">
													<tr>
														<td>5</td>
														<td>Wholeseller Markup</td>
														<td>1</td>
														<td></td>
														<td><s:property value="invoiceData.distributorMarkup" /></td>
													</tr>
												</c:if>	
												</s:if>
												<s:if test="logedInCompany != 'agent'">
												<c:if test="${invoiceData.childMarkup!=null}">
													<tr>
														<td>6</td>
														<td></td>
														<td></td>
														<td>Agent Markup</td>
														<td><s:property value="invoiceData.childMarkup" /></td>
													</tr>
												</c:if>	
												</s:if>
												<tr>
													<td>7</td>
													<td></td>
													<td></td>
													<td>Total After Markup</td>
													<td><s:property value="invoiceData.totalAfterMarkup" /></td>
												</tr>
												<tr>
										            <td>8</td>
										            <td> </td>
										            <td> </td>
										             <td> <span class="text-success"><s:text name="tgi.label.discount_amount" /></span> </td>
										              <td><span class="text-success">- ${invoiceData.discountAmount}</span></td>
										          </tr>
										          <tr>
											            <td></td>
											            <td>  </td>
											            <td> </td>
											             <td>Net Amount<span> </span> </td>
											              <td><s:property  value="invoiceData.netAmount"/></td>
											          </tr>
												
												<tr>
													<td>8</td>
													<td></td>
													<td></td>
													<td>Tax</td>
													<td><s:property value="invoiceData.tax" /></td>
												</tr>
												<tr>
													<td><s:text name="tgi.label.sr" /></td>
													<td></td>
													<td></td>
													<td><s:text name="tgi.label.processing_fees" /></td>
													<td><s:property value="invoiceData.processingFees" /></td>

												</tr>
												 <tr>
													<td><s:text name="tgi.label.sr" /></td>
													<td></td>
													<td></td>
													<td><s:text name="tgi.label.insurance_fee" /></td>
													<td>
									              	<c:choose>
													<c:when test="${invoiceData.tourOrderRow.tourOrderRowInsurance.price != null}">
													${invoiceData.tourOrderRow.tourOrderRowInsurance.price}
													</c:when>
													<c:otherwise>
													<s:text name="tgi.label.n_a" />
													</c:otherwise>
													</c:choose>
									              </td>
												</tr>
												<tr>
													<th></th>
													<th></th>
													<th><s:text name="tgi.label.calculation" /></th>
													<th></th>
													<th></th>
												</tr>

												<tr>
													<td>
														<!-- 6% -->
													</td>
													<td>
														<!-- SR -->
													</td>
													<td>
													</td>
													<td><s:text name="tgi.label.booking_amount" /></td>
													<td colspan="4">${invoiceData.price}<br> 
													</td>

												</tr>
												<tr>
													<td>
													</td>
													<td>
													</td>
													<td>
													</td>
													<td><b>Total Pay Amount</b></td>
													<td colspan="4"><b>${invoiceData.totalAmount}</b><br>
													</td>
													</tr>
													<tr>
													<th></th>
													<th></th>
													<th>Commision Details</th>
													<th></th>
													<th></th>
												</tr>

												<c:if test="${invoiceData.myCommission!=null}">
													<tr>
														<td>9</td>
														<td>My Commision</td>
														<td>1</td>
														<td><s:property value="invoiceData.myCommission" /></td>
														<td><s:property value="invoiceData.myCommission" /></td>
													</tr>
												</c:if>
												<s:if test="logedInCompany != 'agent'">
													<c:if test="${invoiceData.sharedCommission!=null}">
														<tr>
															<td>10</td>
															<td>Shared Commision</td>
															<td></td>
															<td><s:property value="invoiceData.sharedCommission" /></td>
															<td><s:property value="invoiceData.sharedCommission" /></td>
														</tr>
													</c:if>
												</s:if>
												<tr>
													<td>11</td>
													<td></td>
													<td></td>
													<td>My Revenue</td>
													<td><s:property value="invoiceData.myProfit" /></td>
												</tr>
												
												<tr>
														<td>12</td>
														<td></td>
														<td></td>
														<td><b>Total Price After My Revenue</b></td>
														<td><span class="text-success"><b><s:property value="invoiceData.totalPriceAfterMyRevenue" /></b></span></td>
													</tr> 
											</tbody>
										</table>
									</div>

								<c:if test="${invoiceData.tourOrderRow.tourOrderRowInsurance != null}">		    
    							<div class="clearfix">
								<div class="payment-in">
									<div class="panel panel-info">
									<c:choose>
									<c:when test="${invoiceData.tourOrderRow.tourOrderRowInsurance != null}">
											<h4>
												<s:text name="tgi.label.flight_insurance" />
											</h4>
											<!-- <div class="panel-body"> -->
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="info">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
										<tr>
											<td data-title="><s:text name="tgi.label.insurance_id" />">${invoiceData.tourOrderRow.tourOrderRowInsurance.insuranceId}</td>
											<td data-title="<s:text name="tgi.label.name" />">${invoiceData.tourOrderRow.tourOrderRowInsurance.name}</td>
											<td data-title="<s:text name="tgi.label.price" />">${invoiceData.tourOrderRow.tourOrderRowInsurance.price}</td>
											<td data-title="<s:text name="tgi.label.description" />">${invoiceData.tourOrderRow.tourOrderRowInsurance.description}</td>
											
										</tr>
									</table>
									</c:when>
									<c:otherwise>
											<h4 class="pl-2">
												<s:text name="tgi.label.flight_insurance" />
											</h4>
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="info">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.insurance_not_available" /></td>
												</tr>
											</table>
											</c:otherwise>
										</c:choose>
										</div>
										</div></div>
									</c:if>
										

									<div class="clearfix">
										<div class="payment-in">
											<div class="panel panel-info">
												<h4 class="pl-2">
													<s:text name="tgi.label.payment_details" />
												</h4>

												<table
													class="table table-bordered in-table-border no-table-css">
													<tr class="info">
														<th><s:text name="tgi.label.receipt" /></th>
														<th><s:text name="tgi.label.date" /></th>
														<th><s:text name="tgi.label.payment_method" /></th>
														<th><s:text name="tgi.label.payable_amount" /></th>
														<th>Payment Status</th>
														<th><s:text name="tgi.label.remark" /></th>
													</tr>
													<s:if test="invoiceData.txDetails.size()>0">
														<s:iterator value="invoiceData.txDetails">
															<tr>
																<td>${transactionId}</td>
																<td>${spysr:formatDate(createdDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy hh:mm')}</td>
																<td>${paymentMethod}</td>
																<td><fmt:formatNumber type="number" pattern="#.00" value="${totalPaidAmount}" /></td>
																<td>${paymentStatus}</td>
																<td>${responseMessage}</td>
															</tr>
														</s:iterator>
													</s:if>
													<s:else>
														<tr>
															<td colspan="5"><s:text
																	name="tgi.label.payment_details_not_available" /></td>
														</tr>
													</s:else>

												</table>

											</div>
											<c:if test="${invoiceData.agentWalletTxDetails.size()>0}">
											<div class="panel panel-info">
												<h4 class="pl-2">
													<s:text name="tgi.label.wallet_details" />
												</h4>
												<table
													class="table table-bordered table-striped-column no-table-css">
													<tr class="info">
														<th><s:text name="tgi.label.created_at" /></th>
														<th><s:text name="tgi.label.action" /></th>
														<th><s:text name="tgi.label.amount" /></th>
														<th><s:text name="tgi.label.openingbal" /></th>
														<th><s:text name="tgi.label.closingbal" /></th>
													</tr>

													<s:if test="invoiceData.agentWalletTxDetails.size()>0">
														<s:iterator value="invoiceData.agentWalletTxDetails">
															<tr>
																<td data-title="<s:text name="tgi.label.created_at" />"><s:property
																		value="invoiceData.convertDate" /></td>

																<td data-title="<s:text name="tgi.label.action" />"><s:property
																		value="invoiceData.action" /></td>

																<td data-title="<s:text name="tgi.label.amount" />"><s:property
																		value="invoiceData.amount" /></td>
																<td data-title="<s:text name="tgi.label.openingbal" />"><s:property
																		value="invoiceData.openingBalance" /></td>
																<td data-title="<s:text name="tgi.label.closingbal" />"><s:property
																		value="invoiceData.closingBalance" /></td>
																<%-- <td><s:property  value="value.currency"/></td> --%>
															</tr>
														</s:iterator>
													</s:if>
													<s:else>
														<tr>
															<td colspan="5"><s:text
																	name="tgi.label.wallet_details_not_available" /></td>
														</tr>
													</s:else>
												</table>

											</div>
											</c:if>
										</div>
									</div>
								</div>

								<div class="clearfix signature-lint no-table-css">

									<div class="col-sm-6 ">
										<h4>
											<s:text name="tgi.label.recived_by" />
										</h4>
										<br> <br>
										<p>
											<span style="border-top: 1px solid #adadad; padding: 5px;"><s:text
													name="tgi.label.customers_signature" /> &amp; Chop</span>
										</p>
									</div>

									<div class="col-sm-6 pull-right">
										<div class="pull-right">
											<h4>
												TGI
											</h4>
											<br> <br>
											<p>
												<span style="border-top: 1px solid #adadad; padding: 5px;"><s:text name="tgi.label.autorised_signature" /></span>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div id="editor"></div>

					</div>
				</section>
<!-- /.content -->
<script type="text/javascript">
	function sendCustomerInvoiceToCustomer() {
		var companyType = $("#companyType").val();
		console.log("companyType..." + companyType);
		var orderId = $("#orderId").val();
		console.log("orderId..." + orderId);
		//var invoice = "<html><body style='border:2px solid;padding:10px 10px 10px 10px'>"+$("#invoice").html()+"</body></html>";
		/*   var htmlMessage=$('#invoice').html(); */
		//console.log("--htmlMessage..."+invoice);
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "sendHotelInvoiceToMail";
		//console.log("finalUrl..."+finalUrl);
		$('#h4').show();
		$.ajax({
			method : "POST",
			url : finalUrl,
			data : {
				orderId : $("#orderId").val(),
				companyType : $("#companyType").val()
			},
			success : function(data, status) {
				$.each(data, function(index, element) {
					console.log("data-------" + element.status);

					if (element.status == "success") {
						$('#h4').hide();
						$('#success-alert').show();
						$('#message').text("Successfully sent mail.");
						$('#success').click(function() {
							$('#success-alert').hide();
							window.location.assign($(location).attr('href'));
						});

					} else if (element.status == "fail") {
						$('#h4').hide();
						$('#success-alert').show();
						$('#message').text("Failed.Try again.");
						$('#success').click(function() {
							$('#success-alert').hide();

						});
					}

				});

			},
			error : function(xhr, status, error) {
				$('#h4').hide();
				$('#success-alert').show();
				$('#message').text(error);
				$('#success').click(function() {
					$('#success-alert').hide();
				});
				console.log("Error----------" + error);
			}
		});
	}
</script>
