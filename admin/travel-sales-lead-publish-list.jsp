<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
@media screen and (max-width:480px){
.detail-btn {
	margin-top: -27px;
    margin-left: 15px;
}
}
.center {
	text-align: center;
}

.btn-xsm, .btn-group-xs>.btn {
	padding: 2px 6px;
	font-size: 13px;
	line-height: 1.5;
	border-radius: 2px;
}

a.collapsed {
	color: #02214c;
	text-decoration: none !important;
}

a.collapsed:hover {
	color: #337ab7;
	text-decoration: none !important;
}

.highlight {
	background-color: #F44336;
}

.highlight-light {
	background-color: #ff6559;
}

.line-btn {
	padding: 5px 0 0 5px;
	font-size: 20px;
	color: #9E9E9E;
}

.clippy {
	margin-bottom: 3px;
}

span.text-spysr:hover {
	color: #3a7df9;
}
</style>

   <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="">
                        <div class="card1">
                        <div class="pnl">
                        <div class="hd clearfix">
					<h5 class="pull-left">Buy Leads</h5>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="#collapseFive" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text">Show</span> Filter Bar <i class="fa fa-angle-up"></i></span></a>
					</div>
				</div>
                                 <br>
                         		<form action="listTravelSalesLeadReceive" class="filter-form" id="resetform">
								<div class="" id="filterDiv" style="display: none;">
								<div class="form-group">
								<div class="col-md-2">
									<div class="form-group">
										<select name="createdBy" id="created-by" class="form-control input-sm input-sm">
											<option  selected="selected">Select Created By</option>
											<option value="all"><s:text name="tgi.label.all" /></option>
											<option value="my"><s:text name="tgi.label.ibe" /></option>
											<option value="distributor"><s:text name="tgi.label.admin" /></option>
										</select>
									</div>
								</div>
								<div class="col-md-2">
							   <div class="form-group">
								<input type="text" name="bookingStartDate" id="bookingStartDate" placeholder="Created Start Date...." class="form-control input-sm search-query date1 " />
								 </div></div>
							   
							   <div class="col-md-2">
							   <div class="form-group">
								<input type="text" name="firstName" id="name-json" placeholder="Search By Name" class="form-control input-sm search-query" />
							   </div>
							   </div>
								<div class="col-md-2">
								<div class="form-group">
								<input type="text" name="email" id="email" placeholder="Search By Email" class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2">
								<div class="form-group">
								<input type="text" name="phone" id="phone" placeholder="Search By Mobile" class="form-control input-sm search-query" />
								</div></div>
							   <div class="col-md-2">
							   <div class="form-group">
								<input type="text" name="city" id="city-json" placeholder="Search By City" class="form-control input-sm search-query" />
								</div>
								</div>
							   <div class="col-md-2">
							   <div class="form-group">
								<select name="country" id="country" class="form-control usst input-sm" data-live-search="true">
								    <option value="">Select Country</option>
									<c:forEach items="${countryList}" var="country">
										<option value="${country.countryCode}" >${country.countryName}</option>
									</c:forEach>
								</select> 
								</div>
								</div>
								<div class="col-md-2">
							   <div class="form-group">
								<input type="text" name="departureDateFlag" id="country-json" placeholder="Search By Departure Date..." class="form-control input-sm search-query" />
								 </div></div>
							   <div class="col-md-2">
							   <div class="form-group">
								<select aria-hidden="true" name="leadSource" id="leadSource" class="form-control input-sm">
								    <option value="">Lead Source*</option>
									<option value="web">Website</option>
									<option value="socal">Socal Network</option>
									<option value="walkin">Walk-In</option>
									<option value="telephone">Telephone</option>
									<option value="b2b">B2B</option>
									<option value="justdial">Justdial</option>
									<option value="other">Others</option>
								</select>
								 </div></div>
							   <div class="col-md-2">
							   <div class="form-group">
								<select aria-hidden="true" name="callTime" id="callTime" class="form-control input-sm">
								  <option value="" selected="SELECTED">Best time to call</option>
									<option value="morning">Morning</option>
									<option value="afternoon">Afternoon</option>
									<option value="evening">Evening</option>
									<option value="anytime">Any time</option>
								</select>
								 </div></div>
							   <div class="col-md-2">
							   <div class="form-group">
								<select name="terminate" id="terminate" class="form-control input-sm">
								<option value="" selected="selected">Lead Terminate</option>
								<option value="1"><s:text name="tgi.label.yes" /></option>
                                <option value="0" ><s:text name="tgi.label.no" /></option>
                                </select>
								 </div></div>
							<div class="row">
							 <div class=""></div>
							   <div class="col-md-1">
								<div class="form-group">
								<button type="reset" class="btn btn-danger btn-sm" id="configreset">&nbsp;Clear&nbsp;</button>
							</div>
							</div>
								<div class="col-md-1">
								<div class="form-group">
								<button type="submit" class="btn btn-info btn-sm" value="Search" >Search</button>
							</div>
							</div>
							<input type="hidden" name="filterFlag" id="filterFlag">
							</div>
						</div></div>
				</form>
                            
                                   
<div class="dash-table">
			<div class="content">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"  role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
                                        <thead>
                                        <tr class="table-sub-header">
                                        	<th colspan="16" class="noborder-xs">
                                        	<div class="pull-right" style="padding:4px;margin-top: -6px;margin-bottom: -10px;">   
                                        	<div class="btn-group"> 
											<div class="dropdown pull-left" style="margin-right:5px;">
											  <a class="btn btn-sm btn-default " href="#" >&nbsp;Import&nbsp;</a> 
											</div>
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a  class="btn btn-sm btn-default filterBtn" data-toggle="dropdown"  style="margin-bottom: 2px;margin-right: 3px;" title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadAssignDetailList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px;margin-right: 3px;">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Reject</span>
											  </button>  
											</div>
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadAssignDetailList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
										<tr class="border-radius border-color" role="row">
                                                   	<th></th>
	                                                   <th style="width: 50px;" data-priority="1">
																<div class="center">
																<div data-toggle="tooltip" data-placement="top" title="Select All">
																<div class="checkbox checkbox-default">
											                        <input id="master_check" data-action="all" type="checkbox" >
											                        <label for="master_check">&nbsp;</label>
											                    </div>
											                    </div>
											                    </div>
													  </th>
                                                      <th data-priority="2">Lead Number</th>
                                                      <th data-priority="3">Full Name</th>
                                                      <th data-priority="3">Phone</th>
                                                      <th data-priority="3">Email</th>
                                                      <th data-priority="4">Title</th>
                                                      <th class="col-md-1">Location</th>
                                                      <th class="col-md-1">People</th>
                                                      <th class="col-md-1">Start Date</th>
                                                      <th class="col-md-1">End Date</th>
                                                      <th class="col-md-1">Call Time</th>
                                                      <th class="col-md-1">Lead Source</th>
                                                     <th></th>
                                                     <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
   											<s:if test="travelSalesLeadAssignDetailList.size > 0">
                                           <s:iterator value="travelSalesLeadAssignDetailList" status="rowstatus">
                                                <tr>
                                                <td scope="row" data-title="S.No"><div class="center"><s:property value="%{#rowstatus.count}" /></div></td>
                                                <td data-title="Select" class="select-checkbox">
														<div class="center">
														<div class="checkbox checkbox-default">
								                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${travelSalesLead.id}" class="check_row">
								                        <label for="checkbox1_sd_${rowstatus.count}"></label>
								                   		</div>
								                   		</div>
														</td>
                                                    
                                                    <td data-title="Company Name"><span class="">
                                                    ${travelSalesLead.leadNumber}
                                                    </span>
                                                    </td>
                                                    <td><span class="">
                                                    <a href="#">${travelSalesLead.firstName} ${travelSalesLead.lastName}</a>
                                                    </span>
                                                    </td>
                                                    <td><span class="">
                                                    ${travelSalesLead.phone}
                                                    </span>
                                                    </td>
                                                    <td><span class="">
                                                    ${travelSalesLead.email}
                                                    </span>
                                                    </td>
                                                    <td data-title="Company Name"><span class="">
                                                    ${travelSalesLead.title}
                                                    </span>
                                                    </td>
                                                    <td data-title="Email">${travelSalesLead.destinationTo}</td>
                                                    <td data-title="Email">${travelSalesLead.adult}</td>
                                                    <td data-title="Start Date">
													N/A                                          
                                                    </td>
                                                    <td data-title="End Date">
                                                    N/A
                                                    </td>
                                                    <td data-title="Call Time" class="text-capitalize">${travelSalesLead.callTime}</td>
                                                    <td data-title="Lead Source" class="text-capitalize">${travelSalesLead.leadSource}</td>
                                        	        <td data-title="Action">
													<div class="dropdown pull-right" style="    margin-right: 20px;">
													  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
													  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
													  <ul class="dropdown-menu">
													    <li class="action"><a href="#" data-lead-id="${travelSalesLead.id}" data-lead-assign-id="${id}" class="buy_travel_sales_leadBtn">Buy Now</a></li>
													    <li class="action"><a href="#">Reject</a></li>
													    <li class="action">
													    </li>
													  </ul>
													</div>
													</td>  
													<td></td>
														 </tr>
                                                </s:iterator></s:if>
                                            </tbody>
											
                                        </table></div></div></div></div>
										</div>
                                    </div>
<!--##################### Form for make a change owner ###################################-->
		<div class="modal fade" id="change_owner" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title text-center">Change Lead Owner</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="20" alt="Copy to clipboard">
								</button>
							</div>
							<form id="addUserSkillsForm" action="createDuplicateTour" method="post" class="bv-form">
								<div class="modal-body">
								 <div class="row">
											<div class="col-md-12 mt-20">
												<div class="controls">
														<div class="form-group">
															<input type="text" id="tour_number" value="" class="form-control"  placeholder="Search People"/>
															<input type="hidden" name="ownerId" id="owner_id" value="" />
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="checkbox">
							                        <input id="checkbox1" type="checkbox" name="" value="false">
							                        <label for="checkbox1">&nbsp;Send notification email </label>
							                    </div>
						                    </div>
						                    <div class="col-md-12 mt-30">
						                    <p>The <strong>new owner</strong> will also become the owner of these records related to <strong><span class="is-owner"></span></strong> that are owned by you.</p>
						                    </div>
										</div>
							</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-success" id="changeOwnerSubmitBtn">Save</button>
								</div>
							</form>
						</div>

					</div>
				</div>
<!--##################### Form for make a sell lead owner ###################################-->
                            </div>
            </section>
        <!--ADMIN AREA ENDS-->
        

<!--AutoCompleteter  -->
<script>
/*travel sales lead buy now method*/
  	$(document).ready(function() {
		$(".buy_travel_sales_leadBtn").click(function(e) {

			var leadId = $(this).attr("data-lead-id");
			var leadAssignId = $(this).attr("data-lead-assign-id");
			
			var urlData = "travelLeadId="+leadId+"&"+"id="+leadAssignId;
			$.ajax({
				url : "buyTravelSalesLead?"+urlData,
				type : "POST",
				dataType : 'json',
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						alertify.success(jsonData.message.message);}
	  				else if(jsonData.message.status == 'input'){
	  					alertify.error(jsonData.message.message);}
	  				else if(jsonData.message.status == 'error'){
	  					alertify.error(jsonData.message.message);}
					
					//notify js
				},
				error : function(request, status, error) {
					showModalPopUp("Sales Lead   can not be buy,please try again.", "e");
				}
			});
		});

	});
 
var CName={url:"getTravelSalesLeadJson.action?data=name",getValue:"name",list:{match:{enabled:!0}}};$("#name-json").easyAutocomplete(CName);
var Email={url:"getTravelSalesLeadJson.action?data=email",getValue:"email",list:{match:{enabled:!0}}};$("#email").easyAutocomplete(Email);
var Phone={url:"getTravelSalesLeadJson.action?data=phone",getValue:"phone",list:{match:{enabled:!0}}};$("#phone").easyAutocomplete(Phone);
var City={url:"getTravelSalesLeadJson.action?data=city",getValue:"city",list:{match:{enabled:!0}}};$("#city-json").easyAutocomplete(City);
var DFrom={url:"getTravelSalesLeadJson.action?data=destinationFrom",getValue:"destinationFrom",list:{match:{enabled:!0}}};$("#destination-from").easyAutocomplete(DFrom);
var DTo={url:"getTravelSalesLeadJson.action?data=destinationTo",getValue:"destinationTo",list:{match:{enabled:!0}}};$("#destination-to").easyAutocomplete(DTo);
			
	</script>
	<script type="text/javascript" class="init">
	$(document).ready(function() {
		$('.filterBtn').click(function() {
			$('#filterDiv').toggle(500);
		});
		$('div.easy-autocomplete').removeAttr('style');
		
		$('#configreset').click(function(){
            $('#resetform')[0].reset(); 
       });
	});

	$('.filter-link').click(function() {
		var collapsed = $(this).find('i').hasClass('fa-angle-up');

		$('.filter-link').find('i').removeClass('fa-angle-down');

		$('.filter-link').find('i').addClass('fa-angle-up');
		if (collapsed)
			$(this).find('i').toggleClass('fa-angle-up fa-2x fa-angle-down fa-2x')
	});
</script>
<script>
/*--------------------------script for delete tour prices multiple-----------------------------*/
$('#delete_all_price').on("click", function(event){
	// confirm dialog
	 var allVals = [];
	 $(".check_row:checked").each(function() {  
		 allVals.push($(this).attr('data-id'));
	 });  
		 var check = confirm("Are you sure you want to delete selected data?");
		 if(check == true)
		 {
			 var join_selected_values = []; 
			 join_selected_values = allVals.join(",");
			 $.ajax({
						url : "archiveMultipleTravelLead?travelLeadIds="+join_selected_values,
						type : "POST",
						dataType: 'html',
						success : function(data) {
							alertify.success("You've clicked OK Your data have successfully deleted");
							setTimeout("window.location.reload();", 1000);
						},
						error: function (request, status, error) {
							showModalPopUp("Lead can not be deleted.","e");
						}
				}); 
		 }
		 else
		 {
			 return false;	 
		 }
});
</script>
<script type="text/javascript" src="admin/js/admin/multiple-archive.js"></script>