<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<title><s:property value="%{#session.ReportData.agencyUsername}" /></title>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->

	<s:if test="hasActionErrors()">
		<div class="succfully-updated clearfix" id="error-alert">

			<div class="col-sm-2">
				<i class="fa fa-check fa-3x"></i>
			</div>
			<div class="col-sm-10">
				<p>
					<s:actionerror />
				</p>
				<button type="button" id="cancel" class="btn btn-primary">
					&nbsp;
					<s:text name="tgi.label.ok" />
				</button>
			</div>
		</div>
	</s:if>
	<s:if test="hasActionMessages()">
		<div class="sccuss-full-updated" id="success-alert">
			<div class="succfully-updated clearfix">
				<div class="col-sm-2">
					<i class="fa fa-check fa-3x"></i>
				</div>
				<div class="col-sm-10">
					<s:actionmessage />
					<button type="button" id="success" class="btn btn-primary">
						&nbsp;
						<s:text name="tgi.label.ok" />
					</button>
				</div>
			</div>
		</div>
	</s:if>
</div>

<!-- Main content -->
<section class="wrapper container-fluid">
	<div class="row">
		<div class="">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						&nbsp;
						<s:text name="tgi.label.flight" />
						<s:text name="tgi.label.passenger_order_setails" />
					</h5>
					<div class="set pull-right">

						<a class="btn btn-success btn-xs" href="flightOrderList"><span
							class="pull-right"> <span
								class="glyphicon glyphicon-step-backward"></span>
							<s:text name="tgi.label.back" /></span></a>
					</div>
				</div>

				<section class="col-md-12">
					<!-- Small boxes (Stat box) -->

					<%-- <div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><s:text name="tgi.label.tour_info" /></span>&nbsp;<s:text name="tgi.label.flight_info" /></b>
									</div></div><br> --%>
					<div class="row">
						<div class="profile-timeline-card">
							<p class="pt-0 pb-2 mb-2">
								<span class="company-font-icon"><i class="fa fa-plane"
									aria-hidden="true"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.booking_summary" /></b></span>
							</p>
							<div class="table-responsive no-table-css clearfix">
								<table
									class="table table-bordered table-striped-column table-hover">
									<thead>
										<tr class="border-radius border-color">
											<th><s:text name="tgi.label.booking_date" /></th>
											<th><s:text name="tgi.label.pnr" /></th>
											<th><s:text name="tgi.label.status" /></th>
											<th><s:text name="tgi.label.supplier" /></th>
											<th><s:text name="tgi.label.created_by" /></th>
											<th><s:text name="tgi.label.airline" /></th>
										</tr>
									</thead>
									<tr>
									<td data-title="Booked"><s:property
											value="ReportData.bookingDate" />
										<td data-title="<s:text name="tgi.label.pnr" />"><s:property
												value="ReportData.pnr" /></td>
										<td data-title="<s:text name="tgi.label.status" />"><s:property
												value="ReportData.status" /></td>
										<td data-title="<s:text name="tgi.label.supplier" />"><s:property
												value="ReportData.apiProvider" /></td>
										<td data-title="<s:text name="tgi.label.created_by" />"><s:property
												value="ReportData.createdBy" /></td>
										<td data-title="Airline"><s:property
											value="ReportData.airline" /></td>				
									</tr>
								</table>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="profile-timeline-card">
							<p class="pt-0 pb-2 mb-2">
								<span class="company-font-icon"><i class="fa fa-plane"
									aria-hidden="true"></i></span> <span> <b class="blue-grey-text">Flight
										Info</b></span>
							</p>

							<div class="table-responsive no-table-css clearfix">
								<table
									class="table table-bordered table-striped-column table-hover">
									<thead>
										<tr class="border-radius border-color">
											<th><s:text name="tgi.label.s.no" /></th>
											<th><s:text name="tgi.label.airline" /></th>
											<th><s:text name="tgi.label.flight_no" /></th>
											<!-- <th>Airline_PNR</th> -->
											<th><s:text name="tgi.label.route" /></th>
											<!--  <th>Via</th> -->
											<th><s:text name="tgi.label.take_off" /></th>
											<th><s:text name="tgi.label.landing" /></th>
											<th><s:text name="tgi.label.class" /></th>
											<th><s:text name="tgi.label.trip_type" /></th>
										</tr>
									</thead>
									<s:iterator value="flightInfo" status="data">
										<tr>
											<td data-title="<s:text name="tgi.label.s.no" />"><s:property
													value="%{#data.count}" /></td>
											<td data-title="<s:text name="tgi.label.airline" />"><s:property
													value="ReportData.airline" /></td>
											<td data-title="<s:text name="tgi.label.flight_no" />"><s:property
													value="flight_number" /></td>
											<%-- <td> <s:property value="airlinePNR" />  </td>   --%>
											<td data-title="<s:text name="tgi.label.route" />"><s:property
													value="route" /></td>
											<td data-title="<s:text name="tgi.label.take_off" />"><s:property
													value="take_off" /></td>
											<td data-title="<s:text name="tgi.label.landing" />"><s:property
													value="landing" /></td>
											<td data-title="<s:text name="tgi.label.class" />"><s:property
													value="_class" /></td>
											<td data-title="<s:text name="tgi.label.trip_type" />"><s:property
													value="tripType" /></td>
										</tr>
									</s:iterator>
								</table>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="profile-timeline-card">
							<p class="pt-0 pb-2 mb-2">
								<span class="company-font-icon"><i class="fa fa-user"
									aria-hidden="true"></i></span> <span> <b class="blue-grey-text"><s:text
											name="tgi.label.passanger_info" /></b></span>
							</p>

							<div class="table-responsive no-table-css clearfix">
								<table
									class="table table-bordered table-striped-column table-hover">
									<thead>
										<tr class="border-radius border-color">
											<!-- 	<th>Passengers</th> -->
											<th><s:text name="tgi.label.s.no" /></th>
											<th><s:text name="tgi.label.title" /></th>
											<th><s:text name="tgi.label.name" /></th>
											<th><s:text name="tgi.label.surname" /></th>
											<th><s:text name="tgi.label.gender" /></th>
											<th><s:text name="tgi.label.birth" /></th>
											<th><s:text name="tgi.label.passportexp_date" /></th>

										</tr>
									</thead>
									<s:iterator value="passList" status="data">
										<tr>
											<%-- 	<th><s:property value="Passengers" /></th> --%>
											<td data-title="<s:text name="tgi.label.s.no" />"><s:property
													value="%{#data.count}" /></td>
											<td data-title="Title"><s:property value="title" /></td>		
											<td data-title="<s:text name="tgi.label.name" />"><s:property
													value="name" /></td>
											<td data-title="<s:text name="tgi.label.surname" />"><s:property
													value="surname" /></td>
											<td data-title="<s:text name="tgi.label.gender" />"><s:property
													value="gender" /></td>
											<td data-title="<s:text name="tgi.label.birth" />"><s:property
													value="birth" /></td>
											<%-- <th><s:property value="phone" /></th>
																			  <th><s:property value="mobile" /></th> --%>

											<td data-title="<s:text name="tgi.label.passportexp_date" />"><s:property
													value="PassportExpDate" /></td>

											<%--   <th><s:property value="fuel_other" />20.00</th>	 --%>
											<%--  <th><s:property value="servPrice" /></th> --%>
											<%-- <th><s:property value="agentCom" /></th> --%>

										</tr>
									</s:iterator>
								</table>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="profile-timeline-card">
							<p class="pt-0 pb-2 mb-2">
								<span class="company-font-icon"><i class="fa fa-phone"></i></span>
								<span> <b class="blue-grey-text"><s:text
											name="tgi.label.contact_info" /></b></span>
							</p>

							<div class="table-responsive no-table-css clearfix">
								<table
									class="table table-bordered table-striped-column table-hover">
									<thead>
										<tr class="border-radius border-color">
											<!-- 	<th>Passengers</th> -->
											<th><s:text name="tgi.label.title" /></th>
											<th><s:text name="tgi.label.name" /></th>
											<th><s:text name="tgi.label.surname" /></th>
											<%-- <th><s:text name="tgi.label.gender" /></th> --%>
											<th><s:text name="tgi.label.mobile" /></th>
											<th><s:text name="tgi.label.address" /></th>
											<th><span></span>&nbsp;<s:text name="tgi.label.email" /></th>

										</tr>
									</thead>
									<s:iterator status="index">
										<tr>
											<%-- 	<th><s:property value="Passengers" /></th> --%>
											<td data-title="Title">${flightOrderRowData.flightCustomer.title}</td>
											<td data-title="<s:text name="tgi.label.name" />">${flightOrderRowData.flightCustomer.firstName}</td>
											<td data-title="<s:text name="tgi.label.surname" />">${flightOrderRowData.flightCustomer.lastName}</td>
											<%-- <td data-title="<s:text name="tgi.label.gender" />">${flightOrderRowData.flightCustomer.gender}</td> --%>
											<td data-title="<s:text name="tgi.label.mobile" />">${flightOrderRowData.flightCustomer.mobile}</td>
											<td data-title="<s:text name="tgi.label.address" />">${flightOrderRowData.flightCustomer.address}</td>
											<td data-title="Email">${flightOrderRowData.flightCustomer.email}</td>
										</tr>
									</s:iterator>
								</table>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="profile-timeline-card">
							<c:choose>
								<c:when
									test="${flightOrderRowData.flightOrderInsurance != null}">
									<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i
											class="fa fa-user-plus"></i></span> <span> <b
											class="blue-grey-text"><s:text
													name="tgi.label.flight_insurance" /></b></span>
									</p>

									<div class="table-responsive no-table-css clearfix">
										<table
											class="table table-bordered table-striped-column table-hover">
											<thead>
												<tr class="border-radius border-color">
													<th><s:text name="tgi.label.insurance_id" /></th>
													<th><s:text name="tgi.label.name" /></th>
													<th><s:text name="tgi.label.price" /></th>
													<th><s:text name="tgi.label.description" /></th>

												</tr>
											</thead>
											<tbody>
												<%-- <s:iterator value="flightOrderRow.flightOrderInsurance"
										status="data"> --%>
												<tr>
													<td data-title="Origin">${flightOrderRowData.flightOrderInsurance.insuranceId}</td>
													<td data-title="Destination">${flightOrderRowData.flightOrderInsurance.name}</td>
													<td data-title="Departure">${flightOrderRowData.flightOrderInsurance.price}</td>
													<td data-title="Arrival">${flightOrderRowData.flightOrderInsurance.description}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</c:when>
								<c:otherwise>
									<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i
											class="fa fa-user-plus"></i></span> <span> <b
											class="blue-grey-text"><s:text
													name="tgi.label.flight_insurance" /></b></span>
									</p>
									<table
										class="table table-bordered in-table-border no-table-css">
										<tr class="border-radius border-color">
											<th><s:text name="tgi.label.insurance_id" /></th>
											<th><s:text name="tgi.label.name" /></th>
											<th><s:text name="tgi.label.price" /></th>
											<th><s:text name="tgi.label.description" /></th>
										</tr>
										<tr>
											<td colspan="5"><s:text
													name="tgi.label.insurance_not_available" /></td>
										</tr>
									</table>
								</c:otherwise>
							</c:choose>
						</div>
					</div>

					<div class="row">
						<div class="profile-timeline-card">
							<p class="pt-0 pb-2 mb-2">
								<span class="company-font-icon"><i class="fa fa-money"></i></span>
								<span> <b class="blue-grey-text"><s:text
											name="tgi.label.payment_info" /></b></span>
							</p>
							<div class="table-responsive no-table-css clearfix">
								<table
									class="table table-bordered table-striped-column table-hover">
									<thead>
										<tr class="border-radius border-color">
											<th><span></span>&nbsp;<s:text
													name="tgi.label.transaction_id" /></th>
											<th><s:text name="tgi.label.date" /></th>
											<th><span></span>&nbsp;<s:text
													name="tgi.label.processing_fee" /></th>
											<th><span></span>&nbsp;<s:text
													name="tgi.label.payment_type" /></th>
											<th><span></span>&nbsp;<s:text
													name="tgi.label.res_message" /></th>
											<th><span></span>&nbsp;<s:text
													name="tgi.label.payment_status" /></th>
										</tr>
									</thead>
									<s:if test="paymentInfo.size()>0">
										<s:iterator value="paymentInfo" status="data">
											<tr>
												<td data-title="Transaction_id"><s:property
														value="txId" /></td>
												<td data-title="<s:text name="tgi.label.date" />"><s:property
													value="createdAt" /></td>		
												<td data-title="Amount"><s:property value="c_dAmnt" /></td>
												<td data-title="Payment Type"><s:property
														value="paymentMethod" /></td>
												<td data-title="Res Message"><s:property value="resMsg" /></td>
												<td data-title="Payment Status"><s:property
														value="paymentStatus" /></td>
											</tr>

										</s:iterator>
									</s:if>
									<s:else>
										<tr>
											<td data-title="">&nbsp;<s:text name="tgi.label.no_data" /></td>
										</tr>
									</s:else>
								</table>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="profile-timeline-card">
							<p class="pt-0 pb-2 mb-2">
								<span class="company-font-icon"><i class="fa fa-list-alt"></i></span>
								<span> <b class="blue-grey-text"><s:text
											name="tgi.label.order_summary" /></b></span>
							</p>

							<div class="table-responsive no-table-css clearfix">
								<table
									class="table table-bordered table-striped-column table-hover">
									<thead>
										<tr class="border-radius border-color">
											<th>&nbsp;<s:text name="tgi.label.total_passengers" /></th>
											<th><s:text name="tgi.label.tax" /></th>
											<th><s:text name="tgi.label.base_fare" /></th>
											<th><s:text name="tgi.label.processing.fees" /></th>
											<th><s:text name="tgi.label.insurance_fee" /></th>
											<th>&nbsp;<s:text name="tgi.label.final_amount" /></th>
										</tr>
									</thead>
									<s:iterator value="passList">
										<tr>
											<td data-title="Total Passengers"><s:property
													value="ReportData.Passengers" /></td>
											<td data-title="<s:text name="tgi.label.tax" />"><s:property
													value="tax" /></td>
											<td data-title="<s:text name="tgi.label.base_fare" />"><s:property
													value="price" /></td>
											<td data-title="<s:text name="tgi.label.processing.fees" />"><s:property
													value="processingFee" /></td>
											<td data-title="<s:text name="tgi.label.insurance_fee" />">
												<c:choose>
													<c:when
														test="${flightOrderRowData.flightOrderInsurance.price != null}">
											${flightOrderRowData.flightOrderInsurance.price}
											</c:when>
													<c:otherwise>
														<s:text name="tgi.label.n_a" />
													</c:otherwise>
												</c:choose>
											</td>
											<td data-title="Total Amount"><s:property
													value="ReportData.finalPrice" /></td>
										</tr>
									</s:iterator>
								</table>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12 clearfix report-search">
							<%-- <s:if test="ReportData.isCreditNoteIssued()">
						<div class="in-head" style="border: 1px solid black; color: red">
							<h4 class="text-center">
								<span> <s:text name="tgi.label.credit_note_issued_by" /> <s:property
										value="ReportData.staff" /></span> <span><s:text name="tgi.label.credit_note_issued_by" /> <s:property value="ReportData.staff" />
								</span>
							</h4>
						</div>
					</s:if>
 --%>
							<!-- 		<form class="form-inline" action="" method="post"> -->

							<%-- <div class="table-responsive dash-table">

						<!-- testing -->

						<div class="box clearfix">
							<!-- <div class="box-body"> -->

							<label for="Country"><h4>
									<b><s:property value="user" /></b>
								</h4></label>

							<table class="table" class="table table-striped table-bordered">
								 <tr>
									<th colspan="2"><u><s:text name="tgi.label.created_by" /></u> : <s:property
											value="ReportData.createdBy" /></th>
									<th colspan="2"><u><s:text name="tgi.label.pnr" /></u> : <s:property
											value="ReportData.pnr" /></th>
									<th colspan="2"><u><s:text name="tgi.label.status" /></u> : <s:property
											value="ReportData.status" /></th>
									<th colspan="2"><u><s:text name="tgi.label.supplier" /></u> : <s:property
											value="ReportData.apiProvider" /></th>		
											
								</tr>

								 <tr>
									<th colspan="5"><h5>
											<b><u><s:text name="tgi.label.flight_info" /></u></b>
										</h5></th>


								</tr>
							 --%>

							<%-- <tr>
									<th colspan="5"><h5>
											<b><u><s:text name="tgi.label.passenger_info" /></u></b>
										</h5></th>
								</tr>

								<tr>
									<!-- 	<th>Passengers</th> -->
									<th><s:text name="tgi.label.s.no" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="tgi.label.gender" /></th>
									<th><s:text name="tgi.label.birth" /></th>
									<th><s:text name="tgi.label.passportexp_date" /></th>
									<th><s:text name="tgi.label.tax" /></th>
									<th><s:text name="tgi.label.base_fare" /></th>
									<th><s:text name="tgi.label.total" /></th>

									<th><s:text name="tgi.label.currency" /></th>
								</tr>
								<s:iterator value="passList" status="data">
									<tr>
											<th><s:property value="Passengers" /></th>
										<th><s:property value="%{#data.count}" /></th>
										<th><s:property value="name" /></th>
										<th><s:property value="surname" /></th>
										<th><s:property value="gender" /></th>
										<th><s:property value="birth" /></th>
										<th><s:property value="phone" /></th>
																			  <th><s:property value="mobile" /></th>

										<th><s:property value="PassportExpDate" /></th>
										<th><s:property value="tax" /></th>
										<th><s:property value="price" /></th>
										<th><s:property value="total" /></th>
										  <th><s:property value="fuel_other" />20.00</th>	
										 <th><s:property value="servPrice" /></th>
										<th><s:property value="agentCom" /></th>
										<th><s:property value="ReportData.curCode" /></th>
									</tr>
								</s:iterator>
 --%>
							<%-- <tr>
									<th><h5>
											<b><u><s:text name="tgi.label.payment_info" /></u></b>
										</h5></th>


								</tr>
								<tr>
									<th><s:text name="tgi.label.transaction_id" /></th>
									<th><s:text name="tgi.label.amount" /></th>
									<th><s:text name="tgi.label.payment_type" /></th>
									<th><s:text name="tgi.label.res_message" /></th>
									<th><s:text name="tgi.label.payment_status" /></th>
								</tr>
								<s:if test="paymentInfo.size()>0">
									<s:iterator value="paymentInfo" status="data">
										<tr>
											<th><s:property value="txId" /></th>
											<th><s:property value="c_dAmnt" /></th>
											<th><s:property value="paymentMethod" /></th>
											<th><s:property value="resMsg" /></th>
											<th><s:property value="paymentStatus" /></th>
										</tr>

									</s:iterator>
								</s:if>
								<s:else>
									<tr>
										<td><s:text name="tgi.label.no_data" /></td>
									</tr>
								</s:else> --%>
							<%-- <tr>
									<th colspan="5"><h4>
											<b>Total Passengers:<s:property value="ReportData.Passengers" /></b>
										</h4></th>

									<th align="left" colspan="5"><h4>
											<b>Total :<s:property value="ReportData.finalPrice" /> <s:property
													value="ReportData.curCode" /></b>
										</h4></th>
								</tr>
 --%>

							<!-- Credit Note Node start -->
							<%--  <s:if test="%{ReportData.CreditNoteIssued==false}">
										<th>
											<button type="button" id="orderNow" class="btn btn-primary">Alter
												Order Now</button>
										</th>
									 
									</s:if>
									
									<s:if test="%{#session.User.userRoleId.isSuperUser() && ReportData.CreditNoteIssued==false}">
										<th>
									 <a href="generateCreditNote?id=<s:property value="ReportData.id"/>" id="orderNow1"
										 class="btn btn-primary">View Credit Note</a></th>
									 </s:if>
									
									 <s:if test="%{ReportData.CreditNoteIssued==true}">
										<th>
									 <a href="generateCreditNote?id=<s:property value="ReportData.id"/>" id="orderNow1"
										 class="btn btn-primary">View Credit Note</a></th>
									 
									</s:if> --%>

							<!-- Credit Note Node End -->

							<s:if test="%{#session.Company.Companyid==ReportData.companyId}">
								<s:if test="%{ReportData.isCreditNoteIssued()}">
									<th><a
										href="generateCustomerCreditNote?id=<s:property value="ReportData.id"/>&companyId=<s:property value="ReportData.companyId"/>"
										id="orderNow1" class="btn btn-primary">Customer Credit
											Note</a></th>
								</s:if>
							</s:if>

							<%-- <div class="table-responsive no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover">
                                   <thead>
					<tr id="th">
						<th><s:text name="tgi.lable.s.no" /></th>
						<th><s:text name="tgi.label.alter_by" /></th>
						<th><s:text name="tgi.label.date" /></th>
						<th><s:text name="tgi.label.convenience_fees" /></th>
						<th><s:text name="tgi.label.cancellation_fees" /></th>
					</tr></thead>
					<s:if test="creditNoteList!=null">
						<s:iterator value="creditNoteList" status="serial">
							<tr id="td">
								<td><s:property value="%{#serial.count}" /></td>
								<td><s:property value="alterBy" /></td>
								<td><s:property value="convertDate" /></td>
								<td><s:property value="convenienceFees" /></td>
								<td><s:property value="cancellationFees" /></td>
							</tr>

						</s:iterator>
					</s:if>
					<s:else>
						<tr id="td">
							<td colspan="4"><s:text name="tgi.label.no_data" /></td>
						</tr>
					</s:else>
				</table></div>
 --%>
						</div>
					</div>
				</section>
				<!-- /.box -->
			</div>
			<!-- table-responsive -->
			<!-- </form> -->
			<%--  <form action="insertOrderModifiedInfo" method="post">
			<s:if test="hasActionErrors()">
				<div class="success-alert" style="display: none">
					<span class="fa fa-thumbs-o-up fa-1x"></span>
					<s:actionerror />
				</div>
			</s:if>
			<s:if test="hasActionMessages()">
				<div class="success-alert" style="display: none">
					<span class="fa fa-thumbs-o-up fa-1x"></span>
					<s:actionmessage />
				</div>
			</s:if>
			<table id="editOrder"
				style="border: 2px solid #2283E1; display: none"
				class="table table-striped table-bordered">


				<tr>

					<td><h4><s:text name="tgi.label.bookinstatus" /></h4> <s:if
							test="ReportData.isOrderUpdated()">
							<select name="statusAction" id="statusAction" autocomplete="off"
								required>
								<option value="<s:property value="ReportData.status"/>"><s:property
										value="ReportData.status" /></option>
							</select>
						</s:if> <s:else>
							<select name="statusAction" id="statusAction" autocomplete="off"
								required>
								<option selected="selected" value=""><s:text name="tgi.label.select_booking_status" /></option>
								<option value="ticketed"><s:text name="tgi.label.ticketed" /></option>
								<option value="failed"><s:text name="tgi.label.failed" /></option>
								<option value="confirmed"><s:text name="tgi.label.confirmed" /></option>
								<option value="pending"><s:text name="tgi.label.pending" /></option>

							</select>
						</s:else> <input type="hidden" name="flightOrderRowId"
						value="<s:property value="ReportData.id"/>"> <input
						type="hidden" name="beforeStatus"
						value="<s:property value="ReportData.status"/>"> <input
						type="hidden" name="userId"
						value="<s:property value="%{#session.User.id}"/>"> <input
						type="hidden" name="companyId"
						value="<s:property value="%{#session.Company.companyid}"/>">
						<input type="hidden" name="gstAmount"
						value="<s:property value="ReportData.gstOnMarkup"/>"> <input
						type="hidden" name="totalBookingAmount"
						value="<s:property value="ReportData.finalPrice"/>"> <input
						type="hidden" name="updatedBy"
						value="<s:property value="%{#session.User.Username}"/>">
					</td>

					<td><h4><s:text name="tgi.label.paymentstatus" /></h4> <s:if
							test="ReportData.isOrderUpdated()">
							<select name="paymentStatus" id="paymentStatus"
								autocomplete="off" required>
								<option value="<s:property value="ReportData.paymentStatus"/>"><s:property
										value="ReportData.paymentStatus" /></option>

							</select>
						</s:if> <s:else>
							<select name="paymentStatus" id="paymentStatus"
								autocomplete="off" required>
								<option selected="selected" value=""><s:text name="tgi.label.select_payment_status" /></option>
								<option value="success"><s:text name="tgi.label.success" /></option>
								<option value="failed"><s:text name="tgi.label.failed" /></option>
								<option value="pending"><s:text name="tgi.label.pending" /></option>
								<option value="refund"><s:text name="tgi.label.refund" /></option>

							</select>
						</s:else>
					<td><h4><s:text name="tgi.label.conveniencefees" /></h4> <input type="text"
						name="convenienceFees" id="convenienceFees" autocomplete="off"
						required> <input type="hidden" id="flightMarkup"
						autocomplete="off" value="<s:property value="ReportData.markUp"/>">
					</td>
					<td><h4><s:text name="tgi.label.cancellationfees" /></h4> <s:if
							test="ReportData.isOrderUpdated() && ReportData.cancellationFees!=null">
							<input type="text" name="cancellationFees" id="cancellationFees"
								value="<s:property value="ReportData.cancellationFees"/>"
								autocomplete="off" required>
						</s:if> <s:else>
							<input type="text" name="cancellationFees" id="cancellationFees"
								autocomplete="off" required>
						</s:else></td>
					<td>
						<h4><s:text name="tgi.label.empcomments" /></h4> <textarea name="employeeComments"> </textarea>

					</td>

					<td>
						  <input type="hidden" name="apiComments"
						value="<s:property value="ReportData.apiComments"/>">
 						</td>

					<td><h4><s:text name="tgi.label.action" /></h4>
						<button type="submit" class="btn btn-primary"><s:text name="tgi.label.update" /></button></td>
				</tr>

			</table>
		</form>  
		</section>

		<!-- /.content -->
		
		<form action="insertOrderModifiedInfo"  method="post" >
								<s:if test="hasActionErrors()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionerror />
						</div>
					</s:if>
					<s:if test="hasActionMessages()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionmessage />
						</div>
					</s:if>
							<table id="editOrder" style="border: 2px solid #2283E1;display: none"   
								class="table table-striped table-bordered">


								<tr>

									<td><h4><s:text name="tgi.label.bookingstatus" /></h4> 
									 
									<s:if test="ReportData.isOrderUpdated()">
									 <select name="statusAction"
										id="statusAction" autocomplete="off" required>
											 <option value="<s:property value="ReportData.status"/>"><s:property value="ReportData.status"/></option> 
									 </select>
									 </s:if>
									 <s:else>
									  <select name="statusAction"
										id="statusAction" autocomplete="off" required>
											<option selected="selected" value=""><s:text name="tgi.label.select_booking_status" /></option>
											<option value="ticketed"><s:text name="tgi.label.ticketed" /></option> 
											<option value="failed"><s:text name="tgi.label.failed" /></option>
											 <option value="confirmed"><s:text name="tgi.label.confirmed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option> 
											
									</select>
									 </s:else>
									
									
									
									<input type="hidden"  name="flightOrderRowId"  value="<s:property value="ReportData.id"/>">
									<input type="hidden"  name="beforeStatus"  value="<s:property value="ReportData.status"/>">
									<input type="hidden"  name="userId"  value="<s:property value="%{#session.User.id}"/>">
									<input type="hidden"  name="companyId"  value="<s:property value="%{#session.Company.companyid}"/>">
									<input type="hidden"  name="gstAmount"  value="<s:property value="ReportData.gstOnMarkup"/>">
									<input type="hidden"  name="totalBookingAmount"  value="<s:property value="ReportData.finalPrice"/>">
									 <input type="hidden"  name="updatedBy" value="<s:property
									 value="%{#session.User.Username}"/>">
									</td>

									<td><h4><s:text name="tgi.label.paymentstatus" /></h4>
									<s:if test="ReportData.isOrderUpdated()">
									 <select name="paymentStatus"
										id="paymentStatus"    autocomplete="off" required> 
											 <option value="<s:property value="ReportData.paymentStatus"/>"><s:property value="ReportData.paymentStatus"/></option>
											 
									</select>
									 </s:if>
									<s:else>
									 <select name="paymentStatus"
										id="paymentStatus"    autocomplete="off" required> 
											<option selected="selected" value=""><s:text name="tgi.label.select_payment_status" /></option>
											 <option value="success"><s:text name="tgi.label.success" /></option>
											<option value="failed"><s:text name="tgi.label.failed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option>
											<option value="refund"><s:text name="tgi.label.refund" /></option>

									</select> 
									</s:else>
									 <td><h4><s:text name="tgi.label.conveniencefees" /></h4>
									  <input type="text" name="convenienceFees" id="convenienceFees"  autocomplete="off" required> 
									  
									 </td>
										  <td><h4><s:text name="tgi.label.cancellationfees" /></h4> 
										  <s:if test="ReportData.isOrderUpdated() && ReportData.cancellationFees!=null">
									 <input type="text"  name="cancellationFees"
										id="cancellationFees"  value="<s:property value="ReportData.cancellationFees"/>"  autocomplete="off" required> 
									 </s:if>
										<s:else>
									 <input type="text" name="cancellationFees" id="cancellationFees"  autocomplete="off" required>
									  </s:else> 
										 
									  </td>
									 <td>
										<h4><s:text name="tgi.label.empcomments" /></h4> <textarea name="employeeComments"> </textarea>

									</td>

									<td>
										<h4><s:text name="tgi.label.api_comments" /></h4> <textarea  disabled="disabled"><s:property
												value="%{#session.ReportData.apiComments}" /></textarea>

									<input type="hidden" name="apiComments" value="<s:property
												value="ReportData.apiComments"/>">
									
									
									
									</td>

									<td><h4><s:text name="tgi.label.action" /></h4>
										<button type="submit" class="btn btn-primary"><s:text name="tgi.label.update" />
											 </button></td>
								</tr>

							</table>
						</form> --%>
		</div>
	</div>
</section>
<!-- /.content-wrapper -->
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$("#convenienceFees")
								.keyup(
										function() {
											var convenienceFees = Math
													.round(parseFloat($(
															"#convenienceFees")
															.val()) * 100) / 100;
											var flightMarkup = Math
													.round(parseFloat($(
															"#flightMarkup")
															.val()) * 100) / 100;
											if (convenienceFees > flightMarkup) {
												alert("your markup : "
														+ flightMarkup
														+ "\n"
														+ "***convenienceFee must be less than or equal to flight markup***");
												$("#convenienceFees").val("");
											}
										});
					});
</script>


<script type="text/javascript">
	$(document).ready(function() {
		$("#editOrder").hide();

		$("#orderNow").click(function() {
			$("#editOrder").toggle("slow", function() {

			});
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#twodpd2").datepicker({
			dateFormat : "yy-mm-dd"
		});
		$("#twodpd1").datepicker({
			dateFormat : "yy-mm-dd"
		/*  changeMonth: true,
		 changeYear: true */
		});
	});
</script>


<script type="text/javascript">
	$(function() {
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "mailConfigList";
		$('#success').click(function() {
			// window.location.assign(finalUrl); 
			$('#success-alert').hide();

		});
		$('#cancel').click(function() {
			$('#error-alert').hide();

		});
	});
</script>