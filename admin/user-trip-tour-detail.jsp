<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%--  <script src= https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js> </script>
    <script src= https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js> </script> --%>
<link
	href="admin/css/jquery-ui.css"
	rel="stylesheet" type="text/css" />
 
<title><s:property value="%{#session.ReportData.agencyUsername}" /></title>
		<!-- Content Header (Page header) -->
		
		 <s:if test="hasActionErrors()">
						<div class="succfully-updated clearfix" id="error-alert">

							<div class="col-sm-2">
								<i class="fa fa-check fa-3x"></i>
							</div>

							<div class="col-sm-10">

								<p>
									<s:actionerror />
								</p>

								<button type="button" id="cancel" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

							</div>

						</div>


					</s:if>

					<s:if test="hasActionMessages()">
						<div class="sccuss-full-updated" id="success-alert">
							<div class="succfully-updated clearfix">

								<div class="col-sm-2">
									<i class="fa fa-check fa-3x"></i>
								</div>

								<div class="col-sm-10">
									<s:actionmessage />
									<button type="button" id="success" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

								</div>

							</div>
						</div>
					</s:if>
		
		


 <section class="wrapper container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.tour_passenger_order_details" /></h5>
                                   <div class="set pull-right">
									<a href="userTourTripList?tripType=tour" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								Tour Trip
								</button></a>
								</div>
								<div class="set pull-right">
									<a href="userLimoTripList?tripType=limo" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								Limo Trip
								</button></a>
								</div>
								<div class="set pull-right">
									<a href="userCarTripList?tripType=car" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								Car Trip
								</button></a>
								</div>
                                <div class="set pull-right">
									<a href="userHotelTripList?tripType=hotel" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								Hotel Trip
								</button></a>
								</div>
								<div class="set pull-right">
									<a href="userFlightTripList?tripType=flight" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								Flight Trip
								</button></a>
								</div>
								<div class="set pull-right">
									<a href="userAllTripList?tripType=all" >
									<button style="padding:4px;margin:0px" class="btn btn-info">
								All Trip
								</button></a>
								</div>           </div>


		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row" >
				<div class="col-sm-12 clearfix report-search" >
				 <s:if test="ReportData.isCreditNoteIssued()">
				<div class="in-head" style="border:1px solid black ;color:red">
 							<h4 class="text-center" >
									<span><s:text name="tgi.label.credit_note_issued_by" /><s:property
											value="ReportData.staff" /></span>
											<span><s:text name="tgi.label.credit_note_issued_by" /><s:property
											value="ReportData.staff"/></span>
								</h4>
							</div>
			 </s:if>
				
				
				<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><%-- <s:text name="tgi.label.tour_info" /> --%>Tour Info</b>
									</div></div><br>
								<div class=" no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover">
                                   <thead>
                                   <tr class="border-radius border-color">
                                   <th><s:text name="tgi.label.created_by" /></th>
                                   <th><s:text name="tgi.label.pnr" /></th>
                                   <th><s:text name="Booking Date" /></th>
                                   <th><s:text name="tgi.label.status" /></th>
                                   <th><s:text name="tgi.label.supplier" /></th>
									</tr></thead>
									<tr>
									<td data-title="<s:text name="tgi.label.created_by" />">${tourOrderRow.createdBy}</td>
									<td data-title="<s:text name="tgi.label.pnr" />"><p style="color: #ff3100;">${tourOrderRow.orderReference}</p></td>
									<%-- <td data-title="<s:text name="Booking Date" />">${tourOrderRow.bookingDate}</td> --%>
									<td data-title="<s:text name="Booking Date" />">
									<c:if test="${tourOrderRow != null && tourOrderRow.bookingDate != null}">
									${spysr:formatDate(tourOrderRow.bookingDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy')}
									</c:if>
									</td>
									<td data-title="<s:text name="tgi.label.status" />">${tourOrderRow.statusAction}</td>
									<td data-title="<s:text name="tgi.label.supplier" />">${tourOrderRow.providerAPI}</td>
									</tr>	
										</table>
								</div>
								<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><%-- <s:text name="tgi.label.tour_info" /> --%>Tour Departure Details </b>
									</div></div><br>
								
								<div class="no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover">
                                   <thead>
                                   <tr class="border-radius border-color">
									<th><s:text name="tgi.label.start_date" /></th>
									<th><s:text name="tgi.label.end_date" /></th>
									<th><s:text name="tgi.label.destiation_city" /></th>
									<th><s:text name="tgi.label.destination_country" /></th>
								</tr></thead>
									<tr>
									<td data-title="<s:text name="tgi.label.start_date" />">
									<c:if test="${tourOrderRow != null && tourOrderRow.tourStartDate != null}">
									${spysr:formatDate(tourOrderRow.tourStartDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy hh:mm')}
									</c:if>
									</td>
										<td data-title="<s:text name="tgi.label.start_end" />">
										<c:if test="${tourOrderRow != null && tourOrderRow.tourEndDate != null}">
										${spysr:formatDate(tourOrderRow.tourEndDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy hh:mm')}
										</c:if>
										</td>
								<%-- 		<td data-title="<s:text name="tgi.label.start_date" />">${tourOrderRow.tourStartDate}</td>
										<td data-title="<s:text name="tgi.label.end_date" />">${tourOrderRow.tourEndDate}</td> --%>
										<td data-title="<s:text name="tgi.label.destiation_city" />">${tourOrderRow.orderCustomer.destinationCity}</td>
										<td data-title="<s:text name="tgi.label.destination_country" />">${tourOrderRow.orderCustomer.destinationCountry}</td>
									</tr></table></div>
									
								<br>
								
								<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><%-- <s:text name="tgi.label.tour_info" /> --%><s:text name="tgi.label.passenger_info" /></b>
									</div></div><br>
								<div class="no-table-css clearfix">
								
								<input type="hidden" name="orderId" id="orderId" value="${id}">
									<input type="hidden" name="totalRoomGuest" id="totalRoomGuest" value="${tourOrderRow.tourOrderRoomInfos.size()}">
									<input type="hidden" name="totalRoom" id="totalRoom" value="${tourOrderRow.tourOrderRoomInfos.size()}">
								 <table class="table table-bordered table-striped-column table-hover" id="guestTabel">
                                   <thead>
                                   <tr class="border-radius border-color">
                                   <th><s:text name="S.No" /></th>
									<th><s:text name="tgi.label.title" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="D.O.B" /></th>
									<th><s:text name="Paxtype" /></th>
									<th>Action</th>
								</tr></thead>
                                   
								<tbody>
								<c:forEach items="${tourOrderRow.tourOrderRoomInfos}"	var="roomInfo" varStatus="loop">
										<tr id="roomDataRow${roomInfo.id}" class="roomDataRow">
													<td data-title="<s:text name="tgi.label.room" /> ${loop.count}"><strong><s:text name="tgi.label.room" /> ${loop.count}
													</strong>
													<c:if test="${roomInfo.referenceCode != null && roomInfo.referenceCode != ''}">
													<td><strong>Reference Code : ${roomInfo.referenceCode}</strong></td> 
													</c:if>
													<c:if test="${roomInfo.status != null && roomInfo.status != ''}">
													<td><strong>Status :  ${roomInfo.status}</strong></td>
													</c:if>
													<c:set var="roomId" value="${roomInfo.id}" />
													
													<input type="hidden" id="deleteRoomId${roomId}" value="${roomId}" />
													</td>
										</tr>
										<input type="hidden" id="totalGuestHotelPerRoom${loop.count}" value="${roomInfo.tourOrderGuests.size()}" />
										<c:forEach items="${roomInfo.tourOrderGuests}"
														var="guestInfo" varStatus="loopInner">
										<tr id="roomGuestDataRow${roomInfo.id}${guestInfo.id}">
													<td data-title="<s:text name="tgi.label.guest" />" id="newGuest${roomInfo.id}${guestInfo.id}">
													<input type="text" class="form-control" value="<s:text name='tgi.label.guest' /> ${loopInner.count}" disabled="disabled" />
													<%-- <s:text name="tgi.label.guest" />
														 ${loopInner.count} --%>
														 <c:set var="roomGuestId" value="${guestInfo.id}" />
														 </td>	
													<td data-title="<s:text name="tgi.label.gender" />">
													<select name="title" id="title${roomId}${roomGuestId}" class='form-control' disabled="disabled">
													<option>Title</option>
													<option value ="Mr" ${guestInfo.title == 'Mr' ?'selected' :''}>Mr</option>
													<option value ="Mrs" ${guestInfo.title == 'Mrs' ?'selected' :''}>Mrs</option>
													<option value ="Miss" ${guestInfo.title == 'Miss' ?'selected' :''}>Miss</option></select>
													<%-- ${guestInfo.title} --%>
													</td>
													<td data-title="<s:text name="tgi.label.name" />">
													<input type="text" name="firstName" id="firstName${roomId}${roomGuestId}" value="${guestInfo.firstName}" class="form-control" placeholder="name" readonly="readonly" />
													</td>
													<td data-title="<s:text name="tgi.label.surname" />">
													<input type="text" name="lastName" id="lastName${roomId}${roomGuestId}" value="${guestInfo.lastName}" class="form-control" placeholder="Surname" readonly="readonly" />
													</td>
													<td data-title="<s:text name="D.O.B" />">
													<input type="text" name="dateOfBirth" id="dateOfBirth${roomId}${roomGuestId}" class="form-control dob${roomId}${roomGuestId}" value="${guestInfo.birthDate}" placeholder="DOB" readonly="readonly" />
													</td>
													<td data-title="<s:text name="Paxtype" />">
													<select name="paxType" id="paxType${roomId}${roomGuestId}" class='form-control' disabled="disabled">
													<option>Pax Type</option>
													<option  value ="ADT" ${guestInfo.paxType == 'ADT' ?'selected' :''}>ADT</option>
													<option value="CHD" ${guestInfo.paxType == 'ADT' ?'selected' :''}> CHD</option></select>
													</td>
													<td>
														<input type="hidden" id="roomAndGuestId${roomId}${roomGuestId}" value="${roomId}_${roomGuestId}" />
													  <div id="updateBtnDiv${roomId}${roomGuestId}" style="display:none;">
													  <button onclick="updateRoomGuest('${roomId}','${roomGuestId}')" class="btn btn-success">
											        Update
											        </button></div>
													  <div id="editBtnDiv${roomId}${roomGuestId}">
													   <button onclick="editRoomGuest('${roomId}','${roomGuestId}')">
											          <span class="glyphicon glyphicon-edit"></span>
											        </button>
												     <button onclick="deleteRoomGuest('${roomId}','${roomGuestId}')">
											         <span class="glyphicon glyphicon-trash"></span>
											       </button>
											       </div>
													</td>
										</tr>
										</c:forEach>
										<tr id="guestBtnRow${roomInfo.id}" class="guestBtnRow">
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>
										<input <c:if test='${roomInfo.tourOrderGuests.size() == 4}'> style="display:none;" </c:if> type="button" value="Add Guest" id="addRoomGuestBtn${roomInfo.id}" onclick="showGuestForm('${roomInfo.id}','${roomGuestId + 1}');" class="btn btn-info btn-xs" />
										<%-- <c:choose>
										<c:when test="${roomInfo.tourOrderGuests.size() < 4}">
										<input type="button" value="Add Guest" id="addRoomGuestBtn${roomInfo.id}" onclick="showGuestForm('${roomInfo.id}','${roomGuestId + 1}');" class="btn btn-info btn-xs" />
										</c:when>
										<c:otherwise>
										<input type="button" value="Add Guest" id="addRoomGuestBtn${roomInfo.id}" onclick="showGuestForm('${roomInfo.id}','${roomGuestId + 1}');" class="btn btn-info btn-xs" style="display:none;" />
										</c:otherwise>
										</c:choose> --%>
										</td>
										</tr>
							</c:forEach>
										<tr	id="roomRow">
										<td><input type="button" id="addRoomBtn" value="Add Room" onclick="addNewRoom('${roomId + 1}','${roomGuestId+1}');" class="btn btn-success btn-xs fa-bed" /></td>
										<td><input type="button" id="deleteRoomBtn" value="Delete Room" onclick="deleteRoom('${roomId}','${roomGuestId-1}');" class="btn btn-danger btn-xs" /></td>
										</tr>
							</tbody>
									</table></div>
								
								<br>
								<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><%-- <s:text name="tgi.label.tour_info" /> --%>Contact Info</b>
									</div></div><br>
								<div class="no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover">
                                   <thead>
                                   <tr class="border-radius border-color">
                                   	<th><s:text name="Title" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="tgi.label.mobile" /></th>
									<th><s:text name="Email" /></th>
									<th><s:text name="tgi.label.surname" /></th>
 
								</tr></thead>
									<tr>
										<td data-title="<s:text name="tgi.label.title" />">${tourOrderRow.orderCustomer.title}</td>
										<td data-title="<s:text name="tgi.label.name" />">${tourOrderRow.orderCustomer.firstName}</td>
										<td data-title="<s:text name="tgi.label.surname" />">${tourOrderRow.orderCustomer.lastName}</td>
										<td data-title="<s:text name="Email" />">${tourOrderRow.orderCustomer.mobile}</td>
										<td data-title="<s:text name="Email" />">${tourOrderRow.orderCustomer.email}</td>
										<td data-title="<s:text name="Email" />">${tourOrderRow.orderCustomer.address}</td>
									</tr></table></div>
								
								<br>
								
								<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><%-- <s:text name="tgi.label.tour_info" /> --%><s:text name="tgi.label.payment_info" /> </b>
									</div></div><br>
								<div class="no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover">
                                   <thead>
                                   <tr class="border-radius border-color">
									<th><s:text name="tgi.label.order_id" /></th>
									<th><s:text name="tgi.label.amount" /></th>
									<th><s:text name="tgi.label.fee_amount" /></th>
									<th><s:text name="tgi.label.payment_status" /></th>
									<th>Action</th>
								</tr></thead>
									<tr>
										<td data-title="<s:text name="tgi.label.order_id" />"><input type="text" class="form-control" value="${tourOrderRow.orderReference}" disabled="disabled"></td>
										<td data-title="<s:text name="tgi.label.amount" />"><input type="text" class="form-control" id="totalPrice" value="${tourOrderRow.totalPrice}" readonly="readonly" /> </td>
										<td data-title="<s:text name="tgi.label.fee_amount" />"><input type="text" class="form-control" id="feeAmount" value="${tourOrderRow.feeAmount}" readonly="readonly"/> </td>
										<td data-title="<s:text name="tgi.label.payment_status" />"><input type="text" class="form-control" value="${tourOrderRow.paymentStatus}" disabled="disabled"/> </td>
										<td><button id="editPaymentInfoBtn" onclick="editPaymentInfo()">
											          <span class="glyphicon glyphicon-edit"></span>
											        </button>
											  <button class="btn btn-success" id="updatePaymentInfoBtn" style="display:none;" onclick="updatePaymentInfo('${tourOrderRow.totalPrice}','${tourOrderRow.feeAmount}')">
											          Update
											        </button>
											  </td>
									</tr></table></div>
								<br>
								
								<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><%-- <s:text name="tgi.label.tour_info" /> --%><s:text name="tgi.label.tour_details" /></b>
									</div></div><br>

							<div class="panel panel-default"
								>
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collapse1"
											aria-expanded="true" aria-controls="collapseOne"> <i
											class="more-less glyphicon glyphicon-plus"></i> Highlights
										</a>
									</h4>
								</div>
								<div id="collapse1" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<div class="long-description">
											<p style="color: #c20a2f;">
												<strong>${tourOrderRow.tourItineraryDescription.title}</strong>
											</p>
											<ul>
												<li><b>${tourOrderRow.tourItineraryDescription.category}</b></li>
												<li>${tourOrderRow.tourItineraryDescription.description}</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<c:if test="${itineraryMap != null && itineraryMap.size()>0}">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse"
												data-parent="#accordion" href="#collapse2"
												aria-expanded="true" aria-controls="collapseOne"> <i
												class="more-less glyphicon glyphicon-plus"></i> <s:text name="tgi.label.itinerary_description" />
											</a>
										</h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse"
										role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body" style="padding-top: 0px;">
											<c:forEach items="${itineraryMap}" var="itineraryObj">
												<br>
												<li><strong style="color: #c20a2f; font-weight: 700;">${itineraryObj.key}
														day</strong></li>
												<li><b>${itineraryObj.value.title}</b></li>
												<li>${itineraryObj.value.description}</li>
											</c:forEach>
										</div>
									</div>
								</div>
							</c:if>

							<c:if
								test="${itineraryItemList != null && itineraryItemList.size() >0}">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse"
												data-parent="#accordion" href="#collapse3"
												aria-expanded="true" aria-controls="collapseOne"> <i
												class="more-less glyphicon glyphicon-plus"></i> <s:text name="tgi.label.inclusions_exclusions_&_extras" />
											</a>
										</h4>
									</div>
									<div id="collapse3" class="panel-collapse collapse"
										role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="long-description ">
												<c:forEach items="${itineraryItemList}" var="itemTypeObj">
													<p style="color: #c20a2f; font-weight: 15px;">${itemTypeObj.type}</p>
													<c:if
														test="${itemTypeObj.title != null && itemTypeObj.title !='Hotel'}">
														<p style="font-weight: 700;">${itemTypeObj.title}</p>
														<p style="font-weight: 700;">${itemTypeObj.description}</p>
														<%-- <c:forEach items="${itemTypeObj.tourItineraryDescription}" var="itemDataObj">
                                                                    <li><b><strong>${itemDataObj.title} : </strong></b></li><br><br>
																	<li>${itemDataObj.discription}</li><br><br><br><br><br><br>
																	
															</c:forEach> --%>
													</c:if>
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
							</c:if>


							<br>
								<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><%-- <s:text name="tgi.label.tour_info" /> --%>Order Summary</b>
									</div></div><br>
								<div class="table-responsive no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover">
                                   <thead>
                                   <tr class="border-radius border-color">
								<th><s:text name="tgi.label.total_passengers" /></th>
								<th><s:text name="tgi.label.fee_amount" /></th>
								<th><s:text name="tgi.label.total" /></th>								
								</tr></thead>
								<tr>
										<td data-title="<s:text name="tgi.label.total_passengers" />">${tourOrderRow.totalGuest}</td>
										<td data-title="<s:text name="tgi.label.fee_amount" />">${tourOrderRow.feeAmount}</td>
										<td data-title="<s:text name="tgi.label.total" />">${tourOrderRow.finalPrice} ${tourOrderRow.priceCurrency}</td>
										<%-- <td colspan="6">
 							 	<s:iterator value="tourOrderRow.tourItineraryDescription.tourItineraryDayDescriptions">
 									
												<div class="col-md-3">
													<strong>${passengerTypeCode}</strong>
												</div>
												
													<p>${title}
														${type}</p>
								</s:iterator> --%>
										
									</tr>
								</table></div>
								
				 <!-- 		<form class="form-inline" action="" method="post"> -->

					<%-- <div class="table-responsive dash-table">

						<!-- testing -->

						<div class="box clearfix" >
							<!-- <div class="box-body"> -->

							<label for="Country"><h4>
									<b><s:property value="user"/></b>
								</h4></label>
								
								
								
								
								
							<table class="table" 
								class="table table-striped table-bordered">
								
								<tr>
									<th colspan="5"><h5>
											<b><s:text name="tgi.label.tour_info" /><s:text name="tgi.label.tour_departure_details" /></b>
										</h5></th>


								</tr>
								<tr>
									<th><s:text name="tgi.label.start_date" /></th>
									<th><s:text name="tgi.label.end_date" /></th>
									<th><s:text name="tgi.label.destiation_city" /></th>
									<th><s:text name="tgi.label.destination_country" /></th>
								</tr>
									<tr>
										<td>${tourOrderRow.tourStartDate}</td>
										<td>${tourOrderRow.tourEndDate}</td>
										<td>${tourOrderRow.orderCustomer.destinationCity}</td>
										<td>${tourOrderRow.orderCustomer.destinationCountry}</td>


									</tr>
								<tr>
									<th colspan="5"><h5>
											<b><s:text name="tgi.label.passenger_info" /></b>
										</h5></th>
								</tr>

								<tr>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="tgi.label.gender" /></th>
 									<th><s:text name="tgi.label.tax" /></th>
									<th><s:text name="tgi.label.total" /></th>
 
									<th><s:text name="tgi.label.currency" /></th>
								</tr>
									<tr>
										<td>${tourOrderRow.orderCustomer.firstName}</td>
										<td>${tourOrderRow.orderCustomer.lastName}</td>
										<td>${tourOrderRow.orderCustomer.gender}</td>
										<td>0</td>
										<td>${tourOrderRow.finalPrice}</td>
										<td>${tourOrderRow.priceCurrency}</td>
									</tr>
								
								<tr>
									<th><h5>
											<b><s:text name="tgi.label.payment_info" /></b>
										</h5></th>


								</tr>
								<tr>
									<th><s:text name="tgi.label.order_id" /></th>
									<th><s:text name="tgi.label.amount" /></th>
									<th><s:text name="tgi.label.fee_amount" /></th>
									<th><s:text name="tgi.label.payment_status" /></th>
								</tr>
									<tr>
										<td>${tourOrderRow.orderReference}</td>
										<td>${tourOrderRow.totalPrice}</td>
										<td>${tourOrderRow.feeAmount}</td>
										<td>${tourOrderRow.paymentStatus}</td>
									</tr>
								<tr>
 								<td colspan="6">
 								<h5><b><s:text name="tgi.label.tour_details" /></b></h5>
 								</td>
 								</tr>	
 								<tr>
								<th><s:text name="tgi.label.title" /></th>
								<th><s:text name="tgi.label.category" /> </th>
								<th><s:text name="tgi.label.itinery" /> </th>
								<th><s:text name="tgi.label.description" /> </th>								
								</tr>
								<tr>
										<td>${tourOrderRow.tourItineraryDescription.title}</td>
										<td>${tourOrderRow.tourItineraryDescription.category}</td>
										<td>${tourOrderRow.tourItineraryDescription.description}</td>
										<td colspan="6">
 							 	<s:iterator value="tourOrderRow.tourItineraryDescription.tourItineraryDayDescriptions">
 									
												<div class="col-md-3">
													<strong>${passengerTypeCode}</strong>
												</div>
												
													<p>${title}
														${type}</p>
								</s:iterator>
										
									</tr>
 								<<tr>
 								td colspan="6">
 							 	<s:iterator value="tourOrderRow.tourItineraryDescription.tourItineraryDayDescriptions">
 									<div class="row">
												<div class="col-md-3">
													<strong>${passengerTypeCode}</strong>
												</div>
												<div class="col-md-9">
													<p>${title}
														${type}</p>
												</div>
											</div>
								</s:iterator></td> 
 								</tr>
 								<tr>
 								<td colspan="6">
 							 	<s:iterator value="tourOrderRow.tourItineraryDescription.tourItineraryHotels">
 									<div class="row">
												<div class="col-md-3">
													<strong>${passengerTypeCode}</strong>
												</div>
												<div class="col-md-9">
													<p>${title}
														${type}</p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<p>${description}</p>
												</div>
											</div>
								</s:iterator></td>
 								</tr>						
 								<tr>
 								<td colspan="6">
 							 	<s:iterator value="tourOrderRow.tourItineraryDescription.tourItineraryExclusions">
 									<div class="row">
												<div class="col-md-3">
													<strong></strong>
												</div>
												<div class="col-md-9">
													<p>${title}
														${type}</p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<p>${description}</p>
												</div>
											</div>
								</s:iterator></td>
 								</tr>				
 									<tr>
 								<td colspan="6">
 							 	<s:iterator value="tourOrderRow.tourItineraryDescription.tourItineraryInclusions">
 									<div class="row">
												<div class="col-md-3">
													<strong></strong>
												</div>
												<div class="col-md-9">
													<p>${title}
														${type}</p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<p>${description}</p>
												</div>
											</div>
								</s:iterator></td>
 								</tr>								
 								<tr>
									<th colspan="5"><h5>
											<b><s:text name="tgi.label.total_passengers" /> ${tourOrderRow.totalGuest}</b>
										</h5></th>

									<th align="left" colspan="5"><h4>
											<b><s:text name="tgi.label.total" />${tourOrderRow.finalPrice} ${tourOrderRow.priceCurrency}</b>
										</h4></th>
								</tr>

 							<tr>
						 <s:if test="ReportData.CreditNoteIssued==false"> 
						 <th>
									 <button type="button" id="orderNow"
											class="btn btn-primary"><s:text name="tgi.label.alter_order_now" /></button></th>
											<th colspan="4"></th>
						 </s:if>
						    <s:else>
											<!-- <th>
									 <a href="" id="orderNow"
											class="btn btn-primary">View Credit Note</a></th>
											<th colspan="4"></th> -->
											 </s:else> 
								</tr>
								
 								</table>
 								
 							</div>
 							</div>
 							
						<!-- /.box -->
 							</div> --%>
					<!-- table-responsive -->
					<!-- </form> -->
			 </div>
			
			<!-- /.row -->
			<!-- Main row -->

</div>
		</section>
		<!-- /.content -->
		
		<%-- <form action="insertOrderModifiedInfo"  method="post" >
								<s:if test="hasActionErrors()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionerror />
						</div>
					</s:if>
					<s:if test="hasActionMessages()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionmessage />
						</div>
					</s:if>
							<table id="editOrder" style="border: 2px solid #2283E1;display: none"   
								class="table table-striped table-bordered">


								<tr>

									<td><h4>BookingStatus</h4> 
									 
									<s:if test="ReportData.isOrderUpdated()">
									 <select name="statusAction"
										id="statusAction" autocomplete="off" required>
											 <option value="<s:property value="ReportData.status"/>"><s:property value="ReportData.status"/></option> 
									 </select>
									 </s:if>
									 <s:else>
									  <select name="statusAction"
										id="statusAction" autocomplete="off" required>
											<option selected="selected"   value=""><s:text name="tgi.label.select_booking_status" /></option>
											<option value="ticketed"><s:text name="tgi.label.ticketed" /></option> 
											<option value="failed"><s:text name="tgi.label.failed" /></option>
											 <option value="confirmed"><s:text name="tgi.label.confirmed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option> 
											
									</select>
									 </s:else>
									
									
									
									<input type="hidden"  name="flightOrderRowId"  value="<s:property value="ReportData.id"/>">
									<input type="hidden"  name="beforeStatus"  value="<s:property value="ReportData.status"/>">
									<input type="hidden"  name="userId"  value="<s:property value="%{#session.User.id}"/>">
									<input type="hidden"  name="companyId"  value="<s:property value="%{#session.Company.companyid}"/>">
									<input type="hidden"  name="gstAmount"  value="<s:property value="ReportData.gstOnMarkup"/>">
									<input type="hidden"  name="totalBookingAmount"  value="<s:property value="ReportData.finalPrice"/>">
									 <input type="hidden"  name="updatedBy" value="<s:property
									 value="%{#session.User.Username}"/>">
									</td>

									<td><h4>PaymentStatus</h4>
									<s:if test="ReportData.isOrderUpdated()">
									 <select name="paymentStatus"
										id="paymentStatus"    autocomplete="off" required> 
											 <option value="<s:property value="ReportData.paymentStatus"/>"><s:property value="ReportData.paymentStatus"/></option>
											 
									</select>
									 </s:if>
									<s:else>
									 <select name="paymentStatus"
										id="paymentStatus"    autocomplete="off" required> 
											<option selected="selected" value=""><s:text name="tgi.label.select_payment_status" /></option>
											 <option value="success"><s:text name="tgi.label.success" /></option>
											<option value="failed"><s:text name="tgi.label.failed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option>
											<option value="refund"><s:text name="tgi.label.refund" /></option>

									</select> 
									</s:else>
									 <td><h4><s:text name="tgi.label.conveniencefees" /></h4>
									  <input type="text" name="convenienceFees" id="convenienceFees"  autocomplete="off" required> 
									  
									 </td>
										  <td><h4><s:text name="tgi.label.cancellationfees" /></h4> 
										  <s:if test="ReportData.isOrderUpdated() && ReportData.cancellationFees!=null">
									 <input type="text"  name="cancellationFees"
										id="cancellationFees"  value="<s:property value="ReportData.cancellationFees"/>"  autocomplete="off" required> 
									 </s:if>
										<s:else>
									 <input type="text" name="cancellationFees" id="cancellationFees"  autocomplete="off" required>
									  </s:else> 
										 
									  </td>
									 <td>
										<h4><s:text name="tgi.label.empcomments" /></h4> <textarea name="employeeComments"> </textarea>

									</td>

									<td>
										<h4><s:text name="tgi.label.api_comments" /></h4> <textarea  disabled="disabled"><s:property
												value="%{#session.ReportData.apiComments}" /></textarea>

									<input type="hidden" name="apiComments" value="<s:property
												value="ReportData.apiComments"/>">
									
									
									
									</td>

									<td><h4><s:text name="tgi.label.action" /></h4>
										<button type="submit" class="btn btn-primary"><s:text name="tgi.label.update" />
											 </button></td>
								</tr>

							</table>
						</form> --%>
	</div></div></div></section>
	<!-- /.content-wrapper -->
	<script type="text/javascript">
		$(document).ready(function() {
			$("#editOrder").hide();
			
			$("#orderNow").click(function() {
				$("#editOrder").toggle("slow", function() {

				});
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#twodpd2").datepicker({
				dateFormat : "yy-mm-dd"
			});
			$("#twodpd1").datepicker({
				dateFormat : "yy-mm-dd"
			/*  changeMonth: true,
			 changeYear: true */
			});
		});
	</script>
	<script type="text/javascript">
	/* 	$(document).ready(
				function() {
					var table = $('#example').DataTable({
						lengthChange : false,
						"searching" : false,
						"ordering" : false,
						"paging" : false,
						"info" : false, */
					/* "pagingType" : "full_numbers",
					"lengthMenu" : [ 10, 25, 50, 75, 100, 500 ], */
					/* buttons : [ 'excel', 'pdf', 'print' ] */
				/* 	});

					table.buttons().container().appendTo(
							'#example_wrapper .col-sm-6:eq(0)');

				}); */

		/*  $(function () {
		 	$('#example').DataTable({
		    	 "paging": true,
		         "lengthChange": true,
		        "searching": true,
		        "ordering": true,  
		           "info": true
		         "autoWidth": false,  
		        "search": {
		      	    "regex": true,
		      	  }, 
		      	 
		      "pagingType": "full_numbers",
		      "lengthMenu": [10, 25, 50, 75, 100, 500 ],
		     
		      
		     });  
		  
		   });   */
	</script>
	
	
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"mailConfigList";
	$('#success').click(function() {
	// window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
	<%-- 
<script type="text/javascript">
   $(function(){
	   var txt = $('.success-alert').text();
	   var protocol=location.protocol;
   	   var host=location.host;
   	   //var url=protocol+"//"+host+"/LintasTravelAdmin/superUserReportList";
	   if(txt.length>0){
    	   alert(txt);
    	   //window.location.assign(url);
    	 
    	   
     }
    });
 </script>
 --%>
	<!-- 
 
  $(document).ready(function() 
    	 { 
    		 $("#twodpd1").datepicker({
    			 dateFormat: "yy-mm-dd"  
    			/*  changeMonth: true,
    			 changeYear: true */
    		 }); 
    		 }); -->

<script>
function showGuestForm(roomId, roomGuestId)
		{
	var preRoomGuestId = parseInt(roomGuestId) - 1;
	
	var newGuestFormContentRow = "<tr id='dynamicGuestAddFormRow"+roomId+""+roomGuestId+"'>"+
		+"<td></td>"
		+"<td id='newGuest"+roomId+""+roomGuestId+"'>New Guest</td>"
		+"<td><select name='title' id='title"+roomId+""+roomGuestId+"' class='form-control'>"
		+"<option>Mr</option><option>Miss</option><option>Mrs</option></select></td>"+
		+"<td></td>"
		+"<td><input type='text' name='firstName' id='firstName"+roomId+""+roomGuestId+"' class='form-control' placeholder='Name' /> </td>"
		+"<td><input type='text' name='lastName' id='lastName"+roomId+""+roomGuestId+"' class='form-control' placeholder='Surname' /> </td>"
		+"<td><input type='text' name='dateOfBirth' id='dateOfBirth"+roomId+""+roomGuestId+"' class='form-control dob"+roomId+""+roomGuestId+"' placeholder='DOB' /></td>"
		+"<td><select name='paxType' id='paxType"+roomId+""+roomGuestId+"' class='form-control'>"
		+"<option>ADT</option><option>CHILD</option><option>SENIOR</option>"
		+"</select></td>"
		+"<td><input type='button' value='Add' class='btn btn-info' onclick='submitNewGuestData("+roomId+","+roomGuestId+");' /> </td>"
		+"</tr>";

	$("#guestTabel > tbody > tr#guestBtnRow"+roomId).before(newGuestFormContentRow); 
	$("#guestTabel > tbody > tr#guestBtnRow"+roomId).hide();
	 
	$(".dob"+roomId+""+roomGuestId).datepicker({
		dateFormat : "mm-dd-yy"
	});	
		}

</script>

	
    		 <script>
    			 function submitNewGuestData(roomId, roomGuestId){
    			var nextRoomId = parseInt(roomId) + 1;
    			var preRoomId = parseInt(roomId) - 1;
    			var nextRoomGuestId = parseInt(roomGuestId) + 1;
    			var preRoomGuestId = parseInt(roomGuestId) - 1;
    			
    			var orderId = $("#orderId").val();
			    var title = $("#title"+roomId+""+roomGuestId).val();
			    var firstName = $("#firstName"+roomId+""+roomGuestId).val();
			    var lastName = $("#lastName"+roomId+""+roomGuestId).val();
			    var dateOfBirth = $("#dateOfBirth"+roomId+""+roomGuestId).val();
			    var paxType = $("#paxType"+roomId+""+roomGuestId).val();
			    var totalRoomGuest = $("#totalRoomGuest").val();
			   
			    
			    var totalRoomGuest = parseInt($("#totalRoomGuest").val()) + 1;
			    $("#totalRoomGuest").val(totalRoomGuest);
			    
			//remove guest add form row
				$("#guestTabel > tbody > tr#dynamicGuestAddFormRow"+roomId+""+roomGuestId).remove();
			 
			// show guest add button
				$("#guestTabel > tbody > tr#guestBtnRow"+roomId).show();
				
			 var newAddGuestBtnRow = "<input type='button' style='display:none;' value='Add Guest' id='addRoomGuestBtn"+roomId+"' onclick='showGuestForm("+roomId+","+nextRoomGuestId+");' class='btn btn-info btn-xs' />";
				 $("#addRoomGuestBtn"+roomId).replaceWith(newAddGuestBtnRow);
				  
				 var replaceNewAddRoomBtn = "<td> <input type='button' id='addRoomBtn' value='Add Room' onclick='addNewRoom("+nextRoomId+","+nextRoomGuestId+");' class='btn btn-success btn-xs' /></td>"
				  +"<td><input type='button' id='deleteRoomBtn' value='Delete Room' onclick='deleteRoom("+preRoomId+","+roomGuestId+");' class='btn btn-danger btn-xs' /></td>";
				$("#roomRow").html(replaceNewAddRoomBtn);	 
			    
            	// show new guest data row
            	var titleSelectOption = getSelectedTitle(title, roomId, roomGuestId);
            	var paxTypeSelectOption = getSelectedPaxType(paxType, roomId, roomGuestId);
            	
            	$(".dob"+roomId+""+roomGuestId).datepicker({
            		dateFormat : "mm-dd-yy"
            	});	
            	
                var Obj_data = {
                	orderId : orderId,
                	roomId : roomId,
				    title : title,
                	firstName : firstName,
					lastName : lastName,
					dateOfBirth : dateOfBirth,
					paxType : paxType,
					totalRoomGuest : totalRoomGuest
                };
                $.ajax({
                    type: "POST",
                    url: '<%=request.getContextPath()%>/addTourOrderGuest.action',
                    data : Obj_data,
                    success: function(response)
                    {  
//                    	alert("response"+response.roomAndGuestId);
                    	var newEditContentRow = "<tr id='roomGuestDataRow"+roomId+""+roomGuestId+"'>"+
        				+"<td></td>"
        				+"<td><input type='text' value='New Guest' class='form-control' disabled=disabled /></td>"
        				+titleSelectOption
        				+"<td><input type='text' name='firstName' id='firstName"+roomId+""+roomGuestId+"' value='"+firstName+"' class='form-control' placeholder='Name' readonly='readonly' /> </td>"
        				+"<td><input type='text' name='lastName' id='lastName"+roomId+""+roomGuestId+"' value='"+lastName+"' class='form-control' placeholder='Surname' readonly='readonly' /> </td>"
        				+"<td><input type='text' name='dateOfBirth' id='dateOfBirth"+roomId+""+roomGuestId+"' value='"+dateOfBirth+"' class='form-control dob' placeholder='DOB' readonly='readonly' />"
        				+"<input type='hidden' id='title"+roomId+""+roomGuestId+"' />"
        				+"<input type='hidden' id='paxType"+roomId+""+roomGuestId+"' />"
        				+"</td>"
        				+paxTypeSelectOption
        				+"<td>"
        				+"<input type='hidden' id='roomAndGuestId"+roomId+""+roomGuestId+"' value='"+response.roomAndGuestId+"' />"
        				+"<div id='updateBtnDiv"+roomId+""+roomGuestId+"' style='display:none;'>"
        				+"<button onclick='updateRoomGuest("+roomId+","+roomGuestId+")' class='btn btn-success'>"
        		        +"Update</button></div>"
        				+"<div id='editBtnDiv"+roomId+""+roomGuestId+"'>"
        				+"<button onclick='editRoomGuest("+roomId+","+roomGuestId+")'>"
        		        +"<span class='glyphicon glyphicon-edit'></span></button>"
        			    +"<button onclick='deleteRoomGuest("+roomId+","+roomGuestId+")'>"
        		        +"<span class='glyphicon glyphicon-trash'></span>"
        		       	+"</button></div></td>"
        				+"</tr>";
        				$("#guestBtnRow"+roomId).before(newEditContentRow);
        				
        				var totalRoom = parseInt($("#totalRoom").val());
        				if(totalRoom == 1)
        				$("#deleteRoomBtn").hide();

        				//alert(response.totalGuestPerRoom);
        				if(response.totalGuestPerRoom < 4)
        					$("#addRoomGuestBtn"+roomId).show();
                    }
                });             
             
            }
    </script>
    
    
    <!-- Add room -->
 				<script>
    			 function addNewRoom(roomId, roomGuestId)
    			 { 
    			var preRoomId = parseInt(roomId) - 1;
    			var nextRoomId = parseInt(roomId) + 1;
    			var nextRoomGuestId = parseInt(roomGuestId) + 1;
    			
    			
    			var orderId = $("#orderId").val();
    			var totalRoom = parseInt($("#totalRoom").val())+1;
    			var newGuestRoom = "<tr id='roomDataRow"+roomId+"'>"
								   +"<td data-title='<s:text name='tgi.label.room' />"
								   +"${loop.count}'><strong><s:text name='tgi.label.room' /> "+totalRoom+" </strong>"
								   +"<input type='hidden' id='deleteRoomId"+roomId+"' value='"+roomId+"' /></td>";
								   +"<td><strong></strong></td>"
								   +"<td><strong></strong></td></tr>";
				
				// save total room no as a temprary storage
				$("#totalRoom").val(totalRoom);
				
				var newRoomAddData = newGuestRoom
									+"<tr id='guestBtnRow"+roomId+"'>"
									+"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
									+"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
									+"<td>"
									+"<input type='button' value='Add Guest' id='addRoomGuestBtn"+roomId+"' onclick='showGuestForm("+roomId+","+roomGuestId+");' class='btn btn-info btn-xs' />"
									+"</td>"
									+"</tr>";
    		$("#guestTabel > tbody > tr#guestBtnRow"+preRoomId).after(newRoomAddData);
            
    		var replaceNewAddRoomBtn = "";
    		if(totalRoom == 4)
    			{
			replaceNewAddRoomBtn = "<td><input type='button' style='display:none;' id='addRoomBtn' value='Add Room' onclick='addNewRoom("+nextRoomId+","+roomGuestId+");' class='btn btn-success btn-xs' /></td>"
									  +"<td><input type='button' id='deleteRoomBtn' value='Delete Room' onclick='deleteRoom("+roomId+","+roomGuestId+");' class='btn btn-danger btn-xs' /></td>";
    			}
    		else
    			{
   			replaceNewAddRoomBtn = "<td><input type='button' id='addRoomBtn' value='Add Room' onclick='addNewRoom("+nextRoomId+","+roomGuestId+");' class='btn btn-success btn-xs' /></td>"
			  							+"<td><input type='button' id='deleteRoomBtn' value='Delete Room' onclick='deleteRoom("+roomId+","+roomGuestId+");' class='btn btn-danger btn-xs' /></td>";	
    			}
			$("#roomRow").html(replaceNewAddRoomBtn);
			
			var Obj_data = {
                	orderId : orderId,
                	totalRoom : totalRoom
                };
                $.ajax({
                    type: "POST",
                    url: '<%=request.getContextPath()%>/addTourOrderRoom.action',
                    data : Obj_data,
                    success: function(response)
                    {  
                    	$("#deleteRoomId"+roomId).val(response.roomId);
                    }
                });             
             
            }
    </script>
    
    
     <script>
    function editRoomGuest(roomId, roomGuestId)
    {
    	// enable room guest edit form
    	disableRoomGuestForm(false, roomId, roomGuestId);
    	
    	 $("#editBtnDiv"+roomId+""+roomGuestId).hide();
    	 $("#updateBtnDiv"+roomId+""+roomGuestId).show();
    	 
    	 $(".dob"+roomId+""+roomGuestId).datepicker({
     		dateFormat : "yy-mm-dd"
     	});	
    }
    
    function disableRoomGuestForm(status, roomId, roomGuestId)
    {
    	 $("#title"+roomId+""+roomGuestId).attr('disabled', status);
    	 $("#firstName"+roomId+""+roomGuestId).attr('readonly', status);
    	 $("#lastName"+roomId+""+roomGuestId).attr('readonly', status);
    	 $("#dateOfBirth"+roomId+""+roomGuestId).attr('readonly', status);
    	 $("#paxType"+roomId+""+roomGuestId).attr('disabled', status);
    }
    </script>
    
     <script>
    function updateRoomGuest(roomId, roomGuestId)
    {
    	var orderId = $("#orderId").val();
    	var title = $("#title"+roomId+""+roomGuestId).val();
	    var firstName = $("#firstName"+roomId+""+roomGuestId).val();
	    var lastName = $("#lastName"+roomId+""+roomGuestId).val();
	    var dateOfBirth = $("#dateOfBirth"+roomId+""+roomGuestId).val();
	    var paxType = $("#paxType"+roomId+""+roomGuestId).val();
	    var roomAndGuestId = $("#roomAndGuestId"+roomId+""+roomGuestId).val();
	    
	    // disable room guest edit form
	    disableRoomGuestForm(true, roomId, roomGuestId);
	    $("#editBtnDiv"+roomId+""+roomGuestId).show();
   	 	$("#updateBtnDiv"+roomId+""+roomGuestId).hide();
   	  
   	 var Obj_data = {
         	orderId : orderId,
         	roomId : roomId,
			title : title,
         	firstName : firstName,
			lastName : lastName,
			dateOfBirth : dateOfBirth,
			paxType : paxType,
			roomAndGuestId : roomAndGuestId
         };       
   	 $.ajax({
         type: "POST",
         url: '<%=request.getContextPath()%>/updateTourOrderGuest.action',
         data : Obj_data,
         success: function(response)
         {  
         	
         }
     	});
    }
    </script>
    
    <script>
    function deleteRoomGuest(roomId, roomGuestId)
    {
    	var roomAndGuestId = $("#roomAndGuestId"+roomId+""+roomGuestId).val();
    	$("#roomGuestDataRow"+roomId+""+roomGuestId).remove(); 
    	var Obj_data = {
    			 roomAndGuestId : roomAndGuestId
    	         };  
    	 $.ajax({
             type: "POST",
             url: '<%=request.getContextPath()%>/deleteTourOrderGuest.action',
             data : Obj_data,
             success: function(response)
             { 
            	// alert(roomId+" = "+response.totalGuestPerRoom);
            	 if(response.totalGuestPerRoom < 4)
 					$("#addRoomGuestBtn"+roomId).show();
             }
         });      
    }
    </script>
    
    <script>
    function deleteRoom(roomId, roomGuestId)
    {
    	var orderId = $("#orderId").val();
    	var deleteRoomId = $("#deleteRoomId"+roomId).val();
    	
    	var Obj_data = {
   			 roomId : deleteRoomId,
   			orderId : orderId
   	         };  
    	 $.ajax({
             type: "POST",
             url: '<%=request.getContextPath()%>/deleteTourOrderRoom.action',
             data : Obj_data,
             success: function(response)
             {  
             	// alert(response.roomId);	
            	
             	 $("#roomDataRow"+roomId).remove();
             	$("#roomGuestDataRow"+roomId+""+roomGuestId).remove();
             	$("#guestBtnRow"+roomId).remove();
             	location.reload();
             	/* var nextRoomId = parseInt(roomId) + 1;
             	
             	var replaceNewAddRoomBtn = "<td><input type='button' value='Add Room' onclick='addNewRoom("+nextRoomId+","+roomGuestId+");' class='btn btn-success btn-xs' /></td>"
				  +"<td><input type='button' value='Delete Room' onclick='deleteRoom("+response.roomId+","+roomGuestId+");' class='btn btn-danger btn-xs' /></td>";
					$("#roomRow").html(replaceNewAddRoomBtn); */
             }
         });      
    }
    </script>
    
    <script>
    function getSelectedTitle(title, roomId, roomGuestId)
    {
    	var titleSelectOption = "";
    	if(title == 'Mr')
    		{
    		titleSelectOption = "<td><select name='title' id='title"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
								+"<option selected='selected'>Mr</option><option>Miss</option><option>Mrs</option></select></td>";
    		}
    	else if(title == 'Mrs')
    		{
    		titleSelectOption = "<td><select name='title' id='title"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
				+"<option>Mr</option><option>Miss</option><option selected='selected'>Mrs</option></select></td>";
    		}
    	else if(title == 'Miss')
    		{
    		titleSelectOption = "<td><select name='title' id='title"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
				+"<option selected='selected'>Mr</option><option selected='selected'>Miss</option><option>Mrs</option></select></td>";
    		}
    	return titleSelectOption;
    }
    
    function getSelectedPaxType(paxType, roomId, roomGuestId)
    {
    	var paxTypeSelectOption = "";
    	if(paxType == 'ADT')
    		{
    		paxTypeSelectOption = "<td><select name='paxType' id='paxType"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
									+"<option selected='selected'>ADT</option><option>CHILD</option><option>SENIOR</option></select></td>";
    		}
    	else if(paxType == 'CHILD')
    		{
    		paxTypeSelectOption = "<td><select name='paxType' id='paxType"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
				+"<option>ADT</option><option selected='selected'>CHILD</option><option>SENIOR</option></select></td>";
    		}
    	else if(paxType == 'SENIOR')
    		{
    		paxTypeSelectOption = "<td><select name='paxType' id='paxType"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
				+"<option>ADT</option><option>CHILD</option><option selected='selected'>SENIOR</option></select></td>";
    		}
    	return paxTypeSelectOption;
    }
    </script>

    
    <script>
    $(document).ready(function()
    		{
    	var totalRoom = parseInt($("#totalRoom").val());
    	if(totalRoom == 4)
   		{
    		$("#addRoomBtn").hide();
   		}
    	else if(totalRoom < 4)
   		{
    		$("#addRoomBtn").show();
   			if(totalRoom == 1)
   		    $("#deleteRoomBtn").hide();
   			else
			{
			$("#deleteRoomBtn").show();	
			}
   		}
    		});
    </script>
    
    
    <script>
 	function editPaymentInfo()
    {
    	 $("#totalPrice").attr('readonly',false);	
    	 $("#feeAmount").attr('readonly',false);
    	 $("#editPaymentInfoBtn").hide();
    	 $("#updatePaymentInfoBtn").show();
    }
    
    function updatePaymentInfo()
    {
    	var orderId = $("#orderId").val();
    	var totalPrice = $("#totalPrice").val();
   	 	var feeAmount = $("#feeAmount").val();
		
    	$("#totalPrice").attr('readonly',true);	
   	 	$("#feeAmount").attr('readonly',true);
   	 	$("#editPaymentInfoBtn").show();
   	 	$("#updatePaymentInfoBtn").hide();
    	
    	var Obj_data = {
   			orderId : orderId,
   			totalPrice : totalPrice,
   			feeAmount : feeAmount
   	         };  
    	 
    	$.ajax({
             type: "POST",
             url: '<%=request.getContextPath()%>/updateTourPaymentInfo.action',
             data : Obj_data,
             success: function(response)
             {}
         });      
    }
    </script>
    