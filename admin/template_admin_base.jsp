<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!doctype html>
<html lang="en" class="no-js">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<link rel="stylesheet" href="admin/css/font-awesome.min.css">
<!-- <link rel="stylesheet" href="admin/fonts/fontawesome-all.min.css"> -->
<meta name="description" content="">
<meta name="author" content="">
<!-- <link rel="icon" href="assets/img/favicon.ico"> -->
<title>Travel Admin</title>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="admin/css/bootstrap.min.css">
<link rel="stylesheet" href="admin/css/bootstrap-datepicker3.css">
  <link href="admin/css/multiple-select.css" rel="stylesheet"/>
<link rel="stylesheet" href="admin/css/fileinput.min.css">
<link rel="stylesheet" href="admin/css/flags.css">
<link rel="stylesheet" href="admin/css/airlines.css">
<link rel="stylesheet" href="admin/css/bootstrap-table.css">
<link rel="stylesheet" href="admin/css/less-plugins/awesome-bootstrap-checkbox.css">
<link rel="stylesheet" href="admin/css/bootstrapValidator.min.css" />
<!-- Font Awesome Icons -->
<link rel="stylesheet" href="admin/css/style.css">
<link rel="stylesheet" href="admin/css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="admin/css/alertify.css">
 

<script src="admin/js/jquery.min.js" ></script>
<script src="admin/js/bootstrap.min.js"></script>
<script src="admin/js/bootstrap-table-all.js"></script>
<script src="admin/js/bootstrap-editable.js"></script>
<script src="admin/js/clipboard.min.js"></script>  

</head>

<body>
	<!--************************************
        TOP NOTIFICATION AND LOGOUT
    ****************************************-->

	<div class="main-container clearfix">
		<tiles:insertAttribute name="sidebar" ignore="true" />
<!--******************************************************
        MAIN ADMIN AREA
*******************************************************-->

		<!--ADMIN AREA BEGINS-->
		<section id="main-area" class="main-admin-area shrink">
			<tiles:insertAttribute name="header" ignore="true" />
			<tiles:insertAttribute name="body" ignore="true" />
		</section>
<!--******************************************************
        MAIN ADMIN AREA END
*******************************************************-->		

		<tiles:insertAttribute name="footer" ignore="true" />
	</div>
</body>
</html>