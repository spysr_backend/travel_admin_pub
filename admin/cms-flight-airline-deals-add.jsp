<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addFlightAirlineDeals";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                    <div class="card1">
                        <div class="pnl">
                         <div class="hd clearfix">
                                <h5 class="pull-left">Add Flight Airline Deals From </h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="metatagpage_list"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.meta_dat_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div> 
				</div>
</div>
<div class="container">
					<div class="row">
					<h4 class="text-center p-4 hidden-xs">Add Flight Airline Deals From</h4> 
                   <div class="spy-form-horizontal mb-0"> 
					<div class="">
						<div class="">
						<form action="insertFlightAirlineDeals.action" method="post">
							
							
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Deals Page Number
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="dealPageNumber" placeholder="Deal Page Number" autocomplete="off"
											required> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Airline Code
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="airlineCode" placeholder="Airline Code" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div>
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Depart Range1
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="departRange1" id="departRange1" placeholder="Depart Range1" autocomplete="off"
											required> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Depart Range2
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="departRange2" id="departRange1" placeholder="Depart Range2" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div>
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Total Price
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="totalPrice" placeholder="Total Price" autocomplete="off"
											required> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Tax
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="tax" placeholder="Tax" autocomplete="off"
											required> </div></div>
											</div>
										</div>
									</div>
									
					</div>
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Base Fare
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="baseFare" placeholder="Base Fare" autocomplete="off"
											required> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Currency
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="currency" placeholder="Currency" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div>
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Ticketing Date
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="ticketingDate" id="ticketingDate" placeholder="Ticketing Date" autocomplete="off"
											required> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Advance Purchase Days
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="advancePurchaseDays" placeholder="Advance Purchase Days" autocomplete="off"
											required> </div></div>
											</div>
										</div>
									</div>
									
					</div>
					<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Maximum Stay
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="maximumStay" placeholder="Maximum Stay" autocomplete="off"
											required> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"> &nbsp;
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="submit" class="btn btn-info"></div></div>
											</div>
										</div>
									</div>
								</div>
							 
							
							<!-- <table class="table table-form">
								<thead>
									<tr>
										<th></th>
										<th>&nbsp;</th>


									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<div class="field-name">
												Deals Page Number
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="dealPageNumber" placeholder="Deal Page Number" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Airline Code
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="airlineCode" placeholder="Airline Code" autocomplete="off"
											required></td>
									</tr>
									<tr>
										<td>
											<div class="field-name">
												Depart Range1
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="departRange1" id="departRange1" placeholder="Depart Range1" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Depart Range2
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="departRange2" id="departRange1" placeholder="Depart Range2" autocomplete="off"
											required></td>
									</tr>
									<tr>
										<td>
											<div class="field-name">
												Total Price
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="totalPrice" placeholder="Total Price" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Tax
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="tax" placeholder="Tax" autocomplete="off"
											required></td>
									</tr>
									<tr>
										<td>
											<div class="field-name">
												Base Fare
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="baseFare" placeholder="Base Fare" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Currency
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="currency" placeholder="Currency" autocomplete="off"
											required></td>
									</tr>
									<tr>
										<td>
											<div class="field-name">
												Ticketing Date
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="ticketingDate" id="ticketingDate" placeholder="Ticketing Date" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Advance Purchase Days
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="advancePurchaseDays" placeholder="Advance Purchase Days" autocomplete="off"
											required></td>
									</tr>
									<tr>
										<td>
											<div class="field-name">
												Maximum Stay
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="maximumStay" placeholder="Maximum Stay" autocomplete="off"
											required></td>
										<td></td>
										<td><input type="submit" class="btn btn-info"></td>
									</tr>
								</tbody>
							</table> -->

						</form>
					</div>
				</div>
				</div>
				</div></div>
</div></div>
                            </section>        
                                    			<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	 <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_body').text("${message}");
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
						

						<script>
 $(document).ready(function() 
 { 
	 $("#departRange1").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 $("#departRange2").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 $("#ticketingDate").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 }); 
 </script>