<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
<link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
<script src="admin/js/bootstrap-datepicker.js"></script>


<div class="profile-timeline-card" style="margin: 0px;">
		
							<!-- <a class="btn btn-xs btn-outline-danger pull-right mr-1" href="userRegister">
								<i class="fa fa-times"></i> Mark as Absent
							</a>
							<a class="btn btn-xs btn-outline-warning pull-right mr-1" href="userRegister">
								<i class="fa fa-check"></i> Mark as Leave
							</a>
							<a class="btn btn-xs btn-outline-success pull-right mr-1" data-event="duplicate" data-toggle="modal" data-target="#update_bug_traker">
								<i class="fa fa-check"></i> Mark as Present
							</a> -->
							<p class="">
								<span> <b class="blue-grey-text">Employees <span id="status"></span></b></span>
							</p>
						<hr>
						
		<c:choose>
		<c:when test="${userVos != null && userVos.size()>0}">
		<div class="row timeline-footer">
		<c:forEach items="${userVos}" var="user" varStatus="rowstatus">
	          <div class="student_attendance_row">
			    <div class="attendance_taking_div">
				<div class="row">
							<div class="col-md-1" style="width: 0px;">
							<div class="checkbox checkbox-default" style="margin-top: 0px;">
								<input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${id}" class="check_row">
								<label for="checkbox1_sd_${rowstatus.count}"></label>
							</div>
							</div>
							<div class="col-md-1">
							<div style="display: inline-block; font-size: 14px;">
								${rowstatus.count}
							</div>
							</div>
							<div class="col-md-1">
							<div style="display: inline-block; font-size: 14px;">
							<a href="agentDetails?id=${user.userId}" class="dark-text student-name" data-userId-value="${user.userId}">${user.firstName} ${user.lastName}</a>
							</div>
							</div>
							<div class="col-md-2">
							<div style="display: inline-block; font-size: 14px;"><span class="tooltipp link" title="Student Roll Number">
							<a href="#" class="dark-text roll" data-userId-value="${user.userId}">${user.email}</a></span>
							</div>
							</div>
							
							<div class="col-md-1">
							<div class="btn-group">
			                <button type="submit" class='present-attattendance attendance-btn btn btn-sm ${attendanceByDayUserDataMap[user.userId]!=null && attendanceByDayUserDataMap[user.userId].present?"btn-success":"btn-default"}' 
			                data-userid-value="${user.userId}" data-attendance-type="P" data-attendance-id='<c:if test="${attendance !=null}">${attendance.id}</c:if>'
			                data-day-id='<c:if test="${attendanceByDayUserDataMap[user.userId]!=null}">${attendanceByDayUserDataMap[user.userId].id}</c:if>'  data-row-count="${rowstatus.count}" 
			                id="present-attattendance-btn_${rowstatus.count}" style="display: inline; margin-right: 10px;">Present</button>
			            	</div>
							</div>
							
							<div class="col-md-1">
							<div class="btn-group">
			                <button type="submit" class='absent-attattendance attendance-btn btn btn-sm ${attendanceByDayUserDataMap[user.userId]!=null && attendanceByDayUserDataMap[user.userId].absent?"btn-danger":"btn-default"}'
			                data-userid-value="${user.userId}" data-attendance-type="A" data-attendance-id='<c:if test="${attendance !=null}">${attendance.id}</c:if>'
			                data-day-id='<c:if test="${attendanceByDayUserDataMap[user.userId]!=null}">${attendanceByDayUserDataMap[user.userId].id}</c:if>'
			                id="absent-attattendance-btn_${rowstatus.count}" data-row-count="${rowstatus.count}"  style="display: inline; margin-right: 10px;">Absent</button>
			            	</div>
							</div>
							
							<div class="col-md-1">
							<div class="btn-group">
			                <button type="submit" class='leave-attattendance attendance-btn btn btn-sm ${attendanceByDayUserDataMap[user.userId]!=null && attendanceByDayUserDataMap[user.userId].leave?"btn-warning":"btn-default"}' 
			                data-userid-value="${user.userId}" data-attendance-type="L" data-attendance-id='<c:if test="${attendance !=null}">${attendance.id}</c:if>'
			                data-day-id='<c:if test="${attendanceByDayUserDataMap[user.userId]!=null}">${attendanceByDayUserDataMap[user.userId].id}</c:if>'
			                id="leave-attattendance-btn_${rowstatus.count}" data-row-count="${rowstatus.count}" style="display: inline; margin-right: 10px;">Leave</button>
			            	</div>
			            	</div>
							<div class="col-md-1">
							<div class="btn-group">
			                <button type="submit" class='late-attattendance attendance-btn btn btn-sm ${attendanceByDayUserDataMap[user.userId]!=null && attendanceByDayUserDataMap[user.userId].late?"btn-primary":"btn-default"}' 
			                data-userid-value="${user.userId}" data-attendance-type="LT" data-attendance-id='<c:if test="${attendance !=null}">${attendance.id}</c:if>'
			                data-day-id='<c:if test="${attendanceByDayUserDataMap[user.userId]!=null}">${attendanceByDayUserDataMap[user.userId].id}</c:if>'
			                id="late-attattendance-btn_${rowstatus.count}" data-row-count="${rowstatus.count}" style="display: inline; margin-right: 10px;">Late</button>
			                <%-- <c:if test="${attendanceByDayUserDataMap[user.userId].late}">
			                <span data-toggle="tooltip" data-placement="right" title="Add Late Issue" >
			                <a href="#" data-event="duplicate" data-toggle="modal" data-target="#set-late-atten-issue_${rowstatus.count}"><img class='' src='admin/img/svg/stopwatch.svg' width='20' style="padding-top: 4px;"></a>
			                </span>
			                </c:if> --%>
			                
			            	</div>
			            	</div>
				            	<div class="col-md-3">
				            	<div class="comment">
				            	<div></div>
				                <input type="text" class="comment-box-attattendance-id comment-box input-sm form-control" data-userid-value="${user.userId}" value="${attendanceByDayUserDataMap[user.userId].comment}"
				                data-attendance-id='<c:if test="${attendance !=null}">${attendance.id}</c:if>' data-day-id='<c:if test="${attendanceByDayUserDataMap[user.userId]!=null}">${attendanceByDayUserDataMap[user.userId].id}</c:if>'
				                id="comment-attattendance-input_${rowstatus.count}" data-row-count="${rowstatus.count}" style="color: #F44336;font-weight: 600;" placeholder="Comment..." />
				            	</div>
				            	</div>
			            	</div>
			    </div>
				<br>
				<div class="modal fade" id="set-late-atten-issue_${rowstatus.count}" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title text-center">Late Issue </h4>
							<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
							</button>
						</div>
						<form id="saveUserLateAttendanceForm_${rowstatus.count}" method="post" class="bv-form">
							<input type="hidden" id="quotationId" name="quotationId" value="3">
							<div class="modal-body">
								<div class="row">
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="title">Title</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" name="title" id="title_${rowstatus.count}" class="form-control">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="title">Time</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" name="time" id="time_${rowstatus.count}" class="form-control onlytime">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="description">Description</label>
										<div class="controls">
											<div class="form-group">
												<textarea rows="2" name="description" id="description_${rowstatus.count}" class="form-control"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
								<button type="submit" class="btn btn-primary save-late-atten-issue-btn" id="" 
										data-model-id="set-late-atten-issue_${rowstatus.count}" data-form-name="saveUserLateAttendanceForm_${rowstatus.count}">Save</button>
							</div>
						</form>
					</div>
				</div>
</div>
					</div>  
</c:forEach>
</div>
		
		</c:when>
		<c:otherwise>
		<div class="row">
		<div class="col-md-12">
			<p class="text-warning text-center"><span class="bug-issue-status-max-width-medium follow-status-danger">There is no employee list</span></p>		
		</div>
		</div>
		</c:otherwise>
		</c:choose>
</div>
<%-- <script type="text/javascript" src="admin/js/admin/user-attendance.js"></script> --%>
<script>
/* $(function () {
    $('.onlytime').datetimepicker({
    	format: 'hh:mm A',
    	sideBySide : true,
    	
    }).on('dp.hide', function() {
        $(this).blur();
    });
 }); */
 
/*-------get student list by section wise---------*/
$(".attendance_taking_div div:nth-of-type(5) button").click(function(){ clickAttendanceButton(this, 'success'); });
$(".attendance_taking_div div:nth-of-type(6) button").click(function(){ clickAttendanceButton(this, 'danger'); });
$(".attendance_taking_div div:nth-of-type(7) button").click(function(){ clickAttendanceButton(this, 'warning'); }); 
$(".attendance_taking_div div:nth-of-type(8) button").click(function(){ clickAttendanceButton(this, 'primary'); }); 

function clickAttendanceButton(button, buttonType) {
	$(button).parents('div.attendance_taking_div').find('div.btn-group button').removeClass(function(){return 'btn btn-success btn-danger btn-primary btn-info btn-warning btn-plain';}).addClass('btn').addClass('btn-default');
	$(button).parents('div.attendance_taking_div').find('div.btn-group a').removeClass(function(){return 'btn btn-success btn-danger btn-primary btn-info btn-warning btn-plain';}).addClass('btn').addClass('btn-default');
	$(button).toggleClass('btn-default').toggleClass('btn-'+buttonType);
	if ($(button).next('div.btn-group').length > 0) {
		$(button).next('div.btn-group').find('a').first().toggleClass('btn-default').toggleClass('btn-'+buttonType);
		saveAttendanceDay(button, buttonType, true);
	} else {
		saveAttendanceDay(button, false);
	}
}
/*------save method for attendance by day------*/
function saveAttendanceDay(button, buttonType, needTime) {
	
	 var dataRowCount = $(button).data("row-count");	
		
		var attendDate = $("#attendDate").val();
		var attendanceType = $(button).data("attendance-type"); 
		var attendanceId = $(button).data("attendance-id");
		var dayId = $(button).data("day-id");
		var userId = $(button).data("userid-value");
		
		var urlData = "userId="+userId+"&"+"attendanceType="+attendanceType+"&attendDateFlag="+attendDate+"&attendanceId="+attendanceId+"&id="+dayId;
		$.ajax({
			url : "saveUserAttendance?"+urlData,
			type : "GET",
			dataType: 'json',
			success : function(jsonData)
			{
				if(jsonData.message.status == 'success'){
					alertify.success(jsonData.message.message);
					attendanceId = jsonData.message.id2;
					dayId = jsonData.message.id;
					console.log('AttenId :'+attendanceId);
					console.log('Day Id :'+dayId)
					
					$(".present-attattendance").data('attendance-id', ""+attendanceId);
					$(".absent-attattendance").data('attendance-id', ""+attendanceId);
					$(".leave-attattendance").data('attendance-id', ""+attendanceId);
					$(".late-attattendance").data('attendance-id', ""+attendanceId);
					$(".comment-box-attattendance-id").data('attendance-id', ""+attendanceId);
					
					$("#present-attattendance-btn_"+dataRowCount).data('day-id', ""+dayId);
					$("#absent-attattendance-btn_"+dataRowCount).data('day-id', ""+dayId);
					$("#leave-attattendance-btn_"+dataRowCount).data('day-id', ""+dayId);
					$("#late-attattendance-btn_"+dataRowCount).data('day-id', ""+dayId);
					$("#comment-attattendance-input_"+dataRowCount).data('day-id', ""+dayId);
				}
  				else if(jsonData.message.status == 'error'){
  					alertify.error(jsonData.message.message);
  					}
				
			},
			error: function (request, status, error) {
				alertify.error("User Attendance not be save, try again");
			}
		});
} 

/* save leave comment box data */
$(".comment-box-attattendance-id").focusout(function() { 
	
	    var dataRowCount = $(this).data("row-count");	
		var dayId = $(this).data("day-id");
		var attendanceId = $(this).data("attendance-id");
		var commment = $(this).val();
		$("#status").html();
		console.log(dayId);
		if(dayId >= 0 && dayId != '')
		{
		//var attendDate = $("#attendDate").val();
		//var attendanceType = $(this).data("attendance-type"); 
		//var userId = $(this).data("userid-value");
		
			var urlData = "attendanceId="+attendanceId+"&id="+dayId+"&comment="+commment;
			$.ajax({
				url : "saveAttendanceByDayComment?"+urlData,
				type : "GET",
				dataType: 'json',
				success : function(jsonData)
				{
					if(jsonData.message.status == 'success'){
						alertify.success(jsonData.message.message);
						attendanceId = jsonData.message.id2;
						dayId = jsonData.message.id;
						console.log('AttenId :'+attendanceId);
						console.log('Day Id :'+dayId)
						
						$(".comment-box-attattendance-id").data('attendance-id', ""+attendanceId);
						$("#comment-attattendance-input_"+dataRowCount).data('day-id', ""+dayId);
					}
	  				else if(jsonData.message.status == 'error'){
	  					alertify.error(jsonData.message.message);
	  					}
					
				},
				error: function (request, status, error) {
					notifyDefault("","User Attendance not be save, try again","e");
				}
			});
		}
	else {
			/* $("#status").html("&nbsp;<img class='clippy' src='admin/img/svg/close.svg' width='8'>&nbsp;<span class='text-danger' style='font-size: 12px;'>Please take first attendance as a P or others</span>"); */
			alertify.alert("<span class='text-danger' style='font-size: 14px;'><i class='fa fa-times-circle'></i> Please take attendence before puttinng any comment </span>");
	}
});
</script>
