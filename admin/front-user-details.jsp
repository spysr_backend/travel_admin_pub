<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
<!--************************************
        MAIN ADMIN AREA
    ****************************************-->
<style>
.ms-parent {
	width: 100% !important;
}

.btn-bs-file {
	position: relative;
}
</style>
<!--ADMIN AREA BEGINS-->
<section class="wrapper container-fluid">
	<div class=" ">
		<div class=" ">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">Front User Details</h5>
					<div class="set pull-left"></div>
				</div>
				<div class="row">
					<div class="col-md-12 mt-2">

						<div class="profile-timeline-card">
							<c:choose>
								<c:when test="${userProfile.imagePath != null}">
									<img class="timeline-img-user pull-left"
										src="<s:url action='getImageByPath?imageId=%{userProfile.imagePath}'/>">
								</c:when>
								<c:otherwise>
									<i class="fa fa-user header-icons timeline-img-user pull-left"
										style="box-shadow: none;"></i>
								</c:otherwise>
							</c:choose>
							<div class="dropdown pull-right">
								<button class="btn btn-xs btn-outline-primary" type="button"
									data-toggle="dropdown">
									<i class="fa fa-chevron-down"></i>
								</button>
								<ul class="dropdown-menu">
									<li class=""><a class="" href="frontUserList">Front User List <span class="pull-right"><img class="clippy" src="admin/img/svg/user-list-blue.svg" width="15"></span></a></li>
									<li class=""><a class="" href="getFrontUserWalletTransactionHistory?userId=${id}">Wallet History <span class="pull-right"><img class="clippy" src="admin/img/svg/transaction-blue.svg" width="15"></span></a></li>
									<li class="divider"></li>
									<li class="">
									<a href="resetCompanyUserPassword?id=<s:property value="userProfile.id"/>" class="" data-toggle="modal"> <s:text name="tgi.label.reset_password" /> 
									<span class="pull-right"><img class="clippy" src="admin/img/svg/password-reset-blue.svg" width="16"></span>
									</a></li>
								</ul>
							</div>
							<%-- <form id="imageUploadForm" method="post" enctype="multipart/form-data">
						<label class="btn-bs-file btn btn-xs btn-outline-primary pull-right mr-1" data-toggle="tooltip" data-placement="top" title="Change Profile Image">Browse
		                	<input type="file" name="uploadUserFile" onchange="userProfileUploadFn(${userProfile.id});" />
	            		</label>
	            		</form> --%>
	            			<a href="add_front_user_wallet?userId=${id}" class="btn btn-xs btn-outline-success pull-right mr-1" data-toggle="tooltip" data-placement="top" title="Add User Wallet"><i class="fa fa-plus"></i> Wallet </a>
							<a class="btn btn-xs btn-outline-danger pull-right mr-1" data-event="duplicate" data-toggle="modal" data-target="#update_bug_traker"> <i class="fa fa-pencil"></i>
								Edit
							</a>
							<!-- <a class="btn btn-xs btn-outline-primary pull-right mr-1" href="userRegister">
							<i class="fa fa-plus"></i> Add
						</a> -->
							<div class="">
								<p class="mt-2 mb-4">
									<span> <b class="blue-grey-text"><s:property
												value="showUserDetail.firstName" />&nbsp;<s:property
												value="showUserDetail.middleName" />&nbsp;<s:property
												value="showUserDetail.lastName" /></b></span>
								</p>

							</div>
							<div class="timeline-footer row">
								<div class="col-md-10">
									<div class="col-md-2 col-xs-6 mb-2 mt-2">
										<span class="font-sm"><b> Mobile </b> <br> <small>
										<c:choose>
										<c:when test="${showUserDetail.mobile != null && showUserDetail.mobile != ''}">
										<s:property value="showUserDetail.mobile" />
										</c:when>
										<c:otherwise>
										<span>N/A</span>
										</c:otherwise>
										</c:choose>
										</small> </span>
									</div>
									<div class="col-md-2 col-xs-6 mb-2 mt-2">
										<span class="font-sm"><b> Mail </b> <br> <small>
										<c:choose>
										<c:when test="${showUserDetail.email != null && showUserDetail.mobile != ''}">
										<s:property value="showUserDetail.email" />
										</c:when>
										<c:otherwise>
										<span>N/A</span>
										</c:otherwise>
										</c:choose>
										</small> </span>
									</div>
									<div class="col-md-2 col-xs-6 mb-2 mt-2">
										<span class="font-sm"><b> City </b> <br> <small>
										<c:choose>
										<c:when test="${showUserDetail.userDetail.city != null && showUserDetail.userDetail.city != ''}">
										<s:property value="showUserDetail.userDetail.city" />
										</c:when>
										<c:otherwise>
										<span>N/A</span>
										</c:otherwise>
										</c:choose>
										</small> </span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="profile-timeline-card">
							<div class="">
								<p class="mt-2 mb-4">
									<span class="font-icon"><i class="fa fa-user"></i></span> <span>
										<b class="blue-grey-text">User Details</b>
									</span>
								</p>
							</div>
							<hr>
							<div class="timeline-footer row">
								<div class="col-md-4 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> First Name : </b> <br> <c:choose>
												<c:when
													test="${showUserDetail.firstName != null && showUserDetail.firstName != '' }">
													<small><s:property value="showUserDetail.firstName" /></small>
												</c:when>
												<c:otherwise>
												N/A
												</c:otherwise>
											</c:choose> </span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> Middle Name : </b> <br>
											<c:choose>
												<c:when
													test="${showUserDetail.middleName != null && showUserDetail.middleName != '' }">
													<small><s:property
															value="showUserDetail.middleName" /></small>
												</c:when>
												<c:otherwise>
												N/A
												</c:otherwise>
											</c:choose> </span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> Last Name : </b> <br> <c:choose>
												<c:when
													test="${showUserDetail.lastName != null && showUserDetail.lastName != '' }">
													<small><s:property value="showUserDetail.lastName" /></small>
												</c:when>
												<c:otherwise>
												N/A
												</c:otherwise>
											</c:choose> </span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> Email : </b> <br> <c:choose>
												<c:when
													test="${showUserDetail.email != null && showUserDetail.email != '' }">
													<small><s:property value="showUserDetail.email" /></small>
												</c:when>
												<c:otherwise>
												N/A
												</c:otherwise>
											</c:choose> </span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> Mobile : </b> <br> <c:choose>
												<c:when
													test="${showUserDetail.mobile != null && showUserDetail.mobile != '' }">
													<small><s:property value="showUserDetail.mobile" /></small>
												</c:when>
												<c:otherwise>
												N/A
												</c:otherwise>
											</c:choose> </span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> Birth Date : </b> <br> N/A </span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> City : </b> <br> <c:choose>
												<c:when
													test="${showUserDetail.userDetail.city != null && showUserDetail.userDetail.city != '' }">
													<small><s:property value="showUserDetail.userDetail.city" /></small>
												</c:when>
												<c:otherwise>
												N/A
												</c:otherwise>
											</c:choose> </span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> State : </b> <br> <c:choose>
												<c:when
													test="${showUserDetail.userDetail.state != null && showUserDetail.userDetail.state != '' }">
													<small><s:property value="showUserDetail.userDetail.state" /></small>
												</c:when>
												<c:otherwise>
												N/A
												</c:otherwise>
											</c:choose> </span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> Country : </b> <br> N/A</span>
									</div>
								</div>
								<div class="col-md-12 col-sm-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom pb-2">
										<span class="font-sm"><b> Address : </b> <br> <c:choose>
												<c:when
													test="${showUserDetail.userDetail.address != null && showUserDetail.userDetail.address != '' }">
													<small><s:property
															value="showUserDetail.userDetail.address" /></small>
												</c:when>
												<c:otherwise>
												N/A
												</c:otherwise>
											</c:choose> </span>
									</div>
								</div>
								<hr>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="profile-timeline-card">
								<div class="">
								<p class="mt-2 mb-4">
									<span class="font-icon"><img class="clippy" src="admin/img/svg/wallet-money-orange.svg" width="14" alt="Wallet"></span> <span>
										<b class="blue-grey-text">User Wallet</b>
									</span>
								</p>
							</div>
							<hr>
							<div class="timeline-footer row">
								<c:choose>
								<c:when test="${userWalletBalance != null}">
								<div class="col-md-12 col-xs-12 mb-2 mt-2">
									<div class="pb-2">
									<div class="font-sm">
										<p class="pull-left text-left"><label class="form-control-label">Oxy Pay Balance</label></p> 
										<p class="pull-right text-right text-success"><i class="fa fa-inr" style="font-size:12px"></i>  <fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userWalletBalance.walletBalance}" /></p>
									</div>
									</div>
								</div>
								<div class="col-md-12 col-xs-12">
									<div class="pb-2">
										<p class="pull-left text-left">Oxy Money</p> <p class="pull-right text-right"><i class="fa fa-inr" style="font-size:12px"></i> 
										<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userWalletBalance.depositBalance}" /></p>
									</div>
								</div>
								<div class="col-md-12 col-xs-12">
									<div class="pb-2">
										<p class="pull-left text-left">Oxy Back</p> <p class="pull-right text-right"><i class="fa fa-inr" style="font-size:12px"></i> 
										<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userWalletBalance.cashbackBalance}" /></p>
									</div>
								</div>
								<div class="col-md-12 col-xs-12">
									<div class="pb-2">
										<p class="pull-left text-left">Oxy Credit</p> <p class="pull-right text-right"> <span>Pt.</span>
										<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userWalletBalance.creditBalance}" />
										</p>
										<%-- <br/>
										<span class="text-right">1 oxyCredit = <i class="fa fa-inr" style="font-size:12px"></i> 1</span> --%>
										
									</div>
								</div>
							</c:when>
							<c:otherwise>
							<div class="col-md-12 col-xs-12 mb-2 mt-2">
									<div class="pb-2">
									<div class="font-sm">
										<p class=" text-center">No Wallet Found</p>
									</div>
									</div>
								</div>
							</c:otherwise>
							</c:choose>
							</div>
						</div>
					</div>

				</div>


				<div id="user_edit_detail">
					<div class="modal fade" id="update_bug_traker" role="dialog">
						<div class="modal-dialog modal-lg">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title text-center">Edit Front User
										Details</h4>
									<button type="button" class="close slds-modal__close"
										data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
									</button>
								</div>
								<form id="updateUserProfileForm" name="updateUserProfileForm" method="post" class="bv-form">
									<input type="hidden" name="id" value="<s:property value="showUserDetail.id"/>">
									<div class="modal-body">
										<div class="row">
											<h4 class="slds-section__title slds-theme--shade"
												style="font-weight: 500">
												<span class="section-header-title slds-p-horizontal--small slds-truncate">
													User Details</span>
											</h4>
											<div class="col-md-4">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>First Name</label>
												<div class="controls">
													<div class="form-group">
														<input type="text" class="form-control input-sm"
															id="first-name" name="firstName"
															value="<s:property value="showUserDetail.firstName"/>"
															placeholder="First Name" autocomplete="off" >
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>Middle Name</label>
												<div class="controls">
													<div class="form-group">
														<input type="text" class="form-control input-sm"
															id="middleName" name="middleName"
															value="<s:property value="showUserDetail.middleName"/>"
															placeholder="Middle Name" autocomplete="off">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>Last Name</label>
												<div class="controls">
													<div class="form-group">
														<input type="text" class="form-control input-sm"
															id="last-name" name="lastName"
															value="<s:property value="showUserDetail.lastName"/>"
															placeholder="Last Name" autocomplete="off" >
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>Email</label>
												<div class="controls">
													<div class="form-group">
														<input type="email" class="form-control input-sm" value="<s:property value="showUserDetail.email"/>" placeholder="Email" autocomplete="off" disabled="disabled">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>Mobile</label>
												<div class="controls">
													<div class="form-group">
														<input type="tel" class="form-control input-sm"
															name="mobile" id="mobile"
															value="<s:property value="showUserDetail.mobile"/>"
															placeholder="Mobile" autocomplete="off" >
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>Birth Date</label>
												<div class="controls">
													<div class="form-group">
														<input type="tel" class="form-control input-sm" name="mobile" id="mobile" value="<s:property value="showUserDetail.birthDate"/>">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>City</label>
												<div class="controls">
													<div class="form-group">
														<input type="tel" class="form-control input-sm"
															name="city" id="city"
															value="<s:property value="showUserDetail.city"/>"
															placeholder="City" >
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>State</label>
												<div class="controls">
													<div class="form-group">
														<select class="form-control input-sm" name="state" id="state" >
															<option selected  value="">Select State</option>
															<c:forEach items="${statesList}" var="states">
																<option value="${states.state}" ${states.state == showUserDetail.state ? 'selected': ''}>${states.state}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>Country</label>
												<div class="controls">
													<div class="form-group">
														<select class="form-control input-sm" name="country" id="country" >
															<option selected  value=""><s:text name="tgi.label.select_country" />	</option>
															<c:forEach items="${CountryList}" var="country">
																<option value="${country.countryName}" ${country.countryName == showUserDetail.country ? 'selected': ''}>${country.countryName}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<label class="form-control-label" for="comment"><span
													class="text-danger"></span>Address</label>
												<div class="controls">
													<div class="form-group">
														<textarea class="form-control input-sm" id="streetAddress" name="streetAddress"><s:property value="showUserDetail.streetAddress" /></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4 col-sm-4">
											    <label for="inputEmail3" class="control-label"> Mail Verify Status</label>
											     <div class="checkbox <c:choose><c:when test="${showUserDetail.emailVerified == true}">checkbox-success</c:when><c:otherwise>checkbox-default</c:otherwise></c:choose>">
							                        <input type="checkbox" name="emailVerified" id="verify-status"  value="${showUserDetail.emailVerified}" class="check_row" ${showUserDetail.emailVerified == true ? 'checked': ''}>
							                        <label for="verify-status">&nbsp;Verify</label>
										    </div>
										</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
										<button type="submit" class="btn btn-success" id="update_bug_trakerBtn">Update</button>
									</div>
								</form>
							</div>

						</div>
					</div>
				</div>
				<!--  -->
				<div id="popover-content" class="hidden">
		        <div class="row">
		        <table class="table table-bordered table-striped-column table-hover mb-0">
                               <thead>
                               <tr class="small-trip-table">
					<th>SNo</th>
					<th>Mail Status</th>
					<th>Send At</th>
					</tr>
				</thead>
					<tbody>
					<c:forEach items="${emailCountStatusList}" var="emailData" varStatus="estatus">
					<tr class="small-trip-table">
					<td>${estatus.count}</td>
					<td>
					<c:choose>
					<c:when test="${emailData.mailStatus == 1}">
					<span class="text-success">Success</span>
					</c:when>
					<c:when test="${emailData.mailStatus == 0}">
					<span class="text-warning">Pending</span>
					</c:when>
					<c:when test="${emailData.mailStatus == -1}">
					<span class="text-danger">Failed</span>
					</c:when>
					</c:choose>
					</td>
					<td>
					${spysr:formatDate(emailData.createdAt,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy hh:mm a')}
					</td>
					</tr>
					</c:forEach>
				</tbody></table>
		        </div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$("#updateUserProfileForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			url : "updateUserDetails",
			type : "POST",
			dataType : 'json',
			data : $("#updateUserProfileForm").serialize(),
			success : function(jsonData) {
				alertify.success("Front USer was updated successfully")
				setTimeout(location.reload.bind(location), 1000);
			},
			error : function(request, status, error) {
				alertify.error("Front User can't be updated,please try again");
			}
		});
	});
	 
  $("[data-toggle=popover]").popover({
    html: true, 
   // var popoverId = $(this).attr("data-email-content")
	content: function() {
          return $('#popover-content').html();
        }
}).blur(function() {
    $(this).popover('hide');
});
</script>
