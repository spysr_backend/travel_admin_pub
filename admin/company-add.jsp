<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.*" %>
<style>
	.ms-choice {
	    height: 30px !important;
	    font-size: 12px !important;
    }
</style>
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addCompany";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<script type="text/javascript">
 function isNumberKey(evt,obj){
	    var charCode = (evt.which) ? evt.which : event.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57))   
	    
	        return false;
	   
	}
	function InvalidMsg(textbox) {
	    
	    if(textbox.validity.patternMismatch){
	       textbox.setCustomValidity('Mobile number not vaild.');
	   }    
	   else {
	       textbox.setCustomValidity('');
	   }
	   return true;
	}
	function InvalidEmailMsg(textbox) {
	    
	    if(textbox.validity.patternMismatch){
	       textbox.setCustomValidity('Email address not vaild.');
	   }    
	   else {
	       textbox.setCustomValidity('');
	   }
	   return true;
	}

	function onlyAlphabets(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                return true;
            else
                return false;
        }
        catch (err) {
            alert(err.Description);
        }}

 </script>
            <section class="wrapper container-fluid">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.add_company" /></h5>
                        	  <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-outline-primary" href="companyList"><span class="fa fa-arrow-left"></span> Back To List </a>
									</div>
									</div>
                               </div> 
                            </div>
               <form action="companyRegister" method="post" id="company-register" name="myForm" enctype="multipart/form-data">
 						<div class="row">
							<div class="col-md-12">
								<input type="hidden" name="currencyCode" value="${session.Company.currencyCode}">
                    			<div class="profile-timeline-card">
                    			<div class="">
									<p class="mt-1 mb-2">
										<span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Company Details</b></span>
									</p>
								</div>
								<hr>
                    			<div class="row row-minus">
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.company_name" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm" id="company" name="companyName" value=" " placeholder=" " autocomplete="off" ></div></div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.email" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input class="form-control input-sm"  type="email" name="email" value="" required="required"/></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="mobile"><s:text name="tgi.label.phone" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
												<input type="tel" maxlength="14" class="form-control input-sm" name="mobile" id="mobile" value="" placeholder="080-567895454" autocomplete="off"   ></div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.upload_images" /> 
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input name="uploadFile" type="file" class="form-control input-sm"></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput">Language
											</label>
										<div class="controls">
												<select class="form-control input-sm" name="language" id="Language" >
													<option selected="selected" value=""><s:text name="tgi.label.select_language" /></option>
													<c:forEach items="${LanguageList}" var="itemType">
																	<option value="${itemType.language}" ${itemType.id == 'English'  ? 'selected': ''}>${itemType.language}</option>
													</c:forEach>
												</select>
										</div>
									</div>
									<div class="col-md-9">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.address" />
											</label>
										<div class="controls">
											<div class="form-group">
												<textarea class="form-control input-sm" id="address" name="companyDetail.address" placeholder="Address" rows="1"></textarea>
											</div>
										</div>
									</div>
                    				<div class="col-md-3">
										<label class="form-control-label" for="city"><s:text name="tgi.label.city" />
											</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" class="form-control input-sm" name="companyDetail.city" id="city" value="" autocomplete="off" >
											</div>
										</div>
									</div>
                    				<div class="col-md-3">
										<label class="form-control-label" for="zip-code">Zip Code
											</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" class="form-control input-sm" name="companyDetail.zipCode" id="zip-code" value="<s:property value="zipCode"/>" autocomplete="off" >
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput">State
											</label>
										<div class="controls">
											<div class="form-group">
												<select class="form-control input-sm" name="companyDetail.state" id="state" >
													<option selected  value="">Select State</option>
													<c:forEach items="${statesList}" var="states">
														<option value="${states.state}">${states.state}</option>
													</c:forEach>
					
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.country" />
											</label>
										<div class="controls">
											<div class="form-group">
												<select class="form-control input-sm" name="companyDetail.countryName" id="country" >
													<option selected  value=""><s:text name="tgi.label.select_country" />	</option>
													<c:forEach items="${CountryList}" var="country">
														<option value="${country.countryName}" ${country.countryName == 'India' ? 'selected': ''}>${country.countryName}</option>
													</c:forEach>
					
												</select>
											</div>
										</div>
									</div>
									</div>	
					</div>
					</div>
					<div class="col-md-12">
                    <div class="profile-timeline-card">
                    			<div class="">
									<p class="mt-1 mb-2">
										<span class="company-font-icon"> <img class="clippy" src="admin/img/svg/services.svg" width="16" alt="Login"> </span> <span> <b class="blue-grey-text"> &nbsp;Company Services</b></span>
									</p>
								</div>
								<hr>
				                    <div class="row row-minus">
				                    <div class="row">
				                    <div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.company_type" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control input-sm" name="companyType" id="companyType">
													<s:if test="%{#session.Company!=null && #session.User!=null}">
														<s:if test="%{#session.User.userRoleId.isSuperUser()}">
															<option selected value="agent"><s:text name="tgi.label.agency" /></option>
															<option value="distributor"><s:text name="tgi.label.distributor" /></option>
														</s:if>
														<s:elseif
															test="%{#session.Company.companyRole.isDistributor()}">
															<option selected value="agent"><s:text name="tgi.label.agency" /></option>
														</s:elseif>
					
														<s:elseif test="%{#session.User.userRoleId.isAdmin()}">
															<s:if test="%{#session.User.companyId==#session.Company.companyId}">
																<s:if test="%{#session.Company.companyRole.isDistributor()}">
																	<option selected value="agent"><s:text name="tgi.label.agency" /></option>
																</s:if>
																<s:else>
																	<option selected value="agent"><s:text name="tgi.label.agency" /></option>
																	<option value="distributor"><s:text name="tgi.label.distributor" /></option>
																</s:else>
															</s:if>
														</s:elseif>
													</s:if>
											</select></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-3 distributor-type-div" style="display:none;">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.distributor" />
											</label>
										<div class="controls"><div class="form-group">
												<select class="form-control input-sm distributor-type-div" name="distributorType"
													id="distributorType" style="display:none;" >
													<option selected value="ibe">IBE</option>
													<option value="api">API</option>
												</select>
										</div></div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.wallet_type" /></label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control input-sm" name="typeOfWallet" id="typeOfWallet" >
													<s:if test="%{#session.Company!=null && #session.User!=null}">
													<s:if test="%{#session.User.userRoleId.isSuperUser()}">
													<option selected value="" ><s:text name="tgi.label.select_wallet_type" /></option>
													<option  value="Prepaid"><s:text name="tgi.label.prepaid" /></option>
													<option value="Postpaid"><s:text name="tgi.label.postpaid" /></option>
													</s:if>
													<s:elseif test="%{#session.Company.companyRole.isDistributor()}">
													<option  value="Prepaid"><s:text name="tgi.label.prepaid" /></option>
													<option value="Postpaid"><s:text name="tgi.label.postpaid" /></option>
													</s:elseif>
													<s:elseif test="%{#session.User.userRoleId.isAdmin()}">
														<s:if test="%{#session.User.companyId==#session.Company.companyId}">
															<s:if test="%{#session.Company.companyRole.isDistributor()}">
																<option  value="Prepaid"><s:text name="tgi.label.prepaid" /></option>
																<option value="Postpaid"><s:text name="tgi.label.postpaid" /></option>
															</s:if>
															<s:else>
															<option selected value="" ><s:text name="tgi.label.select_wallet_type" /></option>
																<option  value="Prepaid"><s:text name="tgi.label.prepaid" /></option>
																<option value="Postpaid"><s:text name="tgi.label.postpaid" /></option>
															</s:else>
														</s:if>
													</s:elseif>
												 </s:if>
											</select> </div></div>
											</div>
										</div>
									</div>
									<div class="col-md-3 Wallet-type-div" style="display: none">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.postpaid_amount_range" /></label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="number" style="display: none" onkeypress="return isNumberKey(event,this)" class="Wallet-type-div form-control input-sm" id="postAmount" name="postAmount"  
													value="<s:property value="postAmount"/>" autocomplete="off" > </div></div>
											</div>
										</div>
									</div>
									</div>
									<div class="row">
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.company_level" /></label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control input-sm" name="companyVisibilityType">
                                                        <option value="">Select Company Level</option>
                                                        <option value="b2b">B2B</option>
                                                        <option value="b2c">B2C</option>
                                                        <option value="b2b2c">B2B2C</option>
                                                        <option value="b2b2b2c">B2B2B2C</option>
                                                 </select> </div></div>
											</div>
										</div>
									</div>
                    				<div class="col-md-4">
										<label class="form-control-label" for="appendedInput">Company Service
											</label>
										<div class="controls">
											<div class="form-group">
												<select class="input-md" id="companyServiceType" name="companyBookableServiceArray" multiple="multiple" >
														<c:forEach items="${bookableServiceListMap}" var="bookingService">
																	<option value="${bookingService.key}">${bookingService.value}</option>
														</c:forEach>
												</select> 
											</div>
											</div>
											</div>
                    				<div class="col-md-4">
										<label class="form-control-label" for="appendedInput">Company CMS Service </label>
										<div class="controls">
											<div class="form-group"><div class="">
												<select class="input-md" id="companyCMSType" name="companyCMSServiceArray" multiple="multiple" >
															<c:forEach items="${cmsServiceListMap}" var="cmsService">
																	<option value="${cmsService.key}">${cmsService.value}</option>
															</c:forEach>
													</select> 
													</div></div>
										</div>
									</div>
									
					</div>	
									
					</div>	
					</div>
					</div>
					<!--// end company services -->
					<div class="col-md-12">
					<div class="profile-timeline-card">
						<p class="mt-1 mb-2">
							<span class="company-font-icon"><img class="clippy" src="admin/img/svg/log-in.svg" width="16" alt="Login"> </span><b class="blue-grey-text"> &nbsp;Login Credential</b>
						</p>
						<hr>
					<div class="row row-minus">
                    				<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.first_name" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm" id="firstName" name="user.firstName" value="${companyRegister.firstName}" placeholder="First Name"  required="required"/></div></div>
											</div>
										</div>
									</div>
                    				<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.last_name" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm" id="last_name" name="user.lastName" value="${companyRegister.lastName}" placeholder="Last Name" required="required" /></div></div>
											</div>
										</div>
									</div>
                    				<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.email" /></label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="email" class="form-control input-sm" name="user.email" id="email-address" value="<s:property value="email"/>"  placeholder="Email" autocomplete="off"  required="required">
												<div id="email-status"></div>
												</div></div>
											</div>
										</div>
									</div>
                    				<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.password" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input name="user.password" id="password" type="password" class="form-control input-sm" value="${companyRegister.password}" required="required"></div></div>
											</div>
										</div>
									</div> 
					</div>	   
					</div>
					</div>	
					</div>
					<div class="row">
					<div class="col-md-12">
						<div class="set pull-right" style="margin-bottom: 20px;margin-top: 7px;">
							<div class="form-actions">
								<input type="hidden" name="leadId" value="${param.companyLeadId}">
								<button class="btn btn-default btn-danger" id="tourCancelBtn" type="reset">Reset</button>
								<button type="submit" class="btn btn-primary user-save-btn" id="tourEditBtn">Submit</button>
							</div>
							</div>	
					</div>
					</div>
                                    </form>
                                    </div>
                                    
            </section>
<script src="admin/js/admin/user-validators.js"></script>            
<script type="text/javascript">
	$(document).ready(function() {
		$("#companyServiceType").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a Company Services",
		});
	
	});
	
 $(document).ready(function() {
		$("#companyCMSType").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a Company CMS",
		});
	}); 
</script> 
            <script type="text/javascript">
$(document).ready(function() {
$('#company-register')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
		companyName : {
					message : 'Companyname is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Company Name'
						},
						stringLength : {
							min : 3,
							max : 50,
							
							message : 'Companyname must be more than 3 and less than 100 characters long'
						} 
					}
				},
				companyType : {
					message : 'companyType is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Company Type'
						},
					}
				},
				distributorType : {
					message : 'Distributor is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Distributor Type'
						},
					}
				},
				typeOfWallet : {
					message : 'typeOfWallet is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Wallet Type'
						},
					}
				},
				postAmount : {
					message : 'typeOfWallet is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Enter a Amount'
						},
					}
				},
				currencyCode : {
					message : 'Currency  is not valid',
					validators : {
						 notEmpty : {
							message : 'select Enter a Currency'
						},
					}
				},
				website: {
		                validators: {
		                	notEmpty : {
								message : 'Please enter a website'
							},
		                    uri: {
		                        message: 'The website address is not valid'
		                    }
		                }
		            },
		        userName : {
					message : 'username is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a user name'
						},
						stringLength : {
							min : 3,
							max : 50,
							
							message : 'UserName must be more than 3 and less than 100 characters long'
						} 
					}
				},
				password : {
					message : 'password is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a password'
						},
						/* stringLength : {
							min : 6,
							max : 50,
							
							message : 'Password must be more than 6 and less than 100 characters long'
						}  */
					}
				},
				email : {
					message : 'Email  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a email'
						},
					}
				},
				CountryName : {
					message : 'Countryname  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a country'
						},
					}
				},
				/* Language : {
					message : 'Language  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a language'
						},
					}
				}, */
				phone : {
					message : 'Phone  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Phone'
						},
						integer: {
	                        message: 'The value is not a Phone Number'
	                    },
						 stringLength : {
								min : 4,
								max : 18,
								message : 'Phone must be more than 4 and less than 15 characters long'
						}  
					}
				},
				companyDescription : {
					message : 'Description  is not valid',
					validators : {
						/*  notEmpty : {
							message : 'Please enter a Description '
						}, */
					}
				},
				billingCompany : {
					message : 'Company Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Company Name'
						},
						stringLength : {
							min : 3,
							max : 50,
							
							message : 'Companyname must be more than 3 and less than 50 characters long'
						} 
					}
				},
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {

	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>
        
	<script type="text/javascript">
$(function() {
  
    $('#companyType').change(function(){
    	 if($('#companyType').val()== 'distributor') {
            $('.distributor-type-div').show(); 
        } 
        else if($('#companyType').val() == 'agent') {
        	 $('.distributor-type-div').hide(); 
       } 
    });
    
    $('#typeOfWallet').change(function(){
   	 if($('#typeOfWallet').val()== 'Postpaid') {
           $('.Wallet-type-div').show();
        } 
       else if($('#typeOfWallet').val() == 'Prepaid') {
       	 $('.Wallet-type-div').hide(); 
      } 
       
   });
    
    
    
   
 });
 </script>
<s:if test="message != null && message  != ''">
 <script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
<c:choose>
<c:when test="${fn:contains(message, 'successfully')}">
          	  <script>
          	  $(document).ready(function() 
          			  {
          	  $('#alert_box').modal({
 	        	    show:true,
 	        	    keyboard: false
 	    	    } );
 			  $('#alert_box_body').text('${param.message}');
          			  });
	</script>
</c:when>
<c:otherwise>
          	  <script>
          	  $(document).ready(function() 
          			  {
          		  $('#alert_box_info').modal({
   	        	    show:true,
   	        	    keyboard: false
   	    	    } );
   			  $('#alert_box_info_body').text("${param.message}");
          			  });
	</script>
</c:otherwise>
</c:choose>
</s:if>
