<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"insert_countryinfo_page";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<script type="text/javascript">
$(document).ready(function()
{
	$("#country").val(223);
	$("#Language").val('English');
});
</script>

<section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.add_country_info" /></h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                               <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="fetch_countryinfo"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.country_info_list" /></a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div>
                           <%--  <s:if test="hasActionErrors()">
						<div class="succfully-updated clearfix" id="error-alert">

							<div class="col-sm-2">
								<i class="fa fa-check fa-3x"></i>
							</div>

							<div class="col-sm-10">

								<p>
									<s:actionerror />
								</p>

								<button type="button" id="cancel" class="btn btn-primary">Ok</button>

							</div>

						</div>


					</s:if>

					<s:if test="hasActionMessages()">
						<div class="sccuss-full-updated" id="success-alert">
							<div class="succfully-updated clearfix">

								<div class="col-sm-2">
									<i class="fa fa-check fa-3x"></i>
								</div>

								<div class="col-sm-10">
									<s:actionmessage />
									<button type="button" id="success" class="btn btn-primary">Ok</button>

								</div>

							</div>
						</div>
					</s:if> --%>

<div class="cnt cnt-table">
                                <div class="no-table-css">
                                   <form action="insert_countryinfo" method="post" class="form-horizontal">
                                    <table class="table table-form">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>&nbsp;</th>
                                               
                                                
                                            </tr>
                                        </thead>
     <tbody>
                                            <tr>

                                                <td>
                                                    <div class="field-name">Country Name : </div>
                                              </td>
	 <td>
	 <select class="form-control input-sm" name="countryId" id="country">
	 <s:iterator value="countryList" >
	 <option value="${countryID}">${countryName}</option>
	 </s:iterator>
	 </select>
    <%--   <select class="form-control input-sm" name="country" id="country"
								autocomplete="off" required listValue="nameIsdCode" name="countryId" value="223" class="form-control input-sm"  />	</select> --%></td>
      
     
                                                <td>
                                                    <div class="field-name">Language </div>
                                                </td>
     <td>
     <select class="form-control input-sm" name="language"
								id="Language" required>
								
								<option selected="selected" value="">Select Language</option>
								<s:iterator value="LanguageList">
								<option value='<s:property value="language"/>'><s:property
											value="language" /></option>
								</s:iterator>

							</select>
        
      </td>
      </tr>
	  <tr>
	  <td>
                                                    <div class="field-name">Population  </div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="population"></textarea>
      </td>
       <td>
                                                    <div class="field-name">Capital City</div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="capitalCity"> </textarea>
      </td>
      </tr>
	 <tr>
	  <td>
                                                    <div class="field-name">People</div>
                                                </td>
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="people"></textarea>
      </td>
      <td>
                                                    <div class="field-name">Geographical Area</div>
                                                </td>
     <td>
     
     <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="geoArea"></textarea>
        <!-- <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="geoArea"> -->
      </td>
      </tr>
	   <tr>
	  <td>
                                                    <div class="field-name"><s:text name="tgi.label.religion" /></div>
                                                </td>
   
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="religion"></textarea>
      </td>
     
      
      <td>
                                                    <div class="field-name"><s:text name="tgi.label.goverment" /> </div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="govt"></textarea>
      </td>
     
      
      
      </tr>
	  
	  <tr>
	  <td>
                                                    <div class="field-name"><s:text name="tgi.label.general_info" /></div>
                                                </td>
   
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="generalInfo"></textarea>
      </td>
     
      
      <td>
                                                    <div class="field-name"><s:text name="tgi.label.head_of_government" /></div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="headOfGovt"></textarea>
      </td>
     
      
      
      </tr>
	  
	 
	  <tr>
	  <td>
                                                    <div class="field-name"><s:text name="tgi.label.embassy" /></div>
                                                </td>
   
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="embassy"></textarea>
      </td>
     
      
      <td>
                                                    <div class="field-name"><s:text name="tgi.label.visas" /></div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="visas"></textarea>
      </td>
     
      
      
      </tr>
	  
	 
	  <tr>
	  <td>
                                                    <div class="field-name"><s:text name="tgi.label.health_risks" /></div>
                                                </td>
   
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="healthRisk"></textarea>
      </td>
     
      
      <td>
                                                    <div class="field-name"><s:text name="tgi.label.clothing_suggestions" /></div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="clothing"></textarea>
      </td>
     
      
      
      </tr>
	  
	 
	  <tr>
	  <td>
                                                    <div class="field-name"><s:text name="tgi.label.time_zone" /></div>
                                                </td>
   
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="timeZone"></textarea>
      </td>
     
      
      <td>
                                                    <div class="field-name"><s:text name="tgi.label.banking_exchange" /></div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="banking"></textarea>
      </td>
     
      
      
      </tr>
	  
	 
	  <tr>
	  <td>
                                                    <div class="field-name"><s:text name="tgi.label.currency" /></div>
                                                </td>
   
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="currency"></textarea>
      </td>
     
      
      <td>
                                                    <div class="field-name"><s:text name="tgi.label.electric_current" /></div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="electricCurrent"></textarea>
      </td>
     
      
      
      </tr>
	  
	 
	  <tr>
	  <td>
                                                    <div class="field-name"><s:text name="tgi.label.credit_cards_traveler_checks" /></div>
                                                </td>
   
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="creditCard"></textarea>
      </td>
     
      
      <td>
                                                    <div class="field-name"><s:text name="tgi.label.weights_measures" /></div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="weightMeasure"></textarea>
      </td>
     
      
      
      </tr>
      <tr>
	  <td>
                                                    <div class="field-name"><s:text name="tgi.label.how_to_shop_tax_fee" /></div>
                                                </td>
   
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="taxFee"></textarea>
      </td>
     
      
      <td>
                                                    <div class="field-name"><s:text name="tgi.label.national_airport" /></div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="nationalAirport"></textarea>
      </td>
     
      
      
      </tr>
      <tr>
	  <td>
                                                    <div class="field-name"><s:text name="tgi.label.major_tourist_attractions" /></div>
                                                </td>
   
      <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="touristAttraction"></textarea>
      </td>
     
      
      <td>
                                                    <div class="field-name"><s:text name="tgi.label.tipping_suggestion" /></div>
                                                </td>
     <td>
        <textarea rows="4" cols="50" class="form-control input-sm" id="focusedInput"   name="tipping" ></textarea>
      </td>
     
      
      
      </tr>
      
	 </tbody>
                                    </table>
                                    <div class="form-group table-btn">
                                        <div class="col-sm-10 col-sm-offset-2 ">
                                            <%-- <div class="pull-left"><button class="btn btn-danger" type="reset"><s:text name="tgi.label.clear_form" /></button></div> --%>
                                            <div class="pull-right">
                                                
                                                <button class="btn btn-danger" type="reset"><s:text name="tgi.label.reset" /></button>
                                                <button class="btn btn-primary " type="submit"><s:text name="tgi.label.submit" /></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    <br><br><br>
	  	  
	  	   </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
						
						
						<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>