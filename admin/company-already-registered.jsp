<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<style>
.form-horizontal-login{
background: rgba(255, 255, 255, 0.25);
    padding: 50px;
    /* border: 10px solid #fff; */
    border-radius: 10px;
    box-shadow: 0 16px 24px 2px rgba(0,0,0,0.14), 0 6px 30px 5px rgba(0,0,0,0.12), 0 8px 10px -5px rgba(0,0,0,0.3);
    /* background: linear-gradient(rgba(0, 0, 0, 0.25) , rgba(255, 255, 255, 0.25)); */
}

h2.login-heading{
 	color: white;
    text-shadow: 2px 2px 5px rgba(0, 0, 0, 0.5);
}
.btn-purple{
  	color: #fff !important;
    background-color: #000316;
    border-color: #03051a;
    box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.25);
    border-radius: 4px;
}
.input-sm {
    height: 30px;
    padding: 20px 15px;
    font-size: 15px;
    line-height: 1.5;
    border-radius: 4px;
    border: none;
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.25);
}
ul.errorMessage {
    color: white;
    text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.25);
    padding-bottom: 20px;
    text-align: center;
}
</style>
    <body style="background-image: url(admin/img/background.jpg);">
<hr class="colorgraph colorgraph-header">

<div class="container">

<div class="row" style="margin-top:100px">
<div class="col-md-3">
</div>
    <div class="col-md-6">
    <s:if test="hasActionErrors()">
    <s:actionerror />
    </s:if>
    <s:if test="hasActionMessages()">
	 <s:actionmessage />
	 </s:if>
	 <h1>404</h1>
	</div>
</div>

</div>

   </body> 
