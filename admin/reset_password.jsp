<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
	
<link rel="stylesheet" href="admin/css/bootstrap.min.css">
<script src="admin/js/bootstrap.min.js"></script>
<script src="admin/js/jquery.min.js"></script>
<script>
$(document).ready(function()
		{	
			if($("#oneTimePassword").val() != null && $("#oneTimePassword").val() != "")
				{
				$(".emailSendMsg").hide();
				$(".resetPasswordForm").show();
				}
			else
				{
				$(".resetPasswordForm").hide();
				$(".emailSendMsg").show();
				}
				
				
		});

</script>


<div class="container">
	<div class="row" style="margin-top: 20px">
	<div class="emailSendMsg">
	<h1><s:text name="tgi.label.check_your_email_for_reset_passwd" /></h1>
	</div>
	<div class="resetPasswordForm col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form action="email_reset_password" role="form" method="post">
				<fieldset>
					<h2><s:text name="tgi.label.reset_passwd" /></h2>
					<hr class="colorgraph">
					<div class="form-group">
					</div>
					<div class="form-group">
						<input type="password" name="Password" id="Password"
							class="form-control input-lg" placeholder="Password" required="required">
					</div>
					<div class="form-group">
						<input type="password" name="pw2" id="pw2" class="form-control input-lg"
							placeholder=" Confirm Password" required="required">
					</div>
					<hr class="colorgraph">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-12">
							<input type="hidden" name="email" value="${param.email}">
							<input type="hidden" name="oneTimePassword" id="oneTimePassword" value="${param.OTP}"> 
								<input type="submit" class="btn btn-lg btn-success btn-block"
								value="Reset Password" onclick="checkpw();">
						</div>

					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
<script>
function checkpw() 
		{
			if($("#Password").val() != $("#pw2").val())
				{
					alert("Password not match ! Please try again");
					$("#Password").val("");
					$("#pw2").val("");
					return false;
				}
	
		}

</script>