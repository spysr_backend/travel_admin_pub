<%@ page language="java" isELIgnored="false"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>

<title>Insert title here</title>
<script type="text/javascript">
$(document).ready(function()
		{
	$("#tripType").val($("#type").val());
		});
</script>




<div class="container ">
<div class="hd">
</div>
<div class = "main-wrapper">
<s:if test="destinationDealsVO != null">
<form action="admin_dest_updatedeals"  method="POST" enctype="multipart/form-data">
<div class="form-group row">
   <div class="col-md-3">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.origin" /></label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text" name="depCity" value="${destinationDealsVO.depCity}" value="">
      </div></div></div>
    <div class="col-md-3">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.airport" /></label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text" name="depAirport" value="${destinationDealsVO.depAirport}" value="">
      </div></div></div> 
      <div class="col-md-3">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.destination" /></label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text"name="destCity" value="${destinationDealsVO.destCity}" value="">
      </div></div></div>
      <div class="col-md-3">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.airport" /></label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text" name="destAirport" value="${destinationDealsVO.destAirport}" value="">
      </div></div></div>
      
      
      
      
      </div>
	 <div class="form-group row">
      <div class="col-md-3">
	  <div class="row">
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.trip_type" /></label>
      <div class="col-sm-8">
         <select class="form-control" id="type" value="${destinationDealsVO.tripType}">
                                          <option value="Ony way"><s:text name="tgi.label.ony_way" /></option>
<option value="Round Trip"><s:text name="tgi.label.round_trip" /></option>
<option value="Multi Trip"><s:text name="tgi.label.multi_trip" /></option>
</select>
                                       
      </div></div></div>
       <div class="col-md-3">
	  
	 
      
      <input class="form-control" id="focusedInput" type="text"  name="price" value="${destinationDealsVO.price}"value=""></div>
      <div class="col-md-3">
	  <div class="row">
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.link" /></label>
      <div class="col-sm-8">
      <input class="form-control" id="focusedInput" type="text" name="href" value="${destinationDealsVO.href}" value=""></div></div></div>
      <div class="col-md-3">
	  <div class="row">
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.departure_period" /></label>
      <div class="col-sm-8">
      <input class="form-control" id="focusedInput" type="text" name="departurePeriod" value="${destinationDealsVO.departurePeriod}" value=""></div></div></div></div>
      
      <div class="form-group row">
   <div class="col-md-3">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.days" /></label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text" name="days" value="${destinationDealsVO.days}"value="">
      </div></div></div>
    <div class="col-md-3">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.advance_ticketing" /></label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text"  name="advanceTicketing" value="${destinationDealsVO.advanceTicketing}"value="">
      </div></div></div> 
      <div class="col-md-3">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.advance_purchase" /></label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text" name="advancePurchase" value="${destinationDealsVO.advancePurchase}"value="">
      </div></div></div>
      <div class="col-md-3">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.purchasing_period" /> </label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text"name="purchasingPeriod" value="${destinationDealsVO.purchasingPeriod}" value="">
      </div></div></div></div>
      
       <div class="form-group row">
      
       <div class="col-md-6">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.minimum_stay" /> </label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text" name="minimumStay" value="${destinationDealsVO.minimumStay}"value="">
      </div></div></div> 
      <div class="col-md-6">
	  <div class="row">
	  
	  <label class="col-sm-4 control-label"><s:text name="tgi.label.maximum_stay" /> </label>
      <div class="col-sm-8">
         <input class="form-control" id="focusedInput" type="text"name="maximumStay" value="${destinationDealsVO.maximumStay}" value="">
      </div></div></div>
      </div>
      
      <div class="form-group row">
      <div class= "col-sm-6"><div class = "row"><label class="col-sm-4 control-label"><s:text name="tgi.label.cityguide_information" /> </label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control"  rows="7" cols="50" name="cityGuideInfo">${destinationDealsVO.cityGuideInfo}</textarea>
                                    </div></div></div>
                                <div class= "col-sm-6">
                                <div class = "row">
                                <label class="col-sm-4 control-label"><s:text name="tgi.label.description" /></label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" rows="7" name="description"> ${destinationDealsVO.description}</textarea>
                                        <input type="hidden" name="id" value="${destinationDealsVO.id}" >
                                        <input type="hidden" name="version" value="${destinationDealsVO.version}" >
                                    </div></div>
                                </div></div>
      <div class="form-group row">
      <label class="col-sm-2 control-label"><s:text name="tgi.label.upload_images" /></label>
	  <div class="col-sm-10">
<input id="lefile" type="file" style="display:none">
<div class="input-append">
<input id="photoCover" class="input-large" type="text">
<a class="btn" onclick="$('input[id=lefile]').click();" style="
    background-color:rgb(125, 125, 125);
    color: white;
"><s:text name="tgi.label.browse" /></a>
</div>
 
<script type="text/javascript">
$('input[id=lefile]').change(function() {
$('#photoCover').val($(this).val());
});
</script>
								</div></div>
								<div class="row">
								<div class="col-md-12">
    <a href="" class="btn btn-sm btn-primary pull-right" ><s:text name="tgi.label.submit" /></a>  </div></div>
      </form></s:if></div>
      </div>
<%-- ${destinationDealsVO.depCity}
<s:if test="destinationDealsVO != null">
<form action="admin_dest_updatedeals.do"  method="POST" enctype="multipart/form-data">
origin : <input type="text" name="depCity" value="${destinationDealsVO.depCity}"><br><br>
airport : <input type="text" name="depAirport" value="${destinationDealsVO.depAirport}"><br><br>
destination<input type="text" name="destCity" value="${destinationDealsVO.destCity}"><br><br>
airport<input type="text" name="destAirport" value="${destinationDealsVO.destAirport}"><br><br>
TripType : 
<input type="hidden" id="type" value="${destinationDealsVO.tripType}">
<select name="tripType" id="tripType">
<option value="Ony way">Ony way</option>
<option value="Round Trip">Round Trip</option>
<option value="Multi Trip">Multi Trip</option>
</select>
<br><br>
<input type="text" name="price" value="${destinationDealsVO.price}">
<br><br>
link : <input type="text" name="href" value="${destinationDealsVO.href}"><br><br>
departure period : <input type="text" name="departurePeriod" value="${destinationDealsVO.departurePeriod}"><br><br>
days : <input type="text" name="days" value="${destinationDealsVO.days}"><br><br>
advance ticketing : <input type="text" name="advanceTicketing" value="${destinationDealsVO.advanceTicketing}"><br><br>
advance purchase : <input type="text" name="advancePurchase" value="${destinationDealsVO.advancePurchase}"><br><br>
<input type="hidden" name="id" value="${destinationDealsVO.id}" ><br><br>
Minimum Stay :<br>
<input type="text" name="minimumStay" value="${destinationDealsVO.minimumStay}" >
<br><br>
Maximum Stay : <br>
<input type="text" name="maximumStay" value="${destinationDealsVO.maximumStay}" >
<br><br>
Purchasing period :<br> 
<input type="text" name="purchasingPeriod" value="${destinationDealsVO.purchasingPeriod}" >
<br><br>
CityGuide Information :<br> <textarea rows="7" cols="50" name="cityGuideInfo">
${destinationDealsVO.cityGuideInfo}
</textarea>
<br><br>
Description : <br><textarea rows="7" cols="50" name="description">
${destinationDealsVO.description}
</textarea>
<br><br>
<input type="file" name="upload">
<input type="submit" value="submit" >
</form>
</s:if> --%>
