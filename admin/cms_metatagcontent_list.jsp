<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
function deletePopupCountryInfo(id,version)
{
	$('#alert_box_delete').modal({
  	    show:true,
  	    keyboard: false
	    } );
	$('#alert_box_delete_body').text("Are your sure want to delete ?");
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	var finalUrl = newUrl+"metatagpage_delete.action?id="+id+"&version="+version;
	  $("#deleteItem").val(finalUrl);
}

$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"metatagpage_list";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	$('#alert_box_delete_ok').click(function() {
		window.location.assign($("#deleteItem").val()); 
			$('#alert_box_delete').hide();
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 <section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                        <div class="pnl">
				<div class="hd clearfix">
				<h5 class="pull-left"><s:text name="tgi.label.meta_dat_list" /></h5>
				<div class="set pull-left">
					<!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
				</div>
				<div class="set pull-right">
					<a id="add_button" class="btn btn-sm btn-success "
						href="metatagpage"><i class="fa fa-plus fa-lg"
						aria-hidden="true" style="margin-right: 6px;"></i><s:text name="tgi.label.create" /><span
						class="none-xs"><s:text name="tgi.label.meta_data" /> </span></a>
				</div>
			</div>
			<br>
			<div class="cnt cnt-table">
			<div class="no-table-css">
			<table id="mytable"
				class="table table-bordered table-striped-column table-hover">
				<thead>
					<tr class="border-radius border-color">
						<th><s:text name="tgi.label.sno" /></th>
						<th><s:text name="tgi.label.page_name" /></th>
						<th><s:text name="tgi.label.title_name" /></th>
						<th><s:text name="tgi.label.keyword" /></th>
						<th><s:text name="tgi.label.description" /></th>
						<th><s:text name="tgi.label.action" /></th>
					</tr>
				</thead>
				<tbody>
					<s:if test="metaTagList != null && metaTagList.size >0">
						<s:iterator value="metaTagList" status="i">
							<tr>
								<td data-title="S.No">${i.count}</td>
								<td data-title="Page Name">${pageName}</td>
								<td data-title="Title Name">${pageTitle}</td>
								<td data-title="Keyword">${metaKeyword}</td>
								<td data-title="Description">${metaDescription}</td>
								<%--  <td><a href="metatagpage_edit.action?id=${id}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-edit"></span> <s:text name="tgi.label.edit" /></a> &nbsp;&nbsp;&nbsp;
                                          <input type="hidden" id="deleteItem">
  					         	       <a href="#" onclick="deletePopupCountryInfo('${id}','${version}')" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                                         </td> --%>
								<td data-title="Action">
									<div class="btn-group">
										<a class="btn btn-xs btn-success dropdown-toggle"
											data-toggle="dropdown" style="padding: 3px 6px;"> <i
											class="fa fa-cog fa-lg" aria-hidden="true"></i>
										</a>
										<ul class="dropdown-menu dropdown-info pull-right align-left">
											<li class=""><a href="metatagpage_edit.action?id=${id}"
												class="btn btn-xs "><span
													class="glyphicon glyphicon-edit"></span> <s:text
														name="tgi.label.edit" /></a></li>
											<li class="divider"></li>
											<li class=""><input type="hidden" id="deleteItem">
												<a href="#"
												onclick="deletePopupCountryInfo('${id}','${version}')"
												class="btn btn-xs "><span
													class="glyphicon glyphicon-trash"></span> Delete</a></li>
										</ul>
									</div>
								</td>
							</tr>
						</s:iterator>
					</s:if>
				</tbody>
			</table></div></div>
		</div>
	</div>
</div>
</section>
<s:if test="message != null && message  != ''">
	<script src="admin/js/jquery.min.js"></script>
	<script src="admin/js/bootstrap.min.js"></script>
	<c:choose>
		<c:when test="${fn:contains(message, 'deleted')}">
			<script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
		</c:when>
		<c:otherwise>
			<script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
		</c:otherwise>
	</c:choose>
</s:if>