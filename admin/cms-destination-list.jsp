<%@ page language="java" isELIgnored="false"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<title>Insert title here</title>
<%-- <script type="text/javascript">
function deletedeals(id,v)
{
$.ajax({
		 
	 	url: "<%=request.getContextPath()%>/admin_dest_deletedeals.do?id="+id+"version="+v,
		type: "GET",
		success: function(result)
			{
		 		location.reload();
 	    	}
		});
}

</script> --%>

<section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                  <h5 class="pull-left"><s:text name="tgi.label.all_deals" /></h5>
                               <!--  <div class="set pull-left">
                                    <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button>
                                </div> -->
                                <div class="set pull-right">
                                    <a id="add_button" class="btn btn-sm btn-success " href="admin_dest_insertpage"><i
							class="fa fa-plus fa-lg" aria-hidden="true"
							style="margin-right: 6px;"></i><s:text name="tgi.label.create" /><span class="none-xs">
								<s:text name="tgi.label.destination" /></span></a>
                               </div> 
                            </div>
				<div class="hd clearfix"></div>

				<div class="cnt cnt-table">

					<div class="table-responsive no-table-css">
						<table
							class="table table-bordered table-striped-column table-hover">
							<thead>
								<tr class="border-radius border-color">
									<th><s:text name="tgi.label.s.no" /></th>
									<th><s:text name="tgi.label.dep_city" /></th>
									<th><s:text name="tgi.label.des_city" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.trip" /></th>
									
									<!-- <th>Action</th> -->
								</tr>
							</thead>
							<tbody>
								<s:iterator value="destinationDealsList" status="i">
									<tr>
										<td data-title="S.No">${i.count}</td>
										<td data-title="Dep City">${depCity}</td>
										<td data-title="Dest City">${destCity}</td>
										<td data-title="Price">${price}</td>
										<td data-title="Trip">${tripType}</td>
										<%-- <td>         
                    <a href="admin_dest_editdeals.action?id=${id}" class="btn btn-xs btn-success">Edit</a>
				<a href='admin_dest_deletedeals?id=${id}'  class="btn btn-xs btn-success" >Delete</a>
					</td> --%>
									</tr>

								</s:iterator>
							</tbody>
						</table>
					</div>

					<div class="">

						<div class="col-md-6">

							<div class="" style="margin-top: 27px;">Showing 1 to 10 of
								70 entries</div>
						</div>
						<div class="col-md-6">
							<ul class="pagination" style="float: right;">
								<li><a href="#">Previous</a></li>
								<li class="disabled"><a href="#">&laquo;</a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">&raquo;</a></li>
								<li><a href="#"><s:text name="tgi.label.next" /></a></li>
							</ul>
						</div>


						<br>
						<br> <br>
						<br>
						<br>
					</div>
				</div>
			</div>
                    </div>
                </div>
                
            </section>