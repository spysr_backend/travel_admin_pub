<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script src="admin/js/jspdf.min.js"></script>
<%-- <script src="admin/js/jspdf.debug.js"></script> --%>
<script>
	$(function() {

		$('#pdf').click(function() {
			var doc = new jsPDF();
			doc.addHTML($('#invoice')[0], 15, 15, {
				'background' : '#fff',
			}, function() {
				doc.save('sample-file.pdf');
			});
		});
	});
</script>

<title>Commission Invoice</title>

<!-- Content Header (Page header) -->
<section class="wrapper container-fluid">
	<div class="row">
		<div class="">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.car_agent_invoice" />
					</h5>
					<div class="set pull-right">
						<button class="btn btn-success btn-xs"
							onclick="sendCustomerInvoiceToCustomer();">
							<s:text name="tgi.label.send_invoice" />
							<i class="fa fa-arrow-circle-right"></i>
						</button>
						<button type="button" class="btn btn-primary but no-print" id="h4"
							style="display: none">
							sending-------<i class="fa fa-arrow-circle-right"></i>
						</button>
					</div>
					<div class="set pull-right"> 
						<a class="btn btn btn-primary btn-xs"
							href="carAgentCommInvoiceList"><span class="pull-right">
								<span class="glyphicon glyphicon-step-backward"></span> <s:text
									name="tgi.label.back" />
						</span></a> 
					</div> 
					<div class="set pull-right">
						<a
							href="<%=request.getContextPath()%>/carAgentInvoice?orderId=${orderId}"
							target="blank" class="popup btn btn-info btn-xs"> <span
							class="pull-right"> Print </span></a>
					</div> 
				</div>
				<!-- Main content -->
				
				
				<div class="col-md-12 mt-2"> 
				<div class="profile-timeline-card">   
								<i class="fa fa-user header-icons timeline-img-user pull-left" style="box-shadow: none;"></i> 
						<div class="">
							<p class="mt-2 mb-4">
								<span> <b class="blue-grey-text">${company.companyName}</b></span>
							</p>

						</div>
						<hr>
						<div class="timeline-footer row row-minus"> 
<div class="col-md-4"> 
<h4 class="text-left"><span>From</span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b>Address</b></td>
            <td> ${company.address}</td>
          </tr>
          <tr>
            <td><b>Tel</b></td>
            <td>${company.phone}</td>
          </tr> 
          <tr>
            <td><b>Email</b></td>
            <td>${company.email}</td>
          </tr> 
          <tr>
            <td><b>Website</b></td>
            <td>${company.website}</td>
          </tr>
          </tbody>
        </table>
     </div> 
<s:if test="%{invoiceData.userDetails.size>0}">
<s:iterator value="invoiceData.userDetails">
<c:if test="${invoiceData.company!=null}">		
     <div class="col-md-4">
     <h4 class="text-left"><span><s:text name="tgi.label.to" /></span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b><s:text name="tgi.label.to" /></b></td>
            <td>${invoiceData.company.companyName}</td>
          </tr>
          <tr>
            <td><b><s:text name="tgi.label.address" /></b></td>
            <td>${invoiceData.company.address}</td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.tel" /></b></td>
            <td>${invoiceData.company.mobile}</td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.email" /></b></td>
            <td>${invoiceData.company.email}</td>
          </tr>
          </tbody>
        </table>
     </div> 
     </c:if>
	</s:iterator>
	</s:if> 
     <div class="col-md-4">
     <h4 class="text-left"><span><s:text name="tgi.label.tax_invoice" /></span></h4>
           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
         <tbody>
          <tr>
            <td> <b><s:text name="tgi.label.invoice_no" /></b></td>
            <td> <s:property  value="invoiceData.invNo"/></td>
          </tr>
          <tr>
            <td><b><s:text name="tgi.label.book_no" /></b></td>
            <td><s:property  value="invoiceData.bookNo"/></td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.your_ref" /></b></td>
            <td><s:property  value="invoiceData.yourRef"/></td>
          </tr> 
          <tr>
            <td><b><s:text name="tgi.label.date" /></b></td>
            <td><s:property  value="invoiceData.date"/></td>
          </tr>
          </tbody>
        </table>
     </div>
						</div>
					</div>
					</div>  
			</div>
		</div>
</div>

		<div id="editor"></div>
</section>

<section class="col-md-12">
					<div class="sccuss-full-updated" id="success-alert"
						style="display: none">
						<div class="succfully-updated clearfix">

							<div class="col-sm-2">
								<i class="fa fa-check fa-3x"></i>
							</div>

							<div id="message" class="col-sm-10"></div>
							<button type="button" id="success" class="btn btn-primary">
								<s:text name="tgi.label.ok" />
							</button>
						</div>

					</div>
					<%--   <div class="row">
 	<div class="col-sm-12" >
  <div id="butns" class="clearfix">
   <input type="hidden" value="<s:property  value="invoiceData.yourRef"/>" id="orderId"> 
         <s:if test="%{#session.User.userRoleId.isSuperUser()}">
          <input type="hidden" value="super" id="companyType"> 
          <button class=" btn btn-primary but no-print" onclick="sendCustomerInvoiceToCustomer();">Send Mail <i class="fa fa-arrow-circle-right"></i> </button>
          </s:if>
          <s:elseif test="%{#session.Company.companyRole.isDistributor()}">
           <button class=" btn btn-primary but no-print" onclick="sendCustomerInvoiceToCustomer();">Send Mail <i class="fa fa-arrow-circle-right"></i> </button>
           <input type="hidden" value="dis" id="companyType"> 
          </s:elseif>
            <s:elseif test="%{#session.Company.companyRole.isAgent()}">
          <!--  <button class=" btn btn-primary but no-print" onclick="sendCustomerInvoiceToCustomer();">Send Mail <i class="fa fa-arrow-circle-right"></i> </button> -->
          </s:elseif>
          <button type="button" class="btn btn-primary but no-print" id="h4"  style="display: none" >sending-------<i class="fa fa-arrow-circle-right"></i></button>
         </div>  
<!-- 
        <button  type="button"    class="btn btn-primary but no-print" onclick="jQuery('invoice').print()" >Print <i class="fa fa-arrow-circle-right"></i> </button>
 
           <button type="button"  id="pdf"  class="btn btn-primary but no-print">PDF <i class="fa fa-arrow-circle-right"></i></button>

              <button type="button" class="btn btn-primary but no-print" >Mail   <i class="fa fa-arrow-circle-right"></i></button>
         <input type="email" name="customerMail"  required="required" id="mail" style="width: 300px" placeholder="mail"   class="btn btn-primary but no-print" >  <button class=" btn btn-primary but no-print" onclick="sendCustomerInvoiceToCustomer();">Send <i class="fa fa-arrow-circle-right"></i> </button>
                       <button type="button" class="btn btn-primary but no-print" id="h4"  style="display: none" >sending-------<i class="fa fa-arrow-circle-right"></i></button>
         
          -->
        <button class=" btn btn-primary but no-print"
						onclick="sendCustomerInvoiceToCustomer();">
						Send <i class="fa fa-arrow-circle-right"></i>
					</button>
					<button type="button" class="btn btn-primary but no-print" id="h4"
						style="display: none">
						sending-------<i class="fa fa-arrow-circle-right"></i>
					</button>

      
      </div>   --%>


					<div id="invoice" class="clearfix mt-0">
						<%-- <div class="row" >
   
 
 <s:if test="%{invoiceData.companyAddress.companyRole.isDistributor() || invoiceData.companyAddress.companyRole.isAgent()}">
<div class="col-xs-4">
<div class="logo">
                <img src="<s:url action='getImageByPath?imageId=%{invoiceData.companyAddress.Imagepath}'/>">
              </div>
</div>
<div class="col-sm-8">
          <div class="lint-invoice-adde text-right">
             <h2>    <s:property  value="invoiceData.companyAddress.Companyname"/> </h2>
         <!--     <p>GST Reg.No: 001363410944 --><br>
        <s:property  value="invoiceData.companyAddress.Address"/> <br>
            <s:text name="tgi.label.tel" /> <s:property  value="invoiceData.companyAddress.Phone"/><!--  Fax: +603-9287 2323 --><br>
            <b><s:text name="tgi.label.email" /></b><s:property  value="invoiceData.companyAddress.Email"/> <b><s:text name="tgi.label.website" /></b><s:property  value="invoiceData.companyAddress.Website"/></p>
          </div>
            </div>
 </s:if>
  <s:elseif test="%{invoiceData.companysGstOn=='lintas'}">
 <div class="col-xs-4">
<div class="logo">
                <img src="images/lintus-logo-admin.png">
              </div>
</div>
<div class="col-sm-8">
          <div class="lint-invoice-adde text-right">
             <h2>  <s:text name="tgi.label.tgi" /> </h2>
             
          </div>
            </div>
   </s:elseif>
  <s:elseif test="%{invoiceData.companysGstOn=='tayyarah'}">
 <div class="col-xs-4">
<div class="logo">
                <img src="images/tayarrah-admin.png">
              </div>
</div>
<div class="col-sm-8">
          <div class="lint-invoice-adde text-right">
          <h2> <s:text name="tgi.label.tgi" /> </h2>
          </div>
            </div>
  </s:elseif>
  
    </div> --%>
						<%-- <div class="row">
							<div class="col-xs-4">
								<div class="text-center pro-img">

									<img
										src="<s:url action='getImageByPath?imageId=%{#session.User.imagePath}'/>"
										class="avatar img-circle img-thumbnail img-responsive"
										alt="profile image">
								</div>
							</div>
							<div class="col-sm-8">
								<div class="lint-invoice-adde text-right">
									<h2>${company.companyName}</h2>
									<p>
										<br>${company.address} <br> Tel: ${company.phone}<br>
										<b>Email:</b> ${company.email}<b> <br>Website:
										</b> ${company.website}
									</p>
								</div>
							</div>


						</div> --%>
						<div class="profile-timeline-card">

							<%-- <div class="in-head">
								<h4 class="text-center">
									<span><s:text name="tgi.label.tax_invoice" /></span>
								</h4>
							</div>

							<div class="invoice-addres clearfix">

								<div class="col-md-5">
									<div class="panel panel-default">
										<s:if test="%{invoiceData.userDetails.size>0}">
											<s:iterator value="invoiceData.userDetails">
												<c:if test="${invoiceData.company!=null}">
													<div class="panel-body">
														<h4>
															<b><s:text name="tgi.label.to" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
															${invoiceData.company.companyName}
														</h4>
														<p>
															<b> <s:text name="tgi.label.address" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															</b> ${invoiceData.company.address}
														</p>
														<p>
															<b><s:text name="tgi.label.tel" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
															${invoiceData.company.mobile}
														</p>
														<!--  <p>Fax: 99876544</p> -->
														 <p><b><s:text name="tgi.label.attn" /></b> <s:property  value="	"/> <s:property  value="Lastname"/></p>
														<p>
															<b><s:text name="tgi.label.email" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
															${invoiceData.company.email}
														</p>

													</div>
												</c:if>
											</s:iterator>
										</s:if>
									</div>
								</div>


								<div class="col-md-6">

									<table class="table inn-num table-bordered no-table-css">
										<tbody>
											<tr>
												<td><s:text name="tgi.label.invoice_no" /></td>
												<td><s:property value="invoiceData.invNo" /></td>
											</tr>
											<tr>
            <td><s:text name="tgi.label.acct_code" /></td>
            <td><a href="#"> <s:property  value="invoiceData.ActCode"/></a></td>
          </tr>
          <tr>
            <td><s:text name="tgi.label.consultant" /> </td>
            <td><a href="#"><s:property  value="invoiceData.consultant"/></a></td>
          </tr>
											<tr>
												<td><s:text name="tgi.label.book_no" /></td>
												<td><s:property value="invoiceData.bookNo" /></td>
											</tr>

											<tr>
												<td><s:text name="tgi.label.your_ref" /></td>
												<td><s:property value="invoiceData.yourRef" /></td>
											</tr>
											<tr>
            <td><s:text name="tgi.label.page" /></td>
            <td><a href="#"><s:text name="tgi.label.page 1 of 1" /></a></td>
          </tr>
											<tr>
												<td><s:text name="tgi.label.date" /></td>
												<td><s:property value="invoiceData.date" /></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div> --%>
							<!-- / end client details section -->

							<div class="">
								<table class="table table-bordered in-table-border no-table-css">
									<thead>
										<tr class="info">
											<th><h4>GST Type</h4></th>
											<th><h4>
													<s:text name="tgi.label.car_booking_particular" />
												</h4></th>
											<th><h4>Qty</h4></th>
											<th><h4>Price</h4></th>
											<th><h4>Total (USD)</h4></th>
										</tr>
									</thead>
									<tbody>
										<%-- <s:iterator value="invoiceData.carOrderGuests"
											status="rowCount"> --%>
											<tr>
												<td>ZR</td>
												<td><%-- (<s:property value="%{#rowCount.count}" />) --%> <s:property value="invoiceData.carOrderRow.customer.firstName" />
												<s:property value="invoiceData.carOrderRow.customer.lastName" />
												</td>
												<td>1</td>
												<td>0</td>
												<td>0</td>
											</tr>
									<%-- 	</s:iterator> --%>
										<tr>
											<td>ZR</td>
											<td>${invoiceData.carOrderInfo.carTitle}/${invoiceData.carOrderInfo.carModel}
												-
												${spysr:formatDate(invoiceData.carOrderInfo.pickUpDateTime,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy')}
												-
												${spysr:formatDate(invoiceData.carOrderInfo.returnDateTime,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy')}
												<br>Pick-Up/ Drop-Off
												${invoiceData.carOrderInfo.pickUpOfficeName} /
												${invoiceData.carOrderInfo.returnOfficeName}
											</td>
											<td>1</td>
											<td><s:property value="invoiceData.basePrice" /></td>
											<td><s:property value="invoiceData.basePrice" /></td>
										</tr>
										<tr>
											<td>SR</td>
											<td>Total Before Markup</td>
											<td>1</td>
											<td><s:property value="invoiceData.basePrice" /></td>
											<td><s:property value="invoiceData.basePrice" /></td>
										</tr>
										<tr>
											<td>SR</td>
											<td>Self Markup</td>
											<td>1</td>
											<td><s:property value="invoiceData.markup" /></td>
											<td><s:property value="invoiceData.markup" /></td>
										</tr>
										<s:if test="logedInCompany == 'superuser'">
											<c:if test="${invoiceData.distributorMarkup!=null}">
												<tr>
													<td>SR</td>
													<td>Wholeseller Markup</td>
													<td>1</td>
													<td><s:property value="invoiceData.distributorMarkup" /></td>
													<td><s:property value="invoiceData.distributorMarkup" /></td>
												</tr>
											</c:if>
										</s:if>
										<s:if test="logedInCompany != 'agent'">
											<c:if test="${invoiceData.childMarkup!=null}">
												<tr>
													<td>SR</td>
													<td>Agent Markup</td>
													<td>1</td>
													<td><s:property value="invoiceData.childMarkup" /></td>
													<td><s:property value="invoiceData.childMarkup" /></td>
												</tr>
											</c:if>
										</s:if>
										<tr>
											<td>SR</td>
											<td>Total After Markup</td>
											<td>1</td>
											<td><s:property value="invoiceData.totAmount" /></td>
											<td><s:property value="invoiceData.totAmount" /></td>
										</tr>
										
										<tr>
											<td>SR</td>
											<td>Tax<span> </span>
											</td>
											<td>1</td>
											<td><s:property value="invoiceData.tax" /></td>
											<td><s:property value="invoiceData.tax" /></td>
										</tr>
										<tr>
											<td><s:text name="tgi.label.sr" /></td>
											<td><s:text name="tgi.label.processing_fees" /> <span>
											</span></td>
											<td>1</td>
											<td><s:property value="invoiceData.processingFees" /></td>
											<td><s:property value="invoiceData.processingFees" /></td>

										</tr>
										<tr>
											<td><s:text name="tgi.label.sr" /></td>
											<td><s:text name="tgi.label.insurance_fee" /> <span>
											</span></td>
											<td>1</td>
											 <td>
									              	<c:choose>
													<c:when test="${invoiceData.carOrderRow.carOrderInsurance.price != null}">
													${invoiceData.carOrderRow.carOrderInsurance.price}
													</c:when>
													<c:otherwise>
													<s:text name="tgi.label.n_a" />
													</c:otherwise>
													</c:choose>
									              </td>
											 <td>
									              	<c:choose>
													<c:when test="${invoiceData.carOrderRow.carOrderInsurance.price != null}">
													${invoiceData.carOrderRow.carOrderInsurance.price}
													</c:when>
													<c:otherwise>
													<s:text name="tgi.label.n_a" />
													</c:otherwise>
													</c:choose>
									              </td>

										</tr>
										<tr>
											<th></th>
											<th></th>
											<th><s:text name="tgi.label.calculation" /></th>
											<th></th>
											<th></th>
										</tr>

										<tr>
											<td>
												<!-- 6% -->
											</td>
											<td>
												<!-- SR -->
											</td>
											<td>
												<%-- <s:property  value="invoiceData.totGst"/> --%>
											</td>
											<td><s:text name="tgi.label.booking_amount" /></td>
											<td colspan="4">${invoiceData.price}<br> <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         	 <s:text name="tgi.label.totamount" /> <s:property  value="invoiceData.totWithGst"/> --%>
											</td>

										</tr>

										<tr>
											<td>
												<!-- 6% -->
											</td>
											<td>
												<!-- SR -->
											</td>
											<td>
												<%-- <s:property  value="invoiceData.totGst"/> --%>
											</td>
											<td><s:text name="tgi.label.discount_amount" /></td>
											<td>${invoiceData.discountAmount}<br> <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         	 <s:text name="tgi.label.totamount" />  <s:property  value="invoiceData.totWithGst"/> --%>
											</td>

										</tr>

										<tr>
											<td>
												<!-- 6% -->
											</td>
											<td>
												<!-- SR -->
											</td>
											<td>
												<%-- <s:property  value="invoiceData.totGst"/> --%>
											</td>
											<td><s:text name="tgi.label.total_paid" /></td>
											<td colspan="4">${invoiceData.totPrice}<br> <%-- TotGST :   <s:property  value="invoiceData.totGst"/><br>
         <s:text name="tgi.label.totamount"/>   <s:property  value="invoiceData.totWithGst"/> --%>
											</td>

										</tr>

										<tr>
											<th></th>
											<th>Commision Details<%-- <s:text name="tgi.label.commision_details" /> --%></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
										
										<c:if test="${invoiceData.myCommission!=null}">
											<tr>
												<td>SR</td>
												<td>My Commision</td>
												<td>1</td>
												<td><s:property value="invoiceData.myCommission" /></td>
												<td><s:property value="invoiceData.myCommission" /></td>
											</tr>
										</c:if>
										<s:if test="logedInCompany != 'agent'">
											<c:if test="${invoiceData.sharedCommission!=null}">
												<tr>
													<td>SR</td>
													<td>Shared Commision</td>
													<td>1</td>
													<td><s:property value="invoiceData.sharedCommission" /></td>
													<td><s:property value="invoiceData.sharedCommission" /></td>
												</tr>
											</c:if>
										</s:if>
										<tr>
											<td>SR</td>
											<td>My Revenue</td>
											<td>1</td>
											<td><s:property value="invoiceData.myProfit" /></td>
											<td><s:property value="invoiceData.myProfit" /></td>
										</tr>
										
										<tr>
														<td>SR</td>
														<td>Total Price Afetr My Revenue</td>
														<td>1</td>
														<td><s:property value="invoiceData.totalPriceAfterMyRevenue" /></td>
														<td><s:property value="invoiceData.totalPriceAfterMyRevenue" /></td>
													</tr> 
										
									</tbody>
								</table>

							</div>
							
							<div class="clearfix">
								<div class="payment-in">
									
									<div class="panel panel-info">
									<c:choose>
									<c:when test="${invoiceData.carOrderRow.carOrderInsurance != null}">
											<h4>
												<s:text name="tgi.label.flight_insurance" />
											</h4>
											<!-- <div class="panel-body"> -->
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="info">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
										<tr>
											<td data-title="><s:text name="tgi.label.insurance_id" />">${invoiceData.carOrderRow.carOrderInsurance.insuranceId}</td>
											<td data-title="<s:text name="tgi.label.name" />">${invoiceData.carOrderRow.carOrderInsurance.name}</td>
											<td data-title="<s:text name="tgi.label.price" />">${invoiceData.carOrderRow.carOrderInsurance.price}</td>
											<td data-title="<s:text name="tgi.label.description" />">${invoiceData.carOrderRow.carOrderInsurance.description}</td>
											
										</tr>
									</table>
									</c:when>
									<c:otherwise>
											<h4 class="pl-2">
												<s:text name="tgi.label.flight_insurance" />
											</h4>
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="info">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.insurance_not_available" /></td>
												</tr>
											</table>
											</c:otherwise>
										</c:choose>
										</div>
										</div></div>
							
							<div class="clearfix">
											<div class="payment-in">
												<div class="panel panel-info">
													<!--   <div class="panel-heading">
    <h4>Payment details</h4>
  </div> -->

													<h4 class="pl-2">
														<s:text name="tgi.label.payment_details" />
													</h4>
													<!-- <div class="panel-body"> -->
													<table
														class="table table-bordered in-table-border no-table-css">
														<tr class="info">
															<th><s:text name="tgi.label.receipt" /></th>
															<th><s:text name="tgi.label.date" /></th>
															<th><s:text name="tgi.label.remark" /></th>
															<th><s:text name="tgi.label.payment_type" /></th>
															<th><s:text name="tgi.label.payable_amount" /></th>
															<th><s:text name="tgi.label.paid_amount" /></th>
															<!-- 
<th>
AgentComm
</th> -->
														</tr>
														<s:if test="invoiceData.txDetails.size()>0">
															<s:iterator value="invoiceData.txDetails">
																<tr>
																	<td data-title="<s:text name="tgi.label.receipt" />"><s:property value="apiTransactionId"/></td>
																	<td data-title="<s:text name="tgi.label.date" />"><s:property
																			value="convertDate" /></td>
																	<td data-title="<s:text name="tgi.label.remark" />"><s:property
																			value="responseMessage" /></td>
																	<td
																		data-title="<s:text name="tgi.label.payment_type" />"><s:property
																			value="paymentMethod" /></td>
																	<td
																		data-title="<s:text name="tgi.label.processing.fees" />"><s:property value="totalPaidAmount"/></td>
																	<td
																		data-title="<s:text name="tgi.label.processing.fees" />"><s:property value="totalPaidAmount"/></td>
																	<!-- <td><s:property  value="%{#session.agentCommInvoiceObj.totMarkup}"/></td>-->
																</tr>
															</s:iterator>

														</s:if>
														<s:else>
															<tr>
																<td colspan="6"><s:text
																		name="tgi.label.payment_details_not_ available" /></td>
															</tr>
														</s:else>

													</table>
												</div>
												<div class="panel panel-info">
													<h4>
														<s:text name="tgi.label.wallet_details" />
													</h4>
													<table
														class="table table-bordered in-table-border no-table-css">
														<tr class="info">
															<th><s:text name="tgi.label.created_at" /></th>
															<th><s:text name="tgi.label.action" /></th>
															<th><s:text name="tgi.label.amount" /></th>
															<th><s:text name="tgi.label.openingbal" /></th>
															<th><s:text name="tgi.label.closingbal" /></th>
															<th><s:text name="tgi.label.currency" /></th>

														</tr>
														<s:if test="invObj.agentWalletTxDetails.size()>0">
															<s:iterator value="invObj.agentWalletTxDetails">
																<tr>
																	<td data-title="<s:text name="tgi.label.created_at" />"><s:property
																			value="invObj.convertDate" /></td>
																	<td data-title="<s:text name="tgi.label.action" />"><s:property
																			value="invObj.action" /></td>
																	<td data-title="<s:text name="tgi.label.amount" />"><s:property
																			value="invObj.amount" /></td>
																	<td data-title="<s:text name="tgi.label.openingbal" />"><s:property
																			value="invObj.openingBalance" /></td>
																	<td data-title="<s:text name="tgi.label.closingbal" />"><s:property
																			value="invObj.closingBalance" /></td>
																	<td data-title="<s:text name="tgi.label.currency" />"><s:property
																			value="invObj.currency" /></td>
																</tr>
															</s:iterator>
														</s:if>
														<s:else>
															<tr>
																<td colspan="5"><s:text
																		name="tgi.label.wallet_details_not_available" /></td>
															</tr>


														</s:else>

													</table>

												</div>
											</div>
										</div>
<div class="clearfix signature-lint"> 
						<div class="col-sm-6">
							<h4>
								<s:text name="tgi.label.recived_by" />
							</h4>
							<br> <br>
							<p>
								<span style="border-top: 1px solid #adadad; padding: 5px;"><s:text
										name="tgi.label.customers_signature" /> &amp; Chop</span>
							</p>
						</div>

						<div class="col-sm-6 pull-right">
							<div class="pull-right">
								<h4>TGI</h4>
								<br> <br>
								<p>
									<span style="border-top: 1px solid #adadad; padding: 5px;"><s:text
											name="tgi.label.autorised_signature" /></span>
								</p>
							</div>
						</div>
					</div>
						</div> 
					</div>

</section>
<!-- /.row -->
<!-- Main row -->

<!-- table-responsive -->

<!-- Main row -->



<!-- /.content -->
<script type="text/javascript">
	function sendCustomerInvoiceToCustomer() {
		var companyType = $("#companyType").val();
		console.log("companyType..." + companyType);
		var orderId = $("#orderId").val();
		console.log("orderId..." + orderId);
		//var invoice = "<html><body style='border:2px solid;padding:10px 10px 10px 10px'>"+$("#invoice").html()+"</body></html>";
		/*   var htmlMessage=$('#invoice').html(); */
		//console.log("--htmlMessage..."+invoice);
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "sendHotelInvoiceToMail";
		//console.log("finalUrl..."+finalUrl);
		$('#h4').show();
		$.ajax({
			method : "POST",
			url : finalUrl,
			data : {
				orderId : $("#orderId").val(),
				companyType : $("#companyType").val()
			},
			success : function(data, status) {
				$.each(data, function(index, element) {
					console.log("data-------" + element.status);

					if (element.status == "success") {
						$('#h4').hide();
						$('#success-alert').show();
						$('#message').text("Successfully sent mail.");
						$('#success').click(function() {
							$('#success-alert').hide();
							window.location.assign($(location).attr('href'));
						});

					} else if (element.status == "fail") {
						$('#h4').hide();
						$('#success-alert').show();
						$('#message').text("Failed.Try again.");
						$('#success').click(function() {
							$('#success-alert').hide();

						});
					}

				});

			},
			error : function(xhr, status, error) {
				$('#h4').hide();
				$('#success-alert').show();
				$('#message').text(error);
				$('#success').click(function() {
					$('#success-alert').hide();
				});
				console.log("Error----------" + error);
			}
		});
	}
</script>
