<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
    <!-- Placecomplete plugin -->
    
<link rel="stylesheet" href="admin/css/switchery.min.css">
<link rel="stylesheet" href="admin/css/fakeLoader.css">
<!-- Custom stylesheet in less-->
<link rel="stylesheet" href="admin/css/flexslider.css">
<link rel="stylesheet" href="admin/css/animate.css">
<link rel="stylesheet" href="admin/css/owl.carousel.css">
 -->
<link rel="stylesheet" href="admin/css/bootstrap-datepicker3.css">
<link rel="stylesheet" href="admin/css/clockpicker.css">
<link rel="stylesheet" href="admin/css/bootstrap-select.min.css">
<link rel="stylesheet" href="admin/css/jquery.bxslider.css">
    
<!-- <link rel="stylesheet" href="admin/css/style-search.css">    --> 
    <link rel="stylesheet" href="admin/css/jquery.auto-complete.css">
<link href="admin/css/chart.css" rel="stylesheet" type="text/css" />
<script src="admin/js/jquery.min.js"></script>
<script src="admin/js/owl.carousel.min.js"></script>
<script src="admin/js/modernizr.js"></script>
<script src="admin/js/jquery.auto-complete.js"></script>
    <!-- Placecomplete plugin -->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script src="admin/js/jquery.geocomplete.js"></script>
<script src="admin/js/logger.js"></script>
   <script>
   
 function autoRefresh_div()
 {	 
    //a function which will load data from other file after x seconds
      var protocol=location.protocol;
  	   var host=location.host;
  	  /* var url=protocol+"//"+host+"/TGIAdmin/getHotelNamesJson"; */
  	  //var url = protocol+"//"+host+"/getDashboardDataJson";
  		var url ="getDashboardDataJson";
   				comfirmList= [];
 				$.ajax({
					    method: "GET",
					    url:url,
					   /*  data: {type:"today"}, */
					    success:function(data,status)
						{ 
					    	//console.log("id.."+data.jsonobj.FlightOrdercount);
					     $("#flightOrders").text(data.jsonobj.FlightOrdercount);
					     $("#flightpaymentcount").text(data.jsonobj.Flightpaymentordercount);
					     $("#flightconfirmCount").text(data.jsonobj.Flightconfirmordercount);
					    	 
					     $("#hotelOrders").text(data.jsonobj.HotelOrdercount);
					     $("#hotelconfirmCount").text(data.jsonobj.Hotelconfirmordercount);
					     $("#hotelpaymentcount").text(data.jsonobj.Hotelpaymentordercount);
					     
					     $("#carOrders").text(data.jsonobj.CarOrdercount);
					     $("#carconfirmCount").text(data.jsonobj.Carconfirmordercount);
					     $("#carpaymentcount").text(data.jsonobj.Carpaymentordercount);
					     
					     $("#tourOrders").text(data.jsonobj.TourOrdercount);
					     $("#tourconfirmCount").text(data.jsonobj.Tourconfirmordercount);
					     $("#tourpaymentcount").text(data.jsonobj.Tourpaymentordercount);
					     
					     $("#limoOrders").text(data.jsonobj.LimoOrdercount);
					     $("#limoconfirmCount").text(data.jsonobj.Limoconfirmordercount);
					     $("#limopaymentcount").text(data.jsonobj.Limopaymentordercount);
					     
					     $("#distributorlist").text(data.jsonobj.totaldistributorlist);		
					     $("#agentlist").text(data.jsonobj.totalagentlist);	
					     },
						error: function(xhr,status,error)
						{
							console.log(error);
						}
					});  
  }
  $(function() {
	 //autoRefresh_div();
	//getgraphdata();
	/*  setInterval('autoRefresh_div()',60000); // refresh div after 60 secs */
 }); 
  
     </script>
      
      <style>
.nav-horizontal {
    padding: 5px 5px 10px;
    margin: 0;
    list-style: none;
    background: white;
    margin-bottom: 2rem;
    border-radius: 10px;
    box-shadow: 0px 15px 25px -5px rgba(0, 0, 0, 0.10);
}
.nav-horizontal li {
    display: inline-block;
    margin: 0 3px 9px;
}
.nav-horizontal li {
    display: inline-block;
    margin: 0 3px 9px;
}
.nav-horizontal a:hover, .nav-horizontal li.active a {
    background-color: #353a40;
    text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.5);
    text-decoration: none;
    color: #fff;
    border-radius: 10px;
    transition: 0.40s;
    box-shadow: 0px 8px 20px -5px rgba(0, 0, 0, 0.5);
}

.headtop .nav>li>a:hover, .nav>li>a:focus {
    background: #ffde01 !important;
    color: #353a40 !important;
}
.nav-horizontal a {
    display: block;
    min-width: 130px;
    min-height: 110px;
    border-radius: 10px;
    font-weight: 100;
    text-align: center;
    color: #353a40;
    padding: 8px 10px;
    background-color: #ffde01;
    transition: 0.40s;
}
@media screen and (max-width: 730px){
.nav-horizontal a {
    text-align: center;
    min-width: 130px;
    min-height: 10px;
    border-radius: 0px !important; 
    padding: 12px 15px;
}
}
@media screen and (max-width: 992px){
.nav-horizontal a {
    text-align: center;
    min-width: 110px;
    min-height: 10px;
    border-radius: 0px !important;
    padding: 12px 15px;
}
}
.nav-horizontal a:hover i, .nav-horizontal li.active a i {
    color: #fff;
    opacity: 1;
}
@media screen and (min-width: 992px){
.nav-horizontal i {
    display: block;
    height: 55px;
    margin-right: 0;
    margin-bottom: 10px;
    color: #353a40;
    font-size: 35px;
    padding: 10px;
}
}
.card.card-inverse{
    margin-bottom: 15px;
} 

h3.quick-text {
    padding: 15px 0px 15px 0px;
    color: #363a40;
    font-weight: 100;
}
.clippy {
    margin-top: -1px;
    position: relative;
    top: 1px;
}

img {
    border: 0;
}
.input-group {
    display: table;
}
.input-group-button {
    width: 1%;
    vertical-align: middle;
}

.input-group input, .input-group-button {
    display: table-cell;
}
.input-group-button:last-child .btn {
    margin-left: -1px;
}
</style>
	 <section class="content-header" style="text-align:center;">
      <div class="container" style="margin-top: 35px;">
	 	<div class="form-group row">
	 	<div class="col-md-2">
	 	<h4 style="margin-top: 8px;"><s:text name="tgi.label.dashboard" /> <s:text name="tgi.label.control_panel" /></h4>
	 	</div>
       <label class="col-md-1 form-control-label" style="margin-top: 6px;font-size: 15px;font-weight: 500;">Refer link:</label>
	       <div class="col-md-5">
	         <div class="input-group">
			<input id="refer_code_link" type="text" value="https://sacredsensualgetaways.com/tour_search.do?referCode=<s:property value="%{#session.User.referralCode}"/>" class="form-control text-danger">
			<span class="input-group-button">
			<button class="btn btn-default " type="button" data-clipboard-demo="" data-clipboard-target="#refer_code_link" title="Click to copy text">
			<img class="clippy" src="admin/img/svg/download.svg" width="13" alt="Copy to clipboard">
			</button>
			</span>
			 </div>
	       </div>
       <label class="col-md-1 form-control-label" style="margin-top: 6px;font-size: 15px;font-weight: 500;">Refer Code:</label>
       <div class="col-md-3">
         <div class="input-group">
		<input id="refer_code" type="text" value="<s:property value="%{#session.User.referralCode}"/>" class="form-control text-danger">
		<span class="input-group-button">
		<button class="btn btn-default" type="button" data-clipboard-demo="" data-clipboard-target="#refer_code" title="Click to copy text">
		<img class="clippy" src="admin/img/svg/download.svg" width="13" alt="Copy to clipboard">
		</button>
		</span>
	 </div>
       </div>
       <hr>
	 </div>
<div class="content-header company">
<ul class="nav-horizontal text-center">
<h3 class="quick-text">Company Quick Links </h3>
<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUsermode()) && #session.User.userRoleId.isSuperUser())}">
<li class="active">
<a href="<s:url action="walletTransactions"/>"><i class="fa fa-money"></i> Transactions List</a>
</li>
<li>
<a href="<s:url action="goMyWallet"/>"><i class="fa fa-google-wallet" ></i> Add My Wallet</a>
</li>
</s:if>
<s:if test="%{ #session.User.userRoleId.isAdmin() || #session.Company.companyRole.isDistributor()}">
<li>
<a href="<s:url action="showWalletUsers"/>"><i class="fa fa-google-wallet" ></i> Add User Wallet</a>
</li>
</s:if>
<s:if test="%{((#session.User.userRoleId.isSuperUser()||#session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">  
<li>
<a href="<s:url action="addCompany"/>"><i class="fa fa-building-o"></i> Add Company</a>
</li>
<li>
<a href="<s:url action="companyList"/>"><i class="fa fa-list-alt" ></i> Company List</a>
</li>
<li>
<a href="<s:url action="addNewCompanyConfig"/>"><i class="fa fa-plus-square-o"></i> Add Config</a>
</li>

<li>
<a href="<s:url action="userRegister"/>"><i class="fa fa-user-plus"></i> Add Employee</a>
</li>
<li>
<a href="<s:url action="userList"/>"><i class="fa fa-list-alt"></i>Employee List</a>
</li>
</s:if>
</ul>
</div>

<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isFlight())}">
<div class="content-header flight">
<ul class="nav-horizontal text-center">
<h3 class="quick-text">Flight Quick Links </h3>
<li class="active">
<a href="<s:url action="flightMarkupList"/>"><i class="fa fa-list-alt" ></i>Markup List</a>
</li>
<li>
<a href="<s:url action="flightReportList"/>"><i class="fa fa-list-alt" ></i>Report List</a>
</li>
<li>
<a href="<s:url action="flightOrderList"/>"><i class="fa fa-list-alt" ></i>Order List</a>
</li>
<li>
<a href="<s:url action="flightCommissionReportList"/>"><i class="fa fa-files-o" ></i>Commision Report</a>
</li>
<li>
<a href="<s:url action="flightCustomerInvoiceList"/>"><i class="fa fa-files-o" ></i>Customer Invoice </a>
</li>
<li>
<a href="<s:url action="flightAgentInvoiceList"/>"><i class="fa fa-files-o" aria-hidden="true"></i>Agent Invoice </a>
</li>
</ul>
</div>
</s:if>
<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isHotel())}">
	<div class="content-header hotel">
	<ul class="nav-horizontal text-center">
	<h3 class="quick-text">Hotel Quick Links </h3>
	<li class="active">
	<a href="<s:url action="hotelMarkupList"/>"><i class="fa fa-list-alt"></i>Markup List</a>
	</li>
	<li>
	<a href="<s:url action="hotelReportList"/>"><i class="fa fa-list-alt"></i>Report List</a>
	</li>
	<li>
	<a href="<s:url action="hotelOrdersList"/>"><i class="fa fa-list-alt"></i>Order List</a>
	</li>
	<li>
	<a href="<s:url action="hotelCommissionReport"/>"><i class="fa fa-file-text-o"></i>Commision Report</a>
	</li>
	<li>
	<a href="<s:url action="hotelCustomerInvoiceList"/>"><i class="fa fa-file-text-o"></i>Customer Invoice </a>
	</li>
	<li>
	<a href="<s:url action="hotelAgentCommInvoiceList"/>"><i class="fa fa-file-text-o"></i>Agent Invoice </a>
	</li>
	</ul>
	</div>
	</s:if>
	<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isCar())}">
	<div class="content-header car">
	<ul class="nav-horizontal text-center">
	<h3 class="quick-text">Car Quick Links </h3>
	<li class="active">
	<a href="<s:url action="carMarkupList"/>"><i class="fa fa-list-alt"></i>Markup List</a>
	</li>
	<li>
	<a href="<s:url action="carReportList"/>"><i class="fa fa-list-alt"></i>Report List</a>
	</li>
	<li>
	<a href="<s:url action="carOrderList"/>"><i class="fa fa-list-alt"></i>Order List</a>
	</li>
	<li>
	<a href="<s:url action="carCommissionReport"/>"><i class="fa fa-files-o"></i>Commision Report</a>
	</li>
	<li>
	<a href="<s:url action="carCustomerInvoiceList"/>"><i class="fa fa-files-o"></i>Customer Invoice </a>
	</li>
	<li>
	<a href="<s:url action="carAgentCommInvoiceList"/>"><i class="fa fa-files-o"></i>Agent Invoice </a>
	</li>
	</ul>
	</div>
	</s:if>
	<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isTour())}">
	<div class="content-header tour">
	<ul class="nav-horizontal text-center">
	<h3 class="quick-text">Tour Quick Links </h3>
	<li class="active">
	<a href="<s:url action="tourMarkupList"/>"><i class="fa fa-list-alt"></i>Markup List</a>
	</li>
	<li>
	<a href="<s:url action="tourReportList"/>"><i class="fa fa-list-alt"></i>Report List</a>
	</li>
	<li>
	<a href="<s:url action="tourOrderList"/>"><i class="fa fa-list-alt"></i>Order List</a>
	</li>
	<li>
	<a href="<s:url action="tourCommissionReport"/>"><i class="fa fa-file-text-o"></i>Commision Report</a>
	</li>
	<li>
	<a href="<s:url action="tourCustomerInvoiceList"/>"><i class="fa fa-file-text-o"></i>Customer Invoice </a>
	</li>
	<li>
	<a href="<s:url action="tourAgentCommInvoiceList"/>"><i class="fa fa-file-text-o"></i>Agent Invoice </a>
	</li>
	</ul>
	</div>
     </s:if>
	<s:if test="%{(#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isTechSupport() || #session.User.userRoleId.isTechHead() || #session.User.userRoleId.isTravelDesk())">
	<div class="content-header tour">
	<ul class="nav-horizontal text-center">
	<h3 class="quick-text">Task Management Quick Links </h3>
	<li class="active">
	<a href="<s:url action="tourMarkupList"/>"><i class="fa fa-list-alt"></i>New Task/Bug</a>
	</li>
	<li>
	<a href="<s:url action="tourReportList"/>"><i class="fa fa-list-alt"></i>Task/Bug List</a>
	</li>
	<li>
	<a href="<s:url action="tourOrderList"/>"><i class="fa fa-list-alt"></i>Assign Task/Bug</a>
	</li>
	</ul>
	</div>
     </s:if>
      
<c:if test="${appConfig.enableSearchBox==true}">      
<section class="content">
			<div class="travel-tabs mb">
							<div role="tabpanel">
						
					<span style="color:red;"> ${errorMessage} </span>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						
						<li role="presentation" class="active"><a href="#flight-tab" aria-controls="flights" role="tab" data-toggle="tab"><i class="fa fa-plane"></i> <span class="hidden-xs"> <s:text name="tgi.label.flights" /></span></a>
						</li>
						<li role="presentation" class=""><a href="#hotel-tab" aria-controls="hotel" role="tab" data-toggle="tab"><i class="fa fa-hotel"></i> <span class="hidden-xs"> <s:text name="tgi.label.hotels" /></span></a>
						</li>
						<li role="presentation"><a href="#car-tab" aria-controls="car-tab" role="tab" data-toggle="tab"><i class="fa fa-car"></i> <span class="hidden-xs"> <s:text name="tgi.label.car" /></span></a>
						</li>
						<li role="presentation" class=""><a href="#limo"
					aria-controls="home" role="tab" data-toggle="tab"><i
						class="fa fa-hotel"></i> <span class="hidden-xs"> <s:text name="tgi.label.limo" />
					</span></a></li>
						<li role="presentation" class=""><a href="#cruise-tab" aria-controls="Tour" role="tab" data-toggle="tab"><i class="fa fa-bus"></i> <span class="hidden-xs"> <s:text name="tgi.label.cruise" /></span></a>
						</li>
						<li role="presentation" class=""><a href="#tour-tab" aria-controls="Tour" role="tab" data-toggle="tab"><i class="fa fa-bus"></i> <span class="hidden-xs"> <s:text name="tgi.label.tours" /></span></a>
						</li>
					
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					<div role="tabpanel" class="tab-pane" id="cruise-tab">
		<div class="wrapper">
		    <div class="h_iframe">
		        <!-- a transparent image is preferable -->
		        <img class="ratio" src="https://placehold.it/16x9"/>
		        <iframe src="https://cs.cruisebase.com/cs/default.aspx?skin=681&amp;target=_self" width="100%" height="400px" frameborder="0" scrolling="yes">
		        </iframe>
		    </div>
		</div>
			</div>
					<div role="tabpanel" class="tab-pane" id="limo">
					<div class="car-type-nav">
						<a class="active same-drop-btn" id="" href="#"><s:text
								name="tgi.label.same_dropoff" /> </a> <a class="diff-drop-btn"
							id="diff-drop" href="#"><s:text
								name="tgi.label.differnet_dropoff" /> </a>
					</div>
					 <script>
                                $('document').ready(function () {
                                	 $(".limoList").geocomplete()
                                     .bind("geocode:result", function(event, result){
                                       $.log("Result: " + result.formatted_address);
                                     })
                                     .bind("geocode:error", function(event, status){
                                       $.log("ERROR: " + status);
                                     })
                                     .bind("geocode:multiple", function(event, results){
                                       $.log("Multiple: " + results.length + " results found");
                                     });
                                    
                                });
                            </script>
					
					<form method="get" action="${appConfig.ibeLink}limo_result.do" id="car1"
						class="main-form cars-form flight-form-1">
						<div class="clearfix">
							<div class="if fl2 car-pick">
								<label for=""><s:text name="tgi.label.pickup" /> </label> <input
									name="pickUpCity" type="text" id="pickUpCity"
									class="limoList" minlength="3" type="text" required=""
									aria-required="true" aria-invalid="true" placeholder="From"
									autocomplete="off">
							</div>
							<div class="if fl2 car-drop "><!-- car-drop"> -->
								<label for=""><s:text name="tgi.label.drop_off" /> </label> <input
									name="returnCity" type="text" id="returnCity"
									class="limoList" placeholder="From" minlength="3"
									aria-required="true" aria-invalid="true"
									autocomplete="off">
							</div>
							<div class="if fl2 car-dt">
								<label for=""><s:text name="tgi.label.pickup_date" /> </label>
								<input name="pickUpDate" type="text" id="pickUpDate"
									class="datepicker date1 transparent" type="text" required=""
									aria-required="true" aria-invalid="true"
									placeholder="Pick-up Date" autocomplete="off"  style="color:transparent;">
							</div>
							<div class="if fl1 car-tt">
								<label for="">&nbsp;</label> <input type="text"
									name="pickUpTime" id="pickUpTime" class="clockpicker "
									placeholder="Time" required="" aria-required="true"
									aria-invalid="true" autocomplete="off" value="09:00">
							</div>
							<%-- <div class="if fl2 car-dt">
								<label for=""><s:text name="tgi.label.drop_off" /> <i
									class="icon-chevron-up"></i> <s:text name="tgi.label.date" />
								</label> <input name="returnDate" type="text" id="returnDate"
									class="datepicker date2 transparent" type="text" required=""
									aria-required="true" aria-invalid="true"
									placeholder="Drop-of Date" autocomplete="off"  style="color:transparent;">
							</div> 
							<div class="if fl1 car-tt">
								<label for="">&nbsp;</label> <input name="returnTime"
									id="returnTime" class="clockpicker" type="text"
									placeholder="Time" required="" aria-required="true"
									aria-invalid="true" autocomplete="off" value="09:00">
							</div>
							--%>
						<!-- </div>
						<div class="clearfix"> -->
							<div class="if fl2 car-type">
								<label for="">&nbsp;</label>
								<s:select list="limoCarList" listKey="code" listValue="description"
									id="SIPPCode" name="SIPPCode" headerKey=""
									headerValue="Limo Car Type"
									cssClass="show-tick form-control"
									data-live-search="true" />

							</div>
							<div class="if fl1 car-form-sumbit">
							<label for="">&nbsp;</label>
								<button class="form-sumbit">
									<s:text name="tgi.label.submit" />
									<i class="fa fa-arrow-right"></i>
								</button>
								<input type="hidden" id="carSearchType" name="carSearchType"
									value="S" />
								<s:hidden name="request_locale" value="%{request_locale}" />
								<s:hidden name="requestadmindata" value="%{#session.adminLoginKey}" />
									<s:hidden name="sessionKey" />
							</div>
						</div>
					</form>
				</div>
					
					<div role="tabpanel" class="tab-pane" id="tour-tab">
							<script>
								$('document').ready(function() {
									var nowDate = new Date();
									var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

									$('.flight-hotel-form').on('focus', ".datepicker", function() {
										$(this).datepicker({
											startDate: today,
											todayBtn: "linked",
											todayHighlight: true
										});
									});

								});

							</script>
							  <form action="${appConfig.ibeLink}tour_search_by_category_result.do" class="form-horizontal">
                        <div class="row">
                             <div class="col-md-2">
                                 <lable><s:text name="tgi.label.tour_type" /></lable>
                                <%--  <select class="show-tick form-control col-sm-2 required" name="tourCategoryId" id="tourCategoryId" data-live-search="true">
                                 <c:forEach var="packageMainCategoryItem"
							items="${packageCategory.packageMainCategoryMap}"
							varStatus="loooooop">
							<c:set var="packageMainCategory"
								value="${packageMainCategoryItem.value}" />
								<option value="${packageMainCategory.id}">${packageMainCategory.description}</option>
    						</c:forEach>
                                </select> --%>
                                <select class="show-tick form-control col-sm-2 required" name="tourCategoryId" id="tourCategoryId" data-live-search="true">
                                 
							
								<option value="337"><s:text name="tgi.label.adventure_tours" /></option>
    						
							
								<option value="326"><s:text name="tgi.label.culture_turs" /></option>
    						
							
								<option value="321"><s:text name="tgi.label.escorted_tours" /></option>
    						
							
								<option value="351"><s:text name="tgi.label.europe_delights" /></option>
    						
							
								<option value="324"><s:text name="tgi.label.gallipolo_tours" /></option>
    						
							
								<option value="323"><s:text name="tgi.label.gullet_cruises" /></option>
    						
							
								<option value="359"><s:text name="tgi.label.israel_tour" /></option>
    						
							
								<option value="320"><s:text name="tgi.label.packages" /></option>
    						
							
								<option value="363"><s:text name="tgi.label.religious_tours_europe" /></option>
    						
							
								<option value="322"><s:text name="tgi.label.small_size_tours" /></option>
    						
							
								<option value="335"><s:text name="tgi.label.test_istanbul" /></option>
    						
							
								<option value="348"><s:text name="tgi.label.trade_shows" /></option>
    						
                                </select>
                             </div>
                             <div class="col-md-2">
                                 <lable><s:text name="tgi.label.month" /> </lable>
                                	<s:select list="monthsList" listKey="cmbId"
														listValue="cmbName" name="tourMonth"
														id="tourMonth"
														cssClass="show-tick form-control col-sm-2 required"
														data-live-search="true" />
                             </div>
                             <div class="col-md-2">
                                 <lable><s:text name="tgi.label.year" /> </lable>
                                 <s:select list="yearList" listKey="cmbId"
														listValue="cmbName" name="tourYear"
														value="2" id="tourYear"
														cssClass="show-tick form-control col-sm-2 required"
														data-live-search="true" />
                             </div>
                             <div class="col-md-4">
                                <lable><s:text name="tgi.label.select_rooms" /> </lable>                            
                                <div class="clearfix room-rel">
                                    <div class="if tour-room-select hotel-rooms hotel-rooms1">
                                        <div class="choice-field"><span class="perup">1</span> <s:text name="tgi.label.persons" /> , <span class="roomup">1</span> <s:text name="tgi.label.room" /> <i class="pull-right fa fa-caret-down"></i></div>
                                   <input id="guestCount" name="guestCount" type="hidden" value="1">
								<input id="numberOfRooms" name="numberOfRooms" type="hidden"
									value="1">
                                    </div>
                            
                             </div>
                             </div>
                             <div class="col-md-2">
                                 <lable>&nbsp;</lable>
                                 <input id="promotional" name="promotional" type="hidden"
									value="false">
									 <input id="promotionalId" name="promotionalId" type="hidden"
									value="0">
									<s:hidden name="request_locale" value="%{request_locale}" />
									<s:hidden name="requestadmindata" value="%{#session.adminLoginKey}" />
									<s:hidden name="sessionKey" />
                                 <button type="submit" class="btn btn-brand btn-block"><s:text name="tgi.label.search" /></button>
                             </div>
                         </div>   
                         
                        <div class="hotel-count hotel-count1 clearfix noselect">
                                        <div class="multi-rooms clearfix">
                                            <div class="multi-room">
                                                <div class="count-box">
                                                   <div class="room-no"><s:text name="tgi.label.room" /> 1</div>
                                                    <div class="count-adults">
                                                        <span class="title"><s:text name="tgi.label.adult" /></span>
                                                        <span class="add addNo"><i class="fa fa-plus"></i></span>
                                                        <span class="field"><input id="roomCountInput" name="adult1" type="text" value="1" ></span>
                                                        <span class="sub subNo"><i class="fa fa-minus"></i></span>
                                                    </div>
                                                    <div class="count-child">
                                                        <span class="title"><s:text name="tgi.label.child" /></span>
                                                        <span class="add addNo"><i class="fa fa-plus"></i></span>
                                                        <span class="field"><input id="roomCountInput" name="child1" type="text" value="0" ></span>
                                                        <span class="sub subNo"><i class="fa fa-minus"></i></span>
                                                    </div>
                                                    <div class="child-age">
                                                        <div class="select child1">
                                                            <select name="childAge11" class="form-control">
                                                        <option value="0">---</option><option value="0">&lt;1</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                                        </select>
                                                        </div>
                                                        <div class="select child2">
                                                            <select name="childAge12" class="form-control">
                                                        <option value="0">---</option><option value="0">&lt;1</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                                        </select>
                                                        </div>
                                                        <div class="select child3">
                                                            <select name="childAge13" class="form-control">
                                                        <option value="0">---</option><option value="0">&lt;1</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="col-md-12">
                                                 <div class="add-room-section"><s:text name="tgi.label.add" /> <i class="fa fa-plus"></i></div>
                                                 <div class="del-room-section"><s:text name="tgi.label.remove" /> <i class="fa fa-remove"></i></div>
                                                 <div class="can-room-section"><i class="fa fa-arrow-left"></i> <s:text name="tgi.label.back" /></div>
                                                 <small>* <s:text name="tgi.label.adult" /> 12+ , <s:text name="tgi.label.child" /> 0 - 12</small>
                                             </div>
                                        </div>  
                                    </div>

                                                              
                    </form>
			</div>
					
						<div role="tabpanel" class="tab-pane active" id="flight-tab">
							<div class="form-switch-nav">
								<a class="active round-trip-btn" id="" href="#"><s:text name="tgi.label.round_trip" /></a>
								<a class="one-way-btn" id="one-way" href="#"><s:text name="tgi.label.one_way" /></a>
							<!-- 	<a class="multi-way-btn" id="multi-way" href="#">Multiple Destination</a> -->
							</div>
								<script>
                                $(function(){
                                    $('.airportList').autoComplete({
                                        minChars: 3,
                                        source: function(term, suggest){
                                            term = term.toLowerCase();
                                            var choices = $.getJSON('<%=request.getContextPath()%>/flightAutocomplete',
																		{
																			q : term
																		},
																		function(
																				data) {
																			var suggestions = [];
																			$
																					.each(
																							data,
																							function(
																									key,
																									val) {
																								if (~val
																										.toLowerCase()
																										.indexOf(
																												term))
																									suggestions
																											.push(val);
																							});
																			suggest(suggestions);
																		});
													}
												});
							});
						</script>
	<%-- <script>
                                $('document').ready(function () {
                                    var nowDate = new Date();
                                    nowDate.setDate(nowDate.getDate() + 90);
                                    var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
                                
                                    $('.flight-form').on('focus',".datepicker", function(){
                                        $(this).datepicker({
                                            startDate: today,
                                            format: "mm-dd-yyyy",
                                            todayBtn: "linked",
                                            todayHighlight: true
                                        });
                                    });
                                });
                            </script> --%>
						<form method="get" action="${appConfig.ibeLink}flight_result.do" id="flight1"
							class="main-form flight-form flight-form-1 one-wayx multi-wayx">
							<div class="clearfix">
								<span class="multi-field-list"> 
								 <span class="multi-field visible-multi-field">
										<div class="if fl2 flight-from">
											<label for=""><s:text name="tgi.label.from" /> </label> <input
												class="place airportList" name="city1" type="text"
												id="city1" placeholder="Search..." minlength="3" required=""
												aria-required="true" aria-invalid="true" autocomplete="off">
										</div>
										<div class="if fl2 flight-to">
											<label for=""><s:text name="tgi.label.to" /> </label> <input
												class="place airportList" name="city2" type="text"
												id="city2" placeholder="Search..." minlength="3" required=""
												aria-required="true" aria-invalid="true" autocomplete="off">
										</div>

										<div class="if fl2 flight-dep">
											<label for=""><s:text name="tgi.label.depart" /> </label> <input
												type="text" class="datepicker date1 transparent"
												name="date1" type="text" id="date1" placeholder="Depart"
												required="" aria-required="true" aria-invalid="true" style="color:transparent;"
												autocomplete="off">
										</div>

								</span> <span class="multi-field">
										<div class="if fl2 flight-from">
											<label for="">#2 <s:text name="tgi.label.flights" />
												<s:text name="tgi.label.from" /></label> <input class="place airportList"
												name="city11" type="text" id="city11"
												placeholder="Search..." minlength="3" 
												aria-invalid="true">
										</div>
										<div class="if fl2 flight-to">
											<label for=""><s:text name="tgi.label.to" /></label> <input
												class="place airportList" name="city12" type="text" id="city12"
												placeholder="Search..." minlength="3"
												aria-invalid="true">
										</div>
										<div class="if fl2 flight-dep">
											<label for=""><s:text name="tgi.label.depart" /></label> <input
												name="date11" type="text" id="date11" class="datepicker"
												placeholder="Depart" style="color:transparent;"
												aria-invalid="true" autocomplete="off" >
										</div>
								</span>
								</span>
								<div class="if fl2 flight-ret">
									<label for=""><s:text name="tgi.label.return" /></label> <input
										name="date2" type="text" id="date2"
										class="datepicker date2 transparent" placeholder="Return"
										aria-invalid="true" style="color:transparent;"
										autocomplete="off">
								</div>
								<span class="multi-choice">
									<div class="if fl2 flight-add">
										<button class="addMoreFl">
											<s:text name="tgi.label.add_flight" />
											<i class="fa fa-plus"></i>
										</button>
									</div>
									<div class="if fl2 flight-remove">
										<button class="remFl" style="display: none;">
											<s:text name="tgi.label.remove_flight" />
											<i class="fa fa-remove"></i>
										</button>
									</div>
									<div class="if fl2 flight-trav travlist1">
										<label for=""><s:text name="tgi.label.choice" /> </label>
										<div class="choice-field">
											0
											<s:text name="tgi.label.traveller" />
										</div>
										<!--                                            <input class="choice-field" type="text" placeholder="" value="6 travellers" >-->

										<div class="flight-people">
											<div class="counter req">
												<span class="type"><s:text name="tgi.label.senior" />
												</span> <span class="add"><i class="fa fa-plus"></i></span><span
													class="field"> <input name="seniors" id="seniors"
													type="text" value="0" readonly="readonly"></span><span
													class="sub"><i class="fa fa-minus"></i></span> <span
													class="age"></span>
											</div>
											<div class="counter req">
												<span class="type"><s:text name="tgi.label.adults" />
												</span> <span class="add"><i class="fa fa-plus"></i></span><span
													class="field"> <input name="adults" id="adults"
													type=text value="1" readonly="readonly"></span><span
													class="sub"><i class="fa fa-minus"></i></span> <span
													class="age">12+</span>
											</div>
											<div class="counter req">
												<span class="type"><s:text name="tgi.label.student" />
												</span> <span class="add"><i class="fa fa-plus"></i></span><span
													class="field"> <input name="student" id="student"
													type="text" value="0" readonly="readonly"></span><span
													class="sub"><i class="fa fa-minus"></i></span> <span
													class="age"></span>
											</div>
											<div class="counter">
												<span class="type"><s:text name="tgi.label.chilred" />
												</span> <span class="add"><i class="fa fa-plus"></i></span><span
													class="field"> <input name="child" id="child"
													type="text" value="0" readonly="readonly"></span><span
													class="sub"><i class="fa fa-minus"></i></span> <span
													class="age">2-12</span>
											</div>
											<div class="counter">
												<span class="type"><s:text name="tgi.label.infant" />
												</span> <span class="add"><i class="fa fa-plus"></i></span><span
													class="field"> <input name="infant" id="infant"
													type="text" value="0" readonly="readonly"></span><span
													class="sub"><i class="fa fa-minus"></i></span> <span
													class="age">0-2</span>
											</div>
											<div class="ok-btn">
												<s:text name="tgi.label.done" />
											</div>
										</div>
									</div>

								</span>

								<script>
									$(document)
											.ready(
													function() {
														var scntDiv = $('.multi-field-list');
														var i = $(
																'.multi-field-list .multi-field')
																.size() + 1;
														$('.addMoreFl')
																.on(
																		'click',
																		function() {
																			$(
																					'<span class="multi-field"><div class="if fl2 flight-from"><label for="">#'
																							+ i
																							+ ' Flight From</label><input name="city'+i+'" class="place airportList" type="text" placeholder="Search..."></div><div class="if fl2 flight-to"><label for="">To</label><input name="flTo_'+i+'" class="place" type="text" placeholder="Search..."></div><div class="if fl2 flight-dep"><label for="">Depart</label><input name="flDate_'+i+'" type="text" class="datepicker" placeholder="Depart"></div></span>')
																					.appendTo(
																							scntDiv);

																			if (i > 2) {
																				$(
																						'.remFl')
																						.css(
																								'display',
																								'inline-block');
																			} else {
																				$(
																						'.remFl')
																						.css(
																								'display',
																								'none');
																			}

																			i++;
																			return false;
																		});

														$('.remFl')
																.on(
																		'click',
																		function() {
																			if (i > 3) {
																				$(
																						".multi-field-list .multi-field:last-child")
																						.remove()
																				i--;
																			}
																			if (i > 3) {
																				$(
																						'.remFl')
																						.css(
																								'display',
																								'inline-block');
																			} else {
																				$(
																						'.remFl')
																						.css(
																								'display',
																								'none');
																			}
																			return false;
																		});
													});
								</script>
							</div>
							<div class="clearfix mt10">
								<!--   <div class="if fl2 ifcheckfox">
                                       <div class=""><input type="checkbox" class="js-switch" id="airplace-nonstop" />  <label for="airplace-nonstop">NonStop Only</label></div>
                                   </div>
                                   <div class="if fl2 ifcheckfox">
                                       <div class=""><input type="checkbox" class="js-switch" id="airplane-refundable" />   <label for="airplane-refundable">Refundable flights only</label></div>
                                   </div> -->
                                   
								<div class="if fl2 flight-">
									 <s:select list="flightAirlineList" listKey="name"
										listValue="name" name="airline" id="airline" headerKey="All"
										headerValue="All" cssClass="show-tick form-control"
										data-live-search="true" />
								</div>
								<div class="if fl2 flight-selectpicker">
									<select id="basic" class="show-tick form-control"
										data-live-search="true">
										<option value=""><s:text name="tgi.label.any_class" />
										</option>
										<option><s:text name="tgi.label.first_class" /></option>
										<option><s:text name="tgi.label.business" /></option>
										<option><s:text name="tgi.label.economy" /></option>
										<option><s:text name="tgi.label.premium_economy" />
										</option>
									</select>
								</div>
								<div class="if fl2 flight-form-sumbit">
									<button class="form-sumbit">
										<s:text name="tgi.label.submit" />
										<i class="fa fa-arrow-right"></i>
									</button>
								</div>
							</div>
							<input type="hidden" name="flightTripType" id="flightTripType"
								value="R"> <input type="hidden" name="crossSellItem"
								id="crossSellItem" value="F" /> <input type="hidden"
								name="tripcount" id="tripcount" value="2">
							<s:hidden name="request_locale" value="%{request_locale}" />
							<s:hidden name="requestadmindata" value="%{#session.adminLoginKey}" />
									<s:hidden name="sessionKey" />
						</form>
			</div>	
			
			<div role="tabpanel" class="tab-pane" id="hotel-tab">
                         <%--   <script>
                                $('document').ready(function () {
                                    var nowDate = new Date();
                                    nowDate.setDate(nowDate.getDate() + 90);
                                    var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
                                
                                    $('.hotel-page-form').on('focus',".datepicker", function(){
                                        $(this).datepicker({
                                            startDate: today,
                                            format: "mm-dd-yyyy",
                                            todayBtn: "linked",
                                            todayHighlight: true
                                        });
                                    });
                                });
                            </script> --%>
					<script>
                                $(function(){
                                    $('.airportList').autoComplete({
                                        minChars: 3,
                                        source: function(term, suggest){
                                            term = term.toLowerCase();
                                            var choices = $.getJSON('<%=request.getContextPath()%>/flightAutocomplete',
																	{
																		q : term
																	},
																	function(
																			data) {
																		var suggestions = [];
																		$
																				.each(
																						data,
																						function(
																								key,
																								val) {
																							if (~val
																									.toLowerCase()
																									.indexOf(
																											term))
																								suggestions
																										.push(val);
																						});
																		suggest(suggestions);
																	});
												}
											});
						});
					</script>
					<form method="get" id="hotel1" action="${appConfig.ibeLink}hotel_result.do"
						class="main-form hotel-page-form hotel-multi-room-form flight-form-2"
						novalidate="novalidate" class="hotel-page-form">
						<div class="clearfix">
							<div class="if fl2 hotel-from">
								<label for=""><s:text name="tgi.label.place_from" /> </label> <input
									id="city" name="city" class="place airportList" minlength="3"
									type="text" required aria-required="true" aria-invalid="true"
									placeholder="Search...">
							</div>
							<div class="if fl2 hotel-in">
								<label for=""><s:text name="tgi.label.check_in" /></label> <input
									type="text" name="checkInDate" id="checkInDate"
									class="datepicker date1 transparent" required
									aria-required="true" aria-invalid="true" placeholder="Date" style="color:transparent;">
							</div>
							<div class="if fl2 hotel-out">
								<label for=""><s:text name="tgi.label.check_out" /> </label> <input
									type="text" name="checkOutDate" id="checkOutDate"
									class="datepicker date2 transparent" placeholder="Date"
									required="" aria-required="true" aria-invalid="true" style="color:transparent;">
							</div>
							<div class="if fl2 hotel-name">
								<label for=""><s:text name="tgi.label.hotel_name" /></label> <input
									type="text" name="hotelName" placeholder="Hotel Name..">
							</div>
						</div>
						<div class="room-rel">
							<div class="if fl2 hotel-rooms hotel-rooms2">
								<div class="choice-field">
									<span class="perup">1</span>
									<s:text name="tgi.label.persons" />
									, <span class="roomup">1</span>
									<s:text name="tgi.label.room" />
									<i class="pull-right fa fa-caret-down"></i>
								</div>
								<input id="guestCount" name="guestCount" type="hidden" value="1">
								<input id="numberOfRooms" name="numberOfRooms" type="hidden"
									value="1">
							</div>
							<div class="hotel-count hotel-count2 clearfix noselect">
								<div class="multi-rooms clearfix">
									<div class="col-md-3 col-sm-6 multi-room">
										<div class="count-box">
											<div class="room-no">
												<s:text name="tgi.label.room"></s:text>
												1
											</div>
											<div class="count-adults">
												<span class="title"><s:text name="tgi.label.adults" /><br>(12+)
												</span> <span class="add addNo"><i class="fa fa-plus"></i></span> <span
													class="field"><input id="roomCountInput"
													name="adult1" type="text" value="1"></span> <span
													class="sub subNo"><i class="fa fa-minus"></i></span>
											</div>
											<div class="count-child">
												<span class="title"><s:text name="tgi.label.children" /><br>(0
													- 11) </span> <span class="add addNo"><i class="fa fa-plus"></i></span>
												<span class="field"><input id="roomCountInput"
													name="child1" type="text" value="0"></span> <span
													class="sub subNo"><i class="fa fa-minus"></i></span>
											</div>
											<div class="child-age">
												<div class="select child1">
													<select name="childAge11" class="form-control">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
													</select>
												</div>
												<div class="select child2">
													<select name="childAge12" class="form-control">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
													</select>
												</div>
												<div class="select child3">
													<select name="childAge13" class="form-control">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="add-room-section">
										<s:text name="tgi.label.add_room" />
										<i class="fa fa-plus"></i>
									</div>
									<div class="del-room-section">
										<s:text name="tgi.label.remove_room" />
										<i class="fa fa-remove"></i>
									</div>
									<div class="can-room-section">
										<i class="fa fa-arrow-left"></i>
										<s:text name="tgi.label.back" />
									</div>
								</div>

							</div>
						</div>
						<div class="clearfix">
							<div class="if fl2 car-form-sumbit">
								<s:hidden name="request_locale" value="%{request_locale}" />
								<s:hidden name="requestadmindata" value="%{#session.adminLoginKey}" />
									<s:hidden name="sessionKey" />
								<button class="form-sumbit form-sm">
									<s:text name="tgi.label.submit" />
									<i class="fa fa-arrow-right"></i>
								</button>
							</div>
						</div>
					</form>
                        </div>
                        
			<div role="tabpanel" class="tab-pane" id="car-tab">
                            <div class="car-type-nav">
                                <a class="active same-drop-btn" id="" href="#"><s:text name="tgi.label.same_dropoff" /> </a>
                                <a class="diff-drop-btn" id="diff-drop" href="#"><s:text name="tgi.label.differnet_dropoff" /></a>
                            </div>
                            <script>
                                $(function(){
                                    $('.airportList').autoComplete({
                                        minChars: 3,
                                        source: function(term, suggest){
                                            term = term.toLowerCase();
                                            var choices = $.getJSON('<%=request.getContextPath()%>/flightAutocomplete',
																	{
																		q : term
																	},
																	function(
																			data) {
																		var suggestions = [];
																		$
																				.each(
																						data,
																						function(
																								key,
																								val) {
																							if (~val
																									.toLowerCase()
																									.indexOf(
																											term))
																								suggestions
																										.push(val);
																						});
																		suggest(suggestions);
																	});
												}
											});
						});
					</script>
					 <%-- <script>
                                $('document').ready(function () {
                                    var nowDate = new Date();
                                    nowDate.setDate(nowDate.getDate() + 90);
                                    var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
                                
                                    $('.cars-form').on('focus',".datepicker", function(){
                                        $(this).datepicker({
                                            startDate: today,
                                            format: "mm-dd-yyyy",
                                            todayBtn: "linked",
                                            todayHighlight: true
                                        });
                                    });
                                });
                            </script> --%>
					<form method="get" action="${appConfig.ibeLink}car_result.do" id="car1"
						class="main-form cars-form flight-cars-form">
						<div class="clearfix">
							<div class="if fl2 car-pick">
								<label for=""><s:text name="tgi.label.pickup" /> </label> <input
									name="pickUpCity" type="text" id="pickUpCity"
									class="airportList" minlength="3" type="text" required=""
									aria-required="true" aria-invalid="true" placeholder="From"
									autocomplete="off">
							</div>
							<div class="if fl2 car-drop">
								<label for=""><s:text name="tgi.label.drop_off" /> </label> <input
									name="returnCity" type="text" id="returnCity"
									class="airportList" placeholder="From" minlength="3"
									 aria-invalid="true"
									autocomplete="off">
							</div>
							<div class="if fl2 car-dt">
								<label for=""><s:text name="tgi.label.pickup_date" /> </label>
								<input name="pickUpDate" type="text" id="pickUpDate"
									class="datepicker date1 transparent" type="text" required=""
									aria-required="true" aria-invalid="true"
									placeholder="Pick-up Date" autocomplete="off" style="color:transparent;">
							</div>
							<div class="if fl1 car-tt">
								<label for="">&nbsp;</label> <input type="text"
									name="pickUpTime" id="pickUpTime" class="clockpicker "
									placeholder="Time" required="" aria-required="true"
									aria-invalid="true" autocomplete="off" value="09:00">
							</div>
							<div class="if fl2 car-dt">
								<label for=""><s:text name="tgi.label.drop_off" /> <i
									class="icon-chevron-up"></i> <s:text name="tgi.label.date" />
								</label> <input name="returnDate" type="text" id="returnDate"
									class="datepicker date2 transparent" type="text" required=""
									aria-required="true" aria-invalid="true"
									placeholder="Drop-of Date" autocomplete="off" style="color:transparent;">
							</div>
							<div class="if fl1 car-tt">
								<label for="">&nbsp;</label> <input name="returnTime"
									id="returnTime" class="clockpicker" type="text"
									placeholder="Time" required="" aria-required="true"
									aria-invalid="true" autocomplete="off" value="09:00">
							</div>
						</div>
						<div class="clearfix">
							<%-- <div class="if fl2 car-type">

								<s:select list="carTypeList" listKey="code" listValue="type"
									id="SIPPCode" name="SIPPCode" headerKey=""
									headerValue="Car Type"
									cssClass="show-tick form-control"
									data-live-search="true" /> 

							</div>
							<div class="if fl2 car-rental">

							 	<s:select list="carVendorCompanyList" listKey="carCode"
									listValue="name" id="prefVendor" name="prefVendor" headerKey=""
									headerValue="Rental Car Company"
									cssClass="show-tick form-control"
									data-live-search="true" /> 
							</div>
							<!-- <div class="if fl2 car-code">
                                        <input class="if-sm" type="text" placeholder="Do you have a discount code?">
                                   </div> -->
							<div class="if fl2 car-option-box">
								<div class="if-sm car-options-select">
									<s:text name="tgi.label.more_options" />
									&nbsp; <i class="fa fa-caret-down"></i>
								</div>
								<div class="car-options clearfix">
									<div class="if option-item ifcheckfox">
										<div class="">
											<input type="checkbox" class="js-switch" id="car-infant-seat" />
											<label for="car-infant-seat"><s:text
													name="tgi.label.infant_seat" /></label>
										</div>
									</div>
									<div class="if option-item ifcheckfox">
										<div class="">
											<input type="checkbox" class="js-switch" id="car-toddle-seat" />
											<label for="car-toddle-seat"><s:text
													name="tgi.label.toddler_seat" /></label>
										</div>
									</div>
									<div class="if option-item ifcheckfox">
										<div class="">
											<input type="checkbox" class="js-switch" id="car-nav-sys" />
											<label for="car-nav-sys"><s:text
													name="tgi.label.navigation_system" /> </label>
										</div>
									</div>
									<div class="if option-item ifcheckfox">
										<div class="">
											<input type="checkbox" class="js-switch" id="car-ski-rack" />
											<label for="car-ski-rack"><s:text
													name="tgi.label.ski_rack" /></label>
										</div>
									</div>
									<div class="if option-item ifcheckfox">
										<div class="">
											<input type="checkbox" class="js-switch" id="car-snow-chains" />
											<label for="car-snow-chains"><s:text
													name="tgi.label.snow_chains" /> </label>
										</div>
									</div>
									<div class="if option-item ifcheckfox">
										<div class="">
											<input type="checkbox" class="js-switch" id="car-lh-control" />
											<label for="car-lh-control"><s:text
													name="tgi.label.left_hand_control" /></label>
										</div>
									</div>
									<div class="if option-item ifcheckfox">
										<div class="">
											<input type="checkbox" class="js-switch" id="car-rh-control" />
											<label for="car-rh-control"><s:text
													name="tgi.label.right_hand_control" /> </label>
										</div>
									</div>
								</div>
							</div> --%>
							<div class="if fl2 car-form-sumbit">
								<button class="form-sumbit">
									<s:text name="tgi.label.submit" />
									<i class="fa fa-arrow-right"></i>
								</button>
							</div>
							<div class="if fl2 car-form-sumbit">
								<input type="hidden" id="carSearchType" name="carSearchType"
									value="S" />
								<s:hidden name="request_locale" value="%{request_locale}" />
								<s:hidden name="requestadmindata" value="%{#session.adminLoginKey}" />
									<s:hidden name="sessionKey" />
							</div>
						</div>
					</form>
                        </div>
		</div>
	</div>
</div>
</section>

</c:if>
      
        <!-- Content Header (Page header) -->
       

        <!-- Main content -->
        <section class="content" style="display:none;">
          <!-- Small boxes (Stat box) -->
          <div class="row">
          
          <div class="col-sm-12 col-md-12">
          
        <!--  flightorder row -->
        <s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isReports()
                   					||  #session.User.userRoleId.isOrder() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isFlight())}">
	         <div class="row">
	               <div class="col-sm-4">
              <!-- small box -->
              <div class="card card-inverse card-primary">
               <div class="card-block p-b-0">
              <!-- <div class="small-week">
               <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer"  > Today <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                          <a href="showFlightWeekOrderList?dateType=WEEK" class="small-box-footer"  >Week<i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm active">
                                <a href="showFlightWeekOrderList?dateType=MONTH" class="small-box-footer"  >Month <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div> -->
              
                <div class="inner" >
                  <h3 id="flightOrders">0<sup style="font-size: 20px"></sup></h3>
                  <p><s:text name="tgi.label.flight_new_orders" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                
                <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showFlightWeekOrderList?dateType=WEEK" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showFlightWeekOrderList?dateType=MONTH" class="small-box-footer"  ><s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
               </div> 
                
               <!--  <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
              </div>
            </div><!-- ./col -->
            <div class="col-sm-4">
              <!-- small box -->
               <div class="card card-inverse card-primary">
               <div class="card-block p-b-0">
             	  <div class="inner">
                  <h3 id="flightconfirmCount">0<sup style="font-size: 20px"></sup></h3>
                  <p><s:text name="tgi.label.confirmed_order_counts" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                 <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showFlightWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showFlightWeekOrderList?dateType=WEEK&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showFlightWeekOrderList?dateType=MONTH&bookingStatus=CONFIRM" class="small-box-footer"  ><s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                
               <%--  <a href="showFlightWeekOrderList?type=flightconfirm" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div>
            
            <div class="col-sm-4">
              <!-- small box -->
                <div class="card card-inverse card-primary">
               <div class="card-block p-b-0">
                <div class="inner">
                  <h3 id="flightpaymentcount">0</h3>
                  <p><s:text name="tgi.label.payment_done" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                  <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showFlightWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showFlightWeekOrderList?dateType=WEEK&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showFlightWeekOrderList?dateType=MONTH&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
               <%--  <a href="showFlightWeekOrderList?type=flightpayment" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div><!-- ./col -->
	         </div>
	         </s:if>
	         <br>
	         <!--  flightorder row ends -->
	         <!--  hotel order row -->
	                      
         <!-- Small boxes (Stat box) -->
         <s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isReports()
                   					||  #session.User.userRoleId.isOrder() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isHotel())}">
          <div class="row">
            <div class="col-sm-4">
              <!-- small box -->
             <div class="card card-inverse card-warning">
               <div class="card-block p-b-0">
              <div class="small-week">
               <!-- <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <a href="showHotelWeekOrderList?dateType=TODAY" class="small-box-footer"  > Today <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                          <a href="showHotelWeekOrderList?dateType=WEEK" class="small-box-footer"  >Week<i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                                <a href="showHotelWeekOrderList?dateType=MONTH" class="small-box-footer"  >Month <i class="fa fa-arrow-circle-right"></i></a>
              </label> -->
              </div>
                <!-- <div class="actions">
                <div class="btn-group btn-group-devided" data-toggle="buttons" >
                 
                </div>
              </div> -->
              	
                <div class="inner" >
                  <h3 id="hotelOrders">0</h3>
                  <p> <s:text name="tgi.label.hotel_new-orders" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                
                <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa  btn-sm ">
                    <a href="showHotelWeekOrderList?dateType=TODAY" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-sm ">
                          <a href="showHotelWeekOrderList?dateType=WEEK" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showHotelWeekOrderList?dateType=MONTH" class="small-box-footer"  ><s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                
                <!-- <a href="showHotelWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
              </div></div>
            </div><!-- ./col -->
            <div class="col-sm-4">
              <!-- small box -->
              <div class="card card-inverse card-warning">
               <div class="card-block p-b-0">
                <div class="inner">
                  <h3 id="hotelconfirmCount">0<sup style="font-size: 20px"></sup></h3>
                  <p><s:text name="tgi.label.confirmed_order_counts" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa  btn-sm ">
                    <a href="showHotelWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-sm ">
                          <a href="showHotelWeekOrderList?dateType=WEEK&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showHotelWeekOrderList?dateType=MONTH&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
               <%--  <a href="showHotelWeekOrderList?type=hotelconfirm" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div><!-- ./col -->
            <div class="col-sm-4">
              <!-- small box -->
              <div class="card card-inverse card-warning">
               <div class="card-block p-b-0">
                <div class="inner">
                  <h3 id="hotelpaymentcount">0</h3>
                  <p><s:text name="tgi.label.payment_done" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
							<div class="small-week small-box-footer">
								<label class="btn btn-transparent grey-salsa  btn-sm ">
									<a
									href="showHotelWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS"
									class="small-box-footer" > <s:text
											name="tgi.label.today" /> <i
										class="fa fa-arrow-circle-right"></i></a>
								</label> <label class="btn btn-transparent grey-salsa btn-sm ">
									<a
									href="showHotelWeekOrderList?dateType=WEEK&paymentStatus=SUCCESS"
									class="small-box-footer" ><s:text
											name="tgi.label.week" /><i class="fa fa-arrow-circle-right"></i></a>
								</label> <label class="btn btn-transparent grey-salsa btn-sm ">
									<a
									href="showHotelWeekOrderList?dateType=MONTH&paymentStatus=SUCCESS"
									class="small-box-footer" ><s:text
											name="tgi.label.month" /> <i
										class="fa fa-arrow-circle-right"></i></a>
								</label>
							</div>
							<%-- <a href="showHotelWeekOrderList?type=hotelpayment" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div><!-- ./col -->
             
          </div>
          </s:if>
          <br>
	         <!--  hotel order row -->
         

          <!-- car order row -->
          <s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isReports()
                   					||  #session.User.userRoleId.isOrder() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isCar())}">
              <div class="row">
	          <div class="col-sm-4">
              <!-- small box -->
              <div class="card card-inverse card-danger">
               <div class="card-block p-b-0">
              
              <!-- <div class="small-week">
               <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer"  > Today <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                          <a href="showFlightWeekOrderList?dateType=WEEK" class="small-box-footer"  >Week<i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm active">
                                <a href="showFlightWeekOrderList?dateType=MONTH" class="small-box-footer"  >Month <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div> -->
               
                <div class="inner" >
                  <h3 id="carOrders">0<sup style="font-size: 20px"></sup></h3>
                  <p><s:text name="tgi.label.car_new_orders" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                
                <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showCarWeekOrderList?dateType=TODAY" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showCarWeekOrderList?dateTyp=WEEK" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showCarWeekOrderList?dateTyp=MONTH" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                
                
               <!--  <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
              </div></div>
            </div><!-- ./col -->
            <div class="col-sm-4">
              <!-- small box -->
              <div class="card card-inverse card-danger">
               <div class="card-block p-b-0">
                <div class="inner">
                  <h3 id="carconfirmCount">0<sup style="font-size: 20px"></sup></h3>
                  <p><s:text name="tgi.label.confirmed_order_counts" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showCarWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showCarWeekOrderList?dateTyp=WEEK&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showCarWeekOrderList?dateTyp=MONTH&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                			
                <%-- <a href="showCarWeekOrderList?bookingStatus=CONFIRM" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div><!-- ./col -->
            <div class="col-sm-4">
              <!-- small box -->
             <div class="card card-inverse card-danger">
               <div class="card-block p-b-0">
                <div class="inner">
                  <h3 id="carpaymentcount">0</h3>
                  <p><s:text name="tgi.label.payment_done" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                 <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showCarWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showCarWeekOrderList?dateTyp=WEEK&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showCarWeekOrderList?dateTyp=MONTH&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                <%-- <a href="showCarWeekOrderList?paymentStatus=SUCCESS" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div><!-- ./col -->
	         </div>
	         </s:if>
	         <br>
          <!-- end car order row -->
          
          
          <!-- tour order row -->
          <s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isReports()
                   					||  #session.User.userRoleId.isOrder() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isLimo())}">
          <!-- limo order row -->
			    <div class="row">
	                     <div class="col-sm-4">
              <!-- small box -->
             <div class="card card-inverse card-purple">
               <div class="card-block p-b-0">
              
              <!-- <div class="small-week">
               <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer"  > Today <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                          <a href="showFlightWeekOrderList?dateType=WEEK" class="small-box-footer"  >Week<i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm active">
                                <a href="showFlightWeekOrderList?dateType=MONTH" class="small-box-footer"  >Month <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div> -->
               
                <div class="inner" >
                  <h3 id="limoOrders">0<sup style="font-size: 20px"></sup></h3>
                  <p><s:text name="tgi.label.limo_new_orders" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                
                <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showLimoWeekOrderList?dateType=TODAY" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showLimoWeekOrderList?dateType=WEEK" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showLimoWeekOrderList?dateType=MONTH" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                
                
               <!--  <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
              </div></div>
            </div><!-- ./col -->
            <div class="col-sm-4">
              <!-- small box -->
              <div class="card card-inverse card-purple">
               <div class="card-block p-b-0">
                <div class="inner">
                  <h3 id="limoconfirmCount">0<sup style="font-size: 20px"></sup></h3>
                  <p><s:text name="tgi.label.confirmed_order_counts" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showLimoWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showLimoWeekOrderList?dateType=WEEK&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showLimoWeekOrderList?dateType=MONTH&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                
                <%-- <a href="showLimoWeekOrderList?bookingStatus=CONFIRM" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div><!-- ./col -->
            <div class="col-sm-4">
              <!-- small box -->
             <div class="card card-inverse card-purple">
               <div class="card-block p-b-0">
                <div class="inner">
                  <h3 id="limopaymentcount">0</h3>
                  <p><s:text name="tgi.label.payment_done" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                 <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showLimoWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showLimoWeekOrderList?dateType=WEEK&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showLimoWeekOrderList?dateType=MONTH&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                <%-- <a href="showLimoWeekOrderList?paymentStatus=SUCCESS" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div><!-- ./col -->
	         </div><br>
			  </s:if>        
          <!-- end limo order row -->
          <s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isReports()
                   					||  #session.User.userRoleId.isOrder() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isTour())}">
          <div class="row">
	          <div class="col-sm-4">
              <!-- small box -->
              <div class="card card-inverse card-success">
               <div class="card-block p-b-0">
              
              <!-- <div class="small-week">
               <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer"  > Today <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                          <a href="showFlightWeekOrderList?dateType=WEEK" class="small-box-footer"  >Week<i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm active">
                                <a href="showFlightWeekOrderList?dateType=MONTH" class="small-box-footer"  >Month <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div> -->
               
                <div class="inner" >
                  <h3 id="tourOrders">0<sup style="font-size: 20px"></sup></h3>
                  <p><s:text name="tgi.label.tour_new_orders" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                
                <div class="small-week small-box-footer">
               <label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showTourWeekOrderList?dateType=TODAY" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showTourWeekOrderList?dateType=WEEK" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showTourWeekOrderList?dateType=MONTH" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                
                
               <!--  <a href="showFlightWeekOrderList?dateType=TODAY" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
              </div></div>
            </div><!-- ./col -->
            <div class="col-sm-4">
              <!-- small box -->
              <div class="card card-inverse card-success">
               <div class="card-block p-b-0">
                <div class="inner">
                  <h3 id="tourconfirmCount">0<sup style="font-size: 20px"></sup></h3>
                  <p><s:text name="tgi.label.confirmed_order_counts" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                
                 <div class="small-week small-box-footer">
               		<label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showTourWeekOrderList?dateType=TODAY&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showTourWeekOrderList?dateType=WEEK&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showTourWeekOrderList?dateType=MONTH&bookingStatus=CONFIRM" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
              </label>
              </div>
                <%-- <a href="showTourWeekOrderList?bookingStatus=CONFIRM" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div><!-- ./col -->
            <div class="col-sm-4">
              <!-- small box -->
              <div class="card card-inverse card-success">
               <div class="card-block p-b-0">
                <div class="inner">
                  <h3 id="tourpaymentcount">0</h3>
                  <p><s:text name="tgi.label.payment_done" /></p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                  <div class="small-week small-box-footer">
               		<label class="btn btn-transparent grey-salsa btn-sm ">
                    <a href="showTourWeekOrderList?dateType=TODAY&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.today" /> <i class="fa fa-arrow-circle-right"></i></a>
                    </label>
                    <label class="btn btn-transparent grey-salsa  btn-sm ">
                          <a href="showTourWeekOrderList?dateType=WEEK&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.week" /> <i class="fa fa-arrow-circle-right"></i></a>
                                </label>
                                <label class="btn btn-transparent grey-salsa btn-sm ">
                                <a href="showTourWeekOrderList?dateType=MONTH&paymentStatus=SUCCESS" class="small-box-footer"  > <s:text name="tgi.label.month" /> <i class="fa fa-arrow-circle-right"></i></a>
             			 </label>
              			</div>
                <%-- <a href="showTourWeekOrderList?type=tourpayment" class="small-box-footer"><s:text name="tgi.label.more_info" /> <i class="fa fa-arrow-circle-right"></i></a> --%>
              </div></div>
            </div><!-- ./col -->
	         </div>
	         </s:if>
	         <br>
          
         </div>
          </div>
          <!--  Number of agents and distributers ends --> 
          
                  <!--  Number of agents and distributers -->  
          <div class="col-sm-12 col-md-12 onlyagents">
          <div class="row">
             <s:if test="%{#session.User.userRoleId.isSuperUser()}">
                      <div class="col-sm-6">
                      <div class="row">
              <!-- small box -->
              <div class="small-box bg-orange-active">
                <div class="inner clearfix" >
                   <div class="col-sm-7">
                  <h3 id="distributorlist">0</h3>
                  </div>
                  <div class="col-sm-5 text-right">
              
                  <p><a href="getAllDistributors" class="small-box-footer"><s:text name="tgi.label.distributors" />  
                <i class="fa fa-arrow-circle-right"></i></a></p>
              
                  </div>
                  
                  </div>
                 
                </div>             
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
              </s:if>
            
            <!--   Agents  -->
           <s:if test="%{(#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isDistributor())}">
          <div class="col-sm-6">
          <div class="row">
              <!-- small box -->
              <div class="small-box bg-lime-active">
							<div class="inner clearfix">
								<div class="inner">
									<div class="col-sm-6">
										<h3 id="agentlist">1</h3>
									</div>
									<div class="col-sm-5 text-right">
										<p><a href="AllAgencyList" class="small-box-footer"> <s:text
													name="tgi.label.agents" /> <i
												class="fa fa-arrow-circle-right"></i></a>
										</p>
									</div>

								</div>

							</div>
							<div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                
              </div>
            </div>
          </div>
          </s:if>
          </div>
          </div>

            
            
            
            

          

          
          
          <div class="row">

          <div class="col-sm-12 caption-subject">
            <h4><span><s:text name="tgi.label.sales_summary" /></span></h4>
          </div>
<div class="col-md-4">
                    <div class="pnl">
                        <div class="hd clearfix">
                           <h5><s:text name="tgi.label.weekly_sales" /></h5>
                           <div class="set pull-right">
                                <a href="#" class="fa fa-refresh"></a>
                                <a href="#" class="fa fa-expand"></a>
                           </div>
                        </div>
                        <div class="cnt">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="chart-dot-list">
                                        <li class="air"><s:text name="tgi.label.air" /></li>
                                        <li class="hotel"><s:text name="tgi.label.hotel" /></li>
                                       <!--  <li class="car">Car</li> -->
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <div class="chart chart-doughnut">
                                        <canvas id="chart-area" width="262" height="196" style="width: 262px; height: 196px;">
                                    </canvas></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="pnl ">
                        <div class="hd clearfix">
                           <h5><s:text name="tgi.label.weekly_sales" /></h5>
                           <div class="set pull-right">
                                <a href="#" class="fa fa-refresh"></a>
                                <a href="#" class="fa fa-expand"></a>
                           </div>
                        </div>
                        <div class="cnt">
                            <div class="chart">
                                <canvas id="chart-bar" height="289" width="867" style="width: 867px; height: 289px; "></canvas>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="pnl">
                        <div class="hd clearfix">
                           <h5><s:text name="tgi.label.monthly_sales" /></h5>
                           <div class="set pull-right">
                                <a href="#" class="fa fa-refresh"></a>
                                <a href="#" class="fa fa-expand"></a>
                           </div>
                        </div>
                        <div class="cnt">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="chart-dot-list">
                                        <li class="air"><s:text name="tgi.label.air" /></li>
                                        <li class="hotel"><s:text name="tgi.label.hotel" /></li>
                                       <!--  <li class="car">Car</li> -->
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <div class="chart chart-doughnut">
                                        <canvas id="chart-area-month" width="262" height="196" style="width: 262px; height: 196px;">
                                    </canvas></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="pnl ">
                        <div class="hd clearfix">
                           <h5><s:text name="tgi.label.monthly_sales" /></h5>
                           <div class="set pull-right">
                                <a href="#" class="fa fa-refresh"></a>
                                <a href="#" class="fa fa-expand"></a>
                           </div>
                        </div>
                        <div class="cnt">
                            <div class="chart">
                                <canvas id="chart-bar-month" height="289" width="867" style="width: 867px; height: 289px;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
</div>

       
        </section><!-- /.content -->
      </section>
      <!-- /.content-wrapper -->
     
<script src="admin/js/jquery.validate.min.js"></script>
     <script src="admin/js/Chart.min.js"></script>
    <script src="admin/js/chartDataSample.js"></script>
     <!-- 2. Include library -->
    <!-- 3. Instantiate clipboard by passing a string selector -->
    <script>
    var clipboard = new Clipboard('.btn');
    clipboard.on('success', function(e) {
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>
<script>
	$(document).ready(function() {
		tgi_date('.flight-cars-form');
		tgi_date('.flight-form-1');// flight
		tgi_date('.flight-form-2'); // FHC

		$('#flight1').validate();
	});
</script>
    