<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style>
.inner-addon .glyphicon {
    position: absolute;
    padding: 5px 5px 5px 5px;
    pointer-events: none;
    margin-top: 25px;
}
.right-addon .glyphicon {
    right: 20px;
}
</style>
            <section class="wrapper container-fluid">
                <div class="">
                    <div class="">
                    	<div class="">
                        <div class="pnl">
                        <div class="hd clearfix">
                                <h5 class="pull-left">Add Flight Trip</h5>
                        	  <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-outline-danger" href="companyList"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;<s:text name="tgi.label.company_list" /></a>
									</div>
									</div>
                               </div>
                                <div class="set pull-center">        
						<div class="col-sm-9 clearfix " data-placement="top">
						<a href="addTourTrip" class="btn btn-top-link btn-xs"> All Trips </a>
						<a href="addTourTrip" class="btn btn-top-link btn-xs"> Tour Trips </a>
						<a href="addHotelTrip" class="btn btn-top-link btn-xs"> Hotel Trips </a> 
						<a href="addFlightTrip" class="btn btn-top-link btn-xs"> Flight Trips </a> 
						<a href="addCarTrip" class="btn btn-top-link btn-xs"> Car Trips </a> 
						<a href="addMiscellaneousTrip" class="btn btn-top-link btn-xs"> Miscellaneous Trips </a>
					</div>
					 </div> 
                            </div>
   <form method="post" class="form-horizontal ng-pristine ng-valid" name="saveFlightTripForm" id="saveFlightTripForm">                      
    <section class="customer_details">
				 <div class="col-md-12">
				 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse01" aria-expanded="false" aria-controls="collapseOne">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Customer Details</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse01" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="">
                                        <div class="panel-body">
								
					
									<div class="col-sm-3">
									<div class="form-group">
										<label for="tripType">Trip Type</label> <select name="flightTravelRequest.tripType" class="form-control input-sm " id="tripT" required="required">
											<!-- <option value="">Select Trip Type</option> -->
											<option value="O" selected="selected">One Way</option>
											<option value="R">Round Trip</option>
											 <option value="M">Multi Trip</option>
										</select>
										
									</div>
									</div>
									<div class="col-sm-3">
									<div class="form-group">
									<label for="flightOrderRow.managementFeesdummy">Product Type<span id="mandatory"> * </span></label> 
									<select class="form-control input-sm productTypeVal" id="managementFeesForSend" name="flightOrderRow.managementFeesdummy" required="required" onchange="getmangmentfee(this)"> 
										<option value="00.00" selected="selected">Select Product Type</option>
										<option value="150.000">Domestic</option>
										<option value="100.000">International</option>
										<option value="0.000000">Domestic Reissue</option>
										<option value="0.000000">International Reissue</option>
									</select>
									</div>
									</div>
									<input type="hidden" autocomplete="off" name="flightOrderRow.productType" class="form-control input-sm" required="required" id="productType" placeholder="Passenger Count" value="">
								    <input type="hidden" autocomplete="off" name="flightTravelRequest.passengerCount" class="form-control input-sm" required="required" id="PassengerCountNew" placeholder="Passenger Count" value="1">
									<div class="col-sm-3">
									<div class="form-group">
										<label for="booking-date" class="control-label">Booking Date</label> 
										<input type="text" autocomplete="off" name="flightOrderRow.bookingDate" class="form-control input-sm datetimepicker" id="bookingDate" placeholder="Booking Date" required="required">
									</div>
									</div>
								</div>
                                </div>
                                </div>
                                </div>
                                </div>
    </section>
    <section class="passenger_details">
                                <div class="col-md-12">
								 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse02" aria-expanded="false" aria-controls="collapseTwo">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Passenger Details</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="">
                                        <div class="panel-body"> 
                       		 <div class="panel-group" id="passengerNested">
								<div class="panel panel-default passeng1" style="border: 1px solid #ccc;" id="passlength">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#passengerNested" href="#collapseinside">
											<b>Passenger Details 1  </b>
										</a>
									</h4>
								</div>
									<div id="collapseinside" class="panel-collapse collapse in">
									<div class="panel-body passval">
									<div class="detailswithRM"> 	 
										<div class="col-sm-1">
											<div class="form-group">
												<label for="currency" class="control-label">PassengerType
												</label> <select class="form-control input-sm" name="flightOrderCustomerList[0].passengerTypeCode">
													<option value="ADT">Adult</option>
													<option value="INF">Infant</option>
													<option value="CHD">Child</option>
													<option value="MASTER">Master</option>

												</select>
												
											</div>
										</div>
										<div class="col-sm-1">
											<div class="form-group">
												<label for="hotelName" class=" control-label">Title
												</label> <select name="flightOrderCustomerList[0].title" class="form-control input-sm" id="title1">
													<option value="Mr" selected="selected">Mr</option>
													<option value="Mrs">Mrs</option>
													<option value="Miss">Miss</option>
													<option value="Ms">Ms</option>
													<option value="Master">Master</option>

												</select>
												
											</div>
										</div>

										<div class="col-sm-2">
											<div class="form-group">
												<label for="hotelName" class=" control-label">FirstName
												</label> <input type="text" autocomplete="off" name="flightOrderCustomerList[0].firstName" class="form-control input-sm firstName" required="required" id="firstName" placeholder="First Name" value="">
													

											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label for="hotelChain" class="  control-label">LastName
												</label> <input type="text" autocomplete="off" required="required" name="flightOrderCustomerList[0].lastName" id="lastName" class="form-control input-sm " placeholder="Last Name" value="">

											</div>
										</div>
										<div class="col-sm-2">
															<div class="form-group">
																<label for="City" class=" control-label">Date of Birth</label>

																<input type="text" autocomplete="off" name="flightOrderCustomerList[0].dateOfBirth" id="dateOfBirth" class="form-control input-sm" placeholder="DOB">
																	

															</div>
														</div>
											<div class="col-sm-2">
															<div class="form-group">
																<label for="mobile" class=" control-label">Mobile Number </label>

																<input type="text" autocomplete="off" name="flightOrderCustomerList[0].mobile" id="mobileNo" class="form-control input-sm" placeholder="Mobile">
																	

															</div>
											 </div>
										<div class="col-sm-2">
															<div class="form-group">
																<label for="City" class=" control-label">Email </label>

																<input type="email" autocomplete="off" name="flightOrderCustomerList[0].email" id="emailId" class="form-control input-sm" placeholder="Email">
																	

															</div>
											 </div>
														 <div class="col-sm-2">
											<div class="form-group">
												<label for="hotelName" class=" control-label">Booking Class
												</label> <select name="flightOrderCustomerList[0].bookingClassPreffer" class="form-control input-sm" id="bookingClassPreffer">
													<option value="Economy">Economy</option>
													<option value="All">ALL</option>
													<option value="PremiumEconomy">Premium Economy</option>
													<option value="Business">Business</option>
													<option value="PremiumBusiness">Premium Business</option>
												</select>
											</div>
										</div> 
									<div class="col-sm-2">
											<div class="form-group">
												<label for="hotelChain" class="  control-label">Ticket No
												</label> <input type="text" autocomplete="off" name="flightOrderCustomerList[0].eticketnumber" class="form-control input-sm cusValidationAlphaNum" required="required" value="" placeholder="Enter ticket no." id="tno1">

											</div>
										</div>

										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="currency" class=" control-label">Nationality
												</label> <input type="text" class="form-control input-sm " name="flightOrderCustomerList[0].nationality" placeholder="Nationality" id="nationality">
													
											</div>
										</div>
										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="currency" class="  control-label">Passport
													 Date of Issue</label> <input type="text" class="form-control input-sm issueddate hasDatepicker" id="issueddate1" placeholder="Passport Date Of Issue " name="flightOrderCustomerList[0].passport_issuedDate">
													
											</div>
										</div>
										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="currency" class="  control-label">Passport
													Expiry Date </label> <input type="text" class="form-control input-sm expiredate hasDatepicker" id="expiredate1" placeholder="Passport Expire Date" name="flightOrderCustomerList[0].passport_expiryDate">
													
											</div>
										</div>
										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="currency" class="control-label">Passport
													Number </label> <input type="text" class="form-control input-sm cusValidationAlphaNum" placeholder="Passport Number" name="flightOrderCustomerList[0].passportNo" id="passportNumb1">
													
											</div>
										</div>
										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="hotel Country" class=" control-label">Passport Issuing Country </label> 
												<select class="form-control input-sm" name="flightOrderCustomerList[0].passportIssuingCountry">
													
												</select>
												

											</div>
										</div>
							   			<!-- <div class="col-sm-2 filmore" style="margin-top: 25px;">

											<a class="btn btn-primary btn-xs" role="button"
												data-toggle="collapse" href="javascript:void(0)"  onclick="more(this)"  id="fil" > More options </a>

										</div>  
  -->
									</div>
									 <div class="col-sm-2 filmore pull-right" style="margin-top: 25px;">

											<a class="btn btn-primary btn-xs pull-right" role="button" data-toggle="collapse" href="javascript:void(0)" onclick="more(this)" id="fil"> More options </a>

										</div> 
										 
 

						           <div class="col-sm-12  pricebreakup"> 
											<a class="btn btn-success createdquotation collapsed" role="button" data-toggle="collapse" href="#priceFilters" aria-expanded="false" aria-controls="priceFilters"> Price Breakup(s)1 </a>
									</div>  

										<div class="col-sm-12 clearfix pricebb price-details">
											<div class="row">
												<div class="collapse" id="priceFilters" aria-expanded="false" style="height: 0px;">
													<div class="panel-body">
														<!-- Filter of main info -->
														<div class=" col-sm-12 clearfix">
														
														<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">Currency
																	</label> <input type="text" readonly="readonly" autocomplete="off" value="INR" class="form-control input-sm">
																		
																</div>

															</div>		
															
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">Base
																		Fare </label> <input type="text" autocomplete="off" id="baseFare1" required="required" data-name="baseFare" name="flightOrderCustomerPriceBreakupList[0].baseFare" class="form-control input-sm required baseFareprice cusValidationforprice" value="0" placeholder="Base Fare">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">YQ
																	</label> <input type="text" autocomplete="off" id="YQTax1" value="0" data-name="YQTax" name="flightOrderCustomerPriceBreakupList[0].YQTax" class="form-control input-sm cusValidationforprice" placeholder="YQTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">YR Tax
																	</label> <input type="text" autocomplete="off" id="YRTax1" value="0" data-name="YRTax" name="flightOrderCustomerPriceBreakupList[0].YRTax" class="form-control input-sm cusValidationforprice" placeholder="YRTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">WO Tax
																	</label> <input type="text" autocomplete="off" id="WOTax1" value="0" data-name="WOTax" name="flightOrderCustomerPriceBreakupList[0].WOTax" class="form-control input-sm cusValidationforprice" placeholder="WOTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">PSF Tax
																	</label> <input type="text" autocomplete="off" id="PSFTax1" value="0" data-name="PSFTax" name="flightOrderCustomerPriceBreakupList[0].PSFTax" class="form-control input-sm cusValidationforprice" placeholder="PSFTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">UDF Tax
																	</label> <input type="text" autocomplete="off" id="UDFTax1" value="0" data-name="UDFTax" name="flightOrderCustomerPriceBreakupList[0].UDFTax" class="form-control input-sm cusValidationforprice" placeholder="UDFTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2"> 
																<div class="form-group">
																	<label for="hotelName" class=" control-label">K3 Tax
																	</label> <input type="text" autocomplete="off" id="K3Tax1" value="0" data-name="K3Tax" name="flightOrderCustomerPriceBreakupList[0].K3Tax" class="form-control input-sm cusValidationforprice" placeholder="K3Tax" required="required">
																		

																</div>
															</div>
															                     
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">IN Tax
																	</label> <input type="text" autocomplete="off" id="INTax1" value="0" data-name="INTax" name="flightOrderCustomerPriceBreakupList[0].INTax" class="form-control input-sm cusValidationforprice" placeholder="INTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">Transaction Fee
																	</label> <input type="text" autocomplete="off" id="transactionFee1" value="0" data-name="transactionFee" name="flightOrderCustomerPriceBreakupList[0].transactionFee" class="form-control input-sm cusValidationforprice" placeholder="transactionFee" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">G1 Tax
																	</label> <input type="text" autocomplete="off" id="G1Tax1" value="0" data-name="G1Tax" name="flightOrderCustomerPriceBreakupList[0].G1Tax" class="form-control input-sm cusValidationforprice" placeholder="G1Tax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName " class=" control-label">F2 Tax
																	</label> <input type="text" autocomplete="off" id="F2Tax1" value="0" data-name="F2Tax" name="flightOrderCustomerPriceBreakupList[0].F2Tax" class="form-control input-sm cusValidationforprice" placeholder="F2Tax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">F6 Tax
																	</label> <input type="text" autocomplete="off" id="F6Tax1" value="0" data-name="F6Tax" name="flightOrderCustomerPriceBreakupList[0].F6Tax" class="form-control input-sm cusValidationforprice" placeholder="F6Tax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">ZR Tax
																	</label> <input type="text" autocomplete="off" id="ZRTax1" value="0" data-name="ZRTax" name="flightOrderCustomerPriceBreakupList[0].ZRTax" class="form-control input-sm cusValidationforprice" placeholder="ZRTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">YC Tax
																	</label> <input type="text" autocomplete="off" id="YCTax1" value="0" data-name="YCTax" name="flightOrderCustomerPriceBreakupList[0].YCTax" class="form-control input-sm cusValidationforprice" placeholder="YCTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">US Tax
																	</label> <input type="text" autocomplete="off" id="USTax1" value="0" data-name="USTax" name="flightOrderCustomerPriceBreakupList[0].USTax" class="form-control input-sm cusValidationforprice" placeholder="USTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">XA Tax
																	</label> <input type="text" autocomplete="off" id="XATax1" value="0" data-name="XATax" name="flightOrderCustomerPriceBreakupList[0].XATax" class="form-control input-sm cusValidationforprice" placeholder="XATax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">XY Tax
																	</label> <input type="text" autocomplete="off" id="XYTax1" value="0" data-name="XYTax" name="flightOrderCustomerPriceBreakupList[0].XYTax" class="form-control input-sm cusValidationforprice" placeholder="XYTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">AY Tax
																	</label> <input type="text" autocomplete="off" id="AYTax1" value="0" data-name="AYTax" name="flightOrderCustomerPriceBreakupList[0].AYTax" class="form-control input-sm cusValidationforprice" placeholder="AYTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">XF Tax
																	</label> <input type="text" autocomplete="off" id="XFTax1" value="0" data-name="XFTax" name="flightOrderCustomerPriceBreakupList[0].XFTax" class="form-control input-sm cusValidationforprice" placeholder="XFTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label notone">Total Tax
																	</label> <input type="text" autocomplete="off" id="tax1" value="0" data-name="tax" name="flightOrderCustomerPriceBreakupList[0].tax" class="form-control input-sm cusValidationforprice" placeholder="Tax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label notone1">Markup
																	</label> <input type="text" autocomplete="off" name="flightOrderCustomerPriceBreakupList[0].markup" id="markup1" value="0" data-name="markup" class="form-control input-sm cusValidationforprice" placeholder="Markup" required="required">
																		

																</div>
															</div>
																												
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>

								</div>
							</div><div class="additionalpackage"><div class="panel panel-default passeng1" style="border: 1px solid #ccc;" id="passlength">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#passengerNested" href="#collapseinside1">Passenger Details2</a>
									</h4>
								</div>
									<div id="collapseinside1" class="panel-collapse collapse in">
									<div class="panel-body passval">
<div class="detailswithRM"> 	 
										<div class="col-sm-1">
											<div class="form-group">
												<label for="currency" class="control-label">PassengerType
												</label> <select class="form-control input-sm" name="flightOrderCustomerList[1].passengerTypeCode">
													<option value="ADT">Adult</option>
													<option value="INF">Infant</option>
													<option value="CHD">Child</option>
													<option value="MASTER">Master</option>

												</select>
												
											</div>
										</div>
										<div class="col-sm-1">
											<div class="form-group">
												<label for="hotelName" class=" control-label">Title
												</label> <select name="flightOrderCustomerList[1].title" class="form-control input-sm" id="title1">
													<option value="Mr" selected="selected">Mr</option>
													<option value="Mrs">Mrs</option>
													<option value="Miss">Miss</option>
													<option value="Ms">Ms</option>
													<option value="Master">Master</option>

												</select>
												
											</div>
										</div>

										<div class="col-sm-2">
											<div class="form-group">
												<label for="hotelName" class=" control-label">FirstName
												</label> <input type="text" autocomplete="off" name="flightOrderCustomerList[1].firstName" class="form-control input-sm firstName" required="required" id="firstName1" placeholder="First Name" value="">
													

											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label for="hotelChain" class="  control-label">LastName
												</label> <input type="text" autocomplete="off" required="required" name="flightOrderCustomerList[1].lastName" id="lastName1" class="form-control input-sm " placeholder="Last Name" value="">

											</div>
										</div>
										<div class="col-sm-2">
															<div class="form-group">
																<label for="City" class=" control-label">Date of Birth</label>

																<input type="text" autocomplete="off" name="flightOrderCustomerList[1].dateOfBirth" id="dateOfBirth1" class="form-control input-sm dateOfBirth hasDatepicker" placeholder="DOB">
																	

															</div>
														</div>
											<div class="col-sm-2">
															<div class="form-group">
																<label for="mobile" class=" control-label">Mobile Number </label>

																<input type="tel" autocomplete="off" name="flightOrderCustomerList[1].mobile" id="mobileNo1" class="form-control input-sm" placeholder="Mobile">
																	

															</div>
											 </div>
										<div class="col-sm-2">
															<div class="form-group">
																<label for="City" class=" control-label">Email </label>

																<input type="email" autocomplete="off" name="flightOrderCustomerList[1].email" id="emailId1" class="form-control input-sm" placeholder="Email">
																	

															</div>
											 </div>
														 <div class="col-sm-2">
											<div class="form-group">
												<label for="hotelName" class=" control-label">Booking Class
												</label> <select name="flightOrderCustomerList[1].bookingClassPreffer" class="form-control input-sm" id="bookingClassPreffer">
													<option value="Economy">Economy</option>
									<option value="All">ALL</option>
									<option value="PremiumEconomy">Premium Economy</option>
									<option value="Business">Business</option>
									<option value="PremiumBusiness">Premium Business</option>

												</select>
											</div>
										</div> 
									<div class="col-sm-2">
											<div class="form-group">
												<label for="hotelChain" class="  control-label">Ticket No
												</label> <input type="text" autocomplete="off" name="flightOrderCustomerList[1].eticketnumber" class="form-control input-sm cusValidationAlphaNum" required="required" value="" placeholder="Enter ticket no." id="tno11">

											</div>
										</div>
										

										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="currency" class=" control-label">Nationality
												</label> <input type="text" class="form-control input-sm " name="flightOrderCustomerList[1].nationality" placeholder="Nationality" id="nationality1">
													
											</div>
										</div>
										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="currency" class="  control-label">Passport
													 Date of Issue</label> <input type="text" class="form-control input-sm issueddate hasDatepicker" id="issueddate11" placeholder="Passport Date Of Issue " name="flightOrderCustomerList[1].passport_issuedDate">
													
											</div>
										</div>
										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="currency" class="  control-label">Passport
													Expire Date </label> <input type="text" class="form-control input-sm expiredate hasDatepicker" id="expiredate11" placeholder="Passport Expire Date" name="flightOrderCustomerList[1].passport_expiryDate">
													
											</div>
										</div>
										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="currency" class="control-label">Passport
													Number </label> <input type="text" class="form-control input-sm cusValidationAlphaNum" placeholder="Passport Number" name="flightOrderCustomerList[1].passportNo" id="passportNumb11">
													
											</div>
										</div>
										<div class="col-sm-2 mopt" style="display: none;">
											<div class="form-group">
												<label for="hotel Country" class=" control-label">Passport
													Issuing Country </label> <select class="form-control input-sm" name="flightOrderCustomerList[1].passportIssuingCountry">
													
														
													
												</select>
												

											</div>
										</div>
							   			<!-- <div class="col-sm-2 filmore" style="margin-top: 25px;">

											<a class="btn btn-primary btn-xs" role="button"
												data-toggle="collapse" href="javascript:void(0)"  onclick="more(this)"  id="fil" > More options </a>

										</div>  
  -->
									</div>
									 <div class="col-sm-2 filmore pull-right" style="margin-top: 25px;">

											<a class="btn btn-primary btn-xs pull-right" role="button" data-toggle="collapse" href="javascript:void(0)" onclick="more(this)" id="fil2"> More options </a>

										</div> 
										 
 

						           <div class="col-sm-12  pricebreakup"> 
											<a class="btn btn-success createdquotation collapsed" role="button" data-toggle="collapse" href="#priceFilters1" aria-expanded="false" aria-controls="priceFilters"> Price Breakup(s)2</a>
									</div>  
												
												
									
 

										<div class="col-sm-12 clearfix pricebb price-details">
											<div class="row">
												<div class="collapse" id="priceFilters1" aria-expanded="false" style="height: 0px;">
													<div class="panel-body">
														<!-- Filter of main info -->
														<div class=" col-sm-12 clearfix">
														
														<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">Currency
																	</label> <input type="text" readonly="readonly" autocomplete="off" value="INR" class="form-control input-sm">
																		
																</div>

															</div>		
															
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">Base
																		Fare </label> <input type="text" autocomplete="off" id="baseFare2" required="required" data-name="baseFare" name="flightOrderCustomerPriceBreakupList[1].baseFare" class="form-control input-sm required baseFareprice cusValidationforprice" value="0" placeholder="Base Fare">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">YQ
																	</label> <input type="text" autocomplete="off" id="YQTax2" value="0" data-name="YQTax" name="flightOrderCustomerPriceBreakupList[1].YQTax" class="form-control input-sm cusValidationforprice" placeholder="YQTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">YR Tax
																	</label> <input type="text" autocomplete="off" id="YRTax2" value="0" data-name="YRTax" name="flightOrderCustomerPriceBreakupList[1].YRTax" class="form-control input-sm cusValidationforprice" placeholder="YRTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">WO Tax
																	</label> <input type="text" autocomplete="off" id="WOTax2" value="0" data-name="WOTax" name="flightOrderCustomerPriceBreakupList[1].WOTax" class="form-control input-sm cusValidationforprice" placeholder="WOTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">PSF Tax
																	</label> <input type="text" autocomplete="off" id="PSFTax2" value="0" data-name="PSFTax" name="flightOrderCustomerPriceBreakupList[1].PSFTax" class="form-control input-sm cusValidationforprice" placeholder="PSFTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">UDF Tax
																	</label> <input type="text" autocomplete="off" id="UDFTax2" value="0" data-name="UDFTax" name="flightOrderCustomerPriceBreakupList[1].UDFTax" class="form-control input-sm cusValidationforprice" placeholder="UDFTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2"> 
																<div class="form-group">
																	<label for="hotelName" class=" control-label">K3 Tax
																	</label> <input type="text" autocomplete="off" id="K3Tax2" value="0" data-name="K3Tax" name="flightOrderCustomerPriceBreakupList[1].K3Tax" class="form-control input-sm cusValidationforprice" placeholder="K3Tax" required="required">
																		

																</div>
															</div>
															                     
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">IN Tax
																	</label> <input type="text" autocomplete="off" id="INTax2" value="0" data-name="INTax" name="flightOrderCustomerPriceBreakupList[1].INTax" class="form-control input-sm cusValidationforprice" placeholder="INTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">Transaction Fee
																	</label> <input type="text" autocomplete="off" id="transactionFee2" value="0" data-name="transactionFee" name="flightOrderCustomerPriceBreakupList[1].transactionFee" class="form-control input-sm cusValidationforprice" placeholder="transactionFee" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">G1 Tax
																	</label> <input type="text" autocomplete="off" id="G1Tax2" value="0" data-name="G1Tax" name="flightOrderCustomerPriceBreakupList[1].G1Tax" class="form-control input-sm cusValidationforprice" placeholder="G1Tax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName " class=" control-label">F2 Tax
																	</label> <input type="text" autocomplete="off" id="F2Tax2" value="0" data-name="F2Tax" name="flightOrderCustomerPriceBreakupList[1].F2Tax" class="form-control input-sm cusValidationforprice" placeholder="F2Tax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">F6 Tax
																	</label> <input type="text" autocomplete="off" id="F6Tax2" value="0" data-name="F6Tax" name="flightOrderCustomerPriceBreakupList[1].F6Tax" class="form-control input-sm cusValidationforprice" placeholder="F6Tax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">ZR Tax
																	</label> <input type="text" autocomplete="off" id="ZRTax2" value="0" data-name="ZRTax" name="flightOrderCustomerPriceBreakupList[1].ZRTax" class="form-control input-sm cusValidationforprice" placeholder="ZRTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">YC Tax
																	</label> <input type="text" autocomplete="off" id="YCTax2" value="0" data-name="YCTax" name="flightOrderCustomerPriceBreakupList[1].YCTax" class="form-control input-sm cusValidationforprice" placeholder="YCTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">US Tax
																	</label> <input type="text" autocomplete="off" id="USTax2" value="0" data-name="USTax" name="flightOrderCustomerPriceBreakupList[1].USTax" class="form-control input-sm cusValidationforprice" placeholder="USTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">XA Tax
																	</label> <input type="text" autocomplete="off" id="XATax2" value="0" data-name="XATax" name="flightOrderCustomerPriceBreakupList[1].XATax" class="form-control input-sm cusValidationforprice" placeholder="XATax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">XY Tax
																	</label> <input type="text" autocomplete="off" id="XYTax2" value="0" data-name="XYTax" name="flightOrderCustomerPriceBreakupList[1].XYTax" class="form-control input-sm cusValidationforprice" placeholder="XYTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">AY Tax
																	</label> <input type="text" autocomplete="off" id="AYTax2" value="0" data-name="AYTax" name="flightOrderCustomerPriceBreakupList[1].AYTax" class="form-control input-sm cusValidationforprice" placeholder="AYTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label">XF Tax
																	</label> <input type="text" autocomplete="off" id="XFTax2" value="0" data-name="XFTax" name="flightOrderCustomerPriceBreakupList[1].XFTax" class="form-control input-sm cusValidationforprice" placeholder="XFTax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label notone">Total Tax
																	</label> <input type="text" autocomplete="off" id="tax2" value="0" data-name="tax" name="flightOrderCustomerPriceBreakupList[1].tax" class="form-control input-sm cusValidationforprice" placeholder="Tax" required="required">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label for="hotelName" class=" control-label notone1">Markup
																	</label> <input type="text" autocomplete="off" name="flightOrderCustomerPriceBreakupList[1].markup" id="markup2" value="0" data-name="markup" class="form-control input-sm cusValidationforprice" placeholder="Markup" required="required">
																		

																</div>
															</div>
																												
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>

								</div>
							</div></div>
							</div>
												<div class="clearfix add-remove">
													<a class="btn btn-primary" role="button" id="addroom" onclick="add(this);"> <!-- onclick="add() onclick="remove_field()" -->
														Add More
													</a> <a class="btn btn-primary remove_field" id="removeroom" role="button" onclick="remove_field(this)">
														Remove </a>

												</div>
												</div>
                                    </div>
                                </div>
                                </div>
                                </div>
    </section>
    <section class="trip_details">
                                <div class="col-md-12">
								 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse03" aria-expanded="false" aria-controls="collapseThree">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingThree">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Trip Details</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="">
                                        <div class="panel-body">
									<div id="tripdetails" class="clearfix">
									
									<div class="col-md-2 ">
											<div class="form-group">
												<label for="NetRate" class=" control-label">Airline </label> <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
												<input type="text" name="flightOrderTripDetailList[0].operatedByName" autocomplete="off" class="form-control input-sm airline ui-autocomplete-input" required="required" value="" placeholder="Airline" id="airline">
													 	
											</div>
										</div>

										<div class="col-md-2">
											<div class="form-group">
												<label for="NetRate" class=" control-label">FlightNumber </label> 
												<input type="text" name="flightOrderTripDetailList[0].flightNumber" autocomplete="off" class="form-control input-sm cusValidationAlphaNum" required="required" placeholder="Flight Number" id="flightNumber">	
													 
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="NetRate" class=" control-label">Origin </label>
												<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
												<input type="text" id="origin" name="flightOrderTripDetailList[0].originName" class="form-control input-sm origin ui-autocomplete-input" required="required" placeholder="Origin Name" value="" autocomplete="off">
													 
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="NetRate" class=" control-label">Destination
												</label> <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
												<input type="text" name="flightOrderTripDetailList[0].destinationName" class="form-control input-sm destination ui-autocomplete-input" id="destination" autocomplete="off" required="required" placeholder="Destination Name" value="">
													 
											</div>
										</div>
										
										<input type="hidden" id="totalNumberOfPassenger" value="2">
										<div class="col-md-2">
											<div class="form-group">
												<label for="NetRate" class=" control-label">Departure Date </label>
												<input type="text" id="departureDate" name="flightOrderTripDetailList[0].depDate" class="form-control input-sm dep datepicker_start" autocomplete="off"  required="required" placeholder="ex : dd-mm-yyyy" value="" readonly="readonly">
													 
											</div>
										</div>

										<div class="col-md-2">
											<div class="form-group sm-4 has-feedback">
												<label for="NetRate" class=" control-label">Departure Time(24 hrs) </label> 
												<input type="text" id="departureTime" name="flightOrderTripDetailList[0].depTime" class="form-control input-sm datetimepicker_start" autocomplete="off"  required="required" onchange="checkTimeFormat(this)" placeholder="ex - HH:mm">
											</div>
										</div>
										
										<div class="col-md-2">
											<div class="form-group">
												<label for="NetRate" class=" control-label">Arrival Date </label> 
													<input type="text" id="arrivalDate" name="flightOrderTripDetailList[0].arrDate" class="form-control input-sm arr datepicker_start" autocomplete="off" required="required" placeholder="ex : dd-mm-yyyy" value="" readonly="readonly">
													 
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="NetRate" class=" control-label">Arrival Time(24 hrs)</label> 
												<input type="text" id="arrivalTime" name="flightOrderTripDetailList[0].arrTime" class="form-control input-sm datetimepicker_start" autocomplete="off" required="required" placeholder="ex - HH:mm" autocomplete="off">
													 
											</div>
										</div>
									</div>
									<div id="add-trips"></div>
									<div class="col-sm-12 " style="margin-top: 20px;">
										<div class="row">
											<a class="btn btn-primary btn-sm" id="add_trip">Add Trip</a>
											<a class="btn btn-primary btn-sm" id="remove_trip">Remove Trip</a>
										</div>

									</div>
								</div>
                                    </div>
                                </div>
                                </div>
                                </div> 
    </section>
    <section class="supplier_amount_payable">
                                <div class="col-md-12">
								 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse04" aria-expanded="false" aria-controls="collapseFour">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingFour">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Supplier Amount Payable</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse04" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="">
                                        <div class="panel-body">
 						
									<div class="col-sm-2">
													<div class="form-group">
								<label for="Vendor" class=" control-label">Supplier </label>	
								 
								<select class="form-control input-sm" name="providerAPI" id="supplierName" required="required">
									<option value="" selected="selected">select Supplier</option>
								
									<option value="Desia">Desia</option>
								
									<option value="TBO">TBO</option>
								
								 </select>
								</div>
								</div>
									<div class="col-sm-2">
									<div class="form-group">
										<label for="currency" class=" control-label">PNR
										  </label>
										 
											<input type="text" class="form-control input-sm" required="required" placeholder="PNR" name="pnr" id="pnr">
												  
										</div>
									</div>
									<div class="col-sm-2">
									<div class="form-group">
										<label for="currency" class=" control-label">GDS-PNR
										  </label>
										 
											<input type="text" class="form-control input-sm" required="required" placeholder="GDS-PNR" name="gdsPnr" id="gdsPnr">
												  
										</div>
									</div>
									
										 
									<div class="col-sm-2">
									<input type="hidden" class="form-control input-sm" required="required" placeholder="Confirmation Number" name="orderId" id="orderId" value="TAYA786">
									<div class="form-group">
										<label for="currency" class="  control-label">Supplier
											Currency </label>
										 
											<select class="form-control input-sm" required="required" name="apiCurrency">
									
												

											</select>
											
										</div>
									</div>
									 
									<input type="hidden" class="form-control input-sm" required="required" placeholder="Conversion Rate" value="1.0" name="apiToBaseExchangeRate">
									<div class="col-sm-2">
										<div class="form-group">
											<label for="flightOrderRow.managementFee" class=" control-label">Management Fees
											</label> 
										 
											<input type="text" autocomplete="off" name="flightOrderRow.tempManagementFees" id="managementFees" value="0.00" class="form-control input-sm" required="required" placeholder="management Fees" readonly="readonly">
													</div>

											</div>
													
													
											<div class="">
													<div class="col-sm-2">
													<div class="form-group has-feedback ">
													<label for="currency" class="control-label">Supplier PaymentType </label>
													
														<select class="form-control input-sm" id="supplierPaymentType" name="supplierPaymentType">
															<option value="Full">Full Payment</option>
															<option value="Partial">Partial Payment</option>
															<option selected="selected" value="Zero">No Payment</option>
														</select>
														
													</div> 
													</div>
									
											<div class="col-sm-2" style="margin-top: 25px;">
											  <a class="btn btn-info btn-xs" data-toggle="modal" href="#suppliermyNotification"><i class="fa fa-bell-o fa-sm"></i> Reminder</a>
											</div>
											
											
											
											
											
											<div class="col-sm-2 supplierPayment" style="display: none">							
									
									<div class="form-group has-feedback ">
										<label for="currency" class=" control-label">Amount</label>
										 
											<input type="text" class="form-control input-sm" placeholder="amount" id="supplierAmount" name="paymentTransaction.apiProviderAmount">
												
										</div>

									</div>
									<div class="col-sm-2 supplierPayment" style="display: none">
									<div class="form-group">
										<label for="currency" class="col-sm-2 control-label">PaidBy</label> 
											<input type="text" class="form-control input-sm" placeholder="Paid By" name="paymentTransaction.paidBySupplier">
												
										</div>

									</div>

									<!-- <div class="form-group card-details">
										<label for="currency" class="col-sm-2 control-label">CARD
											DETAILS </label>
									</div>
 -->							<div class="col-sm-2 no-payment-mode" style="display: none;">
									<div class="form-group has-feedback ">
										<label for="currency" class=" control-label">Payment
											Mode </label>
									 
											<select class="form-control input-sm" id="supplierPaymentMethod" name="apiProviderPaymentTransaction.payment_method" required="required">
												<option value="card">CARD</option>
												<option value="cash">CASH</option>
												<option value="online">ONLINE</option>

											</select>
											
										</div>
									</div>

								<div class="col-sm-2 supplier-comments" style="display: none">
									<div class="form-group">
										<label for="currency" class="col-sm-2 control-label">Comments
										</label> 
											<textarea rows="1" class="form-control input-sm" placeholder="Type text" name="apiProviderPaymentTransaction.response_message"></textarea>
												
										</div>
									</div>

								<div class="col-sm-2 card-details" style="display: none;">

									<div class="form-group">
										<label for="currency" class=" control-label">CardHolderName
										</label> 
											<select class="form-control input-sm" name="paymentTransaction.supplierCardHolderId">
												<option value="">Select card holder name</option>
												



											</select>
											
										</div>
									</div>
													</div>		
								</div>
                                    </div>
                                </div>
                                </div>
                                </div> 
   </section>
    <section class="client_payment_receivables">
                                <div class="col-md-12">
							 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
							                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse05" aria-expanded="false" aria-controls="collapseFive">
							                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingFive">
							                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Client Payment Receivables</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse05" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive" aria-expanded="false" style="">
                                        <div class="panel-body">
								<div class="col-sm-2">
								<div class="form-group">
												<!-- <label for="NetRate" class="control-label"
													style="color: red; font-size: 12px">Ticket Price </label> -->
												 
												 <label for="totalGstTax" style="color: red;">Ticket Price </label> 
												 
													<input type="text" style="color: red; font-size: 18px" disabled="disabled" id="quotedAmount1" value="0.00" class="form-control input-sm">
														
												</div>
											</div>
											<div class="col-sm-2"> 
											<input type="hidden" id="taxType" value="GST">
											<input type="hidden" autocomplete="off" name="totalGstTax" onchange="numbersonly(this)" class="form-control input-sm" id="gstTax" value="0.0">
											<div class="form-group">
												<label for="totalGstTaxAmount">Gst Tax Amount </label> 
													<div class="inner-addon right-addon">
      												<span class="glyphicon">@ <span id="gstTaxPer"></span>%</span>
      												<input type="text" autocomplete="off" name="totalGstTaxAmount" onchange="numbersonly(this)" class="form-control input-sm" id="gstTaxAmount" value="0.0" placeholder="Enter Gst Tax " readonly="readonly"> 
    												</div>
													
											</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="totInvoiceAmount">Invoice Amount </label> 
												<input type="text" autocomplete="off" onclick="numbersonly(this);addtax();" name="totInvoiceAmount" id="invoiceamount" class="form-control input-sm" placeholder="Invoice Amount" value="0" readonly="readonly">
											</div>
											</div>
										<div class="col-sm-2">
											<div class="form-group">
											<label for="currency">Booking Currency </label>
												<select class="form-control input-sm" required="required" name="bookingCurrency">
												</select>
												
											</div>
									</div>
									<div class="col-sm-2">	
									<div class="form-group">
										<label for="currency">INR To Booking </label>
											<input type="text" class="form-control input-sm" value="1.0" required="required" placeholder="Conversion Rate" name="baseToBookingExchangeRate">
										</div>
									</div>
								 <div class="col-sm-2">
									<div class="form-group">
										<label for="NetRate">Payment Status </label>
											<select class="form-control input-sm" required="required" name="paymentStatus" id="paymentStatus">
												<option value="Paid">Success</option>
												<option value="Pending">Pending</option>
											</select>
										</div>
									</div>
									<div class="">
									<div class="col-sm-2 ">	
									<div class="form-group has-feedback ">
										<label for="discount">Discount</label>
											<input type="text" class="form-control input-sm " value="0" required="required" placeholder="Enter the discount amount" id="discount" name="discount" readonly="readonly">
										</div>
									</div>
									
									<div class="col-sm-2">
									<div class="form-group">
										<label for="NetRate" class=" control-label">Booking Status </label>
											<select class="form-control input-sm" required="required" name="statusAction">
												<option value="Confirmed">Confirmed</option>
												<option value="Pending">Pending</option>
												<option value="Reserved">Reserved</option>
												<option value="Failed">Failed</option>
											</select>
											
										</div>
									</div>
									<div class="col-sm-2"> 
									<div class="form-group">
										<label for="currency" class=" control-label">Customer PaymentType </label>
											<select class="form-control input-sm" name="customerPaymentType" id="clientPaymentType">
												<option value="Full">Full Payment</option>
												<option value="Partial">Partial Payment</option>
												<option selected="selected" value="Zero">No Payment</option>
											</select>
										</div>
									</div>
									<div class="col-sm-1 " style="margin-top: 25px;">
													 <a class="btn btn-info btn-xs" data-toggle="modal" href="#clientmyNotification"><i class="fa fa-bell-o fa-sm"></i>Reminder</a>
													</div>
													
										<div class="col-sm-2 clientPayment" style="display: none;">  
							 			<div class="form-group">
											<label for="currency" class="  control-label">Amount</label>
											<input type="text" class="form-control input-sm" placeholder="amount" name="paymentTransaction.clientAmount">
										</div>
									</div>
								<div class="col-sm-2  clientPayment" style="display: none;"> 
									<div class="form-group">
										<label for="currency" class=" control-label">Paid By</label>
											<input type="text" class="form-control input-sm" placeholder="Paid By" name="paymentTransaction.paidByClient">
										</div>
									</div> 
									<div class="col-sm-2 no-client-payment-mode" style="display: none;"> 
									<div class="form-group">
										<label for="currency" class=" control-label">Payment Mode </label>
											<select class="form-control input-sm" id="clientPaymentMethod" name="paymentTransaction.payment_method" required="required">
												<option value="card">CARD</option>
												<option value="cash">CASH</option>
												<option value="online">ONLINE</option>
											</select>
										</div>
									</div>
									<div class="col-sm-2 client-comments" style="display: none"> 
									<div class="form-group has-feedback ">
										<label for="currency" class=" control-label">Comments
										</label>
											<textarea rows="1" class="form-control input-sm" placeholder="Type text" name="paymentTransaction.response_message"></textarea>
										</div>
									</div>

								<div class="col-sm-2 client-card-details" style="display: none;"> 
									<div class="form-group">
										<label for="currency" class=" control-label">CardHolderName
										</label> 
											<select class="form-control input-sm" name="paymentTransaction.clientCardHolderId">
												<option value="">Select card holder name</option>
											</select>
											
										</div>
									</div>
									</div>
								</div>
                                    </div>
                                </div>
                                </div>
                                </div> 
                                </section>
                                
                                <div class="row">
									<div class="col-md-12">
									<div class="set pull-right" style="margin-bottom: 20px;margin-top: 7px;">
										<div class="form-actions">
											<input type="hidden" name="leadId" value="${param.companyLeadId}">
											<button class="btn btn-default btn-danger" id="tourCancelBtn" type="reset">Reset</button>
											<button type="submit" class="btn btn-primary user-save-btn" id="tourEditBtn">Submit</button>
										</div>
										</div>	
										</div>
								</div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
            
            
  <div id="suppliermyNotification" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close slds-modal__close" data-dismiss="modal">
		<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard"></button>
        <h4 class="modal-title">Supplier Payment Notification Alert !</h4>
      </div>
      <div class="modal-body">
       <span style="color:red;font-size:11px;display:block"> (* Please separate emails with semicolon) </span>
     <div class="row">
     <div class="col-sm-12"> 
      <div class="col-sm-12  "> 
      <div class="form-group">
  <label for="comment">To Mail(s):</label> 
  <textarea class="form-control" rows="1" id="supplierToEmails"></textarea>
</div>
</div>
     
     <div class="col-sm-12"> 
      <div class="form-group">
  <label for="comment">CC Mail(s):</label>
  <textarea class="form-control" rows="1" id="supplierCcEmails"></textarea>
</div>
</div>
  
     <div class="col-sm-6 fd"> 
      <div class="form-group">
      <label for="currency" class=" control-label"> 
       From Date </label>
        <input type="text" class="form-control input-sm hasDatepicker" value="" required="required" id="supplierFromDate" placeholder="FromDate">
       </div>
      </div>
      <div class="col-sm-6"> 
      <div class="form-group">
      <label for="currency" class=" control-label"> 
       To Date <font size="1" color="red">(Selected time is preferred)</font></label>
        
        <input type="text" class="form-control input-sm hasDatepicker" value="" required="required" id="supplierToDate" name="" placeholder="date with preferable time">
       </div>
      </div>
      <div class="col-sm-6 fd"> 
      <div class="form-group">
      <label for="currency" class=" control-label"> 
       Due Date </label>
        
        <input type="text" class="form-control input-sm hasDatepicker" value="" required="required" id="supplierDueDate" name="" placeholder="FromDate">
       </div>
      </div>
      <div class="col-sm-6"> 
      <div class="form-group">
      <label for="currency" class=" control-label"> 
       Paid Date </label>
        
        <input type="text" class="form-control input-sm hasDatepicker" value="" required="required" id="supplierPaidDate" name="" placeholder="FromDate">
       </div>
      </div>
      <div class="col-sm-12"> 
      <div class="form-group">
  <label for="comment">Comment:</label>
  <textarea class="form-control" rows="1" id="supplierComment"></textarea>
</div>
</div>
     </div>
     </div>
      </div>
      <div class="modal-footer">
  <div class="col-sm-12">
  <button type="button" id="paymentNotificationAlert" onclick="sendSupplierAndClientNotification('supplier');" class="btn btn-primary btn-sm">Set Supplier Reminder</button>
       
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
  
  </div>
       
      </div>
    </div>

  </div>
</div>
            
  <div id="clientmyNotification" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
									</button>
        <h4 class="modal-title">Client Payment Notification Alert !</h4>
      </div>
     
      <div class="modal-body">
       <span style="color:red;font-size:11px">(* Please separate mails with semicolon) </span>
     <div class="row">
     <div class="col-sm-12"> 
   
      <div class="col-sm-12 "> 
      <div class="form-group">
  <label for="comment">To Mail(s):</label> 
  <textarea class="form-control" rows="1" id="clientToEmails"></textarea>
</div>
</div>
     
<div class="col-sm-12"> 
 <div class="form-group">
  <label for="comment">CC Mail(s):</label>
  <textarea class="form-control" rows="1" id="clientCcEmails"></textarea>
</div>
</div>
  
     <div class="col-sm-6 fd"> 
      <div class="form-group">
      <label for="currency" class=" control-label"> 
       From Date </label>
        <input type="text" class="form-control input-sm hasDatepicker" value="" required="required" id="clientFromDate" placeholder="FromDate">
       </div>
      </div>
      <div class="col-sm-6"> 
      <div class="form-group">
      <label for="currency" class=" control-label"> 
       To Date <font size="1" color="red">(Selected time is preferred)</font></label>
        
        <input type="text" class="form-control input-sm hasDatepicker" value="" required="required" id="clientToDate" name="" placeholder="date with preferable time">
       </div>
      </div>
      <div class="col-sm-6 fd"> 
      <div class="form-group">
      <label for="currency" class=" control-label"> 
       Due Date </label>
        
        <input type="text" class="form-control input-sm hasDatepicker" value="" required="required" id="clientDueDate" name="" placeholder="FromDate">
       </div>
      </div>
      <div class="col-sm-6"> 
      <div class="form-group">
      <label for="currency" class=" control-label"> 
       Paid Date </label>
        
        <input type="text" class="form-control input-sm hasDatepicker" value="" required="required" id="clientPaidDate" name="" placeholder="FromDate">
       </div>
      </div>
      <div class="col-sm-12"> 
      <div class="form-group">
  <label for="comment">Comment:</label>
  <textarea class="form-control" rows="1" id="clientComment"></textarea>
</div>
</div>
     </div>
     </div>
      </div>
      <div class="modal-footer">
      <div class="col-sm-12">
       <button type="button" id="paymentNotificationAlert" onclick="sendSupplierAndClientNotification('client');" class="btn btn-primary btn-sm">Set Client Reminder</button>
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>

  </div>
</div>
        
<script>    
$(function () {
    $('#bookingDate').datetimepicker({
        format: 'MM/DD/YYYY',  
        minDate:new Date(),
        ignoreReadonly : true
    });
    $('.datepicker_start').datetimepicker({
        format: 'MM/DD/YYYY',  
        minDate:new Date(),
        ignoreReadonly : true
    });
    $('.datetimepicker_start').datetimepicker({
    	format: 'LT', 
    	keepOpen:true,
    });
});
$(function () {
    var date = new Date();
    var currentMonth = date.getMonth();
    var currentDate = date.getDate();
    var currentYear = date.getFullYear();
    $('#dateOfBirth').datetimepicker({
        format: 'MM/DD/YYYY',  
        maxDate: new Date(currentYear, currentMonth, currentDate)
    });
});


/*-------------------------------------------------------------------------------------------------*/
$("#saveFlightTripForm").submit(function(e) {
			e.preventDefault();
			$.ajax({
				url : "updateTravelSalesLeadFollowUpComment",
				type : "POST",
				dataType : 'json',
				data : $("#saveFlightTripForm").serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						$("#" + modalId).hide();
						alertify.success(jsonData.message.message);}
	  				else if(jsonData.message.status == 'error'){
	  					$("#" + modalId).hide();
	  					alertify.error(jsonData.message.message);}
					setTimeout(location.reload.bind(location), 1000);
				},
				error : function(request, status, error) {
					alertify.error("follow up call can't be updated,please try again");
				}
			});
		});

</script>    
        
