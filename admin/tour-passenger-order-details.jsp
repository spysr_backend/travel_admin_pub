<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
 <style>
 p {
    margin: 0 0 0px;
}
.pnl1 {
    margin-top: 0px;
    border: 1px solid #eaeaea;
    background: whitesmoke;
    padding: 0px 0px 1px 10px;
    border-radius: 3px;
}
.panel-body {
    background: #eaeaea;
}
.item-list {
    padding: 10px;
}
 </style>
<title><s:property value="%{#session.ReportData.agencyUsername}" /></title>
		
		 <s:if test="hasActionErrors()">
						<div class="succfully-updated clearfix" id="error-alert">

							<div class="col-sm-2">
								<i class="fa fa-check fa-3x"></i>
							</div>

							<div class="col-sm-10">

								<p>
									<s:actionerror />
								</p>

								<button type="button" id="cancel" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

							</div>

						</div>


					</s:if>

					<s:if test="hasActionMessages()">
						<div class="sccuss-full-updated" id="success-alert">
							<div class="succfully-updated clearfix">

								<div class="col-sm-2">
									<i class="fa fa-check fa-3x"></i>
								</div>

								<div class="col-sm-10">
									<s:actionmessage />
									<button type="button" id="success" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

								</div>

							</div>
						</div>
					</s:if>
		
		


 <section class="wrapper container-fluid">
                <div class="row">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.tour_passenger_order_details" /></h5>
                                 <div class="set pull-right">
                                   
								 <a class="btn btn-success btn-xs" href="tourOrderList"><span
								class="pull-right">
									<span class="glyphicon glyphicon-step-backward"></span><s:text name="tgi.label.back" /></span></a>
			
                                     </div></div>


		<!-- Main content -->
		<section class="col-md-12">
			<!-- Small boxes (Stat box) -->
			<div class="row" >
				<div class="clearfix report-search" >
				 <s:if test="ReportData.isCreditNoteIssued()">
				<div class="in-head" style="border:1px solid black ;color:red">
 							<h4 class="text-center" >
									<span><s:text name="tgi.label.credit_note_issued_by" /><s:property
											value="ReportData.staff" /></span>
											<span><s:text name="tgi.label.credit_note_issued_by" /><s:property
											value="ReportData.staff"/></span>
								</h4>
							</div>
			 </s:if>
				
				
				<div class="row">
				<div class="profile-timeline-card"> 
				<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-globe" aria-hidden="true"></i></span> <span> <b class="blue-grey-text">Tour Info</b></span>
									</p>
								<div class=" no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
                                   <th><s:text name="tgi.label.created_by" /></th>
                                   <th><s:text name="tgi.label.pnr" /></th>
                                   <th><s:text name="Booking Date" /></th>
                                   <th><s:text name="tgi.label.status" /></th>
                                   <th><s:text name="tgi.label.supplier" /></th>
									</tr></thead>
									<tr>
									<td data-title="<s:text name="tgi.label.created_by" />"> ${spysr:formatDate(tourOrderRow.createdDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM/d/yyyy hh:mm a')}</td>
									<td data-title="<s:text name="tgi.label.pnr" />"><p style="color: #ff3100;">${tourOrderRow.apiProvoderConfirmationNumber}</p></td>
									<td data-title="<s:text name="Booking Date" />">
									<c:if test="${tourOrderRow != null && tourOrderRow.bookingDate != null}">
									${spysr:formatDate(tourOrderRow.bookingDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy')}
									</c:if>
									</td>
									<td data-title="<s:text name="tgi.label.status" />">${tourOrderRow.bookingStatus}</td>
									<td data-title="<s:text name="tgi.label.supplier" />">${tourOrderRow.bookingSource}</td>
									</tr>	
										</table>
								</div>
								</div> 
								</div>
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-location-arrow"></i></span> <span> <b class="blue-grey-text">Tour Departure Details</b></span>
									</p> 
									<div class="no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
									<th><s:text name="tgi.label.start_date" /></th>
									<th><s:text name="tgi.label.end_date" /></th>
									<th><s:text name="tgi.label.destiation_city" /></th>
									<th><s:text name="tgi.label.destination_country" /></th>
								</tr></thead>
									<tr>
									<td data-title="<s:text name="tgi.label.start_date" />">
									<c:if test="${tourOrderRow != null && tourOrderRow.tourStartDate != null}">
									${spysr:formatDate(tourOrderRow.tourStartDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM/d/yyyy hh:mm a')}
									</c:if>
									</td>
										<td data-title="<s:text name="tgi.label.start_end" />">
										<c:if test="${tourOrderRow != null && tourOrderRow.tourEndDate != null}">
										${spysr:formatDate(tourOrderRow.tourEndDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM/d/yyyy hh:mm a')}
										</c:if>
										</td>
								<%-- 		<td data-title="<s:text name="tgi.label.start_date" />">${tourOrderRow.tourStartDate}</td>
										<td data-title="<s:text name="tgi.label.end_date" />">${tourOrderRow.tourEndDate}</td> --%>
										<td data-title="<s:text name="tgi.label.destiation_city" />">${tourOrderRow.origin}</td>
										<td data-title="<s:text name="tgi.label.destination_country" />">${tourOrderRow.destination}</td>
									</tr></table></div>
									</div>
									</div>
									 
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-user"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.passenger_info" /></b></span>
									</p>  
								<div class="no-table-css clearfix"> 
								<input type="hidden" name="orderId" id="orderId" value="${id}">
									<input type="hidden" name="totalRoomGuest" id="totalRoomGuest" value="${tourOrderRow.tourOrderRowRoomInfos.size()}">
									<input type="hidden" name="totalRoom" id="totalRoom" value="${tourOrderRow.tourOrderRowRoomInfos.size()}">
								 <table class="table table-bordered table-striped-column table-hover mb-0" id="guestTabel">
                                   <thead>
                                   <tr class="border-radius border-color">
                                   <th><s:text name="S.No" /></th>
									<th><s:text name="tgi.label.title" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="D.O.B" /></th>
									<th><s:text name="Paxtype" /></th>
									<th>Action</th>
								</tr></thead>
                                   
								<tbody>
								<c:forEach items="${tourOrderRow.tourOrderRowRoomInfos}"	var="roomInfo" varStatus="loop">
										<tr id="roomDataRow${roomInfo.id}" class="roomDataRow">
													<td data-title="<s:text name="tgi.label.room" /> ${loop.count}"><strong><s:text name="tgi.label.room" /> ${loop.count}
													</strong>
													<c:if test="${roomInfo.referenceCode != null && roomInfo.referenceCode != ''}">
													<td><strong>Reference Code : ${roomInfo.referenceCode}</strong></td> 
													</c:if>
													<c:if test="${roomInfo.status != null && roomInfo.status != ''}">
													<td><strong>Status :  ${roomInfo.status}</strong></td>
													</c:if>
													<c:set var="roomId" value="${roomInfo.id}" />
													
													<input type="hidden" id="deleteRoomId${roomId}" value="${roomId}" />
													</td>
										</tr>
										<input type="hidden" id="totalGuestHotelPerRoom${loop.count}" value="${roomInfo.tourOrderRowGuests.size()}" />
										<c:forEach items="${roomInfo.tourOrderRowGuests}"
														var="guestInfo" varStatus="loopInner">
										<tr id="roomGuestDataRow${roomInfo.id}${guestInfo.id}">
													<td data-title="<s:text name="tgi.label.guest" />" id="newGuest${roomInfo.id}${guestInfo.id}">
													<input type="text" class="form-control" value="<s:text name='tgi.label.guest' /> ${loopInner.count}" disabled="disabled" />
													<%-- <s:text name="tgi.label.guest" />
														 ${loopInner.count} --%>
														 <c:set var="roomGuestId" value="${guestInfo.id}" />
														 </td>	
													<td data-title="<s:text name="tgi.label.gender" />">
													<select name="title" id="title${roomId}${roomGuestId}" class='form-control' disabled="disabled">
													<option>Title</option>
													<option value ="Mr" ${guestInfo.title == 'Mr' ?'selected' :''}>Mr</option>
													<option value ="Mrs" ${guestInfo.title == 'Mrs' ?'selected' :''}>Mrs</option>
													<option value ="Miss" ${guestInfo.title == 'Miss' ?'selected' :''}>Miss</option></select>
													<%-- ${guestInfo.title} --%>
													</td>
													<td data-title="<s:text name="tgi.label.name" />">
													<input type="text" name="firstName" id="firstName${roomId}${roomGuestId}" value="${guestInfo.firstName}" class="form-control" placeholder="name" readonly="readonly" />
													</td>
													<td data-title="<s:text name="tgi.label.surname" />">
													<input type="text" name="lastName" id="lastName${roomId}${roomGuestId}" value="${guestInfo.lastName}" class="form-control" placeholder="Surname" readonly="readonly" />
													</td>
													<td data-title="<s:text name="D.O.B" />">
													<input type="text" name="dateOfBirth" id="dateOfBirth${roomId}${roomGuestId}" class="form-control dob${roomId}${roomGuestId}" value="${guestInfo.birthDate}" placeholder="DOB" readonly="readonly" />
													</td>
													<td data-title="<s:text name="Paxtype" />">
													<select name="paxType" id="paxType${roomId}${roomGuestId}" class='form-control' disabled="disabled">
													<option>Pax Type</option>
													<option value="ADT" ${guestInfo.paxType == 'adult' ?'selected' :''}>ADT</option>
													<option value="CHD" ${guestInfo.paxType == 'child' ?'selected' :''}>CHD</option></select>
													</td>
													<td>
														<input type="hidden" id="roomAndGuestId${roomId}${roomGuestId}" value="${roomId}_${roomGuestId}" />
													  <div id="updateBtnDiv${roomId}${roomGuestId}" style="display:none;">
													  <button onclick="updateRoomGuest('${roomId}','${roomGuestId}')" class="btn btn-success btn-sm">
											        Update
											        </button></div>
													  <div id="editBtnDiv${roomId}${roomGuestId}">
													   <button onclick="editRoomGuest('${roomId}','${roomGuestId}')">
											          <span class="glyphicon glyphicon-edit"></span>
											        </button>
												     <button onclick="deleteRoomGuest('${roomId}','${roomGuestId}')">
											         <span class="glyphicon glyphicon-trash"></span>
											       </button>
											       </div>
													</td>
										</tr>
										</c:forEach>
										<tr id="guestBtnRow${roomInfo.id}" class="guestBtnRow">
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>
										<input <c:if test='${roomInfo.tourOrderRowGuests.size() == 4}'> style="display:none;" </c:if> type="button" value="Add Guest" id="addRoomGuestBtn${roomInfo.id}" onclick="showGuestForm('${roomInfo.id}','${roomGuestId + 1}');" class="btn btn-info btn-xs" />
										<%-- <c:choose>
										<c:when test="${roomInfo.tourOrderRowGuests.size() < 4}">
										<input type="button" value="Add Guest" id="addRoomGuestBtn${roomInfo.id}" onclick="showGuestForm('${roomInfo.id}','${roomGuestId + 1}');" class="btn btn-info btn-xs" />
										</c:when>
										<c:otherwise>
										<input type="button" value="Add Guest" id="addRoomGuestBtn${roomInfo.id}" onclick="showGuestForm('${roomInfo.id}','${roomGuestId + 1}');" class="btn btn-info btn-xs" style="display:none;" />
										</c:otherwise>
										</c:choose> --%>
										</td>
										</tr>
							</c:forEach>
										<tr	id="roomRow">
										<td><input type="button" id="addRoomBtn" value="Add Room" onclick="addNewRoom('${roomId + 1}','${roomGuestId+1}');" class="btn btn-success btn-xs fa-bed" /></td>
										<td><input type="button" id="deleteRoomBtn" value="Delete Room" onclick="deleteRoom('${roomId}','${roomGuestId-1}');" class="btn btn-danger btn-xs" /></td>
										</tr>
							</tbody>
									</table></div>
									</div></div>
								 
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-phone"></i></span> <span> <b class="blue-grey-text">Contact Info</b></span>
									</p>
								<div class="no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
                                   	<th><s:text name="Title" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="tgi.label.mobile" /></th>
									<th><s:text name="Email" /></th>
									<th>DOB</th>
 
								</tr></thead>
									<tr>
										<td data-title="<s:text name="tgi.label.title" />">${tourOrderRow.orderCustomer.title}</td>
										<td data-title="<s:text name="tgi.label.name" />">${tourOrderRow.orderCustomer.firstName}</td>
										<td data-title="<s:text name="tgi.label.surname" />">${tourOrderRow.orderCustomer.lastName}</td>
										<td data-title="<s:text name="Email" />">${tourOrderRow.orderCustomer.mobile}</td>
										<td data-title="<s:text name="Email" />">${tourOrderRow.orderCustomer.email}</td>
										<td data-title="<s:text name="Email" />">${spysr:formatDate(tourOrderRow.orderCustomer.birthDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy')}</td>
									</tr></table></div></div></div>
									
									<div class="row">
								<div class="profile-timeline-card">
								<c:choose>
								<c:when test="${tourOrderRow.tourOrderRowInsurance != null}"> 
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-user-plus"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.flight_insurance" /></b></span>
									</p> 
									<div class="table-responsive no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                  <tr class="border-radius border-color">
										<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
										
									</tr></thead>
									<s:iterator status="index"><tr>
									
										<td data-title="><s:text name="tgi.label.insurance_id" />">${tourOrderRow.tourOrderRowInsurance.insuranceId}</td>
											<td data-title="<s:text name="tgi.label.name" />">${tourOrderRow.tourOrderRowInsurance.name}</td>
											<td data-title="<s:text name="tgi.label.price" />">${tourOrderRow.tourOrderRowInsurance.price}</td>
											<td data-title="<s:text name="tgi.label.description" />">${tourOrderRow.tourOrderRowInsurance.description}</td>
										
									</tr></s:iterator>
								</table></div>
								</c:when>
								<c:otherwise>
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-user-plus"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.flight_insurance" /></b></span>
									</p>  
											<table class="table table-bordered in-table-border no-table-css mb-0">
												 <tr class="border-radius border-color">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.insurance_not_available" /></td>
												</tr>
											</table>
											</c:otherwise> 
								</c:choose>
								</div></div>
								 
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-money"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.payment_info" /> </b></span>
									</p>  
								<div class="no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
									<th><s:text name="tgi.label.order_id" /></th>
									<th><s:text name="tgi.label.amount" /></th>
									<th><s:text name="tgi.label.fee_amount" /></th>
									<th><s:text name="tgi.label.payment_status" /></th>
									<th>Action</th>
								</tr></thead>
									<tr>
										<td data-title="<s:text name="tgi.label.order_id" />"><input type="text" class="form-control" value="${tourOrderRow.orderId}" disabled="disabled"></td>
										<td data-title="<s:text name="tgi.label.amount" />"><input type="text" class="form-control" id="totalPrice" value="${tourOrderRow.totalPrice}" readonly="readonly" /> </td>
										<td data-title="<s:text name="tgi.label.fee_amount" />"><input type="text" class="form-control" id="feeAmount" value="${tourOrderRow.feeAmount}" readonly="readonly"/> </td>
										<td data-title="<s:text name="tgi.label.payment_status" />"><input type="text" class="form-control" value="${tourOrderRow.paymentStatus}" disabled="disabled"/> </td>
										<td><button id="editPaymentInfoBtn" onclick="editPaymentInfo()">
											          <span class="glyphicon glyphicon-edit"></span>
											        </button>
											  <button class="btn btn-success" id="updatePaymentInfoBtn" style="display:none;" onclick="updatePaymentInfo('${tourOrderRow.totalPrice}','${tourOrderRow.feeAmount}')">
											          Update
											        </button>
											  </td>
									</tr></table></div>
									</div></div> 
									
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-globe"></i></span> <span> <b class="blue-grey-text">
										<s:text name="tgi.label.tour_details" /> : </b><span class="text-warning">${tourOrderRow.tourOrderRowItineraryDescription.title}</span> </span>
									</p>   

							<c:if test="${itineraryMap != null && itineraryMap.size()>0}">
								<div class="panel panel-default">
									<div role="button" data-toggle="collapse"
												data-parent="#accordion" href="#collapse2"
												aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse"
												data-parent="#accordion" href="#collapse2"
												aria-expanded="true" aria-controls="collapseOne"> <i
												class="more-less glyphicon glyphicon-plus pull-right"></i> <s:text name="tgi.label.itinerary_description" />
											</a>
										</h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse"
										role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body" style="padding-top: 0px;">
											<c:forEach items="${itineraryMap}" var="itineraryObj">
												<br>
												<li><strong style="color: #c20a2f; font-weight: 700;">${itineraryObj.key}
														day</strong></li>
												<li><b>${itineraryObj.value.title}</b></li>
												<li>${itineraryObj.value.description}</li>
											</c:forEach>
										</div>
									</div>
								</div>
							</c:if>
							<c:if test="${tourOrderRowVo.tourItineraryInclusionList != null && tourOrderRowVo.tourItineraryInclusionList.size() >0}">
								<div class="panel panel-default">
									<div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5"
												aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapseOne"> 
												<i class="more-less glyphicon glyphicon-plus pull-right"></i> Tour Inclusion
											</a>
										</h4>
									</div>
									<div id="collapse5" class="panel-collapse collapse"
										role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="long-description ">
												<c:forEach items="${tourOrderRowVo.tourItineraryInclusionList}" var="inclusionObj" varStatus="j">
													<div class="item-list">
														<span class="" style="font-size:12px"><img class="clippy" src="admin/img/svg/checkedg.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span style="font-size:14px">  ${inclusionObj.description }</span></span>
													</div>
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
							</c:if>
							<c:if test="${tourOrderRowVo.tourItineraryExclusionList != null && tourOrderRowVo.tourItineraryExclusionList.size() >0}">
								<div class="panel panel-default">
									<div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6"
												aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapseOne"> 
												<i class="more-less glyphicon glyphicon-plus pull-right"></i> Tour Exclusion
											</a>
										</h4>
									</div>
									<div id="collapse6" class="panel-collapse collapse"
										role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="long-description ">
												<c:forEach items="${tourOrderRowVo.tourItineraryExclusionList}" var="exclusionObj" varStatus="j">
													<div class="item-list">
														<span class="" style="font-size:12px"><img class="clippy" src="admin/img/svg/close.svg" width="10" alt="Copy to clipboard" style="margin-bottom: 3px;"><span style="font-size:14px">  ${exclusionObj.description }</span></span>
													</div>
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
							</c:if>
							<c:if test="${itineraryItemList != null && itineraryItemList.size() >0}">
								<div class="panel panel-default">
									<div role="button" data-toggle="collapse"
												data-parent="#accordion" href="#collapse3"
												aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse"
												data-parent="#accordion" href="#collapse3"
												aria-expanded="true" aria-controls="collapseOne"> 
												<i class="more-less glyphicon glyphicon-plus pull-right"></i> Tour Items
											</a>
										</h4>
									</div>
									<div id="collapse3" class="panel-collapse collapse"
										role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="long-description ">
												<c:forEach items="${itineraryItemMap}" var="itemTypeObj" varStatus="j">
													<div class="pnl1">
													<h4 style="color: #c20a2f; font-weight:700;font-size:16px;" >${itemTypeObj.key}</h4>
													</div>
													<div class="item-list">
													<c:forEach items="${itemTypeObj.value}" var="item" varStatus="i"> 
														<span class="" style="font-size:12px"><i class="fa fa-hand-o-right" style="font-size:12px;color: #8d1735;"></i><span style="font-size:14px"> ${item.description}</span></span>
													</c:forEach>
													</div>
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
							</c:if>
							<c:if test="${tourOrderRow.tourOrderRowCancellationPolicies != null && tourOrderRow.tourOrderRowCancellationPolicies.size() >0}">
								<div class="panel panel-default">
									<div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4"
												aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapseOne"> 
												<i class="more-less glyphicon glyphicon-plus pull-right"></i> Tour Cancellation Policy
											</a>
										</h4>
									</div>
									<div id="collapse4" class="panel-collapse collapse"
										role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="long-description ">
												<c:forEach items="${tourOrderRow.tourOrderRowCancellationPolicies}" var="cancellationObj" varStatus="j">
													<div class="item-list">
														<span class="" style="font-size:12px"><i class="fa fa-hand-o-right" style="font-size:12px;color: #8d1735;"></i><span style="font-size:14px">  ${cancellationObj.remarks}</span></span>
													</div>
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
							</c:if>
							</div></div> 
							
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-list-alt"></i></span> <span> <b class="blue-grey-text"> Order Summary</b></span>
									</p> 
								<div class="table-responsive no-table-css clearfix mb-0">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
								<th><s:text name="tgi.label.total_passengers" /></th>
								<th><s:text name="tgi.label.base_amount" /></th>
								<th><s:text name="tgi.label.fee_amount" /></th>
								<th><s:text name="tgi.label.insurance_fee" /></th>
								<th><s:text name="tgi.label.total" /></th>								
								</tr></thead>
								<tr>
										<td data-title="<s:text name="tgi.label.total_passengers" />">${tourOrderRow.totalGuest}</td>
										<td data-title="<s:text name="tgi.label.base_amount" />">${tourOrderRow.baseFare}</td>
										<td data-title="<s:text name="tgi.label.fee_amount" />">${tourOrderRow.feeAmount}</td>
										<td data-title="<s:text name="tgi.label.insurance_fee" />">
										<c:choose>
											<c:when test="${tourOrderRow.tourOrderRowInsurance.price != null}">
											${tourOrderRow.tourOrderRowInsurance.price}
											</c:when>
											<c:otherwise>
											<s:text name="tgi.label.n_a" />
											</c:otherwise>
											</c:choose>
										</td>
										<td data-title="<s:text name="tgi.label.total" />">${tourOrderRow.finalPrice} ${tourOrderRow.priceCurrency}</td>
									</tr>
								</table></div></div></div>
			 </div>
</div>
		</section>
	</div></div></div></section>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#editOrder").hide();
			
			$("#orderNow").click(function() {
				$("#editOrder").toggle("slow", function() {

				});
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#twodpd2").datepicker({
				dateFormat : "yy-mm-dd"
			});
			$("#twodpd1").datepicker({
				dateFormat : "yy-mm-dd"
			/*  changeMonth: true,
			 changeYear: true */
			});
		});
	</script>

<script>
function showGuestForm(roomId, roomGuestId)
		{
	var preRoomGuestId = parseInt(roomGuestId) - 1;
	
	var newGuestFormContentRow = "<tr id='dynamicGuestAddFormRow"+roomId+""+roomGuestId+"'>"+
		+"<td></td>"
		+"<td id='newGuest"+roomId+""+roomGuestId+"'>New Guest</td>"
		+"<td><select name='title' id='title"+roomId+""+roomGuestId+"' class='form-control'>"
		+"<option>Mr</option><option>Miss</option><option>Mrs</option></select></td>"+
		+"<td></td>"
		+"<td><input type='text' name='firstName' id='firstName"+roomId+""+roomGuestId+"' class='form-control' placeholder='Name' /> </td>"
		+"<td><input type='text' name='lastName' id='lastName"+roomId+""+roomGuestId+"' class='form-control' placeholder='Surname' /> </td>"
		+"<td><input type='text' name='dateOfBirth' id='dateOfBirth"+roomId+""+roomGuestId+"' class='form-control dob"+roomId+""+roomGuestId+"' placeholder='DOB' /></td>"
		+"<td><select name='paxType' id='paxType"+roomId+""+roomGuestId+"' class='form-control'>"
		+"<option>ADT</option><option>CHILD</option><option>SENIOR</option>"
		+"</select></td>"
		+"<td><input type='button' value='Add' class='btn btn-info' onclick='submitNewGuestData("+roomId+","+roomGuestId+");' /> </td>"
		+"</tr>";

	$("#guestTabel > tbody > tr#guestBtnRow"+roomId).before(newGuestFormContentRow); 
	$("#guestTabel > tbody > tr#guestBtnRow"+roomId).hide();
	 
	$(".dob"+roomId+""+roomGuestId).datepicker({
		dateFormat : "mm-dd-yy"
	});	
		}

</script>

	
    		 <script>
    			 function submitNewGuestData(roomId, roomGuestId){
    			var nextRoomId = parseInt(roomId) + 1;
    			var preRoomId = parseInt(roomId) - 1;
    			var nextRoomGuestId = parseInt(roomGuestId) + 1;
    			var preRoomGuestId = parseInt(roomGuestId) - 1;
    			
    			var orderId = $("#orderId").val();
			    var title = $("#title"+roomId+""+roomGuestId).val();
			    var firstName = $("#firstName"+roomId+""+roomGuestId).val();
			    var lastName = $("#lastName"+roomId+""+roomGuestId).val();
			    var dateOfBirth = $("#dateOfBirth"+roomId+""+roomGuestId).val();
			    var paxType = $("#paxType"+roomId+""+roomGuestId).val();
			    var totalRoomGuest = $("#totalRoomGuest").val();
			   
			    
			    var totalRoomGuest = parseInt($("#totalRoomGuest").val()) + 1;
			    $("#totalRoomGuest").val(totalRoomGuest);
			    
			//remove guest add form row
				$("#guestTabel > tbody > tr#dynamicGuestAddFormRow"+roomId+""+roomGuestId).remove();
			 
			// show guest add button
				$("#guestTabel > tbody > tr#guestBtnRow"+roomId).show();
				
			 var newAddGuestBtnRow = "<input type='button' style='display:none;' value='Add Guest' id='addRoomGuestBtn"+roomId+"' onclick='showGuestForm("+roomId+","+nextRoomGuestId+");' class='btn btn-info btn-xs' />";
				 $("#addRoomGuestBtn"+roomId).replaceWith(newAddGuestBtnRow);
				  
				 var replaceNewAddRoomBtn = "<td> <input type='button' id='addRoomBtn' value='Add Room' onclick='addNewRoom("+nextRoomId+","+nextRoomGuestId+");' class='btn btn-success btn-xs' /></td>"
				  +"<td><input type='button' id='deleteRoomBtn' value='Delete Room' onclick='deleteRoom("+preRoomId+","+roomGuestId+");' class='btn btn-danger btn-xs' /></td>";
				$("#roomRow").html(replaceNewAddRoomBtn);	 
			    
            	// show new guest data row
            	var titleSelectOption = getSelectedTitle(title, roomId, roomGuestId);
            	var paxTypeSelectOption = getSelectedPaxType(paxType, roomId, roomGuestId);
            	
            	$(".dob"+roomId+""+roomGuestId).datepicker({
            		dateFormat : "mm-dd-yy"
            	});	
            	
                var Obj_data = {
                	orderId : orderId,
                	roomId : roomId,
				    title : title,
                	firstName : firstName,
					lastName : lastName,
					dateOfBirth : dateOfBirth,
					paxType : paxType,
					totalRoomGuest : totalRoomGuest
                };
                $.ajax({
                    type: "POST",
                    url: '<%=request.getContextPath()%>/addTourOrderGuest.action',
                    data : Obj_data,
                    success: function(response)
                    {  
//                    	alert("response"+response.roomAndGuestId);
                    	var newEditContentRow = "<tr id='roomGuestDataRow"+roomId+""+roomGuestId+"'>"+
        				+"<td></td>"
        				+"<td><input type='text' value='New Guest' class='form-control' disabled=disabled /></td>"
        				+titleSelectOption
        				+"<td><input type='text' name='firstName' id='firstName"+roomId+""+roomGuestId+"' value='"+firstName+"' class='form-control' placeholder='Name' readonly='readonly' /> </td>"
        				+"<td><input type='text' name='lastName' id='lastName"+roomId+""+roomGuestId+"' value='"+lastName+"' class='form-control' placeholder='Surname' readonly='readonly' /> </td>"
        				+"<td><input type='text' name='dateOfBirth' id='dateOfBirth"+roomId+""+roomGuestId+"' value='"+dateOfBirth+"' class='form-control dob' placeholder='DOB' readonly='readonly' />"
        				+"<input type='hidden' id='title"+roomId+""+roomGuestId+"' />"
        				+"<input type='hidden' id='paxType"+roomId+""+roomGuestId+"' />"
        				+"</td>"
        				+paxTypeSelectOption
        				+"<td>"
        				+"<input type='hidden' id='roomAndGuestId"+roomId+""+roomGuestId+"' value='"+response.roomAndGuestId+"' />"
        				+"<div id='updateBtnDiv"+roomId+""+roomGuestId+"' style='display:none;'>"
        				+"<button onclick='updateRoomGuest("+roomId+","+roomGuestId+")' class='btn btn-success'>"
        		        +"Update</button></div>"
        				+"<div id='editBtnDiv"+roomId+""+roomGuestId+"'>"
        				+"<button onclick='editRoomGuest("+roomId+","+roomGuestId+")'>"
        		        +"<span class='glyphicon glyphicon-edit'></span></button>"
        			    +"<button onclick='deleteRoomGuest("+roomId+","+roomGuestId+")'>"
        		        +"<span class='glyphicon glyphicon-trash'></span>"
        		       	+"</button></div></td>"
        				+"</tr>";
        				$("#guestBtnRow"+roomId).before(newEditContentRow);
        				
        				var totalRoom = parseInt($("#totalRoom").val());
        				if(totalRoom == 1)
        				$("#deleteRoomBtn").hide();

        				//alert(response.totalGuestPerRoom);
        				if(response.totalGuestPerRoom < 4)
        					$("#addRoomGuestBtn"+roomId).show();
                    }
                });             
             
            }
    </script>
    
    
    <!-- Add room -->
 				<script>
    			 function addNewRoom(roomId, roomGuestId)
    			 { 
    			var preRoomId = parseInt(roomId) - 1;
    			var nextRoomId = parseInt(roomId) + 1;
    			var nextRoomGuestId = parseInt(roomGuestId) + 1;
    			
    			
    			var orderId = $("#orderId").val();
    			var totalRoom = parseInt($("#totalRoom").val())+1;
    			var newGuestRoom = "<tr id='roomDataRow"+roomId+"'>"
								   +"<td data-title='<s:text name='tgi.label.room' />"
								   +"${loop.count}'><strong><s:text name='tgi.label.room' /> "+totalRoom+" </strong>"
								   +"<input type='hidden' id='deleteRoomId"+roomId+"' value='"+roomId+"' /></td>";
								   +"<td><strong></strong></td>"
								   +"<td><strong></strong></td></tr>";
				
				// save total room no as a temprary storage
				$("#totalRoom").val(totalRoom);
				
				var newRoomAddData = newGuestRoom
									+"<tr id='guestBtnRow"+roomId+"'>"
									+"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
									+"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
									+"<td>"
									+"<input type='button' value='Add Guest' id='addRoomGuestBtn"+roomId+"' onclick='showGuestForm("+roomId+","+roomGuestId+");' class='btn btn-info btn-xs' />"
									+"</td>"
									+"</tr>";
    		$("#guestTabel > tbody > tr#guestBtnRow"+preRoomId).after(newRoomAddData);
            
    		var replaceNewAddRoomBtn = "";
    		if(totalRoom == 4)
    			{
			replaceNewAddRoomBtn = "<td><input type='button' style='display:none;' id='addRoomBtn' value='Add Room' onclick='addNewRoom("+nextRoomId+","+roomGuestId+");' class='btn btn-success btn-xs' /></td>"
									  +"<td><input type='button' id='deleteRoomBtn' value='Delete Room' onclick='deleteRoom("+roomId+","+roomGuestId+");' class='btn btn-danger btn-xs' /></td>";
    			}
    		else
    			{
   			replaceNewAddRoomBtn = "<td><input type='button' id='addRoomBtn' value='Add Room' onclick='addNewRoom("+nextRoomId+","+roomGuestId+");' class='btn btn-success btn-xs' /></td>"
			  							+"<td><input type='button' id='deleteRoomBtn' value='Delete Room' onclick='deleteRoom("+roomId+","+roomGuestId+");' class='btn btn-danger btn-xs' /></td>";	
    			}
			$("#roomRow").html(replaceNewAddRoomBtn);
			
			var Obj_data = {
                	orderId : orderId,
                	totalRoom : totalRoom
                };
                $.ajax({
                    type: "POST",
                    url: '<%=request.getContextPath()%>/addTourOrderRoom.action',
                    data : Obj_data,
                    success: function(response)
                    {  
                    	$("#deleteRoomId"+roomId).val(response.roomId);
                    }
                });             
             
            }
    </script>
    
    
     <script>
    function editRoomGuest(roomId, roomGuestId)
    {
    	// enable room guest edit form
    	disableRoomGuestForm(false, roomId, roomGuestId);
    	
    	 $("#editBtnDiv"+roomId+""+roomGuestId).hide();
    	 $("#updateBtnDiv"+roomId+""+roomGuestId).show();
    	 
    	 $(".dob"+roomId+""+roomGuestId).datepicker({
     		dateFormat : "yy-mm-dd"
     	});	
    }
    
    function disableRoomGuestForm(status, roomId, roomGuestId)
    {
    	 $("#title"+roomId+""+roomGuestId).attr('disabled', status);
    	 $("#firstName"+roomId+""+roomGuestId).attr('readonly', status);
    	 $("#lastName"+roomId+""+roomGuestId).attr('readonly', status);
    	 $("#dateOfBirth"+roomId+""+roomGuestId).attr('readonly', status);
    	 $("#paxType"+roomId+""+roomGuestId).attr('disabled', status);
    }
    </script>
    
     <script>
    function updateRoomGuest(roomId, roomGuestId)
    {
    	var orderId = $("#orderId").val();
    	var title = $("#title"+roomId+""+roomGuestId).val();
	    var firstName = $("#firstName"+roomId+""+roomGuestId).val();
	    var lastName = $("#lastName"+roomId+""+roomGuestId).val();
	    var dateOfBirth = $("#dateOfBirth"+roomId+""+roomGuestId).val();
	    var paxType = $("#paxType"+roomId+""+roomGuestId).val();
	    var roomAndGuestId = $("#roomAndGuestId"+roomId+""+roomGuestId).val();
	    
	    // disable room guest edit form
	    disableRoomGuestForm(true, roomId, roomGuestId);
	    $("#editBtnDiv"+roomId+""+roomGuestId).show();
   	 	$("#updateBtnDiv"+roomId+""+roomGuestId).hide();
   	  
   	 var Obj_data = {
         	orderId : orderId,
         	roomId : roomId,
			title : title,
         	firstName : firstName,
			lastName : lastName,
			dateOfBirth : dateOfBirth,
			paxType : paxType,
			roomAndGuestId : roomAndGuestId
         };       
   	 $.ajax({
         type: "POST",
         url: '<%=request.getContextPath()%>/updateTourOrderGuest.action',
         data : Obj_data,
         success: function(response)
         {  
         	
         }
     	});
    }
    </script>
    
    <script>
    function deleteRoomGuest(roomId, roomGuestId)
    {
    	var roomAndGuestId = $("#roomAndGuestId"+roomId+""+roomGuestId).val();
    	$("#roomGuestDataRow"+roomId+""+roomGuestId).remove(); 
    	var Obj_data = {
    			 roomAndGuestId : roomAndGuestId
    	         };  
    	 $.ajax({
             type: "POST",
             url: '<%=request.getContextPath()%>/deleteTourOrderGuest.action',
             data : Obj_data,
             success: function(response)
             { 
            	// alert(roomId+" = "+response.totalGuestPerRoom);
            	 if(response.totalGuestPerRoom < 4)
 					$("#addRoomGuestBtn"+roomId).show();
             }
         });      
    }
    </script>
    
    <script>
    function deleteRoom(roomId, roomGuestId)
    {
    	var orderId = $("#orderId").val();
    	var deleteRoomId = $("#deleteRoomId"+roomId).val();
    	
    	var Obj_data = {
   			 roomId : deleteRoomId,
   			orderId : orderId
   	         };  
    	 $.ajax({
             type: "POST",
             url: '<%=request.getContextPath()%>/deleteTourOrderRoom.action',
             data : Obj_data,
             success: function(response)
             {  
             	// alert(response.roomId);	
            	
             	 $("#roomDataRow"+roomId).remove();
             	$("#roomGuestDataRow"+roomId+""+roomGuestId).remove();
             	$("#guestBtnRow"+roomId).remove();
             	location.reload();
             	/* var nextRoomId = parseInt(roomId) + 1;
             	
             	var replaceNewAddRoomBtn = "<td><input type='button' value='Add Room' onclick='addNewRoom("+nextRoomId+","+roomGuestId+");' class='btn btn-success btn-xs' /></td>"
				  +"<td><input type='button' value='Delete Room' onclick='deleteRoom("+response.roomId+","+roomGuestId+");' class='btn btn-danger btn-xs' /></td>";
					$("#roomRow").html(replaceNewAddRoomBtn); */
             }
         });      
    }
    </script>
    
    <script>
    function getSelectedTitle(title, roomId, roomGuestId)
    {
    	var titleSelectOption = "";
    	if(title == 'Mr')
    		{
    		titleSelectOption = "<td><select name='title' id='title"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
								+"<option selected='selected'>Mr</option><option>Miss</option><option>Mrs</option></select></td>";
    		}
    	else if(title == 'Mrs')
    		{
    		titleSelectOption = "<td><select name='title' id='title"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
				+"<option>Mr</option><option>Miss</option><option selected='selected'>Mrs</option></select></td>";
    		}
    	else if(title == 'Miss')
    		{
    		titleSelectOption = "<td><select name='title' id='title"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
				+"<option selected='selected'>Mr</option><option selected='selected'>Miss</option><option>Mrs</option></select></td>";
    		}
    	return titleSelectOption;
    }
    
    function getSelectedPaxType(paxType, roomId, roomGuestId)
    {
    	var paxTypeSelectOption = "";
    	if(paxType == 'ADT')
    		{
    		paxTypeSelectOption = "<td><select name='paxType' id='paxType"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
									+"<option selected='selected'>ADT</option><option>CHILD</option><option>SENIOR</option></select></td>";
    		}
    	else if(paxType == 'CHILD')
    		{
    		paxTypeSelectOption = "<td><select name='paxType' id='paxType"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
				+"<option>ADT</option><option selected='selected'>CHILD</option><option>SENIOR</option></select></td>";
    		}
    	else if(paxType == 'SENIOR')
    		{
    		paxTypeSelectOption = "<td><select name='paxType' id='paxType"+roomId+""+roomGuestId+"' class='form-control' disabled='disabled'>"
				+"<option>ADT</option><option>CHILD</option><option selected='selected'>SENIOR</option></select></td>";
    		}
    	return paxTypeSelectOption;
    }
    </script>

    
    <script>
    $(document).ready(function()
    		{
    	var totalRoom = parseInt($("#totalRoom").val());
    	if(totalRoom == 4)
   		{
    		$("#addRoomBtn").hide();
   		}
    	else if(totalRoom < 4)
   		{
    		$("#addRoomBtn").show();
   			if(totalRoom == 1)
   		    $("#deleteRoomBtn").hide();
   			else
			{
			$("#deleteRoomBtn").show();	
			}
   		}
    		});
    </script>
    
    
    <script>
 	function editPaymentInfo()
    {
    	 $("#totalPrice").attr('readonly',false);	
    	 $("#feeAmount").attr('readonly',false);
    	 $("#editPaymentInfoBtn").hide();
    	 $("#updatePaymentInfoBtn").show();
    }
    
    function updatePaymentInfo()
    {
    	var orderId = $("#orderId").val();
    	var totalPrice = $("#totalPrice").val();
   	 	var feeAmount = $("#feeAmount").val();
		
    	$("#totalPrice").attr('readonly',true);	
   	 	$("#feeAmount").attr('readonly',true);
   	 	$("#editPaymentInfoBtn").show();
   	 	$("#updatePaymentInfoBtn").hide();
    	
    	var Obj_data = {
   			orderId : orderId,
   			totalPrice : totalPrice,
   			feeAmount : feeAmount
   	         };  
    	 
    	$.ajax({
             type: "POST",
             url: '<%=request.getContextPath()%>/updateTourPaymentInfo.action',
             data : Obj_data,
             success: function(response)
             {}
         });      
    }
    </script>
    