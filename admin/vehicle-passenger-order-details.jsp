<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%--  <script src= https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js> </script>
    <script src= https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js> </script> --%>
 
<title><s:property value="%{#session.ReportData.agencyUsername}" /></title>
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		
		 <s:if test="hasActionErrors()">
						<div class="succfully-updated clearfix" id="error-alert">

							<div class="col-sm-2">
								<i class="fa fa-check fa-3x"></i>
							</div>

							<div class="col-sm-10">

								<p>
									<s:actionerror />
								</p>

								<button type="button" id="cancel" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

							</div>

						</div>


					</s:if>

					<s:if test="hasActionMessages()">
						<div class="sccuss-full-updated" id="success-alert">
							<div class="succfully-updated clearfix">

								<div class="col-sm-2">
									<i class="fa fa-check fa-3x"></i>
								</div>

								<div class="col-sm-10">
									<s:actionmessage />
									<button type="button" id="success" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

								</div>

							</div>
						</div>
					</s:if>
		
		
		<section class="content-header">
			<h1><s:text name="tgi.label.passenger_order_details" /></h1>
			<ol class="breadcrumb">
				<li><a href="home"><i class="fa fa-dashboard"></i> <s:text name="tgi.label.home" /></a></li>
				<li class="active"><s:text name="tgi.label.dashboard" /></li>
			</ol>
		</section>





		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="col-sm-12">
				<h4>
					<a href="javascript:history.back();"><span class="pull-right"><i
							class="fa fa-angle-left"></i> <s:text name="tgi.label.back" /></span></a>
				</h4>
			</div>
			<div class="row" >
				<div class="col-sm-12 clearfix report-search"  >

					<!-- 		<form class="form-inline" action="" method="post"> -->

					<div class="table-responsive dash-table" style="border: 2px solid #2283E1">

						<!-- testing -->

						<div class="box clearfix" >
							<!-- <div class="box-body"> -->

							<label for="Country"><h4>
									<b><s:property value="user" /></b>
								</h4></label>

							<table class="table" 
								class="table table-striped table-bordered">
								<tr>
									<th colspan="2"><u><s:text name="tgi.label.pnr" /></u>:<s:property
											value="%{#session.ReportData.pnr}" /></th>

									<th colspan="2"><u><s:text name="tgi.label.created_by" /></u>:<s:property
											value="%{#session.ReportData.agencyUsername}" /></th>
								</tr>

								<tr>
									<th colspan="5"><h5>
											<b><u><s:text name="tgi.label.flight_info" /></u></b>
										</h5></th>


								</tr>
								<tr>
									<th><s:text name="tgi.label.s.no" /></th>
									<th><s:text name="tgi.label.airline" /></th>
									<th><s:text name="tgi.label.flight_no" /></th>
									<!-- <th>Airline_PNR</th> -->
									<th><s:text name="tgi.label.route" /></th>
									<!--  <th>Via</th> -->
									<th><s:text name="tgi.label.take_off" /></th>
									<th><s:text name="tgi.label.landing" /></th>
									<th><s:text name="tgi.label.class" /></th>
									<th><s:text name="tgi.label.trip_type" /></th>




								</tr>
								<s:iterator value="#session.flightInfo" status="data">
									<tr>

										<th><s:property value="%{#data.count}" /></th>
										<th><s:property value="%{#session.ReportData.airline}" />
										</th>
										<th><s:property value="flight_number" /></th>
										<%--  <th> <s:property value=" airlinePNR}" />  </th>   --%>
										<th><s:property value="route" /></th>
										<!-- <th>_</th> -->
										<th><s:property value="take_off" /></th>
										<th><s:property value="landing" /></th>
										<th><s:property value="_class" /></th>
										<th><s:property value="tripType" /></th>


									</tr>
								</s:iterator>


								<tr>
									<th colspan="5"><h5>
											<b><u><s:text name="tgi.label.passenger_info" /></u></b>
										</h5></th>
								</tr>

								<tr>
									<!-- 	<th>Passengers</th> -->
									<th><s:text name="tgi.label.s.no" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="tgi.label.gender" /></th>
									<th><s:text name="tgi.label.birth" /></th>
									<!-- <th>Phone</th>	
																		 <th>Mobile</th>	 -->
									<!--  <th>T.C.No</th>
																		   <th>Passport No</th> -->
									<th><s:text name="tgi.label.passport_exp.date" /></th>


									<th><s:text name="tgi.label.tax" /></th>
									<th><s:text name="tgi.label.base_fare" /></th>
									<th><s:text name="tgi.label.total" /></th>


									<!--  <th>Fuel/Other</th>	
																		         <th>Ser.Pri</th> -->
									<th><s:text name="tgi.label.ag_com" /></th>
									<th><s:text name="tgi.label.curr" /></th>
								</tr>
								<s:iterator value="#session.passengerList" status="data">
									<tr>
										<%-- 	<th><s:property value="Passengers" /></th> --%>
										<th><s:property value="%{#data.count}" /></th>
										<th><s:property value="name" /></th>
										<th><s:property value="surname" /></th>
										<th><s:property value="gender" /></th>
										<th><s:property value="birth" /></th>
										<%-- <th><s:property value="phone" /></th>
																			  <th><s:property value="mobile" /></th> --%>

										<th><s:property value="PassportExpDate" /></th>
										<th><s:property value="tax" /></th>
										<th><s:property value="price" /></th>
										<th><s:property value="total" /></th>
										<%--   <th><s:property value="fuel_other" />20.00</th>	 --%>
										<%--  <th><s:property value="servPrice" /></th> --%>
										<th><s:property value="agentCom" /></th>
										<th><s:property value="%{#session.ReportData.curCode}" /></th>
									</tr>
								</s:iterator>

								<tr>
								<tr>
									<th><h5>
											<b><u><s:text name="tgi.label.payment_info" /></u></b>
										</h5></th>


								</tr>
								<tr>
									<th><s:text name="tgi.label.transaction_id" /></th>
									<th><s:text name="tgi.label.amount" /></th>
									<th><s:text name="tgi.label.payment_type" /></th>
									<th><s:text name="tgi.label.res_message" /></th>
									<th><s:text name="tgi.label.payment_status" /></th>
								</tr>
								<s:if test="%{#session.paymentInfo.size()>0}">
							 	<s:iterator value="#session.paymentInfo" status="data">
									<tr>
										<th><s:property value="txId" /></th>
										<th><s:property value="c_dAmnt" /></th>
										<th><s:property value="paymentMethod" /></th>
										<th><s:property value="resMsg" /></th>
										<th><s:property value="paymentStatus" /></th>
									</tr>

								</s:iterator>
								</s:if>
								<s:else>
								<tr>
									<td>
									<s:text name="tgi.label.no_data" />
									</td>	 
									</tr>
								 </s:else>



								<tr>
									<th colspan="7"><h4>
											<b><s:text name="tgi.label.all_passengers" /><s:property
													value="%{#session.ReportData.Passengers}" /></b>
										</h4></th>

									<th align="right" colspan="7"><h4>
											<b><s:text name="tgi.label.total" /><s:property
													value="%{#session.ReportData.finalPrice}" /> <s:property
													value="%{#session.ReportData.curCode}" /></b>
										</h4></th>
								</tr>

								<tr>

									<th colspan="5"><button type="button" id="orderNow"
											class="btn btn-primary"><s:text name="tgi.label.alter_order_now" /></button></th>
								</tr>

 								</table>

 							</div>
 							
 							
						<!-- /.box -->

					</div>
					
 							
							<form action="insertOrderModifiedInfo"  method="post"  >
								<s:if test="hasActionErrors()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionerror />
						</div>
					</s:if>
					<s:if test="hasActionMessages()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionmessage />
						</div>
					</s:if>
							<table id="editOrder" style="border: 2px solid #2283E1"
								class="table table-striped table-bordered">


								<tr>

									<td><h4><s:text name="tgi.label.booking_status" /></h4> <select name="statusAction"
										id="statusAction" autocomplete="off" required>
											<option selected="selected" selected="selected" value="0"><s:text name= "tgi.label.select_booking_status" /></option>
											<option value="booked"><s:text name="tgi.label.booked" /></option>
											<option value="ticketed"><s:text name="tgi.label.ticketed" /></option>
											<option value="cancelled"><s:text name="tgi.label.cancelled" /></option>
											<option value="expired"><s:text name="tgi.label.expired" /></option>
											<option value="reserved"><s:text name="tgi.label.reserved" /></option>
											<option value="confirmed"><s:text name="tgi.label.confirmed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option>
									</select>
									<input type="hidden"  name="orderId"  value="<s:property value="%{#session.ReportData.orderId}" />">
									
									</td>

									<td><h4><s:text name="tgi.label.payment_status" /></h4> <select name="paymentStatus"
										id="paymentStatus"    autocomplete="off" required> 
											<option selected="selected" value="0"><s:text name="tgi.label.select_payment_status" /></option>
											<option value="success" ><s:text name="tgi.label.success" /></option>
											<option value="failed"><s:text name="tgi.label.failed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option>

									</select></td>
									<td>
										<h4><s:text name="tgi.label.user_comments" /></h4> <textarea name="userComments" > <s:property
												value="%{#session.ReportData.userComments}" /></textarea>

									</td>

									<td>
									<%-- 	<h4>Api Comments</h4> <textarea  disabled="disabled"><s:property
												value="%{#session.ReportData.apiComments}" /></textarea>
 --%>
									<input type="hidden" name="apiComments" value="<s:property
												value="%{#session.ReportData.apiComments}" />">
									
									
									
									</td>

									<td><h4><s:text name="tgi.label.action" /></h4>
										<button type="submit" class="btn btn-primary">
                                            <s:text name="tgi.label.order_now" /></button></td>
								</tr>

							</table>
						</form>
 							
					<!-- table-responsive -->
					<!-- </form> -->
				</div>



			</div>
			<!-- /.row -->
			<!-- Main row -->


		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
		<script type="text/javascript">
		$(document).ready(function() {
			$("#orderNow").click(function() {
				$("#editOrder").toggle("slow", function() {

				});
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#twodpd2").datepicker({
				dateFormat : "yy-mm-dd"
			});
			$("#twodpd1").datepicker({
				dateFormat : "yy-mm-dd"
			/*  changeMonth: true,
			 changeYear: true */
			});
		});
	</script>
	<script type="text/javascript">
	/* 	$(document).ready(
				function() {
					var table = $('#example').DataTable({
						lengthChange : false,
						"searching" : false,
						"ordering" : false,
						"paging" : false,
						"info" : false, */
					/* "pagingType" : "full_numbers",
					"lengthMenu" : [ 10, 25, 50, 75, 100, 500 ], */
					/* buttons : [ 'excel', 'pdf', 'print' ] */
				/* 	});

					table.buttons().container().appendTo(
							'#example_wrapper .col-sm-6:eq(0)');

				}); */

		/*  $(function () {
		 	$('#example').DataTable({
		    	 "paging": true,
		         "lengthChange": true,
		        "searching": true,
		        "ordering": true,  
		           "info": true,
		         "autoWidth": false,  
		        "search": {
		      	    "regex": true,
		      	  }, 
		      	 
		      "pagingType": "full_numbers",
		      "lengthMenu": [10, 25, 50, 75, 100, 500 ],
		     
		      
		     });  
		  
		   });   */
	</script>
	
	
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"mailConfigList";
	$('#success').click(function() {
	// window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
	<%-- 
<script type="text/javascript">
   $(function(){
	   var txt = $('.success-alert').text();
	   var protocol=location.protocol;
   	   var host=location.host;
   	   //var url=protocol+"//"+host+"/LintasTravelAdmin/superUserReportList";
	   if(txt.length>0){
    	   alert(txt);
    	   //window.location.assign(url);
    	 
    	   
     }
    });
 </script>
 --%>
	<!-- 
 
  $(document).ready(function() 
    	 { 
    		 $("#twodpd1").datepicker({
    			 dateFormat: "yy-mm-dd"  
    			/*  changeMonth: true,
    			 changeYear: true */
    		 }); 
    		 }); -->

