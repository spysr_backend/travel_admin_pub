<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addFlightDeals";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                    <div class="card1">
                        <div class="pnl">
                         <div class="hd clearfix">
                                <h5 class="pull-left">Add Flight Deals </h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="metatagpage_list"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.meta_dat_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div> 
</div></div>
<div class="container">
					<div class="row">
					<h4 class="text-center p-4 hidden-xs">Add Flight Deals</h4> 
                   <div class="spy-form-horizontal mb-0"> 
					<div class="">
						<div class="">
						<form action="insertFlightDeals.action" method="post">
							
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Deal-Location
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select name="dealLocation" class="form-control">
										<option value=''>Select</option>
										<option value='Fly to Seoul'>
										Fly to Seoul
										</option>
										<option value='Fly  to Beijing'>
										Fly to Beijing
										</option>
										<option value='Fly Shanghai'>
										Fly Shanghai
										</option>
										 <option value='Fly to Manila'>
										Fly to Manila
										<!--</option>
										<option value='Fly to Hochiminh'>
										Fly to Hochiminh
										</option>
										<option value='Fly Hanoi'>
										Fly Hanoi
										</option>
										<option value='Fly Guangzhou'>
										Fly Guangzhou
										</option>
										<option value='Fly Beijing'>
										Fly Beijing
										</option> -->
										</select></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Priority
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="priority" placeholder="Priority" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Origin
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="origin" placeholder="Origin" autocomplete="off"
											required></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Destination
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm"
											name="destination" placeholder="Destination" autocomplete="off"
											required></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Crrier Code
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" name="carrierCode" placeholder="Carrier Code" class="form-control" /></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Trip Type
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select name="tripType" class="form-control">
										<option value="O">One-Way</option>
										<option value="R">Round-Trip</option>
										</select></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Base Fare
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" name="baseFare" placeholder="Base Fare" class="form-control" /></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Tax
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" name="tax" placeholder="Tax" class="form-control" /></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Price
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" name="price" placeholder="Price" class="form-control" /></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Service Charge
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" name="serviceCharge" placeholder="Service Charge" class="form-control" /></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Cash Back
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" name="cashBack" placeholder="Cash Back" class="form-control" /></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Currency Code
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" name="currencycode" placeholder="Currency Code" class="form-control" /></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Classes
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" name="classes" placeholder="Classes" class="form-control" /> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Advance Purchased
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" name="advancePurchased" placeholder="Advance Purchased" class="form-control" /> </div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Flight Application
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" name="flightApplication" placeholder="Flight Application" class="form-control" /> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Minimum Stay
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" name="minimumStay" placeholder="Minimum Stay" class="form-control" /> </div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Maximum Stay
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" name="maximumStay" placeholder="Maximum Stay" class="form-control" /> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Ticketing Date
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" name="ticketingDate" id="ticketingDate" placeholder="Ticketing Date" class="form-control" /> </div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Departure Date From
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" name="departureDateFrom" id="departureDateFrom" placeholder="Departure Date From" class="form-control" /> </div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Departure Date Till
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" name="departureDateTill" id="departureDateTill" placeholder="Departure Date Till" class="form-control" /> </div></div>
											</div>
										</div>
									</div>
									
					</div>	
					
								<div class=" row"> 
									<div class="col-md-6"> 
										<div class="controls">
											<div class=""><div class=""> 
												<input type="submit" class="btn btn-info"> </div> 
											</div>
										</div>
									</div> 
								</div>	
							 
							
							<%-- <table class="table table-form">
								<thead>
									<tr>
										<th></th>
										<th>&nbsp;</th>


									</tr>
								</thead>
								<tbody>
								<tr>

										<td>
											<div class="field-name">
												Deal-Location
											</div>
										</td>
										<td>
										<select name="dealLocation" class="form-control">
										<option value=''>Select</option>
										<option value='Fly to Seoul'>
										Fly to Seoul
										</option>
										<option value='Fly  to Beijing'>
										Fly to Beijing
										</option>
										<option value='Fly Shanghai'>
										Fly Shanghai
										</option>
										 <option value='Fly to Manila'>
										Fly to Manila
										<!--</option>
										<option value='Fly to Hochiminh'>
										Fly to Hochiminh
										</option>
										<option value='Fly Hanoi'>
										Fly Hanoi
										</option>
										<option value='Fly Guangzhou'>
										Fly Guangzhou
										</option>
										<option value='Fly Beijing'>
										Fly Beijing
										</option> -->
										</select>
										</td>
										<td>
											<div class="field-name">
												Priority
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="priority" placeholder="Priority" autocomplete="off"
											required></td>


									</tr>
									<tr>
										<td>
											<div class="field-name">
												Origin
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="origin" placeholder="Origin" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Destination
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="destination" placeholder="Destination" autocomplete="off"
											required></td>


									</tr>

									<tr>
									<td>
											<div class="field-name">
												Crrier Code
											</div>
										</td>
										<td><input type="text" name="carrierCode" placeholder="Carrier Code" class="form-control" /></td>
										<td>
											<div class="field-name">
												Trip Type
											</div>
										</td>
										<td><select name="tripType" class="form-control">
										<option value="O">One-Way</option>
										<option value="R">Round-Trip</option>
										</select> </td>
									</tr>
									<tr>
									<td>
											<div class="field-name">
												Base Fare
											</div>
										</td>
										<td><input type="text" name="baseFare" placeholder="Base Fare" class="form-control" /></td>
										<td>
											<div class="field-name">
												Tax
											</div>
										</td>
										<td><input type="text" name="tax" placeholder="Tax" class="form-control" /></td>
									</tr>
									<tr>
									<td>
											<div class="field-name">
												Price
											</div>
										</td>
										<td><input type="text" name="price" placeholder="Price" class="form-control" /></td>
									<td>
											<div class="field-name">
												Service Charge
											</div>
										</td>
										<td><input type="text" name="serviceCharge" placeholder="ServiceCharge" class="form-control" /></td>
									</tr>	
									<tr>
										<td>
											<div class="field-name">
												Cash Back
											</div>
										</td>
										<td><input type="text" name="cashBack" placeholder="Cash Back" class="form-control" /> </td>
									<td>
											<div class="field-name">
												Currency Code
											</div>
										</td>
										<td><input type="text" name="currency" placeholder="Currency Code" class="form-control" /> </td>
									</tr>
										<tr>
										<td><div class="field-name">
												Classes
											</div>
										</td>
										<td><input type="text" name="classes" placeholder="Classes" class="form-control" /> </td>
										<td>
											<div class="field-name">
												Advance Purchased
											</div>
										</td>
										<td><input type="text" name="advancePurchased" placeholder="Advance Purchased" class="form-control" /> </td>
									</tr>
										<tr>
										<td>
											<div class="field-name">
												Flight Application
											</div>
										</td>
										<td><input type="text" name="flightApplication" placeholder="Flight Application" class="form-control" /> </td>	
										<td>
											<div class="field-name">
												Minimum Stay
											</div>
										</td>
										<td><input type="text" name="minimumStay" placeholder="Minimum Stay" class="form-control" /></td>
									</tr>
									<tr>
										<td>
											<div class="field-name">
												Maximum Stay
											</div>
										</td>
										<td><input type="text" name="maximumStay" placeholder="Maximum Stay" class="form-control" /> </td>
										<td>
											<div class="field-name">
												Ticketing Date
											</div>
										</td>
										<td><input type="text" name="ticketingDate" id="ticketingDate" placeholder="Ticketing Date" class="form-control" /></td>
									</tr>
									
																		<tr>
										<td>
											<div class="field-name">
												Departure Date From
											</div>
										</td>
										<td><input type="text" name="departureDateFrom" id="departureDateFrom" placeholder="Departure Date From" class="form-control" /> </td>
										<td>
											<div class="field-name">
												Departure Date Till
											</div>
										</td>
										<td><input type="text" name="departureDateTill" id="departureDateTill" placeholder="Departure Date Till" class="form-control" /></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td><input type="submit" class="btn btn-info"></td>
									</tr>
								</tbody>
							</table> --%>
						</form>
					</div>
				</div>
</div>
</div></div>
</div></div>
                            </section>        
                                    			<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	 <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_body').text("${message}");
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
						
 <script>
 $(document).ready(function() 
 { 
	 $("#ticketingDate").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 $("#departureDateFrom").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 $("#departureDateTill").datepicker({
		 format: "dd-mm-yyyy" 
	 });
	 }); 
 </script>
						