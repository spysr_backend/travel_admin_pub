<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<!DOCTYPE html >
<html>
<head>
<%--  <script src= https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js> </script>
    <script src= https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js> </script> --%>
<link
	href="admin/css/jquery-ui.css"
	rel="stylesheet" type="text/css" />
 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><s:property value="%{#session.ReportData.agencyUsername}" /></title>
 </head>
<body>
		
		 <s:if test="hasActionErrors()">
						<div class="succfully-updated clearfix" id="error-alert">

							<div class="col-sm-2">
								<i class="fa fa-check fa-3x"></i>
							</div>
							<div class="col-sm-10">
								<p>
									<s:actionerror />
								</p>
								<button type="button" id="cancel" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>
							</div>
						</div>
					</s:if>
					<s:if test="hasActionMessages()">
						<div class="sccuss-full-updated" id="success-alert">
							<div class="succfully-updated clearfix">

								<div class="col-sm-2">
									<i class="fa fa-check fa-3x"></i>
								</div>

								<div class="col-sm-10">
									<s:actionmessage />
									<button type="button" id="success" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

								</div>

							</div>
						</div>
					</s:if>
		
		
	<%-- 	<section class="content-header">
			<h1></h1>
			<ol class="breadcrumb">
				<li><a href="home"><i class="fa fa-dashboard"></i><s:text name="tgi.label.home" /></a></li>
				<li class="active"><s:text name="tgi.label.dashboard" /></li>
			</ol>
		</section> --%>



<section class="wrapper container-fluid">
                <div class="row">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.cars" /> <s:text name="tgi.label.passenger_report_details" /></h5>
                                  <div class="set pull-right">
					<a class="btn btn-success btn-xs" href="carReportList"><span class="pull-right"><span class="glyphicon glyphicon-step-backward"></span> <s:text name="tgi.label.back" /></span></a>
			</div></div>

		<!-- Main content -->
		<section class="col-md-12">
			<!-- Small boxes (Stat box) --> 
			<div class="row" >
				<div class="clearfix report-search" >
				 <s:if test="ReportData.isCreditNoteIssued()">
				<div class="in-head" style="border:1px solid black ;color:red">
 							<h4 class="text-center" >
									<span><s:text name="tgi.label.credit_note_issued_by" /> :<s:property
											value="ReportData.staff" /></span>
											<span><s:text name="tgi.label.credit_note_issued_by" /> :<s:property
											value="ReportData.staff"/></span>
								</h4>
							</div>
			 </s:if>
			 
			 
			 
			 <div class="row">
				<div class="profile-timeline-card"> 
				<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-car" aria-hidden="true"></i></span> <span> <b class="blue-grey-text"></b></span>
									</p>
									
									<div class="table-responsive no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
                                   <th><s:text name="tgi.label.created_by" /></th>
                                   <th><s:text name="tgi.label.pnr" /></th>
                                   <th><s:text name="tgi.label.status" /></th>
                                   <th><s:text name="tgi.label.supplier" /></th>
									</tr></thead>
									<tr>
									<td data-title="<s:text name="tgi.label.created_by" />">${carOrder.createdBy}</td>
									<td data-title="<s:text name="tgi.label.pnr" />"><p style="color: #ff3100;">${carOrder.orderReference}</p></td>
									<td data-title="<s:text name="tgi.label.status" />">${carOrder.status}</td>
									<td data-title="<s:text name="tgi.label.supplier" />">${carOrder.providerAPI}</td>
									</tr>	
										</table>
								</div>
								</div></div> 
								
				<div class="row">
				<div class="profile-timeline-card"> 
				<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-car" aria-hidden="true"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.car_info" /></b></span>
									</p>  
									<div class="table-responsive no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
									<th>Car Model</th>
									<th><s:text name="tgi.label.pickup_city" /> </th>
									<th><s:text name="tgi.label.drop_off_city" /> </th>
									<th><s:text name="tgi.label.pickup_date" /></th>
									<th><s:text name="tgi.label.drop_off_date" /> </th>
									<th><s:text name="tgi.label.company_name" /> </th>
									<th><s:text name="tgi.label.hire_days" /></th>
								</tr></thead>
								<tr>
										<td data-title="<s:text name="tgi.label.pickup_city" />">${carOrder.carOrderInfo.carTitle}</td>
										<td data-title="<s:text name="tgi.label.pickup_city" />">${carOrder.carOrderInfo.pickUpOfficeName}</td>
										<td data-title="<s:text name="tgi.label.drop_off_city" />">${carOrder.carOrderInfo.returnOfficeName}</td>
										<%-- <td data-title="">${carOrder.carOrderInfo.returnOfficeName}</td> --%>
										<td data-title="<s:text name="tgi.label.pickup_date" />">${spysr:formatDate(carOrder.carOrderInfo.pickUpDateTime,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy')}</td>
										<td data-title="<s:text name="tgi.label.pickup_date" />">${spysr:formatDate(carOrder.carOrderInfo.returnDateTime,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy')}</td>
										<td data-title="<s:text name="tgi.label.company_name" />">${carOrder.carOrderInfo.companyName}</td>
										<td data-title="<s:text name="tgi.label.hire_days" />">${carOrder.carOrderInfo.hireDays}</td>
								</tr> </table></div>
								</div></div>
								 
								 <div class="row"> 
								 <div class="profile-timeline-card"> 
									<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-user" aria-hidden="true"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.passanger_info" /></b></span>
									</p>  
									<div class="table-responsive no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
									<th><s:text name="tgi.label.sno" /></th>
									<th><s:text name="tgi.label.title" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="tgi.label.date_of_birth" /></th>
								</tr></thead>
									<s:iterator status="index"><tr>
									
										<td data-title="">${index.count}</td>
										<td data-title="<s:text name="tgi.label.gender" />">${carOrder.customer.title}</td>
										<td data-title="<s:text name="tgi.label.name" />">${carOrder.customer.firstName}</td>
										<td data-title="<s:text name="tgi.label.surname" />">${carOrder.customer.lastName}</td>
										<td data-title="<s:text name="D.O.B." />">${spysr:formatDate(carOrder.customer.birthday,'MM-dd-yyy', 'MMM-d-yyyy')}</td>
										
									</tr></s:iterator>
								</table></div>
								</div></div>
								
								
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-phone"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.contact_info" /></b></span>
									</p>  
									<div class="table-responsive no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
                                   <th><s:text name="tgi.label.title" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="tgi.label.mobile" /></th>
									<th><s:text name="tgi.label.email" /></th>
								</tr></thead>
									<s:iterator status="index"><tr>
									
										<td data-title="<s:text name="tgi.label.gender" />">${carOrder.customer.title}</td>
										<td data-title="<s:text name="tgi.label.name" />">${carOrder.customer.firstName}</td>
										<td data-title="<s:text name="tgi.label.surname" />">${carOrder.customer.lastName}</td>
										<td data-title="<s:text name="tgi.label.mobile" />">${carOrder.customer.mobile}</td>
										<td data-title="<s:text name="tgi.label.email" />">${carOrder.customer.email}</td>
										
									</tr></s:iterator>
								</table></div>
								</div></div>
								
								<div class="row">
								<div class="profile-timeline-card"> 
								<c:choose>
								<c:when test="${carOrder.carOrderInsurance != null}"> 
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-user-plus"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.flight_insurance" /></b></span>
									</p>
										
								
									<div class="table-responsive no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                  <tr class="border-radius border-color">
										<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
										
									</tr></thead>
									<s:iterator status="index"><tr>
									
										<td data-title="><s:text name="tgi.label.insurance_id" />">${carOrder.carOrderInsurance.insuranceId}</td>
											<td data-title="<s:text name="tgi.label.name" />">${carOrder.carOrderInsurance.name}</td>
											<td data-title="<s:text name="tgi.label.price" />">${carOrder.carOrderInsurance.price}</td>
											<td data-title="<s:text name="tgi.label.description" />">${carOrder.carOrderInsurance.description}</td>
										
									</tr></s:iterator>
								</table></div>
								</c:when>
								<c:otherwise>
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-user-plus"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.flight_insurance" /></b></span>
									</p>
											<table class="table table-bordered in-table-border no-table-css mb-0">
												 <tr class="border-radius border-color">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.insurance_not_available" /></td>
												</tr>
											</table>
											</c:otherwise>
								</c:choose>
								</div></div>
								
								<div class="row">
								<div class="profile-timeline-card"> 
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-money"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.payment_info" /></b></span>
									</p>  
										<div class="table-responsive no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
									<th><s:text name="tgi.label.order_id" /></th>
									<th><s:text name="tgi.label.amount" /></th>
									<th><s:text name="tgi.label.payment_fees" /></th>
									<th><s:text name="tgi.label.payment_status" /></th>
								</tr></thead>
									<tr>
										
										<td data-title="<s:text name="tgi.label.order_id" />">${carOrder.orderId}</td>
										<td data-title="<s:text name="tgi.label.amount" />">${carOrder.totalPrice}</td>
										<td data-title="<s:text name="tgi.label.payment_fees" />">${carOrder.paymentFees}</td>
										<td data-title="<s:text name="tgi.label.payment_status" />">${carOrder.status}</td>
									</tr>
								</table></div>
								</div></div>
								
								<div class="row">
								<div class="profile-timeline-card"> 
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-list-alt"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.order_summary" /></b></span>
									</p>    
										<div class="table-responsive no-table-css clearfix mb-0">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
									<th><s:text name="tgi.label.order_id" /></th>
									<th><s:text name="tgi.label.base_amount" /></th>
									<th><s:text name="tgi.label.payment_fees" /></th>
									<th><s:text name="tgi.label.insurance_fee" /></th>
									<th><s:text name="tgi.label.total_amount" /></th>
								</tr></thead>
									<tr>
										
										<td data-title="<s:text name="tgi.label.total_passengers" />">${carOrder.orderId}</td>
										<td data-title="Base Amount">${carOrder.totalPrepaidPrice}</td>
										<td data-title="<s:text name="tgi.label.payment_fees" />">${carOrder.paymentFees}</td>
										<td data-title="<s:text name="tgi.label.insurance_fee" />">
										<c:choose>
											<c:when test="${carOrder.carOrderInsurance != null}">
											${carOrder.carOrderInsurance.price}
											</c:when>
											<c:otherwise>
											<s:text name="tgi.label.n_a" />
											</c:otherwise>
											</c:choose>
										</td>
										<td data-title="<s:text name="tgi.label.total" />">${carOrder.totalPrice}</td>
									</tr>
								</table></div>
								</div></div>
				 <!-- 		<form class="form-inline" action="" method="post"> -->

					<div class="table-responsive dash-table no-table-css clearfix">

						<!-- testing -->

						<div class="box clearfix" >
							<!-- <div class="box-body"> -->

							<label for="Country">
									<b><s:property value="user"/></b>
								</label>

							<table  class="">
								<%-- <tr>
									 <th colspan="2"><u><s:text name="tgi.label.created_by" /></u> :
										${carOrder.createdBy}
										</th>
											<th colspan="2"><u><s:text name="tgi.label.pnr" /></u> :
											${carOrder.orderReference}
											</th>
											<th colspan="2"><u><s:text name="tgi.label.status" /></u> : 
											${carOrder.status}
											</th>
											<th colspan="2"><u><s:text name="tgi.label.supplier" /></u> :${carOrder.providerAPI}</th>
								</tr> --%>

								<%-- <tr>
									<th colspan="5"><h5>
											<b><u><s:text name="tgi.label.car_info" /></u></b>
										</h5></th>


								</tr>
								<tr>
									<th><s:text name="tgi.label.pickup_city" /> </th>
									<th><s:text name="tgi.label.drop_off_city" /> </th>
									<th><s:text name="tgi.label.pickup_date" /></th>
									<th><s:text name="tgi.label.drop_off_date" /> </th>
									<th><s:text name="tgi.label.company_name" /> </th>
									<th><s:text name="tgi.label.hire_days" /></th>
								</tr>
								<tr>
										<th>${carOrder.carOrderInfo.pickUpOfficeName}</th>
										<th>${carOrder.carOrderInfo.returnOfficeName}</th>
										<th>${carOrder.carOrderInfo.returnOfficeName}</th>
										<th>${carOrder.carOrderInfo.pickUpDateTime}</th>
										<th>${carOrder.carOrderInfo.returnDateTime}</th>
										<th>${carOrder.carOrderInfo.companyName}</th>
										<th>${carOrder.carOrderInfo.hireDays}</th>


								</tr>  --%>
								<%-- <tr>
									<th colspan="5"><h5>
											<b><u><s:text name="tgi.label.passanger_info" /></u></b>
										</h5></th>
								</tr>

								<tr>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<th><s:text name="tgi.label.gender" /></th>
 									<th><s:text name="tgi.label.tax" /></th>
									<th><s:text name="tgi.label.total" /></th>
									<th><s:text name="tgi.label.currency" /></th>
								</tr>
									<tr>
										<th>${carOrder.customer.firstName}</th>
										<th>${carOrder.customer.lastName}</th>
										<th>${carOrder.customer.gender}</th>
										<th>0</th>
										<th>${carOrder.totalPrice}</th>
										<th>${carOrder.bookingCurrency}</th>
									</tr> --%>
								<%-- <tr>
								<tr>
									<th><h5>
											<b><u><s:text name="tgi.label.payment_info" /></u></b>
										</h5></th>


								</tr>
								<tr>
									<th><s:text name="tgi.label.order_id" /></th>
									<th><s:text name="tgi.label.amount" /></th>
									<th><s:text name="tgi.label.payment_fees" /></th>
									<th><s:text name="tgi.label.payment_status" /></th>
								</tr>
									<tr>
										<th>${carOrder.orderId}</th>
										<th>${carOrder.totalPrice}</th>
										<th>${carOrder.paymentFees}</th>
										<th>${carOrder.status}</th>
									</tr> --%>
 								<%-- <tr>
									<th colspan="5"><h4>
											<b><s:text name="tgi.label.total_passengers" />:1</b>
										</h4></th>

									<th align="left" colspan="5"><h4>
											<b>Total :${carOrder.totalPrice} ${carOrder.bookingCurrency}</b>
										</h4></th>
								</tr> --%>

 							<tr>
						<%--  <s:if test="ReportData.CreditNoteIssued==false"> 
						 <th>
									 <button type="button" id="orderNow"
											class="btn btn-primary"><s:text name="tgi.label.alter_order_now" /></button></th>
						 </s:if> --%>
						 <%--    <s:else> --%>
											<!-- <th>
									 <a href="" id="orderNow"
											class="btn btn-primary">View Credit Note</a></th>
											<th colspan="4"></th> -->
											<%--  </s:else>  --%>
											 
								</tr>
 								</table>

 							</div>
 							</div>
 							
						<!-- /.box -->
 							</div>
					<!-- table-responsive -->
					<!-- </form> -->
			 </div>
			<!-- /.row -->
			<!-- Main row -->


		</section>
		<!-- /.content -->
		<%-- <form action="insertOrderModifiedInfo"  method="post" >
								<s:if test="hasActionErrors()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionerror />
						</div>
					</s:if>
					<s:if test="hasActionMessages()">
						<div class="success-alert" style="display: none">
							<span class="fa fa-thumbs-o-up fa-1x"></span>
							<s:actionmessage />
						</div>
					</s:if>
							<table id="editOrder" style="display: none"   
								class="table table-striped table-bordered no-table-css">


								<tr style="background: #fff; padding: 20px; ">

									<td><h4><s:text name="tgi.label.booking_status" /></h4> 
									 
									<s:if test="ReportData.isOrderUpdated()">
									 <select name="statusAction"
										id="statusAction" autocomplete="off" required>
											 <option value="<s:property value="ReportData.status"/>"><s:property value="ReportData.status"/></option> 
									 </select>
									 </s:if>
									 <s:else>
									  <select name="statusAction"
										id="statusAction" autocomplete="off" required>
											<option selected="selected"   value=""><s:text name="tgi.label.select_booking_status" /></option>
											<option value="ticketed"><s:text name="tgi.label.ticketed" /></option> 
											<option value="failed"><s:text name="tgi.label.failed" /></option>
											 <option value="confirmed"><s:text name="tgi.label.confirmed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option> 
											
									</select>
									 </s:else>
									
									
									
									<input type="hidden"  name="flightOrderRowId"  value="<s:property value="ReportData.id"/>">
									<input type="hidden"  name="beforeStatus"  value="<s:property value="ReportData.status"/>">
									<input type="hidden"  name="userId"  value="<s:property value="%{#session.User.id}"/>">
									<input type="hidden"  name="companyId"  value="<s:property value="%{#session.Company.companyid}"/>">
									<input type="hidden"  name="gstAmount"  value="<s:property value="ReportData.gstOnMarkup"/>">
									<input type="hidden"  name="totalBookingAmount"  value="<s:property value="ReportData.finalPrice"/>">
									 <input type="hidden"  name="updatedBy" value="<s:property
									 value="%{#session.User.Username}"/>">
									</td>

									<td><h4><s:text name="tgi.label.paymentstatus" /></h4>
									<s:if test="ReportData.isOrderUpdated()">
									 <select name="paymentStatus"
										id="paymentStatus"    autocomplete="off" required> 
											 <option value="<s:property value="ReportData.paymentStatus"/>"><s:property value="ReportData.paymentStatus"/></option>
											 
									</select>
									 </s:if>
									<s:else>
									 <select name="paymentStatus"
										id="paymentStatus"    autocomplete="off" required> 
											<option selected="selected" value=""><s:text name="tgi.label.select_payment_status" /></option>
											 <option value="success"><s:text name="tgi.label.success" /></option>
											<option value="failed"><s:text name="tgi.label.failed" /></option>
											<option value="pending"><s:text name="tgi.label.pending" /></option>
											<option value="refund"><s:text name="tgi.label.refund" /></option>

									</select> 
									</s:else>
									 <td><h4><s:text name="tgi.label.convenience_fees" /></h4>
									  <input type="text" name="convenienceFees" id="convenienceFees"  autocomplete="off" required> 
									  
									 </td>
										  <td><h4><s:text name="tgi.label.cancellation_fees" /></h4> 
										  <s:if test="ReportData.isOrderUpdated() && ReportData.cancellationFees!=null">
									 <input type="text"  name="cancellationFees"
										id="cancellationFees"  value="<s:property value="ReportData.cancellationFees"/>"  autocomplete="off" required> 
									 </s:if>
										<s:else>
									 <input type="text" name="cancellationFees" id="cancellationFees"  autocomplete="off" required>
									  </s:else> 
										 
									  </td>
									 <td>
										<h4><s:text name="tgi.label.emp_comments" /></h4> <textarea name="employeeComments"> </textarea>

									</td>

									<td>
										<h4>Api Comments</h4> <textarea  disabled="disabled"><s:property
												value="%{#session.ReportData.apiComments}" /></textarea>

									<input type="hidden" name="apiComments" value="<s:property
												value="ReportData.apiComments"/>">
									
									
									
									</td>

									<td><h4><s:text name="tgi.label.action" /></h4>
										<button type="submit" class="btn btn-primary"><s:text name="tgi.label.update" />
											 </button></td>
								</tr>

							</table>
						</form> --%>
						</div>
	</div></div></section>
	<!-- /.content-wrapper -->
	<script type="text/javascript">
		$(document).ready(function() {
			$("#editOrder").hide();
			
			$("#orderNow").click(function() {
				$("#editOrder").toggle("slow", function() {

				});
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#twodpd2").datepicker({
				dateFormat : "yy-mm-dd"
			});
			$("#twodpd1").datepicker({
				dateFormat : "yy-mm-dd"
			/*  changeMonth: true,
			 changeYear: true */
			});
		});
	</script>
	<script type="text/javascript">
	/* 	$(document).ready(
				function() {
					var table = $('#example').DataTable({
						lengthChange : false,
						"searching" : false,
						"ordering" : false,
						"paging" : false,
						"info" : false, */
					/* "pagingType" : "full_numbers",
					"lengthMenu" : [ 10, 25, 50, 75, 100, 500 ], */
					/* buttons : [ 'excel', 'pdf', 'print' ] */
				/* 	});

					table.buttons().container().appendTo(
							'#example_wrapper .col-sm-6:eq(0)');

				}); */

		/*  $(function () {
		 	$('#example').DataTable({
		    	 "paging": true,
		         "lengthChange": true,
		        "searching": true,
		        "ordering": true,  
		           "info": true,
		         "autoWidth": false,  
		        "search": {
		      	    "regex": true,
		      	  }, 
		      	 
		      "pagingType": "full_numbers",
		      "lengthMenu": [10, 25, 50, 75, 100, 500 ],
		     
		      
		     });  
		  
		   });   */
	</script>
	
	
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"mailConfigList";
	$('#success').click(function() {
	// window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
	<%-- 
<script type="text/javascript">
   $(function(){
	   var txt = $('.success-alert').text();
	   var protocol=location.protocol;
   	   var host=location.host;
   	   //var url=protocol+"//"+host+"/LintasTravelAdmin/superUserReportList";
	   if(txt.length>0){
    	   alert(txt);
    	   //window.location.assign(url);
    	 
    	   
     }
    });
 </script>
 --%>
	<!-- 
 
  $(document).ready(function() 
    	 { 
    		 $("#twodpd1").datepicker({
    			 dateFormat: "yy-mm-dd"  
    			/*  changeMonth: true,
    			 changeYear: true */
    		 }); 
    		 }); -->

</body>
</html>