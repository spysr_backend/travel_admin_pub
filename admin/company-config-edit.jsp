<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
  <script type="text/javascript" src="admin/js/admin/panel-app.js"></script>
  <link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"companyConfigDetails?config_id="+"${param.config_id}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>	
 <style>
hr {
    margin-top: 0px;
    margin-bottom: 6px;
    border: 0;
    border-top: 2px solid #eeeeee;
}
.panel-flat {
     border-left: 2px solid #FF5722;
}
 </style>
<%-- <script type="text/javascript">
	 
	  $(function() {
		var id=$("#uniqueId").val();
		//var country_id=$("#country_id").val();
		var configStatus=$("#status").val();
		/* var rateType=$("#rateType1").val(); */
		var rateTypeFlight=$("#rateType1").val();
		var rateTypeHotel=$("#rateType2").val();
		var rateTypeCar=$("#rateType3").val();
		var rateTypeTour=$("#rateType4").val();
		
		var commissionTypeFlight=$("#commissionType1").val();
		var commissionTypeHotel=$("#commissionType2").val();
		var commissionTypeCar=$("#commissionType3").val();
		var commissionTypeTour=$("#commissionType4").val();
	 console.log("configStatus-----"+configStatus);
	 console.log("commissionType1-----"+commissionTypeFlight);
		document.getElementById('configStatus'+id).value =configStatus;
		document.getElementById('rateTypeFlight'+id).value =rateTypeFlight+"Flight";
		document.getElementById('rateTypeHotel'+id).value =rateTypeHotel+"Hotel";
		document.getElementById('rateTypeCar'+id).value =rateTypeCar+"Car";
		document.getElementById('rateTypeTour'+id).value =rateTypeTour+"Tour";
		document.getElementById('commissionTypeFlight'+id).value =commissionTypeFlight;
		document.getElementById('commissionTypeHotel'+id).value =commissionTypeHotel;
		document.getElementById('commissionTypeCar'+id).value =commissionTypeCar;
		document.getElementById('commissionTypeTour'+id).value =commissionTypeTour;
		 
	    });
	 
</script> --%>

        <!--*********  MAIN ADMIN AREA ***********************-->
        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">
                <div class="row">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><img class="clay" src="admin/img/svg/config.svg" width="26" alt="Higest"> &nbsp; Edit Company Configuration</h5>
                          		 <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
										<a class="btn btn-xs btn-outline-primary" href="companyConfigList"><span class="fa fa-arrow-left"></span> Back To List </a>
									</div>
									</div>
                                </div> 
                            </div>
                            <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-12 mt-4">
										<div class="panel panel-flat">
											<div class="panel-heading">
												<h6 class="panel-title">Config Detail<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
													<div class="heading-elements">
													<ul class="icons-list">
								                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
								                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
								                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
								                	</ul>
							                	</div>
											</div>
											<hr>
						<form id="updateCompanyConfigForm" class="form-horizontal" method="post">
							<div class="panel-body" style="display: block;">
								<div class="row">
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Company User Id</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
											<select class="form-control input-sm" name="configCompanyName" id="company_id" required="required">
														<s:if test="%{#session.Company.companyRole.isDistributor()}">
															<s:iterator value="companyList">
																<option value="${id},${companyName},${companyUserId}" ${id == companyConfig.companyId ? 'selected':'' }>${companyUserId}(${companyName})</option>
															</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="companyList">
																<option value="${id},${companyName},${companyUserId}" ${id == companyConfig.companyId ? 'selected':'' }>${companyUserId}(${companyName})</option>
															</s:iterator>
														</s:else>


											</select>
											</div></div>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.config_name" /></label>
										<div class="controls"><div class="form-group">
												<input type="text" class="form-control input-sm" id="config-name" name="configName" autocomplete="off" required="required" value="${companyConfig.configName}">
										</div>
										</div>
									</div>
									<div class="col-md-2">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.config_type" /></label>
										<div class="controls"><div class="form-group">
												<select class="form-control input-sm" name="configType" id="config-type" required="required">
												<option selected="selected" value="">Select Config Type</option>
												<option value="B2C" ${companyConfig.companyConfigType.b2c == true ? 'selected':''}>B2C</option>
												<option value="B2B" ${companyConfig.companyConfigType.b2b == true ? 'selected':''}>B2B</option>
												<option value="B2E" ${companyConfig.companyConfigType.b2e == true ? 'selected':''}>B2E</option>
												<option value="API" ${companyConfig.companyConfigType.api == true ? 'selected':''}>API</option>
												<option value="WL" ${companyConfig.companyConfigType.whitelable == true ? 'selected':''}><s:text name="tgi.label.whitelable" /></option>
												<option value="OWN_WEBSITE" ${companyConfig.companyConfigType.ownWebsite == true ? 'selected':''}>Own Website</option>
										</select>
										</div>
										</div>
									</div>
									<div class="col-md-2">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.parent_configId" /></label>
										<div class="controls"><div class="form-group">
											<select class="form-control input-sm" name="parentConfigIdSplit" id="parentConfigIdSplit" >
												<option selected value="">Select Parent Configid</option>
												<s:iterator value="%{configList}">
													<option value="<s:property value="id "/>,<s:property value="companyUserId "/>">
														<s:property value="id" /> - <s:property value="configName" /> ( <s:property value="companyUserId" />)
													</option>
												</s:iterator>
											</select>
										</div>
										</div>
									</div>
									<div class="col-md-2">
										<label class="form-control-label" for="prependedInput">Domain Name</label>
										<div class="controls"><div class="form-group">
												<input type="text" class="form-control" id="domainName" name="domainName" autocomplete="off" value="${companyConfig.domainName}"required="required">
										</div>
										</div>
									</div>
									<div class="col-md-2">
										<label class="form-control-label" for="prependedInput">Domain Url</label>
										<div class="controls"><div class="form-group">
												<input type="text" class="form-control" id="domainUrl" name="domainUrl" autocomplete="off" value="${companyConfig.domainUrl}" required="required">
										</div>
										</div>
									</div>
									
									<div class="col-md-1">
										<label class="form-control-label" for="prependedInput">Language
											</label>
										<div class="controls"><div class="form-group">
											<select class="form-control input-sm" name="languageId" id="Language" >
												<c:forEach items="${languageList}" var="lang">
																<option value="${lang.id}" ${lang.id == companyConfig.languageId  ? 'selected': ''}>${lang.language}</option>
												</c:forEach>
											</select>
										</div>
										</div>
									</div>
									<div class="col-md-1">
										<label class="form-control-label" for="currency">Currency
											</label>
										<div class="controls"><div class="form-group">
											<select class="form-control input-sm" name="currency" id="currency" >
												<option selected="selected" value="">Select Currency</option>
												<c:forEach items="${countryList}" var="currency">
																<option value="${currency.currencyCode}" ${currency.currencyCode == companyConfig.currency  ? 'selected': ''}>${currency.currencyCode}</option>
												</c:forEach>
											</select>
										</div>
										</div>
									</div>
									<%-- <div class="col-md-1">
										<label class="form-control-label" for="payment-type">Payment Type</label>
										<div class="controls"><div class="form-group">
											<select class="form-control input-sm" name="paymentType" id="payment-type" >
												<option selected="selected" value="">Select Payment Type</option>
												<option value=""></option>
											</select>
										</div>
										</div>
									</div> --%>
									<div class="col-md-1">
										<label class="form-control-label" for="tax-type">Tax Type ${companyConfig.taxType}</label>
										<div class="controls"><div class="form-group">
											<select class="form-control input-sm" name="taxType" id="tax-type" >
												<option value="GST" >GST</option>
											</select>
										</div>
										</div>
									</div>
									<div class="col-md-1" style="margin-top: 25px;"> 
										<div class="set pull-right">
										<input type="hidden" name="id" value="${companyConfig.id}">
											<button type="submit" class="btn btn-sm btn-success" id="user-company-config-btn">Update</button>
										</div>
									</div>
								</div>	
											</div>
											</form>
									</div>
									</div>
									</div>
								
	                          <div class="row">
	                    <!-- // Tour company config panel-->
	                          <div class="col-md-12 col-xs-12 col-sm-12">
											<div class="panel panel-flat">
												<div class="panel-heading">
													<h6 class="panel-title"><img class="clay" src="admin/img/svg/tourist.svg" width="22" alt="Tour"> Tour Config<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
														<div class="heading-elements">
														<ul class="icons-list">
									                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
									                	</ul>
								                	</div>
												</div>
												<hr>
							<form id="updateTourCompanyConfigForm" class="form-horizontal" method="post">
								<div class="panel-body" style="display: block;">
									<div class="row">
									<div class="col-md-12">
									<p class="text-danger"><strong>Commission</strong></p>
									</div>
									</div>
									<div class="row">
											<div class="col-md-3">
											<label class="form-control-label" for="rate-type">Rate Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="tourCommisionConfig.rateType"  id="rate-type" required>
														<option value="">Select Rate Type</option>
														<option value="Net" ${'Net' == companyConfig.tourCompanyConfig.tourCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.net" /></option>
													  	<option value="Commission" ${'Commission' == companyConfig.tourCompanyConfig.tourCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.commission" /></option>
													</select>
												</div>
												</div>
										</div>
											<div class="col-md-3">
											<label class="form-control-label" for="commission-type">Commission Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="tourCommisionConfig.commissionType" id="commission-type" >
														  <option value="">Select Commission Type</option>
														  <option value="Percentage" ${'Percentage' == companyConfig.tourCompanyConfig.tourCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.percentage" /></option>
													   	  <option value="Fixed" ${'Fixed' == companyConfig.tourCompanyConfig.tourCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.fixed" /></option>  
													 </select>
												</div>
												</div>
										</div>
										<div class="col-md-3">
											<label class="form-control-label" for="appendedInput">Commission Amount</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="tourCommisionConfig.commissionAmount" id="commission-amount" value="${companyConfig.tourCompanyConfig.tourCommisionConfig.commissionAmount}" class="form-control input-sm" autocomplete="true" placeholder="0.00">
												</div>
												</div>
										</div>
										</div>
										<hr>
										<div class="row">
										<div class="col-md-12">
										<p class="text-danger"><strong>GST</strong></p>
										</div>
										</div>
										<div class="row">
										<div class="col-md-2">
											<label class="form-control-label" for="appendedInput">Applicable Fare</label>
												<div class="controls">
												<div class="form-group">
													<input type="text" name="tourGstTaxConfig.applicableFare" id="applicable-fare" class="form-control input-sm" autocomplete="true" placeholder="0.00"
														value="${companyConfig.tourCompanyConfig.tourGstTaxConfig.applicableFare}">
												</div>
										</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="intlManagementFee">International Management Fee</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="tourGstTaxConfig.intlManagementFee" id="intlManagementFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.tourCompanyConfig.tourGstTaxConfig.intlManagementFee}">
												</div>
												</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="domesticManagementFee">Domestic Management Fee</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="tourGstTaxConfig.domesticManagementFee" id="domesticManagementFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.tourCompanyConfig.tourGstTaxConfig.domesticManagementFee}">
												</div>
												</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="convenienceFee">Convenience Fee</label >
												<div class="controls">
												<div class="form-group">
													<input type="number" name="tourGstTaxConfig.convenienceFee" id="convenienceFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.tourCompanyConfig.tourGstTaxConfig.convenienceFee}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="cgst">CGST</label>
												<div class="controls">	
												<div class="form-group">
													<input type="number" name="tourGstTaxConfig.cgst" id="cgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.tourCompanyConfig.tourGstTaxConfig.cgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="sgst">SGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="tourGstTaxConfig.sgst" id="sgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.tourCompanyConfig.tourGstTaxConfig.sgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="igst">IGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="tourGstTaxConfig.igst" id="igst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.tourCompanyConfig.tourGstTaxConfig.igst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="ugst">UGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="tourGstTaxConfig.ugst" id="ugst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.tourCompanyConfig.tourGstTaxConfig.ugst}">
												</div>
												</div>
										</div>
										</div>
										<div class="row mb-2">
										<div class="col-md-12">
											<div class="set pull-right" style="margin-bottom:0px;margin-top: 7px;">
											<div class="form-actions">
												<input type="hidden" name="configId" value="${companyConfig.id}">
												<c:choose>
												<c:when test="${companyConfig.tourCompanyConfig != null}">
													<button type="submit" class="btn btn-sm btn-success" id="user-company-config-btn">Update</button>
												</c:when>
												<c:otherwise>
													<button type="submit" class="btn btn-sm btn-primary" id="user-company-config-btn">Save</button>
												</c:otherwise>
												</c:choose>
											</div></div>
										</div>
										</div>
									</div>
									</form>
									</div>
								</div>
						<!-- Hotel Company Config Panel -->	
	                          <div class="col-md-12 col-xs-12 col-sm-12">
											<div class="panel panel-flat">
												<div class="panel-heading">
													<h6 class="panel-title"><img class="clay" src="admin/img/svg/room-key.svg" width="22" alt="Higest"> Hotel Config<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
														<div class="heading-elements">
														<ul class="icons-list">
									                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
									                	</ul>
								                	</div>
												</div>
												<hr>
									
							<form id="updateHotelCompanyConfigForm" class="form-horizontal" method="post">
								<div class="panel-body" style="display: block;">
									<div class="row">
											<div class="col-md-3">
											<label class="form-control-label" for="rate-type">Rate Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="hotelCommisionConfig.rateType"  id="rate-type" required>
														<option value="">Select Rate Type</option>
														<option value="Net" ${'Net' == companyConfig.hotelCompanyConfig.hotelCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.net" /></option>
													  	<option value="Commission" ${'Commission' == companyConfig.hotelCompanyConfig.hotelCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.commission" /></option>
													</select>
												</div>
												</div>
										</div>
											<div class="col-md-3">
											<label class="form-control-label" for="commission-type">Commission Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="hotelCommisionConfig.commissionType" id="commission-type" >
														  <option value="">Select Commission Type</option>
														  <option value="Percentage" ${'Percentage' == companyConfig.hotelCompanyConfig.hotelCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.percentage" /></option>
													   	  <option value="Fixed" ${'Fixed' == companyConfig.hotelCompanyConfig.hotelCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.fixed" /></option>  
													 </select>
												</div>
												</div>
										</div>
										<div class="col-md-3">
											<label class="form-control-label" for="appendedInput">Commission Amount</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="hotelCommisionConfig.commissionAmount" id="commission-amount" value="${companyConfig.hotelCompanyConfig.hotelCommisionConfig.commissionAmount}" class="form-control input-sm" autocomplete="true" placeholder="0.00">
												</div>
												</div>
										</div>
										</div>
										<hr>
										<div class="row">
										<div class="col-md-2">
											<label class="form-control-label" for="appendedInput">Applicable Fare</label>
												<div class="controls">
												<div class="form-group">
													<input type="text" name="hotelGstTaxConfig.applicableFare" id="applicable-fare" class="form-control input-sm" autocomplete="true" placeholder="0.00"
														value="${companyConfig.hotelCompanyConfig.hotelGstTaxConfig.applicableFare}">
												</div>
										</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="intlManagementFee">International Management Fee</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="hotelGstTaxConfig.intlManagementFee" id="intlManagementFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.hotelCompanyConfig.hotelGstTaxConfig.intlManagementFee}">
												</div>
												</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="domesticManagementFee">Domestic Management Fee</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="hotelGstTaxConfig.domesticManagementFee" id="domesticManagementFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.hotelCompanyConfig.hotelGstTaxConfig.domesticManagementFee}">
												</div>
												</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="convenienceFee">Convenience Fee</label >
												<div class="controls">
												<div class="form-group">
													<input type="number" name="hotelGstTaxConfig.convenienceFee" id="convenienceFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.hotelCompanyConfig.hotelGstTaxConfig.convenienceFee}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="cgst">CGST</label>
												<div class="controls">	
												<div class="form-group">
													<input type="number" name="hotelGstTaxConfig.cgst" id="cgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.hotelCompanyConfig.hotelGstTaxConfig.cgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="sgst">SGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="hotelGstTaxConfig.sgst" id="sgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.hotelCompanyConfig.hotelGstTaxConfig.sgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="igst">IGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="hotelGstTaxConfig.igst" id="igst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.hotelCompanyConfig.hotelGstTaxConfig.igst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="ugst">UGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="hotelGstTaxConfig.ugst" id="ugst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.hotelCompanyConfig.hotelGstTaxConfig.ugst}">
												</div>
												</div>
										</div>
										</div>
										<div class="row mb-2">
										<div class="col-md-12">
											<div class="set pull-right" style="margin-bottom:0px;margin-top: 7px;">
											<div class="form-actions">
												<input type="hidden" name="configId" value="${companyConfig.id}">
												<c:choose>
												<c:when test="${companyConfig.hotelCompanyConfig != null}">
													<button type="submit" class="btn btn-sm btn-success" id="user-company-config-btn">Update</button>
												</c:when>
												<c:otherwise>
													<button type="submit" class="btn btn-sm btn-primary" id="hotel-company-config-btn">Save</button>
												</c:otherwise>
												</c:choose>
											</div></div>
										</div>
										</div>
									</div>
									</form>
									</div>
								</div>
						<!-- Car Company Config Panel-->	
	                          <div class="col-md-12 col-xs-12 col-sm-12">
											<div class="panel panel-flat">
												<div class="panel-heading">
													<h6 class="panel-title"><img class="clay" src="admin/img/svg/car-circle.svg" width="26" alt="Higest"> Car Config<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
														<div class="heading-elements">
														<ul class="icons-list">
									                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
									                	</ul>
								                	</div>
												</div>
												<hr>
									
							<form id="updateCarCompanyConfigForm" class="form-horizontal" method="post">
								<div class="panel-body" style="display: block;">
									<div class="row">
											<div class="col-md-3">
											<label class="form-control-label" for="rate-type">Rate Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="carCommisionConfig.rateType"  id="rate-type" required>
														<option value="">Select Rate Type</option>
														<option value="Net" ${'Net' == companyConfig.carCompanyConfig.carCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.net" /></option>
													  	<option value="Commission" ${'Commission' == companyConfig.carCompanyConfig.carCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.commission" /></option>
													</select>
												</div>
												</div>
										</div>
											<div class="col-md-3">
											<label class="form-control-label" for="commission-type">Commission Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="carCommisionConfig.commissionType" id="commission-type" >
														  <option value="">Select Commission Type</option>
														  <option value="Percentage" ${'Percentage' == companyConfig.carCompanyConfig.carCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.percentage" /></option>
													   	  <option value="Fixed" ${'Fixed' == companyConfig.carCompanyConfig.carCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.fixed" /></option>  
													 </select>
												</div>
												</div>
										</div>
										<div class="col-md-3">
											<label class="form-control-label" for="appendedInput">Commission Amount</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="carCommisionConfig.commissionAmount" id="commission-amount" value="${companyConfig.carCompanyConfig.carCommisionConfig.commissionAmount}" class="form-control input-sm" autocomplete="true" placeholder="0.00">
												</div>
												</div>
										</div>
										</div>
										<hr>
										<div class="row">
										<div class="col-md-2">
											<label class="form-control-label" for="appendedInput">Applicable Fare</label>
												<div class="controls">
												<div class="form-group">
													<input type="text" name="carGstTaxConfig.applicableFare" id="applicable-fare" class="form-control input-sm" autocomplete="true" placeholder="0.00"
														value="${companyConfig.carCompanyConfig.carGstTaxConfig.applicableFare}">
												</div>
										</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="convenienceFee">Convenience Fee</label >
												<div class="controls">
												<div class="form-group">
													<input type="number" name="carGstTaxConfig.convenienceFee" id="convenienceFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.carCompanyConfig.carGstTaxConfig.convenienceFee}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="cgst">CGST</label>
												<div class="controls">	
												<div class="form-group">
													<input type="number" name="carGstTaxConfig.cgst" id="cgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.carCompanyConfig.carGstTaxConfig.cgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="sgst">SGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="carGstTaxConfig.sgst" id="sgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.carCompanyConfig.carGstTaxConfig.sgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="igst">IGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="carGstTaxConfig.igst" id="igst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.carCompanyConfig.carGstTaxConfig.igst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="ugst">UGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="carGstTaxConfig.ugst" id="ugst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.carCompanyConfig.carGstTaxConfig.ugst}">
												</div>
												</div>
										</div>
										</div>
										<div class="row mb-2">
										<div class="col-md-12">
											<div class="set pull-right" style="margin-bottom:0px;margin-top: 7px;">
											<div class="form-actions">
												<input type="hidden" name="configId" value="${companyConfig.id}">
												<c:choose>
												<c:when test="${companyConfig.carCompanyConfig != null}">
													<button type="submit" class="btn btn-sm btn-success" id="car-company-config-btn">Update</button>
												</c:when>
												<c:otherwise>
													<button type="submit" class="btn btn-sm btn-primary" id="car-company-config-btn">Save</button>
												</c:otherwise>
												</c:choose>
											</div></div>
										</div>
										</div>
									</div>
									</form>
									</div>
								</div>
						<!-- Flight Company Config Panel  -->
	                          <div class="col-md-12 col-xs-12 col-sm-12">
											<div class="panel panel-flat">
												<div class="panel-heading">
													<h6 class="panel-title"><img class="clay" src="admin/img/svg/flight.svg" width="22" alt="Higest"> Flight Config<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
														<div class="heading-elements">
														<ul class="icons-list">
									                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
									                	</ul>
								                	</div>
												</div>
												<hr>
									
							<form id="updateFlightCompanyConfigForm" class="form-horizontal" method="post">
								<div class="panel-body" style="display: block;">
									<div class="row">
											<div class="col-md-3">
											<label class="form-control-label" for="rate-type">Rate Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="flightCommisionConfig.rateType"  id="rate-type" required>
														<option value="">Select Rate Type</option>
														<option value="Net" ${'Net' == companyConfig.flightCompanyConfig.flightCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.net" /></option>
													  	<option value="Commission" ${'Commission' == companyConfig.flightCompanyConfig.flightCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.commission" /></option>
													</select>
												</div>
												</div>
										</div>
											<div class="col-md-3">
											<label class="form-control-label" for="commission-type">Commission Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="flightCommisionConfig.commissionType" id="commission-type" >
														  <option value="">Select Commission Type</option>
														  <option value="Percentage" ${'Percentage' == companyConfig.flightCompanyConfig.flightCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.percentage" /></option>
													   	  <option value="Fixed" ${'Fixed' == companyConfig.flightCompanyConfig.flightCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.fixed" /></option>  
													 </select>
												</div>
												</div>
										</div>
										<div class="col-md-3">
											<label class="form-control-label" for="appendedInput">Commission Amount</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="flightCommisionConfig.commissionAmount" id="commission-amount" value="${companyConfig.flightCompanyConfig.flightCommisionConfig.commissionAmount}" class="form-control input-sm" autocomplete="true" placeholder="0.00">
												</div>
												</div>
										</div>
										</div>
										<hr>
										<div class="row">
										<div class="col-md-2">
											<label class="form-control-label" for="appendedInput">Applicable Fare</label>
												<div class="controls">
												<div class="form-group">
													<input type="text" name="flightDomesticGstTaxConfig.applicableFare" id="applicable-fare" class="form-control input-sm" autocomplete="true" placeholder="0.00"
														value="${companyConfig.flightCompanyConfig.flightDomesticGstTaxConfig.applicableFare}">
												</div>
										</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="convenienceFee">Convenience Fee</label >
												<div class="controls">
												<div class="form-group">
													<input type="number" name="flightDomesticGstTaxConfig.convenienceFee" id="convenienceFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightDomesticGstTaxConfig.convenienceFee}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="cgst">CGST</label>
												<div class="controls">	
												<div class="form-group">
													<input type="number" name="flightDomesticGstTaxConfig.cgst" id="cgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightDomesticGstTaxConfig.cgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="sgst">SGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="flightDomesticGstTaxConfig.sgst" id="sgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightDomesticGstTaxConfig.sgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="igst">IGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="flightDomesticGstTaxConfig.igst" id="igst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightDomesticGstTaxConfig.igst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="ugst">UGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="flightDomesticGstTaxConfig.ugst" id="ugst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightDomesticGstTaxConfig.ugst}">
												</div>
												</div>
										</div>
										</div>
										<div class="row">
										<div class="col-md-2">
											<label class="form-control-label" for="appendedInput">Applicable Fare</label>
												<div class="controls">
												<div class="form-group">
													<input type="text" name="flightInternationalGstTaxConfig.applicableFare" id="applicable-fare" class="form-control input-sm" autocomplete="true" placeholder="0.00"
														value="${companyConfig.flightCompanyConfig.flightInternationalGstTaxConfig.applicableFare}">
												</div>
										</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="convenienceFee">Convenience Fee</label >
												<div class="controls">
												<div class="form-group">
													<input type="number" name="flightInternationalGstTaxConfig.convenienceFee" id="convenienceFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightInternationalGstTaxConfig.convenienceFee}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="cgst">CGST</label>
												<div class="controls">	
												<div class="form-group">
													<input type="number" name="flightInternationalGstTaxConfig.cgst" id="cgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightInternationalGstTaxConfig.cgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="sgst">SGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="flightInternationalGstTaxConfig.sgst" id="sgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightInternationalGstTaxConfig.sgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="igst">IGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="flightInternationalGstTaxConfig.igst" id="igst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightInternationalGstTaxConfig.igst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="ugst">UGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="flightInternationalGstTaxConfig.ugst" id="ugst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.flightCompanyConfig.flightInternationalGstTaxConfig.ugst}">
												</div>
												</div>
										</div>
										</div>
										<div class="row mb-2">
										<div class="col-md-12">
											<div class="set pull-right">
												<input type="hidden" name="configId" value="${companyConfig.id}">
												<c:choose>
												<c:when test="${companyConfig.flightCompanyConfig != null}">
													<button type="submit" class="btn btn-sm btn-success" id="flight-company-config-btn">Update</button>
												</c:when>
												<c:otherwise>
													<button type="submit" class="btn btn-sm btn-primary" id="flight-company-config-btn">Save</button>
												</c:otherwise>
												</c:choose>
											</div>
										</div>
										</div>
									</div>
									</form>
									</div>
								</div>
						<!-- Insurance Company Config Panel  -->
	                          <div class="col-md-12 col-xs-12 col-sm-12">
											<div class="panel panel-flat">
												<div class="panel-heading">
													<h6 class="panel-title"><img class="clay" src="admin/img/svg/insurance.svg" width="22" alt="Higest"> Insurance Config<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
														<div class="heading-elements">
														<ul class="icons-list">
									                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
									                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
									                	</ul>
								                	</div>
												</div>
												<hr>
									
							<form id="updateInsuranceCompanyConfigForm" class="form-horizontal" method="post">
								<div class="panel-body" style="display: block;">
									<div class="row">
											<div class="col-md-3">
											<label class="form-control-label" for="rate-type">Rate Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="insuranceCommisionConfig.rateType"  id="rate-type" required>
														<option value="">Select Rate Type</option>
														<option value="Net" ${'Net' == companyConfig.insuranceCompanyConfig.insuranceCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.net" /></option>
													  	<option value="Commission" ${'Commission' == companyConfig.insuranceCompanyConfig.insuranceCommisionConfig.rateType ? 'selected':'' }><s:text name="tgi.label.commission" /></option>
													</select>
												</div>
												</div>
										</div>
											<div class="col-md-3">
											<label class="form-control-label" for="commission-type">Commission Type</label>
												<div class="controls">
												<div class="form-group">
													<select class="form-control input-sm" name="insuranceCommisionConfig.commissionType" id="commission-type" >
														  <option value="">Select Commission Type</option>
														  <option value="Percentage" ${'Percentage' == companyConfig.insuranceCompanyConfig.insuranceCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.percentage" /></option>
													   	  <option value="Fixed" ${'Fixed' == companyConfig.insuranceCompanyConfig.insuranceCommisionConfig.commissionType ? 'selected':''}><s:text name="tgi.label.fixed" /></option>  
													 </select>
												</div>
												</div>
										</div>
										<div class="col-md-3">
											<label class="form-control-label" for="appendedInput">Commission Amount</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="insuranceCommisionConfig.commissionAmount" id="commission-amount" value="${companyConfig.insuranceCompanyConfig.insuranceCommisionConfig.commissionAmount}" class="form-control input-sm" autocomplete="true" placeholder="0.00">
												</div>
												</div>
										</div>
										</div>
										<hr>
										<div class="row">
										<div class="col-md-2">
											<label class="form-control-label" for="appendedInput">Applicable Fare</label>
												<div class="controls">
												<div class="form-group">
													<input type="text" name="insuranceGstTaxConfig.applicableFare" id="applicable-fare" class="form-control input-sm" autocomplete="true" placeholder="0.00"
														value="${companyConfig.insuranceCompanyConfig.insuranceGstTaxConfig.applicableFare}">
												</div>
										</div>
										</div>
										<div class="col-md-2">
											<label class="form-control-label" for="convenienceFee">Convenience Fee</label >
												<div class="controls">
												<div class="form-group">
													<input type="number" name="insuranceGstTaxConfig.convenienceFee" id="convenienceFee" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.insuranceCompanyConfig.insuranceGstTaxConfig.convenienceFee}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="cgst">CGST</label>
												<div class="controls">	
												<div class="form-group">
													<input type="number" name="insuranceGstTaxConfig.cgst" id="cgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.insuranceCompanyConfig.insuranceGstTaxConfig.cgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="sgst">SGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="insuranceGstTaxConfig.sgst" id="sgst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.insuranceCompanyConfig.insuranceGstTaxConfig.sgst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="igst">IGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="insuranceGstTaxConfig.igst" id="igst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.insuranceCompanyConfig.insuranceGstTaxConfig.igst}">
												</div>
												</div>
										</div>
										<div class="col-md-1">
											<label class="form-control-label" for="ugst">UGST</label>
												<div class="controls">
												<div class="form-group">
													<input type="number" name="insuranceGstTaxConfig.ugst" id="ugst" class="form-control input-sm" autocomplete="true" placeholder="0.00"
													value="${companyConfig.insuranceCompanyConfig.insuranceGstTaxConfig.ugst}">
												</div>
												</div>
										</div>
										</div>
										<div class="row mb-2">
										<div class="col-md-12">
											<div class="set pull-right">
												<input type="hidden" name="configId" value="${companyConfig.id}">
												<c:choose>
												<c:when test="${companyConfig.insuranceCompanyConfig != null}">
													<button type="submit" class="btn btn-sm btn-success" id="insurance-company-config-btn">Update</button>
												</c:when>
												<c:otherwise>
													<button type="submit" class="btn btn-sm btn-primary" id="insurance-company-config-btn">Save</button>
												</c:otherwise>
												</c:choose>
											</div>
										</div>
										</div>
									</div>
									</form>
									</div>
								</div>
								</div>
                            </div>
                        </div>
            </section>
        <!--ADMIN AREA ENDS-->

 
<script type="text/javascript">

$("#updateCompanyConfigForm").submit(function(e) {
	e.preventDefault();
	$.ajax({
		url : "updateCompanyConfigDetails",
		type : "POST",
		dataType : 'json',
		data : $("#updateCompanyConfigForm").serialize(),
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
				setTimeout(location.reload.bind(location), 1000);
			}
			else if(jsonData.message.status == 'error'){
				 alertify.error(jsonData.message.message);
			}
		},
		error : function(request, status, error) {
			alertify.error("company config can't be updated,please try again");
		}
	});
}); 
$("#updateTourCompanyConfigForm").submit(function(e) {
	e.preventDefault();
	$.ajax({
		url : "saveTourCompanyConfig",
		type : "POST",
		dataType : 'json',
		data : $("#updateTourCompanyConfigForm").serialize(),
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
				setTimeout(location.reload.bind(location), 1000);
			}
			else if(jsonData.message.status == 'error'){
				 alertify.error(jsonData.message.message);
			}
		},
		error : function(request, status, error) {
			alertify.error("tour company config can't be updated,please try again");
		}
	});
}); 
//Hotel Company Config Save Ajax-
$("#updateHotelCompanyConfigForm").submit(function(e) {
	e.preventDefault();
	$.ajax({
		url : "saveHotelCompanyConfig",
		type : "POST",
		dataType : 'json',
		data : $("#updateHotelCompanyConfigForm").serialize(),
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
				setTimeout(location.reload.bind(location), 1000);
			}
			else if(jsonData.message.status == 'error'){
				 alertify.error(jsonData.message.message);
			}
		},
		error : function(request, status, error) {
			alertify.error("hotel company config can't be updated,please try again");
		}
	});
}); 
//Car Company Config Save Ajax-
$("#updateCarCompanyConfigForm").submit(function(e) {
	e.preventDefault();
	$.ajax({
		url : "saveCarCompanyConfig",
		type : "POST",
		dataType : 'json',
		data : $("#updateCarCompanyConfigForm").serialize(),
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
				setTimeout(location.reload.bind(location), 1000);
			}
			else if(jsonData.message.status == 'error'){
				 alertify.error(jsonData.message.message);
			}
		},
		error : function(request, status, error) {
			alertify.error("hotel company config can't be updated,please try again");
		}
	});
}); 
//Flight Company Config Save Ajax-
$("#updateFlightCompanyConfigForm").submit(function(e) {
	e.preventDefault();
	$.ajax({
		url : "saveFlightCompanyConfig",
		type : "POST",
		dataType : 'json',
		data : $("#updateFlightCompanyConfigForm").serialize(),
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
				setTimeout(location.reload.bind(location), 1000);
			}
			else if(jsonData.message.status == 'error'){
				 alertify.error(jsonData.message.message);
			}
		},
		error : function(request, status, error) {
			alertify.error("hotel company config can't be updated,please try again");
		}
	});
}); 
//Flight Company Config Save Ajax-
$("#updateInsuranceCompanyConfigForm").submit(function(e) {
	e.preventDefault();
	$.ajax({
		url : "saveInsuranceCompanyConfig",
		type : "POST",
		dataType : 'json',
		data : $("#updateInsuranceCompanyConfigForm").serialize(),
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
				setTimeout(location.reload.bind(location), 1000);
			}
			else if(jsonData.message.status == 'error'){
				 alertify.error(jsonData.message.message);
			}
		},
		error : function(request, status, error) {
			alertify.error("hotel company config can't be updated,please try again");
		}
	});
}); 
/*----------------------------------------------------------------*/
$(function() {
	
	var id=$("#uniqueId").val();
    $('#rateTypeFlight'+id).change(function(){
    	 if($('#rateTypeFlight'+id).val()== 'CommissionFlight') {
            $('.commission-group').show(); 
        } 
        else{
        	 $('.commission-group').hide(); 
          
       } 
        
    });
    
    $('#rateTypeHotel'+id).change(function(){
    	 if($('#rateTypeHotel'+id).val()== 'CommissionHotel') {
            $('.commission-group').show(); 
        } 
        else{
        	 $('.commission-group').hide(); 
          
       } 
        
    });
    
    
    $('#rateTypeCar'+id).change(function(){
   	 if($('#rateTypeCar'+id).val()== 'CommissionCar') {
           $('.commission-group').show(); 
       } 
       else{
       	 $('.commission-group').hide(); 
         
      } 
       
   });
    
    
    $('#rateTypeTour'+id).change(function(){
   	 if($('#rateTypeTour'+id).val()== 'CommissionTour') {
           $('.commission-group').show(); 
       } 
       else{
       	 $('.commission-group').hide(); 
         
      } 
       
   });
   
 });
 /*-------------------------------*/
 $(document).ready(function(){
	$("#config-type").change(function() { 
	var configType = $("#config-type").val();
	$("#status").html('<img src="admin/img/loading.gif" align="absmiddle">&nbsp;Checking availability...');
	    $.ajax({  
	    url: "checkConfigTypeExist?configType="+configType,  
	    type: "POST",  
	    dataType: 'json',  
	    success: function(jsonData){  
	  		if(jsonData.message.status == 'success')
	  		{
	  			$("#config-type").removeClass('object_error'); // if necessary
				$("#config-type").addClass("object_ok");
				$("#user-save-btn").prop("disabled", false);
				$("#status").html("<img class='clippy' src='admin/img/svg/checked_1.svg' width='12'>&nbsp;<span class='' style='font-size: 12px;color: #3cc341;'>"+jsonData.message.message+"</span>");
			}
			else if(jsonData.message.status == 'error')
			{
				$("#config-type").removeClass('object_ok'); // if necessary
				$("#config-type").addClass("object_error");
				$("#user-save-btn").prop("disabled", true);
				$("#status").html("<img class='clippy' src='admin/img/svg/close.svg' width='10'>&nbsp;<span class='text-danger' style='font-size: 12px;'>"+jsonData.message.message+"</span>");
			}
	   		}
	    });   
	});
 });
 </script>  
 
