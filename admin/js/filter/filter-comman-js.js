$(document).ready(function(){$(".filterBtn").click(function(){$("#filterDiv").toggle(500)}),
$("div.easy-autocomplete").removeAttr("style"),$("#configreset").click(function(){$("#resetform")[0].reset()})}),
$(".filter-link").click(function(){var e=$(this).find("i").hasClass("fa-angle-up");$(".filter-link").find("i").removeClass("fa-angle-down"),
$(".filter-link").find("i").addClass("fa-angle-up"),e&&$(this).find("i").toggleClass("fa-angle-up fa-2x fa-angle-down fa-2x")});
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------*/
$(function() {
	  $('input[name="createdDateRange"],input[name="assignDateRange"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: {
	          cancelLabel: 'Clear'
	      },
	      "showDropdowns": true,
	      ranges: {
	           /* 'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')], */
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        },
	        "cancelClass": "btn-danger"
	  });

	  $('input[name="createdDateRange"],input[name="assignDateRange"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
	  });

	  $('input[name="createdDateRange"],input[name="assignDateRange"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });
	});

$(function() {
	$('input[name="expiryDateFlag"]').daterangepicker({
		 autoUpdateInput: false,
		singleDatePicker: true,
	    showDropdowns: true,

	},function(start, end, label) {
		
		$('input[name="expiryDateFlag"]').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(start.format('MM/DD/YYYY'));
		});
		
		$('input[name="expiryDateFlag"]').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
		
	});
});