
$(document).ready(function(){
	$("#email-address").change(function() { 
	var email = $("#email-address").val();
	if(email.length >= 4)
	{
	$("#email-status").html('<img src="admin/img/loading.gif" align="absmiddle">&nbsp;Checking availability...');
	    $.ajax({  
	    url: "checkUserExist?email_address="+email,  
	    type: "POST",  
	    dataType: 'json',  
	    success: function(jsonData){  
	  	// $("#status").ajaxComplete(function(event, request, settings){ 
			
	  		if(jsonData.message.status == 'success')
	  		{
	  			$("#email-address").removeClass('object_error'); // if necessary
				$("#email-address").addClass("object_ok");
				$("#email-status").html("<img class='' src='admin/img/svg/checked_1.svg' width='10'>&nbsp;<span class='' style='font-size: 12px;color: #3cc341;'>"+jsonData.message.message+"</span>");
				$(".user-save-btn").prop("disabled", false);
			}
			else if(jsonData.message.status == 'error')
			{
				$("#email-address").removeClass('object_ok'); // if necessary
				$("#email-address").addClass("object_error");
				$("#email-status").html("<img class='' src='admin/img/svg/close.svg' width='8'>&nbsp;<span class='text-danger' style='font-size: 12px;'>"+jsonData.message.message+"</span>");
				$(".user-save-btn").prop("disabled", true);
			}
	   //});
	 } 
	   
}); 
}
else
	{
	$("#email-status").html('<font color="red" style="font-size: 12px;">The email should have at least <strong>4</strong> characters.</font>');
	$("#email-address").removeClass('object_ok'); // if necessary
	$("#email-address").addClass("object_error");
	}

	});
	});