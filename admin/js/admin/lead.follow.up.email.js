$('#send_email_agent').on("click", function(event){
	 var allVals = [];
	 $(".check_row:checked").each(function() {  
		 allVals.push($(this).attr('data-lead-id'));
	 });  
			 var join_selected_values = []; 
			 join_selected_values = allVals.join(",");
			 console.log(join_selected_values);
			 $.ajax({
						url : "send_company_lead_follow_email?companyLeadIds="+join_selected_values,
						type : "POST",
						dataType: 'json',
						data : $("#sendEmailToAgentForm").serialize(),
						success : function(jsonData) {
							if(jsonData.message.status == 'success'){
								$.notify({icon: 'fa fa-check',message: jsonData.message.message},{type: 'success'});
								window.location.reload(1000);
							}
			  				else if(jsonData.message.status == 'error'){
			  					$(".email-msg-alert").html('<div class="msg-alert msg-danger msg-danger-text"> <i class="fa fa-times"></i><span class="error-msg">&nbsp;'+jsonData.message.message+'</span></div>');
			  				}
						},
						error: function (request, status, error) {
							alertify.error("Something problem saving data,Try again");
						}
				}); 
});
$('#master_check,.check_row').on('click', function(e) {
	if($(this).is(':checked',true)) {
		if($(this).data("action")=="all")
			{
				$(".check_row").prop('checked', true);
				$(".check_row").closest('td').toggleClass("highlight", this.checked);
			}	
		else
			{
				$(this).closest('td').toggleClass("highlight", this.checked);
			}
	}  
	else {  
		if($(this).data("action")=="all")
		{
			$(".check_row").prop('checked',false);
			$(".check_row").closest('td').removeClass("highlight", this.checked);
		}
		else
			$(this).closest('td').removeClass("highlight", this.checked);
		$("#btn-title").attr('data-original-title', 'Please Select Row');
	}
	var allVals = [];
	 $(".check_row:checked").each(function() {  
		 allVals.push($(this).attr('data-lead-id'));
	 });  
	console.log(allVals);
	if(allVals.length>0)
		{
			if(allVals.length==$("div#btn-title").data("size"))
				{
					console.log("all selected");
					$("#master_check").prop('checked',true);
					$("div#btn-title").attr('data-original-title', 'Archive All');
					$("#span-all-del").html("Archive All <b>( "+allVals.length+" )</b>");
					$("#span-all-email").html(" Email All <b>( "+allVals.length+" )</b>");
				}
			else{
					console.log("selected"+allVals.length);
					$("div#btn-title").attr('data-original-title', 'Archive Selected');
					$("#span-all-email").html(" Email Selected <b>( "+allVals.length+" )</b>");
					$("#span-all-del").html("Archive Selected <b>( "+allVals.length+" )</b>");
					$("#master_check").prop('checked',false);
			}
			$("#send_lead_email").prop("disabled", false);
			$("#archive_lead_data").prop("disabled", false);
		}
	else
		{
			$("#send_lead_email").prop("disabled", true);
			$("#archive_lead_data").prop("disabled", true);
		}
	
});
$('input[type=checkbox]').change(function (){
    if ($(this).prop("checked")) 
		$(this).val(true);
    else
    	$(this).val(false);
});
