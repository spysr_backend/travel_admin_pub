
$('#master_check,.check_row').on('click', function(e) {
	if($(this).is(':checked',true)) {
		if($(this).data("action")=="all")
			{
				$(".check_row").prop('checked', true);
				$(".check_row").closest('td').toggleClass("highlight", this.checked);
			}	
		else
			{
				$(this).closest('td').toggleClass("highlight", this.checked);
			}
	}  
	else {  
		if($(this).data("action")=="all")
		{
			$(".check_row").prop('checked',false);
			$(".check_row").closest('td').removeClass("highlight", this.checked);
		}
		else
			$(this).closest('td').removeClass("highlight", this.checked);
		$("#btn-title").attr('data-original-title', 'Please Select Row');
	}
	var allVals = [];
	 $(".check_row:checked").each(function() {  
		 allVals.push($(this).attr('data-id'));
	 });  
	console.log(allVals);
	if(allVals.length>0)
		{
			if(allVals.length==$("div#btn-title").data("size"))
				{
					console.log("all selected");
					$("#master_check").prop('checked',true);
					$("div#btn-title").attr('data-original-title', 'Archive All');
					$("#span-all-del").html("Archive All");
					$("#master_check").closest('th').removeClass("highlight-light", this.checked);
					$("#master_check").closest('th').toggleClass("highlight", this.checked);
				}
			else{
					console.log("selected"+allVals.length);
					$("div#btn-title").attr('data-original-title', 'Archive Selected');
					$("#span-all-del").html("Archive");
					$("#master_check").closest('th').removeClass("highlight");
					$("#master_check").closest('th').addClass("highlight-light");
					$("#master_check").prop('checked',false);
			}
			$("#get_selected_row_data").prop("disabled", false);
		}
	else
		{
			$("#get_selected_row_data").prop("disabled", true);
			$("#master_check").closest('th').removeClass("highlight");
			$("#master_check").closest('th').removeClass("highlight-light");
		}
	
});
$('input[type=checkbox]').change(function (){
    if ($(this).prop("checked")) 
		$(this).val(true);
    else
    	$(this).val(false);
});
