<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"editFlightDealsFrom?id="+"${flightDealsFrom.id}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                         <div class="hd clearfix">
                                <h5 class="pull-left">Update Flight Deals From 
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="metatagpage_list"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.meta_dat_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div>
                             <div class="hd clearfix">
                            </div>
                              <div class="cnt cnt-table">
					<div class="table-responsive no-table-css">
						<form action="updateFlightDealsFrom">
							<table class="table table-form">
								<thead>
									<tr>
										<th></th>
										<th>&nbsp;</th>


									</tr>
								</thead>
								<tbody>
									<tr>

										<td>
											<div class="field-name">
												Origin
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="origin" placeholder="Origin" value="${flightDealsFrom.origin}" autocomplete="off"
											required></td>

										<td>
											<div class="field-name">
												Destination
											</div>
										</td>
										<td><input type="text" class="form-control input-sm"
											name="destination" value="${flightDealsFrom.destination}" placeholder="Destination" autocomplete="off"
											required></td>


									</tr>

									<tr>
										<td>
											<div class="field-name">
												Trip Type
											</div>
										</td>
										<td><select name="tripType" class="form-control">
										<option value="O"  ${flightDealsFrom.tripType == 'O' ?'selected' :''}  >One-Way</option>
										<option value="R" ${flightDealsFrom.tripType == 'R' ?'selected' :''}  >Round-Trip</option>
										</select> </td>
										<td>
											<div class="field-name">
												Active 
											</div>
										</td>
										<td><input type="checkbox" name="active" <c:if test="${flightDealsFrom.active == true}">checked="checked"</c:if>  class="form-control" /></td>
									<tr>
										<td></td>
										<td></td>
										<td>
										<input type="hidden" name="id" value="${flightDealsFrom.id}" />
										<input type="hidden" name="version" value="${flightDealsFrom.version}" />
										<input type="submit" class="btn btn-info">
										</td>
										<td></td>
									</tr>
								</tbody>
							</table>

						</form>
					</div>
				</div>
</div></div></div>
                            </section>        
                                    			<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	 <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_body').text("${message}");
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>