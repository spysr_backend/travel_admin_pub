<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
  <link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="admin/css/less-plugins/awesome-bootstrap-checkbox.css">
  <script type="text/javascript" src="admin/js/admin/panel-app.js"></script>
      <!--************************************
        MAIN ADMIN AREA
    ****************************************-->
<style>
.comment-box {
	display: none;
}
.comment-list{
    border-top: 1px dashed #b2b2b3;
}
.dropdown-menu {
    min-width: 130px;
    padding: 8px 0px;
}
.dropdown-menu>li>a {
    display: block;
    padding: 4px 15px;
    clear: both;
    font-weight: normal;
    line-height: 23px;
    color: rgb(0, 112, 210);
    white-space: nowrap;
}
button.btn.dropdown-toggle.btn-default {
    padding: 4px 6px;
}
button.btn.btn-sm.btn-default.dropdown-toggle {
    padding: 3px 8px !important;
    font-size: 12px !important;
    line-height: 1.5 !important;
    border-radius: 2px !important;
}
.btn-drop{
    padding: 4px 10px;
}
.clay{
margin-bottom: 3px;
}
.img-fluid {
    width: 100%;
    height: auto;
    border: 2px dotted #a0a0a0;
}

@media screen and (max-width:767px){
	.img-fluid {
    width: 100%;
    height: auto;
    border: 2px dotted #a0a0a0;
}
}
.check
{
  	opacity:1;
	border: 12px solid #d4d4d4;
	
}
.bottom-line
{
    margin-top: 12px;
    margin-bottom: 12px;
    border: 0;
    border-top: 4px solid #eeeeee;
    border-radius: 32px;
}
</style>
        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">  
                <div class="row">
                <div class="pnl">
                <nav aria-label="breadcrumb">
				  <div class="hd clearfix own-bradcum mb-4">
				  <h5 class="">Company Lead Profile</h5>   
				  			<div class="dropdown pull-right mt-10">
							  <a href="addCompanyLeadDetail" class="btn btn-xs btn-primary dropdown-toggle" type="button"><i class="fa fa-plus" aria-hidden="true"></i> New Lead</a>
							  <a href="listCompanyLead" class="btn btn-xs btn-primary dropdown-toggle" type="button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back To List</a>
							</div>
				  </div>
				</nav>
				</div>
                <div class="col-md-2 col-xs-12 col-sm-12" style="padding-right: 0px;">
					<div class="sidebar-detached">
						<div class="sidebar sidebar-default sidebar-separate">
							<div class="sidebar-content">
								<!-- User details -->
								<div class="content-group">
									<div class="panel-body bg-indigo-400 border-radius-top text-center" style="background-image: url(admin/img/bg.png); background-size: contain;">
										<a href="#" class="display-inline-block content-group-sm">
										<c:choose>
										<c:when test="${companyLead.companyLeadDetail.company_logo_path != null}">
										<img src="<s:url action='getImageByRequest?imagePath=%{companyLead.companyLeadDetail.company_logo_path}'/>" class="img-responsive" alt="" style="width: 385px; height: 100px;">
										</c:when>
										<c:otherwise>
										<img src="admin/img/no-image.jpg" class="img-responsive" alt="" style="width: 100%; height: 150px; object-fit:cover;">
										</c:otherwise>
										</c:choose>
										</a>
										<div class="content-group-sm">
											<h5 class="text-semibold no-margin-bottom">
												${companyLead.companyName}
											</h5>
											<!-- <p class="display-block" style="font-size:12px">Head of UX</p> -->
										</div>
										<!-- <ul class="list-inline list-inline-condensed no-margin-bottom">
											<li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-google-drive"></i></a></li>
											<li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-twitter"></i></a></li>
											<li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-github"></i></a></li>
										</ul> -->
										<h6 class="pull-left text-semibold no-margin-bottom mt-0 pt-0">Change Logo</h6>
										<form id="imageUploadForm" method="post" enctype="multipart/form-data">
										<label class="btn-bs-file btn btn-xs btn-outline-primary pull-right mr-1" data-toggle="tooltip" data-placement="top" title="Upload New Logo">Browse
						                	<input type="file" name="uploadFile" onchange="companyProfileUploadFn(${companyLead.id});" />
					            		</label>
					            		</form>
									</div>

									<div class="panel no-border-top no-border-radius-top">
										<ul class="navigation">
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Email Address" >
											<a href="mailto:${companyLead.email}" aria-expanded="false" class="a-tab"><i class="icon-files-empty"></i> ${companyLead.email} 
											<span class="pull-right bg-warning-400"><i class="fa fa-envelope" aria-hidden="true"></i></span></a></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Confirmation Mail Status" >
											<a href="#schedule" data-toggle="tab" aria-expanded="false" class="a-tab">
											<i class="icon-files-empty"></i> Email Verified 
											<span class="pull-right bg-warning-400">
											<c:choose>
                                                    <c:when test="${companyLead.emailVerified != false}">
                                                    <div class="center">
                                                    <img class="clippy" src="admin/img/svg/checkedg.svg" width="15" alt="Copy to clipboard" style="margin-bottom: 3px;">
                                                    </div>
                                                    </c:when>
                                                    <c:otherwise>
                                                     <div class="center">
                                                     <img class="clippy" src="admin/img/svg/close.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
                                                     </div>
                                                    </c:otherwise>
                                                    </c:choose>
											</span></a></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Contact Number" >
											<a href="tel:${companyLead.phone}" aria-expanded="false" class="a-tab"><i class="icon-files-empty"></i> ${companyLead.phone}</a>
											<span class="pull-right bg-warning-400"><a href="tel:${phone}"><img class="clippy" src="admin/img/svg/telephone-g.svg" width="14"style="margin-bottom: 3px;"></a></span></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Company Website" >
											<a href="http://${companyLead.website}"  aria-expanded="false" target="_blank" class="a-tab">
											${companyLead.website} <span class="pull-right bg-warning-400"><i class="fa fa-globe" aria-hidden="true"></i></span></a></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Created User By" ><i class="icon-files-empty"></i> Created By 
											<span class="pull-right bg-warning-400">${companyLead.createdByName}</span></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Created At Date" ><i class="icon-files-empty"></i> Created At 
											<span class="pull-right bg-warning-400">${spysr:formatDate(companyLead.createdDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy - HH:mm a')}</span></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Created At Date" ><i class="icon-files-empty"></i> Email Sent Count
											<span class="pull-right bg-warning-400"><c:choose>
								                <c:when test="${emailCountStatusList.size() > 0}">
								                ${emailCountStatusList.size()} 
								                <span class="link">
								                <a data-email-content="popover-content" data-placement="left" data-toggle="popover" title="Email Sent History" data-container="body" data-html="true" href="#!" id="login" style="cursor: pointer;" class="">times
								                </a>
								                </span>
								                </c:when>
								                <c:otherwise>
													0 times														                
								                </c:otherwise>
								                </c:choose></span></span>
								                </li>
											<c:if test="${companyLead.emailVerified != false}">
											<c:if test="${companyLead.companyCreated == false}">
											<hr>
											<c:choose>
											<c:when test="${companyLead.emailExist != true}">
												<li class="li-change">
												<span class="" data-toggle="tooltip" data-placement="top" title="Please Insert Company If All Varification Is Complete">
												<a href="createCompany?companyLeadId=${companyLead.id}" class="btn btn-primary btn-block" aria-expanded="false" class="a-tab" target="_blank"><i class="icon-switch2"></i> Create Agent Company Profile
												<span class="pull-right bg-warning-400"><img class="clippy" src="admin/img/svg/plus-symbol-w.svg" width="18" alt="Copy to clipboard" style="margin-bottom: 3px;"></span>
												</a>
												</span>
												</li>
											</c:when>
											<c:otherwise>
											<li class="li-change">
												<p class="text-warning" id="">${companyLead.emailExistText}</p>
												</li>
											</c:otherwise>
											</c:choose>
											
											</c:if>
											</c:if>
										</ul>
									</div>
								</div>

							</div>
						</div>
					</div>
	<form id="updateCompanyLeadForm" class="form-horizontal" method="post">
							<div class="container-detached">
							<div class="content-detached row-minus">
										<!-- Profile info -->
										<div class="col-md-12 col-xs-12 col-sm-12">
										<div class="panel panel-flat">
											<div class="panel-heading">
												<h6 class="panel-title">Profile information<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
												<div class="heading-elements">
													<ul class="icons-list">
								                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
								                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
								                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
								                	</ul>
							                	</div>
											</div>
											<hr>
											<div class="panel-body" style="display: block;margin-bottom: 0.9rem;">
													<div class="">
														<div class="row">
															<div class="col-md-6 form-group">
																<label class="form-control-label" for="firstName">First Name</label>
																<input type="text" name="firstName" value="${companyLead.firstName}" class="form-control input-sm">
															</div>
															<div class="col-md-6 form-group">
																<label>Last Name</label>
																<input type="text" name="lastName" value="${companyLead.lastName}" class="form-control input-sm">
															</div>
															<div class="col-md-12 form-group">
																<label>Email</label>
																<input type="text" name="email" id="email-address" value="${companyLead.email}" class="form-control input-sm">
															</div>
															<div class="col-md-12 form-group">
																<label>Phone #</label>
																<input type="text" name="phone" value="${companyLead.phone}" class="form-control input-sm" maxlength="12">
															</div>
															<div class="col-md-12 form-group">
																<label>Company</label>
																<input type="text" name="companyName" value="${companyLead.companyName}" class="form-control input-sm">
															</div>
															<div class="col-md-12 form-group">
																<label>Website</label>
																<input type="text" name="website" value="${companyLead.website}" class="form-control input-sm">
															</div>
															<div class="col-md-12 form-group">
																<label>Description</label>
																<textarea name="companyLeadDetail.description" class="form-control mb-15" rows="2" cols="1" placeholder="What's on your mind?">${companyLead.companyLeadDetail.description}</textarea>
															</div>
															
														</div>
													</div>
											</div>
											<div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
										<div class="heading-elements">
											<span class="heading-text text-semibold text-warning">Email Verify:</span>
											<div class="heading-form pull-right">
												<div class="form-group">
													<div class="checkbox checkbox-success col-sm-6 mt-2">
														<input name="emailVerified" id="emailVerified" type="checkbox" <c:if test="${companyLead.emailVerified =='true'}">checked </c:if> value="${companyLead.emailVerified}"> 
														<label for="terminate" class="text-danger"></label>
													</div>
												</div>
											</div>
										</div>
									</div>
										</div>
										</div>
										<!-- /profile info -->
										<!-- Account settings -->
										<div class="col-md-12 col-xs-12 col-sm-12">
										<div class="panel panel-flat">
											<div class="panel-heading">
												<h6 class="panel-title">Company Detail<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
													<div class="heading-elements">
													<ul class="icons-list">
								                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
								                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
								                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
								                	</ul>
							                	</div>
											</div>
											<hr>
											<div class="panel-body" style="display: block;">
													<div class="form-group">
														<div class="row">
															<div class="col-md-12">
																<label>Address</label>
																<textarea name="companyLeadDetail.address" rows="2" class="form-control input-sm" style="height: 45px;">${companyLead.companyLeadDetail.address}</textarea>
															</div>
															</div>
															</div>
															<div class="row">
															<div class="col-md-6 form-group">
																<label>City</label>
																<input type="text" name="companyLeadDetail.city" value="${companyLead.companyLeadDetail.city}" class="form-control input-sm">
															</div>
															<div class="col-md-6 form-group">
																<label>Zip Code</label>
																<input type="text" name="companyLeadDetail.zip_code" value="${companyLead.companyLeadDetail.zip_code}" class="form-control input-sm">
															</div>
															<div class="col-md-6 form-group">
																<label>State</label>
																<select name="companyLeadDetail.state" id="state" class="form-control usst input-sm" data-live-search="true">
																    <option selected  value="">Select State</option>
																	<c:forEach items="${statesList}" var="states">
																		<option value="${states.state}">${states.state}</option>
																	</c:forEach>
																</select> 
															</div>
															<div class="col-md-6 form-group">
																<label>Country</label>
																<select name="companyLeadDetail.country" id="country" class="form-control usst input-sm" data-live-search="true">
																    <option value="">Select Country</option>
																			<c:forEach items="${countryList}" var="country">
																				<option value="${country.countryName}" ${country.countryName == companyLead.companyLeadDetail.country ? 'selected' : ''} >${country.countryName}</option>
																	</c:forEach>
																</select> 
															</div>
															</div>
															<hr>
															<div class="row">
															<div class="col-md-12">
															<label class="center text-danger">Services</label>
															</div>
															</div>
															<div class="row">
																<div class="col-md-4 col-xs-6 form-group">
																<div class="checkbox checkbox-default">
																	<input name="companyLeadDetail.tour" id="tour" type="checkbox" value="${companyLead.companyLeadDetail.tour}" ${companyLead.companyLeadDetail.tour == true ? 'checked' : ''}>
																	<label for="tour">&nbsp;&nbsp;Tour </label>
																</div>
																<div class="checkbox checkbox-default">
																	<input name="companyLeadDetail.flight" id="flight" type="checkbox" value="${companyLead.companyLeadDetail.flight}" ${companyLead.companyLeadDetail.flight == true ? 'checked' : ''}>
																	<label for="flight">&nbsp;&nbsp;Flight</label>
																</div>
																</div>
																<div class="col-md-4 col-xs-6 form-group">
																<div class="checkbox checkbox-default">
																	<input name="companyLeadDetail.hotel" id="hotel" type="checkbox" value="${companyLead.companyLeadDetail.hotel}" ${companyLead.companyLeadDetail.hotel == true ? 'checked' : ''}>
																	<label for="hotel">&nbsp;&nbsp;Hotel</label>
																</div>
																<div class="checkbox checkbox-default">
																	<input name="companyLeadDetail.car" id="car" type="checkbox" value="${companyLead.companyLeadDetail.car}" ${companyLead.companyLeadDetail.car == true ? 'checked' : ''}>
																	<label for="car">&nbsp;&nbsp;Car</label>
																</div>
																</div>
																<div class="col-md-4 col-xs-6 form-group">
																<div class="checkbox checkbox-default">
																	<input name="companyLeadDetail.train" id="train" type="checkbox" value="${companyLead.companyLeadDetail.train}" ${companyLead.companyLeadDetail.train == true ? 'checked' : ''}>
																	<label for="train">&nbsp;&nbsp;Train</label>
																</div>
																<div class="checkbox checkbox-default">
																	<input name="companyLeadDetail.cruise" id="cruise" type="checkbox" value="${companyLead.companyLeadDetail.cruise}" ${companyLead.companyLeadDetail.cruise == true ? 'checked' : ''}>
																	<label for="cruise">&nbsp;&nbsp;Cruise</label>
																</div>
																</div>
																<div class="col-md-12 col-xs-6 form-group">
																<div class="checkbox checkbox-default">
																	<input name="companyLeadDetail.visa" id="visa" type="checkbox" value="${companyLead.companyLeadDetail.visa}" ${companyLead.companyLeadDetail.visa == true ? 'checked' : ''}>
																	<label for="visa">&nbsp;&nbsp;Visa</label>
																</div>
																<div class="checkbox checkbox-default">
																	<input name="companyLeadDetail.travelInsurance" id="travelInsurance" type="checkbox" value="${companyLead.companyLeadDetail.travelInsurance}" ${companyLead.companyLeadDetail.travelInsurance == true ? 'checked' : ''}>
																	<label for="travelInsurance">&nbsp;&nbsp;Travel Insurance</label>
																</div>
																</div>
														</div>
											</div>
											<div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
										<div class="heading-elements">
											<span class="heading-text text-semibold text-warning">Account Terminate:</span>
											<div class="heading-form pull-right">
												<div class="form-group">
													<div class="checkbox checkbox-default col-sm-6 mt-2">
														<input name="terminate" id="terminate" type="checkbox" <c:if test="${companyLead.terminate =='true'}">checked </c:if> value="${companyLead.terminate}"> 
														<label for="terminate" class="text-danger">&nbsp;<span class="" data-toggle="tooltip" data-placement="top" title="Terminate account if there is no activity"></span></label>
													</div>
												</div>
											</div>
										</div>
									</div>
									</div>
									</div>
							</div>
							
					</div>
					<div style="margin-bottom:20px;">
					<div class="text-right">
						<input type="hidden" name="id" value="${companyLead.id}">
	                     <button type="submit" class="btn btn-success btn-block">Update Changes <b><i class="icon-circle-right2"></i></b></button>
	            	</div>
					</div>
					</form>	
					</div>
<!---------------------------------------------------------------------------------------------->
				<div class="col-md-2"> </div>
				<div class="col-md-10 col-xs-12 col-sm-12">
					<div class="container-detached">
						<div class="content-detached">

									<!-- Follow Up info -->
									<div class="panel panel-flat panel-bg-navy">
										<div class="panel-heading">
											<h6 class="panel-title">Follow Up<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="10" alt="Copy to clipboard" ></a></li>
							                	</ul>
						                	</div>
										</div>
										<hr >
										<div class="panel-body" style="display: block;">
														<form id="company-lead-followup-form" class="from-horizontal" method="post" enctype="multipart/form-data">
														<div class="row">
														<div class="col-md-2">
														<div class="form-group">
														<span data-toggle="tooltip" data-placement="top" title="Select Lead Status" >
															<select aria-hidden="true" name="leadStatus" id="lead-status" class="form-control selectpicker input-sm">
															  <option value="">Select Lead Status</option>
																<c:forEach items="${leadStatusMap}" var="lstatus">
																	<option value="${lstatus.key}" ${lstatus.key == companyLeadFollowUpData.leadStatus ? 'selected': ''}>${lstatus.value}</option>
																</c:forEach>
															 </select>
														</span>
														</div>
													  </div>
														<div class="col-md-2">
												  		<div class="form-group">
												  		<span data-toggle="tooltip" data-placement="top" title="Select Comment" >
															<select aria-hidden="true" name="title" id="title" class="form-control selectpicker input-sm">
															  <option value="">Add Title</option>
																<c:forEach items="${followupStatusMap}" var="title">
																	<option value="${title.key}" ${title.key == companyLeadFollowUpData.title ? 'selected': ''}>${title.value}</option>
																</c:forEach>
																<option value="lead_converted" ${ 'lead_converted' == companyLeadFollowUpData.leadStatus ? 'selected': ''}>Lead Converted</option>
															 </select>
														</span>
														</div>
														</div>
														<div class="col-md-3">
												  		<div class="form-group">
												  		<span data-toggle="tooltip" data-placement="top" title="Add Description" >
															<textarea name="note" rows="1" class="form-control input-sm" placeholder="Please add note...">${companyLeadFollowUpData.note}</textarea>
														</span>
														</div>
														</div>
														<div class="col-sm-2">
														<span data-toggle="tooltip" data-placement="top" title="Select Assign To User" >
													     <select name="assignTo" id="assign-to" class="form-control selectpicker" data-live-search="true">
															<option value="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="assign">
																		<option value="${assign.userId}" ${assign.userId == companyLeadFollowUpData.assignTo ? 'selected' : ''} >${assign.firstName} ${assign.lastName}</option>
															</c:forEach>
														
														</select>
														</span>
										    			</div>
														<div class="col-md-1">
														<div class="form-group">
														<span data-toggle="tooltip" data-placement="top" title="Select Lead Rating" >
															<select aria-hidden="true" name="leadRating" id="lead-rating" class="form-control selectpicker input-sm">
															  <option value="">Lead Rating</option>
																<c:forEach items="${leadRatingMap}" var="rating">
																	<option value="${rating.key}" ${rating.key == companyLeadFollowUpData.leadRating ? 'selected': ''}>${rating.value}</option>
																</c:forEach>
															 </select>
														</span>
														</div>
													</div>
													
													<div class="col-md-1">
														<div class="form-group">
															<button type="button" class="btn btn-sm btn-default btn-block" data-event="duplicate" data-toggle="modal" data-target="#set-reminder" style="padding: 5px 10px;" 
															 ${companyLeadFollowUpData.id != null?'':'disabled'}>Set Reminder</button>
														</div>
														</div>
														<div class="col-md-1">
														<div class="form-group">
														<button type="button" class="btn btn-default btn-sm btn-success btn-block" id="update-company-lead-followupBtn" data-form-name="company-lead-followup-form">Save</button>
														 <input type="hidden" name="id" value="${companyLeadFollowUpData.id}">
														 <input type="hidden" name="companyLeadId" value="${companyLead.id}">
														</div>
														</div>
														</div>
											</form>
										<!--follow up detail  -->
										<div class="col-md-12">
											<h5 class="subtitle subtitle-lined">Follow Up History</h5>
											<div class="row">
								<div class="col-sm-4">
								<div class="row row-minus">
									<div class="col-xs-6"><label class="form-control-label">Title</label></div>
									<div class="col-xs-6"><span class="text-uppercase">
									<c:choose>
									<c:when test="${companyLeadFollowUpData.title != null}">
									${companyLeadFollowUpData.title}
									</c:when>
									<c:otherwise>
									<span class="text-warning">N/A</span>
									</c:otherwise>
									</c:choose>
									</span></div>
								</div>
								<div class="row row-minus">
									<div class="col-xs-6"><label class="form-control-label">Lead Status</label></div>
									<div class="col-xs-6"><span class="text-uppercase">
									<c:choose>
										<c:when test="${companyLeadFollowUpData.leadStatus == 'New'}">
										<span class="bug-issue-status-max-width-medium follow-status-info">${companyLeadFollowUpData.leadStatus}</span>
										</c:when>						
										<c:when test="${companyLeadFollowUpData.leadStatus == 'Closed'}">
										<span class="bug-issue-status-max-width-medium follow-status-danger">${companyLeadFollowUpData.leadStatus}</span>
										</c:when>
										<c:otherwise>
											${companyLeadFollowUpData.leadStatus}
										</c:otherwise>					
										</c:choose>
									</span></div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="row row-minus">
									<div class="col-xs-6"><label class="form-control-label">Lead Rating</label></div>
									<div class="col-xs-6">
									<c:choose>
										<c:when test="${companyLeadFollowUpData.leadRating == 'Highest'}">
										<span class="">${companyLeadFollowUpData.leadRating}</span> <img class="clay" src="admin/img/svg/highest.svg" width="14" alt="Higest">
										</c:when>						
										<c:when test="${companyLeadFollowUpData.leadRating == 'High'}">
										<span class="">${companyLeadFollowUpData.leadRating}</span> <img class="clay" src="admin/img/svg/high.svg" width="14" alt="Higest">
										</c:when>						
										<c:when test="${companyLeadFollowUpData.leadRating == 'Medium'}">
										<span class="">${companyLeadFollowUpData.leadRating}</span> <img class="clay" src="admin/img/svg/medium.svg" width="14" alt="Higest">
										</c:when>						
										<c:when test="${companyLeadFollowUpData.leadRating == 'Low'}">
										<span class="">${companyLeadFollowUpData.leadRating}</span> <img class="clay" src="admin/img/svg/low.svg" width="14" alt="Higest">
										</c:when>						
										<c:when test="${companyLeadFollowUpData.leadRating == 'Lowest'}">
										<span class="">${companyLeadFollowUpData.leadRating}</span> <img class="clay" src="admin/img/svg/lowest.svg" width="14" alt="Higest">
										</c:when>	
										<c:otherwise>
										<span class="text-warning"> N/A</span>
										</c:otherwise>					
										</c:choose>
									</div>
								</div>
								
								<div class="row row-minus">
									<div class="col-xs-6"><label class="form-control-label">Assigned Date</label></div>
									<div class="col-xs-6">
										<c:choose>
											<c:when test="bugTracker.assignDate!=null">
												<fmt:formatDate value="${companyLeadFollowUpData.assignDate}"
													pattern="E, MMM dd, yyyy h:m a" />
											</c:when>
											<c:otherwise>
										<span class="text-warning">N/A</span>
										</c:otherwise>
										</c:choose>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="row row-minus">
									<div class="col-xs-6"><label class="form-control-label">Assignee By</label></div>
									<div class="col-xs-6">${companyLeadFollowUpData.assignedByName}</div>
								</div>
								<div class="row row-minus">
									<div class="col-xs-6"><label class="form-control-label">Assigned To</label></div>
									<div class="col-xs-6">${companyLeadFollowUpData.assignedToName}</div>
								</div>
							</div>
						</div>
										</div>
										<c:if test="${companyLeadFollowUpData.note != null && companyLeadFollowUpData.note != ''}">
										<div class="col-md-12">
											<h5 class="subtitle subtitle-lined mt-4">DESCRIPTION</h5>
												<p>${fn:replace(companyLeadFollowUpData.note, newLineChar, "</br>")}</p>
										</div>
										</c:if>
										<div class="col-md-12">
											<h5 class="subtitle subtitle-lined"></h5>
										</div>
									
										<!--follow up history template -->
							<div class="col-md-12">
									<ul class="nav nav-tabs nav-tabs-line" role="tablist">
							             <!--  <li class="nav-item active">
							              <a class="nav-link " data-toggle="tab" href="#all-changes" aria-controls="all-changes" role="tab" aria-expanded="true" aria-selected="true"><i class="icon wb-plugin" aria-hidden="true"></i>All</a>
							              </li> -->
							              <li class="nav-item active">
							              <a class="nav-link" data-toggle="tab" href="#history" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>History</a>
							              </li>
							              <li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#comment" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>Comment</a>
							              </li>
							              <li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#call-log" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>Log a Call</a>
							              </li>
							              <li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#mail-sent" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>Mail Sent</a>
							              </li>
							              <li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#reminder" aria-controls="#reminder" role="tab" aria-selected="false"><i class="icon wb-cloud" aria-hidden="true"></i>Reminders</a>
							              </li>
						            </ul>
						            <div class="panel-tab-body">
					              	<div class="tab-content">
					          <!--Follow Up History Tab  -->
					                <div class="tab-pane active" id="history" role="tabpanel">
					                 <div class="row">
									<div class="bug-tracker">
										<div class="bug-section">
											<div class="panel-group" id="accordion" role="tablist"
												aria-multiselectable="true">
												<s:iterator value="companyLeadFollowUpHistoryList" status="rowCount">
													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="headingOne">
															<h4 class="panel-title">
																<a role="button" class="bugtracker-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true">
																	${rowCount.count} <span><b>Date : </b> <small class="text-muted"> 
																		<fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" /></small></span>
																	<span><b>Lead Status : </b> ${leadStatus}</span>
																</a>
															</h4>
														</div>
														<div id="collapse" class="panel-collapse collapse" role="tabpanel">
															<div class="panel-body">
																<div class="row">
																	<div class="col-sm-6">
																		<div class="row">
																			<div class="col-xs-6">Title</div>
																			<div class="col-xs-6"><span class="text-capitalize">${title}</span></div>
																		</div>
																		<div class="row">
																			<div class="col-xs-6">Type</div>
																			<div class="col-xs-6"><span class="text-capitalize">${leadStatus}</span></div>
																		</div>
																		<div class="row">
																			<div class="col-xs-6">Priority</div>
																			<div class="col-xs-6"><span class="text-capitalize">${leadRating}</span></div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="row">
																			<div class="col-xs-6">Assignee</div>
																			<div class="col-xs-6">${assignedByName}</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-6">Assigned To</div>
																			<div class="col-xs-6">${assignedToName}</div>
																		</div>

																	</div>
																	<!-- col-sm-6 -->
																	<div class="col-md-12 mt-10">
																		<h5 class="subtitle subtitle-lined">Note</h5>
																		<p>${fn:replace(note, newLineChar, "</br>")}</p>
																	</div>
																</div>
																<div class="row">
																	<div class="col-sm-12 ">
																		<c:if test="comments!=null">
																			<div class="b-comments">
																				<p>
																					<b>Comments : </b> ${comments}
																				</p>
																			</div>
																		</c:if>
																		<c:if test="${filePath !=null}">
																			<div class="download">
																				<p>Download</p>
																				<p>
																					<a href="downloadBugTrackerHistoryFile?fileName=${filePath}" class="btn btn-success btn-xs">${filePath} </a>
																				</p>
																			</div>
																		</c:if>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</s:iterator>
											</div>
										</div>
									</div>
								</div>
					                </div>
					          <div class="tab-pane" id="comment" role="tabpanel">
							  <div class="card">
							  <div class="card-header">Add Comment</div>
							  <div class="card-body">
							  <div class="row">
										<form method="post" class="form-horizontal" name="myForm" id="companyLeadFollowCommentForm" enctype="multipart/form-data">
											 <input type="hidden" name="leadFollowUpId" id="" value="${companyLeadFollowUpData.id}">
												<div class="col-md-12">
												
												<label class="">To &nbsp;&nbsp;<span class="text-primary">this company</span></label>
													<div class="form-group mt-10">
															<textarea rows="5" cols="2" class="form-control input-sm" name="comments" placeholder="comments" required="required"></textarea>
													</div>
													<!-- <div class="form-group">
														<label for="uploadimage" class="col-sm-2 control-label">Upload
															File </label>
														<div class="col-sm-8">
															<div class="row">
																<div class="col-sm-6 file-upload">
																	<input type="file" id="filePath" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,image/*"
																		ng-file-select="onFileSelect($files)" name="bugTrackerHistory.filePath">
																</div>
																<div class="col-sm-6 ">
																	<div id="fileinfo">
																		<div id="fileError"></div>
																	</div>
																</div>
															</div>
														</div>
													</div> -->
												</div>
												<div class="col-4">
													<div class="form-group">
														<div class="col-xs-12 submitWrap">
															<button type="submit" id="save-follow-up-comment" class="btn btn-success" data-form-name="companyLeadFollowCommentForm">Save</button>
														</div>
													</div>
												</div>
										</form>
										</div>
										</div>  
										</div>
											<s:iterator value="companyLeadFollowUpCommentList" status="rowstatus">
													<ul class="media-list comment-list mt-10">
															<li class="media">
																<div class="media-body">
																<div class="dropdown pull-right mt-10">
																	  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
																	  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
																	  <ul class="dropdown-menu">
																	    <li class="action"><a href="#" data-event="duplicate" data-toggle="modal" data-target="#update_comment_${rowstatus.count}">Edit</a></li>
																	    <li class="action"><a href="#" class="delete-follow-up-comment" data-comment-id="${id}">Delete</a></li>
																	  </ul>
																	</div>
																	<h4>${createdByUserName}</h4>
																	<small class="text-muted">
																	Created At :<em><fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" /></em> 
																	<c:choose>
																	<c:when test="${updatedAt != null}">
																	-- Updated At:<em><fmt:formatDate value="${updatedAt}" pattern="E, MMM dd, yyyy h:m a" /></em>
																	</c:when>
																	<c:otherwise>
																	
																	</c:otherwise>
																	</c:choose>
																	
																	</small>
																	<p>${fn:replace(comments, newLineChar, "</br>")}</p>
																	<div class="row">
																		<div class="col-sm-12 ">
																			<c:if test="${filePath !=null}">
																				<div class="download">
																					<p>
																						<b>Download</b>
																					</p>
																					<p>
																						<a href="downloadBugTrackerHistoryFile?fileName=${filePath}" class="btn btn-success btn-xs">${filePath} </a>
																					</p>
																				</div>
																			</c:if>
																		</div>
																	</div>
																</div> 
															</li>
													</ul>
	<!--update follow up comment model-->
									<div class="modal fade" id="update_comment_${rowstatus.count}" role="dialog">
										<div class="modal-dialog modal-dialog-centered">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title text-center">Edit Comment</h4>
													<button type="button" class="close slds-modal__close" data-dismiss="modal">
													<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="20" alt="Copy to clipboard">
													</button>
												</div>
												<form id="update_commentForm_${rowstatus.count}" method="post" class="bv-form">
												<input type="hidden" name="commentId" id="comment-id" value="${id}">
												<input type="hidden" name="leadFollowUpId" value="${companyLeadFollowUpData.id}">
													<div class="modal-body">
													 <div class="row">
																<div class="col-md-12 mt-20">
																	<div class="controls">
																			<div class="form-group">
																			<textarea rows="5" cols="" name="comments" class="form-control input-sm">${comments}</textarea>
																			</div>
																	</div>
																</div>
															</div>
													</div>
													<div class="modal-footer">
														<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
														<button type="submit" class="btn btn-success update-followup-comment-btn" id="" data-modal-id="update_comment_${rowstatus.count}" data-form-name="update_commentForm_${rowstatus.count}">Save</button>
													</div>
												</form>
											</div>
										</div>
								</div>
									<!-- // update follow up comment-->
											</s:iterator>
					                </div>
<!--Follow Up Call Log Tab Panel  -->
			                <div class="tab-pane" id="call-log" role="tabpanel">
			                <a href="#" class="btn btn-sm btn-info" data-event="duplicate" data-toggle="modal" data-target="#follow-up-log-call"><i class="fa fa-plus"></i> Log a Call</a>
					                <s:if test="companyLeadFollowUpCallList.size > 0">
											<s:iterator value="companyLeadFollowUpCallList" status="rowstatus">
					               	 <ul class="media-list comment-list mt-10">
												<li class="media">
												<div class="dropdown pull-right mt-10">
												  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
												  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
												  <ul class="dropdown-menu">
												    <li class="action"><a href="#" data-event="duplicate" data-toggle="modal" data-target="#update_call_${rowstatus.count}">Edit</a></li>
												    <li class="action"><a href="#" class="delete-follow-up-call" data-call-id="${id}">Delete</a></li>
												    <li class="action"><a href="#">Change Date</a></li>
												    <!-- <li class="action"><a href="#">Edit Comments</a></li>
												    <li class="action"><a href="#">Change Status</a></li> -->
												  </ul>
												</div>
													<div class="media-body">
														<h4>${createdByUserName}
														<small class="">
															<fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" />
														</small></h4>
														<h5>${subject}</h5>
														<p>${comment}.</p>
													</div> 
												</li>
										</ul>
										<!--update follow up comment-->
									<div class="modal fade" id="update_call_${rowstatus.count}" role="dialog">
										<div class="modal-dialog modal-lg">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title text-center">Edit Call Log</h4>
													<button type="button" class="close slds-modal__close" data-dismiss="modal">
													<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="20" alt="Copy to clipboard">
													</button>
												</div>
												<form id="update_call_form_${rowstatus.count}" method="post" class="bv-form">
												<input type="hidden" name="callId" id="comment-id" value="${id}">
												<input type="hidden" name="leadFollowUpId" value="${companyLeadFollowUpData.id}">
													<div class="modal-body">
													 <div class="row">
													 <h4 class="slds-section__title slds-theme--shade" style="font-weight: 500">
																<span class="section-header-title slds-p-horizontal--small slds-truncate">Call Information</span></h4>
																<div class="col-md-6">
																<label class="form-control-label" for="comment"><span class="text-danger">*</span> Subject</label>
																	<div class="controls">
																			<div class="form-group">
																			<input type="text" name="subject" id="subject"  class="form-control input-sm" value="${subject}" required="required"/>
																			</div>
																	</div>
																</div>
																<div class="col-md-12">
																<label class="form-control-label" for="comment">Comment</label>
																	<div class="controls">
																			<div class="form-group">
																			<textarea rows="4" cols="" name="comment" class="form-control input-sm">${comment}</textarea>
																			</div>
																	</div>
																</div>
														</div>
														<div class="row">
																<h4 class="slds-section__title slds-theme--shade" style="font-weight: 500">
																<span class="section-header-title slds-p-horizontal--small slds-truncate">Additional Information</span></h4>
																<div class="row">
																<div class="col-md-6">
																<label class="form-control-label" for="comment"><span class="text-danger">*</span> Priority${priority}</label>
																	<div class="controls">
																			<div class="form-group">
																			<select name="priority" class="form-control input-sm">
																				<option value="None" selected="selected">--None--</option>
																				<option value="Normal" ${'Normal' == priority ? 'selected': ''}>Normal</option>
																				<option value="High"   ${'High' == priority ? 'selected': ''}>High</option>
																			</select>
																			</div>
																	</div>
																</div>
																</div>
																<div class="row">
																<div class="col-md-6">
																<label class="form-control-label" for="status"><span class="text-danger">*</span> Status</label>
																	<div class="controls">
																			<div class="form-group">
																			<select name="status" class="form-control input-sm">
																			    <option value="None" selected="selected">--None--</option>
																				<option value="Open"   ${'Open' == status ? 'selected': ''}>Open</option>
																				<option value="Completed" ${'Completed' == status ? 'selected': ''}>Completed</option>
																			</select>
																			</div>
																	</div>
																</div>
																</div>
																
														</div>
														<div class="row">
																<h4 class="slds-section__title slds-theme--shade" style="font-weight: 500">
																<span class="section-header-title slds-p-horizontal--small slds-truncate">System Information</span></h4>
														<div class="col-md-6">
														<label class="form-control-label" for="comment">Created By</label>
														<div class="controls">
														<h5>${createdByUserName} <small><fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy hh:mm a" /></small></h5>
														</div>
														</div>
														<div class="col-md-6">
														<label class="form-control-label" for="comment">Updated By</label>
														<div class="controls">
														<h5>${updatedByUserName} <small><fmt:formatDate value="${updatedAt}" pattern="E, MMM dd, yyyy hh:mm a" /></small></h5>
														</div>
														</div>
														
														</div>
												</div>
													<div class="modal-footer">
														<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
														<button type="submit" class="btn btn-success update-followup-call-btn" id="" data-modal-id="update_call_${rowstatus.count}" data-form-name="update_call_form_${rowstatus.count}">Save</button>
													</div>
												</form>
											</div>
					
										</div>
								</div>
									<!-- // update follow up comment-->
											</s:iterator>
										</s:if>
					                </div>
<!--Follow Up Email Tab-->
					                <div class="tab-pane" id="mail-sent" role="tabpanel">
					                <a href="#" class="btn btn-sm btn-info" data-event="duplicate" data-toggle="modal" data-backdrop="static" data-target="#send_company_lead_email"><i class="fa fa-plus"></i> Send Email</a>
					                <a href="#" class="btn btn-sm btn-info" data-event="duplicate" data-toggle="modal" data-backdrop="static" data-target="#send_company_lead_custom_email"><i class="fa fa-plus"></i> Compose</a>
					                 <s:iterator value="companyLeadFollowUpEmailList" status="rowstatus">
											<ul class="media-list comment-list mt-10">
															<li class="media">
																<div class="media-body">
																<div class="dropdown pull-right mt-10">
																	  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
																	  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
																	  <ul class="dropdown-menu">
																	    <li class="action"><a href="#" data-event="duplicate" data-toggle="modal" data-target="#update_comment_${rowstatus.count}">Edit</a></li>
																	    <li class="action"><a href="#" class="delete-follow-up-comment" data-comment-id="${id}">Delete</a></li>
																	  </ul>
																	</div>
																	<h4>${createdByUserName}</h4>
																	<small class="text-muted">
																	Created At :<em><fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" /></em> 
																	<c:choose>
																	<c:when test="${updatedAt != null}">
																	<b>--</b> Updated At:<em><fmt:formatDate value="${updatedAt}" pattern="E, MMM dd, yyyy h:m a" /></em>
																	</c:when>
																	<c:otherwise>
																	
																	</c:otherwise>
																	</c:choose>
																	
																	</small>
																	<p><strong>${emailSubject}.</strong></p>
																</div> 
															</li>
													</ul>
													</s:iterator>
													<!-- company lead followup email -->
													<div class="modal fade" id="send_company_lead_email" role="dialog">
													<div class="modal-dialog modal-lg">
														<!-- Modal content-->
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title">Send Email To Agent</h4>
																<button type="button" class="close slds-modal__close" data-dismiss="modal">
																	<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="14" alt="Copy to clipboard">
																</button>
															</div>
															<form id="sendEmailToAgentForm" method="post" class="bv-form">
																<div class="modal-body">
																	<div class="row">
																	<div class="col-md-12">
																	<div class="email-msg-alert"></div>
																	</div>
																	</div>
																	<div class="row">
																	<div class="col-md-12">
																		<label class="input-sm-label" for="firstName">Subject</label>
																	<div class="form-group">
																		<input type="text" name="emailSubject" id="email-subject" value="" class="form-control">
																	</div>
																	</div>
																	</div>
																	<div class="row">
																	<div class="col-md-12">
																		<label class="text-danger">Please Select A Template :</label>
																	</div>
																		<div class="form-group">
																		<div class="col-md-4">
																				<a href="#">
																					<img src="admin/img/email-template/email-template-1.jpg" class="img-fluid img-check" style="height: 320px">
																					<input type="radio" name="templateFlag" id="template-1" value="TEMPLATE_1" class="hidden" autocomplete="off">
																				</a>
																				<div class="mt-1 center">
																					<a href="admin/img/email-template/email-template-1.jpg" id="" class="open-image btn btn-xs btn-block btn-outline-primary">Preview</a>
																				</div>
																		</div>
																		<div class="col-md-4">
																				<a href="#">
																					<img src="admin/img/email-template/email-template-2.jpg" class="img-fluid img-check" style="height: 320px">
																					<input type="radio" name="templateFlag" id="template-2" value="TEMPLATE_2" class="hidden" autocomplete="off">
																				</a>
																				<div class="mt-1 center">
																					<a href="admin/img/email-template/email-template-2.jpg" id="" class="open-image btn btn-xs btn-block btn-outline-primary">Preview</a>
																				</div>
																		</div>
																		<div class="col-md-4">
																				<a href="#">
																				<img src="admin/img/email-template/email-template-3.png" class="img-fluid img-check" style="height: 320px">
																				<input type="radio" name="templateFlag" id="template-3" value="TEMPLATE_3" class="hidden" autocomplete="off">
																				</a>
																				<div class="mt-1 center">
																					<a href="admin/img/email-template/email-template-3.jpg" id="" class="open-image btn btn-xs btn-block btn-outline-primary">Preview</a>
																				</div>
																		</div>
																		</div>
																	</div>
																	<hr class="bottom-line">
																	<script>
																	$('.open-image').click(function (e) {
														                e.preventDefault();
														                alwaysShowClose: true,
														                $(this).ekkoLightbox();
														            });
																	
																	$(document).ready(function(e){
															    		
																	$('.img-check').click(function(e) 
																	{
															        	$('.img-check').not(this).removeClass('check').siblings('input').prop('checked',false);
															    		$(this).addClass('check').siblings('input').prop('checked',true);
															    	});
																		
																	});
																	</script>
																</div>
																<div class="modal-footer">
																	<input type="hidden" name="companyLeadIds" id="companyLead-ids" value="${param.id}">
																	<button type="button" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
																	<button type="button" class="btn btn-success" id="send_email_agent" data-model-id="send_company_lead_email" data-form-name="sendEmailToAgentForm">
																	Send <img class="clippy" src="admin/img/svg/sent-mail-w.svg" width="14"> </button>
																</div>
															</form>
														</div>
													</div>
									</div>
													<!-- company lead followup email custom-->
													<div class="modal fade" id="send_company_lead_custom_email" role="dialog">
													<div class="modal-dialog modal-lg">
														<!-- Modal content-->
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title text-center">Send Custom Email To Agent</h4>
																<button type="button" class="close slds-modal__close" data-dismiss="modal">
																	<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
																</button>
															</div>
															<form id="sendCustomEmailToAgentForm" method="post" class="bv-form">
																<div class="modal-body">
																	<div class="row">
																	<div class="col-md-12">
																	<div class="email-msg-alert"></div>
																	</div>
																	</div>
																	<div class="row">
																	<div class="col-md-12">
																		<label class="input-sm-label" for="firstName">Subject</label>
																	<div class="form-group">
																		<input type="text" name="emailSubject" id="email-subject" value="" class="form-control">
																	</div>
																	</div>
																	<div class="col-md-12">
																		<label class="input-sm-label" for="firstName">Body</label>
																		<div class="form-group">
																		 <textarea id="summernote" name="smtdescription"></textarea>
																		</div>
																	</div>
																	</div>
																	<script>
																		$(document).ready(function() {
																		  $('#summernote').summernote();
																		});
																	</script>
																</div>
																<div class="modal-footer">
																	<input type="hidden" name="companyLeadId" id="companyLead-ids" value="${param.id}">
																	<input type="hidden" name="templateFlag" id="custom-email" value="CUSTOM">
																	<button type="button" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
																	<button type="button" class="btn btn-success" id="send_custom_email_agent">
																	Send <img class="clippy" src="admin/img/svg/sent-mail-w.svg" width="14"> </button>
																</div>
															</form>
														</div>
													</div>
									</div>
					                </div>
<!-- follow up reminder tab panel -->
					             <div class="tab-pane" id="reminder" role="tabpanel">
					                 <div class="">
					                 <div class="btn-group pull-left">
					                 <a href="#" class="btn btn-sm btn-info" data-event="duplicate" data-toggle="modal" data-target="#set-reminder"><i class="fa fa-plus"></i> Reminder</a>
					                 </div>
					                 <br/>
					                 <div class="row">
												<s:iterator value="companyLeadFollowUpReminderList" status="rowCount">
													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="headingOne">
															<h4 class="panel-title">
																<a role="button" class="bugtracker-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_${rowCount.count}" aria-expanded="true">
																	${rowCount.count} <span><b>${reminderType} : </b> <small class="text-muted"> 
																		<fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" /></small></span>
																</a>
															</h4>
														</div>
														<div id="collapse_${rowCount.count}" class="panel-collapse collapse" role="tabpanel">
															<div class="panel-body">
																<div class="row mt-1 mb-1">
																	<div class="col-sm-4">
																		<div class="row">
																			<div class="col-xs-5"><b>Reminder Type</b></div>
																			<div class="col-xs-7"><span class="text-capitalize">${reminderType}</span></div>
																		</div>
																		<div class="row">
																			<div class="col-xs-5"><b>Title</b></div>
																			<div class="col-xs-7"><span class="text-capitalize">${title}</span></div>
																		</div>
																	</div>
																	<div class="col-sm-8">
																		<div class="row">
																			<div class="col-xs-5"><b>Created By</b></div>
																			<div class="col-xs-7">${createdByName}</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-5"><b>Date Time</b></div>
																			<div class="col-xs-7">${spysr:formatDate(reminderDate,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}</div>
																		</div>
																	</div>
																	
																</div>
															</div>
														</div>
													</div>
												</s:iterator>
												</div>
								</div>
					                </div>
					             <!--/ follow up reminder tab panel -->
					              </div>
					             <!--/ follow up tab panel  -->
				           		 </div>
								</div>						
								<!--/follow up history template -->
								</div>
								</div>
									<!-- /profile info -->
							</div>
						</div>
<!---------------------------------------------------------------------------------------------->
						
					</div>
				</div>
			</section>
						<div id="models-windows">
						<div class="modal fade" id="set-reminder" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-simple-header">
										<h4 class="modal-title text-center">Set Reminder</h4>
										<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
										</button>
									</div>
									<form id="saveFollowUpReminderForm" method="post" class="bv-form">
										<div class="modal-body">
										 <div class="row">
											<div class="col-md-12">
												<label class="form-control-label" for="title">Reminder Type</label>
												<div class="controls">
														<div class="form-group">
															<select name="reminderType" id="reminder-type" class="form-control">
																<option value="" >Select</option>
																<option value="SELF" >Self</option>
																<option value="CUSTOMER" >Customer</option>
															</select>
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<label class="form-control-label" for="title">Title</label>
												<div class="controls">
														<div class="form-group">
															<input type="text" name="title" id="title"  class="form-control" value=""/>
															<span class="help-msg"></span>
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<label class="form-control-label" for="title">Description</label>
												<div class="controls">
														<div class="form-group">
															<textarea  name="description" id="description"  class="form-control" rows="4" /></textarea>
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<label class="form-control-label" for="datetimepicker-inline">Set Date & Time</label>
												<div class="controls">
														<div class="form-group">
															<input type="text" name="reminderDateFlag" id="datetimepicker-inline"  class="form-control" value=""/>
														</div>
												</div>
											</div>
										</div>
										<div class="row">
										<div class="col-md-12">
											<label class="form-control-label" for="datetimepicker-inline">Reminder On Service</label>
										</div>
										</div>
										<div class="row">
										<div class="col-md-3">
														<div class="form-group">
														<div class="checkbox checkbox-success">
															<input name="onEmail" id="on-email" type="checkbox" value="true" checked="checked"> 
															<label for="on-email" class="text-danger"> On Email</label>
														</div>
													</div>
												</div>
												<div class="col-md-3">
														<div class="form-group">
														<div class="checkbox checkbox-success">
															<input name="onNotification" id="on-notify" type="checkbox" value="false"> 
															<label for="on-notify" class="text-danger"> On Notify</label>
														</div>
													</div>
												</div>
												<div class="col-md-3">
														<div class="form-group">
														<div class="checkbox checkbox-success">
															<input name="onSms" id="on-sms" type="checkbox" value="false"> 
															<label for="on-sms" class="text-danger"> On SMS</label>
														</div>
													</div>
												</div>
												<div class="col-md-3">
														<div class="form-group">
														<div class="checkbox checkbox-success">
															<input name="onphone" id="on-phone" type="checkbox" value="false"> 
															<label for="on-phone" class="text-danger"> On Phone</label>
														</div>
													</div>
												</div>
												
										</div>
									</div>
										<div class="modal-simple-footer">
											<input type="hidden" name="followUpId" value="${companyLeadFollowUpData.id}">
											<input type="hidden" name="companyLeadId" value="">
											<button type="button" class="btn btn-success" id="save_lead_follow_reminder_btn">Save</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="modal fade" id="follow-up-log-call" role="dialog">
											<div class="modal-dialog modal-side modal-bottom-right">
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title text-center">Log a Call</h4>
														<button type="button" class="close slds-modal__close" data-dismiss="modal">
														<img class="" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
														</button>
													</div>
													<form method="post" class="form-horizontal" name="myForm" id="lead-followup-call-form">
														<div class="modal-body">
											                <div class="row">
											                <div class="col-md-12">
																<label class="form-control-label" for="title"><span class="text-danger">*</span> Subject</label>
																<div class="controls"><div class="form-group"><div class="">
																		<input type="text" name="subject" id="subject"  class="form-control input-sm" required="required"/></div></div>
																</div>
															</div>
											                <div class="col-md-12">
																<label class="form-control-label" for="comment">Comment</label>
																<div class="controls"><div class="form-group">
																		<textarea name="comment" id="comment" class="form-control input-sm" rows="4"></textarea>
																		</div>
																</div>
															</div>
											                </div>
													</div>
														<div class="modal-footer">
														<input type="hidden" name="leadFollowUpId" value="${companyLeadFollowUpData.id}">
															<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
															<button type="submit" class="btn btn-success" id="save-follow-up-call" data-form-name="lead-followup-call-form">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
														</div>
													</form>
													
												</div>
						
											</div>
										</div>
						</div>
							<!-- email history popover -->
										<div id="popover-content" class="hidden">
								        <div class="row">
								        <table class="table table-bordered table-striped-column table-hover mb-0">
						                               <thead>
						                               <tr class="small-trip-table">
											<th>SNo</th>
											<th>Mail Status</th>
											<th>Send At</th>
											</tr>
										</thead>
											<tbody>
											<c:forEach items="${emailCountStatusList}" var="emailData" varStatus="estatus">
											<tr class="small-trip-table">
											<td>${estatus.count}</td>
											<td>
											<c:choose>
											<c:when test="${emailData.mailStatus == 1}">
											<span class="text-success">Success</span>
											</c:when>
											<c:when test="${emailData.mailStatus == 0}">
											<span class="text-warning">Pending</span>
											</c:when>
											<c:when test="${emailData.mailStatus == -1}">
											<span class="text-danger">Failed</span>
											</c:when>
											</c:choose>
											</td>
											<td>
											${spysr:formatDate(emailData.createdDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy hh:mm a')}
											</td>
											</tr>
											</c:forEach>
										</tbody></table>
								        </div>
				</div>
        <!--ADMIN AREA ENDS-->
   <script type="text/javascript">
        $(function () {
            $('#datetimepicker-inline').datetimepicker({
                inline: false,
                sideBySide: true
            });
        });
    </script>
  <script>
  $(document).ready(function()
		  {
		    $("#updateCompanyLeadForm").submit(function(e) {
		    notifySavingMsg();
		  	e.preventDefault();	
		  		$.ajax({
		  			url : "updateCompanyLeadProfile",
		  			type : "POST",
		  			dataType: 'json',
		  			data : $("#updateCompanyLeadForm").serialize(),
		  			success : function(jsonData) {
		  				if(jsonData.json.status == 'success'){
							alertify.success(jsonData.json.message);;}
		  				else if(jsonData.json.status == 'error'){
		  					alertify.error(jsonData.message.message);}
						setTimeout(location.reload.bind(location), 1000);
		  			},
		  			error: function (request, status, error) {
		  				showModalPopUp("Company Lead can not be Updated.","e");
		  			}
		  		});
		  	});

		    $("#update-company-lead-followupBtn").click(function(e) {
				e.preventDefault();
				var formDeleteName = $(this).attr("data-form-name");
				showModalPopUp("Updating Details, Please wait ..", "s");
				$.ajax({
					url : "updateCompanyLeadFollowUp",
					type : "POST",
					dataType : 'json',
					data : $("#" + formDeleteName).serialize(),
					success : function(jsonData) {
						if(jsonData.message.status == 'success'){
							alertify.success(jsonData.message.message);;}
		  				else if(jsonData.message.status == 'error'){
		  					alertify.error(jsonData.message.message);}
						setTimeout(location.reload.bind(location), 1000);
					},
					error : function(request, status, error) {
						showModalPopUp("Travel My Lead   can not be updated.", "e");
					}
				});
			});
			$("#save-follow-up-comment").click(function(e) {
				e.preventDefault();
				var formDeleteName = $(this).attr("data-form-name");
				$.ajax({
					url : "saveCompanyLeadFollowUpComment",
					type : "POST",
					dataType : 'json',
					data : $("#" + formDeleteName).serialize(),
					success : function(jsonData) {
						if(jsonData.message.status == 'success'){
							alertify.success(jsonData.message.message);;}
		  				else if(jsonData.message.status == 'error'){
		  					alertify.error(jsonData.message.message);}
						setTimeout(location.reload.bind(location), 1000);
					},
					error : function(request, status, error) {
						alertify.error("follow up comment can't be updated,please try again");
					}
				});
			});
			$("#save-follow-up-call").click(function(e) {
				e.preventDefault();
				var formDeleteName = $(this).attr("data-form-name");
				$.ajax({
					url : "saveCompanyLeadFollowUpCall",
					type : "POST",
					dataType : 'json',
					data : $("#" + formDeleteName).serialize(),
					success : function(jsonData) {
						if(jsonData.message.status == 'success'){
							alertify.success(jsonData.message.message);}
		  				else if(jsonData.message.status == 'error'){
		  					alertify.error(jsonData.message.message);}
						setTimeout(location.reload.bind(location), 1000);
					},
					error : function(request, status, error) {
						alertify.error("follow up call can't be updated,please try again");
					}
				});
			});
			$(".update-followup-comment-btn").click(function(e) {
				e.preventDefault();
				var formDeleteName = $(this).attr("data-form-name");
				var modalId = $(this).attr("data-modal-id");
				$.ajax({
					url : "updateCompanyLeadFollowUpComment",
					type : "POST",
					dataType : 'json',
					data : $("#" + formDeleteName).serialize(),
					success : function(jsonData) {
						if(jsonData.message.status == 'success'){
							$("#" + modalId).hide();
							alertify.success(jsonData.message.message);}
		  				else if(jsonData.message.status == 'error'){
		  					$("#" + modalId).hide();
		  					alertify.error(jsonData.message.message);}
						setTimeout(location.reload.bind(location), 1000);
					},
					error : function(request, status, error) {
						alertify.error("follow up call can't be updated,please try again");
					}
				});
			});
			$(".delete-follow-up-comment").click(function(e) {
					// confirm dialog
					var commentId = $(this).data("comment-id");
					 alertify.confirm("Are you sure delete this comment", function () {
							$.ajax({
								url : "deleteCompanyLeadFollowUpComment?id="+commentId,
								type : "GET",
								dataType: 'json',
								success : function(jsonData) {
									if(jsonData.message.status == 'success'){
										alertify.success(jsonData.message.message);}
					  				else if(jsonData.message.status == 'error'){
					  					alertify.error(jsonData.message.message);}
									setTimeout(location.reload.bind(location), 1000);
								},
								error: function (request, status, error) {
									alertify.error("Comment  can not be deleted.");
								}
							});
					 }, function() {
						 alertify.error("You've clicked Cancel");
					 });
			});
			$(".update-followup-call-btn").click(function(e) {
				e.preventDefault();
				var formDeleteName = $(this).attr("data-form-name");
				var modalId = $(this).attr("data-modal-id");
				$.ajax({
					url : "updateCompanyLeadFollowUpCall",
					type : "POST",
					dataType : 'json',
					data : $("#" + formDeleteName).serialize(),
					success : function(jsonData) {
						if(jsonData.message.status == 'success'){
							$("#" + modalId).hide();
							alertify.success(jsonData.message.message);}
		  				else if(jsonData.message.status == 'error'){
		  					$("#" + modalId).hide();
		  					alertify.error(jsonData.message.message);}
						setTimeout(location.reload.bind(location), 1000);
					},
					error : function(request, status, error) {
						alertify.error("follow up call can't be updated,please try again");
					}
				});
			});
			$(".delete-follow-up-call").click(function(e) {
					// confirm dialog
					var callId = $(this).data("call-id");
					 alertify.confirm("Are you sure delete this comment", function () {
							$.ajax({
								url : "deleteCompanyLeadFollowUpCall?id="+callId,
								type : "GET",
								dataType: 'json',
								success : function(jsonData) {
									if(jsonData.message.status == 'success'){
										alertify.success(jsonData.message.message);}
					  				else if(jsonData.message.status == 'error'){
					  					alertify.error(jsonData.message.message);}
									setTimeout(location.reload.bind(location), 1000);
								},
								error: function (request, status, error) {
									alertify.error("Comment  can not be deleted.");
								}
							});
					 }, function() {
						 alertify.error("You've clicked Cancel");
					 });
			});
			
			$("#sent-follow-up-email").click(function(e) {
				e.preventDefault();
				var formDeleteName = $(this).attr("data-form-name");
				$.ajax({
					url : "saveCompanyLeadFollowUpEmail",
					type : "POST",
					dataType : 'json',
					data : $("#" + formDeleteName).serialize(),
					success : function(jsonData) {
						if(jsonData.message.status == 'success'){
							alertify.success(jsonData.message.message);}
		  				else if(jsonData.message.status == 'error'){
		  					alertify.error(jsonData.message.message);}
						setTimeout(location.reload.bind(location), 1000);
					},
					error : function(request, status, error) {
						alertify.error("follow up email can't be updated,please try again");
					}
				});
			});
		});
  
  /*-------------------------------------------*/
	function companyProfileUploadFn(companyId){
			 var formData = new FormData($("#imageUploadForm")[0]);
			    $.ajax({
			        url: "uploadCompanyLeadProfileImage?companyLeadId="+companyId,
			        type: 'POST',
			        data: formData,
			        async: false,
			        dataType: 'json',
			        success: function (jsonData) 
			        {
			        	if(jsonData.json.status == 'success'){
							$.notify({icon: 'fa fa-check',message: jsonData.json.message},{type: 'success'});
							window.location.reload();
							}
		  				else if(jsonData.message.status == 'error')
		  				{
		  					$.notify({icon: 'fa fa-times',message: jsonData.json.message},{type: 'danger'});
		  					}
			        	
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });	
		}
  /*----------------------------------------------------------*/
  $('#send_email_agent').on("click", function(event){
			 $.ajax({
						url : "send_company_lead_follow_email",
						type : "POST",
						dataType: 'json',
						data : $("#sendEmailToAgentForm").serialize(),
						success : function(jsonData) {
							if(jsonData.message.status == 'success'){
								$.notify({icon: 'fa fa-check',message: jsonData.message.message},{type: 'success'});
								window.location.reload(1000);
							}
			  				else if(jsonData.message.status == 'error'){
			  					$(".email-msg-alert").html('<div class="msg-alert-danger msg-danger msg-danger-text"> <i class="fa fa-times"></i><span class="error-msg">&nbsp;'+jsonData.message.message+'</span></div>');
			  				}
						},
						error: function (request, status, error) {
							alertify.error("Something problem saving data,Try again");
						}
				}); 
});
  $('#send_custom_email_agent').on("click", function(event){
			 $.ajax({
						url : "send_follow_up_custom_email",
						type : "POST",
						dataType: 'json',
						data : $("#sendCustomEmailToAgentForm").serialize(),
						success : function(jsonData) {
							if(jsonData.message.status == 'success'){
								$.notify({icon: 'fa fa-check',message: jsonData.message.message},{type: 'success'});
								window.location.reload(1000);
							}
			  				else if(jsonData.message.status == 'error'){
			  					$(".email-msg-alert").html('<div class="msg-alert-danger msg-danger msg-danger-text"> <i class="fa fa-times"></i><span class="error-msg">&nbsp;'+jsonData.message.message+'</span></div>');
			  				}
						},
						error: function (request, status, error) {
							alertify.error("Something problem saving data,Try again");
						}
				}); 
});
  
  $('#save_lead_follow_reminder_btn').on("click", function(event){
			 $.ajax({
						url : "set_company_lead_reminder",
						type : "POST",
						dataType: 'json',
						data : $("#saveFollowUpReminderForm").serialize(),
						success : function(jsonData) {
							if(jsonData.message.status == 'success'){
								$.notify({icon: 'fa fa-check',message: jsonData.message.message},{type: 'success'});
								window.location.reload(1000);
							}
			  				else if(jsonData.message.status == 'error'){
			  					$(".email-msg-alert").html('<div class="msg-alert msg-danger msg-danger-text"> <i class="fa fa-times"></i><span class="error-msg">&nbsp;'+jsonData.message.message+'</span></div>');
			  				}
						},
						error: function (request, status, error) {
							alertify.error("Something problem saving data,Try again");
						}
				}); 
});
  </script>
