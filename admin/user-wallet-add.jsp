<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="dj" uri="/struts-dojo-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<style>
.modal-footer {
    border-top: 0px solid rgb(221, 219, 218);
    border-bottom-right-radius: 0.25rem;
    border-bottom-left-radius: .25rem;
    padding: .75rem 1rem;
    background-color: rgb(255, 255, 255);
    text-align: right;
    box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.16);
}
</style>
<section class="wrapper container-fluid">
	<div class="row">
	<div class="pnl">
					<div class="hd clearfix">
					<h5 class="pull-left"><img class="clippy" src="admin/img/svg/user.svg" width="22" alt="Wallet"> <span class="link text-capitalize"><a href="userEdit?id=${userProfile.id}">${userProfile.firstName}</a></span> Wallet</h5>
					<div class="set pull-right">
					<a class="btn btn-xs btn-primary" href="getUserWalletTransactionHistory?userId=${userProfile.id}"><img class="clippy" src="admin/img/svg/history-clock.svg" width="13"> Wallet History </a>
					<a class="btn btn-xs btn-primary" href="userList"><span class="fa fa-arrow-left"></span> Back To List</a>
					</div>
					<div class="set pull-right" style="font-size: 15px;">
						<span class=""><b>Current Balance:&nbsp;</b>
						<c:choose>
						<c:when test="${userProfile.userWallet != null}">
						<small><span class="follow-status-success text-success"><i class="fa fa-inr" style="font-size:12px"></i> <fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userProfile.userWallet.walletBalance}" /> </span></small>
						</c:when>
						<c:otherwise>
						0.00
						</c:otherwise>
						</c:choose>
						</span> 
					</div>
					</div>
	</div>
<div class="mt-5">
	<div class="container">
		<h4 class="text-center p-4">Add <span class="text-capitalize">${userProfile.firstName}</span> Wallet</h4>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form id="saveUserWalletForm" method="post" class="spy-form-horizontal" name="saveUserWalletForm">
					<div class="form-group">
						<label for="pwd">Wallet Type</label>
						 <select class="form-control input-sm" name="userWalletTransferHistory.walletType" id="wallet-type">
						 <option value="" selected="selected">Select Type</option>
						 <option value="oxyMoney">Oxy Money</option>
						 <option value="oxyBack">Oxy Back</option>
						 <option value="oxyCredit">Oxy Credit</option>
						</select>
					</div>
					<div class="" id="oxy-credit">
					<div class="form-group">
						<label for="email">Oxy Credit</label> 
						<input type="text" class="form-control" name="creditBalance" id="credit_balance" autocomplete="off" required value="0"> 
					</div>
					</div>
					<div class="hidden" id="cashback-amount">
					<div class="form-group">
						<label for="email">Oxy Back Amount</label> 
						<input type="text" class="form-control" name="cashbackBalance" id="cashback_balance" autocomplete="off" required value="0"> 
					</div>
					</div>
					<div class="hidden" id="oxy-money">
					<div class="form-group">
						<label for="email">Oxy Money</label> 
						<input type="text" class="form-control" name="depositBalance" id="deposit_balance" autocomplete="off" required value="0"> 
					</div>
					</div>
					<div class="form-group hidden">
						<label for="pwd">Currency</label>
						 <select class="form-control" name="currencyCode" id="currency" required>
							<option selected value="<s:property value="%{#session.User.agentWallet.currencyCode}"/>"><s:property value="%{#session.User.agentWallet.currencyCode}" /></option>
						</select>
					</div>
					<div class="form-group">
						<label for="pwd">Transaction Type </label> 
						<select class="form-control" name="userWalletTransferHistory.transactionType" id="transactionType" required>
							<option selected value="Credit"><s:text name="tgi.label.credit" /></option>
							<option value="Debit"><s:text name="tgi.label.debit" /></option>
						</select>
					</div>
					<div class="form-group">
						<label for="pwd">Expiry Date</label>
						<input type="text" name="userWalletTransferHistory.expiryDate" id="expiry-date" value="" class="form-control">
					</div>
					<div class="form-group mb-5">
					<div class="pull-right">
						<a class="btn btn-xs btn-warning filter-link filterBtn" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false">More <i class="fa fa-angle-down"></i></a>
					</div>
					</div>
					<div class="mt-3" id="filterDiv" style="display: none">
					<div class="form-group">
						<label for="pwd">Refferal Code</label> 
						<input type="text" name="userWalletTransferHistory.refferalCode" id="refferal-code" class="form-control">
					</div>
					<div class="form-group">
						<label for="pwd">Refferal Type</label> 
						<input type="text" name="userWalletTransferHistory.refferalType" id="refferal-type" class="form-control">
					</div>
					</div>
					<input type="hidden" name="id" id="id" value="${userProfile.userWallet.id}">
					<input type="hidden" name="userId" id="user-id" value="${userId}">
					<button type="button" onclick="javascript:showPopUp();" class="btn btn-primary btn-block">Add Wallet</button>
				<!-- 	<button type="submit" id="save-user-wallet-btn" class="btn btn-primary btn-block">Add Wallet</button> -->
				</form>
			</div>
		</div>
		
		<div class="modal fade" id="updateBoxWallet" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header" style="border-bottom: 0px solid #e5e5e5;">
								<h4 class="modal-title text-center">Enter Password</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
									<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
								</button>
							</div>
							<form id="saveActivityTypeForm" method="post" class="bv-form">
								<div class="modal-body">
									<div class="row">
										<div class="col-md-12">
											<label class="form-control-label" for="formGroupInputSmall">Password</label>
											<div class="controls">
												<div class="form-group">
													<input type="password" placeholder="Password" id="Password_1" class="form-control" name="Password_1">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;" onclick="javascript:userWalletVerifyByPassword();">Cancel</button>
									<button type="submit" class="btn btn-primary" id="save-activity-type-btn">Save</button>
								</div>
							</form>
						</div>
					</div>
				</div>
	</div>
</div>
</div>
</section>
<script>
$(function () {
    $('#expiry-date').datetimepicker({
    	format: 'MM/DD/YYYY hh:mm A',
    	sideBySide : true,
    	
    }).on('dp.hide', function() {
        $(this).blur();
    });
 });
/*----------------------------------------------------------------------------*/
$(document).ready(function(){$(".filterBtn").click(function(){$("#filterDiv").toggle(500)}),
$("div.easy-autocomplete").removeAttr("style"),$("#configreset").click(function(){$("#resetform")[0].reset()})}),
$(".filter-link").click(function(){var e=$(this).find("i").hasClass("fa-angle-up");$(".filter-link").find("i").removeClass("fa-angle-down"),
$(".filter-link").find("i").addClass("fa-angle-up"),e&&$(this).find("i").toggleClass("fa-angle-up fa-2x fa-angle-down fa-2x")}); 
/*----------------------------------------------------------------------------*/
$('#wallet-type').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    if(valueSelected == 'oxyCredit'){
    	$("#oxy-credit").removeClass('hidden');
    	$("#cashback-amount").addClass('hidden');
    	$("#oxy-money").addClass('hidden');
    }
    else if(valueSelected == 'oxyBack'){
    	$("#cashback-amount").removeClass('hidden');
    	$("#oxy-credit").addClass('hidden');
    	$("#oxy-money").addClass('hidden');
    }
    else if(valueSelected == 'oxyMoney'){
    	$("#oxy-money").removeClass('hidden');
    	$("#oxy-credit").addClass('hidden');
    	$("#cashback-amount").addClass('hidden');
    }
    
});

/*----------------------------------------------------------------------------*/
	function userWalletVerifyByPassword() {
		var password = $("#Password_1").val();
		console.debug(password);
		$("#Password").val(password);
		$("#addWalletForm").submit();
	}
	function showPopUp() {
		if ($("#credit_balance").val() == null || $("#credit_balance").val() == "") {
			alert("Enter Oxy Money")
		}
		else if ($("#cashback_balance").val() == null || $("#cashback_balance").val() == "") {
			alert("Enter Cashback Amount")
		}
		else if ($("#deposit_balance").val() == null || $("#deposit_balance").val() == "") {
			alert("Enter Cashback Amount")
		}
		else {
			$('#updateBoxWallet').modal({
				show : 'true'
			});
		}
	}
/*----------------------------------------------------------------------------*/
$(document).ready(function() {
    $("#saveUserWalletForm").bootstrapValidator({
        feedbackIcons: {
            valid: "fa fa-check",
            invalid: "fa fa-remove",
            validating: "fa fa-refresh"
        },
        fields:{
        		walletType:{message:"Wallet Type is not valid",validators:{notEmpty:{message:"Please select wallet type"}}},
        		transactionType:{message:"Transaction Type is not valid",validators:{notEmpty:{message:"Please select transaction type"}}}
        	}
        
    }).on("error.form.bv", function(e) {}).on("success.form.bv", function(e) {
    	e.preventDefault();
    	notifySuccess();
            $.ajax({
                url: "add_user_wallet_money",
                type: "POST",
                dataType: "json",
                data: $("#saveUserWalletForm").serialize(),
                success: function(jsonData) {
                	if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);
                	}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message);
	  					
	  				}
                        $("#save-user-wallet-btn").removeAttr("disabled"), console.debug("button disabled ");
                    var s = $(e.target);
                    s.data("bootstrapValidator");
                    s.bootstrapValidator("disableSubmitButtons", !1).bootstrapValidator("resetForm", !0)
                },
                error: function(e, a, s) {
                    showModalPopUp("Somthing Problem, User Wallet can not be Saved.", "e")
                }
            })
    }).on("status.field.bv", function(e, a) {
        a.bv.getSubmitButton() && (console.debug("button disabled "), a.bv.disableSubmitButtons(!1))
    })
}); 
 
</script>
