<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>

<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.show_date_to_user {
    padding: 0px 15px;
    position: relative;
    width: 93%;
    top: -25px;
    left: 5px;
    pointer-events: none;
    display: inherit;
    color: #000;
}
</style>

<section class="wrapper container-fluid">

	<div class="">
		<div class="">
			<div class="card1">
			<div class="pnl">
			<div class="hd clearfix">
					<h5 class="pull-left">
						Hotel Commission Report
					</h5>
					<div class="set pull-left">
						<!--   <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
					</div>
							
			 						<div class="set pull-right">
                                    <div class="dropdown">
                                       <div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="hotelReportList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.report_list" /> </a></li>
									<li class=""><a class="" href="hotelOrdersList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.order_list" /> </a></li>
									<li class=""><a class="" href="hotelCommissionReport"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_report" /></a></li>
									<li class=""><a class="" href="hotelCustomerInvoiceList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.custommer_invoice" /> </a></li>
									<li class=""><a class="" href="hotelAgentCommInvoiceList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_invoice" /></a></li>
								</ul>
								</div>
                                    </div>
                               </div>
                                 <div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
                               </div>
                               <br/>
                 <form action="hotelCommissionReport" class="filter-form" id="resetform">
					<div class="" id="filterDiv" style="display: none;">
						 <s:if test="%{#session.Company!=null}">
         						<s:if test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
                                 <div class="col-md-2">
                                 <div class="form-group">
                                <input type="hidden" id="HdncompanyTypeShowData" value="" />
                                <select name="companyTypeShowData" id="companyTypeShowData" class="form-control input-sm">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="my"><s:text name="tgi.label.my_self" /></option>
                                <s:if test="%{#session.Company!=null}">
         						<s:if test="%{#session.Company.companyRole.isSuperUser()}">
                                 <option value="distributor"><s:text name="tgi.label.distributor" /></option>
                                 </s:if>
                                </s:if>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div></div>
                                </s:if>
                                </s:if>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="orderId" id="orderid-json"
									placeholder="Search By Order Id...."
									class="form-control search-query input-sm" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="bookingStartDate" id="bookingStartDate"
												placeholder="Start Booking Date...."
												class="form-control search-query date1 input-sm " />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="bookingEndDate" id="bookingEndDate"
												placeholder="End Booking Date...."
												class="form-control search-query date2 input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="travelStartDate" id="travelStartDate"
												placeholder="CheckIn Date...."
												class="form-control search-query date3 input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="travelEndDate" id="travelEndDate"
												placeholder="CheckIn Date........"
												class="form-control search-query date4 input-sm" />
									</div>
								</div>
								
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="hotelName" id="hotelName-json"
									placeholder="Hotel Name" class="form-control search-query input-sm" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="country" id="hotelName-json"
									placeholder="Hotel Country" class="form-control search-query input-sm" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="firstName" id="customerFirstName"
												placeholder="Guest FirstName..."
												class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="lastName" id="customerLastName"
												placeholder="Guest LastName..."
												class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="email" id="customerEmail"
												placeholder="Guest Email"
												class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="mobile" id="customerMobile"
												placeholder="Guest Mobile"
												class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="createdBy" id="agency-json"
									placeholder="Agency..." class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="supplier" id="agency-json"
									placeholder="Hotel Supplier..." class="form-control search-query input-sm" />
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<select name="paymentMode" id="paymentMode" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_payment_method" /></option>
                                <option value="ALL"><s:text name="tgi.label.all" /></option>
                             	<option value="CHECK"><s:text name="tgi.label.CHECK" /></option>
                                <option value="CREDIT_CARD"><s:text name="tgi.label.CREDIT_CARD" /></option>
                                <option value="DEBIT_CARD"><s:text name="tgi.label.DEBIT_CARD" /></option>
                                <option value="WALLET"><s:text name="tgi.label.WALLET" /></option>
                                </select>
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<select name="paymentModeType" id="paymentModeType" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_payment_mode" /></option>
								<option value="ALL"><s:text name="tgi.label.all" /></option>
                                <option value="SQUAREUP" ><s:text name="tgi.label.squareUp" /></option>
                             	<option value="EXTERNALLINK_TEST"><s:text name="tgi.label.external" /></option>
                                <option value="AUTHORIZE"><s:text name="tgi.label.authorize" /></option>
                                <option value="WALLET"><s:text name="tgi.label.WALLET" /></option>
                                <option value="WIRECARD"><s:text name="tgi.label.wirecard" /></option>
                                <option value="STRIPE"><s:text name="tgi.label.stripe" /></option>
                                </select>
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<select name="paymentStatus" id="paymentStatus" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_payment_status" /></option>
								<option value="ALL"><s:text name="tgi.label.all" /></option>
                                <option value="PENDING" ><s:text name="tgi.label.PENDING" /></option>
                             	<option value="SUCCESS"><s:text name="tgi.label.SUCCESS" /></option>
                                <option value="FAILED"><s:text name="tgi.label.FAILED" /></option>
                                <option value="PROCESSING"><s:text name="tgi.label.PROCESSING" /></option>
                                <option value="DECLINED"><s:text name="tgi.label.DECLINED" /></option>
                                <option value="REFUND"><s:text name="tgi.label.REFUND" /></option>
                                </select>
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<select name="bookingStatus" id="bookingStatus" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_booking_status" /></option>
								<option value="ALL"><s:text name="tgi.label.all" /></option>
                                <option value="PENDING" ><s:text name="tgi.label.PENDING" /></option>
                             	<option value="CONFIRM"><s:text name="tgi.label.CONFIRM" /></option>
                                <option value="FAILED"><s:text name="tgi.label.FAILED" /></option>
                                <option value="PROCESSING"><s:text name="tgi.label.PROCESSING" /></option>
                                <option value="CANCELLED"><s:text name="tgi.label.CANCELLED" /></option>
                                </select>
								</div></div>
							<br>
								<div class="col-md-1">
								<div class="form-group">
								<input type="reset" class="btn btn-danger btn-block" id="configreset" value="Clear">
							</div>
							</div>
								<div class="col-md-1">
								<div class="form-group">
								<input type="submit" class="btn btn-info btn-block" value="Search" />
							</div>
							</div>
						</div>
				</form> 
                               
                          
			
					<s:if test="%{#session.Company!=null && #session.User!=null}">
					 <form class="form-inline" action="hotelFilterCommissionReport"
								method="post">
								  <div class="" id="filterDiv" style="display:none;" >
								<div class="form-group">
									<label for="exampleInputAmount"><s:text name="tgi.label.from_date" /></label>
									<div class="input-group">
										<input type="text" class="form-control input-sm" id="twodpd1"
											placeholder="yyyy-mm-dd" value="<s:property value="hotelAgentagentTotalReportCommObj.yesterDayDate"/>"   required="required" name="yesterDayDate"
											 >
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
									<label for="exampleInputAmount"><s:text name="tgi.label.to_date" /></label>
									<div class="input-group">
										<input type="text" class="form-control input-sm" id="twodpd2"
												placeholder="yyyy-mm-dd"  value="<s:property value="hotelAgentagentTotalReportCommObj.todayDate"/>"   required="required" name="todayDate"
											 >
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
								<div class="form-group rep-buto">
									<button type="submit" class="btn btn-primary"><s:text name="tgi.label.generate_invoice" /></button>
								</div>
								<div class="form-group rep-buto">
									<s:property value="hotelAgentagentTotalReportCommObj.status"/>
								</div>
								</div></div>
								
								</form>
						
	<div class="scroller ">
			<div class="content mt-0 pt-0">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"
										role="grid" aria-describedby="example_info"
										style="width: 100%;" cellspacing="0">
                                        <!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
                                            <thead>
                                                <tr class="border-radius border-color" role="row">
												<th data-priority="1"><s:text name="tgi.label.s.no" /></th>
												<th data-priority="3"><s:text name="tgi.label.booking_date" /></th>
												<th data-priority="2"><s:text name="tgi.label.orderid" /></th>
												<th data-priority="4">My Markup</th>
												<s:if test="logedInCompany == 'superuser'">
												<th data-priority="5"><s:text name="tgi.label.wholesalermarkup" /></th>
												</s:if>
												<s:if test="logedInCompany != 'agent'">
												<th data-priority="6"><s:text name="tgi.label.agencymarkup" /></th>
												</s:if>
												<th data-priority="7"><s:text name="tgi.label.ticketprice" /></th>
												<th data-priority="8"><s:text name="tgi.label.bookingamount" /></th>
												<%-- <th><s:text name="tgi.label.totalcommission" /></th> --%>
												 <th data-priority="9">My Commission</th>
												 <s:if test="logedInCompany != 'agent'">	
												<th data-priority="10"><s:text name="tgi.label.commissionshared" /></th>
												</s:if>
												<th data-priority="11"><s:text name="tgi.label.myrevenue" /></th>
												<th data-priority="12"><s:text name="tgi.label.status" /></th>
												<th data-priority="13"><s:text name="tgi.label.bookedby" /></th>
												<!-- <th>CheckInDate</th>
												<th>CheckoutDate</th> -->
												<!-- <th>BookedCurrency</th> -->
												<th></th>
											</tr>
											 
												 
											</thead>
											<tbody>
												<s:iterator value="hotelCommissionReportList"
													status="rowCount">
													<tr>
														<td><s:property value="%{#rowCount.count}" /></td>
														<td>${spysr:formatDate(createdAt,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-dd-yyyy')}</td>
														<td><s:property value="orderRef" /></td>
														<td><s:property value="markup" /></td>
														<s:if test="logedInCompany == 'superuser'">
														<td><s:property value="distributorMarkup" /></td>
														</s:if>
														<s:if test="logedInCompany != 'agent'">
														<td><s:property value="childMarkup" /></td>
														</s:if>
														<td><s:property value="ticketPrice" /></td>
														<td><s:property value="finalPrice" /></td>
														<td><s:property value="myCommission" /></td>
														<s:if test="logedInCompany != 'agent'">	
														<td><s:property value="sharedCommission" /></td>
														</s:if>
														<td><s:property value="myProfit" /></td>
														<td><s:property value="status" /></td>
														<td><s:property value="createdBy" /></td>
														<%-- <td><s:property value="checkInDate" /></td>
														<td><s:property value="checkOutDate" /></td> --%>
														<%-- <td><s:property value="curCode" /></td> --%>
														<td></td>
													</tr>
												 </s:iterator>
 												</tbody>
												<tbody class="hidden-xs">
														<tr>
															<th></th>
															<th></th>
															<th></th>
															<th><s:text name="tgi.label.totalmymarkup" /></th>
															<s:if test="logedInCompany == 'superuser'">
															<th><s:text name="tgi.label.totalwholesalermarkup" /></th>
															</s:if>
															<s:if test="logedInCompany != 'agent'">
															<th><s:text name="tgi.label.totalagencymarkup" /></th>
															</s:if>
															<th><s:text name="tgi.label.totalticketprice" /></th>
															<th><s:text name="tgi.label.totalbookingamount" /></th>
															<th><s:text name="tgi.label.totalmycommission" /></th>
															<s:if test="logedInCompany != 'agent'">	
															  <th><s:text name="tgi.label.totalsharedcommission" /></th>
															 </s:if> 
															 <th> 
															 <s:text name="tgi.label.totalrevenue" />
															   </th>
															  <th> 
															  </th>
															  <th> 
															  </th>
															 
														 </tr>
														<tr>
															<th></th>
															<th></th>
															<th></th>
 														<th><s:property
																value="hotelAgentagentTotalReportCommObj.totMyMarkup"/></th>
														<s:if test="logedInCompany == 'superuser'">
														<th><s:property
																value="hotelAgentagentTotalReportCommObj.totWholeSalerMarkup"/></th>
														</s:if>
														<s:if test="logedInCompany != 'agent'">		
														<th><s:property
																value="hotelAgentagentTotalReportCommObj.totAgencyMarkup"/></th>
														</s:if>
														<th><s:property
																value="hotelAgentagentTotalReportCommObj.totTicketPrice"/></th>
														<th><s:property
																value="hotelAgentagentTotalReportCommObj.totAmountSpent"/></th>
														 
														 <th><s:property
																value="hotelAgentagentTotalReportCommObj.totMyCommission"/></th>
 														<s:if test="logedInCompany != 'agent'">	
 														<th><s:property
																value="hotelAgentagentTotalReportCommObj.totSharedCommission"/></th>
														</s:if>
															 <th><s:property
																	value="hotelAgentagentTotalReportCommObj.totAgentSComm" />
															</th>
															 
															<th></th>
															 <th></th>
														 </tr>
 														</tbody>
 														
														</table>
														</div>
														</div>
														</div>
								 </s:if>
								 </div></div></div></div>
								 </section>
							 
	
	<!-- Scripting Start-->
	<link href="admin/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	   	 <script src="admin/js/jquery-ui.js" ></script>
         <script>
			$(document).ready(function() {
				var date1="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				var date2="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
				 
				 var date3="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					var date4="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					 spysr_date_custom_plugin('filter-form','date3','date4','mm/dd/yyyy','MMM DD YYYY',date3 ,date4);
			});
		</script>
	
	<script type="text/javascript">
		$(function() {
			/*  $('#row_dim').hide();  */
			$('#user').change(function() {
				//alert($('#user').val());
				if ($('#user').val() == 'ALL') {
					$('#company_form-group').hide();
				} else if ($('#user').val() == '0') {
					$('#company_form-group').show();

				} else {
					$('#company_form-group').hide();
				}
			});

			$('#companyName').change(function() {
				//alert($('#companyName').val());
				if ($('#companyName').val() == 'ALL') {
					$('#user_form-group').hide();
				} else if ($('#companyName').val() == '0') {
					$('#user_form-group').show();

				} else {
					$('#user_form-group').hide();
				}
			});

		});
	</script>
	<script type="text/javascript" class="init">
			$(document).ready(function() {
					$('#filterBtn').click(function() {
				    $('#filterDiv').toggle();
				});
					
					$('div.easy-autocomplete').removeAttr('style');	
					
					$('#configreset').click(function(){
			            $('#resetform')[0].reset(); 
			            });
			} );
			
			$('.faq-links').click(function() {
				var collapsed = $(this).find('i').hasClass('fa-compress');
			
				$('.faq-links').find('i').removeClass('fa-expand');
			
				$('.faq-links').find('i').addClass('fa-compress');
				if (collapsed)
					$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
			});
			
			$(document).ready(function(){$(".filterBtn").click(function(){$("#filterDiv").toggle(500)}),
				 $("div.easy-autocomplete").removeAttr("style"),$("#configreset").click(function(){$("#resetform")[0].reset()})}),
				 $(".filter-link").click(function(){var e=$(this).find("i").hasClass("fa-angle-up");$(".filter-link").find("i").removeClass("fa-angle-down"),
				 $(".filter-link").find("i").addClass("fa-angle-up"),e&&$(this).find("i").toggleClass("fa-angle-up fa-2x fa-angle-down fa-2x")});
	</script>
	<!--//-- Scripting Start-->
