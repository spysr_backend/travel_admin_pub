<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
@media screen and (max-width:480px) {
	.detail-btn {
		margin-top: -27px;
		margin-left: 15px;
	}
}

.center {
	text-align: center;
}

.btn-xsm, .btn-group-xs>.btn {
	padding: 2px 6px;
	font-size: 13px;
	line-height: 1.5;
	border-radius: 2px;
}

a.collapsed {
	color: #02214c;
	text-decoration: none !important;
}

a.collapsed:hover {
	color: #337ab7;
	text-decoration: none !important;
}

.highlight {
	background-color: #F44336;
}

.highlight-light {
	background-color: #ff6559;
}

.line-btn {
	padding: 5px 0 0 5px;
	font-size: 20px;
	color: #9E9E9E;
}

span.text-spysr:hover {
	color: #3a7df9;
}
</style>

<!--************************************
        MAIN ADMIN AREA
    ****************************************-->

<!--ADMIN AREA BEGINS-->
<section class="wrapper container-fluid">
	<div class="row">
		<div class="">
			<div class="card1">
				<div class="pnl">
					<div class="hd clearfix">
						<h5 class="pull-left"><img class="clippy" src="admin/img/svg/user.svg" width="25" alt="Wallet"> Front User List</h5>
						<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a href="frontUserList" class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
					</div>
					<div class="row mt-20">
					<form action="listFrontUserFilter" class="filter-form" id="resetform" method="post">
								<div class="" id="filterDiv" style="display: none;">
								<div class="form-group">
								<s:if test="%{((#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
								<div class="col-md-2 col-sm-6">
							   	<div class="form-group">
									<select name="leadGenerateSource" id="created-by" class="form-control input-sm input-sm">
											<option value=""  selected="selected">Select Created By</option>
											<option value="all"><s:text name="tgi.label.all" /></option>
											<option value="my">My</option>
											<option value="distributor">Distributer</option>
											<option value="agency">Agency</option>
									</select>
								 </div>
								 </div>
								 </s:if>
								<div class="col-md-2 col-sm-6">
							   	<div class="form-group">
									<input type="text" name="createdDateRange" id="created_date_range" placeholder="Created At From...." class="form-control input-sm search-query" value="" />
								 </div>
								 </div>
							   
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="firstName" id="first-name-json" placeholder="Search By First Name" class="form-control input-sm search-query" />
							   </div>
							   </div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="lastName" id="last-name-json" placeholder="Search By Last Name" class="form-control input-sm search-query" />
							   </div>
							   </div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="email" id="email-json" placeholder="Search By Email" class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="mobile" id="mobile-json" placeholder="Search By Mobile" class="form-control input-sm search-query" />
								</div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="city" id="city-json" placeholder="Search By City" class="form-control input-sm search-query" />
								</div>
								</div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="country" id="country-json" placeholder="Search By Country" class="form-control input-sm search-query" />
								</div>
								</div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select name="emailVerifyFlag" id="emailVerify" class="form-control input-sm">
								<option value="" selected="selected">Mail Verify</option>  
								<option value="Yes"><s:text name="tgi.label.yes" /></option>
                                <option value="No" ><s:text name="tgi.label.no" /></option>
                                </select>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select name="mailStatusFlag" id="terminate" class="form-control input-sm">
								<option value="" selected="selected">Email Status</option>
								<option value="Yes"><s:text name="tgi.label.yes" /></option>
                                <option value="No" ><s:text name="tgi.label.no" /></option>
                                </select>
								 </div></div>
								<div class="row">
								 <div class=""></div>
								   <div class="col-md-1">
									<div class="form-group">
									<button type="reset" class="btn btn-danger btn-sm btn-block" id="configreset">&nbsp;Clear&nbsp;</button>
								</div>
								</div>
									<div class="col-md-1">
									<div class="form-group">
									<input type="hidden" name="filterFlag" id="filterFlag" value="true">
									<button type="submit" class="btn btn-info btn-sm btn-block" value="Search" >Search</button>
								</div>
								</div>
								</div>
						</div></div>
				</form>
					</div>
					<div class="dash-table">
						<div class="content mt-0 pt-0">
							<div id="example_wrapper" class="dataTables_wrapper">
								<table id="example" class="display dataTable responsive nowrap" role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
									<thead>
										<tr class="table-sub-header">
											<th colspan="15" class="noborder-xs">
												<div class="pull-right"
													style="padding: 4px; margin-top: -6px; margin-bottom: -10px;">
													<div class="btn-group">
														<div class="dropdown pull-left" style="margin-right: 5px;"
															data-toggle="tooltip" data-placement="top"
															title="Wallet Transactions History">
															<a class="btn btn-sm btn-default" href="addTravelSalesLead"><img class="clippy" src="admin/img/svg/transaction.svg" width="15"><span class="">&nbsp;WT&nbsp;</span></a>
														</div>
														<span class="line-btn"> | </span>
														<div class="dropdown pull-right" style="margin-left: 5px;">
															<a class="btn btn-sm btn-default filterBtn"
																data-toggle="dropdown"
																style="margin-bottom: 2px; margin-right: 3px;"
																title="Show Filter Row"> <img class="clippy"
																src="admin/img/svg/filter.svg" width="15"
																alt="Copy to clipboard" style="margin-bottom: 3px;">
															</a>
														</div>
														<div class="dropdown pull-right" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip"
																data-placement="top"
																data-size="${travelSalesLeadList.size()}"
																title="Please Select Row">
																<button type="submit" class="btn btn-sm btn-default"
																	data-toggle="dropdown" id="delete_all_price"
																	style="margin-bottom: 2px; margin-right: 3px;"
																	disabled="disabled">
																	<img class="clippy" src="admin/img/svg/cloud-data.svg"
																		width="15" alt="Copy to clipboard"
																		style="margin-bottom: 3px;"> <span
																		id="span-all-del">Archive</span>
																</button>
															</div>
														</div>
													</div>
												</div>
											</th>
										</tr>
										<tr class="border-radius border-color" role="row">

											<th style="width: 50px;" data-priority="1">
												<div class="center">
													<div data-toggle="tooltip" data-placement="top" title="Select All">
														<div class="checkbox checkbox-default">
															<input id="master_check" data-action="all" type="checkbox"> 
														<label for="master_check">&nbsp;</label>
														</div>
													</div>
												</div>
											</th>
											<th data-priority="2" style="width: 50px;">SNo.</th>
											<th data-priority="3"> Full Name</th>
											<th data-priority="4"><s:text name="tgi.label.mobile" /></th>
											<th data-priority="5"><s:text name="tgi.label.email" /></th>
											<th data-priority="6">City</th>
											<th data-priority="7">Country</th>
											<th data-priority="8" class="col-md-1">Created At</th>
											<!-- <th data-priority="6" style="width: 92px;">Mail Status</th> -->
											<th data-priority="9" style="width: 92px;">Email Verify</th>
											<th data-priority="10" style="width: 92px;">Status</th>
											<th data-priority="9" style="width: 92px;">Source</th>
											<s:if test="%{((#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor() || #session.Company.companyRole.isAgent()) && #session.User.userRoleId.isAdmin())}">
											<th data-priority="11" style="width: 50px;">Wallet</th>
											<th data-priority="12" style="width: 50px;"><img class="clippy" src="admin/img/svg/settings.svg" width="15" alt="Settings" style="margin-bottom: 3px;"></th>
											</s:if>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<s:if test="frontUsersList.size > 0">
											<s:iterator value="frontUsersList" status="rowstatus">
												<tr>

													<td data-title="Select" class="select-checkbox">
														<div class="center">
															<div class="checkbox checkbox-default">
																<input id="checkbox1_sd_${rowstatus.count}"
																	type="checkbox" value="false" data-id="${id}"
																	class="check_row"> <label
																	for="checkbox1_sd_${rowstatus.count}"></label>
															</div>
														</div>
													</td>
													<td scope="row" data-title="S.No"><div class="center">
															<s:property value="%{#rowstatus.count}" />
														</div></td>
													<td data-title="FirstName" class="link">
													<c:choose>
													<c:when test="${firstName != null || middleName != null || lastName != null}">
														<a href="user?id=${id}"><s:property value="firstName" />&nbsp;<s:property value="middleName" />&nbsp;<s:property value="lastName" />
														</a>
														</c:when>
														<c:otherwise><a href="user?id=${id}">N/A</a></c:otherwise>
														</c:choose></td>
													<td data-title="Mobile"><c:choose> <c:when test="${mobile != null}"><a href="tel:${phone}"><img class="clippy" src="admin/img/svg/telephone-g.svg" width="16"style="margin-bottom: 3px;"></a> &nbsp;${mobile}</c:when><c:otherwise>N/A</c:otherwise></c:choose></td>
													<td data-title="Email"><a href="mailto:${email}?body= Hi ${firstName}"><img class="clippy" src="admin/img/svg/envelope-g.svg" width="16"style="margin-bottom: 3px;">&nbsp;&nbsp;${email}</a></td>
													<td><c:choose>
													<c:when test="${city != null}">
													${city}
													</c:when>
													<c:otherwise>
														None
													</c:otherwise>
													</c:choose></td>
													<td><c:choose>
															<c:when test="${country != null}">
													${country}
													</c:when>
															<c:otherwise>
																None
															</c:otherwise>
														</c:choose></td>
													<td data-title="Email">
													<c:choose>
													<c:when test="${createdAt != null && createdAt != ''}">
													<s:date name="createdAt" format="dd/MMM/YYYY hh:mm a"/>
													</c:when>
													<c:otherwise>
													<span class="">None</span>	
													</c:otherwise>
													</c:choose>
													</td>
													<%-- <td><c:choose>
															<c:when test="${activeStatus != false}">
																	<img class="clippy" src="admin/img/svg/checkedg.svg"
																		width="14" alt="Copy to clipboard"
																		style="margin-bottom: 3px;">
															</c:when>
															<c:otherwise>
																	<img class="clippy" src="admin/img/svg/close.svg"
																		width="10" alt="Copy to clipboard"
																		style="margin-bottom: 3px;">
															</c:otherwise>
														</c:choose></td> --%>
													<td><c:choose>
															<c:when test="${emailVerified != false}">
																	<img class="clippy" src="admin/img/svg/checkedg.svg" width="14" alt="Verify Email" style="margin-bottom: 3px;">
															</c:when>
															<c:otherwise>
																	<img class="clippy" src="admin/img/svg/close.svg"
																		width="10" alt="Copy to clipboard"
																		style="margin-bottom: 3px;">
															</c:otherwise>
														</c:choose></td>
													<td><c:choose>
															<c:when test="${activeStatus != false}">
																	<img class="clippy" src="admin/img/svg/checkedg.svg"
																		width="14" alt="Copy to clipboard"
																		style="margin-bottom: 3px;">
															</c:when>
															<c:otherwise>
																	<img class="clippy" src="admin/img/svg/close.svg"
																		width="10" alt="Copy to clipboard"
																		style="margin-bottom: 3px;">
															</c:otherwise>
														</c:choose></td>
													<td>
													<c:choose>
													<c:when test="${source != null && source != ''}">
													${source}
													</c:when>
													<c:otherwise>
													N/A
													</c:otherwise>													
													</c:choose>
													</td>
													<s:if test="%{((#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor() || #session.Company.companyRole.isAgent()) && #session.User.userRoleId.isAdmin())}">
													<td><span data-toggle="tooltip" data-placement="top" title="Add User Wallet">
													<a href="add_front_user_wallet?userId=${id}" class="btn btn-sm btn-default" style="padding: 3px 13px;border-radius: 5px;"><img class="clippy" src="admin/img/svg/wallet-money-orange.svg" width="16" alt="Wallet"></a>
													</span>
													</td>
													</s:if>
													<td data-title="Action">
													<div class="dropdown pull-right" style="">
													  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
													  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
													  <ul class="dropdown-menu">
													    <li class="action"><a href="user?id=${id}">Edit</a></li>
													    <s:if test="%{#session.User.userRoleId.isSuperUser() && #session.User.userRoleId.isAdmin()}">
													    <li class="action">
													    <a class="a-tag" href="#" data-user-id="${id}" data-wallet-id="${userWallet.id}" onclick="javascript:delteFrontUser(this)">
																<i class="icon-trash"></i>Delete
														</a>
													    </s:if>
													    <li class="action">
													    <a href="getFrontUserWalletTransactionHistory?userId=${id}" class="plus-icon" >Wallet History</a>
													    </li>
													  </ul>
													</div>
													</td>  
													<td></td>
												</tr>
											</s:iterator>
										</s:if>
									</tbody>

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
/* $(function() {

	
	 $('#created_date_range').daterangepicker({
	      autoUpdateInput: false,
	      locale: {
	          cancelLabel: 'Clear'
	      }
	  });
	 
    var start = moment().subtract(29, 'days');
    var end = moment();
    /*  function cb(start, end) {
        $('#created_date_range span').html(start.format('dd/mm/yyyy') + ' - ' + end.format('dd/mm/yyyy'));    }  */

        /*  $('#created_date_range').daterangepicker({
       startDate: start,
        endDate: end, 
        "showDropdowns": true,
        
     /*    ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "cancelClass": "btn-danger"
    	
    }); */

    /* cb(start, end); */

/*  }); */

/*------------------------------*/
	function delteFrontUser(sel) {
				// confirm dialog
				 alertify.confirm("Are you sure delete this user", function () {
					 var userId = $(sel).data("user-id");
						$.ajax({
							url : "deleteFrontUser?id="+userId,							
							type : "GET",
							dataType: 'json',
							success : function(jsonData) {
								if(jsonData.json.status == 'success'){
									alertify.success(jsonData.json.message);
									setTimeout(location.reload.bind(location), 1000);	
								}
				  				else if(jsonData.json.status == 'error'){
				  					alertify.error(jsonData.json.message, function(ev) {
				  					    alertify.error("Ok Try again");
				  					});
				  				}
							},
							error: function (request, status, error) {
								showModalPopUp("Somthing Wrong, please try again.","e");
							}
						});
				 }, function() {
					 alertify.error("You've clicked Cancel");
				 });
				
			} 
</script>
<script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>
<script type="text/javascript" src="admin/js/filter/front-user.js"></script>
<script type="text/javascript" src="admin/js/admin/multiple-archive.js"></script>