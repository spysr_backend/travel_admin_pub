<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="admin/css/sass/style.scss" rel="stylesheet/scss"/>
<style>
.center {
	text-align: center;
}

.btn-xsm, .btn-group-xs>.btn {
	padding: 2px 6px;
	font-size: 13px;
	line-height: 1.5;
	border-radius: 2px;
}
 
a.collapsed:hover {
	color: #337ab7;
	    text-decoration: none !important;
}
.img-fluid {
    width: 100%;
    height: auto;
    border: 2px dotted #a0a0a0;
}

@media screen and (max-width:767px){
	.img-fluid {
    width: 100%;
    height: auto;
    border: 2px dotted #a0a0a0;
}
}
.check
{
  	opacity:1;
	border: 12px solid #d4d4d4;
	
}
.bottom-line
{
    margin-top: 12px;
    margin-bottom: 12px;
    border: 0;
    border-top: 4px solid #eeeeee;
    border-radius: 32px;
}
.btn-group-addon {
    padding: 4px 12px;
    font-size: 14px;
    font-weight: normal;
    line-height: 1;
    color: #555555;
    text-align: center;
    background-color: #eeeeee;
    border: 1px solid #cccccc;
    border-radius: 0;
}
/* image checkbox */
</style>

        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">
                        <div class="card1">
                        <div class="pnl">
                        <div class="hd clearfix">
							<h5 class="pull-left">Company Lead List</h5>
							<div class="set pull-right hidden-xs" style="margin-top:5px;font-size: 15px;">
								<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
							</div>
							<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
								<a href="listCompanyLead" class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
							</div>
							<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
								<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
								<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
							</div>
						</div>
							<div class="row mt-2">
                         		<form action="listCompanyLeadFilter" class="filter-form" id="resetform" method="post">
								<div class="" id="filterDiv" style="display: none;">
								<div class="col-md-2">
									<div class="form-group">
										<select name="leadSource" id="lead-source" class="form-control input-sm input-sm js-select-created-by">
											<option  selected="selected" value="">Select Created By</option>
											<option value="">All</option>
											<option value="IBEUSER">IBE</option>
											<option value="ADMIN"><s:text name="tgi.label.admin" /></option>
										</select>
									</div>
								</div>
								<div class="col-md-2">
							   <div class="form-group">
								<input type="text" name="createdDateRange" id="created-date-range" placeholder="Created Start Date...." class="form-control input-sm search-query" />
								 </div></div>
							   
							   <!-- <div class="col-md-2">
							   <div class="form-group">
								<input type="text" name="createdAt" id="bookingEndDate" placeholder="Created End Date...." class="form-control input-sm search-query date2" />
							   </div>
							   </div> -->
							   <div class="col-md-2">
							   <div class="form-group">
								<input type="text" name="companyName" id="company-name-json" placeholder="Company Name" class="form-control input-sm search-query" />
							   </div>
							   </div>
								<div class="col-md-2">
								<div class="form-group">
								<input type="text" name="firstName" id="firstName" placeholder="User FirstName..." class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2">
								<div class="form-group">
								<input type="text" name="lastName" id="lastName" placeholder="User LastName..." class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2">
								<div class="form-group">
								<input type="text" name="email" id="email" placeholder="User Email" class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2">
								<div class="form-group">
								<input type="text" name="phone" id="phone" placeholder="User Mobile" class="form-control input-sm search-query" />
								</div></div>
							   <div class="col-md-2">
							   <div class="form-group">
								<input type="text" name="city" id="city-json" placeholder="City" class="form-control input-sm search-query" />
								</div></div>
							   <div class="col-md-2">
							   <div class="form-group">
								<input type="text" name="country" id="country-json" placeholder="Country" class="form-control input-sm search-query" />
								 </div></div>
							   <div class="col-md-2">
							   <div class="form-group">
								<select name="isEmailVerified" id="email_verified" class="form-control input-sm">
								<option value="" selected="selected">Select Email Verified</option>
                                <option value="true"><s:text name="tgi.label.yes" /></option>
                             	<option value="false"><s:text name="tgi.label.no" /></option>
                                </select>
								 </div></div>
							   <div class="col-md-2">
							   <div class="form-group">
								<select name="isTermnated" id="terminate" class="form-control input-sm">
								<option value="" selected="selected">Account Terminate</option>
								<option value="true"><s:text name="tgi.label.yes" /></option>
                                <option value="false" ><s:text name="tgi.label.no" /></option>
                                </select>
								 </div></div>
							   <div class="col-md-1">
								<div class="form-group">
								<button type="reset" class="btn btn-danger btn-sm btn-block" id="configreset" value="">Clear&nbsp;</button>
							</div>
							</div>
								<div class="col-md-1">
								<div class="form-group">
								<input type="hidden" name="filterFlag" id="filterFlag" value="true" />
								<button type="submit" class="btn btn-info btn-sm btn-block" value="Search" >Search</button>
							</div>
						</div></div>
				</form>
				</div>
                 <div class="row">
		        <div class="col-md-12">
		            <div class="json-msg-alert"></div>
		        </div>    
		        </div>   
				<div class="dash-table">
				<div class="content">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display nowrap dataTable responsive "  role="grid" aria-describedby="example_info" style="width: 100%;">
                                        <thead>
										<tr class="table-sub-header">
                                        	<th colspan="18" class="noborder-xs">
                                        	<div class="pull-left">   
                                        	<div class="btn-group"> 
                                        	<div class="pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="Download Sample Excel File">
											  <a class="btn btn-sm btn-outline-success" href="admin/assets/excel/sample_company_lead.xlsx" ><img class="clippy" src="admin/img/svg/excel-g.svg" width="13" alt="Excel"><span class=""> Sample</span></a> 
											</div>
											<div class="dropdown pull-left" style="margin-right:5px;">
											  <form id="leadsExcelUploadForm" method="post" enctype="multipart/form-data">
												<label class="btn-bs-file btn btn-sm btn-outline-success pull-right mr-1" data-toggle="tooltip" data-placement="top" title="Upload Excel File"><img class="clippy" src="admin/img/svg/import.svg" width="13" alt="Import" style="margin-bottom: 3px;"> Import
								                	<input type="file" name="leadExcelFile" onchange="javascript:leadExcelUpload();" />
							            		</label>
							            		</form>
											</div>
											<span class="line-btn">  | </span>
											</div>
											</div> 
											<div class="pull-left">
													<div class="btn-group">
														<div class="dropdown pull-left" style="margin-right: 5px;margin-left: 5px;" data-toggle="tooltip" data-placement="top" title="Lead Status : New">
															<a class="btn btn-sm btn-default" href="listCompanyLead">
															<span class=""><strong>All</strong></span></a>
														</div>
														<div class="dropdown pull-left" style="margin-right: 5px;margin-left: 5px;" data-toggle="tooltip" data-placement="top" title="Lead Status : New">
															<a class="btn btn-sm btn-outline-info" href="listCompanyLeadFilter?leadStatus=New&filterFlag=true">
															<span class="notice-info"><strong>New &nbsp;( ${leadStatusUtil.newstatus} )</strong></span></a>
															
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status: Active">
																<a class="btn btn-sm btn-default" href="listCompanyLeadFilter?leadStatus=Active&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-active"><strong>Active &nbsp;( ${leadStatusUtil.active} )</strong></span>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status: Converted">
																<a class="btn btn-sm btn-default" href="listCompanyLeadFilter?leadStatus=Converted&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-success"><strong>Converted &nbsp;( ${leadStatusUtil.convert} )</strong></span>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status:No Connect">
																<a class="btn btn-sm btn-default" href="listCompanyLeadFilter?leadStatus=No Connect&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-warning"><strong>No Connect &nbsp;( ${leadStatusUtil.noconnect} )</strong></span>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status:No Connect">
																<a class="btn btn-sm btn-default" href="listCompanyLeadFilter?leadStatus=Closed&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-danger"><strong>Closed &nbsp;( ${leadStatusUtil.closed} )</strong></span>
																</a>
															</div>
														</div>
													</div>
											</div>
                                        	<div class="pull-left">   
                                        	<span class="line-btn">  | </span>
														<div class="dropdown pull-right" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Filter By Lead Title">
																<select aria-hidden="true" name="leadTitle" id="lead-title-filter" class="form-control input-sm btn btn-sm btn-outline-spysr " style="height: 28px;font-size:13px">
																  <option value="">Search By Lead Title</option>
																	<c:forEach items="${followupStatusMap}" var="title">
																		<option value="${title.key}">${title.value}</option>
																	</c:forEach>
																 </select>
															</div>
														</div>
                                        	</div>
                                        	<div class="pull-right">   
                                        	<div class="btn-group"> 
											<div class="pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Company Lead">
											  <a class="btn btn-sm btn-default" href="addCompanyLeadDetail" ><img class="clippy" src="admin/img/svg/add1.svg" width="10"><span class="">&nbsp;New&nbsp;</span></a> 
											</div>
											<div class="pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="Send Email">
											  <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#send_company_lead_email" id="send_lead_email" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/sent-mail.svg" width="13"><span id="span-all-email"> Email</span></button> 
											</div>
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a  class="btn btn-sm btn-default filterBtn" data-toggle="tooltip" data-placement="top" title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${companyLeadVoList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="archive_lead_data" disabled="disabled">
											  	<img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Archive" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
										<tr class="border-radius border-color" role="row">
													 <th></th>
													<th style="width: 50px;" data-priority="1">
															<div class="center">
															<div data-toggle="tooltip" data-placement="top" title="Select All">
															<div class="checkbox checkbox-default">
										                        <input id="master_check" data-action="all" type="checkbox" >
										                        <label for="master_check">&nbsp;</label>
										                    </div>
										                    </div>
										                    </div>
													</th>
                                                     <th data-priority="2">Lead Number</th>
                                                     <th data-priority="3"><s:text name="tgi.label.company_name" /></th>
                                                     <th data-priority="4">User Name</th>
                                                     <th data-priority="5"><s:text name="tgi.label.phone" /></th>
                                                     <th data-priority="6"><s:text name="tgi.label.email" /></th>
                                                     <th data-priority="7"><s:text name="tgi.label.city" /></th>
                                                     <th data-priority="8"><s:text name="tgi.label.country" /></th>
                                                     <th style="width: width: 135px;">Created At</th>
                                                     <th style="width: width: 135px;">Lead Source</th>
                                                     <th data-priority="9" style="width: 100px;">Assign To</th>
                                                     <th data-priority="10" style="width: 90px;">Email Varify</th>
                                                     <th data-priority="11" style="width: 106px;"><span data-toggle="tooltip" data-placement="top" title="Lead Converted">Lead Converted</span></th>
                                                     <th data-priority="12" style="width: 56px;">Rating</th>
                                                     <th data-priority="13" style="width: 90px;">Email Count</th>
                                                     <th style="width: 22px;"><div class=""><img class="clippy" src="admin/img/svg/settings.svg" width="15" alt="Settings" style="margin-bottom: 3px;"></div></th>
                                                     <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
   											<s:if test="companyLeadVoList.size > 0">
                                           <c:forEach items="${companyLeadVoList}" var="companyLead" varStatus="rowstatus">
                                                <tr>
                                                 <td class="<c:choose>
													<c:when test="${companyLead.leadStatus == 'New'}">
													lead-status-new
													</c:when>
													<c:when test="${companyLead.leadStatus == 'Active'}">
													lead-status-active
													</c:when>
													<c:when test="${companyLead.leadStatus == 'Blocked'}">
													lead-status-blocked
													</c:when>
													<c:when test="${companyLead.leadStatus == 'No_Connect'}">
													lead-status-no-connect
													</c:when>
													<c:when test="${companyLead.leadStatus == 'Closed'}">
													lead-status-closed
													</c:when>
													<c:when test="${companyLead.leadStatus == 'Converted'}">
													lead-status-booked
													</c:when>
													<c:otherwise>
													lead-status-none
													</c:otherwise>
													</c:choose>"><div class="center">${rowstatus.count}</div></td>
                                                	<td data-title="Select" class="select-checkbox">
																<div class="center">
																<div class="checkbox checkbox-default">
										                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-lead-id="${companyLead.id}" class="check_row">
										                        <label for="checkbox1_sd_${rowstatus.count}"></label>
										                   		</div>
										                   		</div>
													</td>
                                                    <td>
                                                    <a href="editCompanyLead?id=${companyLead.id}"  data-toggle="modal" class="text-primary">
                                                    ${companyLead.leadNumber != null && companyLead.leadNumber != ''?companyLead.leadNumber:"N/A"}
                                                    </a>
                                                    </td>
                                                    <td data-title="Company Name">
                                                    <a href="editCompanyLead?id=${companyLead.id}"  data-toggle="modal" class="text-primary">
                                                    ${companyLead.companyName}
                                                    </a>
                                                    </td>
                                                    <td data-title="First Name">${companyLead.firstName}</td>
                                                    <td data-title="Phone">
                                                    <a href="https://api.whatsapp.com/send?phone=+91${companyLead.phone}&text=hi ${companyLead.firstName}"><img class="clippy" src="admin/img/svg/whatsapp.svg" width="16"style="margin-bottom: 3px;"></a>&nbsp;&nbsp;
                                                    <a href="tel:${companyLead.phone}"><img class="clippy" src="admin/img/svg/telephone-g.svg" width="16"style="margin-bottom: 3px;"></a>
                                                    &nbsp;&nbsp;${companyLead.phone}
                                                    </td>
                                                    <td data-title="Email"><a href="mailto:${companyLead.email}?subject=Lead Detail&body= Hi ${companyLead.firstName}">
                                                    <img class="clippy" src="admin/img/svg/envelope-g.svg" width="16"style="margin-bottom: 3px;">&nbsp;&nbsp;${companyLead.email}</a></td>
                                                     <td data-title="<s:text name="tgi.label.city" />">${companyLead.city}</td>
                                                    <td data-title="<s:text name="tgi.label.country" />">${companyLead.country}</td>
                                                    <td>${spysr:formatDate(companyLead.createdAt,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}</td>
                                                    <td>
                                                    ${companyLead.leadSource != null && companyLead.leadSource != ''?companyLead.leadSource:"N/A"}
                                                    </td>
                                                    <td>
                                                     ${companyLead.assignTo != null && companyLead.assignTo != ''?companyLead.assignTo:"N/A"}
                                                    </td>
                                                    <td data-title="<s:text name="tgi.label.email" />">
                                                    <c:choose>
                                                    <c:when test="${companyLead.emailVerified != false}">
                                                    <img class="clippy" src="admin/img/svg/checkedg.svg" width="15" alt="Copy to clipboard" style="margin-bottom: 3px;">
                                                    </c:when>
                                                    <c:otherwise>
                                                     <img class="clippy" src="admin/img/svg/close.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                        	        <td>
                                        	        <c:choose>
                                                    <c:when test="${companyLead.companyCreated == true}">
                                                   	 <span class="text-success">Yes</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                    <span class="text-danger">No</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                        	        </td>
                                        	        <td>
                                        	        <c:choose>
														<c:when test="${companyLead.leadRating == 'Highest'}">
														<span data-toggle="tooltip" data-placement="top" title="Highest">
															<img class="clay" src="admin/img/svg/highest.svg" width="17" alt="Higest">
														</span>
														</c:when>						
														<c:when test="${companyLead.leadRating == 'High'}">
														<span data-toggle="tooltip" data-placement="top" title="High">
														<img class="clay" src="admin/img/svg/high.svg" width="17" alt="High">
														</span>
														</c:when>						
														<c:when test="${companyLead.leadRating == 'Medium'}">
														<span data-toggle="tooltip" data-placement="top" title="Medium">
														<img class="clay" src="admin/img/svg/medium.svg" width="17" alt="Medium">
														</span>
														</c:when>						
														<c:when test="${companyLead.leadRating == 'Low'}">
														<span data-toggle="tooltip" data-placement="top" title="Low">
														<img class="clay" src="admin/img/svg/low.svg" width="17" alt="Low">
														</span>
														</c:when>						
														<c:when test="${companyLead.leadRating == 'Lowest'}">
														<span data-toggle="tooltip" data-placement="top" title="Lowest">
															<img class="clay" src="admin/img/svg/lowest.svg" width="17" alt="Lowest">
														</span>
														</c:when>	
														<c:otherwise>
														<strike><span data-toggle="tooltip" data-placement="top" title="None" style="    font-size: 14px;">&nbsp;N</span></strike>
														</c:otherwise>					
														</c:choose>
                                        	        </td>
                                        	        <td>
                                        	       <strong class="text-danger">${companyLead.emailCount != null?companyLead.emailCount:"0"}</strong> times
                                        	        </td>
													<td data-title="Action">
													<div class="dropdown pull-right" style="">
													  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
													  	<img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
													  <ul class="dropdown-menu">
													    <li class="action"><a href="editCompanyLead?id=${companyLead.id}">Edit Details</a></li>
													    <s:if test="%{#session.User.userRoleId.isSuperUser() && #session.User.userRoleId.isAdmin()}">
													    <li class="action">
													    <a class="a-tag" href="#" data-lead-id="${companyLead.id}" data-lead-follow-id="${companyLead.leadFollowUpId}" onclick="javascript:delteCompanySalesLead(this)">
																<i class="icon-trash"></i> Delete
														</a>
													    </s:if>
													    <%-- <li class="action">
													    <a href="delte?id=${id}" class="plus-icon" data-event="duplicate" data-toggle="modal" data-target="#assign_lead_${rowstatus.count}" 
													       data-tour-id="${companyLead.id}">Delete</a>
													    </li> --%>
													  </ul>
													</div>
													</td>
													<td></td> 
														 </tr>
                                                </c:forEach>
                                                </s:if>
                                            </tbody>
											
                                        </table></div></div></div>
                                        <!--  -->
                                        <div class="modal fade" id="send_company_lead_email" role="dialog">
										<div class="modal-dialog modal-lg">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header" style="background-color: #ffde01;">
													<h4 class="modal-title text-center">Send Email To Agent</h4>
													<button type="button" class="close slds-modal__close" data-dismiss="modal">
														<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
													</button>
												</div>
												<form id="sendEmailToAgentForm" method="post" class="bv-form">
													<div class="modal-body">
														<div class="row">
														<div class="col-md-12">
														<div class="email-msg-alert"></div>
														</div>
														</div>
														<div class="row">
														<div class="col-md-12">
															<label class="input-sm-label" for="firstName">Subject</label>
														<div class="form-group">
															<input type="text" name="emailSubject" id="email-subject" value="" class="form-control">
														</div>
														</div>
														</div>
														<div class="row">
														<div class="col-md-12">
															<label class="text-danger">Please Select A Template :</label>
														</div>
															<div class="form-group">
															<div class="col-md-4">
															<label class="">Template 1</label>
																	<a href="#">
																		<img src="admin/img/email-template/email-template-1.jpg" class="img-fluid img-check" style="height: 320px">
																		<input type="radio" name="templateFlag" id="template-1" value="TEMPLATE_1" class="hidden" autocomplete="off">
																	</a>
																	<div class="mt-1 center">
																		<a href="admin/img/email-template/email-template-1.jpg" id="" class="open-image btn btn-xs btn-block btn-outline-primary">Preview</a>
																	</div>
															</div>
															<div class="col-md-4">
																<label class="">Template 2</label>
																	<a href="#">
																		<img src="admin/img/email-template/email-template-2.jpg" class="img-fluid img-check" style="height: 320px">
																		<input type="radio" name="templateFlag" id="template-2" value="TEMPLATE_2" class="hidden" autocomplete="off">
																	</a>
																	<div class="mt-1 center">  
																		<a href="admin/img/email-template/email-template-2.jpg" id="" class="open-image btn btn-xs btn-block btn-outline-primary">Preview</a>
																	</div>
															</div>
															<div class="col-md-4">
																<label class="">Template 3</label>
																	<a href="#">
																	<img src="admin/img/email-template/email-template-3.png" class="img-fluid img-check" style="height: 320px">
																	<input type="radio" name="templateFlag" id="template-3" value="TEMPLATE_3" class="hidden" autocomplete="off">
																	</a>
																	<div class="mt-1 center">
																		<a href="admin/img/email-template/email-template-3.jpg" id="" class="open-image btn btn-xs btn-block btn-outline-primary">Preview</a>
																	</div>
															</div>
															</div>
														</div>
														<hr class="bottom-line">
														<script>
														$('.open-image').click(function (e) {
											                e.preventDefault();
											                alwaysShowClose: true,
											                $(this).ekkoLightbox();
											            });
														
														$(document).ready(function(e){
												    		
														$('.img-check').click(function(e) {
												        $('.img-check').not(this).removeClass('check')
												    		.siblings('input').prop('checked',false);
												    	$(this).addClass('check')
												            .siblings('input').prop('checked',true);
												    	});
															
														});
														</script>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
														<button type="button" class="btn btn-success" id="send_email_agent" data-model-id="send_company_lead_email" data-form-name="sendEmailToAgentForm">
														Send <img class="clippy" src="admin/img/svg/sent-mail-w.svg" width="14"> </button>
													</div>
												</form>
											</div>
										</div>
									</div>
                                        
                                        </div>
										</div>
            </section>
        <!--ADMIN AREA ENDS-->
<script>
var CName={url:"getCompanyLeadJson.action?data=companyName",getValue:"companyName",list:{match:{enabled:!0}}};$("#company-name-json").easyAutocomplete(CName);
var FName={url:"getCompanyLeadJson.action?data=firstName",getValue:"firstName",list:{match:{enabled:!0}}};$("#firstName").easyAutocomplete(FName);
var LName={url:"getCompanyLeadJson.action?data=lastName",getValue:"lastName",list:{match:{enabled:!0}}};$("#lastName").easyAutocomplete(LName);
var Email={url:"getCompanyLeadJson.action?data=email",getValue:"email",list:{match:{enabled:!0}}};$("#email").easyAutocomplete(Email);
var Phone={url:"getCompanyLeadJson.action?data=phone",getValue:"phone",list:{match:{enabled:!0}}};$("#phone").easyAutocomplete(Phone);
var City={url:"getCompanyLeadJson.action?data=city",getValue:"city",list:{match:{enabled:!0}}};$("#city-json").easyAutocomplete(City);
var Country={url:"getCompanyLeadJson.action?data=country",getValue:"countryName",list:{match:{enabled:!0}}};$("#country-json").easyAutocomplete(Country);
	
 /*--------------------*/
</script>

<script>
function delteCompanySalesLead(sel) {
	// confirm dialog
	 alertify.confirm("Are you sure delete this lead", function () {
		 var leadId = $(sel).data("lead-id");
		 var followUpId = $(sel).data("lead-follow-id");
			$.ajax({
				url : "deleteCompanyLead?id="+leadId+"&followUpId="+followUpId,							
				type : "GET",
				dataType: 'json',
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);	
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message, function(ev) {
	  					    alertify.error("Ok Try again");
	  					});
	  				}
				},
				error: function (request, status, error) {
					showModalPopUp("Somthing Wrong, please try again.","e");
				}
			});
	 }, function() {
		 alertify.error("You've clicked Cancel");
	 });
	
}
/*--------------------------script for upload excel file-----------------------------*/
 	function leadExcelUpload(){
 		$(".json-msg-alert").html(" ");
			 var formData = new FormData($("#leadsExcelUploadForm")[0]);
			    $.ajax({
			        url: "upload_company_lead_excel",
			        type: 'POST',
			        data: formData,
			        async: false,
			        dataType: 'json',
			        success: function (jsonData) 
			        {
			        	if(jsonData.json.status == 'success'){
							$.notify({icon: 'fa fa-check',message: jsonData.json.message},{type: 'success'});
							window.location.reload();
						}
		  				else if(jsonData.json.status == 'error'){
		  					$(".json-msg-alert").html('<div class="msg-alert msg-danger msg-danger-text"> <i class="fa fa-times"></i><span class="error-msg">&nbsp;'+jsonData.json.message+'</span></div>');
		  				}
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });	
		}
///filter company lead followup title
 	$('#lead-title-filter').on('change', function (e) {
 	    var optionSelected = $("option:selected", this);
 	    var leadtitle = this.value;
 	    
 	    if(leadtitle != null && leadtitle != '')
 		{
 			var url = '<%=request.getContextPath()%>/listCompanyLeadFilter';
 			window.location.replace(url+'?leadTitle='+leadtitle+'&filterFlag='+true);
 		}
 		else{
 			return false;
 		}
 	});
 
</script>

<script type="text/javascript" src="admin/js/admin/lead.follow.up.email.js"></script>
<script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>