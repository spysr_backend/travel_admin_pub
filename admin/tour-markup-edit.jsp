<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
 <%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src="admin/js/jquery-ui.js" ></script>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"tourMarkupEdit?id="+"${param.id}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>

<%-- <script type="text/javascript">
 	 $(function() {
		 loadstations();
	 });
 	function loadstations() {
		  $.getJSON("hotels.json",{
				format : "json"
			}).done(
					 function(jsonbusdata) {
					 
						  citylist = [];
						  names = [];
						 
						 $.each(jsonbusdata, function(i, station) {
							citylist.push(station.chain);
							 
						 });
						 $("#hotelChain").autocomplete({
					        source: citylist,
					         
					    });
						 
						 $('#hotelChain').on('autocompleteselect', function (e, ui) {
							    	 
			 }); 
					    $.ui.autocomplete.filter = function (array, term) {
					        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
					        return $.grep(array, function (value) {
					            return matcher.test(value.label || value.value || value);
					        });
					    };
					 });
 	}
 	
 	
 	
 	
 	
 	</script> --%>
 	 
 <%-- 
 <script>
   $(function() {
		var id=$("#uniqueId").val();
		var accumulative=$("#accumulative").val();
		var fixedAmount=$("#fixedAmount").val();
	/* 	var positionOfMarkup=$("#positionOfMarkup").val(); */
		//var airline=$("#airline").val();
		var country=$("#country").val();
		//var classOfService=$("#classOfService").val();
		//alert(id+"\n"+country_id);
		 document.getElementById('accumulative'+id).value =accumulative;
		 document.getElementById('fixedAmount'+id).value =fixedAmount;
	/* 	 document.getElementById('positionOfMarkup'+id).value =positionOfMarkup; */
		 //document.getElementById('airline'+id).value =airline;
		 document.getElementById('country'+id).value =country;
		 //document.getElementById('classOfService'+id).value =classOfService;

		 
	}); 
   
   
   
   
   
 </script>  --%>
 <script>
 
		$(document).ready(function() {
			$("#datepicker_arr").datepicker({
				format : "M-dd-yyyy"
			/*  changeMonth: true,
			 changeYear: true */
			});
		});

		$(document).ready(function() {
			$("#datepicker_dep").datepicker({
				format : "M-dd-yyyy"
			});
		});
		$(document).ready(function() {
			$("#datepicker_PromofareStart").datepicker({
				format : "M-dd-yyyy"
			});
		});
		$(document).ready(function() {
			$("#datepicker_PromofareEnd").datepicker({
				format : "M-dd-yyyy"
			});
		});
	</script> 
    

        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
     
            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card1">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.edit_tour_markup" /></h5>
                                <div class="set pull-left">
                                  <!--   <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                                <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="tourMarkupList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.tour_markup_list" /> </a></li>
									<%-- <li class="divider"></li>
									<li class=""><input type="hidden" id="deleteItem"> <a
											href="#"onclick="deletePopupCountryInfo('${id}','${version}')"
											class="btn btn-xs "><span
												class="glyphicon glyphicon-trash"></span> <s:text name="tgi.label.delete" /></a></li> --%>
									
								</ul>
							</div>
						</div>
                               </div> 
                            </div>
							<div class="row">
					<div class="col-lg-12">
						<div class="card-block1">
						 <form action="updateTourMarkup" id="tour-markup-update" method="post" class="form-horizontal">
                                   
                                   
                           <div class="row">
                           <div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.config_number" />
											</label> <input type="hidden" value="${configData}" id="configDataHdn">
											
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control input-sm" name="configData"
								id="configData" autocomplete="off" required>
								<option selected value="0"><s:text name="tgi.label.select_company_confignumber" /> </option>
								<s:if test="%{#session.Company.companyRole.isDistributor()}">
								 <s:iterator value="%{#session.AgencyConfigIdsList}">
									<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
								  </s:iterator>
								  <s:iterator value="%{#session.companyConfigIds}">
									<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
								  </s:iterator>
 						 		</s:if>
 						 		<s:elseif test="%{#session.Company.companyRole.isAgent()}">
								  <s:iterator value="%{#session.AgencyConfigIdsList}">
									<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
								  </s:iterator>
 						 		</s:elseif>
 						 		
								<s:else>
								<s:iterator value="%{#session.companyConfigIds}">
								<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
								 </s:iterator>
 							</s:else>
						 

							</select></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_name" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm" id="name"
									name="name" placeholder="markup Name" autocomplete="off" value="${tourMarkup.name}"
									required></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.tour_origin" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm airportList" id="tourOrigin"
									name="tourOrigin" placeholder="Tour Origin"  
									  value="${tourMarkup.tourOrigin}"   ></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.tour_destianation" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm airportList" id="tourDestination"
									name="tourDestination" placeholder="Tour Destination" autocomplete="off"
									  value="${tourMarkup.tourDestination}" ></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.is_accumulative" />
											</label>
											<input type="hidden" id="isaccumulativeHdn" value="${tourMarkup.isaccumulative}" />
										<div class="controls"><div class="form-group"><div class="">
														<select class="form-control input-sm" name="isaccumulative"
									  autocomplete="off" required>
 									<option value="1"><s:text name="tgi.label.yes" /></option>
									<option value="0" selected="selected"><s:text name="tgi.label.no" /></option>
									 
								</select></div></div>
												</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.lable.is_fixedamount" />
											</label>
											 <input type="hidden" id="isfixedAmount" value="${tourMarkup.isfixedAmount}" />
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control input-sm" name="isfixedAmount"
													  autocomplete="off" >
				 									<option value="1"><s:text name="tgi.label.yes" /></option>
													<option value="0"><s:text name="tgi.label.no" /></option>
												 </select></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_amount"/>
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" class="form-control input-sm" id="markupAmount"
									name="markupAmount" placeholder="amount" autocomplete="off" value="${tourMarkup.markupAmount}"></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.position_of_markup"/>
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="number" class="form-control input-sm" name="positionMarkup"
									autocomplete="off" value="${tourMarkup.positionMarkup}"></div></div>
											</div>
										</div>
									</div>
										
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.start_date" />
												</label>
											<div class="controls">
												<div class="form-group"><div class="">
												<input type="text" id="datepicker_arr"
								class="form-control input-sm" name="checkInDateTr" autocomplete="off"
								placeholder="ALL"  value="${spysr:formatDate(tourMarkup.checkInDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}" >
													</div></div>
												</div>
											</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.end_date" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
													<input type="text" id="datepicker_dep"
								class="form-control input-sm" name="checkOutDateTr" autocomplete="off" value="${spysr:formatDate(tourMarkup.checkOutDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"
								placeholder="ALL"  >
											</div></div>
										</div>
									</div>
										
									
					</div>
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_startdate" />
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" id="datepicker_PromofareStart"
								class="form-control input-sm" name="promofareStartDateTr"  value="${spysr:formatDate(tourMarkup.promofareStartDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"  autocomplete="off"
								placeholder="ALL" ></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_enddate" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
													<input type="text" id="datepicker_PromofareEnd"
								class="form-control input-sm" name="promofareEndDateTr"  value="${spysr:formatDate(tourMarkup.promofareEndDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"  autocomplete="off"
								placeholder="ALL" >
											</div></div>
										</div>
									</div>
										
									
					</div>  
                                  
                                    
                                    <div class="form-group table-btn">
                                        <div class="col-sm-10 col-sm-offset-2 ">
                                            <!-- <div class="pull-left"><button class="btn btn-danger" type="submit">Clear Form</button></div> -->
                                            <div class="pull-right">
                                                
                                                <input type="hidden" name="id" value="${tourMarkup.id}">
                                                <input type="hidden" name="createdbyUserId" value="${tourMarkup.createdbyUserId}" />
                                                <input type="hidden" name="createdbyCompanyId" value="${tourMarkup.createdbyCompanyId}" />
                                                <input type="hidden" name="createDate" value="${tourMarkup.createdDate}" />
                                                 <button class="btn btn-default" type="reset"><s:text name="tgi.label.reset" /></button>
                                                <button class="btn btn-primary " type="submit"><s:text name="tgi.label.update" /></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
						
 </div></div>
							<br>
                                
                                   
                                </div>
                            </div>
                        </div>
</div></div>
<script type="text/javascript">
$(document).ready(function()
	{
		$("#configData").val($("#configDataHdn").val());		
	});

</script>
<script>
                	  $(".airportList").autocomplete({
                          minChars: 3,
                          source: "city_autocomplete"
          										});
						</script>
                
            </section>
        <!--bootstrap vadidator  -->
<script type="text/javascript">
$(document).ready(function() {
$('#tour-markup-update')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				
			 configData : {
					message : 'ConfigData Role is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a ConfigData  '
						},
					}
				},
				name : {
					message : 'MarkUp Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a MarkUp Name'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'MarkUp Name  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				tourOrigin : {
					message : 'TourOrigin Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a TourOrigin'
						},
						stringLength : {
							min : 3,
							max : 100,
							
							message : 'TourOrigin Name  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				tourDestination : {
					message : 'TourDestination Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a TourDestination'
						},
						stringLength : {
							min : 3,
							max : 100,
							
							message : 'TourDestination Name  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				isaccumulative : {
					message : 'isaccumulative  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a isaccumulative'
						},
					}
				},
				isfixedAmount : {
					message : 'IsfixedAmount  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a IsfixedAmount'
						},
					}
				},
				markupAmount : {
					message : 'Amount Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Amount'
						},
						  numeric: {
	                            message: 'The value is not a number',
	                            // The default separators
	                            thousandsSeparator: '',
	                            decimalSeparator: '.'
	                        }
						
					}
				},
				positionMarkup : {
					message : 'positionMarkup Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a position markup'
						},
					}
				},
			
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {
	/* notifySuccess(); */
	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>
        <!--ADMIN AREA ENDS-->

  	   <s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>