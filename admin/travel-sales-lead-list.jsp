<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
@media screen and (max-width:480px){
.detail-btn {
	margin-top: -27px;
    margin-left: 15px;
}
}
.center {
	text-align: center;
}

.btn-xsm, .btn-group-xs>.btn {
	padding: 2px 6px;
	font-size: 13px;
	line-height: 1.5;
	border-radius: 2px;
}

a.collapsed {
	color: #02214c;
	text-decoration: none !important;
}

a.collapsed:hover {
	color: #337ab7;
	text-decoration: none !important;
}
span.text-spysr:hover {
	color: #3a7df9;
}
button.btn.dropdown-toggle.bs-placeholder.btn-default {
    height: 31px;
}
.alertfy-title-msg{
    margin: 0px;
    padding: 0px;
    color: #000;
    font-size: .9375rem;
    font-weight: 500;
    line-height: 1.5;
}
</style>

   <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">
                <div class="">
                    <div class="">
                        <div class="card1">
                        <div class="pnl">
                        <div class="hd clearfix">
					<h5 class="pull-left">
					<c:choose>
					<c:when test="${employeeReferralId >0}">
						My Referral Travel Leads Receive
					</c:when>
					<c:when test="${assignToFlagId >0}">
						Assign To Me Travel Leads Receive
					</c:when>
					<c:otherwise>
					<img class="clay" src="admin/img/svg/tourist.svg" width="26" alt="Higest"> Travel Sales Lead 
					</c:otherwise>
					</c:choose>
					
					</h5>
					<div class="set pull-right hidden-xs" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a href="listTravelSalesLead" class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
				</div>
                                 <br>
                         		<form action="listTravelSalesLeadFilter" class="filter-form" id="resetform" method="post">
                         		<input type="hidden" name="filterFlag" id="filterFlag" value="true">
								<div class="" id="filterDiv" style="display: none;">
								<div class="form-group">
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<select name="leadGenerateSource" id="created-by" class="form-control input-sm input-sm">
											<option value=""  selected="selected">Select Lead Source</option>
											<option value=""><s:text name="tgi.label.all" /></option>
											<option value="IBEUSER">IBE</option>
											<option value="ADMIN"><s:text name="tgi.label.admin" /></option>
										</select>
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="createdDateRange" id="created-date-range" placeholder="Search by created between...." class="form-control input-sm search-query" />
								 </div></div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="assignDateRange" id="assign-date-range" placeholder="Search by assign between...." class="form-control input-sm search-query" />
								</div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="leadNumber" id="lead-number-json" placeholder="Search By Lead Number" class="form-control input-sm search-query" />
							   </div>
							   </div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="firstName" id="name-json" placeholder="Search By Name" class="form-control input-sm search-query" />
							   </div>
							   </div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="email" id="email" placeholder="Search By Email" class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="phone" id="phone" placeholder="Search By Phone" class="form-control input-sm search-query" />
								</div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="city" id="city-json" placeholder="Search By City" class="form-control input-sm search-query" />
								</div>
								</div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="country" id="country-json" placeholder="Search By Country" class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="departureDateFlag" id="departure-date" placeholder="Search By Departure Date..." class="form-control input-sm" />
								 </div></div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="destinationFrom" id="destination-from" placeholder="Search By Departure From..." class="form-control input-sm search-query" />
								 </div></div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="destinationTo" id="destination-to" placeholder="Search By Departure To..." class="form-control input-sm search-query" />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select aria-hidden="true" name="leadSource" id="leadSource" class="form-control input-sm selectpicker">
									    <option value="">Search By Lead Source*</option>
										<option value="Advertisement">Advertisement</option>
										<option value="CustomerEvent">Customer Event</option>
										<option value="Employee Referral">Employee Referral</option>
										<option value="Google AdWords">Google AdWords</option>
										<option value="Website">Website</option>
										<option value="Socal Network">Socal Network</option>
										<option value="Walk-In">Walk-In</option>
										<option value="Telephone">Telephone</option>
										<option value="B2B">B2B</option>
										<option value="Justdial">Justdial</option>
										<option value="Others">Others</option>
									</select>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select aria-hidden="true" name="callTime" id="callTime" class="form-control input-sm">
								  <option value="" selected="SELECTED"> Search By Call Time</option>
									<option value="morning">Morning</option>
									<option value="afternoon">Afternoon</option>
									<option value="evening">Evening</option>
									<option value="anytime">Any time</option>
								</select>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select name="isEmailVerify" id="terminate" class="form-control input-sm">
								<option value="" selected="selected">Search By Email Verify</option>
								<option value="true"><s:text name="tgi.label.yes" /></option>
                                <option value="false" ><s:text name="tgi.label.no" /></option>
                                </select>
								 </div>
							   </div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select aria-hidden="true" name="leadTitle" id="lead-title" class="form-control selectpicker input-sm">
								  <option value="">Search By Lead Title</option>
									<c:forEach items="${followupStatusMap}" var="title">
										<option value="${title.key}">${title.value}</option>
									</c:forEach>
								 </select>
								 </div>
							   </div>
							<div class="row">
							 <div class=""></div>
							   <div class="col-md-1">
								<div class="form-group">
								<button type="reset" class="btn btn-danger btn-sm btn-block" id="configreset">&nbsp;Clear&nbsp;</button>
							</div>
							</div>
								<div class="col-md-1">
								<div class="form-group">
								<button type="submit" class="btn btn-info btn-sm btn-block" value="Search" >Search</button>
							</div>
							</div>
							</div>
						</div></div>
				</form>
                            
                                   
<div class="dash-table">
			<div class="content mt-0 pt-0">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"  role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
                                        <thead>
                                        <tr class="table-sub-header">
                                        	
                                        	<th colspan="16" class="noborder-xs">
                                        	<div class="pull-left">
													<div class="btn-group">
														<div class="dropdown pull-left" style="margin-right: 5px;" data-toggle="tooltip" data-placement="top" title="Lead Status : New">
															<a class="btn btn-sm btn-default" href="listTravelSalesLead">
															<span class=""><strong>All &nbsp;( ${travelSalesLeadList.size()} )</strong></span></a>
														</div>
														<div class="dropdown pull-left" style="margin-right: 5px;" data-toggle="tooltip" data-placement="top" title="Lead Status : New">
															<a class="btn btn-sm btn-default" href="listTravelSalesLeadFilter?leadStatus=New&filterFlag=true">
															<span class="notice-info"><strong>New &nbsp;( ${leadStatusUtil.newstatus} )</strong></span></a>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status: Active">
																<a class="btn btn-sm btn-default" href="listTravelSalesLeadFilter?leadStatus=Active&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-active"><strong>Active &nbsp;( ${leadStatusUtil.active} )</strong></span>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status: Booked">
																<a class="btn btn-sm btn-default" href="listTravelSalesLeadFilter?leadStatus=Booked&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-success"><strong>Booked &nbsp;( ${leadStatusUtil.booked} )</strong></span>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status:No Connect">
																<a class="btn btn-sm btn-default" href="listTravelSalesLeadFilter?leadStatus=No Connect&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-warning"><strong>No Connect &nbsp;( ${leadStatusUtil.noconnect} )</strong></span>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status:No Connect">
																<a class="btn btn-sm btn-default" href="listTravelSalesLeadFilter?leadStatus=Closed&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-danger"><strong>Closed &nbsp;( ${leadStatusUtil.closed} )</strong></span>
																</a>
															</div>
														</div>
														<span class="line-btn">  | </span>
														<div class="dropdown pull-right" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Filter By Lead Title">
																<select aria-hidden="true" name="leadTitle" id="lead-title-filter" class="form-control input-sm btn btn-sm btn-outline-success " style="font-size:13px">
																  <option value="">Search By Lead Title</option>
																	<c:forEach items="${followupStatusMap}" var="title">
																		<option value="${title.key}">${title.value}</option>
																	</c:forEach>
																 </select>
															</div>
														</div>
													</div>
											</div>
                                        	
                                        	<div class="pull-right">   
                                        	<div class="btn-group"> 
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="Add New Quick Lead">
											  <a class="btn btn-sm btn-default " href="#" data-toggle="modal" data-target="#add_travel_lead_quick" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"> Add Lead</a> 
											</div>
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Travel Sales Lead">
											  <a class="btn btn-sm btn-default" href="addTravelSalesLead" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class="">&nbsp;New&nbsp;</span></a> 
											</div>
											
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a  class="btn btn-sm btn-default filterBtn" data-toggle="dropdown"  style="margin-bottom: 2px;margin-right: 3px;" title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
												<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
	                                        <!-- <tr class="table-sub-header">
																<th colspan="1" class="noborder-xs"> <div class="center">Select</div></th>
																<th colspan="6" class="noborder-xs"> <div class="center">Basic Detail</div></th>
																<th colspan="2" class="noborder-xs"> <div class="center">Date</div></th>
																<th colspan="2" class="noborder-xs"> <div class="center"></div></th>
																<th colspan="1" class="noborder-xs"><div class="center"><i class="fa fa-cog fa-lg"></i></div></th>
															</tr> -->
										<tr class="border-radius border-color" role="row">
                                                   	<th data-priority="2"></th>
                                                   	<th style="width: 50px;" data-priority="1">
															<div class="center">
															<div data-toggle="tooltip" data-placement="top" title="Select All">
															<div class="checkbox checkbox-default">
										                        <input id="master_check" data-action="all" type="checkbox" >
										                        <label for="master_check">&nbsp;</label>
										                    </div>
										                    </div>
										                    </div>
													</th>
                                                    
                                                    <th data-priority="3">LE Number</th>
                                                    <th data-priority="4">Full Name</th>
                                                    <th data-priority="5"><s:text name="tgi.label.phone" /></th>
                                                    <th data-priority="6"><s:text name="tgi.label.email" /></th>
                                                     <th data-priority="8">Journey Date</th>
                                                     <th class="" data-priority="7">From -To</th>
                                                     <th><s:text name="tgi.label.adult" /></th>
                                                     <th>Duration</th>
                                                     <!-- <th class="col-md-1" style="width: 70px">Owner</th>
                                                     <th class="col-md-1">Assign At</th> -->
                                                     <th class="" style="width:145px">Created Date</th>
                                                     <th class="">Source</th>
                                                     <th class=""><span data-toggle="tooltip" data-placement="top" title="Email Verify Status"><img class="clippy" src="admin/img/svg/mail.svg" width="16" alt="Mail" style="margin-bottom: 3px;"></span></th>
                                                     <th class=""><span data-toggle="tooltip" data-placement="top" title="Lead Priority">LP</span></th>
                                                     <th style="width: 22px;"><div class=""><img class="clippy" src="admin/img/svg/settings.svg" width="16" alt="Settings" style="margin-bottom: 3px;"></div></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
   											<s:if test="travelSalesLeadList.size > 0">
                                           <s:iterator value="travelSalesLeadList" status="rowstatus">
                                                <tr>
                                                <td scope="row" data-title="S.No" 
                                                	class="<c:choose>
													<c:when test="${travelSalesLeadFollowUp.leadStatus == 'New'}">
													lead-status-new
													</c:when>
													<c:when test="${travelSalesLeadFollowUp.leadStatus == 'Active'}">
													lead-status-active
													</c:when>
													<c:when test="${travelSalesLeadFollowUp.leadStatus == 'Booked'}">
													lead-status-booked
													</c:when>
													<c:when test="${travelSalesLeadFollowUp.leadStatus == 'No Connect'}">
													lead-status-no-connect
													</c:when>
													<c:when test="${travelSalesLeadFollowUp.leadStatus == 'Closed'}">
													lead-status-closed
													</c:when>
													<c:otherwise>
													lead-status-none
													</c:otherwise>
													</c:choose>"><div class="center"><s:property value="%{#rowstatus.count}" /></div>
													</td>
                                                <td data-title="Select" class="select-checkbox">
																<div class="center">
																<div class="checkbox checkbox-default">
										                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${id}" class="check_row">
										                        <label for="checkbox1_sd_${rowstatus.count}"></label>
										                   		</div>
										                   		</div>
														</td>
                                                    <td class="link"><a href="editTravelSalesLead?id=${id}">${leadNumber}</a></td>
                                                    <td><span class="">
                                                    <a href="editTravelSalesLead?id=${id}" data-toggle="modal" class="text-primary">${firstName}</a>
                                                    </span>
                                                    </td>
                                                    <td data-title="Phone">
                                                    <a href="https://api.whatsapp.com/send?phone=${phone}&text=hi ${firstName}"><img class="clippy" src="admin/img/svg/whatsapp.svg" width="16"style="margin-bottom: 3px;"></a>&nbsp;&nbsp;
                                                    <a href="tel:${phone}"><img class="clippy" src="admin/img/svg/telephone-g.svg" width="16"style="margin-bottom: 3px;"></a>
                                                    &nbsp;&nbsp;${phone}
                                                    </td>
                                                    <td data-title="Email"><a href="mailto:${email}?subject=${title}&body= Hi ${firstName}">${email != null && email != ''?'<img class="clippy" src="admin/img/svg/envelope-g.svg" width="16"style="margin-bottom: 3px;">':'N/A'}&nbsp;&nbsp;${email}</a></td>
                                                    <td>
                                                    <c:choose>
                                                    <c:when test="${departureDate != null}">
                                                    ${spysr:formatDate(departureDate,'yyyy-mm-dd', 'mm/dd/yyyy')}
                                                    </c:when>
                                                    <c:otherwise>
													N/A                                                    
                                                    </c:otherwise>
                                                    </c:choose>
                                                     </td>
                                                    <td>${destinationFrom} - ${destinationTo}</td>
                                                    <td>${adult}</td>
                                                    <td>
                                                    <c:choose>
                                                    <c:when test="${duration != null && duration != ''}">
                                                    ${duration}
                                                    </c:when>
                                                    <c:otherwise>
                                                    None
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                   <%-- 	<td class="text-capitalize">
                                                   		${travelSalesLeadFollowUp.assignedToName != null && travelSalesLeadFollowUp.assignedToName != '' ?travelSalesLeadFollowUp.assignedToName :'None'}
                                                    </td>
                                                    <td class="text-capitalize">
                                                    <c:choose>
                                                    <c:when test="${travelSalesLeadFollowUp.assignDate != null}">
                                                    ${spysr:formatDate(travelSalesLeadFollowUp.assignDate,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}
                                                    </c:when>
                                                    <c:otherwise>
                                                    N/A
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td> --%>
                                                    
                                        	        <td data-title="Action">${spysr:formatDate(createdDate,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}</td>
                                        	        <td>
                                                    <c:choose>
                                                    <c:when test="${leadGenerateSource != null && leadGenerateSource != ''}">
                                                    <span>${leadGenerateSource}</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                    <span>N/A</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                    <td>
                                                    <c:choose>
                                                    <c:when test="${verifyStatus != false}">
                                                    <img class="clippy" src="admin/img/svg/checkedg.svg" width="14" alt="Copy to clipboard">
                                                    </c:when>
                                                    <c:otherwise>
                                                     <img class="clippy" src="admin/img/svg/close.svg" width="10" alt="Copy to clipboard">
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                    <td>
                                                    <c:choose>
														<c:when test="${travelSalesLeadFollowUp.leadRating == 'Highest'}">
														<span data-toggle="tooltip" data-placement="top" title="Highest">
															<img class="clay" src="admin/img/svg/highest.svg" width="17" alt="Higest">
														</span>
														</c:when>						
														<c:when test="${travelSalesLeadFollowUp.leadRating == 'High'}">
														<span data-toggle="tooltip" data-placement="top" title="High">
														<img class="clay" src="admin/img/svg/high.svg" width="17" alt="High">
														</span>
														</c:when>						
														<c:when test="${travelSalesLeadFollowUp.leadRating == 'Medium'}">
														<span data-toggle="tooltip" data-placement="top" title="Medium">
														<img class="clay" src="admin/img/svg/medium.svg" width="17" alt="Medium">
														</span>
														</c:when>						
														<c:when test="${travelSalesLeadFollowUp.leadRating == 'Low'}">
														<span data-toggle="tooltip" data-placement="top" title="Low">
														<img class="clay" src="admin/img/svg/low.svg" width="17" alt="Low">
														</span>
														</c:when>						
														<c:when test="${travelSalesLeadFollowUp.leadRating == 'Lowest'}">
														<span data-toggle="tooltip" data-placement="top" title="Lowest">
															<img class="clay" src="admin/img/svg/lowest.svg" width="17" alt="Lowest">
														</span>
														</c:when>	
														<c:otherwise>
														<strike><span data-toggle="tooltip" data-placement="top" title="None" style="    font-size: 14px;">&nbsp;N</span></strike>
														</c:otherwise>					
														</c:choose>
                                                    </td>
                                        	        <td data-title="Action">
													<div class="dropdown pull-right" style="">
													  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
													  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
													  <ul class="dropdown-menu">
													    <li class="action"><a href="editTravelSalesLead?id=${id}">Edit</a></li>
													    <s:if test="%{#session.User.userRoleId.isSuperUser() && #session.User.userRoleId.isAdmin()}">
													    <li class="action">
													    <a class="a-tag" href="#" data-lead-id="${id}" data-lead-follow-id="${travelSalesLeadFollowUp.id}" onclick="javascript:delteSalesLead(this)">
																<i class="icon-trash"></i> &nbsp;Delete
														</a>
													    </s:if>
													    <li class="action">
													    <a href="#!" class="plus-icon" data-event="duplicate" data-toggle="modal" data-target="#assign_lead_${rowstatus.count}" 
													       data-tour-id="${id}">Assign User</a>
													    </li>
													    <li class="action">
													    <a href="#!" class="plus-icon" data-event="duplicate" data-toggle="modal" data-target="#assign_sales_lead_${rowstatus.count}" 
													        data-tour-id="${id}">Publish To Others </a>
													    </li>
													  </ul>
													</div>
													</td>  
													<td></td>
														 </tr>
	<!--##################### Form for make a change owner ###################################-->
		<div class="modal fade" id="assign_lead_${rowstatus.count}" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title text-center">Assign To : <span class="text-warning">${leadNumber}</span></h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
								</button>
							</div>
							<form id="saveAssignLeadForm_${rowstatus.count}" method="post" class="bv-form">
								<div class="modal-body">
								 <div class="row">
											<div class="col-md-12 mt-20">
												<div class="controls">
														<div class="form-group">
															<select name="assignTo" id="assign-to" class="form-control" data-live-search="true">
															<option value="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="user">
																		<option value="${user.userId}" ${user.userId == travelSalesLeadFollowUp.assignTo ? 'selected' : ''}>${user.firstName} ${user.lastName}</option>
															</c:forEach>
														
														</select>
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="checkbox">
							                        <input id="checkbox1_${rowstatus.count}" type="checkbox" name="sendMailToUser" value="false">
							                        <label for="checkbox1_${rowstatus.count}">&nbsp;Send notification email </label>
							                    </div>
						                    </div>
						                    <div class="col-md-12 mt-30">
						                    <p>The <strong>new owner</strong> will also become the owner of these records related to <strong><span class="is-owner"></span></strong> that are owned by you.</p>
						                    </div>
										</div>
								<input type="hidden" name="id" id="" value="${travelSalesLeadFollowUp.id}">
							</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-success assignToUserSubmitBtn" id="" data-modal-id="assign_lead_${rowstatus.count}" data-form-name="saveAssignLeadForm_${rowstatus.count}">Save</button>
								</div>
							</form>
						</div>

					</div>
				</div>
<!--##################### Form for make a sell lead owner ###################################-->
		<div class="modal fade" id="assign_sales_lead_${rowstatus.count}" role="dialog">
					<div class="modal-dialog ">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title text-center">Assign Lead :<span class="text-danger"> ${leadNumber}</span></h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
								</button>
							</div>
							<form id="assignTravelLeadForm_${rowstatus.count}" method="post" class="bv-form">
								<div class="modal-body">
								<%-- <div class="row">
									<div class="col-md-12">
									<div class="alert alert-danger hidden" role="alert">
									  <strong><i class="fa fa-exclamation-triangle fa-2x"></i></strong>&nbsp;&nbsp;<span class="success-msg"></span>
									</div>
									</div>
									</div> --%>
									<div id="message-alert_${rowstatus.count}"></div>
								 <div class="row mt-20">
											<div class="col-md-3">
												<div class="checkbox">
							                        <input id="checkbox2_${rowstatus.count}" type="checkbox" name="publishStatus" value="false">
							                        <label for="checkbox2_${rowstatus.count}">&nbsp;Publish </label>
							                    </div>
						                    </div>
											<div class="col-md-3">
												<div class="checkbox">
							                        <input id="checkbox3_${rowstatus.count}" type="checkbox" name="createLead" value="false">
							                        <label for="checkbox3_${rowstatus.count}">&nbsp;Create</label>
							                    </div>
						                    </div>
											<div class="col-md-3">
												<div class="checkbox">
							                        <input id="checkbox4_${rowstatus.count}" type="checkbox" name="rejectLead" value="false">
							                        <label for="checkbox4_${rowstatus.count}">&nbsp;Reject </label>
							                    </div>
						                    </div>
											<div class="col-md-3">
												<div class="checkbox">
							                        <input id="checkbox5_${rowstatus.count}" type="checkbox" name="closeLead" value="false">
							                        <label for="checkbox5_${rowstatus.count}">&nbsp;Close </label>
							                    </div>
						                    </div>
						                    <div class="col-md-12 mt-30">
												<div class="controls">
														<div class="form-group">
															<select name="publishCompanyIdArrray" id="" multiple="multiple" class="company-list">
																<c:forEach items="${companyVos}" var="company">
																	<option value="${company.companyId}">${company.companyName} : (${company.companyLoginId})</option>
																</c:forEach>
															</select>
														</div>
												</div>
											</div>
										</div>
										<input type="hidden" name="travelLeadId" id="travel_leadId" value="${id}">
							</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-success assign_travel_leadBtn" id="" data-modal-id="assign_sales_lead_${rowstatus.count}" 
											data-form-name="assignTravelLeadForm_${rowstatus.count}" data-alert-id="message-alert_${rowstatus.count}">Save</button>
								</div>
							</form>
						</div>

					</div>
				</div>
                                                </s:iterator>
                                            </s:if>
                                            </tbody>
											
                                        </table></div></div></div>
                                        <div class="row">
											<div class="col-md-2">
										    <div class="notice notice-sm notice-info">
										        <strong>New &nbsp;:</strong> The lead status is New
										    </div>
											<div class="notice notice-sm notice-active">
										        <strong>Active&nbsp;&nbsp;&nbsp;&nbsp;:</strong> 
										    </div>
										    <div class="notice notice-sm notice-success">
										        <strong>Booked &nbsp;:</strong> 
										    </div>
										    <div class="notice notice-sm notice-warning">
										        <strong>No Connect &nbsp;:</strong> 
										    </div>
										    <div class="notice notice-sm notice-danger">
										        <strong>Closed &nbsp;:</strong> 
										    </div>
											</div>
										</div>
                                        
                                       	</div>
										</div>
										<!-- ####################### Add Travel Lead Quick ####################### -->
										<div class="modal fade" id="add_travel_lead_quick" role="dialog">
										<div class="modal-dialog">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title">Add Quick Lead <span class="text-warning"></span></h4>
													<button type="button" class="close slds-modal__close" data-dismiss="modal">
													<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
													</button>
												</div>
												<form id="saveTravelSalesLeadForm" method="post" class="form-horizontal">
													<div class="modal-body">
													 <div class="row mt-2">
																<div class="form-group">
																	<label for="callTime" class="form-control-label col-md-4">First Name <span class="text-danger">*</span></label> 
																	<div class="col-md-8">
																	<div class="controls">
																				<input type="text" name="firstName" class="form-control input-sm" placeholder="" autocomplete="on">
																			</div>
																	</div>
																</div>
																<div class="form-group">
																<label for="callTime" class="form-control-label col-md-4">Last Name <span class="text-danger">*</span></label> 
																	<div class="controls">
																			<div class="col-md-8">
																				<input type="text" name="lastName" value="" class="form-control input-sm" placeholder="" autocomplete="on">
																			</div>
																	</div>
																	</div>
																<div class="form-group">
																<label for="callTime" class="form-control-label col-md-4">Phone <span class="text-danger">*</span></label> 
																	<div class="controls">
																			<div class="col-md-8">
																				<input type="text" name="phone" value="" class="form-control input-sm" placeholder="ex- +919999888877" autocomplete="on">
																			</div>
																	</div>
																	</div>
																<div class="form-group">
																<label for="callTime" class="form-control-label col-md-4">Email</label> 
																	<div class="controls">
																			<div class="col-md-8">
																				<input type="text" name="email" value="" class="form-control input-sm" placeholder="" autocomplete="on">
																			</div>
																	</div>
																	</div>
																<div class="form-group">
																<label for="callTime" class="form-control-label col-md-4">Lead Title <span class="text-danger">*</span></label> 
																	<div class="controls">
																			<div class="col-md-8">
																				<select aria-hidden="true" name="travelSalesLeadFollowUpData.title" id="title" class="form-control input-sm">
																				  <option value="">Add Title</option>
																					<c:forEach items="${followupStatusMap}" var="title">
																						<option value="${title.key}" ${title.key == travelSalesLeadFollowUpData.title ? 'selected': ''}>${title.value}</option>
																					</c:forEach>
																				 </select>
																			</div>
																	</div>
																	</div>
											                    <%-- <div class="col-md-12 mt-30">
											                    <p>The <strong>new owner</strong> will also become the owner of these records related to <strong><span class="is-owner"></span></strong> that are owned by you.</p>
											    	 			</div> --%>
												</div>
												</div>
													<div class="modal-simple-footer">
														<input type="hidden" name="travelSalesLeadFollowUpData.leadStatus" id="lead-status" value="New">
														<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
														<button type="submit" class="btn btn-success" id="" data-modal-id="add_travel_lead_quick" data-form-name="saveTravelSalesLeadForm">Save</button>
													</div>
												</form>
											</div>
					
										</div>
				</div>
                                    </div>
                            </div>
            </section>
        <!--ADMIN AREA ENDS-->
        

<!--AutoCompleteter  -->
<script>
$(function() {
	  $('input[name="departureDateFlag"]').daterangepicker({
	      autoUpdateInput: false,
	      singleDatePicker: true,
	      locale: {
	          cancelLabel: 'Clear'
	      },
	      "showDropdowns": true,
	  });
	  $('input[name="departureDateFlag"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('MM/DD/YYYY'));
	  });
	});
//multiple select for company
$(".company-list").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a Company",
		});
/*--assign travel sales lead method--*/
 	$(document).ready(function() {
		$(".assign_travel_leadBtn").click(function(e) {
			e.preventDefault();
			var formDeleteName = $(this).attr("data-form-name");
			var modalId = $(this).attr("data-modal-id");
			var alertId = $(this).attr("data-alert-id");
			$.ajax({
				url : "saveTravelSalesLeadAssign",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						$("#" + modalId).hide();
						alertify.success(jsonData.message.message);
						setTimeout(location.reload.bind(location), 1000);	
					}
	  				else if(jsonData.message.status == 'input'){
	  					alertify.error(jsonData.message.message);}
	  				else if(jsonData.message.status == 'error'){
	  					alertify.error(jsonData.message.message);}
					
					//notify js
				},
				error : function(request, status, error) {
					showModalPopUp("Sales Lead   can not be updated.", "e");
				}
			});
		});

	});
/*##########################################################*/

$('#lead-title-filter').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var leadtitle = this.value;
    
    if(leadtitle != null && leadtitle != '')
	{
		var url = '<%=request.getContextPath()%>/listTravelSalesLeadFilter';
		window.location.replace(url+'?leadTitle='+leadtitle+'&filterFlag='+true);
	}
	else{
		return false;
	}
});
 
/*##########################################################*/
	$(".assignToUserSubmitBtn").click(function(e) {
			e.preventDefault();
			var formDeleteName = $(this).attr("data-form-name");
			var modalId = $(this).attr("data-modal-id");
			$.ajax({
				url : "updateTravelSalesLeadUserOwner",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						$("#" + modalId).hide();
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);
						}
	  				else if(jsonData.json.status == 'error'){
	  					$("#" + modalId).hide();
	  					alertify.error(jsonData.json.message);}
				},
				error : function(request, status, error) {
					alertify.error("follow up call can't be updated,please try again");
				}
			});
		}); 
/*##########################################################*/
//filter Autocompatator 
var LeadNo={url:"getTravelSalesLeadJson.action?data=leadNumber",getValue:"number",list:{match:{enabled:!0}}};$("#lead-number-json").easyAutocomplete(LeadNo);
var CName={url:"getTravelSalesLeadJson.action?data=firstName",getValue:"firstName",list:{match:{enabled:!0}}};$("#name-json").easyAutocomplete(CName);
var Email={url:"getTravelSalesLeadJson.action?data=email",getValue:"email",list:{match:{enabled:!0}}};$("#email").easyAutocomplete(Email);
var Phone={url:"getTravelSalesLeadJson.action?data=phone",getValue:"phone",list:{match:{enabled:!0}}};$("#phone").easyAutocomplete(Phone);
var City={url:"getTravelSalesLeadJson.action?data=city",getValue:"city",list:{match:{enabled:!0}}};$("#city-json").easyAutocomplete(City);
var Country={url:"getTravelSalesLeadJson.action?data=country",getValue:"countryName",list:{match:{enabled:!0}}};$("#country-json").easyAutocomplete(Country);
var DFrom={url:"getTravelSalesLeadJson.action?data=destinationFrom",getValue:"destinationFrom",list:{match:{enabled:!0}}};$("#destination-from").easyAutocomplete(DFrom);
var DTo={url:"getTravelSalesLeadJson.action?data=destinationTo",getValue:"destinationTo",list:{match:{enabled:!0}}};$("#destination-to").easyAutocomplete(DTo);
			
/*--------------------------script for delete tour prices multiple-----------------------------*/

	function delteSalesLead(sel) {
				// confirm dialog
				 alertify.confirm("Are you sure delete this lead<br/>If you delete this lead you will lose all lead data", function () {
					 var leadId = $(sel).data("lead-id");
					 var followUpId = $(sel).data("lead-follow-id");
						$.ajax({
							url : "deleteTravelSalesLead?id="+leadId+"&followUpId="+followUpId,							
							type : "GET",
							dataType: 'json',
							success : function(jsonData) {
								if(jsonData.json.status == 'success'){
									alertify.success(jsonData.json.message);
									setTimeout(location.reload.bind(location), 1000);	
								}
				  				else if(jsonData.json.status == 'error'){
				  					alertify.error(jsonData.json.message, function(ev) {
				  					    alertify.error("Ok Try again");
				  					});
				  				}
							},
							error: function (request, status, error) {
								showModalPopUp("Somthing Wrong, please try again.","e");
							}
						});
				 }, function() {
					 alertify.error("You've clicked Cancel");
				 });
				
			}
 
$('#delete_all_price').on("click", function(event){
	// confirm dialog
	 var allVals = [];
	 $(".check_row:checked").each(function() {  
		 allVals.push($(this).attr('data-id'));
	 });  
		 var check = confirm("Are you sure you want to delete selected data?");
		 if(check == true)
		 {
			 var join_selected_values = []; 
			 join_selected_values = allVals.join(",");
			 $.ajax({
						url : "archiveMultipleTravelLead?travelLeadIds="+join_selected_values,
						type : "POST",
						dataType: 'html',
						success : function(data) {
							alertify.success("You've clicked OK Your data have successfully deleted");
							setTimeout("window.location.reload();", 1000);
						},
						error: function (request, status, error) {
							showModalPopUp("Lead can not be deleted.","e");
						}
				}); 
		 }
		 else
		 {
			 return false;	 
		 }
});

/*-------------------------------------------------------------------------*/
$(document).ready(function() {
    $("#saveTravelSalesLeadForm").bootstrapValidator({
        feedbackIcons: {
            valid: "fa fa-check",
            invalid: "fa fa-remove",
            validating: "fa fa-refresh"
        },
        fields:{
        	firstName:{message:"First Name is not valid",validators:{notEmpty:{message:"First Name is required field"},stringLength:{min:1,message:"First Name must be more than 3 and less than 100 characters long"}}},
        	lastName:{message:"Last Name is not valid",validators:{notEmpty:{message:"Last Name is required field"},stringLength:{min:1,message:"Last Name must be more than 3 and less than 100 characters long"}}},
        	phone:{message:"Phone is not valid",validators:{notEmpty:{message:"Phone is a required field"}}},
        	
        }
        
    }).on("error.form.bv", function(e) {}).on("success.form.bv", function(e) {
            e.preventDefault(), 
            notifySuccess();
            $.ajax({
                url: "saveTravelSalesLead",
                type: "POST",
                dataType: "json",
                data: $("#saveTravelSalesLeadForm").serialize(),
                success: function(jsonData) {
                	if(jsonData.message.status == 'success'){
                		alertify.success(jsonData.message.message);
	  					var url = '<%=request.getContextPath()%>/listTravelSalesLead';
						window.location.replace(url);	
                	}
	  				else if(jsonData.message.status == 'input'){
	  					showModalPopUp(jsonData.message.message,"w");}
	  				else if(jsonData.message.status == 'error'){
	  					showModalPopUp(jsonData.message.message,"e");}
                	
                        $("#save_travel_sales_leadBtn").removeAttr("disabled"), console.debug("button disabled ");
                    var s = $(e.target);
                    s.data("bootstrapValidator");
                    s.bootstrapValidator("disableSubmitButtons", !1).bootstrapValidator("resetForm", !0)
                },
                error: function(e, a, s) {
                    showModalPopUp("Travel Lead can not be Saved.", "e")
                }
            })
    }).on("status.field.bv", function(e, a) {
        a.bv.getSubmitButton() && (console.debug("button disabled "), a.bv.disableSubmitButtons(!1))
    })
});

</script>
	<script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>
<script type="text/javascript" src="admin/js/admin/multiple-archive.js"></script>