<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
@media screen and (max-width:480px) {
	.detail-btn {
		margin-top: -27px;
		margin-left: 15px;
	}
}

.center {
	text-align: center;
}

.btn-xsm, .btn-group-xs>.btn {
	padding: 2px 6px;
	font-size: 13px;
	line-height: 1.5;
	border-radius: 2px;
}

a.collapsed {
	color: #02214c;
	text-decoration: none !important;
}

a.collapsed:hover {
	color: #337ab7;
	text-decoration: none !important;
}

.highlight {
	background-color: #F44336;
}

.highlight-light {
	background-color: #ff6559;
}

.line-btn {
	padding: 5px 0 0 5px;
	font-size: 20px;
	color: #9E9E9E;
}

.clippy {
	margin-bottom: 3px;
}

span.text-spysr:hover {
	color: #3a7df9;
}
</style>
<section class="wrapper container-fluid">

	<div class="row">
		<div class="col-md-12">
			<div class="card1">
				<div class="pnl">
					<div class="hd clearfix">
						<h5 class="pull-left">Request Quotes List</h5>
						<%-- <div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="#collapseFive" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text">Show</span> Filter Bar <i class="fa fa-angle-up"></i></span></a>
					</div> --%>
					</div>
					 
					<div class="dash-table">
						<div class="content">
							<div id="example_wrapper" class="dataTables_wrapper">
								<table id="example" class="display dataTable responsive nowrap" role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
									<thead>
										<tr class="border-radius border-color" role="row">
											<th style="width: 50px;" data-priority="1">
												<div class="center">
													<div data-toggle="tooltip" data-placement="top"
														title="Select All">
														<div class="checkbox checkbox-default">
															<input id="master_check" data-action="all"
																type="checkbox"> <label for="master_check">&nbsp;</label>
														</div>
													</div>
												</div>
											</th>
											<th data-priority="2" style="width: 50px;">Sl No.</th>
											<!-- <th data-priority="3">Name</th> -->
											<th data-priority="4">Email</th>
											<th data-priority="6">Phone</th>
											<th data-priority="7">Where To</th>
											<th data-priority="8">Where From</th>
											<!-- <th data-priority="9">Duration</th> -->
											<!-- <th data-priority="10">Month</th> -->
											<th data-priority="11">No. Of Passengers</th>
											<!-- <th data-priority="12">Reason for Travel</th> -->
											<!-- <th data-priority="13">Party Size</th> -->
											<!-- <th data-priority="14">Accommodation</th> -->
											<!-- <th data-priority="15">Special Interest</th> -->
											<!-- <th data-priority="16">Where See Us</th> -->
											<th data-priority="17" class="text-center">Description</th>
											<!-- <th data-priority="18">Security Code</th> -->
											<!-- <th data-priority="19">Publish Domain</th> -->
											<th></th>
										</tr>
									</thead>
									<tbody>
										<s:if test="requestQuoteList.size > 0">
											<s:iterator value="requestQuoteList" status="rowstatus">
												<tr>
													
													<td data-title="Select" class="select-checkbox">
														<div class="center">
															<div class="checkbox checkbox-default">
																<input id="checkbox1_sd_${rowstatus.count}"
																	type="checkbox" value="false" data-id="${id}"
																	class="check_row"> <label
																	for="checkbox1_sd_${rowstatus.count}"></label>
															</div>
														</div>
													</td>
													<td scope="row" data-title="S.No"><div class="center">
															<s:property value="%{#rowstatus.count}" />
														</div></td>
													<%-- <td data-title="Name">${fullName}</td> --%>
													<td data-title="Email">${email}</td>
													<td data-title="Phone">${phone}</td>
													<td data-title="Where To">${whereTo}</td>
													<td data-title="Where From">${whereFrom}</td>
													<%-- <td data-title="Duration">${duration}</td> --%>
													<%-- <td data-title="Month">${month}</td> --%>
													<td data-title="No. Of Passengers">${numberOfPasseangers}</td>
													<%-- <td data-title="Reason for Travel">${reasonForTravel}</td> --%>
													<%-- <td data-title="Party Size">${partySize}</td> --%>
													<%-- <td data-title="Accommodation">${accommodation}</td> --%>
													<%-- <td data-title="Special Interest">${specialInterest}</td> --%>
													<%-- <td data-title="Where See Us">${whereSeeUs}</td> --%>
													<td data-title="Description">${description}</td>
													<%-- <td data-title="Security Code">${securityCode}</td> --%>
													<%-- <td data-title="Publish Domain">${publishDomain}</td> --%>
													<td></td>
												</tr>
											</s:iterator>
										</s:if>
									</tbody>

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
 
<script type="text/javascript" src="admin/js/admin/multiple-archive.js"></script>