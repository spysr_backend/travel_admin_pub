<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
@media screen and (max-width:480px) {
	.detail-btn {
		margin-top: -27px;
		margin-left: 15px;
	}
}

.center {
	text-align: center;
}

.btn-xsm, .btn-group-xs>.btn {
	padding: 2px 6px;
	font-size: 13px;
	line-height: 1.5;
	border-radius: 2px;
}

a.collapsed {
	color: #02214c;
	text-decoration: none !important;
}

a.collapsed:hover {
	color: #337ab7;
	text-decoration: none !important;
}

.highlight {
	background-color: #F44336;
}

.highlight-light {
	background-color: #ff6559;
}

.line-btn {
	padding: 5px 0 0 5px;
	font-size: 20px;
	color: #9E9E9E;
}

.clippy {
	margin-bottom: 3px;
}

span.text-spysr:hover {
	color: #3a7df9;
}
</style>
<section class="wrapper container-fluid">

	<div class="row">
		<div class="">
			<div class="card1">
				<div class="pnl">
					<div class="hd clearfix">
						<h5 class="pull-left">Contact Us List</h5>
						<%-- <div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="#collapseFive" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text">Show</span> Filter Bar <i class="fa fa-angle-up"></i></span></a>
					</div> --%>
					</div>
					 
					<div class="dash-table">
						<div class="content">
							<div id="example_wrapper" class="dataTables_wrapper">
								<table id="example" class="display dataTable responsive nowrap" role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
									<thead>
										<tr class="border-radius border-color" role="row">
											<th style="width: 50px;" data-priority="1">
												<div class="center">
													<div data-toggle="tooltip" data-placement="top"
														title="Select All">
														<div class="checkbox checkbox-default">
															<input id="master_check" data-action="all" type="checkbox"> <label for="master_check">&nbsp;</label>
														</div>
													</div>
												</div>
											</th>
											<th data-priority="2" style="width: 50px;">SNo.</th>
											<th data-priority="3" >Full Name</th>
											<th data-priority="4">Phone</th>
											<th data-priority="6">Email</th>
											<th data-priority="7">Subject</th>
											<th data-priority="7" style="width: 145px;">Created Date</th>
											<th data-priority="7" style="width: 64px;">Source</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<s:if test="contactUsList.size > 0">
											<s:iterator value="contactUsList" status="rowstatus">
												<tr>
													
													<td data-title="Select" class="select-checkbox">
														<div class="center">
															<div class="checkbox checkbox-default">
																<input id="checkbox1_sd_${rowstatus.count}"
																	type="checkbox" value="false" data-id="${id}"
																	class="check_row"> <label
																	for="checkbox1_sd_${rowstatus.count}"></label>
															</div>
														</div>
													</td>
													<td scope="row" data-title="S.No"><div class="center">
															<s:property value="%{#rowstatus.count}" />
														</div></td>
													<td data-title="First Name">${fullName}</td>
													<td data-title="Number">${phone}</td>
													<td data-title="Email">${email}</td>
													<td data-title="Subject">${subject}</td>
													<td data-title="Subject">${spysr:formatDate(createdAt,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}</td>
													<td data-title="Subject">${source != null && source != ''?source:"None"}</td>
													<td></td>
												</tr>
											</s:iterator>
										</s:if>
									</tbody>

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
 
<script type="text/javascript" src="admin/js/admin/multiple-archive.js"></script>