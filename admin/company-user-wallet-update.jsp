<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="dj" uri="/struts-dojo-tags"%>

<style>
.modal-header .close {
    font-size: 25px;
    margin-top: 0px !important;
    margin-right: 10px;
}
</style>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"userWalletList";
	/* $('#success').click(function() {
	 window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	}); */
	$('#alert_box_info_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box_info').hide();
			
		});
	  $('#cancel').click(function() {
		  window.location.assign(finalUrl); 
		   $('#error-alert').hide();
		  
		 });  
 });
 </script>

  <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->

            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.add_wallet_balance_to" /> <s:property value="Username"/></h5>
                               <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li><a href="userWalletList"><i class="fa fa-home"></i> <s:text name="tgi.label.view_all_transactions" />
										</a></li>
								</ul>
							</div>
						</div>
                               </div> 
                              
                               <%--  <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"	
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="showWalletUsers"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Company User Wallet List </a></li>
								</ul>
							</div>
						</div>
                               </div>  --%>
                            </div>
                            <br><br>
                           <div class="cnt cnt-table">
							<div class="no-table-css"> 
                            <form action="addAgentWallet" method="post" class="form-horizontal" id="addWalletForm" 
					name="myForm">
       							<input type="hidden" class="form-control input-sm"  
								name="userPreviousWalletBalance"    id="previousBalance" value="<s:property value="agentWallet.walletBalance"/>"
								required>
							<div class="cnt cnt-table">
							<div class="">
                                    <table class="table table-form">
                                        <thead>
                                            <tr>
                                                <th><s:text name="tgi.label.current_balance" /> :<s:property value="agentWallet.walletBalance"/></th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td>
                                                    <div class="field-name"><s:text name="tgi.label.amount" /></div>
                                                </td>
                                                <td><div class="form-group"><div class="col-md-12">
                                          <input type="text" class="form-control input-sm"  
								name="agentWallet.walletBalance"   id="validAmount"  autocomplete="off"
								required>
								 <input type="hidden" class="form-control input-sm"  
								name="id" placeholder="User Name"  
								required value="<s:property value="id"/>">  
								 <input type="hidden" class="form-control input-sm"  
								name="agentWallet.walletId" placeholder="User Name" autocomplete="off"
								required value="<s:property value="agentWallet.walletId"/>"/>
                                               </div></div> </td>
												<td>
                                                    <div class="field-name"><s:text name="tgi.label.currency" /></div>
                                                </td>
                                                <td>
                                                 <select class="form-control input-sm" name="agentWallet.currencyCode"
								id="currency" autocomplete="off" required>
								<option selected value="<s:property value="agentWallet.currencyCode"/>"><s:property value="agentWallet.currencyCode"/></option>
							</select>
                                                </td>
												</tr>
<tr><td>
                                                    <div class="field-name"><s:text name="tgi.label.transaction_type" /></div>
                                                </td>
                                                <td>
                                                    <select class="form-control input-sm" name="agentWallet.transactionType"
								id="agentWallet.transactionType" autocomplete="off" required>
								<option selected value="Credit"><s:text name="tgi.label.credit" /></option>
								<option  value="Debit"><s:text name="tgi.label.debit" /></option>
					</select>
                                                </td>
                                       </tr>                                               
                                        </tbody>
										
                                    </table>
									<div class="form-group table-btn">
                                        <div class="col-sm-10 col-sm-offset-2 ">
                                            
											<div class="pull-right">
                                           <input type="hidden" placeholder="Password" id="Password"
								class="form-control input-sm" 
								name="Password">
								
                                          <button type="button"  onclick="javascript:showPopUp();"    class="btn btn-primary btn-lg"><s:text name="tgi.label.add_Wallet" /></button>
                                            </div>
                                        </div>
                                    </div><br><br>
 </div></div></form>
							
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
            <div class="modal fade" id="updateBoxWallet" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content up-pass">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard"></button>
        <h4 class="modal-title custom_align text-center" id="Heading"><s:text name="tgi.label.enter_password" /></h4>
      </div>
          <div class="modal-body">
        <div class="form-group">
       		<!-- <label for="exampleInputAmount">Company Type</label> -->
						<!-- <label> Password</label> -->
						<div class="input-group">
							<input type="password" placeholder="Password" id="Password_1"
								class="form-control input-sm" 
								name="Password_1">
						</div>
					</div>
					 <div class="modal-footer text-center">
					 <button type="button" onclick="javascript:userWalletVerifyByPassword();" class="btn btn-success text-center"  ><span><s:text name="tgi.label.send" /></span> </button>
        <%-- <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button> --%>
      </div>
        </div>
       
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>
            
            
             	   <s:if test="hasActionErrors()">
                	   <div id="actionError"><s:actionerror /></div>
                	         <script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
                	  <script>
                	  $(document).ready(function() 
                			  {   
                	  $('#alert_box_info').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
                	  $("#actionError").hide();
	      			  $('#alert_box_info_body').text($("#actionError").text());
                			  });
							</script>
						</s:if>
						 <s:if test="hasActionMessages()">
						  <div id="actionMessage"><s:actionmessage /></div>
						       <script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
                	  <script>
                	  $(document).ready(function() 
                			  { 
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
                	  $("#actionMessage").hide();
  	      			  $('#alert_box_info_body').text($("#actionMessage").text());
                			  });
							</script>
						</s:if>
						
						<script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
						
						 <script type="text/javascript">
 function userWalletVerifyByPassword()
 {
	 var password = $("#Password_1").val();
	 console.debug(password);
	 $("#Password").val(password);
	 $("#addWalletForm").submit();
  }
 function showPopUp()
 {
	 if($("#validAmount").val()==null || $("#validAmount").val()== "" ){
		alert("Enter amount") 
	 }else{
	 $('#updateBoxWallet').modal({
	        show: 'true'
	    }); 
	 }
  }
 </script>
   <!--bootstrap vadidator  -->
<%--  <script type="text/javascript">
$(document).ready(function() {
$('#addWalletForm')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {
	/* notifySuccess(); */
	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script> --%>
        <!--ADMIN AREA ENDS-->
