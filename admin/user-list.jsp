<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.show_date_to_user {
    padding: 0px 15px;
    position: relative;
    width: 93%;
    top: -25px;
    left: 5px;
    pointer-events: none;
    display: inherit;
    color: #000;
}

.scrollbar
{
	margin-left: 30px;
	float: left;
	height: 300px;
	width: 65px;
	background: #F5F5F5;
	overflow-y: scroll;
	margin-bottom: 25px;
}

.force-overflow
{
	min-height: 450px;
}

#wrapper
{
	text-align: center;
	width: 500px;
	margin: auto;
}


#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}
</style>
   <!--************************************
              MAIN ADMIN AREA
   *************************************-->
        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">
                        <div class="card1" style="padding: 0px;">
                        <div class="pnl">
                        <div class="hd clearfix">
							<h5 class="pull-left">Employee List</h5>
							<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
								<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
							</div>
							<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
								<a class="collapsed " href="userList" id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
							</div>
							<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
						</div>
					<div class="row">
                                <form action="userList" class="filter-form form-horizontal" id="resetform" method="post">
								<div class="form-group mt-4" id="filterDiv" style="display: none;">
								<input type="hidden" name="filterFlag" value="true">
         						<s:if test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
                                <div class="col-md-2 form-group">
                                <select name="companyTypeShowData" id="companyTypeShowData" class="form-control input-sm">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="my"><s:text name="tgi.label.my_self" /></option>
                                <s:if test="%{#session.Company!=null}">
         						<s:if test="%{#session.Company.companyRole.isSuperUser()}">
                                 <option value="distributor"><s:text name="tgi.label.distributor" /></option>
                                 </s:if>
                                </s:if>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div>
                                </s:if>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="createdDateRange" id="created-date-range" placeholder="Search by Created Date..." class="form-control input-sm" />
								</div></div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="userName" id="username-json" placeholder="UserName..." class="form-control input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="firstName" id="firstname-json" placeholder="FirstName..." class="form-control input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="lastName" id="lastname-json" placeholder="LastName..." class="form-control input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="email" id="email-json" placeholder="Email..." class="form-control  input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="mobile" id="mobile-json" placeholder="Mobile..." class="form-control input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<div class="">
								<input type="text" name="userDetail.city" id="city-json" placeholder="City...." class="form-control input-sm" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="userDetail.countryName" id="country-json" placeholder="Country...." class="form-control input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="userDetail.address" id="address-json" placeholder="Employee Address...." class="form-control input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<select class="form-control input-sm" id="user-role" name="userRoleFlag">
									<option value="">Select User Role</option>
									<c:forEach items="${userRolesMap}" var="user_role" varStatus="rowStatus">
										<option value="${user_role.key}">${user_role.value}</option>
									</c:forEach>
								</select>
								</div>
                                <div class="col-md-2 col-sm-6 form-group">
                                <select name="verifyStatusFlag" id="verifyStatus" class="form-control input-sm">
                               	<option value="" selected="selected">Search By Verify Status</option>
                                <option value="VERIFY">Verify</option>
                                <option value="NOT_VERIFY">Not Verify</option> 
                                </select>
                                </div>
                                 <div class="col-md-2 col-sm-6 form-group">
                                <select name="flagTypeStatus" id="flagTypeStatus" class="form-control input-sm">
                               	<option value="" selected="selected">Select Account Status</option>
                                <option value="ACTIVE">Active</option>
                                <option value="IN_ACTIVE">Inactive</option> 
                                </select>
                                </div>	
                                <div class="col-md-2 col-sm-6 form-group">
                                <select name="flagTypeLock" id="flagTypeLock" class="form-control input-sm">
                               	<option value="" selected="selected">Select Locked &amp; Unlocked</option>
                                <option value="LOCKED">Locked</option>
                                <option value="UN_LOCKED">Unlocked</option> 
                                </select>
                                </div>
								<div class="col-md-1 form-group">
								<input type="reset" class="btn btn-danger btn-sm btn-block" id="configreset" value="Clear">
								</div>
								<div class="col-md-1 form-group">
								<input type="submit" class="btn btn-info btn-sm btn-block" value="Search" />
							</div>
						</div>
				</form>
                 </div>       
					<div class="row">
						<div id="list">
			<div class="content">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"  role="grid" aria-describedby="example_info" style="width: 100%;">
                                            <thead>
                                            <tr class="table-sub-header">
                                        	<th colspan="15" class="noborder-xs">
                                        	<div class="pull-left">
													<div class="btn-group">
														<div class="dropdown pull-right">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Filter By User Role">
																<select class="form-control input-sm" id="user-role-filter" name="userRoleFlag">
																	<option value="">Select User Role</option>
																	<c:forEach items="${userRolesMap}" var="user_role" varStatus="rowStatus">
																		<option value="${user_role.key}">${user_role.value}</option>
																	</c:forEach>
																</select>
															</div>
														</div>
													</div>
											</div>
                                        	<div class="pull-right">   
                                        	<div class="btn-group"> 
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Employee">
											  <a class="btn btn-sm btn-default" href="userRegister" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class="">&nbsp;New&nbsp;</span></a> 
											</div>
											<div class="dropdown pull-left" style="margin-right:5px;">
											  <a class="btn btn-sm btn-default " href="#" >&nbsp;Import&nbsp;</a> 
											</div>
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a  class="btn btn-sm btn-default filterBtn" data-toggle="dropdown" title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
                                                <tr class="border-radius border-color" role="row">
                                                	 <th data-priority="1"></th>
                                                	<th style="width: 50px;" data-priority="2">
															<div class="center">
															<div data-toggle="tooltip" data-placement="top" title="Select All">
															<div class="checkbox checkbox-default">
										                        <input id="master_check" data-action="all" type="checkbox" >
										                        <label for="master_check">&nbsp;</label>
										                    </div>
										                    </div>
										                    </div>
													</th>
                                                    <th data-priority="3"><s:text name="tgi.label.username" /></th>
                                                    <th data-priority="4"><s:text name="tgi.label.name" /></th>
                                                    <th data-priority="5"><s:text name="tgi.label.email" /></th>
                                                    <th data-priority="6"><s:text name="tgi.label.mobile" /></th>
                                                    <th data-priority="7"><s:text name="tgi.label.city" /></th>
                                                    <th data-priority="7"><s:text name="tgi.label.country" /></th>
                                                    <th data-priority="8">Mail Status</th>
                                                    <th data-priority="8">Verify Status</th>
													<!-- <th data-priority="10" style="width: 133px">Created Date</th> -->
                                                    <th data-priority="9"><s:text name="tgi.label.status" /></th>
													<th data-priority="10"><s:text name="tgi.label.lock" /></th>
													<th data-priority="11" style="width: 50px;">Wallet</th>
													<th data-priority="11" style="width: 47px"><img class="clippy" src="admin/img/svg/settings.svg" width="16" alt="Settings" style="margin-left: 10px;"></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
											<s:iterator value="usersList"  var="user" status="rowstatus">
											<c:choose>
											 <c:when test="${rowstatus.count == 0}">
											<tr role="row" class="even">
											<td data-title="Select" class="select-checkbox">
													<div class="center">
													<div class="checkbox checkbox-default">
							                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${id}" class="check_row">
							                        <label for="checkbox1_sd_${rowstatus.count}"></label>
							                   		</div>
							                   		</div>
											</td>
											</c:when> 
											<c:when test="${rowstatus.count%2 == 0}">
											<tr role="row" class="even">
											</c:when>	
											<c:otherwise>
											<tr role="row" class="odd">
											</c:otherwise>
											</c:choose>
											<td data-title="S.No"><div class="center"><s:property value="#rowstatus.count" /></div></td>
											<td data-title="Select" class="select-checkbox">
													<div class="center">
													<div class="checkbox checkbox-default">
							                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${id}" class="check_row">
							                        <label for="checkbox1_sd_${rowstatus.count}"></label>
							                   		</div>
							                   		</div>
											</td>
											<td data-title="Username" class="link"><a href="userEdit?id=<s:property value="id" />&userRoleId.roleid=<s:property value="userRoleId.roleid"/>">${userName != null && userName != '' ?userName:'N/A' }</a></td>
											<td data-title="Name">${firstName != null && firstName != ''?firstName:'None'} ${lastName != null && lastName != ''? lastName:''}</td>
											<td data-title="Email"><s:property value="email" /></td>
											<td data-title="Mobile">${mobile != null && mobile != '' ? mobile:'N/A'}</td>
											<td data-title="Country">${userDetail.city != null && userDetail.city != ''?userDetail.city:'N/A'}</td>
											<td data-title="Country">${userDetail.countryName != null && userDetail.countryName != ''?userDetail.countryName:'N/A'}</td>
											<td>
											<c:choose>
											<c:when test="${mailStatus == 1}"><span class="text-success"><i class="fa fa-check-circle"></i> Success</span>
											</c:when>
											<c:otherwise><span class="text-danger"><i class="fa fa-times-circle"></i> failed </span>
											</c:otherwise>
											 </c:choose>
											</td>
											<td>
											<c:choose>
											<c:when test="${verifyStatus != false}">
											<span class="text-success"><i class="fa fa-check-circle"></i> Verify</span>
											</c:when>
											<c:otherwise>
											<span class="text-danger"><i class="fa fa-times-circle"></i>  Not Verify</span>
											</c:otherwise>
											</c:choose>
											</td>
											<%-- <td>${spysr:formatDate(createdDate,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}</td> --%>
											<td class="block-highlight" ><label
													class="switch"> ${status}<input type="checkbox"
														data-id="${id}" class="activeStatus"
														<c:if test="${status ==true}">checked</c:if>>
														<span class="slider"></span>
												</label>
											</td>
											<td class="block-highlight">
											 <label class="switch"> ${locked}<input type="checkbox" data-id="${id}" class="locked"
														<c:if test="${locked ==true}">checked</c:if>>
														<span class="slider"></span>
												</label>
											</td>
											<td><span data-toggle="tooltip" data-placement="top" title="Add User Wallet">
												<a href="add_user_wallet?userId=${id}" class="btn btn-sm btn-default" style="padding: 3px 13px;border-radius: 5px;">
												<img class="clippy" src="admin/img/svg/wallet-money-orange.svg" width="16" alt="Wallet"></a>
											</span>
											</td>
											<td data-title="Action">
											<div class="dropdown pull-right" style="margin-right: 20px;">
											  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
											  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
											  <ul class="dropdown-menu">
											    <li class="action"><a href="userEdit?id=<s:property value="id" />">User Detail</a></li>
											    <li class="action"><a href="userDelete?id=${id}">Delete</a></li>
											    <li class="action">
											    	<a href="getUserWalletTransactionHistory?userId=${id}" class="plus-icon" >Wallet History</a>
											    </li>
											  </ul>
											</div>
											</td>
									<td></td>
									</s:iterator>
                                            </tbody>
                                        </table>
                                        </div></div></div></div>
                                        </div>
                                        </div>
            </section>
<script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>	
 <script>
 $('#user-role-filter').on('change', function (e) {
	    var optionSelected = $("option:selected", this);
	    var userRole = this.value;
	    
	    if(userRole != null && userRole != '')
		{
			var url = '<%=request.getContextPath()%>/userList';
			window.location.replace(url+'?userRoleFlag='+userRole+'&filterFlag='+true);
		}
		else{
			return false;
		}
	});
//user name
	var UserName = {
		url: "getUserJson.action?data=userName",
		getValue: "userName",
		
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#username-json").easyAutocomplete(UserName);
	
	// Employee name
	var FirstName = {
		url: "getUserJson.action?data=firstName",
		getValue: "firstName",
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#firstname-json").easyAutocomplete(FirstName);
	
	var LastName = {
		url: "getUserJson.action?data=lastName",
		getValue: "lastName",
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#lastname-json").easyAutocomplete(LastName);
	 // email
		var Email = {
			url: "getUserJson.action?data=email",
			getValue: "email",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#email-json").easyAutocomplete(Email);
		
		// phone
		var Phone = {
			url: "getUserJson.action?data=mobile",
			getValue: "mobile",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#mobile-json").easyAutocomplete(Phone);
		
		// country name
		var Address = {
			url: "getUserJson.action?data=address",
			getValue: "address",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#address-json").easyAutocomplete(Address);
		// country name
		var City = {
			url: "getUserJson.action?data=city",
			getValue: "city",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#city-json").easyAutocomplete(City);
		// country name
		var CountryName = {
			url: "getUserJson.action?data=country",
			getValue: "country",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#country-json").easyAutocomplete(CountryName);
	</script>	
	
	
	
	
	<script>
 $('.activeStatus').change(function () {
    var status = false;
    var id = $(this).data("id");
    if($(this).is(":checked")==false)
     {
      status= false;
      $(this).attr('checked', false);
     }
    else{
      status= true;
      $(this).attr('checked', true);
     }
    
    $.ajax({
    	url : "update_user_profile_status?status="+status+"&flagType=status&id="+id,
		type : "POST",
		dataType : 'json',
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
			}
			else if(jsonData.message.status == 'error'){
				 //alertify.set('notifier','position', 'top-right');
				 alertify.error(jsonData.message.message);
			}
			else if(jsonData.message.status == 'input'){
				 alertify.error(jsonData.message.message);
			}
			
		},
		error : function(request, status, error) {
			alertify.error("user can't be changed ,please try again");
		}
	});
    });
  </script>
 <script>
 $('.locked').change(function () {
    var status = false;
    var id = $(this).data("id");
    if($(this).is(":checked")==false)
     {
      status= false;
      $(this).attr('checked', false);
     }
    else{
      status= true;
      $(this).attr('checked', true);
     }
    $.ajax({
    	 url : "update_user_profile_status?locked="+status+"&flagType=lock&id="+id,
		type : "POST",
		dataType : 'json',
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
			}
			else if(jsonData.message.status == 'error'){
				 alertify.error(jsonData.message.message);
			}
			else if(jsonData.message.status == 'input'){
				 alertify.error(jsonData.message.message);
			}
			
		},
		error : function(request, status, error) {
			alertify.error("user can't be changed ,please try again");
		}
	});
    });

</script>
