<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--  <script src= https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js> </script>
    <script src= https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js> </script> --%>
<link
	href="admin/css/jquery-ui.css"
	rel="stylesheet" type="text/css" />

<title><s:property value="%{#session.ReportData.createdBy}" /></title>


		<!-- Main content -->
		
		 <section class="wrapper container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.hotel_passenger_report_details" /></h5>
                                 <div class="set pull-right">
                                   
								 <a class="btn btn-success btn-xs" href="hotelReportList"><span
								class="pull-right">
									<span class="glyphicon glyphicon-step-backward"></span><s:text name="tgi.label.back" /></span></a>
			
                                     </div></div>
                                     
		<section class="content">
			<!-- Small boxes (Stat box) -->
					
					
					<%-- <div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><s:text name="tgi.label.tour_info" />HOTEL INFO 1</b>
									</div></div><br> --%>

		<div class="table-responsive no-table-css clearfix">
									<table class="table table-striped table-bordered">
									<thead>
                                   <tr class="border-radius border-color">	
									<tr>
										<th><s:text name="tgi.label.order_id" /></th>
										<th><s:text name="tgi.label.booking_date" /></th>
										<th><s:text name="tgi.label.agency" /></th>
										<th><s:text name="tgi.label.reference_code" /></th>
										<th><s:text name="tgi.label.supplier" /></th>
									</tr></thead>
									<tr>
										<td data-title="<s:text name="tgi.label.order_id" />"><s:property value="CurrentHotelReport.orderRef" /></td>
										
										<td data-title="<s:text name="tgi.label.order_id" />">${spysr:formatDate(CurrentHotelReport.createdAt,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy')}</td>
										<%-- <td data-title="<s:text name="tgi.label.order_id" />"><s:property value="CurrentHotelReport.createdAt" /></td> --%>
										<td data-title="<s:text name="tgi.label.reference_code" />"><p>  ${CurrentHotelReport.createdBy}</p></td>
										<td data-title="<s:text name="tgi.label.reference_code" />"><p>  ${CurrentHotelReport.referenceCode}</p></td>
										<td data-title="<s:text name="tgi.label.supplier" />"><p>${CurrentHotelReport.apiProvider.vendorName}</p></td>
									</tr>
			</table></div>
		

		<br>
					<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><s:text name="tgi.label.hotal_info" /></b>
									</div></div><br>
									<div class="table-responsive no-table-css clearfix">
									<table class="table table-striped table-bordered">
									<thead>
                                   <tr class="border-radius border-color">	
									<tr>
										<th><s:text name="tgi.label.hotel_name" /></th>
										<th><s:text name="tgi.label.location" /></th>
										<th><s:text name="tgi.label.checkin" /></th>
										<th><s:text name="tgi.label.checkout" /></th>
										<th><s:text name="tgi.label.state" /></th>
										<th><s:text name="tgi.label.country" /></th>
										<!-- <th>Hotel_type</th> -->
										<th><s:text name="tgi.label.hotel_category" /></th>

									</tr></thead>
									<tr>
										<td data-title="Hotel_name"><s:property value="CurrentHotelReport.hotelName" /></td>
										<td data-title="Location"><s:property value="CurrentHotelReport.hotel_loc" /></td>
										<td data-title="CheckIn"><s:property value="CurrentHotelReport.checkInDate" /></td>
										<td data-title="CheckOut"><s:property value="CurrentHotelReport.checkOutDate" /></td>
										<td data-title="State"><s:property value="CurrentHotelReport.state" /></td>
										<td data-title="Country"><s:property value="CurrentHotelReport.country" /></td>
									<%-- 	<td data-title="Hotel_type"><s:property value="CurrentHotelReport.hotelType" /></td> --%>
										<td data-title="Hotel_category"><s:property value="CurrentHotelReport.hotelStars" /> Star</td>
									</tr>
			</table></div>
									
									<br>
					<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><s:text name="tgi.label.guest_info" /></b>
									</div></div><br>
									<div class="table-responsive no-table-css clearfix">
									<table class="table table-bordered table-striped-column table-hover">
									<thead>
                                   <tr class="border-radius border-color">
										<th><s:text name="tgi.label.s.no" /></th>
										<th><s:text name="tgi.label.gender" /></th>
										<th><s:text name="tgi.label.firstname" /></th>
										<th><s:text name="tgi.label.lastname" /></th>
										<th><s:text name="tgi.label.date_of_birth" /></th>
										

									</tr>
									</thead>
									<s:iterator value="roomGuestInfos" status="serial">
										<tr>

											<td data-title="S.No"><s:property value="%{#serial.count}" /></td>
													<td data-title="Gender"><s:property value="gender" /></td>
											<td data-title="Firstname"><s:property value="firstname" /></td>
											<td data-title="Lastname"><s:property value="lastname" /></td>
											<td data-title="DOB">${spysr:formatDate(DOB,'MM-dd-yyyy', 'MMM-dd-yyyy')}</td>
											

										</tr>
									</s:iterator>
									</table></div>
					
									<br>
									
									<div class="row">
									<div class="col-md-5" style="font-size:20px;">
											<b><s:text name="tgi.label.contact_info" /></b>
									</div></div><br> <div class="table-responsive no-table-css clearfix">
									<table class="table table-bordered table-striped-column table-hover">
									<thead>
                                   <tr class="border-radius border-color">
										<th><s:text name="tgi.label.title" /></th>
										<th><s:text name="tgi.label.name" /></th>
										<!-- <th>Lastname</th> -->
										<th><s:text name="tgi.label.mobile" /></th>
										<th><s:text name="tgi.label.email" /></th>
										<th><s:text name="tgi.label.address" /></th>
										
									</tr>
									</thead>
										<tr>
											<td data-title="Title">${hotelOrderRowData.orderCustomer.title}</td>
											 <td data-title="Name">${hotelOrderRowData.orderCustomer.firstName} ${hotelOrderRowData.orderCustomer.lastName}</td>
											<td data-title="Mobile">${hotelOrderRowData.orderCustomer.mobile}</td>
											<td data-title="Email">${hotelOrderRowData.orderCustomer.email}</td> 
											<td data-title="Address">${hotelOrderRowData.orderCustomer.address}</td>
											
										</tr>
									</table></div><br>
									<s:if test="insuranceDataList.size()>0">
									<div class="row">
									
									<div class="col-md-5" style="font-size:20px;">
									
											<h4>
												<s:text name="tgi.label.flight_insurance" />
											</h4>
									</div></div> <div class="table-responsive no-table-css clearfix">
									<table class="table table-bordered table-striped-column table-hover">
									<thead>
                                   <tr class="border-radius border-color">
										<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
										
									</tr>
									</thead>
									<s:iterator value="insuranceDataList">
										<tr>
											<td data-title="><s:text name="tgi.label.insurance_id" />">${insuranceId}</td>
											<td data-title="<s:text name="tgi.label.name" />">${insuranceTitle}</td>
											<td data-title="<s:text name="tgi.label.price" />">${insurancePrice}</td>
											<td data-title="<s:text name="tgi.label.description" />">${description}</td>
											
										</tr></s:iterator>
									</table>
									
									</div>
									</s:if>
									<s:else>
											<h4>
												<s:text name="tgi.label.flight_insurance" />
											</h4>
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="border-radius border-color">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.insurance_not_available" /></td>
												</tr>
											</table>
										</s:else>
									<br>
									
									
					<div class="row">
								<div class="col-md-5" style="font-size:20px;">
											<b><s:text name="tgi.label.payment_info" /></b>
									</div></div><br>
									<%-- <div class="row" style="border:1px solid #ddd;width: 100%;margin-left: auto;margin-right: auto;">
									<s:iterator value="roomGuestInfos" status="serial">	
									 <div class="col-md-1" style="border-right:1px solid #ddd"><b><s:text name="tgi.label.total_guests" /></b> <br><br> <p><s:property value="CurrentHotelReport.guests" /></p> </div>
									 <div class="col-md-2" style="border-right:1px solid #ddd"><b><s:text name="tgi.label.fee_amount" /></b> <br><br> <p><s:property value="CurrentHotelReport.fee_amount" /></p> </div>
									 <div class="col-md-2" style="border-right:1px solid #ddd"><b><s:text name="tgi.label.discount" /></b> <br><br> <p><s:property value="CurrentHotelReport.discount" /></p> </div>
									 <div class="col-md-1" style="border-right:1px solid #ddd"><b><s:text name="tgi.label.tax" /></b> <br><br> <p><s:property value="CurrentHotelReport.tax" /></p> </div>
									 <div class="col-md-2" style="border-right:1px solid #ddd"><b><s:text name="tgi.label.total" /></b> <br><br> <p><s:property value="CurrentHotelReport.total" /></p> </div>
									 <div class="col-md-2" style="border-right:1px solid #ddd"><b><s:text name="tgi.label.currency" /></b> <br><br> <p><s:property value="CurrentHotelReport.curCode" /></p> </div>
									 <div class="col-md-2" style="border-right:1px solid #ddd"><b><s:text name="tgi.label.payment_status" /></b> <br><br> <p><s:property value="CurrentHotelReport.paymentStatus" /></p> </div>
									</s:iterator>
									</div> --%>
									
									<div class="table-responsive no-table-css clearfix">
									<table class="table table-bordered table-striped-column table-hover">
									<thead>
                                   <tr class="border-radius border-color">
										<th><b><s:text name="tgi.label.total_guests" /></b></th>
										<th><b><s:text name="tgi.label.fee_amount" /></b></th>
										<th><b><s:text name="tgi.label.discount" /></b></th>
										<th><b><s:text name="tgi.label.tax" /></b></th>
										<th><s:text name="tgi.label.insurance_fee" /></th>
										<th><b><s:text name="tgi.label.total" /></b></th>
										<th><b><s:text name="tgi.label.payment_status" /></b></th>
									</tr></thead>
									<s:iterator value="CurrentHotelReport"
										status="serial">
										<tr>

											<td data-title="<s:text name="tgi.label.total_guests" />"><s:property value="CurrentHotelReport.guests" /></td>
											<td data-title="<s:text name="tgi.label.fee_amount" />"><s:property value="CurrentHotelReport.fee_amount" /></td>
											<td data-title="<s:text name="tgi.label.discount" />"><s:property value="CurrentHotelReport.discount" /></td>
											<td data-title="<s:text name="tgi.label.tax" />"><s:property value="CurrentHotelReport.tax" /></td>
											<td data-title="<s:text name="tgi.label.insurance_fee" />">
											<c:choose>
											<c:when test="${CurrentHotelReport.insurancePrice != null}">
											<s:property value="CurrentHotelReport.insurancePrice" />
											</c:when>
											<c:otherwise>
											<s:text name="tgi.label.n_a" />
											</c:otherwise>
											</c:choose>
											</td>
											<td data-title="<s:text name="tgi.label.total" />"><s:property value="CurrentHotelReport.total" /></td>
											<td data-title="<s:text name="tgi.label.payment_status" />"><s:property value="CurrentHotelReport.paymentStatus" /></td>
										</tr>
									</s:iterator>
									</table></div>
					
					
					
		<%-- 	<div class="row">
				<div class="col-sm-12 clearfix report-search">

			<!-- 		<form class="form-inline" action="" method="post"> -->
						 
						<div class="table-responsive dash-table">
  
							<!-- testing -->

							<div class="box clearfix">
								<!-- <div class="box-body"> -->

								<label for="Country"><h4>
										<b><s:property value="user" /></b>
									</h4></label>
									
										<div class="modal-body">
																	<table class="table table-striped table-bordered">
																<tr>
									<th colspan="4"><big><s:text name="tgi.label.created_by" /></big> : ${CurrentHotelReport.agencyUsername}</th>
									
										<th colspan="4"><big><s:text name="tgi.label.order_id" /></big> :<s:property value="CurrentHotelReport.orderRef" /></th>
										<th colspan="4"><big><s:text name="tgi.label.reference_code" /></big> : ${CurrentHotelReport.referenceCode}</th>
										<th colspan="4"><big><s:text name="tgi.label.supplier" /></big> : ${CurrentHotelReport.apiProvider.vendorName}</th>

									</tr>
																		
																		 <tr>
																		<th><h5><b><u><s:text name="tgi.label.hotal_info" /></u></b></h5></th>
																		
																			
																		</tr>
																		<tr>
																		 <th><s:text name="tgi.label.hotel_name" /></th>
																		 <th><s:text name="tgi.label.location" /></th>	
																		 <th><s:text name="tgi.label.state" /></th>
																		 <th><s:text name="tgi.label.country" /></th>	
																		  <th><s:text name="tgi.label.hotel_type" /></th>
																		  <th><s:text name="tgi.label.hotel_category" /></th>
																		 
																		  						
																		</tr>
																		<tr>
																			<th><s:property value="CurrentHotelReport.hotelName" /></th>
																			<th><s:property value="CurrentHotelReport.hotel_loc" /></th>
																			 <th><s:property value="CurrentHotelReport.state" /></th>
																			 <th><s:property value="CurrentHotelReport.country" /></th>
																			  <th><s:property value="CurrentHotelReport.hotelType" /></th>
																			<th><s:property value="CurrentHotelReport.hotel_cat" /></th>
																		 
																			 
																		
																		 </tr>
																		 <tr>
										<th><h5>
												<b><u><s:text name="tgi.label.guest_info" /></u></b>
											</h5></th>
									</tr>
									<tr>
										<th><s:text name="tgi.label.s_no" /></th>
										<th><s:text name="tgi.label.firstname" /></th>
										<th><s:text name="tgi.label.lastname" /></th>
										<th><s:text name="tgi.label.email" /></th>

									</tr>
									<s:iterator value="roomGuestInfos" status="serial">
										<tr>

											<th><s:property value="%{#serial.count}" /></th>
											<th><s:property value="firstname" /></th>
											<th><s:property value="lastname" /></th>
											<th><s:property value="email" /></th>

										</tr>
									</s:iterator>
																		 
																		 <tr>
																		<th><h5><b><u><s:text name="tgi.label.payment_info" /></u></b></h5></th>
																		
																			
																		</tr>
																		<tr>
																		<th><s:text name="tgi.label.total" /></th>
																		 <th><s:text name="tgi.label.discount" /></th>
																		  <th><s:text name="tgi.label.tax" /></th>
																		 <th><s:text name="tgi.label.fee_amount" /></th>	
																		 <th><s:text name="tgi.label.currency" /></th>	
																		  <th><s:text name="tgi.label.payment_status" /></th>
																		   <th><s:text name="tgi.label.total_guests" /></th>
																		  						
																		</tr>
																		<tr>
																			<th><s:property value="CurrentHotelReport.total" /></th>
																			<th><s:property value="CurrentHotelReport.discount" /></th>
																			 <th><s:property value="CurrentHotelReport.tax" /></th>
																			 <th><s:property value="CurrentHotelReport.fee_amount" /></th>
																			  <th><s:property value="CurrentHotelReport.curr" /></th>
																			  <th><s:property value="CurrentHotelReport.curCode" /></th>
																			<th><s:property value="CurrentHotelReport.paymentStatus" /></th>
																			<th><s:property value="CurrentHotelReport.guests" /></th>
																			 
																		
																		 </tr>
																		<tr>
																		<th><h5><b><u><s:text name="tgi.label.room_info" /></u></b></h5></th>
																		 </tr>
																		 <tr>
																		 <th><s:text name="tgi.label.s.no" /></th>
																		<th><s:text name="tgi.label.created_at" /></th>
																		 <th><s:text name="tgi.label.mealtype" /></th>	
																		 <th><s:text name="tgi.label.hotel_name" /></th>
																		  <th><s:text name="tgi.label.status" /></th>
																		 						
																		</tr>
																		<s:iterator value="#session.HotelRoomInfo" status="serial">
																		 <tr>
																		 <th><s:property value="%{#serial.count}" /></th>
																		<th><s:property value="createdAt" /></th>
																			  <th><s:property value="mealType" /></th>
																			  <th><s:property value="hotelName" /></th>
																		  <th><s:property value="status" /></th>
																		 						
																		</tr>
																		</s:iterator>
																		 <tr>
																		<th><h5><b><u><s:text name="tgi.label.hotel_name" />GUEST INFO</u></b></h5></th>
																		 </tr>
																		<tr>
																		  <th><s:text name="tgi.label.s.no" /></th>
																		  <th><s:text name="tgi.label.firstname" /></th>
																		 <th><s:text name="tgi.label.lastname" /></th>	
																		 <th><s:text name="tgi.label.email" /></th>	
																		    							
																		</tr>
																		   <s:iterator value="#session.HotelGuestInfo" status="serial">
																		 <tr>
																		
																		  <th><s:property value="%{#serial.count}" /></th>
																		<th><s:property value="firstname" /></th>
																		 <th><s:property value="lastname" /></th>	
																		 <th><s:property value="email" /></th>	
																		   		
																		</tr>
																		</s:iterator>
																		 <tr>
																		<th colspan="3"><h5><b><u><s:text name="tgi.label.cancellation_info" /></u></b></h5></th>
																		 </tr>
																		<tr>
																		  <th><s:text name="tgi.label.s.no" /></th>
																		  <th><s:text name="tgi.label.start" /></th>
																		 <th><s:text name="tgi.label.end" /></th>	
																		 <th><s:text name="tgi.label.currency" /></th>	
																		    <th><s:text name="tgi.label.amount" /></th>	
																		    <th><s:text name="tgi.label.Fee_amount" /></th>	
																		     <th><s:text name="tgi.label.fee_type" /></th>	
																		      <th><s:text name="tgi.label.remarks" /></th>	 	 							
																		</tr>
																		  <s:iterator value="#session.HotelcancellationPoliciyInfo" status="serial">
																		 <tr>
																		
																		  <th><s:property value="%{#serial.count}" /></th>
																		<th><s:property value="startDate" /></th>
																		 <th><s:property value="endDate" /></th>	
																		 <th><s:property value="curCode" /></th>	
																		   		 <th><s:property value="feeAmount" /></th>	
																		   		  <th><s:property value="formattedFeeAmount" /></th>	
																		   		   <th><s:property value="feeType" /></th>	
																		   		    <th><s:property value="remarks" /></th>	
																		</tr>
																		</s:iterator>
																		
																		 <tr>
																		<th colspan="5"><h4><b><s:text name="tgi.label.guests :" /> <s:property value="%{#session.HotelInfo.guests}" /></b></h4></th>
																		
																		<th   align="right"><h4><b><s:text name="tgi.label.total" />:<s:property value="%{#session.HotelInfo.total}"/><s:property value="%{#session.HotelInfo.curCode}"/></b></h4></th>
																		 </tr>  
																		 
																		 
																	</table>

																</div>


							</div>
							<!-- /.box -->

						</div>
						<!-- table-responsive -->
					<!-- </form> -->
				</div>



			</div> --%>.
			</section>
</div>
</div></div>
		</section>
	<!-- /.content-wrapper -->

	<script>
		$(document).ready(function() {
			$("#twodpd2").datepicker({
				dateFormat : "yy-mm-dd"
			});
			$("#twodpd1").datepicker({
				dateFormat : "yy-mm-dd"
			/*  changeMonth: true,
			 changeYear: true */
			});
		});
	</script>
	
	<%-- <script type="text/javascript">
		$(document).ready(
				function() {
					var table = $('#example').DataTable({
						lengthChange : false,
						   "searching": false,
						   "ordering": false,  
						   "paging": false,
						   "info": false,
						/* "pagingType" : "full_numbers",
						"lengthMenu" : [ 10, 25, 50, 75, 100, 500 ], */
						/* buttons : [ 'excel', 'pdf', 'print' ] */
					});

					table.buttons().container().appendTo(
							'#example_wrapper .col-sm-6:eq(0)');

				});

		/*  $(function () {
		 	$('#example').DataTable({
		    	 "paging": true,
		         "lengthChange": true,
		        "searching": true,
		        "ordering": true,  
		           "info": true,
		         "autoWidth": false,  
		        "search": {
		      	    "regex": true,
		      	  }, 
		      	 
		      "pagingType": "full_numbers",
		      "lengthMenu": [10, 25, 50, 75, 100, 500 ],
		     
		      
		     });  
		  
		   });   */
	</script> --%>


	<!-- 
 
  $(document).ready(function() 
    	 { 
    		 $("#twodpd1").datepicker({
    			 dateFormat: "yy-mm-dd"  
    			/*  changeMonth: true,
    			 changeYear: true */
    		 }); 
    		 }); -->

