<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>



        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
        <section id="main-area" class="main-admin-area">

            <!--HEADER BEGINS-->
            <header class="header clearfix">
                <div class="sidebar-toggle-btn">
                    <div class="fa fa-bars tooltips" data-original-title="Toggle Navigation"></div>
                </div>
                <div class="head-search">
                    <input type="search">
                </div>
                <div class="dropdown header-notification">
                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="fa fa-bell tooltips"></div>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                        <li><i class="fa fa-remove"></i><s:text name="tgi.label.system_notification" /></li>
                    </ul>
                </div>
            </header>
            <!--HEADER ENDS-->

            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.atlas_air_travels" /></h5>
                                <div class="set pull-left">
                                    <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button>
                                </div>
                                <div class="set pull-right">
                                    <div class="dropdown">
                                      <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <s:text name="tgi.label.dropdown" />
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#"><s:text name="tgi.label.action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.another_action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.something_else_here" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.separated_link" /></a></li>
                                      </ul>
                                    </div>
                               </div>
                            </div>
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.company_list" /></h5>
                            </div>
							<thead>
                                            <tr>
											
											<div class="form-group" id="user_form-group" align="right">

				<input type="hidden" value="" id="companyUserId">
					 <input type="hidden" value="yogesh@spysr.in" id="email">
				<input type="hidden" value="" id="user_id">
					
					 


				<form class="form-inline" action="filterCompanyList" method="post">
					<div class="form-group">
						<!-- <label for="exampleInputAmount">Company Type</label> -->
						<input type="hidden" value="" id="user_companyUserId" name="user_companyUserId">
						<div class="input-group">
							<input type="text" placeholder="Type Company Userid" class="form-control input-sm ui-autocomplete-input" id="search" value="" name="company_user_id" autocomplete="off">
						</div>
						<div class="input-group">
							<input type="text" placeholder="Type Emp Code" class="form-control input-sm ui-autocomplete-input" autocomplete="off" value="" id="userIdSearch" name="user_id">
						</div>
					</div>
					<div class="form-group rep-buto">
						<button type="submit" class="btn btn-primary"><s:text name="tgi.label.search" /></button>
					</div>
				</form>

			</div>
			<div class="form-group table-btn">
                                        <div class="col-sm-10 col-sm-offset-2 ">
                                            
											<div class="pull-right">
                                           <button class="btn btn-primary " type="submit"><s:text name="tgi.label.add_new" /></button>
                                            </div>
                                        </div>
                                    </div><br><br>
									
			<div class="form-group" id="user_form-group" align="right">

				<input type="hidden" value="" id="companyUserId">
					 <input type="hidden" value="yogesh@spysr.in" id="email">
				<input type="hidden" value="" id="user_id">
					
				<form class="form-inline" action="filterCompanyList" method="post">
					<div class="form-group">
						<!-- <label for="exampleInputAmount">Company Type</label> -->
						<label><s:text name="tgi.label.search" /></label>
						<input type="hidden" value="" id="" name="">
						<div class="input-group">
							<input type="text" placeholder="Type Company Userid" class="form-control input-sm ui-autocomplete-input" id="search" value="" name="" autocomplete="off">
						</div>
						
				</form>
</div>
			</div>
                                            </tr>
                                        </thead>
                                <div class="cnt cnt-table">
						    <div class="table-responsive">
                                        <table class="table table-bordered table-striped-column table-hover">
                                            <thead>
                                                <tr class="border-radius border-color">
                                                    <th><s:text name="tgi.label.s_no" /></th>
                                                    <th><s:text name="tgi.label.empcode" /></th>
                                                    <th><s:text name="tgi.label.email" /></th>
                                                    <th><s:text name="tgi.label.mobile" /></th>
                                                    <th><s:text name="tgi.label.country" /></th>
                                                    <th><s:text name="tgi.label.expand" /></th>
                                                    <th><s:text name="tgi.label.status" /></th>
													<th><s:text name="tgi.label.lock" /></th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    
                                                  <!--   <th scope="row">1</th>
                                                    <td>DirectUser</td>
                                                    <td>khalidhit2008@mail.com</td>
                                                    <td>5555555555</td>
                                                    <td>Bahamas</td> -->
                                                    
													<td> 
							<p data-placement="top" style="margin-left:30px;" title="Agent Details">
									 <a href="agentDetails?id=3" class="btn btn-success btn-xs" data-title="Update">
								  <span data-placement="top" class="fa fa-plus-circle"></span> <span></span></a>
								  </p>
										 </td>
													</td>
													<td><a href="#" class="btn btn-xs btn-default"><s:text name="tgi.label.true" /></a>
				</td>
                                                    
                                          <td>         
                    <a href="#" class="btn btn-xs btn-default"><s:text name="tgi.label.false" /></a>
				
					</td>
                    				
                                                </tr>
                                                
												   <tr>
                                                    
                                                  <!--   <th scope="row">1</th>
                                                    <td>DirectUser</td>
                                                    <td>khalidhit2008@mail.com</td>
                                                    <td>5555555555</td>
                                                    <td>Bahamas</td> -->
                                                    
													<td> 
							<p data-placement="top" style="margin-left:30px;" title="Agent Details">
									 <a href="agentDetails?id=3" class="btn btn-success btn-xs" data-title="Update">
								  <span data-placement="top" class="fa fa-plus-circle"></span> <span></span></a>
								  </p>
										 </td>
													</td>
													<td><a href="#" class="btn btn-xs btn-default"><s:text name="tgi.label.true" /></a>
				</td>
                                                    
                                          <td>         
                    <a href="#" class="btn btn-xs btn-default"><s:text name="tgi.label.false" /></a>
				
					</td>
                    				
                                                </tr>    <tr>
                                                    
                                                    <!-- <th scope="row">1</th>
                                                    <td>DirectUser</td>
                                                    <td>khalidhit2008@mail.com</td>
                                                    <td>5555555555</td>
                                                    <td>Bahamas</td> -->
                                                    
													<td> 
							<p data-placement="top" style="margin-left:30px;" title="Agent Details">
									 <a href="agentDetails?id=3" class="btn btn-success btn-xs" data-title="Update">
								  <span data-placement="top" class="fa fa-plus-circle"></span> <span></span></a>
								  </p>
										 </td>
													</td>
													<td><a href="#" class="btn btn-xs btn-default"><s:text name="tgi.label.true" /></a>
				</td>
                                                    
                                          <td>         
                    <a href="#" class="btn btn-xs btn-default"><s:text name="tgi.label.false" /></a>
				
					</td>
                    				
                                                </tr>    <tr>
                                                    
                                                   <!--  <th scope="row">1</th>
                                                    <td>DirectUser</td>
                                                    <td>khalidhit2008@mail.com</td>
                                                    <td>5555555555</td>
                                                    <td>Bahamas</td> -->
                                                    
													<td> 
							<p data-placement="top" style="margin-left:30px;" title="Agent Details">
									 <a href="agentDetails?id=3" class="btn btn-success btn-xs" data-title="Update">
								  <span data-placement="top" class="fa fa-plus-circle"></span> <span></span></a>
								  </p>
										 </td>
													</td>
													<td><a href="#" class="btn btn-xs btn-default"><s:text name="tgi.label.true" /></a>
				</td>
                                                    
                                          <td>         
                    <a href="#" class="btn btn-xs btn-default"><s:text name="tgi.label.false" /></a>
				
					</td>
                    				
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
								
								
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        </section>
        <!--ADMIN AREA ENDS-->
