<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"metatagpage_edit?id="+"${param.id}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<script type="text/javascript">
$(document).ready(function()
{
	$("#pageName").val($("#pageNameHidden").val());	
});
</script>
<section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                         <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.update_meta_data" /></h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="metatagpage_list"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.meta_dat_list" /></a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div>
                             <div class="cnt cnt-table">
					<div class="table-responsive no-table-css">
<form action="metatagpage_update.action">
  <table class="table table-form">
  <thead>
									<tr>
										<th></th>
										<th>&nbsp;</th>


									</tr>
								</thead>
                                        <tbody>
                                            <tr>

                                                <td>
                                                    <div class="field-name"><s:text name="tgi.label.page_name" /></div>
                                                </td>
												<td>
										<input type="hidden" id="pageNameHidden" value="${metaTagEntity.pageName}">
											 <select class="form-control input-sm" name="pageName"
								id="pageName" autocomplete="off" required>
							<option value="flight_search"><s:text name="tgi.label.flight_search" /></option>
							<option value="hotel_search"><s:text name="tgi.label.hotel_search" /></option>
							<option value="car_search"><s:text name="tgi.label.car_search" /></option>
							<option value="cruise_search"><s:text name="tgi.label.cruise_search" /></option>
							</select> 
                                                </td>
                             												<td>
                                                    <div class="field-name"><s:text name="tgi.label.page_title" /></div>
                                                </td>
                                                <td>
                                                  <input type="text" class="form-control input-sm" 
								name="pageTitle" placeholder="Page Title" autocomplete="off" value="${metaTagEntity.pageTitle}"
								required>
                                                </td>

                                                
                                            </tr>
                                            
                                            <tr>
											 <td>
                                                    <div class="field-name"><s:text name="tgi.label.meta_keywords" /></div>
                                                </td>
                                                <td>
                                                <textarea rows="6" cols="30" name="metaKeyword" placeholder="Meta Keywords">${metaTagEntity.metaKeyword}</textarea>
                                                </td>
                                                <td>
                                                    <div class="field-name"><s:text name="tgi.label.meta_description" /></div>
                                                </td>
                                                <td>
                                                <textarea rows="6" cols="30" name="metaDescription" placeholder="Meta Description">${metaTagEntity.metaDescription}</textarea>
                                                </td>
                                        <tr>
                                        <td>Language</td>
										<td>
										<select name="language" class="form-control">
										<option value="en" ${metaTagEntity.language == 'en' ?'selected' :''}>English</option>
										<option value="ko" ${metaTagEntity.language == 'ko' ?'selected' :''} >Korean</option>
										<option value="tr" ${metaTagEntity.language == 'tr' ?'selected' :''} >Turkish</option>
										</select>
										</td>
                                        <td>
                                        <input type="hidden" name="id" value="${metaTagEntity.id}">
                                        <input type="hidden" name="version" value="${metaTagEntity.version}">
                                        <input type="submit" class="btn btn-info"></td>
                                        <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    
                                    </form>
                                    </div></div>
</div></div></div></section>
                                    
                                      	   <s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>