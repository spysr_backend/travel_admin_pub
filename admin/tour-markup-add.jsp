<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
 <%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<link href="admin/css/custom-checkbox.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addTourMarkup";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>


<%-- <script type="text/javascript">
 	 $(function() {
		 loadstations();
	 });
 	function loadstations() {
		  $.getJSON("hotels.json",{
				format : "json"
			}).done(
					 function(jsonbusdata) {
					 
						  citylist = [];
						  names = [];
						 
						 $.each(jsonbusdata, function(i, station) {
							citylist.push(station.chain);
							 
						 });
						 $("#hotelChain").autocomplete({
					        source: citylist,
					         
					    });
						 
						 $('#hotelChain').on('autocompleteselect', function (e, ui) {
							    	 
			 }); 
					    $.ui.autocomplete.filter = function (array, term) {
					        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
					        return $.grep(array, function (value) {
					            return matcher.test(value.label || value.value || value);
					        });
					    };
					 });
 	}
 	</script> --%>
   <!--************************************
        MAIN ADMIN AREA
    ****************************************-->
        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">
                <div class="">
                    <div class="">
                        <div class="card1">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.add_tour_markup_configuration" /></h5>
                               <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success" href="tourMarkupList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.tour_markup_list" /> </a> 
									</div>
									</div>
                               </div> 
                            </div> 
                    </div>
                </div>
                
                 <div class="container">
					<div class="row">
					<h4 class="text-center p-4 hidden-xs"><s:text name="tgi.label.add_tour_markup_configuration" /></h4> 
                   <div class="spy-form-horizontal mb-0"> 
					<div class="">
						<div class="">
                                <form action="saveTourMarkup" id="tour-markup-add" method="post" class="form-horizontal filter-form">
			                 <div class=" row">
								<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.config_number" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control" name="configData"
													id="configData" autocomplete="off" >
													<option selected value="0"><s:text name="tgi.label.select_company_confignumber" /> </option>
													<s:if test="%{#session.Company.companyRole.isDistributor()}">
													 <s:iterator value="%{#session.AgencyConfigIdsList}">
														<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
													  </s:iterator>
													  <s:iterator value="%{#session.companyConfigIds}">
														<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
													  </s:iterator>
					 						 		</s:if>
					 						 		<s:elseif test="%{#session.Company.companyRole.isAgent()}">
													  <s:iterator value="%{#session.AgencyConfigIdsList}">
														<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
													  </s:iterator>
					 						 		</s:elseif>
					 						 		
													<s:else>
													<s:iterator value="%{#session.companyConfigIds}">
													<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
													 </s:iterator>
					 							</s:else>
											</select>
											</div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">Markup On Tour Code</label>
										<div class="checkbox">
								            <label style="font-size: 2em">
								                <input type="checkbox" name="promoOnRateCode" id="tour_price_promotion" value="false" data-rate-code="rate-code-show" data-tour-code="tour-code-show">
								                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
								                <span style="font-size:16px"> </span>
								            </label>
								        </div>
									</div>
									</div>	
									<div class="row">
									<div class="col-md-6 hidden" id="tour-code-show">
										<label class="form-control-label" for="appendedInput">Select Tour Code</label>
										<div class="form-group">
							                <c:set var="count" value="0" scope="page" />
												<select class="form-control" name="tourArray" id="tour-code-list" multiple required="required" >
													<option value="TOUR29950">	TOUR29950</option>
													<option value="TOUR26982">	TOUR26982</option>
													<option value="TOUR25319">	TOUR25319</option>
													<option value="TOUR27578">	TOUR27578</option>
													<option value="TOUR17073">	TOUR17073</option>
													<option value="TOUR16839">	TOUR16839</option>
													<option value="TOUR12595">	TOUR12595</option>
												</select>
							            </div>
								</div>
									<div class="col-md-6 hidden" id="rate-code-show">
										<label class="form-control-label" for="appendedInput">Select Rate Code</label>
										<div class="form-group">
							                <c:set var="count" value="0" scope="page" />
												<select class="" name="tourArray" id="rate-code-list" multiple required="required" >
													<option value="RC27488">	RC27488</option>
													<option value="RC23247">	RC23247</option>
													<option value="RC17093">	RC17093</option>
													<option value="RC23482">	RC23482</option>
													<option value="RC23528">	RC23528</option>
													<option value="RC24205">	RC24205</option>
													<option value="RC11853">	RC11853</option>
												</select>
							            </div>
								</div>
									</div>
									<div class="row">
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_name" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control" id="name"
													name="name" placeholder="Name..." autocomplete="off"
													></div></div>
											</div>
										</div>
									</div>
									</div>
					
								<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.tour_origin" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control airportList" id="tourOrigin"
									name="tourOrigin" placeholder="ALL"  
									  ></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.tour_destianation" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control airportList" id="tourDestination"
									name="tourDestination" placeholder="ALL" autocomplete="off"
									  ></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.is_accumulative" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
														<select class="form-control" name="isaccumulative"
									  autocomplete="off" >
 									<option value="1"><s:text name="tgi.label.yes" /></option>
									<option value="0" selected="selected"><s:text name="tgi.label.no" /></option>
									 
								</select></div></div>
												</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.lable.is_fixedamount" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control" name="isfixedAmount"
													  autocomplete="off" >
				 									<option value="1"><s:text name="tgi.label.yes" /></option>
													<option value="0"><s:text name="tgi.label.no" /></option>
												 </select></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_amount"/>
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" class="form-control" id="markupAmount"
									name="markupAmount" placeholder="amount" autocomplete="off" ></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.position_of_markup"/>
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="number" class="form-control" name="positionMarkup"
									id="country" autocomplete="off" ></div></div>
											</div>
										</div>
									</div>
										
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.start_date" />
												</label>
											<div class="controls">
												<div class="form-group"><div class="">
													<input type="text" id=""
															class="form-control date1" name="checkInDateTr" autocomplete="off"
															placeholder="ALL"   ></div></div>
												</div>
											</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.end_date" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
													<input type="text" id=""
								class="form-control date2" name="checkOutDateTr" autocomplete="off"
								placeholder="ALL"  >
											</div></div>
										</div>
									</div>
										
									
					</div>
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_startdate" />
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" id=""
														class="form-control date3" name="promofareStartDateTr"  value='<s:property value="promofareStartDate" />'  autocomplete="off"
														placeholder="ALL" ></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_enddate" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
													<input type="text" id=""
								class="form-control date4" name="promofareEndDateTr"  value='<s:property value="promofareEndDate" />'  autocomplete="off"
								placeholder="ALL" >
											</div></div>
										</div>
									</div> 		
								</div>  
								
								<div class="row">
                                   <div class="col-md-12">
                                   <div class="set pull-right">
										<div class="form-actions">
											 	<button class="btn btn-danger " type="reset"><s:text name="tgi.label.reset" /></button>
                                                <button class="btn btn-primary " type="submit"><s:text name="tgi.label.set_markup" /></button>
										</div></div>
										</div></div> 
                                    </form>  
                                </div>
                            </div>
                        </div>
                        </div></div>
               </div></div> 

<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>           
<script>
$(".airportList").autocomplete({
       minChars: 3,
       source: "city_autocomplete"
	});

</script>
<s:if test="message != null && message  != ''">
<script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
<c:choose>
<c:when test="${fn:contains(message, 'successfully')}">
<script>
    	  $(document).ready(function() 
    			  {
    	  $('#alert_box').modal({
    	    show:true,
    	    keyboard: false
	    } );
$('#alert_box_body').text('${param.message}');
    			  });
</script>
</c:when>
<c:otherwise>
<script>
  	  $(document).ready(function() 
  			  {
  		  $('#alert_box_info').modal({
    	    show:true,
    	    keyboard: false
	    } );
$('#alert_box_info_body').text("${param.message}");
  			  });
</script>
</c:otherwise>
</c:choose>
</s:if>
</section>
         <script>
			$(document).ready(function() {
				var date1="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				var date2="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
				 
				 var date3="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					var date4="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					 spysr_date_custom_plugin('filter-form','date3','date4','mm/dd/yyyy','MMM DD YYYY',date3 ,date4);
			});
		</script>
      
      
      
<script type="text/javascript">
/*-------------------------------------------------------------------*/
$(document).ready(function() {
	$("#tour-code-list").multipleSelect({
		filter: true,
		width: '100%',
        height: '100%',
        placeholder: "Select a Tour Code",
	});
	$("#rate-code-list").multipleSelect({
		filter: true,
		width: '100%',
        height: '100%',
        placeholder: "Select a Rate Code",
	});
});
/*------------------------------------------------------------------------*/
//
$('#tour_price_promotion').change(function(){
	if(this.checked){
		$("#"+$(this).data("rate-code")).removeClass("hidden");
		$("#"+$(this).data("tour-code")).removeClass("hidden");
	}
	else{
		$("#"+$(this).data("rate-code")).addClass("hidden");
		$("#"+$(this).data("tour-code")).addClass("hidden");
		
	}
	});
/*------------------------------------------------------------------------*/
$(document).ready(function() {
$('#tour-markup-add')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				
			 configData : {
					message : 'ConfigData Role is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a ConfigData  '
						},
					}
				},
				name : {
					message : 'MarkUp Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a MarkUp Name'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'MarkUp Name  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				/* tourOrigin : {
					message : 'TourOrigin Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a TourOrigin'
						},
						stringLength : {
							min : 3,
							max : 200,
							
							message : 'TourOrigin Name  must be more than 3 and less than 100 characters long'
						} 
						
					}
				},
				tourDestination : {
					message : 'TourDestination Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a TourDestination'
						},
						stringLength : {
							min : 3,
							max : 100,
							
							message : 'TourDestination Name  must be more than 3 and less than 20 characters long'
						} 
						
					}
				}, */
				isaccumulative : {
					message : 'isaccumulative  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a isaccumulative'
						},
					}
				},
				isfixedAmount : {
					message : 'IsfixedAmount  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a IsfixedAmount'
						},
					}
				},
				markupAmount : {
					message : 'Amount Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Amount'
						},
						  numeric: {
	                            message: 'The value is not a number',
	                            // The default separators
	                            thousandsSeparator: '',
	                            decimalSeparator: '.'
	                        }
						
					}
				},
				positionMarkup : {
					message : 'positionMarkup Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a position markup'
						},
					}
				},
			
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {
	/* notifySuccess(); */
	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>
        <!--ADMIN AREA ENDS-->