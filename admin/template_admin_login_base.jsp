<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!doctype html>
<html lang="en" class="no-js">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<link rel="stylesheet" href="admin/css/font-awesome.min.css">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="assets/img/favicon.ico">
<title>SPYSR Admin Template</title>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="admin/css/bootstrap-datepicker3.css">
<link rel="stylesheet" href="admin/css/fileinput.min.css">
<link rel="stylesheet" href="admin/css/flags.css">
<link rel="stylesheet" href="admin/css/airlines.css">
<link rel="stylesheet" href="admin/css/bootstrap-table.css">
<link rel="stylesheet" href="admin/css/less-plugins/awesome-bootstrap-checkbox.css">
<link rel="stylesheet" href="admin/css/style.css">

<script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="main-container clearfix">
		<section id="main-area">
			<tiles:insertAttribute name="header" ignore="true" />
			<tiles:insertAttribute name="body" ignore="true" />
		</section>
		<tiles:insertAttribute name="footer" ignore="true" />
	</div>
</body>
</html>