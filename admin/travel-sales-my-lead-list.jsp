<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
@media screen and (max-width:480px){
.detail-btn {
	margin-top: -27px;
    margin-left: 15px;
}
}
.center {
	text-align: center;
}

.btn-xsm, .btn-group-xs>.btn {
	padding: 2px 6px;
	font-size: 13px;
	line-height: 1.5;
	border-radius: 2px;
}

a.collapsed {
	color: #02214c;
	text-decoration: none !important;
}

a.collapsed:hover {
	color: #337ab7;
	text-decoration: none !important;
}

.highlight {
	background-color: #F44336;
}

.highlight-light {
	background-color: #ff6559;
}

.line-btn {
	padding: 5px 0 0 5px;
	font-size: 20px;
	color: #9E9E9E;
}

.clippy {
	margin-bottom: 3px;
}

span.text-spysr:hover {
	color: #3a7df9;
}
</style>

   <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                        <div class="card1">
                        <div class="pnl">
                        <div class="hd clearfix">
					<h5 class="pull-left"><img class="clay" src="admin/img/svg/tourist.svg" width="26" alt="Higest"> Travel Sales My Lead</h5>
					<div class="set pull-right hidden-xs" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
				</div>
                                 <br>
                         		<form action="listTravelSalesMyLeadFilter" class="filter-form" id="resetform" method="post">
                         		<input type="hidden" name="filterFlag" id="filterFlag" value="true">
								<div class="" id="filterDiv" style="display: none;">
								<div class="form-group">
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<select name="leadGenerateSource" id="created-by" class="form-control input-sm input-sm">
											<option value=""  selected="selected">Select Lead Source</option>
											<option value=""><s:text name="tgi.label.all" /></option>
											<option value="IBEUSER">IBE</option>
											<option value="ADMIN"><s:text name="tgi.label.admin" /></option>
										</select>
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="createdDateRange" id="created-date-range" placeholder="Search by created between...." class="form-control input-sm search-query" />
								 </div></div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="assignDateRange" id="assign-date-range" placeholder="Search by assign between...." class="form-control input-sm search-query" />
								</div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="leadNumber" id="lead-number-json" placeholder="Search By Lead Number" class="form-control input-sm search-query" />
							   </div>
							   </div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="firstName" id="name-json" placeholder="Search By Name" class="form-control input-sm search-query" />
							   </div>
							   </div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="email" id="email" placeholder="Search By Email" class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="phone" id="phone" placeholder="Search By Phone" class="form-control input-sm search-query" />
								</div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="city" id="city-json" placeholder="Search By City" class="form-control input-sm search-query" />
								</div>
								</div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="country" id="country-json" placeholder="Search By Country" class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="departureDateFlag" id="country-json" placeholder="Search By Departure Date..." class="form-control input-sm search-query" />
								 </div></div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="destinationFrom" id="destination-from" placeholder="Search By Departure From..." class="form-control input-sm search-query" />
								 </div></div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="destinationTo" id="destination-to" placeholder="Search By Departure To..." class="form-control input-sm search-query" />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select aria-hidden="true" name="leadSource" id="leadSource" class="form-control input-sm">
									    <option value="">Search By Lead Source*</option>
										<option value="Advertisement">Advertisement</option>
										<option value="CustomerEvent">Customer Event</option>
										<option value="Employee Referral">Employee Referral</option>
										<option value="Google AdWords">Google AdWords</option>
										<option value="Website">Website</option>
										<option value="Socal Network">Socal Network</option>
										<option value="Walk-In">Walk-In</option>
										<option value="Telephone">Telephone</option>
										<option value="B2B">B2B</option>
										<option value="Justdial">Justdial</option>
										<option value="Others">Others</option>
									</select>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select aria-hidden="true" name="callTime" id="callTime" class="form-control input-sm">
								  <option value="" selected="SELECTED"> Search By Call Time</option>
									<option value="morning">Morning</option>
									<option value="afternoon">Afternoon</option>
									<option value="evening">Evening</option>
									<option value="anytime">Any time</option>
								</select>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select name="isEmailVerify" id="terminate" class="form-control input-sm">
								<option value="" selected="selected">Search By Email Verify</option>
								<option value="true"><s:text name="tgi.label.yes" /></option>
                                <option value="false" ><s:text name="tgi.label.no" /></option>
                                </select>
								 </div>
							   </div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select aria-hidden="true" name="leadTitle" id="lead-title" class="form-control input-sm">
								  <option value="">Search By Lead Title</option>
									<c:forEach items="${followupStatusMap}" var="title">
										<option value="${title.key}">${title.value}</option>
									</c:forEach>
								 </select>
								 </div>
							   </div>
							<div class="row">
							 <div class=""></div>
							   <div class="col-md-1">
								<div class="form-group">
								<button type="reset" class="btn btn-danger btn-sm btn-block" id="configreset">&nbsp;Clear&nbsp;</button>
							</div>
							</div>
								<div class="col-md-1">
								<div class="form-group">
								<button type="submit" class="btn btn-info btn-sm btn-block" value="Search" >Search</button>
							</div>
							</div>
							</div>
						</div></div>
				</form>
                            
                                   
			<div class="dash-table">
			<div class="content mt-0 pt-0">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"  role="grid" aria-describedby="example_info" style="width: 100%;">
                                        <thead>
                                        <tr class="table-sub-header">
                                        	<th colspan="16" class="noborder-xs">
                                        	<div class="pull-left">
													<div class="btn-group">
														<div class="dropdown pull-left" style="margin-right: 5px;" data-toggle="tooltip" data-placement="top" title="Lead Status : New">
															<a class="btn btn-sm btn-default" href="listTravelSalesMyLeadFilter?leadStatus=New&filterFlag=true">
															<span class="notice-info"><strong>New &nbsp;( ${leadStatusUtil.newstatus} )</strong></span></a>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status: Active">
																<a class="btn btn-sm btn-default" href="listTravelSalesMyLeadFilter?leadStatus=Active&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-active"><strong>Active &nbsp;( ${leadStatusUtil.active} )</strong></span>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status: Booked">
																<a class="btn btn-sm btn-default" href="listTravelSalesMyLeadFilter?leadStatus=Booked&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-success"><strong>Booked &nbsp;( ${leadStatusUtil.booked} )</strong></span>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status:No Connect">
																<a class="btn btn-sm btn-default" href="listTravelSalesMyLeadFilter?leadStatus=No Connect&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-warning"><strong>No Connect &nbsp;( ${leadStatusUtil.noconnect} )</strong></span>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Lead Status:No Connect">
																<a class="btn btn-sm btn-default" href="listTravelSalesMyLeadFilter?leadStatus=Closed&filterFlag=true" style="margin-bottom: 2px; margin-right: 3px;">
																	<span class="notice-danger"><strong>Closed &nbsp;( ${leadStatusUtil.closed} )</strong></span>
																</a>
															</div>
														</div>
														<span class="line-btn">  | </span>
														<div class="dropdown pull-right" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Filter By Lead Title">
																<select aria-hidden="true" name="leadTitle" id="lead-title-filter" class="form-control input-sm btn btn-sm btn-outline-success " style="font-size:13px">
																  <option value="">Search By Lead Title</option>
																	<c:forEach items="${followupStatusMap}" var="title">
																		<option value="${title.key}">${title.value}</option>
																	</c:forEach>
																 </select>
															</div>
														</div>
													</div>
											</div>
                                        	<div class="pull-right">   
	                                        	<div class="btn-group"> 
												<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Travel Sales Lead">
												  <a class="btn btn-sm btn-default" href="addTravelSalesMyLead" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class="">&nbsp;New&nbsp;</span></a> 
												</div>
												<span class="line-btn">  | </span>
												<div class="dropdown pull-right" style="margin-left:5px;">
												  <a  class="btn btn-sm btn-default filterBtn" data-toggle="dropdown"  style="margin-bottom: 2px;margin-right: 3px;" title="Show Filter Row">
												  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
												  </a>  
												</div>
												<div class="dropdown pull-right" style="margin-left:5px;">
												<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesMyLeadList.size()}" title="Please Select Row">
												  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
												  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
												  </button>  
												</div>
												</div>
												  </div>
											</div> 
                                        	</th>
                                        </tr>
										<tr class="border-radius border-color" role="row">
                                                   	<th></th>
                                                   	<th style="width: 50px;" data-priority="1">
															<div class="center">
															<div data-toggle="tooltip" data-placement="top" title="Select All">
															<div class="checkbox checkbox-default">
										                        <input id="master_check" data-action="all" type="checkbox" >
										                        <label for="master_check">&nbsp;</label>
										                    </div>
										                    </div>
										                    </div>
													</th>
                                                    
                                                    <th data-priority="2">LE Number</th>
                                                    <th data-priority="3">Name</th>
                                                    <th data-priority="5"><s:text name="tgi.label.phone" /></th>
                                                    <th data-priority="6"><s:text name="tgi.label.email" /></th>
                                                     <th data-priority="8">Journey Date</th>
                                                     <th class="" data-priority="7">From -To</th>
                                                     <th><s:text name="tgi.label.adult" /></th>
                                                     <th>Duration</th>
                                                     <!-- <th class="col-md-1" style="width: 70px">Owner</th>
                                                     <th class="col-md-1">Assign At</th> -->
                                                     <th class="" style="width:145px">Created Date</th>
                                                     <th class="">Source</th>
                                                     <th class=""><span data-toggle="tooltip" data-placement="top" title="Email Verify Status"><img class="clippy" src="admin/img/svg/mail.svg" width="16" alt="Mail" style="margin-bottom: 3px;"></span></th>
                                                     <th class=""><span data-toggle="tooltip" data-placement="top" title="Lead Priority">LP</span></th>
                                                     <th style="width: 22px;"><div class=""><img class="clippy" src="admin/img/svg/settings.svg" width="16" alt="Settings" style="margin-bottom: 3px;"></div></th>
                                                     <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
   											<s:if test="travelSalesMyLeadList.size > 0">
                                           <s:iterator value="travelSalesMyLeadList" status="rowstatus">
                                                <tr>
                                                <td class="<c:choose>
													<c:when test="${travelSalesMyLeadFollowUp.leadStatus == 'New'}">
													lead-status-new
													</c:when>
													<c:when test="${travelSalesMyLeadFollowUp.leadStatus == 'Active'}">
													lead-status-active
													</c:when>
													<c:when test="${travelSalesMyLeadFollowUp.leadStatus == 'Booked'}">
													lead-status-booked
													</c:when>
													<c:when test="${travelSalesMyLeadFollowUp.leadStatus == 'No Connect'}">
													lead-status-no-connect
													</c:when>
													<c:when test="${travelSalesMyLeadFollowUp.leadStatus == 'Closed'}"> 
													lead-status-closed
													</c:when>
													<c:otherwise>
													lead-status-none
													</c:otherwise>
													</c:choose>"><div class="center"><s:property value="%{#rowstatus.count}" /></div></td>
                                                <td data-title="Select" class="select-checkbox">
																<div class="center"> 
																<div class="checkbox checkbox-default">
										                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${id}" class="check_row">
										                        <label for="checkbox1_sd_${rowstatus.count}"></label>
										                   		</div>
										                   		</div>
														</td>
                                                    
                                                    <td data-title="Email"><a href="editTravelSalesMyLead?id=${id}" data-toggle="modal" class="text-primary">${leadNumber}</a></td>
                                                    <td data-title="Company Name"><span class="">
                                                    <a href="editTravelSalesMyLead?id=${id}" data-toggle="modal" class="text-primary">${firstName} ${lastName}</a>
                                                    </span>
                                                    </td>
                                                    <td data-title="Phone">
                                                    <a href="https://api.whatsapp.com/send?phone=${phone}&text=hi ${firstName}"><img class="clippy" src="admin/img/svg/whatsapp.svg" width="16"style="margin-bottom: 3px;"></a>&nbsp;&nbsp;
                                                    <a href="tel:${phone}"><img class="clippy" src="admin/img/svg/telephone-g.svg" width="16"style="margin-bottom: 3px;"></a>
                                                    &nbsp;&nbsp;${phone}
                                                    </td>
                                                    <td data-title="Email"><a href="mailto:${email}?subject=${title}&body= Hi ${firstName}"><img class="clippy" src="admin/img/svg/envelope-g.svg" width="16"style="margin-bottom: 3px;">&nbsp;&nbsp;${email}</a></td>
                                                    <td>
                                                    <c:choose>
                                                    <c:when test="${departureDate != null}">
                                                    ${spysr:formatDate(departureDate,'yyyy-mm-dd', 'mm/dd/yyyy')}
                                                    </c:when>
                                                    <c:otherwise>
													N/A                                                    
                                                    </c:otherwise>
                                                    </c:choose>
                                                     </td>
                                                    <td>${destinationFrom} - ${destinationTo}</td>
                                                    <td><div class="" >${adult}</div></td>
                                                    <td>
                                                    <c:choose>
                                                    <c:when test="${duration != null && duration != ''}">
                                                    ${duration}
                                                    </c:when>
                                                    <c:otherwise>
                                                    None
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                   	<%-- <td class="text-capitalize">
                                                   		${travelSalesMyLeadFollowUp.assignedToName != null && travelSalesMyLeadFollowUp.assignedToName != '' ?travelSalesMyLeadFollowUp.assignedToName :'None'}
                                                    </td>
                                                    <td class="text-capitalize">
                                                    <c:choose>
                                                    <c:when test="${travelSalesMyLeadFollowUp.assignDate != null}">
                                                    ${spysr:formatDate(travelSalesMyLeadFollowUp.assignDate,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}
                                                    </c:when>
                                                    <c:otherwise>
                                                    N/A
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td> --%>
                                        	        <td data-title="Action">${spysr:formatDate(createdDate,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}</td>
                                        	        <td>
                                                    <c:choose>
                                                    <c:when test="${leadGenerateSource != null && leadGenerateSource != ''}">
                                                    <span>${leadGenerateSource}</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                    <span>N/A</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                    <td>
                                                    <c:choose>
                                                    <c:when test="${verifyStatus != false}">
                                                    <img class="clippy" src="admin/img/svg/checkedg.svg" width="14" alt="Copy to clipboard">
                                                    </c:when>
                                                    <c:otherwise>
                                                     <img class="clippy" src="admin/img/svg/close.svg" width="10" alt="Copy to clipboard">
                                                    </c:otherwise>
                                                    </c:choose>
                                                    </td>
                                                    <td>
                                                    <c:choose>
														<c:when test="${travelSalesMyLeadFollowUp.leadRating == 'Highest'}">
														<span data-toggle="tooltip" data-placement="top" title="Highest">
															<img class="clay" src="admin/img/svg/highest.svg" width="17" alt="Higest">
														</span>
														</c:when>						
														<c:when test="${travelSalesMyLeadFollowUp.leadRating == 'High'}">
														<span data-toggle="tooltip" data-placement="top" title="High">
														<img class="clay" src="admin/img/svg/high.svg" width="17" alt="High">
														</span>
														</c:when>						
														<c:when test="${travelSalesMyLeadFollowUp.leadRating == 'Medium'}">
														<span data-toggle="tooltip" data-placement="top" title="Medium">
														<img class="clay" src="admin/img/svg/medium.svg" width="17" alt="Medium">
														</span>
														</c:when>						
														<c:when test="${travelSalesMyLeadFollowUp.leadRating == 'Low'}">
														<span data-toggle="tooltip" data-placement="top" title="Low">
														<img class="clay" src="admin/img/svg/low.svg" width="17" alt="Low">
														</span>
														</c:when>						
														<c:when test="${travelSalesMyLeadFollowUp.leadRating == 'Lowest'}">
														<span data-toggle="tooltip" data-placement="top" title="Lowest">
															<img class="clay" src="admin/img/svg/lowest.svg" width="17" alt="Lowest">
														</span>
														</c:when>	
														<c:otherwise>
														<strike><span data-toggle="tooltip" data-placement="top" title="None" style="    font-size: 14px;">&nbsp;N</span></strike>
														</c:otherwise>					
														</c:choose>
                                                    </td>
                                        	        
                                        	        <td data-title="Action">
                                        	        <div class="dropdown pull-right" style="    margin-right: 20px;">
													  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
													  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
													  <ul class="dropdown-menu">
													    <li class="action"><a href="editTravelSalesMyLead?id=${id}">Edit</a></li>
													    <li class="action"><a href="#">Delete</a></li>
													    <li class="action">
													    <a href="#!" class="plus-icon" data-event="duplicate" data-toggle="modal" data-target="#change_owner_${rowstatus.count}" 
													       data-tour-id="${id}">Change Owner</a>
													    </li>
													  </ul>
													</div>
													</td>  
													<td></td>
														 </tr>
                                                </s:iterator></s:if>
                                            </tbody>
											
                                        </table></div></div></div></div>
										</div>
                                    </div>
                            </div>
            </section>
        <!--ADMIN AREA ENDS-->
        

<!--AutoCompleteter  -->
<script>
var CName={url:"getTravelSalesLeadJson.action?data=name",getValue:"name",list:{match:{enabled:!0}}};$("#name-json").easyAutocomplete(CName);
var Email={url:"getTravelSalesLeadJson.action?data=email",getValue:"email",list:{match:{enabled:!0}}};$("#email").easyAutocomplete(Email);
var Phone={url:"getTravelSalesLeadJson.action?data=phone",getValue:"phone",list:{match:{enabled:!0}}};$("#phone").easyAutocomplete(Phone);
var City={url:"getTravelSalesLeadJson.action?data=city",getValue:"city",list:{match:{enabled:!0}}};$("#city-json").easyAutocomplete(City);
var DFrom={url:"getTravelSalesLeadJson.action?data=destinationFrom",getValue:"destinationFrom",list:{match:{enabled:!0}}};$("#destination-from").easyAutocomplete(DFrom);
var DTo={url:"getTravelSalesLeadJson.action?data=destinationTo",getValue:"destinationTo",list:{match:{enabled:!0}}};$("#destination-to").easyAutocomplete(DTo);
			
	</script>
	<script type="text/javascript" class="init">
	$(document).ready(function() {
		$('.filterBtn').click(function() {
			$('#filterDiv').toggle(500);
		});
		$('div.easy-autocomplete').removeAttr('style');
		
		$('#configreset').click(function(){
            $('#resetform')[0].reset(); 
       });
	});

	$('.filter-link').click(function() {
		var collapsed = $(this).find('i').hasClass('fa-angle-up');

		$('.filter-link').find('i').removeClass('fa-angle-down');

		$('.filter-link').find('i').addClass('fa-angle-up');
		if (collapsed)
			$(this).find('i').toggleClass('fa-angle-up fa-2x fa-angle-down fa-2x')
	});
</script>
<script>
/*--------------------------script for delete tour prices multiple-----------------------------*/
$('#delete_all_price').on("click", function(event){
	// confirm dialog
	 var allVals = [];
	 $(".check_row:checked").each(function() {  
		 allVals.push($(this).attr('data-id'));
	 });  
		 var check = confirm("Are you sure you want to delete selected data?");
		 if(check == true)
		 {
			 var join_selected_values = []; 
			 join_selected_values = allVals.join(",");
			 $.ajax({
						url : "archiveMultipleTravelLead?travelLeadIds="+join_selected_values,
						type : "POST",
						dataType: 'html',
						success : function(data) {
							alertify.success("You've clicked OK Your data have successfully deleted");
							setTimeout("window.location.reload();", 1000);
						},
						error: function (request, status, error) {
							showModalPopUp("Lead can not be deleted.","e");
						}
				}); 
		 }
		 else
		 {
			 return false;	 
		 }
});
</script>
<script type="text/javascript" src="admin/js/admin/multiple-archive.js"></script>