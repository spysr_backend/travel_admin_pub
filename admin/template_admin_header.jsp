<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<%@ taglib prefix="s" uri="/struts-tags"%>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<script src="admin/js/admin/wallet.js"></script>
<c:if test="${jsAutocompleteFlag}">
<link href="admin/filter_resource/easy-autocomplete.min.css" rel="stylesheet" type="text/css">
<script src="admin/filter_resource/jquery.easy-autocomplete.min.js" type="text/javascript"></script>
</c:if>
<c:if test="${jsSelect2Flag}">
<link href="admin/css/select2.css" rel="stylesheet" type="text/css">
<link href="admin/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css">
<script src="admin/js/select2.min.js"></script>
<script>
$('.js-basic-select').select2({
	  placeholder: 'Select an option'
	});
</script>
</c:if>
<c:if test="${jsDateRangePickerFlag}">
<link href="admin/css/daterange/daterangepicker.css" rel="stylesheet" type="text/css">
<script src="admin/js/moment.js"></script>
<script src="admin/js/daterange/daterangepicker.js"></script>
</c:if>
<c:if test="${jsDateTimePickerFlag}">
<link href="admin/css/datetime/datetimepicker.css" rel="stylesheet" type="text/css">
<script src="admin/js/moment.js"></script>
<script src="admin/js/datetime/bootstrap-datetimepicker.min.js"></script>
</c:if>
<c:if test="${jsMultiSelectFlag}">
<link href="admin/css/multiple-select.css" rel="stylesheet"/>
<script src="admin/js/multiple-select.js"></script>
</c:if>
<c:if test="${jsEkkoLightboxFlag}">
<link href="admin/css/ekko-lightbox.css" rel="stylesheet"/>
<script src="admin/js/ekko-lightbox.min.js"></script>
</c:if>
<c:if test="${jsSelectBoxUIFlag}">
 <link href="admin/css/select/bootstrap-select.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="admin/js/select/bootstrap-select.min.js"></script>
</c:if>
<c:if test = "${jsDataTableFlag}"> 
	
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/css/responsive.dataTables.min.css">
	
	
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/ga.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/jquery.dataTables.min.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/dataTables.buttons.min.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/buttons.flash.min.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/jszip.min.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/pdfmake.min.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/vfs_fonts.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/buttons.html5.min.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/buttons.print.min.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/buttons.colVis.min.js"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" class="init">

$(document).ready(function() {
	var currentDate = (new Date).getTime();
	$('#example').DataTable( {
		dom: 'Bfrtip',
		 "pageLength": 20,
		 responsive: {
	            details: {
	                type: 'column',
	                target: -1
	            }
	        },
		  "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
		  columnDefs: [
	            { responsivePriority: 1, targets: 0 },
	            { responsivePriority: 2, targets: -1 },
	            {
	                className: 'control',
	                orderable: false,
	                targets:   -1
	            },
	        ],
		 fixedHeader: {
	            header: true,
	        },
		buttons: [
	            {
	                extend: 'copyHtml5',
	                exportOptions: {
	                	columns: "thead th:not(.noExport)"
	                }
	            },
	            {
	                extend: 'excelHtml5',
	                exportOptions: {
	                	columns: "thead th:not(.noExport)"
	                },
	                filename: currentDate
	            },
	            {
	                extend: 'pdfHtml5',
	                exportOptions: {
	                	columns: "thead th:not(.noExport)"
	                },
	                filename: currentDate
	            },
	            {
	                extend: 'print',
	                exportOptions: {
	                	columns: "thead th:not(.noExport)"
	                },
	                filename: currentDate
	            },
	            {
	                extend: 'csv',
	                exportOptions: {
	                	columns: "thead th:not(.noExport)"
	                },
	                filename: currentDate
	            },
	            'colvis'
	        ]
		        
	} );
	
} );

</script>
	
<style>

#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.radio, .checkbox {
    position: relative;
    display: block;
    margin-top: 3px;
    margin-bottom: 3px;
}
.dropdown-menu>li>a {
    display: block;
    padding: 3px 20px;
    clear: both;
    font-weight: normal;
    line-height: 23px;
    color: rgb(0, 112, 210);
    white-space: nowrap;
}
.link>a{
    color: rgb(0, 112, 210);
    text-decoration: none;
}  
button.btn.btn-sm.btn-default.dropdown-toggle {
    padding: 2px 7px !important;
    font-size: 12px !important;
    line-height: 1.5 !important;
    border-radius: 2px !important;
}
</style>
</c:if>
<c:if test = "${jsSimpleDataTableFlag}"> 
	
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/css/responsive.dataTables.min.css">
	
	
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/ga.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/jquery.dataTables.min.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/jszip.min.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/print-resource/vfs_fonts.js.download"></script>
	<script type="text/javascript"  src="admin/print-pdf-excel/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" class="init">

$(document).ready(function() {
	var currentDate = (new Date).getTime();
	$('#example').DataTable( {
		dom: 'Bfrtip',
		 "pageLength": 20,
		 responsive: {
	            details: {
	                type: 'column',
	                target: -1
	            }
	        },
		  "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
		  columnDefs: [
	            { responsivePriority: 1, targets: 0 },
	            { responsivePriority: 2, targets: -1 },
	            {
	                className: 'control',
	                orderable: false,
	                targets:   -1
	            },
	        ],
		 fixedHeader: {
	            header: true,
	        },
		        
	} );
	
} );
</script>
	
<style>

#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.radio, .checkbox {
    position: relative;
    display: block;
    margin-top: 3px;
    margin-bottom: 3px;
}
.dropdown-menu>li>a {
    display: block;
    padding: 3px 20px;
    clear: both;
    font-weight: normal;
    line-height: 23px;
    color: rgb(0, 112, 210);
    white-space: nowrap;
}
.link>a{
    color: rgb(0, 112, 210);
    text-decoration: none;
}  
button.btn.btn-sm.btn-default.dropdown-toggle {
    padding: 3px 8px !important;
    font-size: 12px !important;
    line-height: 1.5 !important;
    border-radius: 2px !important;
}
.clippy {
	margin-bottom: 3px;
}
</style>
</c:if>

<c:if test = "${jsSummernoteFlag}"> 
	<link rel="stylesheet" type="text/css" href="admin/editor-js/summernote/css/summernote.css" />
	<script src="admin/editor-js/summernote/js/summernote.min.js"></script>
	<script src="admin/editor-js/summernote/specialchars/summernote-ext-specialchars.js"></script>
</c:if>


<link rel="shortcut icon" href="../favicon.ico">
<link rel="stylesheet" type="text/css" href="admin/css/hamburger.css" />
<script src="admin/js/snap.svg-min.js"></script>
<script src="admin/js/classie.js"></script>

<script>
$(function() {
	  $('#morph-shape').click(function () {
	     $( ".sidebar-collapse" ).css('margin-left','0px');
	  });
	});
</script>
      <!--HEADER BEGINS-->
            <header class="header clearfix">
                <s:if test="%{#session.Company!=null}">
                <nav class="main-menu navbar navbar-default">
		<div class="navbar-header">
			<div class="sidebar-toggle-btn">
				<button id="hamburger" class="menu-button">
					<span id="morph-shape" class="morph-shape" data-morph-open="M3,20c0,0,12-4,27-4s27,4,27,4;M3,60c0,0,12,4,27,4s27-4,27-4" data-morph-close="M3,20c0,0,12,4,27,4s27-4,27-4;M3,60c0,0,12-4,27-4s27,4,27,4">
						<svg width="100%" height="100%" viewBox="0 0 60 80" preserveAspectRatio="none">
							<path d="M3,20c0,0,12,0,27,0s27,0,27,0"/>
							<line x1="3" y1="40" x2="57" y2="40"/>
							<path d="M3,60c0,0,12,0,27,0s27,0,27,0"/>
						</svg>
					</span>
				</button>
                </div>
                <div class="sidebar-logo2">
                    <div class="lg-logo">
                        <a href="home"><img src="admin/img/touroxy-logo.png" alt="" class="img-responsive"></a>
                    </div>
                    <div class="sm-logo hidden">
                        <a href="home"><img src="admin/img/touroxy-logo.png" alt="" class="img-responsive" style="margin-top: 6px;"></a>
                    </div>
                </div>
				<!-- <button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					
					<div class="fa fa-caret-down" data-original-title="Toggle Navigation"></div>
				</button>  -->
				
				
				<div class="dropdown header-notification"> 
				 <ul class="">
					<li class="admin-wallet pull-left hidden-xs"><h5 class="wallet-balance"><s:text name="tgi.label.wallet_balance" /> : <span class="walletAmount"> </span></h5></li>
					<%-- <li role="presentation" class="active pull-left"><a href="${appConfig.tourAdminLink}companyloginfromtraveladmin?pbu=search&adminLoginKey=${session['adminLoginKey']}">Tour Admin</a></li> --%>
					<li class="nav-item dropdown pull-right">
						<a class="nav-link nav-link" href="#" data-toggle="modal" data-target="#profile">
						<c:choose>
							<c:when test="${session.User.imagePath != null}">
								<img class="header-img-user mr-0"
									src="<s:url action='getImageByPath?imageId=%{#session.User.imagePath}'/>">
							</c:when>
							<c:otherwise>
								<i class="fa fa-user header-icons"></i>
							</c:otherwise>
						</c:choose> 
                            <span class="hidden-md-down"><s:property value="%{#session.User.userName}"/> </span>
                        </a> 
                       <%--  <a class="nav-link nav-link" href="#" data-toggle="modal" data-target="#hotels">
                            <i class="fa fa-hotel header-icons"></i>
                            <span class="hidden-md-down">Hotels </span>
                        </a> 
                        <a class="nav-link nav-link" href="#" data-toggle="modal" data-target="#help">
                            <i class="fa fa-info header-icons"></i>
                            <span class="hidden-md-down">Help </span>
                        </a>  --%>
                    </li> 
                    </ul>
				</div>
				
		</div>
	</nav>
	  </s:if>
  <script>
  $('ul.nav li.dropdown').hover(function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
	}, function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});
  </script>
  <script>
     </script> 
     
     <script>
			(function() {

				function SVGHamburger( el, options ) {
					this.el = el;
					this.init();
				}

				SVGHamburger.prototype.init = function() {
					this.shapeEl = this.el.querySelector( 'span.morph-shape' );

					var s = Snap( this.shapeEl.querySelector( 'svg' ) );
					this.pathEl1 = s.select( 'path:nth-of-type(1)' );
					this.pathEl2 = s.select( 'path:nth-of-type(2)' );
					this.paths = {
						reset : {
							path1 : this.pathEl1.attr( 'd' ),
							path2 : this.pathEl2.attr( 'd' )
						},
						open : this.shapeEl.getAttribute( 'data-morph-open' ).split( ';' ),
						close : this.shapeEl.getAttribute( 'data-morph-close' ).split( ';' )
					};

					this.isOpen = false;

					this.initEvents();
				};

				SVGHamburger.prototype.initEvents = function() {
					this.el.addEventListener( 'click', this.toggle.bind(this) );
				};

				SVGHamburger.prototype.toggle = function() {
					var self = this,
						paths = this.isOpen ? this.paths.close : this.paths.open;

					if( self.isOpen ) {
						setTimeout( function() { classie.remove( self.el, 'menu-button--open' ); }, 200 );
					}
					else {
						setTimeout( function() { classie.add( self.el, 'menu-button--open' ); }, 200 );
					}

					this.pathEl1.stop().animate( { 'path' : paths[0] }, 300, mina.easeout, function() {
						self.pathEl1.stop().animate( { 'path' : self.paths.reset.path1 }, 800, mina.elastic );
					} );
					this.pathEl2.stop().animate( { 'path' : paths[1] }, 300, mina.easeout, function() {
						self.pathEl2.stop().animate( { 'path' : self.paths.reset.path2 }, 800, mina.elastic );
					} );

					this.isOpen = !this.isOpen;
				};

				new SVGHamburger( document.getElementById( 'hamburger' ) );

			})();
		</script>
     
            </header>
            <!--HEADER ENDS-->