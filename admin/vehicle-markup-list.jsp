<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}
.show_date_to_user {
    padding: 0px 15px;
    position: relative;
    width: 93%;
    top: -25px;
    left: 5px;
    pointer-events: none;
    display: block;
    color: #000;
}

.scrollbar
{
	margin-left: 30px;
	float: left;
	height: 300px;
	width: 65px;
	background: #F5F5F5;
	overflow-y: scroll;
	margin-bottom: 25px;
}

.force-overflow
{
	min-height: 450px;
}

#wrapper
{
	text-align: center;
	width: 500px;
	margin: auto;
}


#style-3::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

#style-3::-webkit-scrollbar-thumb
{
	background-color: #000000;
}


</style>

<script type="text/javascript">
	function deletePopupCountryInfo(id, version) {
		$('#alert_box_delete').modal({
			show : true,
			keyboard : false
		});
		$('#alert_box_delete_body').text("Are your sure want to delete ?");
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "deleteCarMarkup.action?id=" + id + "&version="
				+ version;
		$("#deleteItem").val(finalUrl);
	}

	$(function() {
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "carMarkupList";
		$('#alert_box_info_ok').click(function() {
			window.location.assign(finalUrl);
			$('#alert_box_info').hide();

		});
		$('#alert_box_ok').click(function() {
			window.location.assign(finalUrl);
			$('#alert_box').hide();

		});
		$('#alert_box_delete_ok').click(function() {
			window.location.assign($("#deleteItem").val());
			$('#alert_box_delete').hide();
		});
		$('#cancel').click(function() {
			$('#error-alert').hide();

		});
	});
</script>



<!--************************************
        MAIN ADMIN AREA
    ****************************************-->

<!--ADMIN AREA BEGINS-->

<section class="wrapper container-fluid">

	<div class="">
		<div class="card1">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.car_markup_list" />
					</h5>
					<div class="set pull-left">
						<!--   <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
					</div>
					<%-- <div class="set pull-right">

						<a id="add_button" class="btn btn-sm btn-success "
							href="addCarMarkup"><i class="fa fa-plus fa-lg"
							aria-hidden="true" style="margin-right: 6px;"></i><span
							class="none-xs"> <s:text name="tgi.label.add_car_markup" /></span></a>

					</div> --%>
					<div class="set pull-right"
						style="margin-top: 5px; font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id=""
							data-toggle="collapse" style="cursor: pointer;"
							data-toggle="collapse" href="filterDiv" aria-expanded="false">
							<span class="text-spysr"><span class="filter-text"></span>
								<img class="clippy" src="admin/img/svg/filter.svg" width="12"
								alt="Copy to clipboard" style="margin-bottom: 3px;">
								Filter <i class="fa fa-angle-down"></i></span>
						</a>
					</div>
				</div>
				<br>
				<form action="carMarkupList" class="filter-form">
					<div class="" id="filterDiv" style="display: none;">
						<s:if test="%{#session.Company!=null}">
							<s:if
								test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
								<div class="col-md-2">
									<div class="form-group">
										<input type="hidden" id="HdncompanyTypeShowData" value="" />
										<select name="companyTypeShowData" id="companyTypeShowData"
											class="form-control input-sm">
											<option value="all" selected="selected"><s:text
													name="tgi.label.all" /></option>
											<option value="my"><s:text name="tgi.label.my_self" /></option>
											<s:if test="%{#session.Company!=null}">
												<s:if test="%{#session.Company.companyRole.isSuperUser()}">
													<option value="distributor"><s:text
															name="tgi.label.distributor" /></option>
												</s:if>
											</s:if>
											<option value="agency"><s:text
													name="tgi.label.agency" /></option>
										</select>
									</div>
								</div>
							</s:if>
						</s:if>

						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="startDate" id="startDate"
									placeholder="Created Date From........"
									class="form-control search-query date1 input-sm " />
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="endDate" id="endDate"
									placeholder="Created Date To...."
									class="form-control search-query date2 input-sm" />
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="pickUpDate" id="pickUpDate"
									placeholder="PickUp Date........"
									class="form-control search-query date5 input-sm " />
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="dropOffDate" id="dropOffDate"
									placeholder="DropOff Date...."
									class="form-control search-query date6 input-sm" />
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="promofareStartDate"
									id="promofareStartDate" placeholder="PromoStart Date ...."
									class="form-control search-query date3 input-sm" />
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="promofareEndDate" id="promofareEndDate"
									placeholder="PromoStart Date ...."
									class="form-control search-query date4 input-sm" />
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="name" id="markupname-json"
									placeholder="Markup Name...." class="form-control search-query input-sm" />
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="sipp" id="sipp-json"
									placeholder="SIPP ...." class="form-control search-query input-sm" />
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="pickUpCity" id="pickupcity-json"
									placeholder="Pick Up City...."
									class="form-control search-query input-sm" />
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<input type="text" name="dropOffCity" id="dropoffcity-json"
									placeholder="Drop Off City...."
									class="form-control search-query input-sm" />
							</div>
						</div>
						<div class="col-md-1">
							<div class="form-group">
								<input type="reset" class="btn btn-danger btn-block"
									id="configreset" value="Clear">
							</div>
						</div>
						<div class="col-md-1">
							<div class="form-group">
								<input type="submit" class="btn btn-info btn-block"
									value="Search" />
							</div>
						</div>
					</div>
				</form>

				<%-- <div class="set pull-right">
                                    <div class="dropdown">
                                      <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#"><s:text name="tgi.label.action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.another_action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.something_lse_here" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.separated_link" /></a></li>
                                      </ul>
                                    </div>
                               </div> --%>

				<div class="" id="user_form-group" align="right">
					<input type="hidden"
						value="<s:property value="%{#session.Company.companyUserId}"/>"
						id="companyUserId"> <input type="hidden"
						value="<s:property value="%{#session.Company.email}"/>" id="email">
					<input type="hidden"
						value="<s:property value="%{#session.User.companyUserId}"/>"
						id="userId">
					<%-- 			 <form class="form-inline" action="filterHotelMarkupList" method="post">
					<input type="hidden"
					value="<s:property value="%{#session.Company.company_userid}"/>"
					id="user_companyUserId" name="user_companyUserId">
					<div class="form-group">
						<!-- <label for="exampleInputAmount">Company Type</label> -->
						<div class="input-group">
							<input type="text" placeholder="Type Company Userid"
								class="form-control input-sm" id="search"
								value='<s:property value="company_user_id"/>'
								name="company_user_id">
						</div>
						<div class="input-group">
							<input type="text" placeholder="Type User Name"
								class="form-control input-sm" autocomplete="off"
								value='<s:property value="user_id"/>' id="userIdSearch"
								name="user_id">
						</div>
					</div>
					<div class="form-group rep-buto">
						<button type="submit" class="btn btn-primary"><s:text name="tgi.label.search" /></button>
					</div>
				</form>
 --%>
				</div>
				<%-- 							
			<div class="form-group" id="user_form-group" align="right">

				<input type="hidden" value="" id="companyUserId">
					 <input type="hidden" value="lintashelp@intellicommsolutions.com" id="email">
				<input type="hidden" value="" id="user_id">
					
				<form class="form-inline" action="filterCompanyList" method="post">
					<div class="form-group">
						<!-- <label for="exampleInputAmount">Company Type</label> -->
						<label><s:text name="tgi.label.add_new" /></label>
						<input type="hidden" value="" id="" name="">
						<div class="input-group">
							<input type="text" placeholder="Type Company Userid" class="form-control input-sm ui-autocomplete-input" id="search" value="" name="" autocomplete="off">
						</div>
						</div>
				</form>
</div> --%>
				<div class="cnt cnt-table">
					<div class="dash-table ">
						<div class="box clearfix">
							<div class="fw-container">
								<div class="fw-body">
									<div class="content mt-0 pt-0">
										<div id="example_wrapper" class="dataTables_wrapper">
											<table id="example"
												class="display dataTable responsive nowrap" role="grid"
												aria-describedby="example_info" style="width: 100%;"
												cellspacing="0">
												<!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
												<thead>
												<tr class="table-sub-header">
                                        	<th colspan="15" class="noborder-xs">
                                        	<div class="pull-right" style="padding:4px;margin-top: -6px;margin-bottom: -10px;">   
                                        	<div class="btn-group"> 
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Car Markup">
											  <a class="btn btn-sm btn-default" href="addCarMarkup" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class="">&nbsp;New&nbsp;</span></a> 
											</div>
											<div class="dropdown pull-left" style="margin-right:5px;">
											  <a class="btn btn-sm btn-default " href="#" >&nbsp;Import&nbsp;</a> 
											</div>
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a  class="btn btn-sm btn-default filterBtn" data-toggle="dropdown"  style="margin-bottom: 2px;margin-right: 3px;" title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${carMarkupList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
													<tr class="border-radius border-color" role="row">
														<th data-priority="1"><s:text name="tgi.label.sno" /></th>
														<th data-priority="2"><s:text name="tgi.label.markupname" /></th>
														<th data-priority="3"><s:text name="tgi.label.sipp" /></th>
														<th data-priority="4"><s:text name="tgi.label.pickup_city" /></th>
														<th data-priority="5"><s:text name="tgi.label.dropoff_city" /></th>
														<th data-priority="6"><s:text name="tgi.label.pickup_date" /></th>
														<th data-priority="7"><s:text name="tgi.label.dropoff_date" /></th>
														<th data-priority="8"><s:text name="tgi.label.promostarts" /></th>
														<th data-priority="9"><s:text name="tgi.label.promoends" /></th>
														<th data-priority="10"><s:text name="tgi.label.amount" /></th>
														<th data-priority="11"><s:text name="tgi.label.action" /></th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<s:iterator value="carMarkupList" status="rowCount">
														<tr>
															<td data-title="S.No	"><s:property
																	value="%{#rowCount.count}" /></td>
															<td data-title="MarkupName"><s:property value="name" /></td>
															<td data-title="SIPP"><s:property value="sipp" /></td>
															<td data-title="PickUp City"><s:property
																	value="pickUpCity" /></td>
															<td data-title="DropOff City"><s:property
																	value="dropOffCity" /></td>
															<td data-title="Pickup Date">
																${spysr:formatDate(pickUpDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}</td>
															<td data-title="DropOff Date">
																${spysr:formatDate(dropOffDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}</td>
															<td data-title="Promo Starts">${spysr:formatDate(promofareStartDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}</td>
															<td data-title="Promo Ends">${spysr:formatDate(promofareEndDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}</td>

															<td data-title="Amount"><s:property
																	value="markupAmount" /></td>
															<td data-title="Action">
																<div class="btn-group">
																	<a class="btn btn-xs btn-success dropdown-toggle"
																		data-toggle="dropdown" style="padding: 3px 6px;">
																		<i class="fa fa-cog fa-lg" aria-hidden="true"></i>
																	</a>
																	<ul
																		class="dropdown-menu dropdown-info pull-right align-left">
																		<li class=""><a
																			href="carMarkupEdit?id=<s:property value="id"/>"
																			class="btn  btn-xs" data-toggle="modal"> <span
																				class="glyphicon glyphicon-edit"></span> <s:text
																					name="tgi.label.edit" />
																		</a></li>
																		<li class="divider"></li>
																		<li class=""><input type="hidden" id="deleteItem">
																			<a href="#"
																			onclick="deletePopupCountryInfo('${id}','${version}')"
																			class="btn btn-xs "><span
																				class="glyphicon glyphicon-trash"></span> <s:text
																					name="tgi.label.delete" /></a></li>
																	</ul>
																</div>
															</td>
															<td></td>
														</tr>
													</s:iterator>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<!--ADMIN AREA ENDS-->

<link
	href="admin/css/jquery-ui.css"
	rel="stylesheet" type="text/css" />
<script src="admin/js/jquery-ui.js"></script>
<script>
	$(document)
			.ready(
					function() {
						var date1 = "${spysr:formatDate(startDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
						var date2 = "${spysr:formatDate(endDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
						spysr_date_custom_plugin('filter-form', 'date1',
								'date2', 'mm/dd/yyyy', 'MMM DD YYYY', date1,
								date2);

						var date3 = "${spysr:formatDate(startDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
						var date4 = "${spysr:formatDate(endDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
						spysr_date_custom_plugin('filter-form', 'date3',
								'date4', 'mm/dd/yyyy', 'MMM DD YYYY', date3,
								date4);

						var date5 = "${spysr:formatDate(startDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
						var date6 = "${spysr:formatDate(endDate,'MM/dd/yyyy', 'MMM dd yyyy')}";
						spysr_date_custom_plugin('filter-form', 'date5',
								'date6', 'mm/dd/yyyy', 'MMM DD YYYY', date5,
								date6);
					});
</script>

<script type="text/javascript" class="init">
	$(document).ready(function() {
		$('#filterBtn').click(function() {
			$('#filterDiv').toggle();
		});

		$('div.easy-autocomplete').removeAttr('style');
	});

	$('.faq-links').click(function() {
		var collapsed = $(this).find('i').hasClass('fa-compress');

		$('.faq-links').find('i').removeClass('fa-expand');

		$('.faq-links').find('i').addClass('fa-compress');
		if (collapsed)
			$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
	});
</script>


<s:if test="message != null && message  != ''">
	<script src="admin/js/jquery.min.js"></script>
	<script src="admin/js/bootstrap.min.js"></script>
	<c:choose>
		<c:when test="${fn:contains(message, 'deleted')}">
			<script>
				$(document).ready(function() {
					$('#alert_box').modal({
						show : true,
						keyboard : false
					});
					$('#alert_box_body').text('${param.message}');
				});
			</script>
		</c:when>
		<c:otherwise>
			<script>
				$(document).ready(function() {
					$('#alert_box_info').modal({
						show : true,
						keyboard : false
					});
					$('#alert_box_info_body').text("${param.message}");
				});
			</script>
		</c:otherwise>
	</c:choose>
</s:if>

<script>
	//markup name
	var MarkupName = {
		url : "getCarMarkupJson.action?data=markupName",
		getValue : "markupName",

		list : {
			match : {
				enabled : true
			}
		}
	};
	$("#markupname-json").easyAutocomplete(MarkupName);

	// sipp
	var SIPP = {
		url : "getCarMarkupJson.action?data=SIPP",
		getValue : "SIPP",
		list : {
			match : {
				enabled : true
			}
		}
	};
	$("#sipp-json").easyAutocomplete(SIPP);

	// pickup city
	var PickUpCity = {
		url : "getCarMarkupJson.action?data=pickUpCity",
		getValue : "pickUpCity",
		list : {
			match : {
				enabled : true
			}
		}
	};
	$("#pickupcity-json").easyAutocomplete(PickUpCity);

	// drop off city
	var DropOffCity = {
		url : "getCarMarkupJson.action?data=dropOffCity",
		getValue : "dropOffCity",
		list : {
			match : {
				enabled : true
			}
		}
	};
	$("#dropoffcity-json").easyAutocomplete(DropOffCity);

	$(document).ready(
			function() {
				$(".filterBtn").click(function() {
					$("#filterDiv").toggle(500)
				}), $("div.easy-autocomplete").removeAttr("style"), $(
						"#configreset").click(function() {
					$("#resetform")[0].reset()
				})
			}), $(".filter-link").click(
			function() {
				var e = $(this).find("i").hasClass("fa-angle-up");
				$(".filter-link").find("i").removeClass("fa-angle-down"), $(
						".filter-link").find("i").addClass("fa-angle-up"), e
						&& $(this).find("i").toggleClass(
								"fa-angle-up fa-2x fa-angle-down fa-2x")
			});
</script>
