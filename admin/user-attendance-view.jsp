<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
<link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
<style>
/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(admin/img/pre_loader.gif) center no-repeat #fff;
}
</style>
<section class="wrapper container-fluid">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						Employee Attendance
					</h5>
					<div class="set pull-left">
					</div>
				</div>
				
				<div class="row">
				<div class="col-md-12 mt-2">
					<div class="profile-timeline-card">
						<s:if test="%{((#session.User.userRoleId.isSuperUser()|| #session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
						<a class="btn btn-xs btn-outline-primary pull-right mr-1" href="#">
							<i class="fa fa-plus"></i> Report
						</a>
						</s:if>
						<div class="">
							<p class="mt-2">
								<span> <b class="blue-grey-text">Attendance</b></span>
							</p>
						</div>
						<hr>
						<div class="timeline-footer row">
							<div class="form-group row">
							<div class="col-sm-4">
						    <label for="inputEmail3" class="control-label">Date Of Attendance </label>
						    <div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						      <input type="text" name="attendDate" class="form-control input-sm" id="attendDate" placeholder="Date" onchange="javascript:getUserListJsp()">
						    </div>
							</div>
							</div>
						</div>
						</div>
						<div class="se-pre-con"></div>	
						<div class="show-user-list-jsp"></div>
						</div>
	</div>
	</div>
</section>

<script>
 $(function() {
	  $('input[name="attendDate"]').daterangepicker({
	    singleDatePicker: true,
	    showDropdowns: true,
	  }, function(start, end, label) {
		  
	  });
	});
</script>
<script type="text/javascript">

/*-------get student list by section wise---------*/

function getUserListJsp()
{
	var startDate = $("#attendDate").val();
	var formURL = "getUserAttenanceList.action?attendDateFlag="+startDate;
	 $(".se-pre-con").show(); 
	$.ajax({
		url : formURL,
		type : "GET",
		data : "json",
		async: true,
		success : function(data,textStatus,jqXHR) {
			if (jqXHR.status == 200) {
				console.debug(jqXHR.status);
				console.debug(data);
				$(".show-user-list-jsp").html(data);
				setInterval(function() {
					$(".show-user-list-jsp").show();
					$(".se-pre-con").hide();
				}, 1000); 
			} 
		},
		error : function(jqXHR,textStatus,errorThrown) {
			console.debug(jqXHR.status);
		}
	});
} 

</script>
