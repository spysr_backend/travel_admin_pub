<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
function deletePopupCountryInfo(id,version)
{
	$('#alert_box_delete').modal({
  	    show:true,
  	    keyboard: false
	    } );
	$('#alert_box_delete_body').text("Are your sure want to delete ?");
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	var finalUrl = newUrl+"delete_countryinfo.action?id="+id+"&version="+version;
	  $("#deleteItem").val(finalUrl);
}

$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"fetch_countryinfo";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	$('#alert_box_delete_ok').click(function() {
		window.location.assign($("#deleteItem").val()); 
			$('#alert_box_delete').hide();
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 
            <section class="wrapper container-fluid">
                <div class="">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.country_information" /> </h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                                <div class="set pull-right">
                                    <a id="add_button" class="btn btn-sm btn-success " href="insert_countryinfo_page"><i
							class="fa fa-plus fa-lg" aria-hidden="true"
							style="margin-right: 6px;"></i><s:text name="tgi.label.create" /> <span class="none-xs">
								<s:text name="tgi.label.country_information" /></span></a>
                               </div> 
                            </div>
                            <div class="hd clearfix">
                                <h5 class="pull-left"></h5>
                            </div>
							
							<input type="hidden"
					value="<s:property value="%{#session.Company.companyUserId}"/>"
					id="companyUserId"> <input type="hidden"
					value="<s:property value="%{#session.Company.email}"/>" id="email">
				<input type="hidden"
					value="<s:property value="%{#session.User.companyUserId}"/>"
					id="user_id">
					
                                <div class="cnt cnt-table">
										
                                <div class="table-responsive no-table-css">
                                        <table id="mytable" class="table table-bordered table-striped-column table-hover">
                                            <thead>
                                                <tr class="border-radius border-color">
                                                    <th><s:text name="tgi.label.s.no" /></th>
                                                    <th><s:text name="tgi.label.geographical_area" /></th>
                                                    <th><s:text name="tgi.label.population" /></th>
                                                    <th><s:text name="tgi.label.capital_city" /></th>
                                                    <th><s:text name="tgi.label.people" /></th>
                                                    <th><s:text name="tgi.label.language" /></th>
                                                    <th><s:text name="tgi.label.religion" /></th>
                                                    <th><s:text name="tgi.label.action" /></th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
										<s:iterator value="countryInfoList" status="i">
										<tr>
										<td data-title="S.No">${i.count}</td>
										<td data-title="Geographical Area">${geoArea}</td>
										<td data-title="Population">${population}</td>
										<td data-title="Capital City	">${capitalCity}</td>
										<td data-title="People">${people}</td>
										<td data-title="Language">${language}</td>
										<td data-title="Religion">${religion}</td>
           	       <%-- <a href="delete_countryinfo.action?id=${id}&version=${version}" class="btn btn-xs btn-danger">Delete</a> --%>
										<td data-title="Action">
										<div class="btn-group">
											<a class="btn btn-xs btn-success dropdown-toggle"
												data-toggle="dropdown" style="padding: 3px 6px;"> <i
												class="fa fa-cog fa-lg" aria-hidden="true"></i>
											</a>
											<ul class="dropdown-menu dropdown-info pull-right align-left">
												<li class=""><a href="edit_countryinfo.action?id=${id}" class="btn btn-xs ">
												<span class="glyphicon glyphicon-edit"></span> <s:text name="tgi.label.edit" /></a></li>
												<li class="divider"></li>
												<li class=""><input type="hidden" id="deleteItem">
           	       								<a href="#" onclick="deletePopupCountryInfo('${id}','${version}')" class="btn btn-xs ">
           	       								<span class="glyphicon glyphicon-trash"></span> <s:text name="tgi.label.delete" /></a></li>
											</ul>
										</div>
									</td>
										</tr>
										</s:iterator>
                                            </tbody>
                                        </table>
                                    </div>
								
								<div class="">
								
								<div class="col-md-6">
								
<div class="" style="font-size=16px;margin-top:27px;">Showing 1 to 10 of 70 entries</div>
								</div>
								<div class="col-md-6">
                                    <ul class="pagination" style="float:right;">
									<li><a href="#"><s:text name="tgi.label.previous" /></a></li>
    <li class="disabled"><a href="#">&laquo;</a></li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">&raquo;</a></li>
	<li><a href="#"><s:text name="tgi.label.next" /></a></li>
</ul></div>


                                    <br><br>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
            
                  	   <s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'deleted')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>