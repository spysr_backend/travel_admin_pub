<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

 <link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
 <link rel="stylesheet" href="admin/css/less-plugins/awesome-bootstrap-checkbox.css">
 <script type="text/javascript" src="admin/js/admin/panel-app.js"></script>
  
  <style>
.comment-box {
	display: none;
}
.comment-list{
    border-top: 1px dashed #b2b2b3;
}
.dropdown-menu {
    min-width: 130px;
    padding: 8px 0px;
}
.dropdown-menu>li>a {
    display: block;
    padding: 4px 15px;
    clear: both;
    font-weight: normal;
    line-height: 23px;
    color: rgb(0, 112, 210);
    white-space: nowrap;
}
button.btn.dropdown-toggle.btn-default {
    padding: 4px 6px;
}
.clay{
margin-bottom: 3px;
}
span.text-change {
    font-weight: 500;
    font-size: 20px;
}
a.text-phone{
    color: #ffffff;
    text-decoration: none;
}
a.text-phone:focus, a.text-phone:hover {
    color: #ffffff;
    text-decoration: none;
}
span#sizing-addon2 {
    padding-bottom: 0px;
}
.trip-font-icon {
    background-color: #dedede;
    padding: 2px 6px 2px 6px;
    border-radius: 3px;
    box-shadow: darkturquoise;
}
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 8px;
    line-height: 1;
    vertical-align: top;
    border-top: 1px solid #dddddd;
    text-align: left;
}
.image-preview-modal-footer {
    border-top: 0px solid rgb(221, 219, 218);
    border-bottom-right-radius: 0.25rem;
    border-bottom-left-radius: .25rem;
    padding: 5px 0px 5px 0px;
    background-color: rgb(255, 255, 255);
    text-align: right;
}
.image-preview-modal-footer {
    padding: 1px 0px 42px;
    text-align: right;
    border-top: 0px solid #e5e5e5;
}
.image-preview-modal-header {
    min-height: 16.43px;
    padding: 2px 10px 6px 10px;
    border-bottom: 2px solid #e5e5e5;
}
#heading-elements {
    background-color: inherit;
    position: absolute;
    top: 18%;
    right: 4px;
    height: 26px;
    margin-top: 0px;
}
.popover {
    max-width: 382px;
    font-size: 12px;
    }
   a.disabled {
   pointer-events: none;
   cursor: default;
}
.line-btn {
    padding: 0px 0 0 0px;
    font-size: 18px;
    color: #795548;
}
</style>
      <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid"> 

                <div class="row">
                <div class="pnl">
                <nav aria-label="breadcrumb">
				  <div class="hd clearfix own-bradcum mb-4">
				  			<div class="dropdown pull-right mt-10">
							  <a href="addTravelSalesMyLead" class="btn btn-primary btn-xs"> <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
							   <a href="listTravelSalesMyLead" class="btn btn-xs btn-primary" type="button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back To List</a>
							</div>
				    <h4 class=""><img class="clay" src="admin/img/svg/tourist.svg" width="26" alt="Higest"> My Travel Lead : <span class="text-change">${travelSalesMyLead.title}</span></h4>   
				  </div>
				</nav>
				</div>
                <div class="col-md-3">
					<div class="sidebar-detached">
						<div class="sidebar sidebar-default sidebar-separate">
							<div class="sidebar-content">

								<!-- User details -->
								<div class="content-group">
									<div class="row panel-body bg-indigo-400 border-radius-top text-center" style="background-image: url(admin/img/client-bg.jpg); background-size: contain;">
										<div class="col-md-6 content-group-sm pull-left text-left">
											<h5 class="no-margin-bottom"> Name:</h5> 
											<h5>Phone:</h5>
										</div>
										<div class="col-md-6 content-group-sm pull-right text-right">
											<h5 class="no-margin-bottom" style="font-weight:500;"> ${travelSalesMyLead.firstName} ${travelSalesMyLead.lastName}</h5> 
											<h5 style="font-weight:500;">
											<a href="https://api.whatsapp.com/send?phone=+91${travelSalesMyLead.mobile}&text=hi ${firstName}"><img class="clippy" src="admin/img/svg/whatsapp.svg" width="14"style="margin-bottom: 3px;"></a>&nbsp;
											<a class="text-phone" href="tel:${travelSalesMyLead.mobile}"><img class="clippy" src="admin/img/svg/telephone-w.svg" width="14"style="margin-bottom: 3px;">&nbsp;${travelSalesMyLead.mobile}</a>
											</h5>
										</div>
									</div>

									<div class="panel no-border-top no-border-radius-top">
										<ul class="navigation">
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Lead Number" >
											<i class="icon-files-empty"></i> Lead Number
											<span class="pull-right bg-warning-400">${travelSalesMyLead.leadNumber} </span></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Lead Number" >
											<i class="icon-files-empty"></i> Reference No <span class="pull-right bg-warning-400">
											${travelSalesMyLead.referenceNumber != null && travelSalesMyLead.referenceNumber != ''?travelSalesMyLead.referenceNumber:'N/A'}
											</span></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Email Customer" >
												<a href="mailto:${travelSalesMyLead.email}" aria-expanded="false" class="a-tab"><i class="icon-files-empty"></i> ${travelSalesMyLead.email} <img class="clippy" data-clipboard-text="${travelSalesMyLead.email}" src="admin/img/svg/copy-b.svg" width="14"></a>
												<span class="pull-right bg-warning-400">
												<a href="mailto:${travelSalesMyLead.email}?subject=${travelSalesMyLead.title}&body= Hi ${travelSalesMyLead.firstName}">
												<img class="clippy" src="admin/img/svg/envelope-g.svg" width="16"style="margin-bottom: 3px;"></a>
												<span class="text-success">( 
									                <c:choose>
									                <c:when test="${emailCountStatusList.size() > 0}">
									                ${emailCountStatusList.size()} 
									                <span class="link">
									                <a data-email-content="popover-content_${rowstatus}" data-placement="left" data-toggle="lead-popover" title="Email Sent History" 
									                	data-container="body" data-html="true" href="#!" id="login" style="cursor: pointer;" class="">times
									                </a>
									                </span>
									                </c:when>
									                <c:otherwise>
														0 times														                
									                </c:otherwise>
									                </c:choose>)
									                </span>
												
												</span>
												</span>
											</li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Email Verify Status" >
											<a href="#schedule" data-toggle="tab" aria-expanded="false" class="a-tab">
											<i class="icon-files-empty"></i> Email Verified 
											<span class="pull-right bg-warning-400">
											<span class="center">
											${travelSalesMyLead.verifyStatus != false?'<img class="clippy" src="admin/img/svg/checkedg.svg" width="16" alt="Copy to clipboard" style="margin-bottom: 3px;">':'<img class="clippy" src="admin/img/svg/close.svg" width="14" alt="Copy to clipboard" style="margin-bottom: 3px;">'}
                                                    </span>
											</span></a></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Assigned By User" ><i class="icon-files-empty"></i> Booking Status
											<span class="pull-right bg-warning-400">
											${travelSalesMyLead.bookingStatus != null && travelSalesMyLead.bookingStatus != '' ? travelSalesMyLead.bookingStatus : 'N/A'}
											</span></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Offline Trip Booking Status" ><i class="icon-files-empty"></i> Trip Booked
											<span class="pull-right bg-warning-400">
											${travelSalesMyLead.tripBooked == true ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>'}
											</span></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Assigned By User" ><i class="icon-files-empty"></i> Assigned To
											<span class="pull-right bg-warning-400">
											${travelSalesMyLeadFollowUpData.assignedByName != null && travelSalesMyLeadFollowUpData.assignedByName != '' ? travelSalesMyLeadFollowUpData.assignedByName : 'N/A'}
											</span></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Created User By" ><i class="icon-files-empty"></i> Created By 
											<span class="pull-right bg-warning-400">${travelSalesMyLead.createdByName}</span></span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Created At Date" ><i class="icon-files-empty"></i> Created At 
											<c:choose>
											<c:when test="${travelSalesMyLead.createdDate != null}">
												<span class="pull-right bg-warning-400">${spysr:formatDate(travelSalesMyLead.createdDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy hh:mm a')}</span>
											</c:when>
											<c:otherwise>
											<span class="pull-right bg-warning-400">N/A</span>
											</c:otherwise>
											</c:choose>
											</span></li>
											<li class="li-change"><span data-toggle="tooltip" data-placement="top" title="Assign Date" ><i class="icon-files-empty"></i> Assign At 
											<span class="pull-right bg-warning-400">
											<c:choose>
											<c:when test="${travelSalesMyLeadFollowUpData.assignDate != null}">
											${spysr:formatDate(travelSalesMyLeadFollowUpData.assignDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy hh:mm a')}
											</c:when>
											<c:otherwise>
											<span>N/A</span>
											</c:otherwise>
											</c:choose>
											</span></span></li>
											
										</ul>
									</div>
								</div>

							</div>
						</div>
					</div>
					<!--  -->
					<form id="updateTravelSalesMyLeadForm" class="form-horizontal" method="post">
					<div class="container-detached">
						<div class="content-detached row-minus">
									<div class="col-md-12 col-xs-12 col-sm-12">
									<div class="panel panel-flat panel-bg-navy">
										<div class="panel-heading">
											<h6 class="panel-title">Lead Information<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
							                	</ul>
						                	</div>
										</div>
										<hr style="display: block">
										<div class="panel-body" style="display: block;">
												<div class="form-group">
													<div class="row mb-2">
														<div class="col-md-6">
														<label>First Name</label>
															<input type="text" name="firstName" value="${travelSalesMyLead.firstName}" class="form-control input-sm">
														</div>
														<div class="col-md-6">
														<label>Last Name</label>
															<input type="text" name="lastName" value="${travelSalesMyLead.lastName}" class="form-control input-sm">
														</div>
														<div class="col-md-12">
														<label>Email</label>
															<input type="email" name="email" value="${travelSalesMyLead.email}" class="form-control input-sm">
														</div>
														<div class="col-md-12">
														<label>Phone</label>
															<input type="text" name="mobile" value="${travelSalesMyLead.mobile}" class="form-control input-sm">
														</div>
														<div class="col-md-6">
															<label>City</label>
															<input type="text" name="city" value="${travelSalesMyLead.city}" class="form-control input-sm">
														</div>
														<div class="col-md-6">
															<label>Country</label>
															  <select name="country" id="country" class="form-control input-sm" data-live-search="true">
															    <option value="">Select Country</option>
																		<c:forEach items="${countryList}" var="country">
																			<option value="${country.countryName}" ${country.countryName == travelSalesMyLead.country ? 'selected' : ''} >${country.countryName}</option>
																</c:forEach>
															</select> 
														</div>
														<div class="col-sm-12">
												    <label for="inputEmail3" class="control-label"> Mail Verify Status</label>
											     	<div class="checkbox checkbox-default">
								                        <input type="checkbox" name="verifyStatus" id="verify-status"  value="${travelSalesMyLead.verifyStatus}" class="mail-status" ${travelSalesMyLead.verifyStatus == true ? 'checked': ''}>
								                        <label for="verify-status">Verify</label>
							                   		</div>
												    </div>
													</div>
													 <hr class="mt-20">
													<div class="row">
										<div class="form-group">
										    <div class="col-sm-12">
										    <label for="title" class="control-label">Lead Title</label>
										      <input type="text" name="title"  class="form-control input-sm" value="${travelSalesMyLead.title}" id="title" placeholder="lead title">
										    </div>
										    <div class="col-sm-12">
										    <label for="departure-date" class="control-label text-danger">Journey Date *</label>
										    <div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
										      <input type="text" name="departureDateFlag"  class="form-control input-sm" value="${spysr:formatDate(travelSalesMyLead.departureDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}" id="departure-date" placeholder="Date">
										    </div>
										    </div>
										    <%-- <div class="col-md-12">
											 <label for="start-date" class="control-label text-warning">Check-In Date - Check-Out Date *</label>
											<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													<input type="text" name="startEndDateRange" id="start-date" class="form-control input-sm" <c:choose>
										    <c:when test="${travelSalesMyLead.startDate != null && travelSalesMyLead.endDate}">
										    	value="${spysr:formatDate(travelSalesMyLead.startDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')} - ${spysr:formatDate(travelSalesMyLead.endDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}"
										    </c:when>
										    <c:otherwise>
										    value=""
										    </c:otherwise>
										    </c:choose>>
											</div>
											</div> --%>
											<div class="col-md-12">
											 <label for="inputEmail3" class="control-label text-danger">Check-In Date</label>
											<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													<input type="text" name="startDateFlag" id="start-date-flag" value="${spysr:formatDate(travelSalesMyLead.startDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}" class="form-control input-sm" placeholder="End Date...">
											</div>
											</div>
											<div class="col-md-12">
											 <label for="inputEmail3" class="control-label text-danger">Check-Out Date</label>
											<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													<input type="text" name="endDateFlag" id="end-date-flag" value="${spysr:formatDate(travelSalesMyLead.endDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}" class="form-control input-sm datetimepicker2" placeholder="End Date...">
											</div>
											</div>
											<div class="col-md-12">
											 <label for="arrival-date" class="control-label text-danger">Arrival Date *</label>
											<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													<input type="text" name="arrivalDateFlag" id="arrival-date" value="${spysr:formatDate(travelSalesMyLead.arrivalDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}" class="form-control input-sm datetimepicker2" placeholder="End Date...">
											</div>
											</div>
											 <div class="col-sm-6">
										    <label for="inputEmail3" class="control-label">Budget</label>
											<select aria-hidden="true" name="budget" id="budget" class="form-control selectpicker">
														   <option>Budget</option>
														   <option value="0-2" ${'0-2'==travelSalesMyLead.budget ? 'selected': ''}>Economy (0 - 2 star)</option>
														   <option value="3-4" ${'3-4' ==travelSalesMyLead.budget ? 'selected': ''}>Standard (3 - 4 star)</option>
														   <option value="5+" ${'5+' ==travelSalesMyLead.budget ? 'selected': ''}>Luxury (5 star &amp; above)</option>
														</select>
										    </div>
										    <div class="col-sm-6">
										    <label for="inputEmail3" class="control-label">Duration</label>
											<select aria-hidden="true" name="duration" id="duration" class="form-control selectpicker">
														  <option value="0">Duration*</option>
														  	<c:forEach items="${tourDurationMap}" var="duration">
																	<option value="${duration.key}" ${duration.key == travelSalesMyLead.duration ? 'selected': ''}>${duration.value}</option>
																</c:forEach>
											</select>
										    </div>
										    <div class="col-sm-6">
										    <label for="callTime" class="control-label">Best Time to call</label>
										      <select aria-hidden="true" name="callTime" id="callTime" class="form-control selectpicker input-sm">
											  <option value="">Best time to call</option>
												<c:forEach items="${travelSalesLeadUtility.callTime}" var="callTime">
													<c:choose>
													<c:when test="${callTime == travelSalesMyLead.callTime}">
														<option value="${callTime}" selected="selected">${callTime}</option>
													</c:when>
													<c:otherwise>
														<option value="${callTime}">${callTime}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
											 </select>
										    </div>
										    <div class="col-sm-6">
										    <label for="callTime" class="control-label">Lead Source ${travelSalesMyLead.employeeReferralId}</label> 
										    <select aria-hidden="true" name="leadSource" id="lead_source" class="form-control selectpicker">
													<c:forEach items="${travelSalesLeadUtility.leadSource}" var="lead_source">
														<c:choose>
															<c:when test="${lead_source == travelSalesMyLead.leadSource}">
																<option value="${lead_source}" selected="selected">${lead_source}</option>
															</c:when>
															<c:otherwise>
																<option value="${lead_source}">${lead_source}</option>
															</c:otherwise>
														</c:choose>
													</c:forEach>
												</select>
											</div>
											<c:choose>
											<c:when test="${travelSalesMyLead.leadSource == 'Employee Referral'}">
											<div class="col-md-12 col-sm-12" id="employee-name">
										    <label for="callTime" class="control-label">Employee</label> 
											   <select name="employeeReferralId" id="employee-referral-id" class="form-control selectpicker" data-live-search="true">
													<option value="0" selected="selected">Select User</option>
													<c:forEach items="${userVos}" var="userOnj">
																<option value="${userOnj.userId}" ${userOnj.userId == travelSalesMyLead.employeeReferralId ? 'selected' : ''}>${userOnj.firstName} ${userOnj.lastName}</option>
													</c:forEach>
												</select>
											</div>
											</c:when>
											<c:otherwise>
											<div class="col-md-12 col-sm-12" style="display: none" id="employee-name">
										    <label for="callTime" class="control-label">Employee</label> 
											   <select name="employeeReferralId" id="employee-referral-id" class="form-control selectpicker" data-live-search="true">
													<option value="0" selected="selected">Select User</option>
													<c:forEach items="${userVos}" var="userOnj">
																<option value="${userOnj.userId}">${userOnj.firstName} ${userOnj.lastName}</option>
													</c:forEach>
												</select>
											</div>
											</c:otherwise>
											</c:choose>
										<div class="col-sm-12">
										    <label for="noOfRoom" class="control-label">No Of Room</label>
										      <select aria-hidden="true" name="noOfRoom" id="noOfRoom" class="form-control selectpicker input-sm">
											  <option value="0" selected="selected">Select Room</option>
											  <c:forEach var="i" begin="0" end="50" step="1">
												<option value="${i}" ${i == travelSalesMyLead.noOfRoom ? 'selected': ''}>${i}</option>
											</c:forEach>
											</select>
										    </div>
										    <div class="col-md-4">
										    <label for="noOfRoom" class="control-label">Adult</label>
					                            <select aria-hidden="true" name="adult" id="adult" class="form-control input-sm">
												  <option value="0" selected="selected">Adult</option>
												  <c:forEach var="i" begin="0" end="120" step="1">
													<option value="${i}" ${i==travelSalesMyLead.adult ? 'selected': ''}>${i}</option>
												</c:forEach>
												</select>
											</div>		
											<div class="col-md-4">
											<label for="noOfRoom" class="control-label">Kid's (5 to 8) </label>
					                            <select aria-hidden="true" name="kid" id="kid" class="form-control input-sm">
												  <option value="0" selected="selected">Kid's</option>
												  <c:forEach var="i" begin="0" end="120" step="1">
													<option value="${i}" ${i==travelSalesMyLead.kid ? 'selected': ''}>${i}</option>
												</c:forEach>
												</select>
											</div>		
											<div class="col-md-4">
											<label for="noOfRoom" class="control-label">Kid's (0 to 5)</label>
					                            	<select aria-hidden="true" name="infant" id="infant" class="form-control input-sm">
													  <option value="0" selected="selected">New Born</option>
													  <c:forEach var="i" begin="0" end="5" step="1">
														<option value="${i}" ${i==travelSalesMyLead.infant ? 'selected': ''}>${i}</option>
													</c:forEach>
													</select>
											</div>
										    <div class="col-sm-6">
										    <label for="inputEmail3" class="control-label">Departure </label>
										      <input type="text" name="destinationFrom" id="" class="form-control input-sm" value="${travelSalesMyLead.destinationFrom}">
										    </div>
										    <div class="col-sm-6">
										    <label for="inputEmail3" class="control-label">Going To</label>
										      <input type="text" name="destinationTo" id="" class="form-control input-sm" value="${travelSalesMyLead.destinationTo}">
										    </div>
										  </div>
										  </div>
										 <hr>
										 <div class="row">
									  	<div class="form-group">
									  	<h5 style="margin-left: 14px;margin-top: 0px;">Trip Services</h5>
									  	 <div class="row">
									  	 <div class="col-md-3 col-sm-4 col-xs-4">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="tour" id="tour"  value="${travelSalesMyLead.tour}" class="mail-status" ${travelSalesMyLead.tour == true ? 'checked': ''}>
						                        <label for="tour">Tour</label>
					                   		</div>
										    </div>
									  	 <div class="col-md-3 col-sm-4 col-xs-4">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="flight" id="flight"  value="${travelSalesMyLead.flight}" class="" ${travelSalesMyLead.flight == true ? 'checked': ''}>
						                        <label for="flight">Flight</label>
					                   		</div>
										    </div>
									  	 <div class="col-md-3 col-sm-4 col-xs-4">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="hotel" id="hotel"  value="${travelSalesMyLead.hotel}" class="" ${travelSalesMyLead.hotel == true ? 'checked': ''}>
						                        <label for="hotel">Hotel</label>
					                   		</div>
										    </div>
									  	 <div class="col-md-3 col-sm-4 col-xs-4">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="car" id="car"  value="${travelSalesMyLead.car}" class="" ${travelSalesMyLead.car == true ? 'checked': ''}>
						                        <label for="car">Car</label>
					                   		</div>
										    </div>
									  	 <div class="col-md-3 col-sm-4 col-xs-4">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="bus" id="bus"  value="${travelSalesMyLead.bus}" class="" ${travelSalesMyLead.bus == true ? 'checked': ''}>
						                        <label for="bus">Bus</label>
					                   		</div>
										    </div>
									  	 <div class="col-md-3 col-sm-4 col-xs-4">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="cruise" id="cruise"  value="${travelSalesMyLead.cruise}" class="" ${travelSalesMyLead.cruise == true ? 'checked': ''}>
						                        <label for="cruise">Cruise</label>
					                   		</div>
										    </div>
										   </div>
										   <div class="row">
									  	 <div class="col-md-6 col-sm-6 col-xs-6">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="tourInsurance" id="tourInsurance"  value="${travelSalesMyLead.tourInsurance}" class="" ${travelSalesMyLead.tourInsurance == true ? 'checked': ''}>
						                        <label for="tourInsurance">Tour Insurance</label>
					                   		</div>
										    </div>
									  	 <div class="col-md-6 col-sm-6 col-xs-6">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="flightInsurance" id="flightInsurance"  value="${travelSalesMyLead.flightInsurance}" class="" ${travelSalesMyLead.flightInsurance == true ? 'checked': ''}>
						                        <label for="flightInsurance">Flight Insurance</label>
					                   		</div>
										    </div>
									  	 </div>
									  	 </div>
									  	 </div>
										 <hr>
										<div class="row">
									  	<div class="form-group">
									  	<h5 style="margin-left: 14px;margin-top: 0px;">People Traveling</h5>
									  	 <div class="row mb-2">
									  	 <div class="col-md-12 col-sm-12 col-xs-12">
						                         <label for="trip-theme" class="control-label">Trip Theme</label>
						                        <select name="tripTheme" id="trip-theme" class="form-control input-sm">
						                        	<option value=""> Select Trip Theme</option>
						                        	<c:forEach items="${travelSalesLeadUtility.tripTheme}" var="tripTheme">
													<option value="${tripTheme}" ${tripTheme == travelSalesMyLead.tripTheme ? 'selected': ''}>${tripTheme}</option>
													</c:forEach>
						                        </select>
										    </div>
										    </div>
									  	 <%-- <div class="col-md-4 col-sm-4 col-xs-12">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="honeymoon" id="honeymoon"  value="${travelSalesMyLead.honeymoon}" class="mail-status" ${travelSalesMyLead.honeymoon == true ? 'checked': ''}>
						                        <label for="honeymoon">Honeymoon</label>
					                   		</div>
										    </div> --%>
										    <div class="row">
										    <div class="col-md-6 col-sm-6 col-xs-12">
										    <p>When to book </p>
											<div class="radio radio-default radio-inline">
									            <input type="radio" name="whenBook" id="radio7" value="ASAP" ${travelSalesMyLead.whenBook == 'ASAP' ? 'checked': ''}>
									            <label for="radio7">ASAP</label>
									        </div>
									        <div class="radio radio-default radio-inline">
									            <input type="radio" name="whenBook" id="radio8" value="Explore" ${travelSalesMyLead.whenBook == 'Explore' ? 'checked': ''}>
									            <label for="radio8">Explore</label>
									        </div>
										    </div>
										    </div>
										   <div class="col-md-4 col-sm-4 col-xs-12">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="flightBooked" id="flight-booked"  value="${travelSalesMyLead.flightBooked}" class="mail-status" ${travelSalesMyLead.flightBooked == true ? 'checked': ''}>
						                        <label for="flight-booked">Flight Booked</label>
					                   		</div>
										    </div>
										    <div class="col-md-4 col-sm-4 col-xs-12">
									        <div class="checkbox checkbox-default">
						                        <input type="checkbox" name="firstTime" id="first-time"  value="${travelSalesMyLead.firstTime}" class="mail-status" ${travelSalesMyLead.firstTime == true ? 'checked': ''}>
						                        <label for="first-time">First Time</label>
					                   		</div>
										    </div>
										    
										 </div>
										 </div>
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
					                            <label>Requirement<span class="text-danger"> *</span></label>
					                            <textarea name="requirement" id="" rows="4" cols="" placeholder="" class="form-control input-sm" >${travelSalesMyLead.requirement}</textarea>
											</div>		
										</div>
										</div>
										<div class="row">
										<div class="text-right">
											<input type="hidden" name="id" value="${travelSalesMyLead.id}">
						                     <button type="submit" class="btn btn-primary btn-block">Update Changes <b><i class="icon-circle-right2"></i></b></button>
						            	</div>
										</div>
									</div>
										</div>
									</div>
									</div>
						</div>
					</div>
						</form>
					
					</div>
					<div class="col-md-9 col-xs-12 col-sm-12">
					<div class="container-detached">
						<div class="content-detached">

									<!-- Follow Up info -->
									<div class="panel panel-flat panel-bg-navy">
										<div class="panel-heading">
											<h6 class="panel-title">Follow Up<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="15" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="panel-fullscreen" class="icon-a" ><img class="clippy" src="admin/img/svg/full-screen.svg" width="15" alt="Copy to clipboard" ></a></li>
							                	</ul>
						                	</div>
										</div>
										<hr >
										<div class="panel-body" style="display: block;">
												<form id="updateTravelMyLeadFollowUpForm" class="from-horizontal" method="post" enctype="multipart/form-data">
														<div class="row">
														<div class="col-md-2">
														<div class="form-group">
														<span data-toggle="tooltip" data-placement="top" title="Select Lead Status" >
															<select aria-hidden="true" name="leadStatus" id="lead-status" class="form-control selectpicker input-sm">
															  <option value="">Select Lead Status</option>
																<c:forEach items="${leadStatusMap}" var="lstatus">
																	<option value="${lstatus.key}" ${lstatus.key == travelSalesMyLeadFollowUpData.leadStatus ? 'selected': ''}>${lstatus.value}</option>
																</c:forEach>
															 </select>
														</span>
														</div>
													  </div>
														<div class="col-md-2">
												  		<div class="form-group">
												  		<span data-toggle="tooltip" data-placement="top" title="Select Comment" >
															<select aria-hidden="true" name="title" id="title" class="form-control selectpicker input-sm">
															  <option value="">Add Title</option>
																<c:forEach items="${followUpTitleMap}" var="title">
																	<option value="${title.key}" ${title.key == travelSalesMyLeadFollowUpData.title ? 'selected': ''}>${title.value}</option>
																</c:forEach>
															 </select>
														</span>
														</div>
														</div>
														<div class="col-md-3">
												  		<div class="form-group">
												  		<span data-toggle="tooltip" data-placement="top" title="Add Description" >
															<textarea rows="1" name="note" class="form-control input-sm" placeholder="Please add note...">${travelSalesMyLeadFollowUpData.note}</textarea>
														</span>
														</div>
														</div>
														<div class="col-md-2">
														<div class="form-group">
														<span data-toggle="tooltip" data-placement="top" title="Select Lead Rating" >
															<select aria-hidden="true" name="leadRating" id="lead-rating" class="form-control selectpicker input-sm">
															  <option value="">Select Lead Rating</option>
																<c:forEach items="${leadRatingMap}" var="rating">
																	<option value="${rating.key}" ${rating.key == travelSalesMyLeadFollowUpData.leadRating ? 'selected': ''}>${rating.value}</option>
																</c:forEach>
															 </select>
														</span>
														</div>
													</div>
													<div class="col-sm-2">
														<span data-toggle="tooltip" data-placement="top" title="Select Assign To User" >
													     <select name="assignTo" id="assign-to" class="form-control selectpicker" data-live-search="true">
															<option value="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="assign">
																		<option value="${assign.userId}" ${assign.userId == travelSalesMyLeadFollowUpData.assignTo ? 'selected' : ''} >${assign.firstName} ${assign.lastName}</option>
															</c:forEach>
														
														</select>
														</span>
										    		</div>
														<div class="col-md-1">
														<div class="form-group">
														<button type="submit" class="btn btn-sm btn-success btn-block" id="update-travel-lead-followupBtn">Save</button>
														 <input type="hidden" name="id" value="${travelSalesMyLeadFollowUpData.id}">
														 <input type="hidden" name="travelMyLeadId" id="travel-my-lead-id" value="${travelSalesMyLead.id}">
														</div>
														</div>
														</div>
											</form>
										<!--follow up detail  -->
										
										<c:choose>
										<c:when test="${travelSalesMyLeadFollowUpData!=null && travelSalesMyLeadFollowUpData.id !=null}">
										<div class="col-md-12">
											<h5 class="subtitle subtitle-lined">Follow Up History</h5>
											<div class="row">
							<div class="col-sm-4">
								<div class="row">
									<div class="col-xs-6"><label class="form-control-label">Title</label></div>
									<div class="col-xs-6">
									<c:choose>
									<c:when test="${travelSalesMyLeadFollowUpData.title != null && travelSalesMyLeadFollowUpData.title != ''}">
									${travelSalesMyLeadFollowUpData.title}
									</c:when>
									<c:otherwise>
									<span class="text-warning"> N/A</span>
									</c:otherwise>
									</c:choose>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-6"><label class="form-control-label">Lead Status</label></div>
									<div class="col-xs-6">
									<c:choose>
										<c:when test="${travelSalesMyLeadFollowUpData.leadStatus == 'New'}">
										<span class="bug-issue-status-max-width-medium follow-status-info">${travelSalesMyLeadFollowUpData.leadStatus}</span>
										</c:when>						
										<c:when test="${travelSalesMyLeadFollowUpData.leadStatus == 'Active'}">
										<span class="bug-issue-status-max-width-medium follow-status-active">${travelSalesMyLeadFollowUpData.leadStatus}</span>
										</c:when>						
										<c:when test="${travelSalesMyLeadFollowUpData.leadStatus == 'Booked'}">
										<span class="bug-issue-status-max-width-medium follow-status-success">${travelSalesMyLeadFollowUpData.leadStatus}</span>
										</c:when>						
										<c:when test="${travelSalesMyLeadFollowUpData.leadStatus == 'No Connect'}">
										<span class="bug-issue-status-max-width-medium follow-status-warning">${travelSalesMyLeadFollowUpData.leadStatus}</span>
										</c:when>						
										<c:when test="${travelSalesMyLeadFollowUpData.leadStatus == 'Closed'}">
										<span class="bug-issue-status-max-width-medium follow-status-danger">${travelSalesMyLeadFollowUpData.leadStatus}</span>
										</c:when>
										<c:otherwise>
										${travelSalesMyLeadFollowUpData.leadStatus}
										</c:otherwise>					
										</c:choose>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="row">
									<div class="col-xs-6"><label class="form-control-label">Lead Rating</label></div>
									<div class="col-xs-6">
									<c:choose>
										<c:when test="${travelSalesMyLeadFollowUpData.leadRating == 'Highest'}">
										<img class="clay" src="admin/img/svg/highest.svg" width="14" alt="Higest"> <span class="">${travelSalesMyLeadFollowUpData.leadRating}</span>
										</c:when>						
										<c:when test="${travelSalesMyLeadFollowUpData.leadRating == 'High'}">
										<img class="clay" src="admin/img/svg/high.svg" width="14" alt="Higest"> <span class="">${travelSalesMyLeadFollowUpData.leadRating}</span>
										</c:when>						
										<c:when test="${travelSalesMyLeadFollowUpData.leadRating == 'Medium'}">
										<img class="clay" src="admin/img/svg/medium.svg" width="14" alt="Higest"> <span class="">${travelSalesMyLeadFollowUpData.leadRating}</span>
										</c:when>						
										<c:when test="${travelSalesMyLeadFollowUpData.leadRating == 'Low'}">
										<img class="clay" src="admin/img/svg/low.svg" width="14" alt="Higest"> <span class="">${travelSalesMyLeadFollowUpData.leadRating}</span>
										</c:when>						
										<c:when test="${travelSalesMyLeadFollowUpData.leadRating == 'Lowest'}">
										<img class="clay" src="admin/img/svg/lowest.svg" width="14" alt="Higest"> <span class="">${travelSalesMyLeadFollowUpData.leadRating}</span>
										</c:when>	
										<c:otherwise>
										<span class="text-warning"> N/A</span>
										</c:otherwise>					
										</c:choose>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="row">
									<div class="col-xs-6 "><label class="form-control-label">Assignee By</label></div>
									<div class="col-xs-6">${travelSalesMyLeadFollowUpData.assignedByName}</div>
								</div>
								<div class="row">
									<div class="col-xs-6"><label class="form-control-label">Assigned To</label></div>
									<div class="col-xs-6">${travelSalesMyLeadFollowUpData.assignedToName}</div>
								</div>
							</div>
							</div>
											
										</div>
										<c:if test="${travelSalesMyLeadFollowUpData.note != null && travelSalesMyLeadFollowUpData.note != ''}">
										<div class="col-md-12 mt-20">
											<h5 class="subtitle subtitle-lined"><label class="form-control-label">DESCRIPTION</label></h5>
												<p>${fn:replace(travelSalesMyLeadFollowUpData.note, newLineChar, "</br>")}</p>
										</div>
										</c:if>
										<div class="col-md-12">
											<h5 class="subtitle subtitle-lined"></h5>
										</div>
									
										<!--follow up history template -->
							<div class="row">
									<ul class="nav nav-tabs nav-tabs-line" role="tablist">
							              <li class="nav-item active">
							              <a class="nav-link" data-toggle="tab" href="#history" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>History</a>
							              </li>
							              <li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#comment" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>Comment</a>
							              </li>
							              <li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#call-log" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>Log a Call</a>
							              </li>
							              <li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#trip_quotation" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>Trip Quotation</a>
							              </li>
							              <li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#documents" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>Documents</a>
							              </li>
							              <!-- <li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#mail-sent" aria-controls="mail-sent" role="tab" aria-selected="false"><i class="icon wb-user" aria-hidden="true"></i>Mail Sent</a>
							              </li> -->
											<li class="nav-item">
							              <a class="nav-link" data-toggle="tab" href="#reminder" aria-controls="#reminder" role="tab" aria-selected="false"><i class="icon wb-cloud" aria-hidden="true"></i>Reminders</a>
							              </li>
						            </ul>
						            <div class="panel-tab-body">
					              <div class="tab-content">
					              <div class="tab-pane active" id="history" role="tabpanel">
					                 <div class="row">
											<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
												<s:iterator value="travelSalesMyLeadFollowUpHistoryList" status="rowstatus" >
													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="headingOne">
															<h4 class="panel-title">
																<a role="button" class="bugtracker-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_${rowstatus.count}" aria-expanded="true">
																	${rowCount.count} <span><b>Date : </b> <small class="text-muted"> 
																		<fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" /></small></span>
																	<span><b>Lead Status : </b> ${leadStatus}</span>
																</a>
															</h4>
														</div>
														<div id="collapse_${rowstatus.count}" class="panel-collapse collapse" role="tabpanel">
															<div class="panel-body">
																<div class="row">
																	<div class="col-sm-6">
																		<div class="row">
																			<div class="col-xs-6">Title</div>
																			<div class="col-xs-6">${title}</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-6">Type</div>
																			<div class="col-xs-6">${leadStatus}</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-6">Priority</div>
																			<div class="col-xs-6">${leadRating}</div>
																		</div>
																	</div>
																	<!-- col-sm-6 -->
																	<div class="col-sm-6">
																		<div class="row">
																			<div class="col-xs-6">Assignee By</div>
																			<div class="col-xs-6">${assignedByName}</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-6">Assigned To</div>
																			<div class="col-xs-6">${assignedToName}</div>
																		</div>

																	</div>
																	<!-- col-sm-6 -->
																	<div class="col-md-12 mt-10">
																		<h5 class="subtitle subtitle-lined">Note</h5>
																		<p class="">${note}</p>
																	</div>
																</div>
																<div class="row">
																	<div class="col-sm-12 ">
																		<c:if test="comments!=null">
																			<div class="b-comments">
																				<p>
																					<b>Comments : </b> ${comments}
																				</p>
																			</div>
																		</c:if>
																		<c:if test="${filePath !=null}">
																			<div class="download">
																				<p>Download</p>
																				<p>
																					<a href="downloadBugTrackerHistoryFile?fileName=${filePath}" class="btn btn-success btn-xs">${filePath} </a>
																				</p>
																			</div>
																		</c:if>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</s:iterator>
											</div>
								</div>
					                </div>
<!-- follow up comment tab pane -->
					              <div class="tab-pane" id="comment" role="tabpanel">
									<!-- <button class="btn btn-primary add-comment-btn">
										<i class="fa fa-comments mr5"></i>Add Comment
									</button> -->
									<div class="card">
							  <div class="card-header">Add Comment</div>
							  <div class="card-body">
							  <div class="row">
										<form method="post" class="form-horizontal" name="myForm" id="salesMyLeadFollowCommentForm" enctype="multipart/form-data">
											 <input type="hidden" name="leadFollowUpId" id="my-lead-follow-up" value="${travelSalesMyLeadFollowUpData.id}">
												<div class="col-md-12">
												
												<label class="">To &nbsp;&nbsp;<span class="text-primary">this lead</span></label>
													<div class="form-group mt-10">
															<textarea rows="3" cols="2" class="form-control" name="comments" placeholder="please add comments" required="required"></textarea>
													</div>
													<!-- <div class="form-group">
														<label for="uploadimage" class="col-sm-2 control-label">Upload
															File </label>
														<div class="col-sm-8">
															<div class="row">
																<div class="col-sm-6 file-upload">
																	<input type="file" id="filePath" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,image/*"
																		ng-file-select="onFileSelect($files)" name="bugTrackerHistory.filePath">
																</div>
																<div class="col-sm-6 ">
																	<div id="fileinfo">
																		<div id="fileError"></div>
																	</div>
																</div>
															</div>
														</div>
													</div> -->
												</div>
												<div class="col-4">
													<div class="form-group">
														<div class="col-xs-12 submitWrap">
															<button type="submit" id="save-follow-up-comment" class="btn btn-success" data-form-name="salesMyLeadFollowCommentForm">Save</button>
														</div>
													</div>
												</div>
										</form>
								</div>
								</div>  
								</div>
								
											<s:iterator value="travelSalesMyLeadFollowUpCommentList" status="rowstatus">
											<ul class="media-list comment-list mt-10">
															<li class="media">
																<div class="media-body">
																<div class="dropdown pull-right mt-10">
																	  <button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
																	  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
																	  <ul class="dropdown-menu">
																	    <li class="action"><a href="#" data-event="duplicate" data-toggle="modal" data-target="#update_comment_${rowstatus.count}">Edit</a></li>
																	    <li class="action"><a href="#" class="delete-follow-up-comment" data-comment-id="${id}">Delete</a></li>
																	  </ul>
																	</div>
																	<h4>${createdByUserName}</h4>
																	<small class="text-muted">
																	Created At :<em><fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" /></em> 
																	<c:choose>
																	<c:when test="${updatedAt != null}">
																	-- Updated At:<em><fmt:formatDate value="${updatedAt}" pattern="E, MMM dd, yyyy h:m a" /></em>
																	</c:when>
																	<c:otherwise>
																	
																	</c:otherwise>
																	</c:choose>
																	
																	</small>
																	<p>${fn:replace(comments, newLineChar, "</br>")}</p>
																	<div class="row">
																		<div class="col-sm-12 ">
																			<c:if test="${filePath !=null}">
																				<div class="download">
																					<p>
																						<b>Download</b>
																					</p>
																					<p>
																						<a href="downloadBugTrackerHistoryFile?fileName=${filePath}" class="btn btn-success btn-xs">${filePath} </a>
																					</p>
																				</div>
																			</c:if>
																		</div>
																	</div>
																</div> 
															</li>
													</ul>
	<!--update follow up comment model-->
									<div class="modal fade" id="update_comment_${rowstatus.count}" role="dialog">
										<div class="modal-dialog modal-lg">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title text-center">Edit Comment</h4>
													<button type="button" class="close slds-modal__close" data-dismiss="modal">
													<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="20" alt="Copy to clipboard">
													</button>
												</div>
												<form id="update_commentForm_${rowstatus.count}" method="post" class="bv-form">
												<input type="hidden" name="commentId" id="comment-id" value="${id}">
												<input type="hidden" name="leadFollowUpId" value="${travelSalesMyLeadFollowUpData.id}">
													<div class="modal-body">
													 <div class="row">
																<div class="col-md-12 mt-20">
																	<div class="controls">
																			<div class="form-group">
																			<textarea rows="8" cols="" name="comments" class="form-control input-sm">${comments}</textarea>
																			</div>
																	</div>
																</div>
															</div>
												</div>
													<div class="modal-footer">
														<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
														<button type="submit" class="btn btn-success update-followup-comment-btn" id="" data-modal-id="update_comment_${rowstatus.count}" data-form-name="update_commentForm_${rowstatus.count}">Save</button>
													</div>
												</form>
											</div>
					
										</div>
								</div>
									<!-- // update follow up comment-->
											</s:iterator>
					                </div>
<!--Follow Up Call Log Tab Panel  -->
					              <div class="tab-pane" id="call-log" role="tabpanel">
					                <a href="#" class="btn btn-sm btn-info" data-event="duplicate" data-toggle="modal" data-target="#follow-up-log-call"><i class="fa fa-plus"></i> Log a Call</a>
					                <s:if test="travelSalesMyLeadFollowUpCallList.size > 0">
											<s:iterator value="travelSalesMyLeadFollowUpCallList" status="rowstatus">
					                <ul class="media-list comment-list mt-10">
												<li class="media">
												<div class="dropdown pull-right mt-10">
												  <button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
												  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
												  <ul class="dropdown-menu">
												    <li class="action"><a href="#" data-event="duplicate" data-toggle="modal" data-target="#update_call_${rowstatus.count}">Edit</a></li>
												    <li class="action"><a href="#" class="delete-follow-up-call" data-call-id="${id}">Delete</a></li>
												  </ul>
												</div>
													<div class="media-body">
														<h4>${createdByUserName}
														<small class="">
															<fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" />
														</small></h4>
														<h5>${subject}</h5>
														<p>${comment}.</p>
													</div> 
												</li>
										</ul>
										<!--update follow up comment-->
									<div class="modal fade" id="update_call_${rowstatus.count}" role="dialog">
										<div class="modal-dialog">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title text-center">Edit Call Log</h4>
													<button type="button" class="close slds-modal__close" data-dismiss="modal">
													<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="18" alt="Copy to clipboard">
													</button>
												</div>
												<form id="update_call_form_${rowstatus.count}" method="post" class="form-horizontal">
												<input type="hidden" name="callId" id="comment-id" value="${id}">
												<input type="hidden" name="leadFollowUpId" value="${travelSalesMyLeadFollowUpData.id}">
													<div class="modal-body">
													 <div class="row">
																			<div class="form-group">
																<label class="form-control-label col-md-4" for="comment"><span class="text-danger">*</span> Subject</label>
																<div class="col-md-8">
																	<div class="controls">
																			<input type="text" name="subject" id="subject"  class="form-control input-sm" value="${subject}" required="required"/>
																			</div>
																	</div>
																</div>
																			<div class="form-group">
																<label class="form-control-label col-md-4" for="comment">Comment</label>
																<div class="col-md-8">
																	<div class="controls">
																			<textarea rows="4" cols="" name="comment" class="form-control input-sm">${comment}</textarea>
																			</div>
																	</div>
																</div>
														</div>
																<div class="row">
																			<div class="form-group">
																<label class="form-control-label col-md-4" for="comment"><span class="text-danger">*</span> Priority${priority}</label>
																<div class="col-md-8">
																	<div class="controls">
																			<select name="priority" class="form-control input-sm">
																				<option value="None" selected="selected">--None--</option>
																				<option value="Normal" ${'Normal' == priority ? 'selected': ''}>Normal</option>
																				<option value="High"   ${'High' == priority ? 'selected': ''}>High</option>
																			</select>
																			</div>
																	</div>
																</div>
																			<div class="form-group">
																<label class="form-control-label col-md-4" for="status"><span class="text-danger">*</span> Status</label>
																<div class="col-md-8">
																	<div class="controls">
																			<select name="status" class="form-control input-sm">
																			    <option value="None" selected="selected">--None--</option>
																				<option value="Open"   ${'Open' == status ? 'selected': ''}>Open</option>
																				<option value="Completed" ${'Completed' == status ? 'selected': ''}>Completed</option>
																			</select>
																			</div>
																	</div>
																</div>
																</div>
																
												</div>
													<div class="modal-simple-footer">
														<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
														<button type="submit" class="btn btn-success update-followup-call-btn" id="" data-modal-id="update_call_${rowstatus.count}" data-form-name="update_call_form_${rowstatus.count}">Save</button>
													</div>
												</form>
											</div>
					
										</div>
								</div>
									<!-- // update follow up comment-->
											</s:iterator>
										</s:if>
					                </div>
<!--Follow Up Email Tab-->
					         <%-- <div class="tab-pane" id="mail-sent" role="tabpanel">
					         	<a href="#" class="btn btn-sm btn-info" data-event="duplicate" data-toggle="modal" data-target="#follow-up-email"><i class="fa fa-plus"></i> Send Email</a>
					                 <s:iterator value="travelSalesLeadFollowUpEmailList" status="rowstatus">
											<ul class="media-list comment-list mt-10">
															<li class="media">
																<div class="media-body">
																<div class="dropdown pull-right mt-10">
																	  <button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
																	  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
																	  <ul class="dropdown-menu">
																	    <li class="action"><a href="#" data-event="duplicate" data-toggle="modal" data-target="#update_comment_${rowstatus.count}">Edit</a></li>
																	    <li class="action"><a href="#" class="delete-follow-up-comment" data-comment-id="${id}">Delete</a></li>
																	  </ul>
																	</div>
																	<h4>${createdByUserName}</h4>
																	<small class="text-muted">
																	Created At :<em><fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" /></em> 
																	<c:choose>
																	<c:when test="${updatedAt != null}">
																	<b>--</b> Updated At:<em><fmt:formatDate value="${updatedAt}" pattern="E, MMM dd, yyyy h:m a" /></em>
																	</c:when>
																	<c:otherwise>
																	
																	</c:otherwise>
																	</c:choose>
																	
																	</small>
																	<p>${emailSubject}.</p>
																</div> 
															</li>
													</ul>
													</s:iterator>
					                </div> --%>
<!-- START######################################################################  Trip Quotation ###################################################################################-->
								  <div class="tab-pane" id="trip_quotation" role="tabpanel">
					         				<div class="btn-group pull-right">
					         				<div class="pull-left" style="margin-left:5px;">
					         				<span data-toggle="tooltip" data-placement="top" title="Add New Trip Quotation">
					         				<a href="addTripQuotation?travelLeadNumber=${travelSalesMyLead.leadNumber}" class="btn btn-sm btn-info" target="_blank"><img class="clippy" src="admin/img/svg/plus.svg" width="15"> Trip Quote</a>
					         				</span>
					         				</div>
					         				<div class="pull-right" style="margin-left:5px;">
												<div class="" id="btn-title" data-toggle="tooltip" data-placement="top"  data-size="${tripQuotationsList.size()}" title="Please Select Row">
												  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="send_all_trip_mail" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
												  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Send Email</span>
												  </button>  
												</div>
											</div>
											</div>
											<br/>
											<br/>
									<c:choose>
									<c:when test="${tripQuotationVoList != null && tripQuotationVoList.size() >0}">
					                 <c:forEach items="${tripQuotationVoList}" var="tripQuotation" varStatus="rowstatus">
												                   	 <div class="panel panel-default mt-2">
															            <div class="panel-heading">
															                <h5 class="panel-title" style="margin-left: -10px;">
															                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne_${rowstatus.count}"><i class="fa fa-plus"></i>
															                    Quotation ${rowstatus.count} : <span class="text-warning" style="font-weight: 500;">${tripQuotation.quotationNumber}</span></a>
															                </h5>
															                <div class="heading-elements" id="heading-elements">
															                <div class="pull-left">
															                <c:choose>
															                <c:when test="${tripQuotation.tourOrderCount <= 1}">
															                <a href="addTourTrip?quotationId=${tripQuotation.id}&leadNumber=${travelSalesMyLead.leadNumber}" class="btn btn-xs btn-primary" target="_blank" style="    margin-right: 5px;">
															                <img class="clippy" src="admin/img/svg/plus.svg" width="10"><span data-toggle="tooltip" data-placement="top" title="Book New Tour"> Book Trip </span>
															                 <span class="badge" data-toggle="tooltip" data-placement="top"  title="${tripQuotation.tourOrderCount > 0 ?'Tour Booking Success':'No Tour Booking Found'}">
															                 	   ${tripQuotation.tourOrderCount}</span></a>
															                </c:when>
															                <c:otherwise>
															                <span data-toggle="tooltip" data-placement="top" title="You can' book more than 1 booking, firstly cancel that booking then book again">
															                <button type="button" class="btn btn-xs btn-primary" style="cursor: no-drop;"><img class="clippy" src="admin/img/svg/plus.svg" width="10"> 
															                Book Trip <span class="badge"> ${tripQuotation.tourOrderCount}</span></button></span>
															                </c:otherwise>
															                </c:choose>
															                
															                </div>
															                <span class="line-btn" style="font-size: 16px;">  | </span>
															                
															                <div class="pull-right"style="margin-left:5px;">
															               <%--  <span data-toggle="tooltip" data-placement="left" title="Mail Send : ${tripQuotation.tripQuotationEmailStatusList.size()} times"> --%>
															                <span class="text-success">( 
															                <c:choose>
															                <c:when test="${tripQuotation.tripQuotationEmailStatusList.size() > 0}">
															                ${tripQuotation.tripQuotationEmailStatusList.size()} 
															                <span class="link">
															                <a data-email-content="popover-content_${rowstatus}" data-placement="left" data-toggle="popover" title="Email Sent History" data-container="body" data-html="true" href="#!" id="login" style="cursor: pointer;" class="">times
															                </a>
															                </span>
															                </c:when>
															                <c:otherwise>
																				0 times														                
															                </c:otherwise>
															                </c:choose>)</span>
															                </div>
															                <div class="pull-right"style="margin-left:5px;">
															                <div data-toggle="tooltip" data-placement="left" title="Please Select Row">
															                <div class="checkbox checkbox-default" style="margin-top: 0px;">
														                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${tripQuotation.id}" class="check_row">
														                        <label for="checkbox1_sd_${rowstatus.count}"><span class="text-warning">Send Email</span></label>
													                   	 	</div>
													                   	 	</div>
													                   	 	</div>
															                </div>
															                <%-- 
															                <div class="pull-right">
															                <div data-toggle="tooltip" data-placement="top" title="Please Select Row">
															                <div class="checkbox checkbox-default" style=" margin-top: -20px;">
														                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${tripQuotation.id}" class="check_row">
														                        <label for="checkbox1_sd_${rowstatus.count}"><span class="text-warning">Send Email</span></label>
													                   	 	</div>
													                   	 	</div>
													                   	 	</div> --%>
													                  <!-- popover templete for email history -->
													                   	 	<div id="popover-content" class="hidden">
																			        <div class="row">
																			        <table class="table table-bordered table-striped-column table-hover mb-0">
													                                   <thead>
													                                   <tr class="small-trip-table">
																						<th>SNo</th>
																						<th>Mail Status</th>
																						<th>Send At</th>
																						</tr>
																					</thead>
																						<tbody>
																						<c:forEach items="${tripQuotation.tripQuotationEmailStatusList}" var="emailData" varStatus="estatus">
																						<tr class="small-trip-table">
																						<td>${estatus.count}</td>
																						<td>
																						<c:choose>
																						<c:when test="${emailData.mailStatus == 1}">
																						<span class="text-success">Success</span>
																						</c:when>
																						<c:when test="${emailData.mailStatus == 0}">
																						<span class="text-warning">Pending</span>
																						</c:when>
																						<c:when test="${emailData.mailStatus == -1}">
																						<span class="text-danger">Failed</span>
																						</c:when>
																						</c:choose>
																						</td>
																						<td>
																						${spysr:formatDate(emailData.createdDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy hh:mm a')}
																						</td>
																						</tr>
																						</c:forEach>
																					</tbody></table>
																			        </div>
																			    </div>
													                   	 	
															            </div>
															            <div id="collapseOne_${rowstatus.count}" class="panel-collapse collapse">
															                <div class="panel-body">
													<section class="col-md-12">
														<div class="row">
														<p class="pt-0 pb-2 mb-2 mt-3">
																<span class="trip-font-icon"><i class="fa fa-location-arrow"></i></span> <span> <b class="blue-grey-text">Trip Departure Details</b> : ${tripQuotation.title}</span>
																<span class="pull-right">
																<a class="btn btn-xs btn-danger" data-event="duplicate" data-toggle="modal" data-target="#edit-trip-quotation_${rowstatus.count}" data-backdrop="static" data-keyboard="false">
																<img class="clippy" src="admin/img/svg/edit-document-w.svg" width="16"><span class="">&nbsp;Edit</span>
																</a>
																</span>
														</p> 
															<div class="no-table-css clearfix">
														 <table class="table table-bordered table-striped-column table-hover mb-0">
						                                   <thead>
						                                   <tr class="small-trip-table">
															<th>Departure Date</th>
															<th>Check In Date</th>
															<th>Check Out Date</th>
															<th>Arrival Date</th>
															<th><s:text name="tgi.label.destiation_city" /></th>
															<th><s:text name="tgi.label.destination_country" /></th>
														</tr></thead>
															<tr class="small-trip-table">
															<td data-title="Departure Date">
															<c:if test="${tripQuotation != null && tripQuotation.departureDate != null}">
															${tripQuotation.departureDate}
															</c:if>
															</td>
															<td data-title="Check In Date">
															<c:if test="${tripQuotation != null && tripQuotation.startDate != null}">
															${tripQuotation.startDate}
															</c:if>
															</td>
															<td data-title="Check Out Date">
															<c:if test="${tripQuotation != null && tripQuotation.endDate != null}">
															${tripQuotation.endDate}
															</c:if>
															</td>
															<td data-title="Arrival Date">
															<c:if test="${tripQuotation != null && tripQuotation.arrivalDate != null}">
															${tripQuotation.arrivalDate}
															</c:if>
															</td>
																<td data-title="<s:text name="tgi.label.destiation_city" />">${tripQuotation.destinationFrom}</td>
																<td data-title="<s:text name="tgi.label.destination_country" />">${tripQuotation.destinationTo}</td>
															</tr></table></div>
															<!-- edit trip quotation  -->
															</div>
															 <div class="row">
															<p class="pt-0 pb-2 mb-2 mt-2">
																	<span class="trip-font-icon"><i class="fa fa-money"></i></span> <span> <b class="blue-grey-text">Price & Rates</b></span>
																	<span class="pull-right">
																	<a class="btn btn-xs btn-danger" data-event="duplicate" data-toggle="modal" data-target="#edit-trip-quotation-rate_${rowstatus.count}" >
																	<img class="clippy" src="admin/img/svg/edit-document-w.svg" width="16"><span class="">&nbsp;Edit</span>
																	</a>
																	</span>
																</p> 
															<div class="table-responsive no-table-css clearfix mb-0">
															 <table class="table table-bordered table-striped-column table-hover mb-0">
							                                   <thead>
							                                   <tr class="border-radius border-color">
															<th>Supplier Price</th>
															<th>Markup Price</th>
															<th>GST (%)</th>
															<th>Base Price</th>
															<th>Processing Fee (%)</th>								
															<th>Total Amount</th>								
															<th>Price Currency</th>								
															</tr></thead>
															<tr>
																	<td data-title="<s:text name="tgi.label.total_passengers" />">${tripQuotation.supplierPrice}</td>
																	<td data-title="<s:text name="tgi.label.base_amount" />">${tripQuotation.markup}</td>
																	<td data-title="<s:text name="tgi.label.fee_amount" />">${tripQuotation.gst}</td>
																	<td data-title="<s:text name="tgi.label.fee_amount" />">${tripQuotation.basePrice}</td>
																	<td data-title="<s:text name="tgi.label.fee_amount" />">${tripQuotation.processingFee}</td>
																	<td data-title="<s:text name="tgi.label.fee_amount" />">${tripQuotation.totalAmount}</td>
																	<td data-title="<s:text name="tgi.label.total" />">INR</td>
																</tr>
															</table></div>
															</div>
									
								<div class="row">
								<p class="pt-0 pb-2 mb-2 mt-2">
										<span class="trip-font-icon"><i class="fa fa-globe"></i></span> <span> <b class="blue-grey-text">Quotation Info </b></span>
									</p>   
							<c:if test="${tripQuotation.itineraryMap != null && tripQuotation.itineraryMap.size()>0}">
								<div class="panel panel-default" style="padding-bottom: 0px;">
									<div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_ity_${rowstatus.count}"
												aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_ity_${rowstatus.count}" aria-expanded="true" aria-controls="collapseOne"> 
											<i class="more-less fa fa-plus pull-right"></i> <s:text name="tgi.label.itinerary_description" />
											</a>
										</h4>
									</div>
									<div id="collapse_ity_${rowstatus.count}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body" style="padding-top: 0px;">
										<div class="col-md-12">
											<c:forEach items="${tripQuotation.itineraryMap}" var="itineraryObj" varStatus="status">
												<br>
												<div class="pull-right">
												<span data-toggle="tooltip" data-placement="left" title="Edit Itinerary">
												<a class="btn btn-xs btn-primary" data-event="duplicate" data-toggle="modal" data-target="#edit_trip_quotation_itinerary_${status.count}">
												<img class="clippy" src="admin/img/svg/edit-document-w.svg" width="15" alt="Edit"></a>
												</span>
												<span data-toggle="tooltip" data-placement="left" title="Delete Itinerary">
												<a class="btn btn-xs btn-danger" data-itinerary-id="${itineraryObj.value.id}" onclick="javascritp:delteTripItinerary(this)">
												<img class="clippy" src="admin/img/svg/rubbish-bin.svg" width="15" alt="Delete"></a>
												</span>
												</div>
												<li><strong style="color: #c20a2f; font-weight: 700;">${itineraryObj.key} Day : &nbsp;</strong><b>${itineraryObj.value.title}</b></li>
												<li>${fn:replace(itineraryObj.value.description, newLineChar, "</br>")} </li>
												<li class="divider" style="border-bottom: 1px solid #cecece;"></li>
												<li class="mt-2 mb-4">
												<div class="col-md-4 col-sm-12">
												<b>Hotel :</b> ${itineraryObj.value.hotelName}
												</div>
												<div class="col-md-4 col-sm-12">
												<b>City :</b> ${itineraryObj.value.hotelCity}
												</div>
												<div class="col-md-4 col-sm-12">
												<b>Rating :</b> ${itineraryObj.value.hotelRating}
												</div>
												</li>
											<!-- edit trip itinerary -->
											<div class="modal fade" id="edit_trip_quotation_itinerary_${status.count}" role="dialog">
													<div class="modal-dialog">
														<!-- Modal content-->
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title text-center">Edit Trip Quotation Itinerary</h4>
																<button type="button" class="close slds-modal__close" data-dismiss="modal">
																	<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
																</button>
															</div>
															<form id="updateTripQuotationItineraryForm_${status.count}" method="POST" class="bv-form">
																<input type="hidden" id="itinerary-id" name="id" value="${itineraryObj.value.id}" />
																<div class="modal-body">
																	<div class="row">
																		<div class="form-group">
																			<label class="col-md-2 col-form-label">Day <span id="day-to-add">${itineraryObj.key}</span>: </label>
																			<div class="col-md-10"><span data-toggle="tooltip" data-placement="top" title="Ininerary Title" >
																					<input type="text" class="form-control" name="title" id="title" autocomplete="off" value="${itineraryObj.value.title}">
																					</span>
																			</div>
																		</div>
																		<div class="col-md-12">
																			<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">&nbsp;</label>
																			<div class="controls">
																				<div class="form-group"><span data-toggle="tooltip" data-placement="top" title="Ininerary Description" >
																					<textarea class="form-control" name="description" id="description" autocomplete="off">${itineraryObj.value.description}</textarea>
																				</span></div>
																			</div>
																		</div>
																		<hr>
																		<div class="col-md-12">
																			<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Hotel Name</label>
																			<div class="controls">
																				<div class="form-group">
																					<input type="text" class="form-control" name="hotelName" id="hotel-name" autocomplete="off" value="${itineraryObj.value.hotelName}">
																				</div>
																			</div>
																		</div>
																		<div class="col-md-12">
																			<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Hotel City</label>
																			<div class="controls">
																				<div class="form-group">
																				<input type="text" class="form-control" name="hotelCity" id="hotel-city" autocomplete="off" value="${itineraryObj.value.hotelCity}">
																				</div>
																			</div>
																		</div>
																		<div class="col-md-12">
																			<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Rating</label>
																			<div class="controls">
																				<div class="form-group">
																				<select class="form-control" name="hotelRating" id="hotel-rating">
								                                                        <option value="1" ${'1' == itineraryObj.value.hotelRating ? 'selected': ''}>1 Star</option>
								                                                        <option value="2" ${'2' == itineraryObj.value.hotelRating ? 'selected': ''}>2 Star</option>
								                                                        <option value="3" ${'3' == itineraryObj.value.hotelRating ? 'selected': ''}>3 Star</option>
								                                                        <option value="4" ${'4' == itineraryObj.value.hotelRating ? 'selected': ''}>4 Star</option>
								                                                        <option value="5" ${'5' == itineraryObj.value.hotelRating ? 'selected': ''}>5 Star</option>
								                                                        <option value="5+"${'5+' == itineraryObj.value.hotelRating ? 'selected': ''}>5+ Star</option>
								                                                 </select>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
																		<button type="submit" class="btn btn-primary edit-trip-quotation-itinerary-btn" data-form-name="updateTripQuotationItineraryForm_${status.count}" data-modal-id="edit_trip_quotation_itinerary_${status.count}">Save</button>
																	</div>
																</form>
															</div>
														</div>
													</div>
											</c:forEach>
											</div>
										</div>
									</div>
								</div>
							</c:if>

							<c:if test="${tripQuotation.tripQuotationInclusionsList != null && tripQuotation.tripQuotationInclusionsList.size() >0}">
								<div class="panel panel-default">
									<div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_inc_${rowstatus.count}" aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_inc_${rowstatus.count}" aria-expanded="true" aria-controls="collapseOne"> 
												<i class="more-less fa fa-plus pull-right"></i> Trip Inclusion
											</a>
										</h4>
									</div>
									<div id="collapse_inc_${rowstatus.count}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="col-md-12 ">
											<div class="long-description ">
												<div class="pull-right">
													<span data-toggle="tooltip" data-placement="left" title="Add Inclusion">
													<a class="btn- btn-xs btn-primary" data-event="duplicate" data-toggle="modal" data-target="#add_trip_inclusion" style="cursor: pointer;">
													<img class="clippy" src="admin/img/svg/plus.svg" width="10"> Add</a>
													</span>
													</div>
												<c:forEach items="${tripQuotation.tripQuotationInclusionsList}" var="inclusion" varStatus="status">
													<c:choose>
													<c:when test="${inclusion.title != null && inclusion.title != ''}">
													<div class="pnl1">
													<h4 style="color: #c20a2f; font-weight: 700;font-size:16px;" >${inclusion.title}</h4>
													</div>
													</c:when>
													<c:otherwise>
													&nbsp;
													</c:otherwise>
													</c:choose>
													<div class="item-list mb-2 mt-2">
														<span><img class="clippy" src="admin/img/svg/checkedg.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"><span> ${inclusion.description}</span></span>
														<div class="pull-right">
														<span data-toggle="tooltip" data-placement="left" title="Delete Inclusion">
														<a class="btn btn-xs btn-danger" data-inclusion-id="${inclusion.id}" onclick="javascritp:delteTripInclusion(this)">
														<img class="clippy" src="admin/img/svg/rubbish-bin.svg" width="15" alt="Delete"></a>
														</span>
														</div>
													</div>
												</c:forEach>
											</div>
											</div>
										</div>
									</div>
								</div>
							</c:if>
							<!-- trip inclusion add -->
								<div class="modal fade" id="add_trip_inclusion" role="dialog">
											<div class="modal-dialog">
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title text-center">Add Trip Inclusion</h4>
														<button type="button" class="close slds-modal__close" data-dismiss="modal">
															<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
														</button>
													</div>
													<form id="saveTripInclusionForm" method="post" class="bv-form">
														<input type="hidden" id="quotationId" name="quotationId" value="${tripQuotation.id}" />
														<div class="modal-body">
															<div class="row">
																<div class="col-md-12">
																	<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Description</label>
																	<div class="controls">
																		<div class="form-group">
																			<input type="text" name="title" id="title" class="form-control" />
																		</div>
																	</div>
																</div>
																<div class="col-md-12">
																	<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Description</label>
																	<div class="controls">
																		<div class="form-group">
																			<textarea rows="3"  name="description" id="description" class="form-control" ></textarea>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
															<button type="submit" class="btn btn-primary" id="save-destination-faq-btn">Save</button>
														</div>
													</form>
												</div>
											</div>
									</div>
							<!--  -->
							<c:if test="${tripQuotation.tripQuotationExclusionsList != null && tripQuotation.tripQuotationExclusionsList.size() >0}">
								<div class="panel panel-default">
									<div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_exc__${rowstatus.count}" aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_exc__${rowstatus.count}" aria-expanded="true" aria-controls="collapseOne"> 
												<i class="more-less fa fa-plus pull-right"></i> Trip Exculsion
											</a>
										</h4>
									</div>
									<div id="collapse_exc__${rowstatus.count}" class="panel-collapse collapse"
										role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="col-md-12 ">
											<div class="long-description ">
												<div class="pull-right">
												<span data-toggle="tooltip" data-placement="left" title="Add Exclusion">
													<a class="btn- btn-xs btn-primary" data-event="duplicate" data-toggle="modal" data-target="#add_trip_exclusion" style="cursor: pointer;">
													<img class="clippy" src="admin/img/svg/plus.svg" width="10"> Add</a>
												</span>
												</div>
												<c:forEach items="${tripQuotation.tripQuotationExclusionsList}" var="exclusion" varStatus="status">
													<c:choose>
													<c:when test="${exclusion.title != null && exclusion.title != ''}">
													<div class="pnl1">
													<h4 style="color: #c20a2f; font-weight: 700;font-size:16px;" >${exclusion.title}</h4>
													</div>
													</c:when>
													<c:otherwise>
													<br/>
													</c:otherwise>
													</c:choose>
													<c:choose>
													<c:when test="${exclusion.description != null && exclusion.description != ''}">
													<div class="item-list mb-2 mt-2">
														<span><img class="clippy" src="admin/img/svg/close.svg" width="10" alt="Copy to clipboard" style="margin-bottom: 3px;"><span>  ${exclusion.description}</span></span>
														<div class="pull-right">
														<span data-toggle="tooltip" data-placement="left" title="Add Exclusion">
														<a class="btn btn-xs btn-danger" data-exclusion-id="${exclusion.id}" onclick="javascritp:delteTripExclusion(this)">
														<img class="clippy" src="admin/img/svg/rubbish-bin.svg" width="15" alt="Delete"></a>
														</span>
														</div>
														
													</div>
													</c:when>
													<c:otherwise>
													
													</c:otherwise>
													</c:choose>
													
												</c:forEach>
											</div>
											</div>
										</div>
									</div>
									<!-- trip inclusion add -->
								<div class="modal fade" id="add_trip_exclusion" role="dialog">
											<div class="modal-dialog">
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title text-center">Add Trip Exclusion</h4>
														<button type="button" class="close slds-modal__close" data-dismiss="modal">
															<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
														</button>
													</div>
													<form id="saveTripExclusionForm" method="post" class="bv-form">
														<input type="hidden" id="quotationId" name="quotationId" value="${tripQuotation.id}" />
														<div class="modal-body">
															<div class="row">
																<div class="col-md-12">
																	<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="title">Description</label>
																	<div class="controls">
																		<div class="form-group">
																			<input type="text" name="title" id="title" class="form-control" />
																		</div>
																	</div>
																</div>
																<div class="col-md-12">
																	<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="description">Description</label>
																	<div class="controls">
																		<div class="form-group">
																			<textarea rows="3"  name="description" id="description" class="form-control" ></textarea>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
															<button type="submit" class="btn btn-primary" id="save-trip-exclusion-btn">Save</button>
														</div>
													</form>
												</div>
											</div>
									</div>
								</div>
							</c:if>
							</div>
<!--Cancellation policy  -->
							<div class="row">
							<p class="pt-0 pb-2 mb-2 mt-2">
									<span class="trip-font-icon"><i class="fa fa-file-text-o"></i></span> <span> <b class="blue-grey-text">Cancellation & Refund Policy</b></span>
							</p> 
							<c:forEach items="${tripQuotation.tripQuotationCancellationPolicyList}" var="cancellationObj" varStatus="status">
									<ul class="list-group mb-2">
									  <li class="list-group-item" style="    padding: 4px 25px;">${cancellationObj.remarks}</li>
									</ul>
							</c:forEach>
							</div>
							<div class="row">
							<p class="pt-0 pb-2 mb-2 mt-2">
									<span class="trip-font-icon"><img class="clippy" src="admin/img/svg/policy.svg" width="14" alt="policy-o"></span> <span> <b class="blue-grey-text">Payment Policy</b></span>
							</p> 
							<c:forEach items="${tripQuotation.tripQuotationPaymentPolicyList}" var="paymentPolicyObj" varStatus="status">
									<ul class="list-group mb-2">
									  <li class="list-group-item" style="    padding: 4px 25px;">${paymentPolicyObj.remarks}</li>
									</ul>
							</c:forEach>
							</div>
							<div class="row">
							<div class="pt-0 pb-2 mb-2 mt-2">
									<span class="trip-font-icon"><i class="fa fa-file-image-o"></i></span> <span> <b class="blue-grey-text">Trip Images</b></span>
									<div class="pull-right">
									<form id="imageUploadForm" method="post" enctype="multipart/form-data">
									<label class="btn-bs-file btn btn-xs btn-danger pull-right mr-1" data-toggle="tooltip" data-placement="left" title="Upload Trip Images">
									<img class="clippy" src="admin/img/svg/image-bw.svg" width="18"> Browse
					                	<input type="file" name="uploadTripFile" onchange="tripQuotationUploadFn(${tripQuotation.id});" />
				            		</label>
				            		</form>
				            		</div>
							</div>
							<c:choose>
							<c:when test="${tripQuotation.tripQuotationImageList != null && tripQuotation.tripQuotationImageList.size() > 0}">
							<c:forEach items="${tripQuotation.tripQuotationImageList}" var="imageObj" varStatus="status">
									  <div class="col-lg-1 col-md-1 col-xs-4 col-sm-4">
										<a class="thumbnail" href="#" data-image-id="${imageObj.id}" style="line-height: 0;" data-toggle="modal" data-title="${imageObj.imageURL}"
											data-image="get_trip_image_by_path?fileName=${imageObj.imageURL}"
											data-target="#image-gallery"><img src="get_trip_image_by_path?fileName=${imageObj.imageURL}" alt="Path Not Available"><br/>
										</a>
									  </div>
							<!--  -->
							<div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						    <div class="modal-dialog modal-lg">
						        <div class="modal-content">
						            <div class="image-preview-modal-header">
						                <h5 class="modal-title text-center" id="image-gallery-title"></h5>
										<button type="button" class="close slds-modal__close" data-dismiss="modal" style="margin-top: 38px;margin-right: 12px;">
											<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="13" alt="Copy to clipboard">
										</button>
						                
						            </div>
						            <div class="modal-body">
						                <img id="image-gallery-image" class="img-responsive" src="">
						            </div>
						            <div class="image-preview-modal-footer">
						                <div class="col-md-1">
						                    <button type="button" class="btn btn-sm btn-primary" id="show-previous-image">Previous</button>
						                </div>
						                <div class="col-md-10 text-justify" id="image-gallery-caption"> </div>
						                <div class="col-md-1">
						                    <button type="button" id="show-next-image" class="btn btn-sm btn-primary">Next</button>
						                </div>
						            </div>
						        </div>
						    </div>
							</div>
							
							</c:forEach>
							</c:when>
							<c:otherwise>
							<ul class="list-group mb-2">
									  <li class="list-group-item" style="padding: 4px 25px;">No Image Found</li>
									</ul>
							</c:otherwise>
							</c:choose> 
							</div>
								</section>
	               				 </div>
	            					</div>
	       								 </div>
	       								 <!-- trip quotation models -->
	       								 <!-- trip quotation info edit-->
										<div class="modal fade" id="edit-trip-quotation_${rowstatus.count}" role="dialog">
														<div class="modal-dialog">
															<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<h4 class="modal-title text-center">Edit Trip Quotation</h4>
																	<button type="button" class="close slds-modal__close" data-dismiss="modal">
																		<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
																	</button>
																</div>
																<form id="updateTripQuotationInfoForm" method="post" class="bv-form">
																	<input type="hidden" id="info-id" name="id" value="${tripQuotation.id}" />
																	<div class="modal-body">
																		<div class="row">
																			<div class="col-md-12">
																				<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Day Info</label>
																				<div class="controls">
																					<div class="form-group" >
																						<input type="text" name="title" id="title" class="form-control" value="${tripQuotation.title}">
																					</div>
																				</div>
																			</div>
																			<div class="col-md-12">
																				<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Destination From</label>
																				<div class="controls">
																					<div class="form-group">
																						<input type="text" class="form-control" name="destinationFrom" id="" autocomplete="off" value="${tripQuotation.destinationFrom}">
																					</div>
																				</div>
																			</div>
																			<div class="col-md-12">
																				<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Destination To</label>
																				<div class="controls">
																					<div class="form-group">
																						<input type="text" class="form-control" name="destinationTo" autocomplete="off" id="destinationTo" value="${tripQuotation.destinationTo}">
																					</div>
																				</div>
																			</div>
																			<div class="col-md-12">
																				<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Departure Date :</label>
																				<div class="controls">
																					<div class="form-group">
																					<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
																							<input type="text" class="form-control tripdatetimepicker"  name="departureDate" id="departureDate" autocomplete="off" value="${spysr:formatDate(tripQuotation.departureDate,'MMM/d/yyyy hh:mm a', 'MM/dd/yyyy hh:mm a')}" />
																					</div></div>
																				</div>
																			</div>
																			<div class="col-md-12">
																				<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Check In Date :</label>
																				<div class="controls">
																					<div class="form-group">
																					<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
																							<input type="text" class="form-control tripdatetimepicker"  name="startDate" id="startDate" autocomplete="off" value="${spysr:formatDate(tripQuotation.startDate,'MMM/d/yyyy hh:mm a', 'MM/dd/yyyy hh:mm a')}">
																					</div></div>
																				</div>
																			</div>
																			<div class="col-md-12">
																				<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Check Out Date </label>
																				<div class="controls">
																					<div class="form-group">
																					<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
																							<input type="text" class="form-control tripdatetimepicker"  name="endDate" id="endDate" autocomplete="off" value="${spysr:formatDate(tripQuotation.endDate,'MMM/d/yyyy hh:mm a', 'MM/dd/yyyy hh:mm a')}">
																					</div></div>
																				</div>
																			</div>
																			<div class="col-md-12">
																				<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Arrival Date :</label>
																				<div class="controls">
																					<div class="form-group">
																					<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
																							<input type="text" class="form-control tripdatetimepicker"  name="arrivalDate" id="arrivalDate" value="${spysr:formatDate(tripQuotation.arrivalDate,'MMM/d/yyyy hh:mm a', 'MM/dd/yyyy hh:mm a')}">
																					</div></div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="modal-footer">
																		<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
																		<button type="submit" class="btn btn-primary" id="update-trip-quotation-info-btn">Save</button>
																	</div>
																</form>
															</div>
										
														</div>
													</div>
										<!-- trip quotation rate info edit -->
										<div class="modal fade" id="edit-trip-quotation-rate_${rowstatus.count}" role="dialog">
										<div class="modal-dialog">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title text-center">Edit Trip Quotation Rate</h4>
													<button type="button" class="close slds-modal__close" data-dismiss="modal">
														<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
													</button>
												</div>
												<form id="updateTripQuotationRatesForm" method="post" class="bv-form">
													<input type="hidden" id="rate-id" name="id" value="${tripQuotation.id}" />
													<div class="modal-body">
														<div class="row">
															<div class="col-md-12">
																<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Supplier Price</label>
																<div class="controls">
																	<div class="form-group" >
																		<input type="text" class="form-control" name="supplierPrice" id="supplier-price" autocomplete="off" value="${tripQuotation.supplierPrice}">
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Markup Price</label>
																<div class="controls">
																	<div class="form-group">
																		<input type="text" class="form-control" name="markup" id="markup_price" autocomplete="off" value="${tripQuotation.markup}">
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">GST (%)</label>
																<div class="controls">
																	<div class="form-group">
																		<input type="text" class="form-control" name="gst" id="gst-percentage" autocomplete="off" value="${tripQuotation.gst}">
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Base Price</label>
																<div class="controls">
																	<div class="form-group">
																	<input type="text" class="form-control" name="basePrice" id="base-price" autocomplete="off" readonly="readonly" value="${tripQuotation.basePrice}">
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Processing Fee (%)</label>
																<div class="controls">
																	<div class="form-group">
																	<input type="text" class="form-control" name="processingFee" id="processing-fee" autocomplete="off" value="${tripQuotation.processingFee}">
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Total Amount</label>
																<div class="controls">
																	<div class="form-group">
																	<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-inr" aria-hidden="true"></i></span>
																			<input type="text" class="form-control" name="totalAmount" id="total-amount" autocomplete="off" readonly="readonly" value="${tripQuotation.supplierPrice}">
																	</div></div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
															<button type="submit" class="btn btn-primary" id="save-trip-quotation-rate-btn">Save</button>
														</div>
													</form>
												</div>
										
											</div>
										</div>
									</c:forEach>
									</c:when>
									<c:otherwise>
									<span class="text-danger">There is no trip found, Add please</span>
									</c:otherwise>
									</c:choose>
									
					                </div>
<!-- //END ######################################################################  Trip Quotation ###################################################################################-->
					            <div class="tab-pane" id="documents" role="tabpanel">
									<div class="card">
										  <div class="card-header">Uploads Docs</div>
										  <div class="card-body">
										<form method="post" class="form-horizontal" name="uploadMyLeadFollowUpDocumentForm" id="uploadMyLeadFollowUpDocumentForm" enctype="multipart/form-data">
										  <div class="row">
											 <input type="hidden" name="travelMyLeadFollowUpId" id="my-lead-follow-up" value="${travelSalesMyLeadFollowUpData.id}">
												<div class="col-md-3">
												<label for="booking-date" class="control-label">Document Type</label>
													<div class="form-group mt-10">
															<select name="documentType" id="document-type" class="form-control input-sm">
																<option value="" selected="selected">Select Type</option>
																<option value="passport">Passport</option>
																<option value="flightTicket">Flight Ticket</option>
																<option value="drivingLicence">Driving Licence</option>
																<option value="adhar">Adhar Card</option>
																<option value="pan">PAN Card</option>
															</select>
													</div>
												</div>
												<div class="col-md-4">
												<label class=""><span class="text-primary">Document Title</span></label>
													<div class="form-group mt-10">
															<input type="text" class="form-control input-sm" name="title" placeholder="document title" required="required" />
													</div>
												</div>
												<div class="col-md-4 file-upload">
												<label class="">Upload Documen</label>
													<div class="form-group mt-10">
															<input type="file" id="filePath" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,image/*" class="form-control input-sm"
																		ng-file-select="onFileSelect($files)" name="documentPath" style="background-color: #CFD8DC;color: #E91E63;font-weight: 600;">
													</div>
												</div>
												<!-- <div class="col-sm-12">
																	<div id="fileinfo">
																		<div id="fileError"></div>
																	</div>
																</div> -->
												<div class="col-md-1 mt-2">
													<div class="pull-right">
													<label class="">&nbsp;</label>
													<div class="form-group">
															<button type="submit" id="save-follow-up-document" class="btn btn-sm btn-success">Save</button>
													</div>
													</div>
												</div>
								</div>
										</form>
								</div>  
								</div>
											<s:iterator value="travelSalesMyLeadFollowUpDocumentList" status="rowstatus">
											<ul class="media-list comment-list mt-10">
															<li class="media">
																<div class="media-body">
																	<div class="pull-right mt-10">
																	  <a class="btn btn-xs btn-danger" data-document-id="${id}" onclick="javascritp:delteTravelMyLeadDocument(this)">
																		<img class="clippy" src="admin/img/svg/rubbish-bin.svg" width="15" alt="Delete"></a>
																	</div>
																	<div class="row mt-2">
																	<div class="col-md-2">
																	<span><b>Title:</b> ${title}</span>
																	</div>
																	<div class="col-md-2">
																	<span><b>Document Type:</b> ${documentType}</span>
																	</div>
																	<div class="col-md-6">
																			<c:if test="${documentPath !=null}">
																				<div class="download">
																					<p>
																						<b>Download Image:</b>&nbsp;<a href="get_travel_my_lead_document?fileName=${documentPath}" class="btn btn-success btn-xs">${documentPath} </a>
																					</p>
																				</div>
																			</c:if>
																		</div>
																	</div>
																</div> 
															</li>
													</ul>
											</s:iterator>
					                </div>
					            <!-- follow up reminder tab panel -->
					             <div class="tab-pane" id="reminder" role="tabpanel">
									 <div class="btn-group pull-right">
					                 <a href="#" class="btn btn-sm btn-info" data-event="duplicate" data-toggle="modal" data-target="#set-reminder"><i class="fa fa-plus"></i> Reminder</a>
					                 </div>		<br/>	
					                 <br/>	
					                 <div class="row">
												<s:iterator value="travelSalesMyLeadFollowUpReminderList" status="rowCount">
													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="headingOne">
															<h4 class="panel-title">
																<a role="button" class="bugtracker-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_rem_${rowCount.count}" aria-expanded="true">
																	${rowCount.count} : <span><b>${reminderType} : </b> <small class="text-muted"> 
																		<fmt:formatDate value="${createdDate}" pattern="E, MMM dd, yyyy h:m a" /></small></span>
																</a>
															</h4>
														</div>
														<div id="collapse_rem_${rowCount.count}" class="panel-collapse collapse" role="tabpanel">
															<div class="panel-body">
																<div class="row mt-1 mb-1">
																	<div class="col-sm-4">
																		<div class="row">
																			<div class="col-xs-5"><b>Reminder Type</b></div>
																			<div class="col-xs-7"><span class="text-capitalize">${reminderType}</span></div>
																		</div>
																		<div class="row">
																			<div class="col-xs-5"><b>Title</b></div>
																			<div class="col-xs-7"><span class="text-capitalize">${title}</span></div>
																		</div>
																	</div>
																	<div class="col-sm-8">
																		<div class="row">
																			<div class="col-xs-5"><b>Created By</b></div>
																			<div class="col-xs-7">${createdByName}</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-5"><b>Date Time</b></div>
																			<div class="col-xs-7">${spysr:formatDate(reminderDate,'yyyy-MM-dd hh:mm:ss', 'MMM/dd/yyyy hh:mm a')}</div>
																		</div>
																	</div>
																	
																</div>
															</div>
														</div>
													</div>
												</s:iterator>
										</div>
					                </div>
					            
					              </div>
              						
				            	</div>
								</div>						
										<!--/follow up history template -->
										</c:when>
										<c:otherwise>
											<div class="col-md-12">
											<span class="text-warning">* Update Status of Lead to continue follow up with customer</span>
											</div>
										</c:otherwise>
									</c:choose>				
												</div>
										</div>
							</div>
						</div>
					
					
					
				</div>
				</div>
			</section>
<div id="models-windows">
<div class="modal fade" id="set-reminder" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-simple-header">
										<h4 class="modal-title text-center">Set Reminder</h4>
										<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
										</button>
									</div>
									<form id="saveFollowUpReminderForm" method="post" class="bv-form">
										<div class="modal-body">
										 <div class="row">
											<div class="col-md-12">
												<label class="form-control-label" for="title">Reminder Type</label>
												<div class="controls">
														<div class="form-group">
															<select name="reminderType" id="reminder-type" class="form-control">
																<option value="" >Select</option>
																<option value="SELF" >Self</option>
																<option value="CUSTOMER" >Customer</option>
															</select>
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<label class="form-control-label" for="title">Title</label>
												<div class="controls">
														<div class="form-group">
															<input type="text" name="title" id="title"  class="form-control" value=""/>
															<span class="help-msg"></span>
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<label class="form-control-label" for="title">Description</label>
												<div class="controls">
														<div class="form-group">
															<textarea  name="description" id="description"  class="form-control" rows="4" /></textarea>
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<label class="form-control-label" for="datetimepicker-inline">Set Date & Time</label>
												<div class="controls">
														<div class="form-group">
															<input type="text" name="reminderDateFlag" id="datetimepicker-inline"  class="form-control" value=""/>
														</div>
												</div>
											</div>
										</div>
										<div class="row">
										<div class="col-md-12">
											<label class="form-control-label" for="datetimepicker-inline">Reminder On Service</label>
										</div>
										</div>
										<div class="row">
										<div class="col-md-3">
														<div class="form-group">
														<div class="checkbox checkbox-success">
															<input name="onEmail" id="on-email" type="checkbox" value="true" checked="checked"> 
															<label for="on-email" class="text-danger"> On Email</label>
														</div>
													</div>
												</div>
												<div class="col-md-3">
														<div class="form-group">
														<div class="checkbox checkbox-success">
															<input name="onNotification" id="on-notify" type="checkbox" value="false"> 
															<label for="on-notify" class="text-danger"> On Notify</label>
														</div>
													</div>
												</div>
												<div class="col-md-3">
														<div class="form-group">
														<div class="checkbox checkbox-success">
															<input name="onSms" id="on-sms" type="checkbox" value="false"> 
															<label for="on-sms" class="text-danger"> On SMS</label>
														</div>
													</div>
												</div>
												<div class="col-md-3">
														<div class="form-group">
														<div class="checkbox checkbox-success">
															<input name="onphone" id="on-phone" type="checkbox" value="false"> 
															<label for="on-phone" class="text-danger"> On Phone</label>
														</div>
													</div>
												</div>
												
										</div>
									</div>
										<div class="modal-simple-footer">
											<input type="hidden" name="travelMyLeadFollowId" value="${travelSalesMyLeadFollowUpData.id}">
											<button type="button" class="btn btn-success" id="save_my_lead_follow_reminder_btn">Save</button>
										</div>
									</form>
								</div>
							</div>
						</div>
				<div class="modal fade" id="follow-up-email" role="dialog">
					<div class="modal-dialog modal-side modal-bottom-right">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title text-center">Email Send</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
								</button>
							</div>
							<form method="post" class="form-horizontal" name="myForm" id="follow-up-email-from" enctype="multipart/form-data">
					                <input type="hidden" name="leadFollowUpId" value="${travelSalesMyLeadFollowUpData.id}">
								<div class="modal-body">
					                 <div class="row">
					                 <div class="form-group">
								      <label class="control-label col-sm-2 text-left" for="from-email">From</label>
								      <div class="col-sm-10">
								        <input type="text" class="form-control" id="from-email" name="fromEmail" value="<s:property value="%{#session.User.email}"/>">
								      </div>
								    </div>
								    <div class="form-group">
								      <label class="control-label col-sm-2 text-left" for="to-email">To</label>
								      <div class="col-sm-10">          
								        <input type="text" name="toEmail" class="form-control" id="to-email">
								      </div>
								    </div>
								    <div class="form-group">
								      <label class="control-label col-sm-2 text-left" for="bcc-email">Bcc</label>
								      <div class="col-sm-10">          
								        <input type="text"  name="bccEmail" class="form-control" id="bcc-email">
								      </div>
								    </div>
								    <div class="form-group">
								      <label class="control-label col-sm-2 text-left" for="subject">Subject</label>
								      <div class="col-sm-10">          
								        <input type="text" name="emailSubject" class="form-control" id="subject" placeholder="Enter Subject..">
								      </div>
								    </div>
								    <div class="form-group">
								     <label class="control-label col-sm-2 text-left" for="emailBody">&nbsp;</label>
								      <div class="col-sm-10">          
								        <textarea name="emailBody" cols="" rows="8" class="form-control" id="emailBody" placeholder="Email Body"></textarea>
								      </div>
								    </div>
					                 </div>
							</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-success" id="sent-follow-up-email" data-form-name="follow-up-email-from">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
								</div>
							</form>
							
						</div>

					</div>
				</div>
				<div class="modal fade" id="follow-up-log-call" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Log a Call</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
								</button>
							</div>
							<form method="post" class="form-horizontal" name="myForm" id="lead-followup-call-form">
								<div class="modal-body">
					                <div class="row">
					                <div class="col-md-12">
										<label class="form-control-label" for="title"><span class="text-danger">*</span> Subject</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" name="subject" id="subject"  class="form-control input-sm" required="required"/></div></div>
										</div>
									</div>
					                <div class="col-md-12">
										<label class="form-control-label" for="comment">Comment</label>
										<div class="controls"><div class="form-group">
												<textarea name="comment" id="comment" class="form-control input-sm" rows="4"></textarea>
												</div>
										</div>
									</div>
					                </div>
							</div>
								<div class="modal-simple-footer">
								<input type="hidden" name="leadFollowUpId" value="${travelSalesMyLeadFollowUpData.id}">
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-success" id="save-follow-up-call" data-form-name="lead-followup-call-form">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
								</div>
							</form>
							
						</div>

					</div>
				</div>
				<!-- preview model-->
				<div class="modal fade modal-fullscreen force-fullscreen" id="trip-quotation-preview" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title text-center">Edit Trip Quotation Rate</h4>
							<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
							</button>
						</div>
						<form id="updateTripQuotationRatesForm" method="post" class="bv-form">
							<input type="hidden" id="rate-id" name="id" value="${tripQuotation.id}" />
							<div class="modal-body">
								<div class="row">
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Supplier Price</label>
										<div class="controls">
											<div class="form-group" >
												<input type="text" class="form-control" name="supplierPrice" id="supplier-price" autocomplete="off" value="${tripQuotation.supplierPrice}">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Markup Price</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" class="form-control" name="markup" id="markup_price" autocomplete="off" value="${tripQuotation.markup}">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">GST (%)</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" class="form-control" name="gst" id="gst-percentage" autocomplete="off" value="${tripQuotation.gst}">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Base Price</label>
										<div class="controls">
											<div class="form-group">
											<input type="text" class="form-control" name="basePrice" id="base-price" autocomplete="off" readonly="readonly" value="${tripQuotation.basePrice}">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Processing Fee (%)</label>
										<div class="controls">
											<div class="form-group">
											<input type="text" class="form-control" name="processingFee" id="processing-fee" autocomplete="off" value="${tripQuotation.processingFee}">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Total Amount</label>
										<div class="controls">
											<div class="form-group">
											<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-inr" aria-hidden="true"></i></span>
													<input type="text" class="form-control" name="totalAmount" id="total-amount" autocomplete="off" readonly="readonly" value="${tripQuotation.supplierPrice}">
											</div></div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-primary" id="save-trip-quotation-rate-btn">Save</button>
								</div>
							</form>
						</div>
				
					</div>
</div>
<!-- travel sales lead history email contant -->
<div id="travel-lead-popover-content" class="hidden">
       <div class="row">
       <table class="table table-bordered table-striped-column table-hover mb-0">
                       <thead>
                       <tr class="small-trip-table">
			<th>SNo</th>
			<th>Mail Status</th>
			<th>Send At</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${emailCountStatusList}" var="emailData" varStatus="estatus">
		<tr class="small-trip-table">
		<td>${estatus.count}</td>
		<td>
		<c:choose>
		<c:when test="${emailData.mailStatus == 1}">
		<span class="text-success">Success</span>
		</c:when>
		<c:when test="${emailData.mailStatus == 0}">
		<span class="text-warning">Pending</span>
		</c:when>
		<c:when test="${emailData.mailStatus == -1}">
		<span class="text-danger">Failed</span>
		</c:when>
		</c:choose>
		</td>
		<td>
		${spysr:formatDate(emailData.createdDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy hh:mm a')}
		</td>
		</tr>
		</c:forEach>
	</tbody></table>
       </div>
   </div>
</div>
 <script type="text/javascript">
        $(function () {
            $('#datetimepicker-inline').datetimepicker({
                inline: false,
                sideBySide: true
            });
        });
    </script>
  <script>
  /* ------------------------------------------------------------------------------------------*/
   $(function () {
        $('#start-date-flag').datetimepicker({
        	format: 'MM/DD/YYYY hh:mm A',
        	
        }).on('dp.change', function() {
        	 $(this).data('DateTimePicker').hide();
        });
 
        $('#end-date-flag').datetimepicker({
        	format: 'MM/DD/YYYY hh:mm A',
        }).on('dp.change', function() {
        	 $(this).data('DateTimePicker').hide();
        });
        
         $("#start-date-flag").on("dp.change", function (e) {
            $('#end-date-flag').data("DateTimePicker").minDate(e.date);
        });
    });
   $(function () {
        $('#departure-date').datetimepicker({
        	format: 'MM/DD/YYYY hh:mm A',
        	
        }).on('dp.change', function() {
       	 $(this).data('DateTimePicker').hide();
       });
 
        $('#arrival-date').datetimepicker({
        	format: 'MM/DD/YYYY hh:mm A',
        }).on('dp.change', function() {
       	 $(this).data('DateTimePicker').hide();
       });
        
         $("#departure-date").on("dp.change", function (e) {
            $('#arrival-date').data("DateTimePicker").minDate(e.date);
        });
    });
  
  //filter 
   $(document).ready(function(){
       $(".collapse.in").each(function(){
       	$(this).siblings(".panel-heading").find(".fa").addClass("fa-minus").removeClass("fa-plus");
       });
       
       // Toggle plus minus icon on show hide of collapse element
       $(".collapse").on('show.bs.collapse', function(){
       	$(this).parent().find(".fa").removeClass("fa-plus").addClass("fa-minus");
       }).on('hide.bs.collapse', function(){
       	$(this).parent().find(".fa").removeClass("fa-minus").addClass("fa-plus");
       });
   });
   //
   $("[data-toggle=popover]").popover({
	    html: true, 
		content: function() 
		{
	         return $('#popover-content').html();
	    },
	}).blur(function() {
	    $(this).popover('hide');
	});
   //
   $("[data-toggle=lead-popover]").popover({
	    html: true, 
		content: function() 
		{
	         return $('#travel-lead-popover-content').html();
	    }
	}).blur(function() {
	    $(this).popover('hide');
	});
  /* ------------------------------------------------------------------------------------------*/
  $('#lead_source').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    if(valueSelected == 'Employee Referral'){
    	$("#employee-name").show(500);
    }
    else
    {
    	$("#employee-name").hide(500);
    }
    
});
  /* ------------------------------------------------------------------------------------------*/
  $(document).ready(function() {
		$("#updateTravelSalesMyLeadForm").submit(function(e) {
			 notifySavingMsg();
			e.preventDefault();
			$.ajax({
				url : "updateTravelSalesMyLead",
				type : "POST",
				dataType : 'json',
				data : $("#updateTravelSalesMyLeadForm").serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
	  					$.notify({icon: 'fa fa-check',message: jsonData.message.message },{type: 'success'});
	  					}
	  				else if(jsonData.message.status == 'error'){
	  					$.notify({icon: 'fa fa-check',message: jsonData.message.message },{type: 'success'});
	  					}
				},
				error : function(request, status, error) {
					showModalPopUp("Travel Sales Lead   can not be updated.", "e");
				}
			});
		});

		$("#updateTravelMyLeadFollowUpForm").submit(function(e) {
			e.preventDefault();
			notifySuccess();
			var leadId = $("#travel-lead-id").val();
			$.ajax({
				url : "updateTravelSalesMyLeadFollowUp",
				type : "POST",
				dataType : 'json',
				data : $("#updateTravelMyLeadFollowUpForm").serialize(),
				success : function(jsonData) { 
					if(jsonData.message.status == 'success'){
						alertify.success(jsonData.message.message);
						<%-- var url = '<%=request.getContextPath()%>/editTravelSalesLead';
						window.location.replace(url+'?id='+jsonData.json.id2);	 --%>
						setTimeout(location.reload.bind(location), 1000);
					}
	  				else if(jsonData.message.status == 'error'){
	  					alertify.error(jsonData.message.message);}
					
				},
				error : function(request, status, error) {
					showModalPopUp("Travel My Lead  can not be updated.", "e");
				}
			});
		});
		$("#save-follow-up-comment").click(function(e) {
			e.preventDefault();
			var formDeleteName = $(this).attr("data-form-name");
			$.ajax({
				url : "saveTravelSalesMyLeadFollowUpComment",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						alertify.success(jsonData.message.message);;}
	  				else if(jsonData.message.status == 'error'){
	  					alertify.error(jsonData.message.message);}
					setTimeout(location.reload.bind(location), 1000);
				},
				error : function(request, status, error) {
					alertify.error("follow up comment can't be updated,please try again");
				}
			});
		});
		$("#save-follow-up-call").click(function(e) {
			e.preventDefault();
			var formDeleteName = $(this).attr("data-form-name");
			$.ajax({
				url : "saveTravelSalesMyLeadFollowUpCall",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						alertify.success(jsonData.message.message);}
	  				else if(jsonData.message.status == 'error'){
	  					alertify.error(jsonData.message.message);}
					setTimeout(location.reload.bind(location), 1000);
				},
				error : function(request, status, error) {
					alertify.error("follow up call can't be updated,please try again");
				}
			});
		});
		$(".update-followup-comment-btn").click(function(e) {
			e.preventDefault();
			var formDeleteName = $(this).attr("data-form-name");
			var modalId = $(this).attr("data-modal-id");
			$.ajax({
				url : "updateTravelSalesMyLeadFollowUpComment",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						$("#" + modalId).hide();
						alertify.success(jsonData.message.message);}
	  				else if(jsonData.message.status == 'error'){
	  					$("#" + modalId).hide();
	  					alertify.error(jsonData.message.message);}
					setTimeout(location.reload.bind(location), 1000);
				},
				error : function(request, status, error) {
					alertify.error("follow up call can't be updated,please try again");
				}
			});
		});
		$(".delete-follow-up-comment").click(function(e) {
				// confirm dialog
				var commentId = $(this).data("comment-id");
				 alertify.confirm("Are you sure delete this comment", function () {
						$.ajax({
							url : "deleteTravelSalesMyLeadFollowUpComment?id="+commentId,
							type : "GET",
							dataType: 'json',
							success : function(jsonData) {
								if(jsonData.message.status == 'success'){
									alertify.success(jsonData.message.message);}
				  				else if(jsonData.message.status == 'error'){
				  					alertify.error(jsonData.message.message);}
								setTimeout(location.reload.bind(location), 1000);
							},
							error: function (request, status, error) {
								alertify.error("Comment  can not be deleted.");
							}
						});
				 }, function() {
					 alertify.error("You've clicked Cancel");
				 });
		});
		$(".update-followup-call-btn").click(function(e) {
			e.preventDefault();
			var formDeleteName = $(this).attr("data-form-name");
			var modalId = $(this).attr("data-modal-id");
			$.ajax({
				url : "updateTravelSalesMyLeadFollowUpCall",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						$("#" + modalId).hide();
						alertify.success(jsonData.message.message);}
	  				else if(jsonData.message.status == 'error'){
	  					$("#" + modalId).hide();
	  					alertify.error(jsonData.message.message);}
					setTimeout(location.reload.bind(location), 1000);
				},
				error : function(request, status, error) {
					alertify.error("follow up call can't be updated,please try again");
				}
			});
		});
		$(".delete-follow-up-call").click(function(e) {
				// confirm dialog
				var callId = $(this).data("call-id");
				 alertify.confirm("Are you sure delete this comment", function () {
						$.ajax({
							url : "deleteTravelSalesMyLeadFollowUpCall?id="+callId,
							type : "GET",
							dataType: 'json',
							success : function(jsonData) {
								if(jsonData.message.status == 'success'){
									alertify.success(jsonData.message.message);}
				  				else if(jsonData.message.status == 'error'){
				  					alertify.error(jsonData.message.message);}
								setTimeout(location.reload.bind(location), 1000);
							},
							error: function (request, status, error) {
								alertify.error("Comment  can not be deleted.");
							}
						});
				 }, function() {
					 alertify.error("You've clicked Cancel");
				 });
		});
		
		$("#sent-follow-up-email").click(function(e) {
			e.preventDefault();
			var formDeleteName = $(this).attr("data-form-name");
			$.ajax({
				url : "saveTravelSalesMyLeadFollowUpEmail",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						alertify.success(jsonData.message.message);}
	  				else if(jsonData.message.status == 'error'){
	  					alertify.error(jsonData.message.message);}
					setTimeout(location.reload.bind(location), 1000);
				},
				error : function(request, status, error) {
					alertify.error("follow up email can't be updated,please try again");
				}
			});
		});
		
		/* ----------------------*/
	});
  
  </script>
  <script>
  $('#send_all_trip_mail').on("click", function(event){
		// confirm dialog
		 var allVals = [];
		 $(".check_row:checked").each(function() {  
			 allVals.push($(this).attr('data-id'));
		 });  
			 var check = confirm("Are you sure you want to send these quotation ?");
			 if(check == true)
			 {
				 var join_selected_values = []; 
				 join_selected_values = allVals.join(",");
				 $.ajax({
							url : "sendEmailTripQuotation?tripQuoteIds="+join_selected_values,
							type : "POST",
							dataType: 'html',
							success : function(data) {
								alertify.success("You've clicked OK Your email have successfully send");
								setTimeout("window.location.reload();", 1000);
							},
							error: function (request, status, error) {
								showModalPopUp("Email can not be sended.","e");
							}
					}); 
			 }
			 else
			 {
				 alertify.error("You've clicked Cancel");
				 return false;	 
			 }
	});
  </script>
  <script type="text/javascript" src="admin/js/admin/email-send.js"></script>
  <script>
$(function () {
    $('.tripdatetimepicker').datetimepicker({
    	format: 'MM/DD/YYYY hh:mm A',
    	sideBySide : true,
    	
    }).on('dp.hide', function() {
        $(this).blur();
    });
 });
 
 /*-----------------------------*/
 $(document).ready(function(){
		$('#supplier-price, #markup_price, #gst-percentage , #processing-fee').keyup(function() {
			var suuplierPriceInput= $("#supplier-price").val();
			var markUpInput = $("#markup_price").val();
			var gstInput = $("#gst-percentage").val();
			var processingFeeInput = $("#processing-fee").val();
			
			var supplierPrice = parseFloat(suuplierPriceInput!=undefined && suuplierPriceInput!="" ?suuplierPriceInput:0);
			var markUp = parseFloat(markUpInput!=undefined && markUpInput!="" ?markUpInput:0);
			var gst = parseFloat(gstInput!=undefined && gstInput!="" ?gstInput:0);
			var processingFee = parseFloat(processingFeeInput!=undefined && processingFeeInput!="" ?processingFeeInput:0);
		
			
			var baseAmount = 0;
			var totalAmount = 0;
			console.log(supplierPrice);
			var markUpAmount=(supplierPrice+markUp);
			baseAmount = parseFloat((markUpAmount*gst)/100)+markUpAmount;
			totalAmount = parseFloat((baseAmount*processingFee)/100)+baseAmount;
			// display all values
			 $("#base-price").val(baseAmount);
			 $("#total-amount").val(totalAmount);
	});
});
 
 /*--------------------------------------------------------*/
  	// trip quotation rates update ajax
 	$("#updateTripQuotationRatesForm").submit(function(e) {
			e.preventDefault();
			notifySuccess();
			$.ajax({
				url : "updateTripQuotationRate",
				type : "POST",
				dataType : 'json',
				data : $("#updateTripQuotationRatesForm").serialize(),
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message);}
					
				},
				error : function(request, status, error) {
					alertify.error("Trip quotation can not be updated. ");
					
				}
			});
		});
 	// trip quotation info update ajax
 	$("#updateTripQuotationInfoForm").submit(function(e) {
			e.preventDefault();
			notifySuccess();
			$.ajax({
				url : "updateTripQuotationDetail",
				type : "POST",
				dataType : 'json',
				data : $("#updateTripQuotationInfoForm").serialize(),
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message);}
					
				},
				error : function(request, status, error) {
					alertify.error("Trip quotation can not be updated ");
				}
			});
		});
 	// trip quotation itineary edit ajax
 	$(".edit-trip-quotation-itinerary-btn").click(function(e) {
			e.preventDefault();
			notifySuccess();
			var formDeleteName = $(this).attr("data-form-name");
			var modalId = $(this).attr("data-modal-id");
			$.ajax({
				url : "updateQuotationItinerary",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message);}
				},
				error : function(request, status, error) {
					alertify.error("Trip quotation can not be updated ");
				}
			});
		});
 	
 	// trip quotation inclusion save new ajax
 	$("#saveTripInclusionForm").submit(function(e) {
			e.preventDefault();
			notifySuccess();
			$.ajax({
				url : "saveTripQuotationInclusion",
				type : "POST",
				dataType : 'json',
				data : $("#saveTripInclusionForm").serialize(),
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message);}
					
				},
				error : function(request, status, error) {
					alertify.error("Trip Inclusion can not be saved ");
				}
			});
		});
 	// trip quotation exclusion save new ajax
 	$("#saveTripExclusionForm").submit(function(e) {
			e.preventDefault();
			notifySuccess();
			$.ajax({
				url : "saveTripQuotationExclusion",
				type : "POST",
				dataType : 'json',
				data : $("#saveTripExclusionForm").serialize(),
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message);}
					
				},
				error : function(request, status, error) {
					alertify.error("Trip Exclusion can not be saved ");
				}
			});
		});
 	
 	/*-------------------------------------*/
 	  $('#save_my_lead_follow_reminder_btn').on("click", function(event){
			 $.ajax({
						url : "set_travel_my_lead_reminder",
						type : "POST",
						dataType: 'json',
						data : $("#saveFollowUpReminderForm").serialize(),
						success : function(jsonData) {
							if(jsonData.message.status == 'success'){
								$.notify({icon: 'fa fa-check',message: jsonData.message.message},{type: 'success'});
								window.location.reload(1000);
							}
			  				else if(jsonData.message.status == 'error'){
			  					$(".email-msg-alert").html('<div class="msg-alert msg-danger msg-danger-text"> <i class="fa fa-times"></i><span class="error-msg">&nbsp;'+jsonData.message.message+'</span></div>');
			  				}
						},
						error: function (request, status, error) {
							alertify.error("Something problem saving data,Try again");
						}
				}); 
});
</script>
<script>
function delteTripItinerary(sel) {
	// confirm dialog
	 alertify.confirm("Are you sure delete this itinerary", function () {
		 var ItinId = $(sel).data("itinerary-id");
			$.ajax({
				url : "deleteTripQuotationItinerary?id="+ItinId,							
				type : "GET",
				dataType: 'json',
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);	
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message, function(ev) {
	  					    alertify.error("Ok Try again");
	  					});
	  				}
				},
				error: function (request, status, error) {
					showModalPopUp("Somthing Wrong, please try again.","e");
				}
			});
	 }, function() {
		 alertify.error("You've clicked Cancel");
	 });
	
}
function delteTripInclusion(sel) {
	// confirm dialog
	 alertify.confirm("Are you sure delete this inclusion", function () {
		 var inclusionId = $(sel).data("inclusion-id");
			$.ajax({
				url : "deleteTripQuotationInclusion?id="+inclusionId,							
				type : "GET",
				dataType: 'json',
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);	
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message, function(ev) {
	  					    alertify.error("Ok Try again");
	  					});
	  				}
				},
				error: function (request, status, error) {
					showModalPopUp("Somthing Wrong, please try again.","e");
				}
			});
	 }, function() {
		 alertify.error("You've clicked Cancel");
	 });
	
}
function delteTripExclusion(sel) {
	// confirm dialog
	 alertify.confirm("Are you sure delete this exclusion", function () {
		 var excuId = $(sel).data("exclusion-id");
			$.ajax({
				url : "deleteTripQuotationExclusion?id="+excuId,							
				type : "GET",
				dataType: 'json',
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);	
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message, function(ev) {
	  					    alertify.error("Ok Try again");
	  					});
	  				}
				},
				error: function (request, status, error) {
					showModalPopUp("Somthing Wrong, please try again.","e");
				}
			});
	 }, function() {
		 alertify.error("You've clicked Cancel");
	 });
	
}

function delteTravelMyLeadDocument(sel) {
	// confirm dialog
	 alertify.confirm("Are you sure delete this document", function () {
		 var excuId = $(sel).data("document-id");
			$.ajax({
				url : "deleteTravelMyLeadDocument?id="+excuId,							
				type : "GET",
				dataType: 'json',
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);	
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message, function(ev) {
	  					    alertify.error("Ok Try again");
	  					});
	  				}
				},
				error: function (request, status, error) {
					showModalPopUp("Somthing Wrong, please try again.","e");
				}
			});
	 }, function() {
		 alertify.error("You've clicked Cancel");
	 });
	
}
/*-------------------------------------------*/
function tripQuotationUploadFn(quotationId){
		 var formData = new FormData($("#imageUploadForm")[0]);
		 $.notify({title: '<strong>Hy Wait!</strong>',message: 'Image is uploading...'},{type: 'info'});
		    $.ajax({
		        url: "uploadTripImages?quotationId="+quotationId,
		        type: 'POST',
		        data: formData,
		        async: false,
		        dataType: 'json',
		        success: function (jsonData) 
		        {
		        	if(jsonData.json.status == 'success'){
						$.notify({icon: 'fa fa-check',message: jsonData.json.message},{type: 'success'});
						window.location.reload(1000);
					}
	  				else if(jsonData.message.status == 'error')
	  				{
	  					$.notify({icon: 'fa fa-times',message: jsonData.json.message},{type: 'danger'});
	  				}
		        	
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });	
	}
/*---------------------------------------------------------------------------------*/
$(document).ready(function(){

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current){
        $('#show-previous-image, #show-next-image').show();
        if(counter_max == counter_current){
            $('#show-next-image').hide();
        } else if (counter_current == 1){
            $('#show-previous-image').hide();
        }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr){
        var current_image,
            selector,
            counter = 0;
        $('#show-next-image, #show-previous-image').click(function(){
            if($(this).attr('id') == 'show-previous-image'){
                current_image--;
            } else {
                current_image++;
            }
            selector = $('[data-image-id="' + current_image + '"]');
            updateGallery(selector);
        });

        function updateGallery(selector) {
            var $sel = selector;
            current_image = $sel.data('image-id');
            $('#image-gallery-caption').text($sel.data('caption'));
            $('#image-gallery-title').text($sel.data('title'));
            $('#image-gallery-image').attr('src', $sel.data('image'));
            disableButtons(counter, $sel.data('image-id'));
        }

        if(setIDs == true){
            $('[data-image-id]').each(function(){
                counter++;
                $(this).attr('data-image-id',counter);
            });
        }
        $(setClickAttr).on('click',function(){
            updateGallery($(this));
        });
    }
}); 
</script>
<script>
$(document).ready(function() {
    $("#uploadMyLeadFollowUpDocumentForm").bootstrapValidator({
        feedbackIcons: {
            valid: "fa fa-check",
            invalid: "fa fa-remove",
            validating: "fa fa-refresh"
        },
        fields:{title:{message:"Title is not valid",validators:{notEmpty:{message:"Title is required field"},stringLength:{min:1,message:"Name must be more than 3 and less than 100 characters long"}}},
        	documentType:{message:"Document Type is not valid",validators:{notEmpty:{message:"Please select document type"}}},
        	documentPath: {
         	  validators: {
                notEmpty: {
                    message: 'Please select an image'
                },
               /*  file: {
                    extension: 'jpeg,jpg,png,pdf,doc,xlsx',
                    type: 'image/jpeg,image/png,application/pdf,application/msword,application/vnd.ms-excel',
                    message: 'The selected file is not valid'
                } */
            } 
        }
        	}
        
    }).on("error.form.bv", function(e) {}).on("success.form.bv", function(e) {
            e.preventDefault();
            var form = $('form[name="uploadMyLeadFollowUpDocumentForm"]')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            $.ajax({
                url: "uploadTravelMyLeadFollowUpDocument",
                type: "POST",
                dataType: "json",
                data: formData,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false,
                success: function(jsonData) {
                	
                	if(jsonData != null)
                	{
                		if(jsonData.json.status == 'success'){
    						$.notify({icon: 'fa fa-check-square',message: jsonData.json.message },{type: 'success'});
    						window.location.reload(1000);
    					}
    	  				else if(jsonData.json.status == 'error'){
    	  					$.notify({icon: 'fas fa-exclamation-triangle',message: jsonData.json.message },{type: 'danger'});
    	  					}
                	}
                	else
                	{
                		$.notify({icon: 'fas fa-exclamation-triangle',message: "Somthing Wrong Json Data is null" },{type: 'danger'});
                	}
                	
                    $(".save-follow-up-document").removeAttr("disabled"), console.debug("button disabled ");
                    var s = $(e.target);
                    s.data("bootstrapValidator");
                    s.bootstrapValidator("disableSubmitButtons", !1).bootstrapValidator("resetForm", !0)
                },
                error: function(e, a, s) {
                    showModalPopUp("Document can not be Saved.", "e")
                }
            })
    }).on("status.field.bv", function(e, a) {
        a.bv.getSubmitButton() && (console.debug("button disabled "), a.bv.disableSubmitButtons(!1))
    })
});
</script>
