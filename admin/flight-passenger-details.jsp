<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<link
	href="admin/css/jquery-ui.css"
	rel="stylesheet" type="text/css" />

<!-- Content Header (Page header) -->
<section class="wrapper container-fluid">

	<div class="row">
		<div class="col-md-12">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.flight_passenger_report_details" />
					</h5>
					<div class="set pull-right">

						<a class="btn btn-success btn-xs" href="flightReportList"><span
							class="pull-right"> <span
								class="glyphicon glyphicon-step-backward"></span> <s:text
									name="tgi.label.back" />
						</span></a>

					</div>
				</div>

				<!-- Main content -->

				<section class="content">
					<!-- Small boxes (Stat box) -->
					<%-- <div class="col-md-12">
			 <div class="set pull-left">
						
							  <a class="btn btn-success btn-xs" href="javascript:history.back();"><span
								class="pull-right">
									<span class="glyphicon glyphicon-step-backward"></span><s:text name="tgi.label.back" /></span></a>
						</div>
					</div> --%>

					<div class="row">
						<div class="col-md-5" style="font-size: 18px;">
							<b><s:text name="tgi.label.booking_summary" /></b>
						</div>
					</div>
					<br>

					<div class="table-responsive no-table-css clearfix">
						<table
							class="table table-bordered table-striped-column table-hover">
							<thead>
								<tr class="border-radius border-color">
									<th><s:text name="tgi.label.booking_date" /></th>
									<th><s:text name="tgi.label.pnr" /></th>
									<!-- <th>Currency</th> -->
									<!-- <th>DEP</th>
											<th>ARR</th> -->
									<th><s:text name="tgi.label.status" /></th>
									<th><s:text name="tgi.label.supplier" /></th>
									<th><s:text name="tgi.label.created_by" /></th>
									<th><s:text name="tgi.label.airline" /></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td data-title="Booked"><s:property
											value="CurrentReportdata.bookingDate" />
									<td data-title="PNR"><s:property
											value="CurrentReportdata.pnr" /></td>
									<td data-title="Status"><s:property
											value="CurrentReportdata.status" /></td>
									<td data-title="<s:text name="tgi.label.supplier" />"><s:property
												value="CurrentReportdata.apiProvider" /></td>		
									<td data-title="Agency"><s:property
											value="CurrentReportdata.createdBy" /></td>
									<td data-title="Airline"><s:property
											value="CurrentReportdata.airline" /></td>		

								</tr>
							</tbody>
						</table>
					</div>

					<div class="row">
						<div class="col-md-5" style="font-size: 18px;">
							<b><s:text name="tgi.label.flight_details" /></b>
						</div>
					</div>
					<br>

					<div class="table-responsive no-table-css clearfix">
						<table
							class="table table-bordered table-striped-column table-hover">
							<thead>
								<tr class="border-radius border-color">

									<th><s:text name="tgi.label.s.no" /></th>
									<th><s:text name="tgi.label.origin" /></th>
									<th><s:text name="tgi.label.destination" /></th>
									<th><s:text name="tgi.label.departure" /></th>
									<th><s:text name="tgi.label.arrival" /></th>
									<th><s:text name="tgi.label.airline" /></th>
									<th><s:text name="tgi.label.equipment" /></th>
								</tr>
							</thead>
							<tbody>
								<s:iterator value="tripDataList" status="data">
									<tr>
										<td data-title="<s:text name="tgi.label.s.no" />"><s:property
												value="%{#data.count}" /></td>
										<td data-title="Origin"><s:property value="origin" /></td>
										<td data-title="Destination"><s:property
												value="destination" /></td>
										<td data-title="Departure"><s:property
												value="departureDate" /></td>
										<td data-title="Arrival"><s:property value="arrivalDate" /></td>
										<td data-title="Airline"><s:property value="airline" /></td>
										<td data-title="Airline"><s:property value="airplaneType" /></td>
									</tr>
								</s:iterator>
							</tbody>
						</table>
					</div>

					<div class="row">
						<div class="col-md-5" style="font-size: 18px;">
							<b><s:text name="tgi.label.passenger_details" /></b>
						</div>
					</div>
					<br>

					<div class="table-responsive no-table-css clearfix">
						<table
							class="table table-bordered table-striped-column table-hover">
							<thead>
								<tr class="border-radius border-color">

									<th><s:text name="tgi.label.s.no" /></th>
									<th><s:text name="tgi.label.title" /></th>
									<th><s:text name="tgi.label.first_name" /></th>
									<th><s:text name="tgi.label.last_name" /></th>
									<!-- <th>Gender</th> -->
									<th><s:text name="tgi.label.date_of_birth" /></th>
									<th><s:text name="tgi.label.passportexp_date" /></th>
								</tr>
							</thead>
							<tbody>
								<s:iterator value="passList" status="data">
									<tr>
										<td data-title="<s:text name="tgi.label.s.no" />"><s:property
												value="%{#data.count}" /></td>
										<td data-title="Title"><s:property value="title" /></td>
										<td data-title="First Name"><s:property value="name" /></td>
										<td data-title="Last Name"><s:property value="surname" /></td>
										<%-- <td data-title="Gender"><s:property value="gender" /></td> --%>
										<td data-title="DOB"><s:property value="dateofBirth" /></td>
										<td data-title="<s:text name="tgi.label.passportexp_date" />"><s:property
												value="PassportExpDate" /></td>

									</tr>
								</s:iterator>
							</tbody>
						</table>
					</div>

					<div class="row">
						<div class="col-md-5" style="font-size: 20px;">
							<b><s:text name="Contact Info" /></b>
						</div>
					</div>
					<br>

					<div class="table-responsive no-table-css clearfix">
						<table
							class="table table-bordered table-striped-column table-hover">
							<thead>
								<tr class="border-radius border-color">
									<!-- 	<th>Passengers</th> -->
									<th><s:text name="tgi.label.title" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.surname" /></th>
									<%-- <th><s:text name="tgi.label.gender" /></th> --%>
									<th><s:text name="tgi.label.mobile" /></th>
									<th><s:text name="tgi.label.address" /></th>
									<th><s:text name="tgi.label.email" /></th>

								</tr>
							</thead>
							<s:iterator status="index">
								<tr>
									<%-- 	<th><s:property value="Passengers" /></th> --%>
									<td data-title="Title">${flightOrderRowData.flightCustomer.title}</td>
									<td data-title="<s:text name="tgi.label.name" />">${flightOrderRowData.flightCustomer.firstName}</td>
									<td data-title="<s:text name="tgi.label.surname" />">${flightOrderRowData.flightCustomer.lastName}</td>
									<%-- <td data-title="<s:text name="tgi.label.gender" />">${flightOrderRowData.flightCustomer.gender}</td> --%>
									<td data-title="<s:text name="tgi.label.mobile" />">${flightOrderRowData.flightCustomer.mobile}</td>
									<td data-title="<s:text name="tgi.label.address" />">${flightOrderRowData.flightCustomer.address}</td>
									<td data-title="Email">${flightOrderRowData.flightCustomer.email}</td>
								</tr>
							</s:iterator>
						</table>
					</div>
					<br>
					<c:choose>
						<c:when test="${flightOrderRowData.flightOrderInsurance != null}">
							<div class="row">
								<div class="col-md-5" style="font-size: 18px;">
									<b><s:text name="tgi.label.flight_insurance" /></b>
								</div>
							</div>
							<br>

							<div class="table-responsive no-table-css clearfix">
								<table
									class="table table-bordered table-striped-column table-hover">
									<thead>
										<tr class="border-radius border-color">
											<th><s:text name="tgi.label.insurance_id" /></th>
											<th><s:text name="tgi.label.name" /></th>
											<th><s:text name="tgi.label.price" /></th>
											<th><s:text name="tgi.label.description" /></th>
										</tr>
									</thead>
									<tbody>
										<%-- <s:iterator value="flightOrderRow.flightOrderInsurance"
										status="data"> --%>
										<tr>
											<td data-title="><s:text name="tgi.label.insurance_id" />">${flightOrderRowData.flightOrderInsurance.insuranceId}</td>
											<td data-title="<s:text name="tgi.label.name" />">${flightOrderRowData.flightOrderInsurance.name}</td>
											<td data-title="<s:text name="tgi.label.price" />">${flightOrderRowData.flightOrderInsurance.price}</td>
											<td data-title="<s:text name="tgi.label.description" />">${flightOrderRowData.flightOrderInsurance.description}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</c:when>
						<c:otherwise>
							<h4>
								<s:text name="tgi.label.flight_insurance" />
							</h4>
							<table class="table table-bordered in-table-border no-table-css">
								<tr class="border-radius border-color">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
								</tr>
								<tr>
									<td colspan="5"><s:text
											name="tgi.label.insurance_not_available" /></td>
								</tr>
							</table>
						</c:otherwise>
					</c:choose>


					<div class="row">
						<div class="col-md-5" style="font-size: 18px;">
							<b><span> <s:text name="tgi.label.account_details" /></span></b>
						</div>
					</div>
					<br>


					<div class="table-responsive no-table-css clearfix">
						<table
							class="table table-bordered table-striped-column table-hover">
							<thead>
								<tr class="border-radius border-color">
									<th><span></span>&nbsp;<s:text
													name="tgi.label.transaction_id" /></th>
									<th><s:text name="tgi.label.date" /></th>
									<th><s:text name="tgi.label.resmessage" /></th>
									<th><s:text name="tgi.label.payment_status" /></th>
									<th><s:text name="tgi.label.payment_method" /></th>
									<th><s:text name="tgi.label.paid_amount" /></th>
								</tr>
							</thead>
							<tbody>
								<s:if test="endUserTxHistory!=null ">
									<s:iterator value="endUserTxHistory">
										<tr>
											<td data-title="<s:text name="tgi.label.txid" />"><s:property
													value="transactionId" /></td>
											<td data-title="<s:text name="tgi.label.date" />"><s:property
													value="createdAt" /></td>
											<td data-title="<s:text name="tgi.label.resmessage" />"><s:property
													value="responseMessage" /></td>
											<td data-title="<s:text name="tgi.label.payment_method" />"><s:property
													value="paymentStatus" /></td>
											<td data-title="<s:text name="tgi.label.payment_method" />"><s:property
													value="paymentMethod" /></td>
											<td data-title="<s:text name="tgi.label.paid_amount" />"><s:property
													value="totalPaidAmount" /></td>
										</tr>
									</s:iterator>
								</s:if>
								<s:else>
									<tr>
										<td><s:text name="tgi.label.no_available_data" /></td>
									</tr>
								</s:else>
							</tbody>
						</table>
					</div>


					<div class="row">
						<div class="col-md-5" style="font-size: 18px;">
							<b><s:text name="tgi.label.order_summary" /></b>
						</div>
					</div>
					<br>


					<div class="table-responsive no-table-css clearfix">
						<table
							class="table table-bordered table-striped-column table-hover">
							<thead>
								<tr class="border-radius border-color">
									<th>&nbsp;<s:text name="tgi.label.total_passengers" /></th>
									<th><s:text name="tgi.label.tax" /></th>
									<th><s:text name="tgi.label.base_fare" /></th>
									<th><s:text name="tgi.label.processing.fees" /></th>
									<th><s:text name="tgi.label.insurance_fee" /></th>
									<th><s:text name="tgi.label.final_amount" /></th>
								</tr>
							</thead>

							<s:iterator value="passList">
								<tr>
									<td data-title="Total Passengers"><s:property
											value="passengers" /></td>
									<td data-title="<s:text name="tgi.label.tax" />"><s:property
													value="tax" /></td>		
									<td data-title="<s:text name="tgi.label.base_amount" />"><s:property
											value="price" /></td>
									<%-- <td data-title="<s:text name="tgi.label.tax" />"><s:property
												value="tax" /></td> --%>
									<td data-title="<s:text name="tgi.label.processing.fees" />"><s:property
											value="processingFee" /></td>
									<td data-title="<s:text name="tgi.label.insurance_fee" />">
										<c:choose>
											<c:when
												test="${flightOrderRowData.flightOrderInsurance.price != null}">
											${flightOrderRowData.flightOrderInsurance.price}
											</c:when>
											<c:otherwise>
												<s:text name="tgi.label.n_a" />
											</c:otherwise>
										</c:choose>
									</td>

									<td data-title="<s:text name="tgi.label.final_amount" />"><s:property
											value="finalPrice" /></td>
								</tr>
							</s:iterator>
						</table>
					</div>
					<br>



				</section>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<!-- /.content-wrapper -->
<script>
		$(document).ready(function() {
			$("#twodpd2").datepicker({
				dateFormat : "yy-mm-dd"
			});
			$("#twodpd1").datepicker({
				dateFormat : "yy-mm-dd"
			/*  changeMonth: true,
			 changeYear: true */
			});
		});
	</script>