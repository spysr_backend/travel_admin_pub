<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style>
.inner-addon .glyphicon {
    position: absolute;
    padding: 5px 5px 5px 5px;
    pointer-events: none;
    margin-top: 25px;
}
.right-addon .glyphicon {
    right: 20px;
}
</style>


<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"addCompany";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<script type="text/javascript">
$(document).ready(function()
		{
			/*
				assigning keyup event to password field
				so everytime user type code will execute
			*/

			$('#password').keyup(function()
			{
				$('#result').html(checkStrength($('#password').val()))
			})	
			
			/*
				checkStrength is function which will do the 
				main password strength checking for us
			*/
			
			function checkStrength(password)
			{
				//initial strength
				var strength = 0
				
				//if the password length is less than 6, return message.
				if (password.length < 6) { 
					$('#result').removeClass()
					$('#result').addClass('short')
					return 'Too short' 
				}
				
				//length is ok, lets continue.
				
				//if length is 8 characters or more, increase strength value
				if (password.length > 7) strength += 1
				
				//if password contains both lower and uppercase characters, increase strength value
				if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
				
				//if it has numbers and characters, increase strength value
				if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
				
				//if it has one special character, increase strength value
				if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
				
				//if it has two special characters, increase strength value
				if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
				
				//now we have calculated strength value, we can return messages
				
				//if value is less than 2
				if (strength < 2 )
				{
					$('#result').removeClass()
					$('#result').addClass('weak')
					return 'Weak'			
				}
				else if (strength == 2 )
				{
					$('#result').removeClass()
					$('#result').addClass('good')
					return 'Good'		
				}
				else
				{
					$('#result').removeClass()
					$('#result').addClass('strong')
					return 'Strong'
				}
			}
		});
</script>
<script type="text/javascript">
 

 function isNumberKey(evt,obj){
	    var charCode = (evt.which) ? evt.which : event.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57))   
	    
	        return false;
	   
	}
	function InvalidMsg(textbox) {
	    
	    if(textbox.validity.patternMismatch){
	       textbox.setCustomValidity('Mobile number not vaild.');
	   }    
	   else {
	       textbox.setCustomValidity('');
	   }
	   return true;
	}
	function InvalidEmailMsg(textbox) {
	    
	    if(textbox.validity.patternMismatch){
	       textbox.setCustomValidity('Email address not vaild.');
	   }    
	   else {
	       textbox.setCustomValidity('');
	   }
	   return true;
	}

	function onlyAlphabets(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                return true;
            else
                return false;
        }
        catch (err) {
            alert(err.Description);
        }}

 </script>

 
<script type="text/javascript">
	$(document).ready(
			function() {
 			var currency_list = [];
 			 $('#country').change(function(){
 				var country=$('#country').val();
 				 document.getElementById('Billingcountry').value =country;
				/*  $.ajax({
					//Action Name
					url :"currencyJson",
					dataType : "json",
					success : function(data, textStatus, jqXHR) {
						$.each(data.currencyMap, function( key, value ) {
							if(country!="0"){
							if(key==country){
								console.log( key + ": " + value );
								 document.getElementById('currency').value =value;
							
							 
							}
							}
							else{
								 document.getElementById('currency').value ="0";
								 document.getElementById('Billingcountry').value ="0";
							}
							
							});
 					 
					 console.log(data.currencyMap);  
						 
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log("-------Error-----status-----------"+textStatus);
						console.log("-------jqXHR--- -----------"+jqXHR);
						console.log("-----errorThrown-----------"+errorThrown);
					}
				});*/

			   });
 		        });
	 
 </script>

 

<script type="text/javascript">

        $(document).ready(function(){
 			 $('#bilAdress').click(function(){
 					if($(this).is(":checked")){
                	if ($('textarea#address') != undefined) {
                		   var message = $('textarea#address').val();
                		   document.getElementById('Billingaddress').value =message;
                		 }
 				  }

                else if($(this).is(":not(:checked)")){
                	 document.getElementById('Billingaddress').value ="";
                }

            });

        });

    </script>



<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"getApprovalCompaniesList";
	  
	$('#success').click(function() {
	 window.location.assign(finalUrl); 
		$('#success-alert').hide();
		
	});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 
 

            <section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                    	<div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left">Add Car Trip</h5>
                        	  <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-outline-danger" href="companyList"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;<s:text name="tgi.label.company_list" /></a>
									</div>
									</div>
                               </div> 
                            </div>
 				<div class=""> 
 <div class="col-md-12">
 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse01" aria-expanded="false" aria-controls="collapseOne">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Booking Details</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse01" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="">
                                        <div class="panel-body"> 			 
												
											 
										 
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carOrderRow.confirmationNumber"> Confirmation Number </label> <input type="text" autocomplete="off" name="carOrderRow.confirmationNumber" class="form-control input-sm " id="confirmationNumber" value="" required="" placeholder="Enter Confirmation Number ">
															
													</div>
												</div>
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carOrderRow.carCompanyName"> Car Type </label> <select class="form-control input-sm" required="required" id="carcompanyName" name="carOrderRow.carCompanyName">
															<option value="small">Small</option>
															<option value="medium">Medium</option>
														</select>
														
													</div>
												</div>
												 
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carOrderRow.location">City
														</label> <input type="text" autocomplete="off" name="carOrderRow.location" class="form-control input-sm " id="carlocation" value="" placeholder="Location " required="">
															
													</div>
												</div>
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carTravelRequestQuotation.pickUp">Pick
															Up </label> <input type="text" autocomplete="off" name="carTravelRequestQuotation.pickUp" class="form-control input-sm " required="required" placeholder="Enter Pickup Location">
															
													</div>
												</div>
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carTravelRequestQuotation.dropOff">Drop
															Off </label> <input type="text" autocomplete="off" name="carTravelRequestQuotation.dropOff" class="form-control input-sm " required="required" placeholder="Enter DtropOff Location">
															
													</div>
												</div>
											 
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carOrderRow.supplierName">Supplier
															Name</label> <select class="form-control input-sm" name="carOrderRow.supplierName" id="supplierName" required="">
															<option value="" selected="selected">select
																Supplier</option>
															
																<option value="Desia">Desia</option>
															
																<option value="TBO">TBO</option>
															
														</select>
														
													</div>
												</div>

												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carOrderRow.carBookingDate"> Booking Date </label> <input type="text" autocomplete="off" readonly="readonly" name="carOrderRow.carBookingDate" onchange="validateDateFormat(this)" class="form-control input-sm hasDatepicker" id="carBookingDate" value="" placeholder="Enter Booking Date" required="">
															
													</div>
												</div>

												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carOrderRow.travelDate">Travel
															Date </label> <input type="text" autocomplete="off" name="carOrderRow.travelDateTemp" readonly="readonly" onchange="validateDateFormat(this)" class="form-control input-sm hasDatepicker" id="traveldateforcar" value="" required="" placeholder="Enter Travel Date ">
															
													</div>
												</div> 
											</div>
                                    </div>
                                </div>
                                </div>
                                </div>
                                <div class="col-md-12">
 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse02" aria-expanded="false" aria-controls="collapseTwo">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Passenger Details</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="">
                                        <div class="panel-body">
											<div class="detailswithRM">  	 
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="busTravelRequest.title">Title</label> <select class="form-control input-sm" required="required" name="carOrderCustomerList[0].title">
													<option value="Mr" selected="selected">Mr</option>
													<option value="Mrs">Mrs</option>
													<option value="Miss">Miss</option>
													<option value="Ms">Ms</option>
													<option value="Master">Master</option>
														</select>
														
													</div>
												</div>
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carOrderCustomerList.firstName" class=" control-label">First Name</label> <input type="text" autocomplete="off" name="carOrderCustomerList[0].firstName" class="form-control input-sm firstName" id="firstName" value="" required="" placeholder="Enter firstName ">
															
													</div>
												</div>
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carOrderCustomerList.lastName" class=" control-label">Last Name </label> <input type="text" autocomplete="off" name="carOrderCustomerList[0].lastName" class="form-control input-sm " id="lastName" value="" required="" placeholder="Enter lastName ">
															
													</div>
												</div> 
												 
											 
													<div class="col-sm-2">
														<div class="form-group has-feedback">
															<label for="rmConfigTripDetailsModel.trNumber">
																Travel Request Number </label> <input type="text" autocomplete="off" name="carOrderCustomerList[0].rmConfigTripDetailsModel.trNumber" class="form-control input-sm " id="rmtrNumber" value="" required="" placeholder="Enter Travel Request Number ">
																
														</div>
													</div>
													 
											
									
							
							
							
							
										 
											
											
											
											
											
											
											
											
											
											 
											
											</div>
											
											
						           <div class="col-sm-12  pricebreakup"> 
											<a class="btn btn-success createdquotation collapsed" role="button" data-toggle="collapse" href="#priceFilters" aria-expanded="true" aria-controls="priceFilters"> Price Breakup(s)  </a>
									</div>  
									 
										<div class="col-sm-12 clearfix pricebb price-details">
											<div class="row">
												<div class="collapse" id="priceFilters" aria-expanded="true">
													<div class="panel-body">
														<!-- Filter of main info -->
														<div class=" col-sm-12 clearfix">
														
														<div class="col-sm-2"> 
																<div class="form-group has-feedback">
																	<label for="busOrderRow.supplierPrice" class="control-label">Supplier Price</label> <input type="text" autocomplete="off" name="carOrderCustomerList[0].supplierAmount" class="form-control input-sm" id="supplierFare" value="0" required="" onchange="numbersonly(this)" placeholder="Enter supplier Price ">
																		
																</div>
																</div>
															 
															<div class="col-sm-2">
																<div class="form-group has-feedback">
																	<label for="hotelName" class=" control-label">Base
																		Fare </label> <input type="text" autocomplete="off" id="baseFare" name="carOrderCustomerList[0].baseAmount" class="form-control input-sm baseFareprice cusValidationforprice" required="required" value="0" placeholder="Base Fare">
																		

																</div>
															</div>
															
															
															 
															 
															 
															<div class="col-sm-2">
																<div class="form-group has-feedback">
																	<label for="hotelName" class=" control-label notone">Tax
																	</label> <input type="text" autocomplete="off" id="taxFare" value="0" name="carOrderCustomerList[0].tax" class="form-control input-sm cusValidationforprice" required="required" placeholder="Tax">
																		

																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group has-feedback">
																	<label for="hotelName" class=" control-label notone1">Markup
																	</label> <input type="text" autocomplete="off" name="carOrderCustomerList[0].markUp" id="markupFare" value="0" class="form-control input-sm cusValidationforprice" required="required" placeholder="Markup">
																		

																</div>
															</div>
															
														</div>
													</div>
												</div>
											</div>
										</div> 
											</div>
                                    </div>
                                </div>
                                </div>
                                </div>
                                <div class="col-md-12">
 <div class="panel-group mt-4 mb-2" id="accordion" role="tablist" aria-multiselectable="true">
<div class="panel panel-default">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse03" aria-expanded="false" aria-controls="collapseThree">
                                        <div class="panel-heading pt-4 pb-4" role="tab" id="headingThree">
                                            <h4 class="panel-title">
                                                <i class="more-less fa fa-plus pull-right"></i> <span class="company-font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Pricing Details</b></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="">
                                        <div class="panel-body"> 
											
												<div class="col-sm-2">
													<div class="form-group has-feedback">
														<label for="carTravelRequestQuotation.currency">Currency
														</label> <select class="form-control input-sm" name="carTravelRequestQuotation.currency" id="Currency" required="">
															
																
																	
																	
																		<option value="AFN">
																			AFN</option>
																	
																
															
																
																	
																	
																		<option value="ALL">
																			ALL</option>
																	
																
															
																
																	
																	
																		<option value="DZD">
																			DZD</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="AOA">
																			AOA</option>
																	
																
															
																
																	
																	
																		<option value="XCD">
																			XCD</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="XCD">
																			XCD</option>
																	
																
															
																
																	
																	
																		<option value="ARS">
																			ARS</option>
																	
																
															
																
																	
																	
																		<option value="AMD">
																			AMD</option>
																	
																
															
																
																	
																	
																		<option value="AWG">
																			AWG</option>
																	
																
															
																
																	
																	
																		<option value="AUD">
																			AUD</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="AZN">
																			AZN</option>
																	
																
															
																
																	
																	
																		<option value="BSD">
																			BSD</option>
																	
																
															
																
																	
																	
																		<option value="BHD">
																			BHD</option>
																	
																
															
																
																	
																	
																		<option value="BDT">
																			BDT</option>
																	
																
															
																
																	
																	
																		<option value="BBD">
																			BBD</option>
																	
																
															
																
																	
																	
																		<option value="BYR">
																			BYR</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="BZD">
																			BZD</option>
																	
																
															
																
																	
																	
																		<option value="XOF">
																			XOF</option>
																	
																
															
																
																	
																	
																		<option value="BMD">
																			BMD</option>
																	
																
															
																
																	
																		<option value="INR" selected="">
																			INR</option>
																	
																	
																
															
																
																	
																	
																		<option value="BOB">
																			BOB</option>
																	
																
															
																
																	
																	
																		<option value="BAM">
																			BAM</option>
																	
																
															
																
																	
																	
																		<option value="BWP">
																			BWP</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="BRL">
																			BRL</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="BND">
																			BND</option>
																	
																
															
																
																	
																	
																		<option value="BGN">
																			BGN</option>
																	
																
															
																
																	
																	
																		<option value="XOF">
																			XOF</option>
																	
																
															
																
																	
																	
																		<option value="BIF">
																			BIF</option>
																	
																
															
																
																	
																	
																		<option value="KHR">
																			KHR</option>
																	
																
															
																
																	
																	
																		<option value="XAF">
																			XAF</option>
																	
																
															
																
																	
																	
																		<option value="CAD">
																			CAD</option>
																	
																
															
																
																	
																	
																		<option value="CVE">
																			CVE</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="KYD">
																			KYD</option>
																	
																
															
																
																	
																	
																		<option value="XAF">
																			XAF</option>
																	
																
															
																
																	
																	
																		<option value="XAF">
																			XAF</option>
																	
																
															
																
																	
																	
																		<option value="CLP">
																			CLP</option>
																	
																
															
																
																	
																	
																		<option value="CNY">
																			CNY</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="COP">
																			COP</option>
																	
																
															
																
																	
																	
																		<option value="KMF">
																			KMF</option>
																	
																
															
																
																	
																	
																		<option value="XAF">
																			XAF</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="NZD">
																			NZD</option>
																	
																
															
																
																	
																	
																		<option value="CRC">
																			CRC</option>
																	
																
															
																
																	
																	
																		<option value="HRK">
																			HRK</option>
																	
																
															
																
																	
																	
																		<option value="CUP">
																			CUP</option>
																	
																
															
																
																	
																	
																		<option value="ANG">
																			ANG</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="XOF">
																			XOF</option>
																	
																
															
																
																	
																	
																		<option value="DKK">
																			DKK</option>
																	
																
															
																
																	
																	
																		<option value="DJF">
																			DJF</option>
																	
																
															
																
																	
																	
																		<option value="XCD">
																			XCD</option>
																	
																
															
																
																	
																	
																		<option value="DOP">
																			DOP</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="EGP">
																			EGP</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="XAF">
																			XAF</option>
																	
																
															
																
																	
																	
																		<option value="ERN">
																			ERN</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="ETB">
																			ETB</option>
																	
																
															
																
																	
																	
																		<option value="FKP">
																			FKP</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="FJD">
																			FJD</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="XPF">
																			XPF</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="XAF">
																			XAF</option>
																	
																
															
																
																	
																	
																		<option value="GMD">
																			GMD</option>
																	
																
															
																
																	
																	
																		<option value="GEL">
																			GEL</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="GHS">
																			GHS</option>
																	
																
															
																
																	
																	
																		<option value="GIP">
																			GIP</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="DKK">
																			DKK</option>
																	
																
															
																
																	
																	
																		<option value="XCD">
																			XCD</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="GTQ">
																			GTQ</option>
																	
																
															
																
																	
																	
																		<option value="GBP">
																			GBP</option>
																	
																
															
																
																	
																	
																		<option value="GNF">
																			GNF</option>
																	
																
															
																
																	
																	
																		<option value="XOF">
																			XOF</option>
																	
																
															
																
																	
																	
																		<option value="GYD">
																			GYD</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="HNL">
																			HNL</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="HUF">
																			HUF</option>
																	
																
															
																
																	
																	
																		<option value="ISK">
																			ISK</option>
																	
																
															
																
																	
																		<option value="INR" selected="">
																			INR</option>
																	
																	
																
															
																
																	
																	
																		<option value="IDR">
																			IDR</option>
																	
																
															
																
																	
																	
																		<option value="IRR">
																			IRR</option>
																	
																
															
																
																	
																	
																		<option value="IQD">
																			IQD</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="GBP">
																			GBP</option>
																	
																
															
																
																	
																	
																		<option value="ILS">
																			ILS</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="JMD">
																			JMD</option>
																	
																
															
																
																	
																	
																		<option value="JPY">
																			JPY</option>
																	
																
															
																
																	
																	
																		<option value="GBP">
																			GBP</option>
																	
																
															
																
																	
																	
																		<option value="JOD">
																			JOD</option>
																	
																
															
																
																	
																	
																		<option value="KZT">
																			KZT</option>
																	
																
															
																
																	
																	
																		<option value="KES">
																			KES</option>
																	
																
															
																
																	
																	
																		<option value="AUD">
																			AUD</option>
																	
																
															
																
																	
																	
																		<option value="KWD">
																			KWD</option>
																	
																
															
																
																	
																	
																		<option value="KGS">
																			KGS</option>
																	
																
															
																
																	
																	
																		<option value="LAK">
																			LAK</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="LBP">
																			LBP</option>
																	
																
															
																
																	
																	
																		<option value="ZAR">
																			ZAR</option>
																	
																
															
																
																	
																	
																		<option value="LRD">
																			LRD</option>
																	
																
															
																
																	
																	
																		<option value="LYD">
																			LYD</option>
																	
																
															
																
																	
																	
																		<option value="CHF">
																			CHF</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="MOP">
																			MOP</option>
																	
																
															
																
																	
																	
																		<option value="MKD">
																			MKD</option>
																	
																
															
																
																	
																	
																		<option value="MGA">
																			MGA</option>
																	
																
															
																
																	
																	
																		<option value="MWK">
																			MWK</option>
																	
																
															
																
																	
																	
																		<option value="MYR">
																			MYR</option>
																	
																
															
																
																	
																	
																		<option value="MVR">
																			MVR</option>
																	
																
															
																
																	
																	
																		<option value="XOF">
																			XOF</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="MRO">
																			MRO</option>
																	
																
															
																
																	
																	
																		<option value="MUR">
																			MUR</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="MXN">
																			MXN</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="MDL">
																			MDL</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="MNT">
																			MNT</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="XCD">
																			XCD</option>
																	
																
															
																
																	
																	
																		<option value="MAD">
																			MAD</option>
																	
																
															
																
																	
																	
																		<option value="MZN">
																			MZN</option>
																	
																
															
																
																	
																	
																		<option value="MMK">
																			MMK</option>
																	
																
															
																
																	
																	
																		<option value="ZAR">
																			ZAR</option>
																	
																
															
																
																	
																	
																		<option value="AUD">
																			AUD</option>
																	
																
															
																
																	
																	
																		<option value="NPR">
																			NPR</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="XPF">
																			XPF</option>
																	
																
															
																
																	
																	
																		<option value="NZD">
																			NZD</option>
																	
																
															
																
																	
																	
																		<option value="NIO">
																			NIO</option>
																	
																
															
																
																	
																	
																		<option value="XOF">
																			XOF</option>
																	
																
															
																
																	
																	
																		<option value="NGN">
																			NGN</option>
																	
																
															
																
																	
																	
																		<option value="NZD">
																			NZD</option>
																	
																
															
																
																	
																	
																		<option value="AUD">
																			AUD</option>
																	
																
															
																
																	
																	
																		<option value="KPW">
																			KPW</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="NOK">
																			NOK</option>
																	
																
															
																
																	
																	
																		<option value="OMR">
																			OMR</option>
																	
																
															
																
																	
																	
																		<option value="PKR">
																			PKR</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="PGK">
																			PGK</option>
																	
																
															
																
																	
																	
																		<option value="PYG">
																			PYG</option>
																	
																
															
																
																	
																	
																		<option value="PEN">
																			PEN</option>
																	
																
															
																
																	
																	
																		<option value="PHP">
																			PHP</option>
																	
																
															
																
																	
																	
																		<option value="NZD">
																			NZD</option>
																	
																
															
																
																	
																	
																		<option value="PLN">
																			PLN</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="QAR">
																			QAR</option>
																	
																
															
																
																	
																	
																		<option value="RON">
																			RON</option>
																	
																
															
																
																	
																	
																		<option value="RUB">
																			RUB</option>
																	
																
															
																
																	
																	
																		<option value="RWF">
																			RWF</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="WST">
																			WST</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="SAR">
																			SAR</option>
																	
																
															
																
																	
																	
																		<option value="XOF">
																			XOF</option>
																	
																
															
																
																	
																	
																		<option value="RSD">
																			RSD</option>
																	
																
															
																
																	
																	
																		<option value="SCR">
																			SCR</option>
																	
																
															
																
																	
																	
																		<option value="SLL">
																			SLL</option>
																	
																
															
																
																	
																	
																		<option value="SGD">
																			SGD</option>
																	
																
															
																
																	
																	
																		<option value="ANG">
																			ANG</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="SBD">
																			SBD</option>
																	
																
															
																
																	
																	
																		<option value="SOS">
																			SOS</option>
																	
																
															
																
																	
																	
																		<option value="ZAR">
																			ZAR</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="KRW">
																			KRW</option>
																	
																
															
																
																	
																	
																		<option value="SSP">
																			SSP</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="LKR">
																			LKR</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="SHP">
																			SHP</option>
																	
																
															
																
																	
																	
																		<option value="XCD">
																			XCD</option>
																	
																
															
																
																	
																	
																		<option value="XCD">
																			XCD</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="XCD">
																			XCD</option>
																	
																
															
																
																	
																	
																		<option value="SDG">
																			SDG</option>
																	
																
															
																
																	
																	
																		<option value="SRD">
																			SRD</option>
																	
																
															
																
																	
																	
																		<option value="NOK">
																			NOK</option>
																	
																
															
																
																	
																	
																		<option value="SZL">
																			SZL</option>
																	
																
															
																
																	
																	
																		<option value="SEK">
																			SEK</option>
																	
																
															
																
																	
																	
																		<option value="CHF">
																			CHF</option>
																	
																
															
																
																	
																	
																		<option value="SYP">
																			SYP</option>
																	
																
															
																
																	
																	
																		<option value="STD">
																			STD</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="TJS">
																			TJS</option>
																	
																
															
																
																	
																	
																		<option value="TZS">
																			TZS</option>
																	
																
															
																
																	
																	
																		<option value="THB">
																			THB</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="XOF">
																			XOF</option>
																	
																
															
																
																	
																	
																		<option value="NZD">
																			NZD</option>
																	
																
															
																
																	
																	
																		<option value="TOP">
																			TOP</option>
																	
																
															
																
																	
																	
																		<option value="TTD">
																			TTD</option>
																	
																
															
																
																	
																	
																		<option value="TND">
																			TND</option>
																	
																
															
																
																	
																	
																		<option value="TRY">
																			TRY</option>
																	
																
															
																
																	
																	
																		<option value="TMT">
																			TMT</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="AUD">
																			AUD</option>
																	
																
															
																
																	
																	
																		<option value="">
																			</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="GBP">
																			GBP</option>
																	
																
															
																
																	
																	
																		<option value="USD">
																			USD</option>
																	
																
															
																
																	
																	
																		<option value="UGX">
																			UGX</option>
																	
																
															
																
																	
																	
																		<option value="UAH">
																			UAH</option>
																	
																
															
																
																	
																	
																		<option value="AED">
																			AED</option>
																	
																
															
																
																	
																	
																		<option value="UYU">
																			UYU</option>
																	
																
															
																
																	
																	
																		<option value="UZS">
																			UZS</option>
																	
																
															
																
																	
																	
																		<option value="VUV">
																			VUV</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
																
																	
																	
																		<option value="VEF">
																			VEF</option>
																	
																
															
																
																	
																	
																		<option value="VND">
																			VND</option>
																	
																
															
																
																	
																	
																		<option value="XPF">
																			XPF</option>
																	
																
															
																
																	
																	
																		<option value="MAD">
																			MAD</option>
																	
																
															
																
																	
																	
																		<option value="YER">
																			YER</option>
																	
																
															
																
																	
																	
																		<option value="ZMW">
																			ZMW</option>
																	
																
															
																
																	
																	
																		<option value="ZWL">
																			ZWL</option>
																	
																
															
																
																	
																	
																		<option value="EUR">
																			EUR</option>
																	
																
															
														</select>
														
												</div>
											</div>
											
											<div class="col-sm-2">
												<div class="form-group">
													<label for="carOrderRow.supplierPrice"> Supplier Price </label> <input type="text" autocomplete="off" name="carOrderRow.supplierPrice" class="form-control input-sm" id="supplierPrice" value="0" required="" placeholder="Enter Supplier Price" readonly="readonly">
												</div>
											</div>
											
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="carOrderRow.basePrice">
													Base Price </label> <input type="text" autocomplete="off" name="carOrderRow.basePrice" onchange="numbersonly(this)" class="form-control input-sm" id="basePrice" value="0" required="" placeholder="Enter Base Price" readonly="readonly">
											</div>
											</div>
											 
											<div class="col-sm-2"> 
											<div class="form-group has-feedback">
												<label for="carOrderRow.extraKM">
													Extra KM </label> <input type="text" autocomplete="off" name="carOrderRow.extraKM" class="form-control input-sm cusValidationforprice" id="extraKM" value="0" required="" onchange="numbersonly(this)" placeholder="Enter extraKM">
													
											</div>
											</div>

											<div class="col-sm-2"> 
											<div class="form-group has-feedback">
												<label for="carOrderRow.extraHours">
													Extra Hours </label> <input type="text" autocomplete="off" name="carOrderRow.extraHours" onchange="numbersonly(this)" class="form-control input-sm cusValidationforprice" id="extraHours" value="0" required="" placeholder="Enter Extra Hours">
													
											</div>
											</div>

										<div class="col-sm-2"> 
											<div class="form-group has-feedback">
												<label for="carOrderRow.tollOrParkingCharges"> Toll or Parking Charges </label> <input type="text" autocomplete="off" onchange="addtax()" name="carOrderRow.tollOrParkingCharges" class="form-control input-sm cusValidationforprice" id="tollOrParkingCharges" value="0" required="" placeholder="Enter toll/park charges">
													
											</div>
											</div>

										<div class="col-sm-2"> 

											<div class="form-group has-feedback">
												<label for="carOrderRow.driverAllowanceDay"> Driver Allowance Day </label> <input type="text" autocomplete="off" name="carOrderRow.driverAllowanceDay" class="form-control input-sm cusValidationforprice" id="driverAllowanceDay" value="0" required="" onchange="addtax()" placeholder="Enter Driver Allowance Day">
													
											</div>
											</div>
											<div class="col-sm-2"> 

											<div class="form-group has-feedback">
												<label for="carOrderRow.driverAllowanceNight"> Driver Allowance Night </label> <input type="text" autocomplete="off" name="carOrderRow.driverAllowanceNight" class="form-control input-sm cusValidationforprice" id="driverAllowanceNight" value="0" required="" onchange="addtax()" placeholder="Enter Driver Allowance Night">
													
											</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="carOrderRow.otherTaxes">
													Other Taxes </label> <input type="text" autocomplete="off" name="carOrderRow.otherTaxes" onchange="numbersonly(this)" class="form-control input-sm" id="otherTaxes" value="0" required="" placeholder="Enter Other Taxes " readonly="readonly">
											</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="carOrderRow.markUp">Mark
													up Amount</label> <input type="text" autocomplete="off" name="carOrderRow.markUp" class="form-control input-sm" id="markUp" value="0" required="" onchange="numbersonly(this)" placeholder="Enter Management Fee" readonly="readonly">
											</div>
											</div>
											<div class="col-sm-2" style="color: red;display: none" id="balanceCheck"> 
											<div class="form-group" style="color: red;">
												<h5 style="color: red">Total Booking Amount is more than Deposit Or Wallet Balances.Please Check.</h5>
											</div>
											</div>
											<div class="col-sm-2"> 
											<input type="hidden" id="managementFeeToCalc" value="120">
											<div class="form-group">
												<label for="carOrderRow.managementFee">Management Fee</label> <input type="text" autocomplete="off" onchange="numbersonly(this)" name="carOrderRow.managementFee" class="form-control input-sm" id="managementFee" value="120" required="" placeholder="Enter Management Fee" readonly="">
											</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="convenienceFee">
													Convenience Fee </label> <input type="text" autocomplete="off" name="carOrderRow.convenienceFee" onchange="numbersonly(this)" class="form-control input-sm" id="convenienceFee" value="0" required="" placeholder="Enter Convenience Fee" readonly="">
											</div>
											</div>
											<input type="hidden" id="taxType" value="GST">
											
											<div class="col-sm-2"> 
											
											<input type="hidden" autocomplete="off" name="carOrderRow.totalGstTax" onchange="numbersonly(this)" class="form-control input-sm" id="gstTax" value="18.000" required="">
											<div class="form-group">
												<label for="totalGstTaxAmount">
													Gst Tax </label> 
													<div class="inner-addon right-addon">
      												<span class="glyphicon">@ <span id="gstTaxPer">18</span>%</span>
													<input type="text" autocomplete="off" name="totalGstTaxAmount" onchange="numbersonly(this)" class="form-control input-sm" id="gstTaxAmount" value="21.6000" required="" placeholder="Enter Gst Tax " readonly="readonly">
													</div>
											</div>
											</div>
											
											
											
											
											
											
											<div class="col-sm-2"> 
												<div class="form-group has-feedback">
													<label for="carOrderRow.discount"> Discount </label> <input type="text" autocomplete="off" name="carOrderRow.discount" class="form-control input-sm " id="discount" value="50.00" required="" onchange="addtax()" placeholder="Enter Discount" readonly="readonly">
														
												</div>
											</div>
											
										<div class="col-sm-2"> 
											<div class="form-group">
												<label for="carOrderRow.totalAmount">Total
													Amount </label> <input type="text" autocomplete="off" onclick="numbersonly(this);addtax();" name="carOrderRow.totalAmount" id="totalamount" class="form-control input-sm" placeholder="Total Amount" value="0" required="">
													
											</div>
											</div>
											<div class="col-sm-2"> 
											<div class="form-group">
												<label for="carOrderRow.totInvoiceAmount">Invoice
													Amount </label> <input type="text" autocomplete="off" onclick="numbersonly(this);addtax();" name="carOrderRow.totInvoiceAmount" id="invoiceamount" class="form-control input-sm" placeholder="Invoice Amount" value="0" required="">
											</div>
											</div>
										<div class="col-sm-2"> 
											<div class="form-group has-feedback">
												<label for="carTravelRequestQuotation.remarks"> Remarks </label>
												<textarea rows="1" cols="1" class="form-control input-sm " name="carTravelRequestQuotation.remarks" placeholder="remarks" required=""></textarea>
													
											</div>
										</div>
											
											</div>
                                    </div>
                                </div>
                                </div>
                                </div> 
                                  
                                <div class="row">
					<div class="col-md-12">
									<div class="set pull-right" style="margin-bottom: 20px;margin-top: 7px;">
										<div class="form-actions">
											<input type="hidden" name="leadId" value="${param.companyLeadId}">
											<button class="btn btn-default btn-danger" id="tourCancelBtn" type="reset">Reset</button>
											<button type="submit" class="btn btn-primary user-save-btn" id="tourEditBtn">Submit</button>
										</div>
										</div>	
										</div>
					</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
<script src="admin/js/admin/user-validators.js"></script>            
           <script type="text/javascript">
	$(document).ready(function() {
		$("#companyServiceType").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a Company Services",
		});
		
		var arrayuserroleType = '${companyServiceTypeView}'.split(',');
		console.debug(arrayuserroleType);
		$("#companyServiceType").multipleSelect("setSelects", arrayuserroleType);
	
	});
	
 $(document).ready(function() {
		$("#companyCMSType").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a Company CMS",
		});
		
		var arrayuserroleType = '${companyCMSTypeView}'.split(',');
		console.debug(arrayuserroleType);
		$("#companyCMSType").multipleSelect("setSelects", arrayuserroleType);
	
	}); 
</script> 
            <script type="text/javascript">
$(document).ready(function() {
$('#company-register')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
		companyName : {
					message : 'Companyname is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Company Name'
						},
						stringLength : {
							min : 3,
							max : 50,
							
							message : 'Companyname must be more than 3 and less than 100 characters long'
						} 
					}
				},
				companyType : {
					message : 'companyType is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Company Type'
						},
					}
				},
				distributorType : {
					message : 'Distributor is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Distributor Type'
						},
					}
				},
				typeOfWallet : {
					message : 'typeOfWallet is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Wallet Type'
						},
					}
				},
				postAmount : {
					message : 'typeOfWallet is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Enter a Amount'
						},
					}
				},
				currencyCode : {
					message : 'Currency  is not valid',
					validators : {
						 notEmpty : {
							message : 'select Enter a Currency'
						},
					}
				},
				website: {
		                validators: {
		                	notEmpty : {
								message : 'Please enter a website'
							},
		                    uri: {
		                        message: 'The website address is not valid'
		                    }
		                }
		            },
		        userName : {
					message : 'username is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a user name'
						},
						stringLength : {
							min : 3,
							max : 50,
							
							message : 'UserName must be more than 3 and less than 100 characters long'
						} 
					}
				},
				password : {
					message : 'password is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a password'
						},
						/* stringLength : {
							min : 6,
							max : 50,
							
							message : 'Password must be more than 6 and less than 100 characters long'
						}  */
					}
				},
				email : {
					message : 'Email  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a email'
						},
					}
				},
				CountryName : {
					message : 'Countryname  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a country'
						},
					}
				},
				/* Language : {
					message : 'Language  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a language'
						},
					}
				}, */
				phone : {
					message : 'Phone  is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Phone'
						},
						integer: {
	                        message: 'The value is not a Phone Number'
	                    },
						 stringLength : {
								min : 4,
								max : 18,
								message : 'Phone must be more than 4 and less than 15 characters long'
						}  
					}
				},
				companyDescription : {
					message : 'Description  is not valid',
					validators : {
						/*  notEmpty : {
							message : 'Please enter a Description '
						}, */
					}
				},
				billingCompany : {
					message : 'Company Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Company Name'
						},
						stringLength : {
							min : 3,
							max : 50,
							
							message : 'Companyname must be more than 3 and less than 50 characters long'
						} 
					}
				},
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {

	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>
        
	<script type="text/javascript">
$(function() {
  
    $('#companyType').change(function(){
    	 if($('#companyType').val()== 'distributor') {
            $('.distributor-type-div').show(); 
        } 
        else if($('#companyType').val() == 'agent') {
        	 $('.distributor-type-div').hide(); 
          
       } 
        
    });
    
    $('#typeOfWallet').change(function(){
   	 if($('#typeOfWallet').val()== 'Postpaid') {
           $('.Wallet-type-div').show();
           
        } 
       else if($('#typeOfWallet').val() == 'Prepaid') {
       	 $('.Wallet-type-div').hide(); 
     
         
      } 
       
   });
    
    
    
   
 });
 </script>
<script src="admin/js/multiple-select.js"></script>	
		
						<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
