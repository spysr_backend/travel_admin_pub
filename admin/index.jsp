<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<style>
.form-horizontal .form-group {
	margin-left: 0px;
	margin-right: 0px;
}

.form-group {
	margin-bottom: 15px;
}

.form-horizontal-login {
	background: rgba(255, 255, 255, 0.25);
	padding: 25px;
	/* border: 10px solid #fff; */
	border-radius: 20px;
	box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px
		rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.3);
	/* background: linear-gradient(rgba(0, 0, 0, 0.25) , rgba(255, 255, 255, 0.25)); */
	margin-top: 10%;
}

h2.login-heading {
	color: white;
	text-shadow: 2px 2px 5px rgba(0, 0, 0, 0.5);
	font-weight: 700;
}

.btn-login {
	color: #fff !important;
	background-color: #000316;
	border-color: #000000;
	box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.18), 0 6px 30px 5px
		rgba(0, 0, 0, 0.16), 0 8px 10px -5px rgba(0, 0, 0, 0.35);
	border-radius: 25px;
	transition: 0.5s;
}

.btn-login:hover {
	box-shadow: none;
	transition: 0.5s;
	transform: translateY(-5px);
}

.input-sm {
	height: 40px;
	padding: 0px 15px;
	font-size: 15px;
	line-height: 1.5;
	border-radius: 25px;
	border: none;
	box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.25);
}

ul.errorMessage {
	color: white;
	text-shadow: 0px 2px 2px rgba(0, 0, 0, 0.25);
	padding: 10px 10px;
	text-align: center;
	background: #ff6666;
	font-size: 15px;
	border-radius: 30px;
	border: 1px solid #fd2626;
}

.copyright {
	margin-top: 20px;
	font-size: 12px;
	color: white;
	background: #010316;
	padding: 10px 5px;
	bottom: 0px;
	position: fixed;
	width: 100%;
}

.login-admin-logo {
	margin-top: 15%;
	margin-bottom: 15%;
}

.mt-login {
	margin-top: 12%;
}

@media screen and (max-width:765px) {
	.login-admin-logo {
		margin-top: 5%;
		margin-bottom: 10%;
	}
	.copyright {
		margin-top: 50px;
		position: fixed;
	}
}
</style>
<body style="background-image: url(admin/img/background2.jpg);">
	<hr class="colorgraph colorgraph-header">

	<div class="container">

		<div class="row mt-login">
			<div class="col-md-6">
				<div class="col-md-2"></div>
				<div class="col-md-6">
					<img class="img-responsive login-admin-logo" src="admin/img/touroxy-admin-logo.png">
				</div>
			</div>
			<div class="col-md-4">
				<s:if test="hasActionErrors()">
					<s:actionerror />
				</s:if>
				<s:if test="hasActionMessages()">
					<s:actionmessage />
				</s:if>
				<form action="validateLogin" id="logForm" method="post" class="form-horizontal form-horizontal-login">
					<div>
						<h2 class="login-heading">
							<s:text name="tgi.label.login" />
						</h2>
						<hr class="colorgraph">
						<div class="form-group">
							<input type="email" name="Email" id="email" class="form-control input-sm" placeholder="User Email" autocomplete="off" required>
						</div>
						<div class="form-group">
							<input type="password" name="Password" id="Password" class="form-control input-sm" placeholder="******" autocomplete="off" required>
						</div>
						<div class="form-group">
							<input type="text" name="companyUserId" class="form-control input-sm" placeholder="User Company Id" autocomplete="off" required>
						</div>
						<span class="button-checkbox">
						<input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden"> 
						<a href="email_forget_password" class="btn btn-link pull-right" style="color: white; margin-bottom: 10px;">
						<s:text name="tgi.label.forgot_password" />?</a>
						</span>
						<div class="row">
							<div class="">
								<input type="submit" class="btn btn-lg btn-login btn-block"
									value="LogIn">
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>

	</div>

</body>
