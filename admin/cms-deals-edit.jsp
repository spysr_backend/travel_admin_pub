<%@ page language="java" isELIgnored="false"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<script src="admin/js/jscolor.js"></script>
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"admin_deals_edit?id="+"${param.id}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<script type="text/javascript">
$(document).ready(function(){
	$("#catagory").val($("#categoryHdn").val());
	$("#dealsType").val($("#dealstypeHdn").val());
	$("#currency").val($("#currencyHdn").val());
	});
	
	function setImagePath()
	{
		$("#imageUrl").val($("#lefile").val());
	}
</script> 
<section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                            <div class="hd clearfix">
                            
                                   <h3 class="pull-left" ><s:text name="tgi.label.update_home_page_deals" /></h3>  
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                               
                             <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="admin_deals_fetch"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.deal_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                               
                            </div>
                            <div class="hd clearfix">
                                <h5 class="pull-left"></h5>
                            </div>
                            <div class="cnt cnt-table">
                                <div class="table-responsive">
                                   <form action="admin_deals_update.action" method="POST"
		enctype="multipart/form-data">
	<button class="jscolor {valueElement:'bg-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:20px;">
		<s:text name="tgi.label.box_color" />
	</button>
	<input type="text" name="boxBgcolor" id="bg-chosen-color" value="${cmsStyleManagementUtil.bgcolor}" style="width:80px;" />
                                    <table class="table table-form">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>&nbsp;</th>
                                               
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td>
                                                    <div class="field-name"><s:text name="tgi.label.category" /></div>
                                                </td>
												<td>
												<input type="hidden" id="categoryHdn" value="${dealsVO.catagory}">
                                                   <select name="catagory" id="catagory" class="form-control" id="focusedInput" >
			<option value="flight"><s:text name="tgi.label.flight" /></option>
			<option value="hotel"><s:text name="tgi.label.hotel" /></option>
			<option value="car"><s:text name="tgi.label.car" /></option>
			<option value="tour"><s:text name="tgi.label.tour" /></option>
			<option value="cruise"><s:text name="tgi.label.cruise" /></option>
		</select>
							
                                                </td>
                                                												<td>
                                                    <div class="field-name"><s:text name="tgi.label.deals_type" /></div>
                                                </td>
                                                <td>
                                                <input type="hidden" id="dealstypeHdn" value="${dealsVO.dealsType}">
                                                   <select name="dealsType" id="dealsType" class="form-control" >
			<option value="promodeal"><s:text name="tgi.label.Promo_Deals" /></option>
			<option value="bestdeal"><s:text name="tgi.label.Best_Deals" /></option>
			<option value="topdeal"><s:text name="tgi.label.Top_Deals" /></option>
			<option value="dailydeal"><s:text name="tgi.label.Daily_Deals" /></option>
			<option value="lastminutedeal"><s:text name="tgi.label.Last_Minute_Deals" /></option>
			<option value="staticlink"><s:text name="tgi.label.Static_Link" /></option>
		</select>
                                                </td>

                                                
                                            </tr>
                                            
                                            <tr>
											 <td>
                                                    <div class="field-name"><s:text name="tgi.label.title" /></div>
                                                </td>
                                                <td>
                                                   <input type="text" class="form-control input-sm" id=""
									name="title"   placeholder="" value="${dealsVO.title}" autocomplete="off" required>
                                                </td>
                                                 <th style="margin-left:0px;">
                                                 <select name="t_family" class="form-comtrol" > 
                                                 <s:if test="cmsStyleManagementUtil.t_family != null">
                                                 <option value="${cmsStyleManagementUtil.t_family}" selected="selected">${cmsStyleManagementUtil.t_family}</option>
                                                 </s:if>
                                                 <s:else>
                                                 <option value="" selected="selected"><s:text name="tgi.label.Select_Font" /></option>
                                                 </s:else>
												<option value="Arial"><s:text name="tgi.label.arial" /></option>
												<option value="Helvetica Neue"><s:text name="tgi.label.helvetica_neue" /></option>
												<option value="Helvetica"><s:text name="tgi.label.helvetica" /></option>
												<option value="sans-serif"><s:text name="tgi.label.Sans-Serif" /></option>
                                                 </select>
                                                 <select name="t_weight" class="form-comtrol">
                                                   <s:if test="cmsStyleManagementUtil.t_weight != null">
                                                 <option value="${cmsStyleManagementUtil.t_weight}" selected="selected">${cmsStyleManagementUtil.t_weight}</option>
                                                 </s:if>
												<s:else>
                                                 <option value="" selected="selected"><s:text name="tgi.label.Select_Weight" /></option>
                                                 </s:else>
												<option value="normal"><s:text name="tgi.label.normal" /></option>		
												<option value="bold"><s:text name="tgi.label.bold" /></option>
												<option value="bolder"><s:text name="tgi.label.bolder" /></option>
												<option value="lighter"><s:text name="tgi.label.lighter" /></option>
                                                 </select>
                                                 </th>
                                                <th>
                                                	
	<button class="jscolor {valueElement:'title-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:50px;">
		<s:text name="tgi.label.font_color" />
	</button>
	<input type="text" name="t_color" id="title-chosen-color" value="${cmsStyleManagementUtil.t_color}" style="width:80px;" />
                                                </th>
												</tr>
												  <tr>
											<td>
                                                    <div class="field-name"><s:text name="tgi.label.price" /></div>
                                                </td>
                                                <td>
                                                   <input type="text" class="form-control input-sm"  name="price" id="price" value="${dealsVO.price}" />
                                                </td>
                                                 <th style="margin-left:0px;">
                                                 <select name="p_family" class="form-comtrol">
                                                  <s:if test="cmsStyleManagementUtil.p_family != null">
                                                 <option value="${cmsStyleManagementUtil.p_family}" selected="selected">${cmsStyleManagementUtil.p_family}</option>
                                                 </s:if>
                                                 <s:else>
                                                 <option value="" selected="selected"><s:text name="tgi.label.Select_Font" /></option>
                                                 </s:else>
												<option value="Arial"><s:text name="tgi.label.arial" /></option>
												<option value="Helvetica Neue"><s:text name="tgi.label.helvetica_neue" /></option>
												<option value="Helvetica"><s:text name="tgi.label.helvetica" /></option>
												<option value="sans-serif"><s:text name="tgi.label.Sans-Serif" /></option>
                                                 </select>
                                                 <select name="p_weight" class="form-comtrol">
                                                  <s:if test="cmsStyleManagementUtil.p_weight != null">
                                                 <option value="${cmsStyleManagementUtil.p_weight}" selected="selected">${cmsStyleManagementUtil.p_weight}</option>
                                                 </s:if>
                                                 <s:else>
                                                 <option value="" selected="selected"><s:text name="tgi.label.Select_Weight" /></option>
                                                 </s:else>
												<option value="normal"><s:text name="tgi.label.normal" /></option>		
												<option value="bold"><s:text name="tgi.label.bold" /></option>
												<option value="bolder"><s:text name="tgi.label.bolder" /></option>
												<option value="lighter"><s:text name="tgi.label.lighter" /></option>
                                                 </select>
                                                 </th>
                                                <th>
                                                	
	<button class="jscolor {valueElement:'price-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:50px;">
		<s:text name="tgi.label.font_color" />
	</button>
	<input type="text" name="p_color" id="price-chosen-color" value="${cmsStyleManagementUtil.p_color}" style="width:80px;" />
                                                </th>
												</tr>
                                            <tr>
											 <td>
                                                    <div class="field-name"><s:text name="tgi.label.currency" /></div>
                                                </td>
                                                 <td>
                                                  <!-- <input type="text" class="form-control input-sm" id="focusedInput" name="currency" /> -->
                                                  <input type="hidden" id="currencyHdn" value="${dealsVO.currency}">
                                                  	<select class="form-control input-sm" name="currency" id="currency">
								<option value="0" selected="selected"><s:text name="tgi.label.Select_Currency" /></option>
								<s:iterator value="countryList">
									<option value='<s:property value="cur_code"/>'><s:property value="cur_code" /></option>
								</s:iterator>
							</select>
                                                </td>
                                               
                                                                               <th style="margin-left:0px;">
                                                 <select name="c_family" class="form-comtrol">
                                                  <s:if test="cmsStyleManagementUtil.c_family != null">
                                                 <option value="${cmsStyleManagementUtil.c_family}" selected="selected">${cmsStyleManagementUtil.c_family}</option>
                                                 </s:if>
                                                 <s:else>
                                                 <option value="" selected="selected"><s:text name="tgi.label.Select_Font" /></option>
                                                 </s:else>
												<option value="Arial"><s:text name="tgi.label.arial" /></option>
												<option value="Helvetica Neue"><s:text name="tgi.label.helvetica_neue" /></option>
												<option value="Helvetica"><s:text name="tgi.label.helvetica" /></option>
												<option value="sans-serif"><s:text name="tgi.label.Sans-Serif" /></option>
                                                 </select>
                                                 <select name="c_weight" class="form-comtrol">
                                                  <s:if test="cmsStyleManagementUtil.c_weight != null">
                                                 <option value="${cmsStyleManagementUtil.c_weight}" selected="selected">${cmsStyleManagementUtil.c_weight}</option>
                                                 </s:if>
                                                 <s:else>
                                                 <option value="" selected="selected"><s:text name="tgi.label.Select_Weight" /></option>
                                                 </s:else>
												<option value="normal"><s:text name="tgi.label.normal" /></option>		
												<option value="bold"><s:text name="tgi.label.bold" /></option>
												<option value="bolder"><s:text name="tgi.label.bolder" /></option>
												<option value="lighter"><s:text name="tgi.label.lighter" /></option>
                                                 </select>
                                                 </th>
                                                <th>
                                                	
	<button class="jscolor {valueElement:'currency-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:50px;">
		<s:text name="tgi.label.font_color" />
	</button>
	<input type="text" name="c_color" id="currency-chosen-color" value="${cmsStyleManagementUtil.c_color}" style="width:80px;" />
                                                </th>
                                                </tr>
                                                <tr>
<td>
                                                    <div class="field-name"><s:text name="tgi.label.description" /></div>
                                                </td>
                                                 <td>
                                                  <input type="text"  class="form-control input-sm" id="focusedInput" name="description" value="${dealsVO.description}" />
                                                </td>
                                                                              <th style="margin-left:0px;">
                                                 <select name="d_family" class="form-comtrol">
                                                 <s:if test="cmsStyleManagementUtil.d_family != null">
                                                 <option value="${cmsStyleManagementUtil.d_family}" selected="selected">${cmsStyleManagementUtil.d_family}</option>
                                                 </s:if>
                                                 <s:else>
                                                 <option value="" selected="selected"><s:text name="tgi.label.Select_Font" /></option>
                                                 </s:else>
												<option value="Arial"><s:text name="tgi.label.arial" /></option>
												<option value="Helvetica Neue"><s:text name="tgi.label.helvetica_neue" /></option>
												<option value="Helvetica"><s:text name="tgi.label.helvetica" /></option>
												<option value="sans-serif"><s:text name="tgi.label.Sans-Serif" /></option>
                                                 </select>
                                                 <select name="d_weight" class="form-comtrol">
                                                  <s:if test="cmsStyleManagementUtil.d_weight != null">
                                                 <option value="${cmsStyleManagementUtil.d_weight}" selected="selected">${cmsStyleManagementUtil.d_weight}</option>
                                                 </s:if>
                                                 <s:else>
                                                 <option value="" selected="selected"><s:text name="tgi.label.Select_Weight" /></option>
                                                 </s:else>
												<option value="normal"><s:text name="tgi.label.normal" /></option>		
												<option value="bold"><s:text name="tgi.label.bold" /></option>
												<option value="bolder"><s:text name="tgi.label.bolder" /></option>
												<option value="lighter"><s:text name="tgi.label.lighter" /></option>
                                                 </select>
                                                 </th>
                                                <th>
                                                	
	<button class="jscolor {valueElement:'description-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:50px;">
		<s:text name="tgi.label.font_color" />
	</button>
	<input type="text" name="d_color" id="description-chosen-color" value="${cmsStyleManagementUtil.d_color}" style="width:80px;"/>
                                                </th>
</tr>
                                            <tr> <td> 
											
      <div class=" field-name"><s:text name="tgi.label.upload_images" /></div></td>
	 <td> <div class="col-sm-12">
	 <input type="hidden" id="imageUrl" name="imageUrl" value="${dealsVO.imageUrl}">
<input id="lefile" type="file" style="display:none" name="upload" onchange="javascript:setImagePath();" >
<div class="input-append">
<input id="photoCover" class="input-large" type="text" value="${dealsVO.imageUrl}">
<a class="btn" onclick="$('input[id=lefile]').click();" style="
    background-color: rgb(6, 111, 192);
    color: white;
"><s:text name="tgi.label.browse" /></a>
</div></div>
</td>
 <td>
                                                    <div class="field-name"><s:text name="tgi.label.image_caption" /></div>
                                                </td>
                                                 <td>
                                                  <input type="text"  class="form-control input-sm" id="focusedInput" name="imageCaption" value="${dealsVO.imageCaption}" />
                                                </td>												
</tr>
                                        </tbody>
                                    </table>
<div class="form-group table-btn">
                                        <div class="col-sm-10 col-sm-offset-2 ">
                                            <%-- <div class="pull-left"><button class="btn btn-danger" type="submit"><s:text name="tgi.label.clear_form" /></button></div> --%>
                                            <div class="pull-right">
                                                	<input type="hidden" name="id" value="${id}">
     												<input type="hidden" name="version" value="${version}" >
                                                <%-- <button class="btn btn-default" type="reset"><s:text name="tgi.label.cancel" /></button> --%>
                                                <button class="btn btn-primary " type="submit"><s:text name="tgi.label.update" /></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
                    	   <s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>