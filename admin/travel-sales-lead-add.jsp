<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  <link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="admin/js/admin/panel-app.js"></script>
  
  <link href="admin/css/select/bootstrap-select.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="admin/js/select/bootstrap-select.min.js"></script>
     <style>
     .btn-spysr-blue:hover, .btn-spysr-blue:focus, .btn-spysr-blue.focus {
    color: #ffffff !important;
    text-decoration: none;
}
     </style>
    <script>
  /* checkbox button js */
  $(document).ready(function(){
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }
        // Initialization
        function init() {
            updateDisplay();
            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
  </script>
      <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid"> 
                <div class="row">
                <div class="pnl">
                <nav aria-label="breadcrumb">
				  <div class="hd clearfix own-bradcum mb-4">
				  			<div class="dropdown pull-right mt-10">
							  <a href="listTravelSalesLead" class="btn btn-xs btn-outline-danger dropdown-toggle" type="button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back To List</a>
							</div>
				    <h4 class=""><img class="clay" src="admin/img/svg/tourist.svg" width="26" alt="Higest"> Travel Sales Lead Receive<span class="text-danger">${message}</span></h4>    
				  </div>
				</nav>
				</div>
					<div class="col-md-12">
					<div class="container-detached">
						<div class="content-detached">
									<!-- Profile info -->
									<div class="panel panel-flat" style="border-top: 4px solid #02214c;">
										<div class="panel-heading" style="padding: 22px 15px;">
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="12" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="reload" class="icon-a"><img class="clippy" src="admin/img/svg/reload.svg" width="12" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="8" alt="Copy to clipboard" ></a></li>
							                	</ul>
						                	</div>
										</div>
								 <form class="from-horizontal" id="travel_sales_leadForm" name="travel_sales_leadForm" method="post">
										<div class="panel-body" style="display: block;">
													
													<div class="col-md-6 row-minus">
														<div class="col-md-12 form-group">
															<input type="text" name="title" value="" class="form-control" placeholder="Lead Title" autocomplete="on">
														</div>
														<div class="col-md-6 form-group">
															<input type="text" name="firstName" value="" class="form-control" placeholder="First Name" autocomplete="on">
														</div>
														<div class="col-md-6 form-group">
															<input type="text" name="lastName" value="" class="form-control" placeholder="Last Name" autocomplete="on">
														</div>
														<div class="col-md-6 form-group">
															<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
															<input type="email" name="email" value="" class="form-control " placeholder="Email">
														</div>
														</div>
														<div class="col-md-6 form-group">
															<input type="text" name="city" value="" class="form-control " placeholder="City" autocomplete="on">
														</div>
														<div class="col-md-6 form-group">
														<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-phone" aria-hidden="true"></i></span>
																<input type="text" name="phone" value="" class="form-control" placeholder="Phone#1" autocomplete="on">
														</div>
														</div>
														<hr>
														<div class="col-md-6 form-group">
														<span data-toggle="tooltip" data-placement="top" title="How is budget of package" >
														<select aria-hidden="true" name="budget" id="budget" class="form-control selectpicker" >
														   <option>Budget</option>
														   <option value="0-2">Economy (0 - 2 star)</option>
														   <option value="3-4">Standard (3 - 4 star)</option>
														   <option value="5+">Luxury (5 star &amp; above)</option>
														</select>
														</span>
														</div>
														<div class="col-md-6 form-group">
														<div data-toggle="tooltip" data-placement="top" title="What is package duration">
														<select aria-hidden="true" name="duration" id="duration" class="form-control selectpicker">
														  <option value="0">Duration*</option>
														  	<c:forEach items="${tourDurationMap}" var="duration">
																	<option value="${duration.key}">${duration.value}</option>
																</c:forEach>
														</select>
														</div>
														</div>
														<div class="col-md-6 form-group">
														<div data-toggle="tooltip" data-placement="top" title="Departure -- Arrival Date">
														<div class="input-group"> <span class="input-group-addon" id="departure-date"><i class="fa fa-calendar" aria-hidden="true"></i></span>
																<input type="text" name=departArrivalDate  class="form-control datetimepicker1" placeholder="Departure Date - Arrival Date" value="" id="" autocomplete="on">
														</div>
														</div>
														</div>
														<div class="col-md-6 form-group">
														<div data-toggle="tooltip" data-placement="top" title="Where you from to go" >
														<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
																<input type="text" name="destinationFrom" id="destination-from" value="" class="form-control" placeholder="Destination From..." autocomplete="on">
														</div>
														</div>
														</div>
														<div class="col-md-6 form-group">
														<div data-toggle="tooltip" data-placement="top" title="Where want to go" >
														<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
																<input type="text" name="destinationTo" id="destination-to" value="" class="form-control" placeholder="Destination To..." autocomplete="on">
														</div>
														</div>
														</div>
														<%-- <div class="hotel-check-in-out">
														<div class="col-md-6 form-group">
														<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
																<input type="text" name="startDate" id="start-date" value="" class="form-control datetimepicker1" placeholder="Start Date...">
														</div>
														</div>
														<div class="col-md-6 form-group">
														<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
																<input type="text" name="endDate" id="end-date" value="" class="form-control datetimepicker1" placeholder="End Date...">
														</div>
														</div>
														</div> --%>
													</div>
													<div class="col-md-6 row-minus">
														<div class="row">
															<div class="col-md-2 col-sm-3 col-xs-4  form-group">
															<span class="button-checkbox">
														        <button type="button" class="btn" data-color="spysr-blue" id="tour-btn">Tour&nbsp;&nbsp;</button>
														        <input type="checkbox" name="tour" class="hidden" value="false" id="is-tour"/>
														    </span>
															</div>
															<div class="col-md-2 col-sm-2 col-xs-4 form-group">
																	<span class="button-checkbox">
																        <button type="button" class="btn" data-color="spysr-blue" id="flight-btn">Flight</button>
																        <input type="checkbox" name="flight" class="hidden" value="false" id="is-flight"/>
																    </span>
															</div>
															<div class="col-md-2 col-sm-3 col-xs-4 form-group">
																	<span class="button-checkbox">
																        <button type="button" class="btn" data-color="spysr-blue" id="hotel-btn">Hotel&nbsp;</button>
																        <input type="checkbox" name="hotel" class="hidden" value="false" id="is-hotel"/>
																    </span>
															</div>
															
															<div class="col-md-2 col-sm-3 col-xs-4  form-group">
																	<span class="button-checkbox">
																        <button type="button" class="btn" data-color="spysr-blue" id="car-btn">&nbsp;Car&nbsp;&nbsp;&nbsp;&nbsp;</button>
																        <input type="checkbox" name="car" class="hidden" value="false" id="is-car" />
																    </span>
															</div>
															<div class="col-md-2 col-sm-3 col-xs-4  form-group">
																	<span class="button-checkbox">
																        <button type="button" class="btn" data-color="spysr-blue" id="car-btn">&nbsp;Bus&nbsp;&nbsp;&nbsp;&nbsp;</button>
																        <input type="checkbox" name="bus" class="hidden" value="false" id="is-bus" />
																    </span>
															</div>
															<div class="col-md-2 col-sm-3 col-xs-4 form-group">
																	<span class="button-checkbox">
																        <button type="button" class="btn" data-color="spysr-blue" id="car-btn">Cruise</button>
																        <input type="checkbox" name="cruise" class="hidden" value="false" id="is-car" />
																    </span>
															</div>
															</div>
															<div class="col-md-3 form-group">
															<div data-toggle="tooltip" data-placement="top" title="How many rooms" >
															<select aria-hidden="true" name="noOfRoom" id="noOfRoom" class="form-control selectpicker input-sm">
															  <option value="0" selected="selected">Select Room</option>
															  <c:forEach var="i" begin="0" end="50" step="1">
																<option value="${i}" >${i}</option>
															</c:forEach>
															</select>
															</div>
															</div>
															<div class="col-md-3 form-group">
															<div data-toggle="tooltip" data-placement="top" title="Select Adult" >
																<div class="input-group"> <span class="input-group-addon" id="sizing-addon2">Adult</span>
																<select aria-hidden="true" name="adult" id="adult" class="form-control ">
																  <c:forEach var="i" begin="0" end="100" step="1">
																	<option value="${i}">${i}</option>
																</c:forEach>
																</select>
																</div>
																</div>
																</div>
															<div class="col-md-3 form-group">
															<div data-toggle="tooltip" data-placement="top" title="Select Kid's" >
																<div class="input-group"> <span class="input-group-addon" id="sizing-addon2">Kid's(5<8)</span>
																<select aria-hidden="true" name="kid" id="kid" class="form-control ">
																  <c:forEach var="i" begin="0" end="100" step="1">
																	<option value="${i}">${i}</option>
																</c:forEach>
																</select>
																</div>
															</div>
																</div>
															<div class="col-md-3 form-group">
															<div data-toggle="tooltip" data-placement="top" title="Select Infant" >
																<div class="input-group"> <span class="input-group-addon" id="sizing-addon2">Kid's(0<5)</span>
																<select aria-hidden="true" name="infant" id="infant" class="form-control ">
																  <c:forEach var="i" begin="0" end="5" step="1">
																	<option value="${i}">${i}</option>
																</c:forEach>
																</select>
																</div>
																</div>
																</div>
															<div class="col-md-6 form-group">
															<span data-toggle="tooltip" data-placement="top" title="Best time to call" >
																<select aria-hidden="true" name="callTime" id="callTime" class="form-control selectpicker">
																  <option value="" selected="SELECTED">Best time to call</option>
																	<option value="Morning">Morning</option>
																	<option value="Afternoon">Afternoon</option>
																	<option value="Evening">Evening</option>
																	<option value="Any time">Any time</option>
																</select>
															</span>
															</div>
															<div class="col-md-6 form-group">
															<span data-toggle="tooltip" data-placement="top" title="What is source of lead" >
																<select aria-hidden="true" name="leadSource" id="lead-source" class="form-control selectpicker">
																    <option value="">Lead Source*</option>
																	<option value="Advertisement">Advertisement</option>
																	<option value="CustomerEvent">Customer Event</option>
																	<option value="Employee Referral" data-lead-source-type="employeeReferral">Employee Referral</option>
																	<option value="Google AdWords">Google AdWords</option>
																	<option value="Website">Website</option>
																	<option value="Socal Network">Socal Network</option>
																	<option value="Walk-In">Walk-In</option>
																	<option value="Telephone">Telephone</option>
																	<option value="B2B">B2B</option>
																	<option value="Justdial">Justdial</option>
																	<option value="Others">Others</option>
																</select>
																</span>
															</div>
															<div class="col-md-12" style="display: none" id="employee-name">
															<div class="form-group">
															<span data-toggle="tooltip" data-placement="top" title="Employee Name" >
																<select name="employeeReferralId" id="employee-referral-id" class="form-control selectpicker" data-live-search="true">
																<option value="0" selected="selected">Select User</option>
																<c:forEach items="${userVos}" var="userOnj">
																			<option value="${userOnj.userId}">${userOnj.firstName} ${userOnj.lastName}</option>
																</c:forEach>
																</select>
																</span>
															</div>
															</div>
															<div class="col-md-12 form-group">
																<textarea name="requirement" rows="4" id="" class="form-control" placeholder="Requirements..."></textarea>
															</div>
															<div class="row">&nbsp;</div>
															<div class="row">&nbsp;</div>
															<div class="col-md-8"></div>
															<div class="col-md-4">
															<span data-toggle="tooltip" data-placement="top" title="Follow up status" >
															<select class="selectpicker form-control" name="travelSalesLeadFollowUpData.leadStatus" data-style="" data-header="Select a Lead Status">
															  <option value="New">New</option>
															  <option value="Unqualified">Unqualified</option>
															  <option value="Active">Active</option>
															  <option value="No Connect">No Connect</option>
															  <option value="Matured">Matured</option>
															  <option value="Qualified">Qualified</option>
															  <option value="Closed">Closed</option>
															</select>
															</span>
															</div>
													</div>
													<div class="col-md-12">&nbsp;</div>
													<div class="col-md-12 mb-2" style="margin-top:50px">
													<div class="text-center">
									                     <button type="submit" class="btn btn-lg btn-primary" id="save_travel_sales_leadBtn">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
									            	</div>
													</div>

										</div>
										</form>
									</div>
									<!-- /profile info -->

						</div>
						
					</div>
				</div>
				</div>
			</section>
        <!--ADMIN AREA ENDS-->
  <script>
/*-------------------------------------------------------------------------*/
 $(function() {
	  $('input[name="departArrivalDate"]').daterangepicker({
		  "maxSpan": {
	          "days": 7
	      },
		  autoUpdateInput: false,
	      locale: {
	          cancelLabel: 'Clear'
	      },
	      "showDropdowns": true,
	      "cancelClass": "btn-danger",
	      linkedCalendars: true,
	  });

	  $('input[name="departArrivalDate"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
	  });

	  $('input[name="departArrivalDate"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });
	});
/*-------------------------------------------------------------------------*/
$('#lead-source').on('change', function (e) {
    var optionSelected = $('#lead-source option:selected').data('lead-source-type');
   
    if(optionSelected == 'employeeReferral'){
    	$("#employee-name").show(500);
    }
    else
    {
    	$("#employee-name").hide(500);
    }
    
});
/*-------------------------------------------------------------------------*/
$(document).ready(function() {
    $("#travel_sales_leadForm").bootstrapValidator({
        feedbackIcons: {
            valid: "fa fa-check",
            invalid: "fa fa-remove",
            validating: "fa fa-refresh"
        },
        fields:{firstName:{message:"Name is not valid",validators:{notEmpty:{message:"Name is required field"},
        	stringLength:{min:1,message:"Name must be more than 3 and less than 100 characters long"}}},
        	country:{message:"Country is not valid",validators:{notEmpty:{message:"Country is a required field"}}},
        	phone:{message:"Phone is not valid",validators:{notEmpty:{message:"Phone is a required field"}}},
        	adult:{message:"Adult is not valid",validators:{notEmpty:{message:"Adult is a required field"}}}}
        
    }).on("error.form.bv", function(e) {}).on("success.form.bv", function(e) {
            e.preventDefault(), 
            notifySuccess();
            $.ajax({
                url: "saveTravelSalesLead",
                type: "POST",
                dataType: "json",
                data: $("#travel_sales_leadForm").serialize(),
                success: function(jsonData) {
                	if(jsonData.message.status == 'success'){
	  					showModalPopUp(jsonData.message.message,"s");
	  					var url = '<%=request.getContextPath()%>/listTravelSalesLead';
						window.location.replace(url);	
                	}
	  				else if(jsonData.message.status == 'input'){
	  					showModalPopUp(jsonData.message.message,"w");}
	  				else if(jsonData.message.status == 'error'){
	  					showModalPopUp(jsonData.message.message,"e");}
                	
                        $("#save_travel_sales_leadBtn").removeAttr("disabled"), console.debug("button disabled ");
                    var s = $(e.target);
                    s.data("bootstrapValidator");
                    s.bootstrapValidator("disableSubmitButtons", !1).bootstrapValidator("resetForm", !0)
                },
                error: function(e, a, s) {
                    showModalPopUp("Travel Lead can not be Saved.", "e")
                }
            })
    }).on("status.field.bv", function(e, a) {
        a.bv.getSubmitButton() && (console.debug("button disabled "), a.bv.disableSubmitButtons(!1))
    })
});

/* -----------------------------------------------------*/
</script>