<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>

<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}

</style>



        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
       

            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="">
                        <div class="pnl">
                        <div class="hd clearfix">
					<h5 class="pull-left">Company Wallet User List </h5>
					<div class="set pull-right hidden-xs" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-up"></i></span></a>
					</div>
				</div>
                            <%-- <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.company_wallet_user_list" /></h5>
                               <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li><a href="superMyTx"><i class="fa fa-home"></i> <s:text name="tgi.label.view_all_transactions" />
										</a></li>
								</ul>
							</div>
						</div>
                               </div> 
                                 <div class="set pull-right">
                               <div id="category-tabs">
									<button style="padding:4px;" class="btn btn-success" type="reset" id="filterBtn">
								<a class="collapsed faq-links" data-toggle="collapse"  href="#collapseFive" aria-expanded="false" >
								<i class="fa fa-compress "></i> Filter</a></button>
								</div></div>
                                </div> --%>
                                <br>
                                
                      			<form action="listUserWalletFilter" class="filter-form">
                      			 <div class="" id="filterDiv" style="display:none;" >
                      			  <div class="form-group">
                                 <div class="col-md-2 col-sm-6">
								<input type="text" name="userName" id="username-json"
												placeholder="User Id...."
												class="form-control search-query" />
								</div>
                                 <div class="col-md-2 col-sm-6">
                                <input type="text" name="email" id="email-json"
												placeholder="Email...."
												class="form-control search-query" />
                                </div>
                                <div class="col-md-2 col-sm-6">
                                <input type="text" name="phone" id="phone-json"
												placeholder="Phone...."
												class="form-control hasdatepicker" />
                                </div>
                                <div class="col-md-3 col-sm-6">
								<input type="text" name="countryName" id="countryname-json"
												placeholder="Country Name...."
												class="form-control search-query" />
								</div>
								 <div class="col-md-2 col-sm-6">
<%--                                 <input type="text" name="startDate" id="datepicker_startDate" value="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MM/dd/yyyy')}"
												placeholder="Start Date...."
												class="form-control search-query date1" /> --%>
                                <input type="text" name="startDate" id="datepicker_startDate" value="${spysr:formatDate(startDate,'E MMM dd HH:mm:ss Z yyyy', 'MMM dd yyyy')}"
												placeholder="Start Date...."
												class="form-control search-query date1" />
												
                                </div>
                                <div class="col-md-2 col-sm-6">
                                <input type="text" name="endDate" id="datepicker_endDate" value="${spysr:formatDate(endDate,'E MMM dd HH:mm:ss Z yyyy', 'MMM dd yyyy')}}"
												placeholder="End Date...."
												class="form-control hasdatepicker date2" />
                                </div>
                                <div class="col-md-1 col-sm-6">
								<div class="form-group">
								<input type="reset" class="btn btn-danger" id="configreset" value="Clear">
								</div>
								</div>
									<div class="col-md-1 col-sm-6">
									<div class="form-group">
									<input type="submit" class="btn btn-info" value="Search" />
								</div>
								</div>
                                </div></div>
                                </form>
                                <%-- <div class="set pull-right">
                                    <div class="dropdown">
                                      <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Dropdown
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li><a href="#">Separated link</a></li>
                                      </ul>
                                    </div>
                               </div>
 --%>                          
							
							<input type="hidden"
					value="<s:property value="%{#session.Company.companyUserId}"/>"
					id="companyUserId"> <input type="hidden"
					value="<s:property value="%{#session.Company.email}"/>" id="email">
				<input type="hidden"
					value="<s:property value="%{#session.User.companyUserId}"/>"
					id="user_id">
                               
                               
                             <div class="cnt cnt-table">
<div class="dash-table ">
		<div class="box ">
		<div class="fw-container">
		<div class="fw-body">
			<div class="content pt-2 mt-0"> 
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"
									role="grid" aria-describedby="example_info"
									style="width: 100%;" cellspacing="0">
                                        <!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
                                        <thead>
										<tr class="border-radius border-color" role="row">
						<th>S.No</th>
							<th><s:text name="tgi.label.userid" /> </th>
							<!-- <th>Company</th> -->
							<th ><s:text name="tgi.label.walletBalance" /></th>
						
							<th><s:text name="tgi.label.email" /></th>
							<th><s:text name="" />Phone</th>
						  	<th><s:text name="tgi.label.country" /></th>
						  		<th >Update Balance</th>
						  	<th></th>
						</tr>
					</thead>
					<tbody>
						<s:iterator value="walletusersList"  var="user" status="rowstatus">
						<s:if test="#user.userName != 'DirectUser'">
 							<tr>
							 <td data-title="S.No"><s:property value="#rowstatus.count"/></td>
								<!--  <td data-title="User"><input type="checkbox" class="checkthis" /></td> -->
								<td data-title="UserID"><s:property value="userName" /></td>
								<!-- <td data-title="User">TO DO</td> -->
								<td data-title="Wallet Balance"><s:property value="agentWallet.walletBalance" /> <s:property value="agentWallet.currencyCode" /></td>
								<td data-title="Email"><s:property value="Email" /></td>
								<td data-title="Phone"><s:property value="mobile" /> <s:property value="phone"/></td>
								<%-- <td data-title="User"><s:property value="Createddate" /></td> --%>
								<td data-title="Country"><s:property value="countryName" /></td>
								<td data-title="Update Balance"> 
									 <a href="updateUserWallet?id=<s:property value="id"/>&userName=<s:property value="userName"/>&agentWallet.walletId=<s:property value="agentWallet.walletId"/>&agentWallet.walletBalance=<s:property value="agentWallet.walletBalance"/>&agentWallet.currencyCode=<s:property value="agentWallet.currencyCode"/>" class="btn btn-success btn-xs" data-title="Update" >
								  <span data-placement="top" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <s:text name="tgi.label.update" /></span></a>
										 </td>
								 <td></td>

							</tr>
							</s:if>
						</s:iterator>
					</tbody>
                                        </table>
                                        
                                        </div></div></div></div></div>
                                    </div>
								<s:if test="hasActionErrors()">
					<div class="alert" style="display: none">
						<span class="fa fa-thumbs-o-up fa-1x"></span>
						<s:actionerror />
					</div>
				</s:if>
				<s:if test="hasActionMessages()">
					<div class="alert" style="display: none">
						<span class="fa fa-thumbs-o-up fa-1x"></span>
						<s:actionmessage />
					</div>
				</s:if>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        <!--ADMIN AREA ENDS-->

<%-- <script type="text/javascript">
	$(function() {
		/*   $("#mytable1").DataTable(); */
		$('#mytable').DataTable({
			"paging" : true,
			"lengthChange" : true,
			"searching" : true,
			"ordering" : true,
			"info" : true,
			"autoWidth" : false,
			"search" : {
				"regex" : true,
			},
			"pagingType" : "full_numbers",
			"lengthMenu" : [ 10, 25, 50, 75, 100, 500 ],

		});
	});
</script>
 --%>
  <script src="admin/js/jquery-ui.js" ></script>
  <script>
$(document).ready(function() {
	var date1="${spysr:formatDate(startDate,'E MMM dd HH:mm:ss Z yyyy', 'MMM dd yyyy')}";
	var date2="${spysr:formatDate(endDate,'E MMM dd HH:mm:ss Z yyyy', 'MMM dd yyyy')}";
	 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
});
</script>

	<script type="text/javascript" class="init">
	$(document).ready(function() {
			$('#filterBtn').click(function() {
		    $('#filterDiv').toggle();
		});
			
			$('div.easy-autocomplete').removeAttr('style');
	} );
	
	
	$('.faq-links').click(function() {
		var collapsed = $(this).find('i').hasClass('fa-compress');

		$('.faq-links').find('i').removeClass('fa-expand');

		$('.faq-links').find('i').addClass('fa-compress');
		if (collapsed)
			$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
	});
		</script>	 
 <script>
//user name
	var UserName = {
		url: "getUserWalletJson.action?data=userName",
		getValue: "userName",
		
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#username-json").easyAutocomplete(UserName);
	
	
 // email
		var Email = {
			url: "getUserWalletJson.action?data=email",
			getValue: "email",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#email-json").easyAutocomplete(Email);
		
		// phone
		var Phone = {
			url: "getUserWalletJson.action?data=phone",
			getValue: "phone",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#phone-json").easyAutocomplete(Phone);
		
		// country name
		var CountryName = {
			url: "getUserWalletJson.action?data=countryName",
			getValue: "countryName",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#countryname-json").easyAutocomplete(CountryName);
		
	</script>	
