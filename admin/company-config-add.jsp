<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="dj" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style>
#notification_error {
    border: 1px solid #A25965;
    height: auto;
    width: auto;
    padding: 4px;
    background: #F8F0F1;
    text-align: center;
    -moz-border-radius: 5px;
}

#notification_ok {
    border: 1px #567397 solid;
    height: auto;
    width: auto;
    padding: 4px;
    background: #f5f9fd;
    text-align: center;
    -moz-border-radius: 5px;
}

</style>
    <!--************************  MAIN ADMIN AREA ***************************-->
    
            <section class="wrapper container-fluid">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><img class="clay" src="admin/img/svg/config.svg" width="26" alt="Higest"> &nbsp; <s:text name="tgi.label.new_company_configuration" /></h5>
                           <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-outline-primary" href="companyConfigList"><span class="fa fa-arrow-left"></span> Back To List </a>
							</div>
						</div>
                               </div> 
                            </div>
                            <s:if test="hasActionErrors()">
								<div class="success-alert" style="display: none">
									<span class="fa fa-thumbs-o-up fa-1x"></span>
									<s:actionerror />
								</div>
							</s:if>
							<s:if test="hasActionMessages()">
								<div class="success-alert" style="display: none">
									<span class="fa fa-thumbs-o-up fa-1x"></span>
									<s:actionmessage />
								</div>
							</s:if>
                        <form action="insertCompanyConfig" id="add-company-configData" method="post" class="form-horizontal">
							<div class="col-lg-12">
							<div class="profile-timeline-card">
							<div class="pull-right">
							<p class="text-danger">Hi rahul kumar mahur</p>
							</div>
							<p class="mt-1 mb-2">
								<span class="company-font-icon"><i class="fa fa-user"></i></span> <span> <b class="blue-grey-text">Add Company Config</b></span>
							</p>
							<hr>
                               <div class="row row-minus">
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">Company User Id</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
											<select class="form-control" name="configCompanyName" id="company_id" required="required">
														<option selected><s:text name="tgi.label.select_company_userId" /></option>
														<s:if test="%{#session.Company.companyRole.isDistributor()}">
															<s:iterator value="companyList">
																<option value="${id},${companyName},${companyUserId}">${companyUserId}(${companyName})</option>
															</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="companyList">
																<option value="${id},${companyName},${companyUserId}">${companyUserId}(${companyName})</option>
															</s:iterator>
														</s:else>


													</select>
											</div></div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.config_name" /></label>
										<div class="controls"><div class="form-group">
												<input type="text" class="form-control" id="config-name" name="configName" autocomplete="off" required="required">
										</div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.config_type" /></label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control" name="configType" id="configType" required="required">
												<option selected="selected" value="">Select Config Type</option>
												<option value="B2C">B2C</option>
												<option value="B2B">B2B</option>
												<option value="API">API</option>
												<option value="WL"><s:text name="tgi.label.whitelable" /></option>
										</select>
										</div></div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.parent_configId" />
											</label>
										<div class="controls"><div class="form-group">
												<select class="form-control" name="parentConfigIdSplit" id="parentConfigIdSplit" >
													<option selected value="0">Select Parent Configid</option>
													<s:iterator value="%{configList}">
														<option value="<s:property value="id "/>,<s:property value="companyUserId "/>">
															<s:property value="id" /> - <s:property value="configName" /> ( <s:property value="companyUserId" />)
														</option>
													</s:iterator>
												</select>
										</div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.status" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control" name="configActiveStatus" id="config-status">
													<option value="YES"><s:text name="tgi.label.active" /></option>
													<option value="NO" selected="selected"><s:text name="tgi.label.in_active" /></option>
												</select>
									</div></div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput">Domain Name</label>
										<div class="controls"><div class="form-group">
												<input type="text" class="form-control" id="domainName" name="domainName" autocomplete="off" required="required">
										</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput">Domain Url</label>
										<div class="controls"><div class="form-group">
												<input type="text" class="form-control" id="domainUrl" name="domainUrl" autocomplete="off" required="required">
										</div>
										</div>
									</div>
								</div>	     
	                                </div>
                                <div class="row row-minus">
									<div class="col-md-12">
										<div class="set pull-right" style="margin-bottom:0px;margin-top: 7px;">
										<div class="form-actions">
											<input type="hidden" name="languageId" value="EN">
											<input type="hidden" name="taxType" value="GST">
											<button class="btn btn-default btn-danger" id="tourCancelBtn" type="reset">Reset</button>
											<button type="submit" class="btn btn-primary" id="user-company-config-btn"><s:text name="tgi.label.register" /></button>
										</div></div>
									</div>
									</div> 
                        </div>
                        </form>
                    </div>
            </section>
<script type="text/javascript">
$(document).ready(function() {
$('#add-company-configData').bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
	
			}
		}).on('error.form.bv', function(e) {
}).on('success.form.bv', function(e) {
	showModalPopUp("Saving Details, Please wait ..","i");
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>
<script type="text/javascript">
$(function() {
	var companyUserId1 = null;
	var parentCompanyUserId = null;
	var parentConfigValidOption = null;
	$('#company_id').change(function() {
		var parentConfigValid = document.getElementById("parentConfigIdSplit");
		parentConfigValidOption = parentConfigValid.options[0].value;
		var array = $('#company_id').val().split(",");
		if (array[0] == 1) {
			$("#parentConfigIdSplit").hide();
		} else {
			$("#parentConfigIdSplit").show();
		}
		parentCompanyUserId = array[2];
		if ($('#configName').val() != "") {
			$('#configName').val("");
		}
		if ($('#company_id').val() == "") {
			$('#parentconfigid_div').hide();
			$('#Whitelable_div').hide();
		} else {
			$('#parentconfigid_div').show();
			$('#commission_alert-div').show();
		}
		if (parentCompanyUserId != $('#companyUserId').val()) {
			if ($('#company_id').val() == "") {
				$('#parentconfigid_div').hide();
			} else {
				$('#parentconfigid_div').show();
				$('#Whitelable_div').show();
			}
			if ($('#parentConfigIdSplit').val() != "0") {
				$('#parentConfigIdSplit').val("0");
			} else {
				$('#commission_alert-div').hide();
			}
			if ($('#parentConfigIdSplit').val() == "0") {
				parentConfigValid.options[0].value = "";
			} else {
				$('#commission_alert-div').hide();
			}
			$('#parentConfigIdSplit').change(function() {
				var parentArray = $('#parentConfigIdSplit').val().split(",");
				companyUserId1 = parentArray[1];
				rateType1 = parentArray[2];
				commissionType1 = parentArray[3];
				commissionAmount1 = parentArray[4];
				$('.modelType').text(rateType1);
				$('.commType').text(commissionType1);
				$('.commAmnt').text(commissionAmount1);
				if ($('#company_id').val() == "") {
					$('#commission_alert-div').hide();
				} else {
					$('#commission_alert-div').show();
				}
				if ($('#configName').val() != "") {
					$('#configName').val("");
				}
			});
		} else {
			$('#parentconfigid_div').hide();
			$('#commission_alert-div').hide();
			$('.commission-group').hide();
			document.getElementById("commission").value = "0.00";
			document.getElementById("configName").value = "";
			document.getElementById("commissionType").value = "";
			document.getElementById("rateType").value = "Net";
			if ($('#parentConfigIdSplit').val() == "") {
				parentConfigValid.options[0].value = "0";
			}
		}
	});
});
</script>
 <s:if test="message != null && message  != ''">
<c:choose>
<c:when test="${fn:contains(message, 'successfully')}">
         	  <script>
         	  $(document).ready(function() 
         			  {
         	  $('#alert_box').modal({
	        	    show:true,
	        	    keyboard: false
	    	    } );
			  $('#alert_box_body').text('${param.message}');
         			  });
</script>
</c:when>
<c:otherwise>
         	  <script>
         	  $(document).ready(function() 
         			  {
         		  $('#alert_box_info').modal({
  	        	    show:true,
  	        	    keyboard: false
  	    	    } );
  			  $('#alert_box_info_body').text("${param.message}");
         			  });
</script>
</c:otherwise>
</c:choose>
</s:if>