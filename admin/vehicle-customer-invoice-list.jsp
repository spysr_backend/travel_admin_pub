<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>


<style>
#example_filter input {
	height: 25px;
}

#example_wrapper .dt-buttons {
	padding-top: 10px;
}

.show_date_to_user {
	padding: 0px 15px;
	position: relative;
	width: 93%;
	top: -25px;
	left: 5px;
	pointer-events: none;
	display: inherit;
	color: #000;
}
</style>




<title><s:property value="user" /></title>

<!-- Content Header (Page header) -->
<section class="wrapper container-fluid">

	<div class="">
		<div class="card1">
			<div class="pnl">

				<%--  <ol class="breadcrumb">
				<li><a href="home"><i class="fa fa-dashboard"></i> <s:text name="tgi.label.home" /></a></li>
				<li class="active"><s:text name="tgi.label.dashboard" /></li>
			</ol>
		</section> --%>
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.car_customer_invoice_list" />
					</h5>
					<div class="set pull-right">
						<div class="dropdown">
							<div class="btn-group">
								<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
								<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="carReportList"><span
											class="glyphicon glyphicon-list-alt"></span>&nbsp;Report List
									</a></li>
									<li class=""><a class="" href="carOrderList"><span
											class="glyphicon glyphicon-list-alt"></span>&nbsp;Order List
									</a></li>
									<li class=""><a class="" href="carCommissionReport"><span
											class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text
												name="tgi.label.agent_commision_report" /></a></li>
									<li class=""><a class="" href="carCustomerInvoiceList"><span
											class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text
												name="tgi.label.custommer_invoice" /> </a></li>
									<li class=""><a class="" href="carAgentCommInvoiceList"><span
											class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text
												name="tgi.label.agent_commision_invoice" /></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="set pull-right"
						style="margin-top: 5px; font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id=""
							data-toggle="collapse" style="cursor: pointer;"
							data-toggle="collapse" href="filterDiv" aria-expanded="false">
							<span class="text-spysr"><span class="filter-text"></span>
								<img class="clippy" src="admin/img/svg/filter.svg" width="12"
								alt="Copy to clipboard" style="margin-bottom: 3px;">
								Filter <i class="fa fa-angle-down"></i></span>
						</a>
					</div>
				</div>
				<br>
				<form action="carCustomerInvoiceList" class="filter-form"
					id="resetform">
					<div class="" id="filterDiv" style="display: none; margin: 10px;">

						<div class="row">
							<s:if test="%{#session.Company!=null}">
								<s:if
									test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
									<div class="col-md-2">
										<div class="form-group">
											<input type="hidden" id="HdncompanyTypeShowData" value="" />
											<select name="companyTypeShowData" id="companyTypeShowData"
												class="form-control input-sm">
												<option value="all" selected="selected"><s:text
														name="tgi.label.all" /></option>
												<option value="my"><s:text name="tgi.label.my_self" /></option>
												<s:if test="%{#session.Company!=null}">
													<s:if test="%{#session.Company.companyRole.isSuperUser()}">
														<option value="distributor"><s:text
																name="tgi.label.distributor" /></option>
													</s:if>
												</s:if>
												<option value="agency"><s:text
														name="tgi.label.agency" /></option>
											</select>
										</div>
									</div>
								</s:if>
							</s:if>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="orderId" id="orderid-json"
										placeholder="Search By Order Id...."
										class="form-control search-query input-sm" />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="bookingStartDate"
										id="bookingStartDate" placeholder="Start Booking Date...."
										class="form-control search-query date1 input-sm " />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="bookingEndDate" id="bookingEndDate"
										placeholder="End Booking Date...."
										class="form-control search-query date2 input-sm" />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="travelStartDate" id="travelStartDate"
										placeholder="PickUp Date...."
										class="form-control search-query date3 input-sm" />
								</div>

							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="travelEndDate" id="travelEndDate"
										placeholder="DropOff Date........"
										class="form-control search-query date4 input-sm" />
								</div>

							</div>

						</div>
						<div class="row">
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="origin" id="origin"
										placeholder="PickUp From...."
										class="form-control search-query input-sm" />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="destination" id="destination"
										placeholder="DropOff At........"
										class="form-control search-query input-sm" />
								</div>

							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="firstName" id="customerFirstName"
										placeholder="Passenger FirstName..."
										class="form-control search-query input-sm" />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="lastName" id="customerLastName"
										placeholder="Passenger LastName..."
										class="form-control search-query input-sm" />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="email" id="customerEmail"
										placeholder="Passenger Email"
										class="form-control search-query input-sm" />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="mobile" id="customerMobile"
										placeholder="Passenger Mobile"
										class="form-control search-query input-sm" />
								</div>
							</div>
						</div>
						<div class="row clearfix">

							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="pnr" id="hotelName-json"
										placeholder="Car Pnr"
										class="form-control search-query input-sm" />
								</div>
							</div>

							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="createdBy" id="agency-json"
										placeholder="Agency..."
										class="form-control search-query input-sm" />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<select name="paymentMode" id="paymentMode"
										class="form-control input-sm">
										<option value="" selected="selected"><s:text
												name="tgi.label.select_payment_method" /></option>
										<option value="ALL"><s:text name="tgi.label.all" /></option>
										<option value="CHECK"><s:text name="tgi.label.CHECK" /></option>
										<option value="CREDIT_CARD"><s:text
												name="tgi.label.CREDIT_CARD" /></option>
										<option value="DEBIT_CARD"><s:text
												name="tgi.label.DEBIT_CARD" /></option>
										<option value="WALLET"><s:text
												name="tgi.label.WALLET" /></option>
									</select>
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<select name="paymentModeType" id="paymentModeType"
										class="form-control input-sm">
										<option value="" selected="selected"><s:text
												name="tgi.label.select_payment_mode" /></option>
										<option value="ALL"><s:text name="tgi.label.all" /></option>
										<option value="SQUAREUP"><s:text
												name="tgi.label.squareUp" /></option>
										<option value="EXTERNALLINK_TEST"><s:text
												name="tgi.label.external" /></option>
										<option value="AUTHORIZE"><s:text
												name="tgi.label.authorize" /></option>
										<option value="WALLET"><s:text
												name="tgi.label.WALLET" /></option>
										<option value="WIRECARD"><s:text
												name="tgi.label.wirecard" /></option>
										<option value="STRIPE"><s:text
												name="tgi.label.stripe" /></option>
									</select>
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<select name="paymentStatus" id="paymentStatus"
										class="form-control input-sm">
										<option value="" selected="selected"><s:text
												name="tgi.label.select_payment_status" /></option>
										<option value="ALL"><s:text name="tgi.label.all" /></option>
										<option value="PENDING"><s:text
												name="tgi.label.PENDING" /></option>
										<option value="SUCCESS"><s:text
												name="tgi.label.SUCCESS" /></option>
										<option value="FAILED"><s:text
												name="tgi.label.FAILED" /></option>
										<option value="PROCESSING"><s:text
												name="tgi.label.PROCESSING" /></option>
										<option value="DECLINED"><s:text
												name="tgi.label.DECLINED" /></option>
										<option value="REFUND"><s:text
												name="tgi.label.REFUND" /></option>
									</select>
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<select name="bookingStatus" id="bookingStatus"
										class="form-control input-sm">
										<option value="" selected="selected"><s:text
												name="tgi.label.select_booking_status" /></option>
										<option value="ALL"><s:text name="tgi.label.all" /></option>
										<option value="PENDING"><s:text
												name="tgi.label.PENDING" /></option>
										<option value="CONFIRM"><s:text
												name="tgi.label.CONFIRM" /></option>
										<option value="FAILED"><s:text
												name="tgi.label.FAILED" /></option>
										<option value="PROCESSING"><s:text
												name="tgi.label.PROCESSING" /></option>
										<option value="CANCELLED"><s:text
												name="tgi.label.CANCELLED" /></option>
									</select>
								</div>
							</div>
						</div>
						<div class="row clearfix">

							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="invoiceNo" id="agency-json"
										placeholder="Car Invoice No..."
										class="form-control search-query input-sm" />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="supplier" id="agency-json"
										placeholder="Car Supplier..."
										class="form-control search-query input-sm" />
								</div>
							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="carModel" id="hotelName-json"
										placeholder="Car Model"
										class="form-control search-query input-sm" />
								</div>

							</div>
							<div class="col-md-2 col-sm-6">
								<div class="form-group">
									<input type="text" name="carCompany" id="hotelName-json"
										placeholder="Car Company"
										class="form-control search-query input-sm" />
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<input type="reset" class="btn btn-danger btn-block"
										id="configreset" value="Clear">
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<input type="submit" class="btn btn-info btn-block"
										value="Search" />
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- Main content -->
				<%-- <s:if test="%{#session.Company!=null && #session.User!=null}">
						<s:if test="%{#session.User.userRoleId.isSuperUser()}"> --%>

				<div class="dash-table scroller">
					<div class="content mt-0 pt-0">
						<div id="example_wrapper" class="dataTables_wrapper ">
							<table id="example" class="display dataTable responsive nowrap"
								role="grid" aria-describedby="example_info" style="width: 100%;"
								cellspacing="0">
								<!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
								<thead>
									<tr class="border-radius border-color" role="row">
										<th data-priority="1"><s:text name="tgi.label.sno" /></th>
										<th data-priority="3"><s:text
												name="tgi.label.booking_date" /></th>
										<th data-priority="2"><s:text name="tgi.label.orderid" /></th>
										<%-- <th><s:text name="tgi.label.orderref" /></th> --%>
										<th data-priority="4"><s:text name="tgi.label.invoice_no" /></th>
										<th data-priority="5"><s:text
												name="tgi.label.passenger_name" /></th>
										<th data-priority="6"><s:text
												name="tgi.label.pickup_date" /></th>
										<th data-priority="7"><s:text
												name="tgi.label.dropoff_date" /></th>
										<th data-priority="8"><s:text name="tgi.label.tot_amount" /></th>
										<th data-priority="9"><s:text
												name="tgi.label.payment_status" /></th>
										<th data-priority="10"><s:text
												name="tgi.label.booking_status" /></th>
										<th data-priority="11"><s:text name="tgi.label.agency" /></th>
										<th data-priority="12"><s:text name="tgi.label.action" /></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<s:if test="customerInvoiceList.size>0">

										<s:iterator value="customerInvoiceList" status="serial">

											<tr>
												<td data-title="S.No"><s:property
														value="%{#serial.count}" /></td>
												<td data-title="<s:text name="Booking Date" />">${spysr:formatDate(createdAt,'yyyy-mm-dd HH:mm:ss.SSS', 'MMM-d-yyyy')}</td>
												<td data-title="OrderRef"><s:property value="orderId" /></td>
												<%-- <td data-title="OrderRef"><s:property value="orderReference" /></td> --%>
												<td data-title="Invoice No"><s:property
														value="invoiceNo" /></td>
												<td data-title="Invoice No"><s:property
														value="customer.firstName" /> <s:property
														value="customer.lastName" /></td>
												<td data-title="<s:text name="tgi.label.pickup_date" />">${spysr:formatDate(carOrderInfo.pickUpDateTime,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy')}</td>
												<td data-title="<s:text name="DroOffDate" />">${spysr:formatDate(carOrderInfo.returnDateTime,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM-d-yyyy')}</td>
												<td data-title="Total Amt"><s:property
														value="totalPrice" /></td>
												<%-- <td data-title="<s:text name="tgi.label.currency" />"><s:property value="bookingCurrency" /></td> --%>
												<td
													data-title="Booking <s:text name="tgi.label.payment_status" />"><s:property
														value="paymentStatus" /></td>
												<td
													data-title="Booking <s:text name="tgi.label.booking_status" />"><s:property
														value="statusAction" /></td>
												<td data-title="Agency"><s:property value="createdBy" />
												</td>
												<td data-title="Action">
													<p data-placement="top" title="Generate Invoice">

														<a
															href="generateCarCustomerInvoice?id=<s:property value="id"/>&orderid=<s:property value="orderId"/>&orderReference=<s:property value="orderReference"/>&companyId=<s:property value="companyId"/>"
															class="btn btn-success btn-xs" data-toggle="modal"> <i
															class="fa fa-file-text"></i> Invoice
														</a>
													</p>

												</td>
												<td></td>
											</tr>

										</s:iterator>
									</s:if>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content-wrapper -->
<link
	href="admin/css/jquery-ui.css"
	rel="stylesheet" type="text/css" />
<script src="admin/js/jquery-ui.js"></script>
<script>
	$(document)
			.ready(
					function() {
						var date1 = "${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
						var date2 = "${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
						spysr_date_custom_plugin('filter-form', 'date1',
								'date2', 'mm/dd/yyyy', 'MMM DD YYYY', date1,
								date2);

						var date3 = "${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
						var date4 = "${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
						spysr_date_custom_plugin('filter-form', 'date3',
								'date4', 'mm/dd/yyyy', 'MMM DD YYYY', date3,
								date4);
					});
</script>


<script type="text/javascript">
	$(function() {
		/*  $('#row_dim').hide();  */
		$('#user').change(function() {
			//alert($('#user').val());
			if ($('#user').val() == 'ALL') {
				$('#company_form-group').show();
			} else {
				$('#company_form-group').hide();
			}
		});

		$('#companyName').change(function() {
			//alert($('#companyName').val());
			if ($('#companyName').val() == 'ALL') {
				$('#user_form-group').show();
			} else {
				$('#user_form-group').hide();
			}
		});

	});
</script>

<script type="text/javascript" class="init">
	$(document).ready(function() {
		$('#filterBtn').click(function() {
			$('#filterDiv').toggle();
		});
		$('div.easy-autocomplete').removeAttr('style');

		$('#configreset').click(function() {
			$('#resetform')[0].reset();
		});
	});

	$('.faq-links').click(function() {
		var collapsed = $(this).find('i').hasClass('fa-compress');

		$('.faq-links').find('i').removeClass('fa-expand');

		$('.faq-links').find('i').addClass('fa-compress');
		if (collapsed)
			$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
	});
</script>

<!--AutoCompleteter  -->
<script>
	//order id
	var OrderId = {
		url : "getCarReportsJson.action?data=orderId",
		getValue : "orderId",

		list : {
			match : {
				enabled : true
			}
		}
	};

	$("#orderid-json").easyAutocomplete(OrderId);

	//airine
	var PaymentStatus = {
		url : "getCarReportsJson.action?data=status",
		getValue : "status",

		list : {
			match : {
				enabled : true
			}
		}
	};

	$("#pymtstatus-json").easyAutocomplete(PaymentStatus);

	var Agency = {
		url : "getCarReportsJson.action?data=agencyName",
		getValue : "agencyName",

		list : {
			match : {
				enabled : true
			}
		}
	};
	$("#agency-json").easyAutocomplete(Agency);

	var PickUpOfficeName = {
		url : "getCarReportsJson.action?data=pickupOfficeName",
		getValue : "pickupOfficeName",

		list : {
			match : {
				enabled : true
			}
		}
	};
	$("#pickupofficename-json").easyAutocomplete(PickUpOfficeName);

	var OrderReference = {
		url : "getCarReportsJson.action?data=orderReference",
		getValue : "orderReference",

		list : {
			match : {
				enabled : true
			}
		}
	};
	$("#orderreference-json").easyAutocomplete(OrderReference);

	$(document).ready(
			function() {
				$(".filterBtn").click(function() {
					$("#filterDiv").toggle(500)
				}), $("div.easy-autocomplete").removeAttr("style"), $(
						"#configreset").click(function() {
					$("#resetform")[0].reset()
				})
			}), $(".filter-link").click(
			function() {
				var e = $(this).find("i").hasClass("fa-angle-up");
				$(".filter-link").find("i").removeClass("fa-angle-down"), $(
						".filter-link").find("i").addClass("fa-angle-up"), e
						&& $(this).find("i").toggleClass(
								"fa-angle-up fa-2x fa-angle-down fa-2x")
			});
</script>
