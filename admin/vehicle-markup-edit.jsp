<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
  <%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src="admin/js/jquery-ui.js" ></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"carMarkupEdit?id="+"${param.id}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>

<%-- <script type="text/javascript">
 	 $(function() {
		 loadstations();
	 });
 	function loadstations() {
		  $.getJSON("hotels.json",{
				format : "json"
			}).done(
					 function(jsonbusdata) {
					 
						  citylist = [];
						  names = [];
						 
						 $.each(jsonbusdata, function(i, station) {
							citylist.push(station.chain);
							 
						 });
						 $("#hotelChain").autocomplete({
					        source: citylist,
					         
					    });
						 
						 $('#hotelChain').on('autocompleteselect', function (e, ui) {
							    	 
			 }); 
					    $.ui.autocomplete.filter = function (array, term) {
					        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
					        return $.grep(array, function (value) {
					            return matcher.test(value.label || value.value || value);
					        });
					    };
					 });
 	}
 	
 	
 	
 	
 	
 	</script> --%>
 <%-- <script>
   $(function() {
		var id=$("#uniqueId").val();
		var accumulative=$("#accumulative").val();
		var fixedAmount=$("#fixedAmount").val();
	/* 	var positionOfMarkup=$("#positionOfMarkup").val(); */
		//var airline=$("#airline").val();
		var country=$("#country").val();
		//var classOfService=$("#classOfService").val();
		//alert(id+"\n"+country_id);
		 document.getElementById('accumulative'+id).value =accumulative;
		 document.getElementById('fixedAmount'+id).value =fixedAmount;
	/* 	 document.getElementById('positionOfMarkup'+id).value =positionOfMarkup; */
		 //document.getElementById('airline'+id).value =airline;
		 document.getElementById('country'+id).value =country;
		 //document.getElementById('classOfService'+id).value =classOfService;

		 
	}); 
   
   
   
   
   
 </script>  --%>
 <script>
 $(document).ready(function() 
 { 
	 $("#datepicker_arr").datepicker({
		 dateFormat: "M-dd-yyyy"  
		/*  changeMonth: true,
		 changeYear: true */
	 }); 
	 $(".date").datepicker({
		 format: "M-dd-yyyy"  
		/*  changeMonth: true,
		 changeYear: true */
	 }); 
	 });
	 
 </script> 
     <script>
 $(document).ready(function() 
 { 
	 $("#datepicker_dep").datepicker({
		 format: "M-dd-yyyy"
	 });
	 });
 </script>
  <script>
 $(document).ready(function() 
 { 
	 $("#datepicker_PromofareStart").datepicker({
		 format: "M-dd-yyyy"
	 });
	 }); 
 </script>
   <script>
 $(document).ready(function() 
 { 
	 $("#datepicker_PromofareEnd").datepicker({
		 format: "M-dd-yyyy"
	 });
	 }); 
 </script>
 

        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
     
            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card1">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.edit_car_markup" /></h5>
                                <div class="set pull-left">
                                  <!--   <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="carMarkupList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.car_markup_list"/></a></li>
								</ul>
							</div>
							</div>
                               </div> 
                            </div>
		<div class="row">
					<div class="col-lg-12">
						<div class="card-block">
							   <form action="updateCarMarkup" id="car-markup-update" method="get" class="form-horizontal">
                                    <div class=" row">
					<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.config_number" />
											</label>
										<input type="hidden" value="${configData}" id="configDataHdn">
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control input-sm" name="configData"
								id="configData" autocomplete="off" required>
								<option selected value="0"><s:text name="tgi.label.select_company_configNumber"/> </option>
								<s:if test="%{#session.Company.companyRole.isDistributor()}">
								 <s:iterator value="%{#session.AgencyConfigIdsList}">
									<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
								  </s:iterator>
								  <s:iterator value="%{#session.companyConfigIds}">
									<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
								  </s:iterator>
 						 		</s:if>
 						 		<s:elseif test="%{#session.Company.companyRole.isAgent()}">
								  <s:iterator value="%{#session.AgencyConfigIdsList}">
									<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
								  </s:iterator>
 						 		</s:elseif>
 						 		
								<s:else>
								<s:iterator value="%{#session.companyConfigIds}">
								<option value="<s:property value="company_id"/>/<s:property value="config_id"/>/<s:property value="configname"/>/<s:property value="config_number"/>"><s:property value="config_number"/>(<s:property value="configname"/>)</option>
								 </s:iterator>
 							</s:else>
						 

							</select></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_name" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm" id="name"
									name="name" value="${carMarkup.name}" placeholder="ALL" autocomplete="off"
									required></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.pickup_date"/>
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm date" id="pickUpDate"
									name="pickUpDateTr" placeholder="ALL" value="${spysr:formatDate(carMarkup.pickUpDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}"  
									  value="All"   ></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.drop_off_date"/>
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm date" id="dropOffDate"
									name="dropOffDateTr" placeholder="ALL" autocomplete="off" value="${spysr:formatDate(carMarkup.dropOffDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}" ></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.pickup_city"/>
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm airportList" id="pickUpCity"
									name="pickUpCity" placeholder="ALL" value="${carMarkup.pickUpCity}" autocomplete="off" ></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.drop_off_city"/>
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="text" class="form-control input-sm airportList" id="dropOffCity"
									name="dropOffCity" placeholder="ALL" autocomplete="off" value="${carMarkup.dropOffCity}" ></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.is_accumulative" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
														<select class="form-control input-sm" name="isaccumulative"
									  autocomplete="off" required>
 									<option value="1"><s:text name="tgi.label.yes" /></option>
									<option value="0" selected="selected"><s:text name="tgi.label.no" /></option>
									 
								</select></div></div>
												</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.lable.is_fixedamount" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<select class="form-control input-sm" name="isfixedAmount"
												  autocomplete="off" >
			 									<option value="1"><s:text name="tgi.label.yes" /></option>
												<option value="0"><s:text name="tgi.label.no" /></option>
											 </select></div></div>
											</div>
										</div>
									</div>
									
					</div>	
							
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.markup_amount"/>
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" class="form-control input-sm" id="markupAmount"
									name="markupAmount" placeholder="amount" autocomplete="off" value="${carMarkup.markupAmount}"></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.position_of_markup"/>
											</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input type="number" class="form-control input-sm" name="positionMarkup"
									autocomplete="off" value="${carMarkup.positionMarkup}" ></div></div>
											</div>
										</div>
									</div>
										
									
					</div>	
							<div class=" row">
									<div class="col-md-6">
											<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_startdate" />
												</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" id="datepicker_PromofareStart"
								class="form-control input-sm" name="promofareStartDateTr" placeholder="ALL" value="${spysr:formatDate(carMarkup.promofareStartDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}" ></div></div>
												</div>
											</div>
										</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.promofare_enddate" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
													<input type="text" id="datepicker_PromofareEnd"
								class="form-control input-sm" name="promofareEndDateTr"  placeholder="ALL" value="${spysr:formatDate(carMarkup.promofareEndDate,'yyyy-MM-dd hh:mm:ss.S', 'MMM-d-yyyy')}" >
											</div></div>
										</div>
									</div>
										
									
					</div>
					<div class=" row">
									<div class="col-md-6">
										<label class="form-control-label" for="prependedInput">SIPP
											</label>
										<div class="controls"><div class="form-group"><div class="">
											<input type="text" class="form-control input-sm" id="sipp"
												name="sipp" placeholder="ALL" value="${carMarkup.sipp}"
												  value="All"></div></div>
										</div>
									</div>
									
					</div>	
                                    
                                    <div class="form-group table-btn">
                                        <div class="col-sm-10 col-sm-offset-2 ">
                                            <!-- <div class="pull-left"><input type="reset" class="btn btn-danger" type="submit" value="Clear Form"></div> -->
                                            <div class="pull-right">
                                                
                                              <input type="hidden" name="id" value="${carMarkup.id}">
                                                <input type="hidden" name="createdbyUserId" value="${carMarkup.createdbyUserId}" />
                                               <input type="hidden" name="createdbyCompanyId" value="${carMarkup.createdbyCompanyId}" />
                                              <input type="hidden" name="createDate" value="${carMarkup.createdDate}" />
                                                <button class="btn btn-danger" type="reset"><s:text name="tgi.label.reset" /></button>
                                                <button class="btn btn-primary " type="submit">
                                                    <s:text name="tgi.label.update" /></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form><br><br><br>
 </div></div>
							<br>
                                </div>
                            </div>
                        </div></div></div>
             </section>
                         <!--bootstrap vadidator  -->
                 <script type="text/javascript">
$(document).ready(function() {
$('#car-markup-update')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				
				configData : {
					message : 'ConfigData Role is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a ConfigData  '
						},
					}
				},
				name : {
					message : 'MarkUp Name is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a MarkUp Name'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'MarkUp Name  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				sipp : {
					message : 'sipp is not valid',
					validators : {
						 notEmpty : {
							message : 'Select enter a sipp'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'sipp  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
		
				isaccumulative : {
					message : 'isaccumulative  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a isaccumulative'
						},
					}
				},
				isfixedAmount : {
					message : 'IsfixedAmount  Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please Select a IsfixedAmount'
						},
					}
				},
				markupAmount : {
					message : 'Amount Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Amount'
						},
						  numeric: {
	                            message: 'The value is not a number',
	                            // The default separators
	                            thousandsSeparator: '',
	                            decimalSeparator: '.'
	                        }
						
					}
				},
				positionMarkup : {
					message : 'positionMarkup Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a position markup'
						},
					}
				},
				pickUpCity : {
					message : 'City Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a City'
						},
					}
				},
				dropOffCity : {
					message : 'City Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a City'
						},
					}
				},
					
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {
	/* notifySuccess(); */
	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>
<script type="text/javascript">
$(document).ready(function()
	{
		$("#configData").val($("#configDataHdn").val());		
	});

</script>
<script>
                	  $(".airportList").autocomplete({
                          minChars: 3,
                          source: "city_autocomplete"
          										});
						</script>
            
        <!--ADMIN AREA ENDS-->

  	   <s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>