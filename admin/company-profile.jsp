<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">

<style>
hr.profile-hr {
    margin-top: 10px;
    margin-bottom: -2px;
    border-top: 2px solid #eeeeee;
    margin-left: 160px;
}
.ms-choice {
    height: 30px !important;
    font-size: 12px !important;
    }

</style>
<!--************************************
        MAIN ADMIN AREA
    ****************************************-->

<!--ADMIN AREA BEGINS-->
<section class="wrapper container-fluid">

	<div class="">
		<div class="">
			<div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						Company Profile 
					</h5>
					<s:if test="%{((#session.User.userRoleId.isSuperUser()|| #session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
					<div class="set pull-right">
                      <div class="btn-group">
					  	<a class="btn btn-xs btn-outline-success pull-right mr-1" href="addCompany"> <i class="fa fa-plus"></i> New Company </a>
					</div>
					  <div class="btn-group">
						<a class="btn btn-xs btn-outline-success" href="companyList"><span class="fa fa-arrow-left"></span> Back To List </a>
				    </div>
                   </div>
                   </s:if>
				</div>
				<div class="row">
				<div class="col-md-12 mt-2">
					<div class="profile-timeline-card" style="margin-top: 40px;">
						<c:choose>
							<c:when test="${companyProfile.imagePath != null}">
								<img class="timeline-img-company pull-left" src="<s:url action='getImageByPath?imageId=%{companyProfile.imagePath}'/>">
							</c:when>
							<c:otherwise>
								<img class="timeline-img-company pull-left" src="admin/img/no-product-image.png">
							</c:otherwise>
						</c:choose>
						<form id="imageUploadForm" method="post" enctype="multipart/form-data">
							<label class="btn-bs-file btn btn-xs btn-outline-success pull-right mr-1" data-toggle="tooltip" data-placement="top" title="Change Profile Image">Browse
			                	<input type="file" name="uploadCompanyFile" onchange="companyProfileUploadFn(${companyProfile.id});" />
		          			</label>
	            		</form>
						<div class="">
							<h4 class="mt-2 mb-4">
								<span> <b class="blue-grey-text"><s:property value="companyProfile.companyName" /></b></span>
							</h4>

						</div>
						<hr class="profile-hr">
						<div class="timeline-footer row" style="margin-left: 132px;">
							<div class="col-md-8">
								<div class="col-md-2 col-xs-6 mb-2 mt-2">
									<span class="touroxy-form-element_label">Mail<br> 
									<span class="spysr-form-element_data">
									<a href="https://mail.google.com/mail/?view=cm&fs=1&to=${companyProfile.email}">
									<s:property value="companyProfile.email" />
									</a>
									</span> </span>
								</div>
								<div class="col-md-2 col-xs-6 mb-2 mt-2">
									<span class="touroxy-form-element_label"> Phone 1 <br> 
									<span class="spysr-form-element_data">
									<a href="https://api.whatsapp.com/send?phone=${companyProfile.mobile}&text=hi ${companyProfile.companyName}">
									${companyProfile.mobile != null && companyProfile.mobile !='' ? companyProfile.mobile:'<span class="text-warning">N/A</span>'}
									</a>
									</span> </span>
								</div>
								<div class="col-md-2 col-xs-6 mb-2 mt-2">
									<span class="touroxy-form-element_label"> Phone 2<br> 
									<span class="spysr-form-element_data">${companyProfile.companyDetail.phone != null && companyProfile.companyDetail.phone !='' ? companyProfile.companyDetail.phone:'<span class="text-warning">N/A</span>'}</span> </span>
								</div>
								<div class="col-md-2 col-xs-6 mb-2 mt-2">
									<span class="touroxy-form-element_label">Country<br> <span class="spysr-form-element_data">${companyProfile.companyDetail.countryName != null && companyProfile.companyDetail.countryName != '' ?companyProfile.companyDetail.countryName:'<span class="text-warning">N/A</span>'}</span> </span>
								</div>
							</div>
						</div>
					</div>
					</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="profile-timeline-card">
								<div class="dropdown pull-right">
									<button class="btn btn-xs btn-outline-success" type="button" data-toggle="modal" data-target="#edit_company"> <i class="fa fa-pencil"></i> Edit Company
									</button>
								</div>
								<div class="">
									<p class="mt-2 mb-3">
										<span class="font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Company</b></span>
									</p>
								</div>
								<hr>
								<div class="timeline-footer row">
									<div class="col-md-6 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Company Name :  <br>
												<span class="spysr-form-element_data"><s:property value="companyProfile.companyName" /></span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Email  <br>
												<span class="spysr-form-element_data"><s:property value="companyProfile.email" /></span> </span>
										</div>
									</div>
									<div class="row">
									<div class="col-md-3 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Language  <br>
												<span class="spysr-form-element_data"><s:property value="companyProfile.language" /></span> </span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Parent Company Id :  <br>
												<span class="spysr-form-element_data"><s:property
														value="companyProfile.parentCompanyUserId" /></span> </span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Created on :  <br>
												<span class="spysr-form-element_data"><s:property value="companyProfile.createDate" /></span>
											</span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Modified On :  <br>
												<span class="spysr-form-element_data"><s:property value="companyProfile.modifyDate" /></span>
											</span>
										</div>
									</div>
									</div>
									<div class="row">
									<div class="col-md-6 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Company Tagline<br>
												<span class="spysr-form-element_data">${companyProfile.companyDetail.companyTagline != null && companyProfile.companyDetail.companyTagline != '' ?companyProfile.companyDetail.companyTagline:'<span class="text-warning">N/A</span>'}</span>
											</span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Address<br>
												<span class="spysr-form-element_data">${companyProfile.companyDetail.address != null && companyProfile.companyDetail.address != '' ?companyProfile.companyDetail.address:'<span class="text-warning">N/A</span>'}</span>
											</span>
										</div>
									</div>
									</div>
									<div class="row">
									<div class="col-md-3 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> City<br>
												<span class="spysr-form-element_data">${companyProfile.companyDetail.city != null && companyProfile.companyDetail.city != '' ?companyProfile.companyDetail.city:'<span class="text-warning">N/A</span>'}</span>
											</span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Zip<br>
												<span class="spysr-form-element_data">${companyProfile.companyDetail.zipCode != null && companyProfile.companyDetail.zipCode != '' ?companyProfile.companyDetail.zipCode:'<span class="text-warning">N/A</span>'}</span>
											</span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> State <br>
												<span class="spysr-form-element_data">${companyProfile.companyDetail.state != null && companyProfile.companyDetail.state != '' ?companyProfile.companyDetail.state:'<span class="text-warning">N/A</span>'}</span>
											</span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Country<br>
												<span class="spysr-form-element_data">${companyProfile.companyDetail.countryName != null && companyProfile.companyDetail.countryName != '' ?companyProfile.companyDetail.countryName:'<span class="text-warning">N/A</span>'}</span>
											</span>
										</div>
									</div>
									</div>
									<div class="col-md-12 col-xs-12 col-sm-6 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Descritpion :  <br>
												<span class="spysr-form-element_data">${companyProfile.companyDetail.companyDescription}</span> </span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Email Sent Count  <br>
												<c:choose>
								                <c:when test="${emailCountStatusList.size() > 0}">
								                <span class="spysr-form-element_data">${emailCountStatusList.size()} </span>
								                <span class="link">
								                <a data-email-content="popover-content" data-placement="left" data-toggle="popover" title="Email Sent History" data-container="body" data-html="true" href="#!" id="login" style="cursor: pointer;" class="">times
								                </a>
								                </span>
								                </c:when>
								                <c:otherwise>
													<span class="spysr-form-element_data">0 times</span>														                
								                </c:otherwise>
								                </c:choose>
											 </span>
										</div>
										<!-- email history popover -->
										<div id="popover-content" class="hidden">
								        <div class="row">
								        <table class="table table-bordered table-striped-column table-hover mb-0">
						                               <thead>
						                               <tr class="small-trip-table">
											<th>SNo</th>
											<th>Mail Status</th>
											<th>Send At</th>
											</tr>
										</thead>
											<tbody>
											<c:forEach items="${emailCountStatusList}" var="emailData" varStatus="estatus">
											<tr class="small-trip-table">
											<td>${estatus.count}</td>
											<td>
											<c:choose>
											<c:when test="${emailData.mailStatus == 1}">
											<span class="text-success">Success</span>
											</c:when>
											<c:when test="${emailData.mailStatus == 0}">
											<span class="text-warning">Pending</span>
											</c:when>
											<c:when test="${emailData.mailStatus == -1}">
											<span class="text-danger">Failed</span>
											</c:when>
											</c:choose>
											</td>
											<td>
											${spysr:formatDate(emailData.createdDate,'yyyy-MM-dd HH:mm:ss', 'MMM/dd/yyyy hh:mm a')}
											</td>
											</tr>
											</c:forEach>
										</tbody></table>
								        </div>
									</div>
										
									</div>
									 
								</div>
							</div>
							<div class="profile-timeline-card" id="company_social_url">
								<div class="dropdown pull-right">
									<button class="btn btn-xs btn-outline-success" type="button" data-toggle="modal" data-target="#edit_company_social_profile"> <i class="fa fa-pencil"></i> Edit Socal Url
									</button>
								</div>
								<div class="">
									<p class="mt-2 mb-3">
										<span class="font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Social Contact</b></span>
									</p>
								</div>
								<hr>
								<div class="timeline-footer row">
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Website<br>
												<span class="spysr-form-element_data">
												<a href="${companyProfile.companySocialProfile.websiteUrl}">
													${companyProfile.companySocialProfile.websiteUrl != null && companyProfile.companySocialProfile.websiteUrl != '' ? companyProfile.companySocialProfile.websiteUrl:'<span class="text-warning">N/A</span>'}
												</a>
												</span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Facebook<br>
												<span class="spysr-form-element_data">
												<a href="${companyProfile.companySocialProfile.facebookUrl}">
													${companyProfile.companySocialProfile.facebookUrl != null && companyProfile.companySocialProfile.facebookUrl != '' ? companyProfile.companySocialProfile.facebookUrl:'<span class="text-warning">N/A</span>'}
												</a>
												</span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Instagram<br>
												<span class="spysr-form-element_data">
												<a href="${companyProfile.companySocialProfile.instagramUrl}">
													${companyProfile.companySocialProfile.instagramUrl != null && companyProfile.companySocialProfile.instagramUrl != '' ? companyProfile.companySocialProfile.instagramUrl:'<span class="text-warning">N/A</span>'}
												</a>
												</span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Twitter<br>
												<span class="spysr-form-element_data">
												<a href="${companyProfile.companySocialProfile.twitterUrl}">
													${companyProfile.companySocialProfile.twitterUrl != null && companyProfile.companySocialProfile.twitterUrl != '' ? companyProfile.companySocialProfile.twitterUrl:'<span class="text-warning">N/A</span>'}
												</a>
												</span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Linkedin <br>
												<span class="spysr-form-element_data">
												<a href="${companyProfile.companySocialProfile.linkedinUrl}">
													${companyProfile.companySocialProfile.linkedinUrl != null && companyProfile.companySocialProfile.linkedinUrl != '' ? companyProfile.companySocialProfile.linkedinUrl:'<span class="text-warning">N/A</span>'}
												</a>
												</span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Youtube <br>
												<span class="spysr-form-element_data">
												<a href="${companyProfile.companySocialProfile.youtubeUrl}">
													${companyProfile.companySocialProfile.youtubeUrl != null && companyProfile.companySocialProfile.youtubeUrl != '' ? companyProfile.companySocialProfile.youtubeUrl:'<span class="text-warning">N/A</span>'}
												</a>
												</span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Skypee<br>
												<span class="spysr-form-element_data">
												<a href="skype:${companyProfile.companySocialProfile.skypeUrl}?chat">
												${companyProfile.companySocialProfile.skypeUrl != null && companyProfile.companySocialProfile.skypeUrl != '' ? companyProfile.companySocialProfile.skypeUrl:'<span class="text-warning">N/A</span>'}
												</a>
												</span> </span>
										</div>
									</div>

								</div>
							</div>
							<!-- Company Billing Info  -->
							<div class="profile-timeline-card" id="company_billing_info">
								<div class="dropdown pull-right">
									<button class="btn btn-xs btn-outline-success" type="button" data-toggle="modal" data-target="#edit_company_billing_info"> <i class="fa fa-pencil"></i> Edit Billing
									</button>
								</div>
								<div class="">
									<p class="mt-2 mb-3">
										<span class="font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Billing Info</b></span>
									</p>
								</div>
								<hr>
								<div class="timeline-footer row">
									<div class="col-md-4 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Brand Name<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.brandName != null && companyProfile.companyBillingInfo.brandName != '' ? companyProfile.companyBillingInfo.brandName:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-4 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Company Name<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.companyName != null && companyProfile.companyBillingInfo.companyName != '' ? companyProfile.companyBillingInfo.companyName:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-4 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Business Type <br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.businessType != null && companyProfile.companyBillingInfo.businessType != '' ? companyProfile.companyBillingInfo.businessType:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-4 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Billing Email <br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.displayEmail != null && companyProfile.companyBillingInfo.displayEmail != '' ? companyProfile.companyBillingInfo.displayEmail:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-4 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Display Phone 1<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.displayMobile != null && companyProfile.companyBillingInfo.displayMobile != '' ? companyProfile.companyBillingInfo.displayMobile:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-4 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Display Phone 1<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.displayPhone != null && companyProfile.companyBillingInfo.displayPhone != '' ? companyProfile.companyBillingInfo.displayPhone:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="row">
									<div class="col-md-3 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Registration Number<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.registrationNumber != null && companyProfile.companyBillingInfo.registrationNumber != '' ? companyProfile.companyBillingInfo.registrationNumber:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> GST Number<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.gstNumber != null && companyProfile.companyBillingInfo.gstNumber != '' ? companyProfile.companyBillingInfo.gstNumber:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> PAN Number<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.panNumber != null && companyProfile.companyBillingInfo.panNumber != '' ? companyProfile.companyBillingInfo.panNumber:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> CIN Number<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.cinNumber != null && companyProfile.companyBillingInfo.cinNumber != '' ? companyProfile.companyBillingInfo.cinNumber:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									</div>
									<div class="row">
									<div class="col-md-12 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> Address<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.address != null && companyProfile.companyBillingInfo.address != '' ? companyProfile.companyBillingInfo.address:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									</div>
									<div class="row">
									<div class="col-md-3 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label"> City<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.city != null && companyProfile.companyBillingInfo.city != '' ? companyProfile.companyBillingInfo.city:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Zip Code<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.zipCode != null && companyProfile.companyBillingInfo.zipCode != '' ? companyProfile.companyBillingInfo.zipCode:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">State<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.state != null && companyProfile.companyBillingInfo.state != '' ? companyProfile.companyBillingInfo.state:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-3 col-xs-12 mb-2 mt-2">
										<div class="border-bottom ">
											<span class="touroxy-form-element_label">Country Name<br>
												<span class="spysr-form-element_data">
												${companyProfile.companyBillingInfo.countryName != null && companyProfile.companyBillingInfo.countryName != '' ? companyProfile.companyBillingInfo.countryName:'<span class="text-warning">N/A</span>'}</span> </span>
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="profile-timeline-card">
								<div class="dropdown pull-right">
									<button class="btn btn-xs btn-outline-success" type="button" data-toggle="modal" data-target="#edit_company_services"> <i class="fa fa-pencil"></i> Edit Services
									</button>
								</div>
								<div class="">
									<p class="mt-2 mb-3">
										<span class="font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Company Services</b></span>
									</p>
								</div>
								<hr>
								<div class="timeline-footer row">
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom ">
										<span class="touroxy-form-element_label"> Company Level  <br>
											<span class="spysr-form-element_data"><c:choose>
													<c:when test="${companyProfile.companyVisibilityLevel.b2b}">B2B</c:when>
													<c:when test="${companyProfile.companyVisibilityLevel.b2c}">B2C</c:when>
													<c:when
														test="${companyProfile.companyVisibilityLevel.b2b2c}">B2B2C</c:when>
													<c:when
														test="${companyProfile.companyVisibilityLevel.b2b2b2c}">B2B2B2C</c:when>
												</c:choose></span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom ">
										<span class="touroxy-form-element_label"> Selling Platform  <br>
											<span class="spysr-form-element_data">
											<c:choose>
													<c:when test="${companyProfile.companySellingPlatform.ibe}">IBE</c:when>
													<c:when test="${companyProfile.companySellingPlatform.api}">API</c:when>
													<c:when test="${companyProfile.companySellingPlatform.whitelabel}">White Label</c:when>
													<c:when test="${companyProfile.companySellingPlatform.admin}">Admin Panel</c:when>
													<c:when test="${companyProfile.companySellingPlatform.ownWebsite}">Own Website</c:when>
											</c:choose>
											</span>
										</span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
										<c:forEach items="${companyBookableServicesMap}" var="bookable" varStatus="j">
										<c:if test="${bookable.value!=null && bookable.value.size()>0}">
											<div class="touroxy-form-element_label">${bookable.key} <br>
											<div class="">
												<ul class="inline-list">
													<c:forEach items="${bookable.value}" var="companyServiceInner" varStatus="j">
														<li class="roles_css"><span class="spysr-form-element_data"><i class="fa fa-check chack-list"></i> &nbsp; ${companyServiceInner}</span></li>
													</c:forEach>
												</ul>
												</div>
											</div>
										</c:if>
										</c:forEach>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
										<c:forEach items="${companyCMSServicesMap}" var="cms" varStatus="j">
										<c:if test="${cms.value!=null && cms.value.size()>0}">
											<div class="touroxy-form-element_label">${cms.key} <br>
											<div class="">
												<ul class="inline-list">
													<c:forEach items="${cms.value}" var="companyServiceInner" varStatus="j">
														<li class="roles_css"><span class="spysr-form-element_data"><i class="fa fa-check chack-list"></i> &nbsp; ${companyServiceInner}</span></li>
													</c:forEach>
												</ul>
												</div>
											</div>
										</c:if>
										</c:forEach>
									</div>
								</div>
							</div>
						</div> 
						<div class="col-md-4">
							<div class="profile-timeline-card">
								<div class="dropdown pull-right">
									<button class="btn btn-xs btn-outline-success" type="button" data-toggle="modal" data-target="#edit_bank_account"> <i class="fa fa-pencil"></i> Edit Bank Info
									</button>
								</div>
								<div class="">
									<p class="mt-2 mb-3">
										<span class="font-icon"><i class="fa fa-building"></i></span> <span> <b class="blue-grey-text">Bank Accounts</b></span>
									</p>
								</div>
								<hr>
								<div class="timeline-footer row">
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom ">
										<span class="touroxy-form-element_label"> Bank Name<br>
											<span class="spysr-form-element_data">
											${companyProfile.companyBankAccountDetail.bankName != null && companyProfile.companyBankAccountDetail.bankName != '' ?companyProfile.companyBankAccountDetail.bankName: '<span class="text-warning"><span class="text-warning">N/A</span></span>'}
											</span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom ">
										<span class="touroxy-form-element_label"> Branch IFSC<br>
											<span class="spysr-form-element_data">
											${companyProfile.companyBankAccountDetail.branchIfscCode != null && companyProfile.companyBankAccountDetail.branchIfscCode != '' ?companyProfile.companyBankAccountDetail.branchIfscCode: '<span class="text-warning"><span class="text-warning">N/A</span></span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom ">
										<span class="touroxy-form-element_label"> Beneficiary Name<br>
											<span class="spysr-form-element_data">
											${companyProfile.companyBankAccountDetail.beneficiaryName != null && companyProfile.companyBankAccountDetail.beneficiaryName != '' ?companyProfile.companyBankAccountDetail.beneficiaryName: '<span class="text-warning"><span class="text-warning">N/A</span></span>'}</span> </span>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 mb-2 mt-2">
									<div class="border-bottom ">
										<span class="touroxy-form-element_label"> Account Number<br>
											<span class="spysr-form-element_data">
											${companyProfile.companyBankAccountDetail.accountNumber != null && companyProfile.companyBankAccountDetail.accountNumber != '' ?companyProfile.companyBankAccountDetail.accountNumber: '<span class="text-warning"><span class="text-warning">N/A</span></span>'}</span> </span>
										</div>
									</div>
								</div>
							</div>
						</div> 
				</div>
<!-----------------------------------------------------------------------------------------------------------Edit Company profile-------->
<div id="company modals">
					<div class="modal fade" id="edit_company" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Edit Company Details</h4>
									<button type="button" class="close slds-modal__close"
										data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg"
											width="16" alt="Copy to clipboard">
									</button>
								</div>
								<form id="updateCompanyDetailForm" name="updateCompanyDetailForm" method="post" class="form-horizontal">
									<input type="hidden" name="id" value="<s:property value="companyProfile.id"/>">
									<div class="modal-body">
										<div class="row mt-2">
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Company Name</label>
											<div class="col-md-8">
												<div class="controls">
														<input type="text" class="form-control input-sm" id="company" name="companyName" autocomplete="off" value="${companyProfile.companyName}" required="required">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Company Tagline</label>
											<div class="col-md-8">
												<div class="controls">
														<input type="text" class="form-control input-sm" id="companyTagline" name="companyDetail.companyTagline" autocomplete="off" value="${companyProfile.companyDetail.companyTagline}">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Email</label>
												<div class="col-md-8">
												<div class="controls">
														<input type="email" class="form-control input-sm" name="email" id="email" value="<s:property value="companyProfile.email"/>" autocomplete="off" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Mobile</label>
												<div class="col-md-8">
												<div class="controls">
														<input type="tel" class="form-control input-sm" name="mobile" id="mobile" value="<s:property value="companyProfile.mobile"/>" autocomplete="off" required="required">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Telphone</label>
												<div class="col-md-8">
												<div class="controls">
														<input type="tel" class="form-control input-sm" name="companyDetail.phone" id="phone" value="${companyProfile.companyDetail.phone}" autocomplete="off">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Language</label>
												<div class="col-md-8">
												<div class="controls">
														<select class="form-control input-sm" name="language" id="Language" >
															<c:forEach items="${LanguageList}" var="itemType">
																			<option value="${itemType.language}" ${itemType.language == companyProfile.language ? 'selected': ''}>${itemType.language}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Description</label>
												<div class="col-md-8">
												<div class="controls">
														<textarea class="form-control input-sm" id="description" name="companyDetail.companyDescription" rows="3">${companyProfile.companyDetail.companyDescription}</textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Address</label>
												<div class="col-md-8">
												<div class="controls">
															<textarea name="companyDetail.address" id="address"  class="form-control">${companyProfile.companyDetail.address}</textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>City</label>
												<div class="col-md-8">
												<div class="controls">
														<input type="text" class="form-control input-sm" name="companyDetail.city" id="city" value="<s:property value="companyProfile.companyDetail.city"/>" autocomplete="off">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Zip Code</label>
												<div class="col-md-8">
												<div class="controls">
														<input type="text" class="form-control input-sm" name="companyDetail.zipCode" id="zip_code" value="<s:property value="companyProfile.companyDetail.zipCode"/>" autocomplete="off">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>State</label>
											<div class="col-md-8">
												<div class="controls">
														<select class="form-control input-sm" name="companyDetail.state" id="state" >
														<option selected  value="">Select State</option>
														<c:forEach items="${statesList}" var="states">
															<option value="${states.state}" ${states.state == companyProfile.companyDetail.state ?'selected':''} >${states.state}</option>
														</c:forEach>
												</select>
													</div>
												</div>
											</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Country</label>
												<div class="col-md-8">
												<div class="controls">
														<select class="form-control input-sm" name="companyDetail.countryName" id="country" required>
															<s:iterator value="CountryList">
																<option value="<s:property value="c_name"/>" ${c_name == companyProfile.companyDetail.countryName ?'selected':''}><s:property value="c_name"></s:property></option>
															</s:iterator>
														</select>
													</div>
												</div>
											</div>
										</div>
										</div>
										<div class="modal-simple-footer">
											<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
											<button type="submit" class="btn btn-success" id="update_company_detail_btn">Save</button>
										</div>
								</form>
							</div>

						</div>
					</div>
					<!-- Company Social Profile  -->
					<div class="modal fade" id="edit_company_social_profile" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Edit Social Url Field</h4>
									<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="16" alt="Copy to clipboard">
									</button>
								</div>
								<form id="updateCompanySocalForm" name="updateCompanySocalForm" method="post" class="form-horizontal">
									<input type="hidden" name="companyId" value="<s:property value="companyProfile.id"/>">
									<div class="modal-body">
										<div class="row">
											<div class="form-group">
												<label class="form-control-label col-md-4" for="website-url"><span class="text-danger"></span>Website</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="tel" class="form-control input-sm" name="websiteUrl" id="website-url" autocomplete="off" value="${companyProfile.companySocialProfile.websiteUrl}">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="facebook-url"><span class="text-danger"></span>Facebook</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="tel" class="form-control input-sm" name="facebookUrl" id="facebook-url" autocomplete="off" value="${companyProfile.companySocialProfile.facebookUrl}">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="instagram-url"><span class="text-danger"></span>Instagram</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="tel" class="form-control input-sm" name="instagramUrl" id="instagram-url" autocomplete="off" value="${companyProfile.companySocialProfile.instagramUrl}">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="twitter-url"><span class="text-danger"></span>Twitter</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="tel" class="form-control input-sm" name="twitterUrl" id="twitter-url" autocomplete="off" value="${companyProfile.companySocialProfile.twitterUrl}">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="linkedin-url"><span class="text-danger"></span>Linkedin</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="tel" class="form-control input-sm" name="linkedinUrl" id="linkedin-url" autocomplete="off" value="${companyProfile.companySocialProfile.linkedinUrl}">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="youtube-url"><span class="text-danger"></span>Youtube</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="tel" class="form-control input-sm" name="youtubeUrl" id="youtube-url" autocomplete="off" value="${companyProfile.companySocialProfile.youtubeUrl}">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="form-control-label col-md-4" for="skype-url"><span class="text-danger"></span>Skype</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="tel" class="form-control input-sm" name="skypeUrl" id="skype-url" autocomplete="off" value="${companyProfile.companySocialProfile.skypeUrl}">
													</div>
												</div>
											</div>
										</div>
										</div>
										<div class="modal-simple-footer">
											<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
											<button type="submit" class="btn btn-success" id="update_company_socal_btn">Save</button>
										</div>
								</form>
							</div>

						</div>
					</div>
					<!-- Edit company services modal -->
					<div class="modal fade" id="edit_company_services" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Edit Services Field</h4>
									<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="14" alt="Copy to clipboard">
									</button>
								</div>
								<form id="updateCompanyServicesForm" name="updateCompanyServicesForm" method="post" class="form-horizontal">
									<input type="hidden" name="id" value="<s:property value="companyProfile.id"/>">
									<div class="modal-body">
										<div class="row mt-2">
										<s:if test="%{#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isAdmin()}">
										<div class="row">
											<div class="form-group">
													<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Visbility Level</label>
												<div class="col-md-8">
													<div class="controls">
															<select class="form-control input-sm" name="companyVisibilityType" id="companyVisibilityType">
																<option value="">Select Service</option>
																<option ${companyProfile.companyVisibilityLevel.b2b? 'selected="true"' : '' }
																	value="b2b">B2B</option>
																<option ${companyProfile.companyVisibilityLevel.b2c? 'selected="true"' : '' }
																	value="b2c">B2C</option>
																<option ${companyProfile.companyVisibilityLevel.b2b2c? 'selected="true"' : '' }
																	value="b2b2c">B2B2C</option>
																<option ${companyProfile.companyVisibilityLevel.b2b2b2c? 'selected="true"' : '' }
																	value="b2b2b2c">B2B2B2C</option>
															</select>
														</div>
													</div>
												</div>
											<div class="form-group">
													<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Selling Platform</label>
												<div class="col-md-8">
													<div class="controls">
															<select class="form-control input-sm" name="companySellingPlatformType" id="company_selling_platform">
																<option value="">Select Service</option>
																<option ${companyProfile.companySellingPlatform.ibe? 'selected="true"' : '' } value="ibe">IBE</option>
																<option ${companyProfile.companySellingPlatform.whitelabel? 'selected="true"' : '' } value="whitlLabel">WhiteLabel</option>
																<option ${companyProfile.companySellingPlatform.api? 'selected="true"' : '' } value="api">API</option>
																<option ${companyProfile.companySellingPlatform.admin? 'selected="true"' : '' } value="admin">Admin</option>
																<option ${companyProfile.companySellingPlatform.ownWebsite? 'selected="true"' : '' } value="ownWebsite">Own Website</option>
															</select>
														</div>
													</div>
												</div>
										</div>
										<div class="row">
										<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Bookable Service</label>
											<div class="col-md-8">
												<div class="controls">
														<select class="input-md" id="company_bookable_service" name="companyBookableServiceArray" multiple="multiple">
															<c:forEach items="${bookableServiceListMap}" var="bookingService">
																	<option value="${bookingService.key}">${bookingService.value}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>CMS Services</label>
											<div class="col-md-8">
												<div class="controls">
														<select class="input-md" id="company_cms_services" name="companyCMSServiceArray" multiple="multiple">
															<c:forEach items="${cmsServiceListMap}" var="cmsService">
																	<option value="${cmsService.key}">${cmsService.value}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
											<%-- <div class="form-group">
														<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Wallet Type</label>
														<div class="col-md-8">
														<div class="controls">
																<input type="hidden" id="walletType" value="<s:property value="%{companyProfile.agentWallet.walletType}"/>">
																<input type="hidden" id="walletBalanceRange" value="<s:property value="%{companyProfile.agentWallet.walletBalanceRange}"/>">
																<s:if test="%{#session.User.userRoleId.isSuperUser()||#session.Company.companyRole.isDistributor()}">
																		<select class="form-control input-sm" name="typeOfWallet" id="typeOfWallet" required>
																			<s:if test="%{#session.User.agentWallet.walletType == 'Prepaid'}">
																				<option value="Prepaid" selected="selected"><s:text name="tgi.label.prepaid" /></option>
																				<option value="Postpaid"><s:text name="tgi.label.postpaid" /></option>
																			</s:if>
																			<s:else>
																				<option value="Postpaid" selected="selected"><s:text name="tgi.label.postpaid" /></option>
																				<option value="Prepaid"><s:text name="tgi.label.prepaid" /></option>
																			</s:else>
																		</select>
																</s:if>
																<s:else>
																		<select class="form-control input-sm" name="typeOfWallet" id="typeOfWallet" required>
																			<s:if test="%{#session.User.agentWallet.walletType == 'Prepaid'}">
																				<option value="Prepaid" selected><s:text name="tgi.label.prepaid" /></option>
																			</s:if>
																			<s:else>
																				<option value="Postpaid"><s:text name="tgi.label.postpaid" /></option>
																			</s:else>
																		</select>
																</s:else>
														</div>
														</div>
													</div> --%>
										</div>
										</s:if>
										</div>
										</div>
										<div class="modal-simple-footer">
											<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
											<button type="submit" class="btn btn-success" id="update_company_services_btn">Save</button>
										</div>
								</form>
							</div>

						</div>
					</div>
					<!-- Edit company bank account detail-->
					<div class="modal fade" id="edit_bank_account" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Edit Bank Info Field</h4>
									<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="14" alt="Copy to clipboard">
									</button>
								</div>
								<form id="updateCompanyBankInfoForm" name="updateCompanyBankInfoForm" method="post" class="form-horizontal">
									<input type="hidden" name="companyId" value="<s:property value="companyProfile.id"/>">
									<div class="modal-body">
										<div class="row mt-2">
										<s:if test="%{#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isAdmin()}">
										<div class="row">
											<div class="form-group">
													<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Bank Name</label>
												<div class="col-md-8">
													<div class="controls">
															<input type="text" name="bankName" id="bank-name" class="form-control" autocomplete="true" value="${companyProfile.companyBankAccountDetail.bankName}">
														</div>
													</div>
											</div>
											<div class="form-group">
													<label class="form-control-label col-md-4" for="branch-ifsc-code"><span class="text-danger"></span>Bank IFSC</label>
												<div class="col-md-8">
													<div class="controls">
															<input type="text" name="branchIfscCode" id="branch-ifsc-code" class="form-control" autocomplete="true" value="${companyProfile.companyBankAccountDetail.branchIfscCode}">
														</div>
													</div>
											</div>
											<div class="form-group">
													<label class="form-control-label col-md-4" for="beneficiary-name"><span class="text-danger"></span>Beneficiary Name</label>
												<div class="col-md-8">
													<div class="controls">
															<input type="text" name="beneficiaryName" id="beneficiary-name" class="form-control" autocomplete="true" value="${companyProfile.companyBankAccountDetail.beneficiaryName}">
														</div>
													</div>
											</div>
											<div class="form-group">
													<label class="form-control-label col-md-4" for="account-number"><span class="text-danger"></span>Account Number</label>
												<div class="col-md-8">
													<div class="controls">
															<input type="text" name="accountNumber" id="account-number" class="form-control" autocomplete="true" value="${companyProfile.companyBankAccountDetail.accountNumber}">
														</div>
													</div>
											</div>
										</div>
										</s:if>
										</div>
										</div>
										<div class="modal-simple-footer">
											<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
											<button type="submit" class="btn btn-success" id="update_company_bank_btn">Save</button>
										</div>
								</form>
							</div>

						</div>
					</div>
					<!-- Edit company billing modal -->
					<div class="modal fade" id="edit_company_billing_info" role="dialog">
						<div class="modal-dialog modal-lg">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Edit Billing Field</h4>
									<button type="button" class="close slds-modal__close" data-dismiss="modal">
										<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="14" alt="Copy to clipboard">
									</button>
								</div>
								<form id="updateCompanyBillingForm" name="updateCompanyBillingForm" method="post" class="form-horizontal">
									<input type="hidden" name="companyId" value="<s:property value="companyProfile.id"/>">
									<div class="modal-body">
										<div class="row mt-3">
										<div class="col-md-6">
										<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Brand Name</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="brand-name" name="brandName" value="${companyProfile.companyBillingInfo.brandName}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="comment"><span class="text-danger"></span>Company Name</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="company-name" name="companyName" value="${companyProfile.companyBillingInfo.companyName}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="business-type"><span class="text-danger"></span>Business Type</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="business-type" name="businessType" value="${companyProfile.companyBillingInfo.businessType}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="business-type"><span class="text-danger"></span>Billing Email</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="display-email" name="displayEmail" value="${companyProfile.companyBillingInfo.displayEmail}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="business-type"><span class="text-danger"></span>Registration No.</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="registration-number" name="registrationNumber" value="${companyProfile.companyBillingInfo.registrationNumber}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="business-type"><span class="text-danger"></span>GST Number</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="gst-number" name="gstNumber" value="${companyProfile.companyBillingInfo.gstNumber}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="business-type"><span class="text-danger"></span>PAN Number</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="pan-number" name="panNumber" value="${companyProfile.companyBillingInfo.panNumber}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="business-type"><span class="text-danger"></span>CIN Number</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="cin-number" name="cinNumber" value="${companyProfile.companyBillingInfo.cinNumber}" autocomplete="off" >
												</div>
												</div>
										</div>
										</div>
										<div class="col-md-6">
										<div class="form-group">
												<label class="form-control-label col-md-4" for="business-type"><span class="text-danger"></span>Billing Mobile</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="display-mobile" name="displayMobile" value="${companyProfile.companyBillingInfo.displayMobile}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="business-type"><span class="text-danger"></span>Billing Phone</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="display-phone" name="displayPhone" value="${companyProfile.companyBillingInfo.displayPhone}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="address"><span class="text-danger"></span>Billing Address</label>
												<div class="controls">
												<div class="col-md-8">
														<textarea class="form-control input-sm" id="address" name="address" autocomplete="off" >${companyProfile.companyBillingInfo.address}</textarea>
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for=city><span class="text-danger"></span>Billing City</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="city" name="city" value="${companyProfile.companyBillingInfo.city}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for=zip-code><span class="text-danger"></span>Zip Code</label>
												<div class="controls">
												<div class="col-md-8">
														<input type="text" class="form-control input-sm" id="zip-code" name="zipCode" value="${companyProfile.companyBillingInfo.zipCode}" autocomplete="off" >
												</div>
												</div>
										</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="state"><span class="text-danger"></span>Billing State</label>
											<div class="col-md-8">
												<div class="controls">
														<select class="form-control input-sm" name="state" id="state" >
														<option selected  value="">Select State</option>
														<c:forEach items="${statesList}" var="states">
															<option value="${states.state}" ${states.state == companyProfile.companyBillingInfo.brandName?'selected':''}>${states.state}</option>
														</c:forEach>
												</select>
													</div>
												</div>
											</div>
										<div class="form-group">
												<label class="form-control-label col-md-4" for="country"><span class="text-danger"></span>Billing Country</label>
												<div class="col-md-8">
												<div class="controls">
														<select class="form-control input-sm" name="countryName" id="country" required>
															<s:iterator value="CountryList">
																<option value="<s:property value="c_name"/>" ${c_name == companyProfile.companyBillingInfo.countryName ?'selected':''} >${c_name}</option>
															</s:iterator>
														</select>
													</div>
												</div>
											</div>
											</div>
										</div>
										</div>
										<div class="modal-simple-footer">
											<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
											<button type="submit" class="btn btn-success" id="update_company_billing_btn">Save</button>
										</div>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->

		</div>
	</div>

</section>
<script type="text/javascript">
	$(document).ready(
			function() {
				$("#company_bookable_service").multipleSelect({
					filter : true,
					width : '100%',
					height : '100%',
					placeholder : "Select a Services",
				});
				var arrayCompanyBookableService = '${companyProfile.companyBookableServiceData}'.split(',');
				console.debug(arrayCompanyBookableService);
				$("#company_bookable_service").multipleSelect("setSelects",arrayCompanyBookableService);

			});

	$(document).ready(function() {
		$("#company_cms_services").multipleSelect({
			filter : true,
			width : '100%',
			height : '100%',
			placeholder : "Select a CMS Services",
		});

		var arrayuserroleType = '${companyProfile.companyCMSServiceData}'.split(',');
		console.debug(arrayuserroleType);
		$("#company_cms_services").multipleSelect("setSelects", arrayuserroleType);

	});
/* save or update ajax */
 
$(document).ready(function() {
    $("#updateCompanyDetailForm").bootstrapValidator({
        feedbackIcons: {
            valid: "fa fa-check",
            invalid: "fa fa-remove",
            validating: "fa fa-refresh"
        },
        fields:{
        	mobile:{message:"Phone is not valid",validators:{notEmpty:{message:"Phone is a required field"}}},
        }
        
    }).on("error.form.bv", function(e) {}).on("success.form.bv", function(e) {
            e.preventDefault(), 
            notifySuccess();
            $.ajax({
                url: "companyProfileUpdate",
                type: "POST",
                dataType: "json",
                data: $("#updateCompanyDetailForm").serialize(),
                success: function(jsonData) {
                	if(jsonData.message.status == 'success'){
                		alertify.success(jsonData.message.message);
	  					setTimeout(location.reload.bind(location), 1000);
                	}
	  				else if(jsonData.message.status == 'input'){
	  					alertify.success(jsonData.message.message);}
	  				else if(jsonData.message.status == 'error'){
	  					alertify.success(jsonData.message.message);}
                        $("#update_company_detail_btn").removeAttr("disabled"), console.debug("button disabled ");
                    var s = $(e.target);
                    s.data("bootstrapValidator");
                    s.bootstrapValidator("disableSubmitButtons", !1).bootstrapValidator("resetForm", !0)
                },
                error: function(e, a, s) {
                    showModalPopUp("Company Detail can not be saved, Somthing error", "e")
                }
            })
    }).on("status.field.bv", function(e, a) {
        a.bv.getSubmitButton() && (console.debug("button disabled "), a.bv.disableSubmitButtons(!1))
    })
});

	$("#updateCompanySocalForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			url : "save_update_company_socal_link",
			type : "POST",
			dataType : 'json',
			data : $("#updateCompanySocalForm").serialize(),
			success : function(jsonData) {
				if(jsonData.message.status == 'success'){
            		alertify.success(jsonData.message.message);
  					setTimeout(location.reload.bind(location), 1000);
            	}
  				else if(jsonData.message.status == 'input'){
  					alertify.success(jsonData.message.message);}
  				else if(jsonData.message.status == 'error'){
  					alertify.success(jsonData.message.message);}
			},
			error : function(request, status, error) {
				alertify.error("company socal links can't be updated,please try again");
			}
		});
	});
	$("#updateCompanyBillingForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			url : "save_update_company_billing_info",
			type : "POST",
			dataType : 'json',
			data : $("#updateCompanyBillingForm").serialize(),
			success : function(jsonData) {
				if(jsonData.message.status == 'success'){
            		alertify.success(jsonData.message.message);
  					setTimeout(location.reload.bind(location), 1000);
            	}
  				else if(jsonData.message.status == 'input'){
  					alertify.success(jsonData.message.message);}
  				else if(jsonData.message.status == 'error'){
  					alertify.success(jsonData.message.message);}
			},
			error : function(request, status, error) {
				alertify.error("company billing info can't be updated,please try again");
			}
		});
	});
	$("#updateCompanyBankInfoForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			url : "save_update_company_bank_info",
			type : "POST",
			dataType : 'json',
			data : $("#updateCompanyBankInfoForm").serialize(),
			success : function(jsonData) {
				if(jsonData.message.status == 'success'){
            		alertify.success(jsonData.message.message);
  					setTimeout(location.reload.bind(location), 1000);
            	}
  				else if(jsonData.message.status == 'input'){
  					alertify.success(jsonData.message.message);}
  				else if(jsonData.message.status == 'error'){
  					alertify.success(jsonData.message.message);}
			},
			error : function(request, status, error) {
				alertify.error("company billing info can't be updated,please try again");
			}
		});
	});
	$("#updateCompanyServicesForm").submit(function(e) {
		e.preventDefault();
		$.ajax({
			url : "updateCompanyServices",
			type : "POST",
			dataType : 'json',
			data : $("#updateCompanyServicesForm").serialize(),
			success : function(jsonData) {
				if(jsonData.message.status == 'success'){
            		alertify.success(jsonData.message.message);
  					setTimeout(location.reload.bind(location), 1000);
            	}
  				else if(jsonData.message.status == 'input'){
  					alertify.success(jsonData.message.message);}
  				else if(jsonData.message.status == 'error'){
  					alertify.success(jsonData.message.message);}
			},
			error : function(request, status, error) {
				alertify.error("company services info can't be updated,please try again");
			}
		});
	});
	/*-------------------------------------------------*/
		function companyProfileUploadFn(companyId){
			 var formData = new FormData($("#imageUploadForm")[0]);
			    $.ajax({
			        url: "uploadCompanyProfileImage?id="+companyId,
			        type: 'POST',
			        data: formData,
			        async: false,
			        dataType: 'json',
			        success: function (jsonData) 
			        {
			        	if(jsonData.json.status == 'success'){
							$.notify({icon: 'fa fa-check',message: jsonData.json.message},{type: 'success'});
							window.location.reload();
							}
		  				else if(jsonData.message.status == 'error')
		  				{
		  					$.notify({icon: 'fa fa-times',message: jsonData.json.message},{type: 'danger'});
		  				}
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });	
		}
</script>
