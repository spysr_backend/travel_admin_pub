<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	   
        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->

            <section class="wrapper container-fluid">

                <div class="">
                    <div class="card1">
                        <div class="pnl">
                        <div class="hd clearfix">
					<h5 class="pull-left"><img class="clay" src="admin/img/svg/config.svg" width="26" alt="Higest"> &nbsp; Company Configurations List</h5>
					<div class="set pull-right hidden-xs" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
				</div>
                                <form action="companyConfigList" class="filter-form">
                                <div class="clearfix" id="filterDiv" style="display:none;" >
                                <div class="form-group" style="margin-top:15px;">
                              	 <s:if test="%{#session.Company!=null}">
									<s:if test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
                                 <div class="col-md-2">
                                <input type="hidden" id="HdncompanyTypeShowData" value="" />
                                <select name="companyTypeShowData" id="companyTypeShowData" class="form-control">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="my"><s:text name="tgi.label.my_self" /></option>
                                <s:if test="%{#session.Company!=null}">
									<s:if test="%{#session.Company.companyRole.isSuperUser()}">
	                                <option value="distributor"><s:text name="tgi.label.distributor" /></option>
	                                </s:if>
                                </s:if>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div>
                                </s:if>
                                </s:if>
                              	<div class="col-md-2 col-sm-6 form-group">
                                <input type="text" name="startDate" id="datepicker_startDate" value="${startDate}"
												placeholder="Start Date...."
												class="form-control search-query date1" />
												
                                </div>
                                <div class="col-md-2 col-sm-6 form-group">
                                <input type="text" name="endDate" id="datepicker_endDate" value="${endDate}"
												placeholder="End Date...."
												class="form-control hasdatepicker date2" />
                                </div>
                              
                                <div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="companyName" id="companyname-json"
												placeholder="Company Name...."
												class="form-control search-query" />
								</div>
                                <div class="col-md-2 col-sm-6 form-group">
                                <input type="text" name="companyType" id="companytype-json"
												placeholder="Company Type...."
												class="form-control search-query" />
                                </div>
                              <div class="col-md-2 col-sm-6 form-group">
                                <input type="text" name="configname" id="configname-json"
												placeholder="Config Name...."
												class="form-control hasdatepicker" />
                                </div> 
                              	
                              	<!-- <div class="col-md-2 col-sm-6">
                                <input type="text" name="configType" id="configType-json"
												placeholder="Config Type...."
												class="form-control hasdatepicker" />
                                </div> -->
                                
                                <div class="col-md-2 col-sm-6 form-group">
								<select name="configType" id="configType" class="form-control">
								<option value="" selected="selected"><s:text name="tgi.label.select_config_type" /></option>
                                <option value="B2B">B2B</option>
                             	<option value="B2C">B2C</option>
                                <option value="API">API</option>
                                <option value="WL"><s:text name="tgi.label.whitelable" /></option>
                                </select>
								</div>
                                
                             	<div class="col-md-2 col-sm-6 form-group">
                                <select name="rateTypeKey" id="rateTypeKey" class="form-control">
                               	<option value="" selected="selected">Select</option>
                                <option value="flight">RateTypeFlight</option>
                                <option value="hotel">RateTypeHotel</option> 
                                <option value="car">RateTypeCar</option> 
                                <option value="tour">RateTypeTour</option> 
                                </select>
                                </div>
                             	<div class="col-md-2 col-sm-6 form-group">
                                <select name="rateType" id="rateType" class="form-control">
                               	<option value="" selected="selected">Select Rate Type</option>
                                <option value="commission">Commission</option>
                                <option value="net">Net</option> 
                                </select>
                                </div>
                                
                                <div class="col-md-1">
								<div class="form-group">
								<input type="reset" class="btn btn-danger btn-block" id="configreset" value="Clear">
								</div>
								</div>
									<div class="col-md-1">
									<div class="form-group">
									<input type="submit" class="btn btn-info btn-block" value="Search" />
								</div>
								</div>
                                </div></div>
                                </form>
<div class="cnt cnt-table">
<div class="dash-table">
		<div class="box ">
		<div class="fw-container">
		<div class="fw-body">
			<div class="content mt-0 pt-2">
				<div id="example_wrapper" class="dataTables_wrapper table-responsive">
										<table id="example" class="display dataTable responsive nowrap"  role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
                                        <thead>
                                        <tr class="table-sub-header">
                                        	<th colspan="15" class="noborder-xs">
                                        	<div class="pull-right">   
                                        	<div class="btn-group"> 
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Company Config">
											  <a class="btn btn-sm btn-default" href="addNewCompanyConfig" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class="">&nbsp;New&nbsp;</span></a> 
											</div>
											<div class="dropdown pull-left" style="margin-right:5px;">
											  <a class="btn btn-sm btn-default " href="#" >&nbsp;Import&nbsp;</a> 
											</div>
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a  class="btn btn-sm btn-default filterBtn" data-toggle="dropdown"  title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
										<tr class="border-radius border-color" role="row">
                                                    <th data-priority="1" style="width: 30px"></th>
                                                    <th data-priority="2" style="" class="col-md-1">Config Number</th>
                                                    <th data-priority="2">Company Name</th>
                                                    <th data-priority="3">Company Type</th>
                                                    <th data-priority="4">Config Name</th>
                                                    <th data-priority="4">Config Type</th>
                                                    <th data-priority="7">Flight *(RT)</th>
                                                    <th data-priority="8">Hotel *(RT)</th>
                                                    <th data-priority="9">Car *(RT)</th>
                                                    <th data-priority="10">Tour *(RT)</th>
                                                    <th data-priority="10">Status</th>
													<th data-priority="12" style="width: 30px"><img class="clippy" src="admin/img/svg/settings.svg" width="16" alt="Settings"></th>
													<th></th>
                                                </tr>
                                            </thead>
                                          <tbody>
											<s:iterator value="configList" var="" status="rowstatus">
												<tr>
													<td data-title="S.No"><div class="center"><s:property value="%{#rowstatus.count}" /></div></td>
													<td><a href="companyConfigDetails?id=${id}">${configNumber != null && configNumber != '' ? configNumber:'N/A' }</a></td>
													<td>${companyName != null && companyName != '' ? companyName:'N/A' }</td>
													<td data-title="Company Type"><s:property value="companyType" /></td>
													<td data-title="Config Type"><s:property value="configName" /> - ${domainName}</td>
													<td data-title="Config Type">
													<c:choose>
													<c:when test="${companyConfigType.b2c == true}">
													B2C
													</c:when>
													<c:when test="${companyConfigType.b2b == true}">
													B2B
													</c:when>
													<c:when test="${companyConfigType.b2e == true}">
													B2E
													</c:when>
													<c:when test="${companyConfigType.api == true}">
													API
													</c:when>
													<c:when test="${companyConfigType.whitelable == true}">
													Whitelable
													</c:when>
													<c:when test="${companyConfigType.ownWebsite == true}">
													Own Website
													</c:when>
													<c:otherwise>
													N/A
													</c:otherwise>
													</c:choose>
													</td>
													<td data-title="Flight *(RT)">
													${flightCompanyConfig != null? '<img class="clippy" src="admin/img/svg/checkedg.svg" width="15" alt="Yes" style="margin-bottom: 3px;">':'<img class="clippy" src="admin/img/svg/close.svg" width="12" alt="No" style="margin-bottom: 3px;">'}
													</td> 
													<td data-title="Hotel *(RT)">
													${hotelCompanyConfig != null? '<img class="clippy" src="admin/img/svg/checkedg.svg" width="15" alt="Yes" style="margin-bottom: 3px;">':'<img class="clippy" src="admin/img/svg/close.svg" width="12" alt="No" style="margin-bottom: 3px;">'}
													</td>	
													<td data-title="Car *(RT)">
													${carCompanyConfig != null? '<img class="clippy" src="admin/img/svg/checkedg.svg" width="15" alt="Yes" style="margin-bottom: 3px;">':'<img class="clippy" src="admin/img/svg/close.svg" width="12" alt="No" style="margin-bottom: 3px;">'}
													</td> 
													<td data-title="Tour *(RT)">
													${tourCompanyConfig != null? '<img class="clippy" src="admin/img/svg/checkedg.svg" width="15" alt="Yes" style="margin-bottom: 3px;">':'<img class="clippy" src="admin/img/svg/close.svg" width="12" alt="No" style="margin-bottom: 3px;">'}
													</td> 
													<td><label class="switch"> ${active}<input type="checkbox"  data-id="${id}" class="config_toggle" ${active == true?'checked':''}>
															<span class="slider"></span>
															</label></td>
													<td data-title="Action">
														<div class="dropdown pull-right" style="    margin-right: 20px;">
														  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
														  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
														  <ul class="dropdown-menu">
														    <li class="action"><a href="companyConfigDetails?id=${id}">Edit </a></li>
														    <li class="action"><a href="companyConfigDetails?id=${id}">Delete</a></li>
														  </ul>
														</div>
													</td>
													<td></td>
												</tr>
											</s:iterator>
										</tbody>
                                        </table>
                                    </div>
								</div></div></div></div></div>
                            </div>
                        </div>
                    </div>
				</div>
</section>
 <script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>	
 <script>
 $('.config_toggle').change(function () {
    var status = false;
    var id = $(this).data("id");
    if($(this).is(":checked")==false)
     {
      status= false;
      $(this).attr('checked', false);
     }
    else{
      status= true;
      $(this).attr('checked', true);
     }
    $.ajax({
    	url : "update_company_config_status?statusFlag="+status+"&flagType=STATUS&id="+id,
		type : "POST",
		dataType : 'json',
		success : function(jsonData) {
			if(jsonData.message.status == 'success'){
				alertify.success(jsonData.message.message)
			}
			else if(jsonData.message.status == 'error'){
				 alertify.error(jsonData.message.message);
			}
			else if(jsonData.message.status == 'input'){
				 alertify.error(jsonData.message.message);
			}
			
		},
		error : function(request, status, error) {
			alertify.error("company config status can't be changed ,please try again");
		}
	});
    });
</script>
  <script>
//company name
	var CompanyName = {
		url: "getCompanyConfigJson.action?data=companyName",
		getValue: "companyName",
		
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#companyname-json").easyAutocomplete(CompanyName);
	
 // company type
		var CompanyType = {
			url: "getCompanyConfigJson.action?data=companyType",
			getValue: "companyType",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#companytype-json").easyAutocomplete(CompanyType);
		
		// config name
		var ConfigName = {
			url: "getCompanyConfigJson.action?data=configName",
			getValue: "configName",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#configname-json").easyAutocomplete(ConfigName);
		
		// config type
		var ConfigType = {
			url: "getCompanyConfigJson.action?data=configType",
			getValue: "configType",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#configType-json").easyAutocomplete(ConfigType);
		
		
		// rate type
		var RateType = {
			url: "getCompanyConfigJson.action?data=rateType",
			getValue: "rateType",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		$("#rateType-json").easyAutocomplete(RateType);
		
	</script>	