<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!--HEADER ENDS-->

<!--************************************
        MAIN SIDEBAR MENU
    ****************************************-->
<s:property value="session.Company.companyRole.isDistributor()" />
<s:property value="session.User.userRoleId.isAdmin()" />
<!--SIDEBAR BEGINS-->
<%-- <s:if test="%{#session.Company!=null && #session.User!=null && #session.User.userRoleId.isSuperUser()}"> --%>
<aside>
	<section id="sidebar" class="sidebar-collapse collapesd">
		<ul class="sidebar-nav" id="sidebar-accordion">
			<li><a href="home" class="clearfix "> <i class="icon fa fa-dashboard"></i> <span class="title"><s:text name="tgi.label.dashboard" /></span>
			</a></li>
<!--All Wallet Links-->			
			<s:if test="%{#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isUsermode()}">
				<li><a href="walletTransactions" class="clearfix withsub">
						<i class="icon fa fa-list"></i> <span class="title"><s:text name="tgi.label.my_transactions" /> </span>
				</a>
					<ul class="submenu">
						<li><a href="walletTransactions"><i class="fa fa-home"></i>
								<s:text name="tgi.label.view_all_transactions" /> </a></li>
					</ul></li>
			</s:if>
			<s:if test="%{((#session.User.userRoleId.isSuperUser()||#session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-money"></i> <span class="title"><s:text name="tgi.label.manage_wallet" /> </span> <span class="arrow">
							<i class="fa fa-angle-left"></i>
					</span> <span class="info"> </span>
				</a>
					<ul class="submenu">
						<s:if test="%{#session.User.userRoleId.isSuperUser() && #session.User.userRoleId.isAdmin()}">
							<li><a href="goMyWallet"><s:text name="tgi.label.add_myWallet" /> </a></li>
						</s:if>

						<s:if test="%{ #session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isUsermode()}">
							<li><a href="userWalletList">List MyWallet</a></li>
						</s:if>
					</ul></li>
			</s:if>
<!--All Company Links-->	
			<s:if test="%{((#session.User.userRoleId.isSuperUser()||#session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-building-o"></i> <span class="title"><s:text name="tgi.label.company" /> </span> <span class="arrow"> <i
							class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">

						<li><a href="companyList"><s:text name="tgi.label.all" />
								<s:text name="tgi.label.company" /></a></li>
						<li><a href="addCompany"><s:text
									name="tgi.label.add_company" /> </a></li>
						<li><a href="getApprovalCompaniesList"><s:text
									name="tgi.label.new_company_approvals" /> </a></li>
					</ul></li>
			</s:if>
<!--Company Configraction  -->
			<s:if test="%{((#session.User.userRoleId.isSuperUser()||#session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-globe"></i> <span class="title"><s:text
								name="tgi.label.company_config" /> </span> <span class="arrow">
							<i class="fa fa-angle-left"></i>
					</span> <span class="info">
							<!--notification-->
					</span>
				</a>
					<ul class="submenu">
						<li><a href="companyConfigList"><s:text
									name="tgi.label.all" /> </a></li>
						<li><a href="addNewCompanyConfig"><s:text
									name="tgi.label.add" />
								<s:text name="tgi.label.config" /> </a></li>
					</ul></li>
			</s:if>
<!-- Employee -->
			<s:if test="%{#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUsermode()}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-users"></i> <span class="title"><s:text
								name="tgi.label.employees" /> </span> <span class="arrow"> <i
							class="fa fa-angle-left"></i>
					</span> <span class="info">
							<!--notification-->
					</span>
				</a>
					<ul class="submenu">
						<li><a href="userList"><s:text name="tgi.label.all" />
								Employee </a></li>
						<li><a href="userRegister"><s:text
									name="tgi.label.add_new_employee" /> </a></li>
					</ul></li>
			</s:if>
<!--All Company Leads/ Agents Sign Up Links-->	
			<s:if test="%{((#session.User.userRoleId.isSuperUser()|| #session.Company.companyRole.isDistributor()) && (#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder()))}">
				<li class=""><a href="#" class="clearfix withsub">
				<i class="icon fa fa-building-o"></i> <span class="title"><s:text name="tgi.label.company" /> Lead </span> <span class="arrow"> <i class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
						<li><a href="listCompanyLead">List</a></li>
					</ul></li>
			</s:if>
			
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-building-o"></i> <span class="title">Travel Sales Lead</span> <span class="arrow"> <i
							class="fa fa-angle-left"></i>
					</span> <span class="info">
							<!--notification-->
					</span>
				</a>
					<ul class="submenu">
					<s:if test="%{(#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder() )}">
						<li><a href="addTravelSalesLead">Add New  Lead</a></li>
						<li><a href="listTravelSalesLead">All Lead</a></li>
					</s:if>
						<s:if test="%{#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isTechSupport}">
							<li><a href="listTravelSalesLead?filterFlag=true&assignToFlagId=<s:property value="%{#session.User.id}"/>">Assign To Me</a></li>
							<li><a href="listTravelSalesLead?filterFlag=true&employeeReferralId=<s:property value="%{#session.User.id}"/>">My Referral Lead</a></li>
						</s:if>
					</ul></li>
			<s:if test="%{((#session.Company.companyRole.isDistributor() || #session.Company.companyRole.isAgent()) || #session.User.userRoleId.isDeveloperMode())}">
				<li class=""><a href="#" class="clearfix withsub"> <i class="icon fa fa-building-o"></i> <span class="title">Buy Travel Leads</span> <span class="arrow"> <i class="fa fa-angle-left"></i>
					</span>
				</a>
					<ul class="submenu">
						<li><a href="listTravelSalesLeadBuyNow">Buy  Now</a></li>
					</ul></li>
			</s:if>
			<s:if test="%{((#session.User.userRoleId.isSuperUser()|| #session.Company.companyRole.isDistributor() || #session.Company.companyRole.isAgent()) && #session.User.userRoleId.isAdmin())}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-building-o"></i> <span class="title">My
							Travel Lead </span> <span class="arrow"> <i
							class="fa fa-angle-left"></i>
					</span> <span class="info">
							<!--notification-->
					</span>
				</a>
					<ul class="submenu">
						<li><a href="addTravelSalesMyLead">Add Lead</a></li>
						<li><a href="listTravelSalesMyLead">My Lead</a></li>
					</ul></li>
			</s:if>
			<s:if
				test="%{(#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isTechHead() || #session.User.userRoleId.isTechSupport())}">
				<li class=""><a href="#" class="clearfix withsub"> 
				<i class="icon fa fa-building-o"></i> <span class="title">Bug Tracker </span> <span class="arrow"> <i class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
						<li><a href="addBugTracker">Add BugTracker</a></li>
						<li><a href="goBugTrackerList">BugTracker List</a></li>
						<li><a href="getAssignBugList"> Assign To Me </a></li>
					</ul></li>
			</s:if>
			
			<s:if test="%{(#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isFlight()}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-globe"></i> <span class="title"><s:text
								name="tgi.label.flight_markup" /> </span> <span class="arrow">
							<i class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
						<li><a href="flightMarkupList"><s:text name="tgi.label.markup_list" /> </a></li>
						<li><a href="addMarkup"><s:text name="tgi.label.add_markup" /> </a></li>
					</ul></li>
			</s:if>
			<s:if test="%{((#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isHotel())}">

				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-globe"></i> <span class="title"><s:text name="tgi.label.hotel_markup" /></span> <span class="arrow"> <i
							class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
						<li><a href="hotelMarkupList"><s:text name="tgi.label.hotel_markup_list" /> </a></li>
						<li><a href="addHotelMarkup"><s:text name="tgi.label.add_hotel_markup" /> </a></li>
					</ul></li>
			</s:if>
			<s:if test="%{((#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isCar())}">
				<li class=""><a href="#" class="clearfix withsub"> <i class="icon fa fa-globe"></i> <span class="title"><s:text name="tgi.label.car_markup" /> </span> <span class="arrow"> <i class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
						<li><a href="carMarkupList"><s:text name="tgi.label.car_markup_list" /> </a></li>
						<li><a href="addCarMarkup"><s:text name="tgi.label.add_car_markup" /> </a></li>
					</ul></li>
			</s:if>
			
			<s:if test="%{((#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isTour())}">
				<li class=""><a href="#" class="clearfix withsub"> <i class="icon fa fa-globe"></i> <span class="title"><s:text name="tgi.label.tour_markup" /> </span> <span class="arrow"> <i
							class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
						<li><a href="tourMarkupList"><s:text name="tgi.label.tour_markup_list" /> </a></li>
						<li><a href="addTourMarkup"><s:text name="tgi.label.add_tour_markup" /> </a></li>
					</ul></li>
			</s:if>
			<s:if
				test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport()
                   					||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isFlight())}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-plane"></i> <span class="title"><s:text name="tgi.label.flight" /> </span> <span class="arrow"> <i
							class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
						<s:if test="%{#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport() || #session.User.userRoleId.isUsermode() }">
							<li><a href="flightReportList"><s:text name="tgi.label.report_list" /> </a></li>
						</s:if>
						<s:if test="%{#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUsermode()}">
							<li><a href="flightOrderList"><s:text name="tgi.label.order_list" /> </a></li>
						</s:if>
						<s:if
							test="%{#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport() || #session.User.userRoleId.isUsermode() }">
							<li><a href="flightCommissionReportList"><s:text name="tgi.label.agent_commision_report" /> </a></li>
							<s:if test="%{#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUsermode() }">
								<li><a href="flightCustomerInvoiceList"><s:text name="tgi.label.custommer_invoice" /> </a></li>
								<li><a href="flightAgentInvoiceList"><s:text name="tgi.label.agent_commision_invoice" /> </a></li>
							</s:if>
						</s:if>
					</ul></li>
			</s:if>
			<s:if test="%{((#session.User.userRoleId.isAdmin()  || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isHotel())}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-bed"></i> <span class="title"><s:text name="tgi.label.hotel" /> </span> <span class="arrow"> <i
							class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
						<s:if test="%{ #session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUserOrder()}">
							<li><a href="hotelReportList"><s:text name="tgi.label.report_list" /> </a></li>
						</s:if>
						<s:if test="%{ #session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUserOrder()}">
							<li><a href="hotelOrdersList"><s:text name="tgi.label.order_list" /> </a></li>
						</s:if>
						<s:if test="%{ #session.User.userRoleId.isAdmin() ||	#session.User.userRoleId.isUserReport() || #session.User.userRoleId.isUserOrder()}">
							<li><a href="hotelCommissionReport"><s:text name="tgi.label.agent_commision_report" /> </a></li>
						</s:if>
						<s:if test="%{ #session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder()}">
							<li><a href="hotelCustomerInvoiceList"><s:text name="tgi.label.custommer_invoice" /> </a></li>
							<li><a href="hotelAgentCommInvoiceList"><s:text name="Agent Commision Invoice" /> </a></li>
						</s:if>
					</ul></li>
			</s:if>
			<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport()
                   					||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isCar())}">
				<li class="">
				<a href="#" class="clearfix withsub"> <i class="icon fa fa-car"></i> <span class="title"><s:text name="tgi.label.car" /></span> 
				<span class="arrow"> <i class="fa fa-angle-left"></i> </span> <span class="info"> </span>
				</a>
					<ul class="submenu">
						<s:if test="%{#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport() || #session.User.userRoleId.isUserOrder() }">
							<li><a href="carReportList"><s:text name="tgi.label.report_list" /> </a></li>
						</s:if>
						<s:if test="%{#session.User.userRoleId.isAdmin()  ||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUserOrder() }">
							<li><a href="carOrderList"><s:text name="tgi.label.order_list" /> </a></li>
						</s:if>

						<s:if test="%{#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserReport() || #session.User.userRoleId.isUserOrder()}">
							<li><a href="carCommissionReport"><s:text name="tgi.label.agent_commision_report" /> </a></li>
						</s:if>
							<s:if test="%{#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder()}">
								<li><a href="carCustomerInvoiceList"><s:text name="tgi.label.custommer_invoice" /> </a></li>
								<li><a href="carAgentCommInvoiceList"><s:text name="tgi.label.agent_commision_invoice" /> </a></li>
							</s:if>
					</ul></li>
			</s:if>

			<s:if test="%{((#session.User.userRoleId.isAdmin() ||  #session.User.userRoleId.isUserReport() ||  #session.User.userRoleId.isUserOrder() || #session.User.userRoleId.isUsermode()) && #session.Company.companyCmsService.isTour())}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-globe"></i> <span class="title"><s:text
								name="tgi.label.tour" /> </span> <span class="arrow"> <i
							class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
						<s:if test="%{#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder()}">
							<li><a href="tourOrderList">My <s:text name="tgi.label.order_list" /> </a></li>
						</s:if>
						<s:if test="%{#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder()}">
							<li><a href="tourAgentCommInvoiceList">My Commision Invoice </a></li>
						</s:if>
						<s:if test="%{#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserReport() || #session.User.userRoleId.isUserOrder()}">
							<li><a href="tourCommissionReport">My Commision Report </a></li>
						</s:if>
						
					</ul></li>
			</s:if>
			<%-- <s:if test="%{#session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUsermode() || #session.User.userRoleId.isCrm()}">
                     <li class="">
                        <a href="#" class="clearfix withsub">
                            <i class="icon fa fa-globe"></i>
                            <span class="title"><s:text name="tgi.label.crm" /> </span>
                            <span class="arrow">
                            <i class="fa fa-angle-left"></i>
                        </span>
                            <span class="info"><!--notification--></span>
                        </a>
                        <ul class="submenu">
                            <li><a href="PassengerProfileList"><s:text name="tgi.label.passenger_profile" /> </a></li>
                             <li><a href="FrequentFlyerList"><s:text name="tgi.label.frequent_flyer" /> </a></li>
                             <li><a href="PassportVisaList"><s:text name="tgi.label.photo_passport_visa_details" /> </a></li>
                             <li><a href="EmergencycontactList"><s:text name="tgi.label.emergency_contact_details" /> </a></li>
                        </ul>
                    </li>
			</s:if> --%>
			<s:if test="%{(#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isDeveloperMode() || #session.User.userRoleId.isAdmin() || #session.User.userRoleId.isUserOrder() )}">
				<li class=""><a href="#" class="clearfix withsub"> 
				<i class="icon fa fa-money"></i> <span class="title">Front User </span> <span class="arrow">
							<i class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
							<li><a href="userList?userRoleFlag=customer&filterFlag=true">All Front User</a></li>
					</ul>
					</li>
			</s:if>
			<s:if test="%{(#session.User.userRoleId.isSuperUser() || #session.User.userRoleId.isAdmin() || #session.User.userRoleId.isCms())}">
				<li class=""><a href="#" class="clearfix withsub"> <i
						class="icon fa fa-money"></i> <span class="title">CMS </span> <span class="arrow">
							<i class="fa fa-angle-left"></i>
					</span> <span class="info">
					</span>
				</a>
					<ul class="submenu">
							<li><a href="contactUsList">Contact Us</a></li>
					</ul>
				</li>
			</s:if>
			<s:if test="%{(#session.User.userRoleId.isSuperUser() || #session.Company.companyRole.isDistributor())  && #session.User.userRoleId.isAdmin()}">
			<li class=""><a href="#" class="clearfix withsub"> <i class="icon fa fa-building-o"></i> <span class="title">Quick
						Trip </span> <span class="arrow"> <i class="fa fa-angle-left"></i>
				</span> <span class="info">
				</span>
			</a>
				<ul class="submenu">
					<li><a href="addTourTrip">Add Tour Trip </a></li>
					<li><a href="addFlightTrip">Add Flight Trip</a></li>
					<li><a href="addHotelTrip">Add Hotel Trip</a></li>
					<li><a href="addCarTrip">Add Car Trip</a></li>
					<li><a href="addMiscellaneousTrip">Add Miscellaneous Trip </a></li>

				</ul>
			</li>
			</s:if>
			<s:if test="%{(#session.User.userRoleId.isSuperUser() || #session.Company.companyRole.isDistributor())  && #session.User.userRoleId.isAdmin()}">
			<li class=""><a href="#" class="clearfix withsub"> <i class="icon fa fa-building-o"></i> <span class="title">HR Services</span> <span class="arrow"> <i class="fa fa-angle-left"></i>
				</span> <span class="info">
				</span>
			</a>
				<ul class="submenu">
					<li><a href="userList">Employee List </a></li>
					<li><a href="getUserAttendanceView">Employee Attendance </a></li>
				</ul>
			</li>
			</s:if>
		</ul>
	</section>
</aside>
