<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="dj" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style>
#notification_error {
    border: 1px solid #A25965;
    height: auto;
    width: auto;
    padding: 4px;
    background: #F8F0F1;
    text-align: center;
    -moz-border-radius: 5px;
}

#notification_ok {
    border: 1px #567397 solid;
    height: auto;
    width: auto;
    padding: 4px;
    background: #f5f9fd;
    text-align: center;
    -moz-border-radius: 5px;
}

</style>
<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"userRegister";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>    
 
<script type="text/javascript">
	$(document).ready(function() {
		$("#userroletype").multipleSelect({
			filter: true,
			width: '100%',
	        height: '100%',
	        placeholder: "Select a User Role",
		});
		
		var arrayuserroleType = '${userroletypeView}'.split(',');
		console.debug(arrayuserroleType);
		$("#userroletype").multipleSelect("setSelects", arrayuserroleType);
	
	});
</script>

<script type="text/javascript">
	$(function() {
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "userList";
		$('#alert_box_info_ok').click(function() {
			 window.location.assign(finalUrl); 
				$('#alert_box_info').hide();
				
			});
		$('#cancel').click(function() {
			$('#error-alert').hide();

		});
	});
</script>
    <!--**************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.employee_registration" /></h5>
                           <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-outline-primary" href="userList"><span class="fa fa-arrow-left"></span> Back To List </a>
							</div>
						</div>
                               </div> 
                            </div>
                        <form action="registerUserAccount" id="user-register" method="post" name="myForm"  enctype="multipart/form-data">
							<div class="col-lg-12">
						<div class="profile-timeline-card">
						
							<p class="mt-1 mb-2">
										<span class="company-font-icon"><i class="fa fa-user"></i></span> <span> <b class="blue-grey-text">User Detail</b></span>
							</p>
							<hr>
									<s:if test="hasActionErrors()">
										<div class="success-alert" style="display: none">
											<span class="fa fa-thumbs-o-up fa-1x"></span>
											<s:actionerror />
										</div>
									</s:if>
							<s:if test="hasActionMessages()">
								<div class="success-alert" style="display: none">
									<span class="fa fa-thumbs-o-up fa-1x"></span>
									<s:actionmessage />
								</div>
							</s:if>
                               <div class="row row-minus">
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.emp_role" /> <span class="text-danger">${message}</span></label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
											<select class="input-md" id="userroletype" name="userRoleTypeArray" multiple="multiple" >
												<c:forEach items="${userRolesMap}" var="user_role" varStatus="rowStatus">
													<option value="${user_role.key}">${user_role.value}</option>
												</c:forEach>
											</select>
											</div></div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.user_name" /></label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control" id="username" name="userName" placeholder="" autocomplete="on" >
												<div id="status" style="margin-bottom: -10px;"></div>
										</div></div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.first_name" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control " maxlength="25" id="only-text" onkeypress="return onlyAlphabets(event,this);" name="firstName" placeholder="First Name" autocomplete="on"></div></div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.last_name" />
											</label>
										<div class="controls">
											<div class=""><div class="form-group">
												<input type="text" class="form-control" id="last-name" name="lastName" placeholder="Last Name" autocomplete="on" onkeypress="return onlyAlphabets(event,this);" >
											</div></div>
										</div>
									</div>
								</div>	     
                               <div class="row row-minus">
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.email" />&nbsp; <span id="email-status"></span> </label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="email" class="form-control " name="email" id="email-address" placeholder="Email" autocomplete="on" >
										</div></div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.password" /></label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input name="password"  id="password" type="password" class="form-control"  placeholder="Password" autocomplete="on" ></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput">Mobile</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
											<input type="tel" maxlength="14" class="form-control " name="mobile" id="mobile" value="" placeholder="080-567895454" autocomplete="on" ></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput">Phone</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
											<input type="tel" maxlength="14" class="form-control " name="userDetail.phone" id="phone" value="" autocomplete="off" ></div></div>
											</div>
										</div>
									</div>
									<div class="row">
									<div class="col-md-3">
										<label class="form-control-label" for="appendedInput">Date Of Birth</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
											<input type="text" class="form-control " name="userDetail.dateOfBirth" id="dateOfBirth" value="" autocomplete="off" ></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput">Upload Images</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="file" name="userDetail.uploadFile" class="form-control"></div></div>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-control-label" for="appendedInput">About Me</label>
										<div class="controls">
											<div class=""><div class="form-group">
											<textarea class="form-control " maxlength="300" id="description"name="userDetail.description" placeholder="Description" autocomplete="off" rows="1"></textarea></div></div>
											</div>
									</div>
									</div>
									</div>	     
	                                </div>
									<div class="profile-timeline-card">
									<p class="mt-1 mb-2">
										<span class="company-font-icon"><i class="fa fa-location-arrow"></i></span> <span> <b class="blue-grey-text">Address Information</b></span>
									</p>
									<hr>
                     				<div class="row row-minus">
									<div class="col-md-12">
										<label class="form-control-label" for="appendedInput"><s:text name="tgi.label.address" /></label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<textarea class="form-control " id="address" name="userDetail.address" placeholder="Address" autocomplete="off" rows="1"></textarea></div></div>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.city" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control " id="city" name="userDetail.city" placeholder="City" autocomplete="off" ></div></div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput">Zip Code
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<input type="text" class="form-control " id="zipCode" name="userDetail.zipCode" placeholder="zip Code" autocomplete="off" ></div></div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput">State
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control " name="userDetail.state" id="state" >
													<option selected  value="">Select State</option>
													<c:forEach items="${statesList}" var="states">
														<option value="${states.state}">${states.state}</option>
													</c:forEach>
					
												</select>
												</div></div>
										</div>
									</div>
									<div class="col-md-3">
										<label class="form-control-label" for="prependedInput"><s:text name="tgi.label.country" />
											</label>
										<div class="controls"><div class="form-group"><div class="">
												<select class="form-control " name="userDetail.countryName" id="country" >
													<option selected="selected" value=""><s:text name="tgi.label.select_country" /></option>
													<c:forEach items="${CountryList}" var="country">
														<option value="${country.countryName}" ${country.countryName == 'India' ? 'selected': ''}>${country.countryName}</option>
													</c:forEach>
												</select>
												</div></div>
										</div>
									</div>
									
									</div>	     
                                </div>
                                <div class="row row-minus">
									<div class="col-md-12">
										<div class="set pull-right" style="margin-bottom:0px;margin-top: 7px;">
										<div class="form-actions">
											<input type="hidden" name="language" id="language" value="English">
											<input type="hidden" name="currencyCode" id="currency-code" value="INR">
											<button class="btn btn-default btn-danger" id="tourCancelBtn" type="reset">Reset</button>
											<button type="submit" class="btn btn-primary user-save-btn" id=""><s:text name="tgi.label.register" /></button>
										</div></div>
									</div>
									</div> 
                        </div>
                        </form>
                    </div>
            </section>
<script src="admin/js/admin/user-validators.js"></script>
<script>
$(function() {
  $('#dateOfBirth').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    maxDate: new Date()
  }, function(start, end, label) {
	  
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#username").change(function() { 
	var usr = $("#username").val();
	if(usr.length >= 4)
	{
	$("#status").html('<img src="admin/img/loading.gif" align="absmiddle">&nbsp;Checking availability...');
	    $.ajax({  
	    url: "checkUserExist?user_name="+usr,  
	    type: "POST",  
	    dataType: 'json',  
	    success: function(jsonData){  
	  	// $("#status").ajaxComplete(function(event, request, settings){ 
			
	  		if(jsonData.message.status == 'success')
	  		{
	  			$("#username").removeClass('object_error'); // if necessary
				$("#username").addClass("object_ok");
				$("#user-save-btn").prop("disabled", false);
				$("#status").html("<img class='clippy' src='admin/img/svg/checked_1.svg' width='12'>&nbsp;<span class='' style='font-size: 12px;color: #3cc341;'>"+jsonData.message.message+"</span>");
			}
			else if(jsonData.message.status == 'error')
			{
				$("#username").removeClass('object_ok'); // if necessary
				$("#username").addClass("object_error");
				$("#user-save-btn").prop("disabled", true);
				$("#status").html("<img class='clippy' src='admin/img/svg/close.svg' width='10'>&nbsp;<span class='text-danger' style='font-size: 12px;'>"+jsonData.message.message+"</span>");
			}
	   //});
	 } 
	   
}); 
}
else
	{
	$("#status").html('<font color="red" style="font-size: 12px;">The username should have at least <strong>4</strong> characters.</font>');
	$("#username").removeClass('object_ok'); // if necessary
	$("#username").addClass("object_error");
	}
	});
	});
/*-------------------------------------------------------------------------------------*/

$(document).ready(function() {
$('#user-register')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				userRoleTypeArray: {
	                    validators: {
	                        callback: {
	                            message: 'Please choose 2-3 browsers you use for developing',
	                            callback: function(value, validator) {
	                                // Get the selected options
	                                var options = validator.getFieldElements('userRoleTypeArray').val();
	                                return (options != null
	                                        && options.length >= 2 && options.length <= 3);
	                            }
	                        }
	                    }
	                },
				userName : {
					message : 'Username CompanyName is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Username'
						},
					}
				},
				userroletype : {
					message : 'Employee Role is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Employee Role '
						},
					}
				},
				firstName : {
					message : 'Firstname is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Firstname'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'Firstname  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				lastName : {
					message : 'Lastname is not valid',
					validators : {
						 notEmpty : {
							message : 'Select enter a Lastname'
						},
						stringLength : {
							min : 0,
							max : 20,
							
							message : 'Lastname  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				email : {
					message : 'Email is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Email'
						},
						
					}
				},
				password : {
					message : 'Password Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Password'
						},
						stringLength : {
							min : 4,
							max : 20,
							
							message : 'Password  must be more than 6 and less than 20 characters long'
						} 
						
					}
				},
				address : {
					message : 'Address Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Address'
						},
						stringLength : {
							min : 3,
							max :100,
							
							message : 'Address  must be more than 6 and less than 100 characters long'
						} 
						
					}
				},
				city : {
					message : 'City Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a City'
						},
						stringLength : {
							min : 0,
							max :50,
							
							message : 'City  must be more than 2 and less than 50 characters long'
						} 
						
					}
				},
				description : {
					message : 'Description Type is not valid',
					validators : {
						
						stringLength : {
							min : 0,
							
							
							message : 'Description  must be more than 2 characters long'
						} 
						
					}
				},
				securityAnswer : {
					message : 'Answer Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Answer'
						},
						stringLength : {
							min : 0,
							
							
							message : 'Answer must be more than 2 characters long'
						} 
						
					}
				},
				phone : {
					message : 'Phone is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Phone'
						},
						integer: {
	                        message: 'The value is not an integer'
	                    },
						stringLength : {
							min : 4,
							max :18,
							
							message : 'Phone  must be more than 2 and less than 15 characters long'
						} 
						
					}
				},
				status : {
					message : 'Status is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Status'
						},
						
					}
				},
				countryName : {
					message : 'Country is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Country'
						},
						
					}
				},
			/* 	Language : {
					message : 'Language is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Language'
						},
						
					}
				}, */
				securityQuestion : {
					message : 'Question is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Question'
						},
						
					}
				},
	
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {
	/* notifySuccess(); */
	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script>
        <!--ADMIN AREA ENDS-->
  		<s:if test="message != null && message  != ''">
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>