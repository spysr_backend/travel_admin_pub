<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

 <script src="admin/js/jspdf.min.js"></script>  
  
  <script>
$(function () {

  $('#pdf').click(function () {
    var doc = new jsPDF();
    doc.addHTML($('#invoice')[0], 15, 15, {
      'background': '#fff',
    }, function() {
      doc.save('sample-file.pdf');
    });
  });
});
</script> 
		<!-- Content Header (Page header) -->
		<section class="wrapper container-fluid">
                <div class="row">
                    <div class="">
                        <div class="pnl">
				<div class="hd clearfix">
					<h5 class="pull-left">
						<s:text name="tgi.label.tour_customer_invoice" />
					</h5>
					<div class="set pull-right">
						<a class="btn btn btn-primary btn-xs" href="tourCustomerInvoiceList"><span class="pull-right">
								<span class="glyphicon glyphicon-step-backward"></span> <s:text name="tgi.label.back" />
						</span></a>
					</div>
						<div class="set pull-right">
						<button class=" btn btn-success btn-xs " onclick="sendCustomerInvoiceToCustomer();">
							<s:text name="tgi.label.send_invoice" /> <span class="glyphicon glyphicon-send"></span>
						</button>
						<button type="button" class="btn btn-primary but no-print" id="h4" style="display: none">
							<s:text name="tgi.label.sending" /><i class="fa fa-arrow-circle-right"></i>
						</button>
						</div>
					<!-- Print Invoice Button  -->
					<div class="set pull-right">
						<a href="<%=request.getContextPath()%>/tour_customer_invoice?orderId=${invoiceData.yourRef}" target="blank" class="popup btn btn-success btn-xs">
						<span class="pull-right"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Save as Invoice
						</span></a>
					</div>
					<div class="set pull-right">
						<a href="<%=request.getContextPath()%>/tour_customer_voucher?orderId=${invoiceData.yourRef}" target="blank" class="popup btn btn-success btn-xs">
						<span class="pull-right"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Save as Voucher 
						</span></a>
					</div>
				</div>
				<div class="col-md-12 mt-2"> 
				<div class="profile-timeline-card">   
					<div class="row row-minus">			
					<div class="col-md-12">
				           <table class="table mb-0 mt-3">
				         <tbody>
				          <tr>
                         	<td style="border-top: 0px solid #dddddd;">
                             <p>&nbsp;</p> <img src="https://i.imgur.com/kg7Lvd4.png" width="200" height="43" style="margin-left: 10px;"/>
                             <p>&nbsp;</p>
                             </td>
                             <td style="text-align: right;border-top: 0px solid #dddddd;">
                             <p>&nbsp;</p>
                             <p style="">
                                 <b> <span>
									${companyVo.companyName}</span></b> <br />
									                                     <span style="">${companyVo.address}</span><br /> <span style=""><b>State :</b> ${companyVo.state != null && companyVo.state != ''?companyVo.state:'...'},</span><br />
									<b>Phone :</b> <span style="">
									<span>${companyVo.phone != null && companyVo.phone != ''?companyVo.phone :'N/A'}</span>
									</span><br /> 
									<b>Email :</b> <span style="">
									${companyVo.email != null && companyVo.email != ''?companyVo.email:''}
									</span>
									<br />
									<b>Website :</b> <span style="">
									${companyVo.website != null && companyVo.website != '' ?companyVo.website : 'N/A'}
									</span>
                             </p>
                         </td>
                     </tr>
				          </tbody>
				        </table>
				     </div> 
				     </div>
						<hr style="border-top: 3px solid #eeeeee;">
					<div class="timeline-footer row row-minus"> 
				     <div class="col-md-3">
				     <h4 class="text-left"><span>Bill <s:text name="tgi.label.to" /></span></h4>
				           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
				         <tbody>
				          <tr>
				            <td> <b><s:text name="tgi.label.to" /></b></td>
				            <td>${invoiceData.tourOrderRow.orderCustomer.firstName} ${invoiceData.tourOrderRow.orderCustomer.lastName}</td>
				          </tr>
				          <tr>
				            <td><b><s:text name="tgi.label.address" /></b></td>
				            <td>${invoiceData.tourOrderRow.orderCustomer.address}</td>
				          </tr> 
				          <tr>
				            <td><b><s:text name="tgi.label.tel" /></b></td>
				            <td>${invoiceData.tourOrderRow.orderCustomer.mobile}</td>
				          </tr> 
				          <tr>
				            <td><b><s:text name="tgi.label.email" /></b></td>
				            <td>${invoiceData.tourOrderRow.orderCustomer.email}</td>
				          </tr>
				          </tbody>
				        </table>
				     </div> 
				     <div class="col-md-6">&nbsp;</div>
				     <div class="col-md-3">
				     <h4 class="text-left"><span><s:text name="tgi.label.tax_invoice" /></span></h4>
				           <table class="table inn-num table-bordered no-table-css mb-0 mt-3">
				         <tbody>
				          <tr>
				            <td> <b><s:text name="tgi.label.invoice_no" /></b></td>
				            <td> <s:property  value="invoiceData.invNo"/></td>
				          </tr>
				          <tr>
				            <td><b><s:text name="tgi.label.book_no" /></b></td>
				            <td><s:property  value="invoiceData.orderNumber"/></td>
				          </tr> 
				          <tr>
				            <td><b><s:text name="tgi.label.date" /></b></td>
				            <td><s:property  value="invoiceData.date"/></td>
				          </tr>
				          </tbody>
				        </table>
				     </div>
				</div>
					</div>
					</div> 
       </div></div></div></section>
       <section class="col-md-12">
	    		<div class="sccuss-full-updated" id="success-alert" style="display:none">
					<div class="succfully-updated clearfix">
	
						<div class="col-sm-2">
							<i class="fa fa-check fa-3x"></i>
						</div>
	
						<div id="message" class="col-sm-10">
	 					</div>
	 					 <button type="button" id="success" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>
	 				 </div>
	 				
				</div>
          <!-- Small boxes (Stat box) -->
          	
     <div class="profile-timeline-card">
 	<div class="">
  <div id="invoice" class="clearfix mt-0">  

<div class="row">
<div class="">
       <table class="table table-bordered in-table-border no-table-css">
        <thead>
          <tr class="info">
            <th><h4><s:text name="tgi.label.gst_type" /></h4></th>
            <th><h4><s:text name="tgi.label.tour_details_and_guest_particulars" /></h4></th>
            <th><h4><s:text name="tgi.label.qty" /></h4></th>
            <th><h4><s:text name="tgi.label.price" /></h4></th>
            <th><h4><s:text name="tgi.label.total" /> (USD)</h4></th>
          </tr>
        </thead>
            <tbody>
          <s:iterator  value="invoiceData.tourOrderGuests" status="rowCount" >
          <tr>
           <td>ZR</td>
              <td >(<s:property  value="%{#rowCount.count}"/>)   <s:property  value="firstName"/> <s:property  value="lastName"/> </td>
             <td >1</td>
             <td >0</td>
             <td >0</td>
          </tr>
          </s:iterator>
          <tr>
            <td>ZR </td>
            <td > ${invoiceData.tourItineraryDescription.category} <br> ${fn:substring(invoiceData.tourItineraryDescription.title, 0, 50)} ...<br>
            ${spysr:formatDate(invoiceData.tourStartDate1,'dd MMM yyyy', 'MMM-d-yyyy')} - ${spysr:formatDate(invoiceData.tourEndDate1,'dd MMM yyyy', 'MMM-d-yyyy')}  
            </td>
            <td >1</td>
            <td >${invoiceData.basePrice}</td>
              <td >${invoiceData.basePrice}</td>
          </tr>
          
           <tr>
            <td></td>
            <td> </td>
            <td> </td>
             <td> <span class="text-success"><s:text name="tgi.label.discount_amount" /></span> </td>
              <td><span class="text-success">- ${invoiceData.discountAmount}</span></td>
          </tr>
           <tr>
            <td></td>
            <td>  </td>
            <td> </td>
             <td>Net Amount<span> </span> </td>
              <td><s:property  value="invoiceData.netAmount"/></td>
          </tr>
           <tr>
            <td></td>
            <td>  </td>
            <td> </td>
             <td><s:text name="tgi.label.tax" /><span> </span> </td>
              <td><s:property  value="invoiceData.tax"/></td>
          </tr>
         
           <tr>
            <td><s:text name="tgi.label.sr" /></td>
            <td> </td>
            <td> </td>
             <td><s:text name="tgi.label.processing_fees" /> <span> </span> </td>
              <td><s:property  value="invoiceData.processingFees"/></td>
              
		           </tr>
		      <c:if test="${invoiceData.tourOrderRow.tourOrderRowInsurance != null}">
		      <tr>
				<td></td>
				<td><span>
				</span></td>
				<td></td>
				<td><s:text name="tgi.label.insurance_fee" /> </td>
				 <td>
              	<c:choose>
				<c:when test="${invoiceData.tourOrderRow.tourOrderRowInsurance.price != null}">
				${invoiceData.tourOrderRow.tourOrderRowInsurance.price}
				</c:when>
				<c:otherwise>
				<s:text name="tgi.label.n_a" />
				</c:otherwise>
				</c:choose>
              </td>
			</tr>
			</c:if>
			
            <tr>
            <th>
           
            </th>
            <th>
           
            </th>
            <th>
          <s:text name="tgi.label.calculation" /> 
            </th>
            <th>
          
            </th>
            <th >
        
            </th>
             </tr>
             
             <tr>
            <td>
          <!-- 6% -->
            </td>
            <td>
          <!-- SR -->
            </td>
            <td>
            </td>
            <td>
        	<s:text name="tgi.label.booking_amount" />
            </td>
            <td colspan="4">
            
            ${invoiceData.price}<br>
            </td>
            
            </tr>
            
              <tr>
            <td>
            </td>
            <td>	
            </td>
            <td>
            </td>
            <td>
        	<s:text name="tgi.label.total_paid" />
            </td>
            <td colspan="4">
	            ${invoiceData.totalAmount}<br>
            </td>
            
            </tr>
        </tbody>
      </table>
    </div>
    <c:if test="${invoiceData.tourOrderRow.tourOrderRowInsurance != null}">
    							<div class="clearfix">
								<div class="payment-in">
									
									<div class="panel panel-info">
									<c:choose>
									<c:when test="${invoiceData.tourOrderRow.tourOrderRowInsurance != null}">
											<h4>
												<s:text name="tgi.label.flight_insurance" />
											</h4>
											<!-- <div class="panel-body"> -->
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="info">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
										<tr>
											<td data-title="><s:text name="tgi.label.insurance_id" />">${invoiceData.tourOrderRow.tourOrderRowInsurance.insuranceId}</td>
											<td data-title="<s:text name="tgi.label.name" />">${invoiceData.tourOrderRow.tourOrderRowInsurance.name}</td>
											<td data-title="<s:text name="tgi.label.price" />">${invoiceData.tourOrderRow.tourOrderRowInsurance.price}</td>
											<td data-title="<s:text name="tgi.label.description" />">${invoiceData.tourOrderRow.tourOrderRowInsurance.description}</td>
											
										</tr>
									</table>
									</c:when>
									<c:otherwise>
											<h4 class="pl-2">
												<s:text name="tgi.label.flight_insurance" />
											</h4>
											<table class="table table-bordered in-table-border no-table-css">
												 <tr class="info">
									<th><s:text name="tgi.label.insurance_id" /></th>
									<th><s:text name="tgi.label.name" /></th>
									<th><s:text name="tgi.label.price" /></th>
									<th><s:text name="tgi.label.description" /></th>
									</tr>
												<tr>
													<td colspan="5"><s:text
															name="tgi.label.insurance_not_available" /></td>
												</tr>
											</table>
											</c:otherwise>
										</c:choose>
										</div>
										</div></div>
										</c:if>

									<div class="clearfix">
										<div class="payment-in">
											<div class="panel panel-info">
												<h4 class="pl-2">
													<s:text name="tgi.label.payment_details" />
												</h4>

												<table
													class="table table-bordered in-table-border no-table-css">
													<tr class="info">
														<th><s:text name="tgi.label.receipt" /></th>
														<th><s:text name="tgi.label.date" /></th>
														<th><s:text name="tgi.label.payment_method" /></th>
														<th><s:text name="tgi.label.payable_amount" /></th>
														<th><s:text name="tgi.label.paid_amount" /></th>
														<th><s:text name="tgi.label.remark" /></th>
													</tr>
													<s:if test="invoiceData.txDetails.size()>0">
													<s:iterator value="invoiceData.txDetails">
														<tr>
															<td>${transactionId != null && transactionId != ''?transactionId:'N/A'}</td>
															<td>${spysr:formatDate(createdDate,'yyyy-MM-dd hh:mm:ss.SSS', 'MMM/dd/yyyy  hh:mm')}</td>
															<td>${paymentMethod}</td>
															<td>${amount}</td>
															<td><fmt:formatNumber type="number" pattern="#.00" value="${totalPaidAmount}" /></td>
															<td>${responseMessage != null && responseMessag != ' ' ?responseMessage:'N/A'}</td>
														</tr>
													</s:iterator>
											</s:if>
										<s:else>
											<tr>
												<td colspan="5"><s:text
														name="tgi.label.payment_details_not_available" /></td>
											</tr>
										</s:else>

												</table>

											</div>
										</div>
									</div>

									<div class="clearfix signature-lint ">

							<div class="col-sm-6">
								<h4><s:text name="tgi.label.recived_by" /></h4>
								<br> <br>
								<p>
									<span style="border-top: 1px solid #adadad; padding: 5px;"><s:text name="tgi.label.customers_signature" /> &amp; <s:text name="tgi.label.chop" /></span>
								</p>
							</div>

							<div class="col-sm-6 pull-right">
								<div class="pull-right">
									<h4>Touroxy</h4>
									<br> <br>
									<p>
										<span style="border-top: 1px solid #adadad; padding: 5px;"><s:text name="tgi.label.autorised_signature" /></span>
									</p>
								</div>
							</div>
						</div>

</div>
</div>

<div id="editor"></div>
            
</div><!-- /.row -->

  </div><!-- table-responsive -->
  </section>
       
        <input type="hidden" id="orderid" value="${param.orderId}">
       <script type="text/javascript">
      function  sendCustomerInvoiceToCustomer(){
    	  var orderid = $("#orderid").val();
    	  var mail=$("#mail").val();
    	  console.log("mail..."+mail);
    	  var invoice = "<html><body style='border:2px solid;padding:10px 10px 10px 10px'>"+$("#invoice").html()+"</body></html>";
    	/*   var htmlMessage=$('#invoice').html(); */
    	  console.log("--htmlMessage..."+invoice);
    	  var totUrl = $(location).attr('href');
  		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
  		var finalUrl = newUrl+"sendTourInvoiceToMail";
  		$('#h4').show();
			 $.ajax({
				    method: "POST",
				    url:finalUrl,
				    data: {customerMail:mail,orderid:orderid,htmlMessage:invoice},
				    success:function(data,status)
					{ 
				        $.each(data, function(index, element) {
				    		  console.log("data-------"+element.status);

						     	if(element.status=="success"){
						     		$('#h4').hide();
						    		  $('#success-alert').show();
									  $('#message').text("Successfully sent mail.");
									    $('#success').click(function() {
						  					  $('#success-alert').hide();
						  					  window.location.assign($(location).attr('href'));
						  					});
						  				 
						  				
						    	}
						    	else if(element.status=="fail"){
						    		$('#h4').hide();
						    	 $('#success-alert').show();
									  $('#message').text("Failed.Try again.");
									    $('#success').click(function() {
						  					  $('#success-alert').hide();
						  					 
						  					});  
					    	}
						    	 
				    	 });
				     
				    	
				     },
					error: function(xhr, status, error)
					{
						$('#h4').hide();
					 $('#success-alert').show();
						 $('#message').text(error);
						  $('#success').click(function() {
		  					  $('#success-alert').hide();
		  					 });  
					   console.log("Error----------"+error);
					}
				});  
    	  
    	  
    			    
    		
    	 }
      
  </script>
 