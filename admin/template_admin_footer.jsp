 <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<style>
.modal-header .close {
    font-size: 25px;
    margin-top: 45px;
    margin-right: 20px;
}
.profile-sidebar-text {
    width: 100%;
    font-size: 15px;
    color: #3e4853;
    display: block;
    padding: 15px 0px 15px 25px;
    transition: 0.25s;
}
.profile-sidebar-text:hover, .profile-sidebar-text:active, .profile-sidebar-text:focus {
    background: #E5E9ED;
    transition: 0.25s;
    text-decoration: none !important;
}
.nav-section-header, .nav-account-section-header {
    background: #E5E9ED;
    text-align: center;
    color: #3e4853;
    padding: 10px;
    font-size: 12px;
    border-top: 1px solid #F2F4F6;
    border-bottom: 1px solid #F2F4F6;
    font-weight: bold;
}
.profile-sidebar-icon {
    width: 40px;
    font-size: 18px;
}
.profile-sidebar-add-text {
    width: 100%;
    float: left;
    text-decoration: none;
    position: relative;
    display: inline-flex;
    padding: 15px;
    background: #F2F4F6;
    transition: 0.25s;
    color: #3e4853;
    font-size: 14px;
    line-height: 1.5;
}
.profile-sidebar-add-text:hover {
    background: #E5E9ED;
    transition: 0.25s;
    text-decoration: none !important; 
}
.topnav-hotel-name {
    display: -webkit-inline-box;
    position: absolute;
    padding: 10px;
}
.hotel-sidebar-text {
    width: 100%;
    font-size: 15px;
    color: #3e4853;
    display: block;
    padding: 10px 0px 10px 10px;
    border-bottom: 1px solid #ddd;
    transition: 0.25s;
}
img.hotel-sidebar-img {
    width: 50px;
    height: 50px;
    border-radius: 50%;
    object-fit: cover;
}
.hotel-sidebar-text:hover {
    background: #E5E9ED;
    transition: 0.25s;
} 
.wallet-balance{
	background: #ffde00;
    padding: 5px;
    margin-top: 0px; 
    color: #353a40;
    border-radius: 50px;
    margin-top:5px;
}
</style>

 <script>
        $(document).ready(function() {

            $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });

            $('#data_2 .input-group.date').datepicker({
                startView: 1,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "dd/mm/yyyy"
            });

            $('#data_3 .input-group.date').datepicker({
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

            $('#data_4 .input-group.date').datepicker({
                minViewMode: 1,
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                todayHighlight: true
            });

            $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

            $("#input-43").fileinput({
                showPreview: false,
                allowedFileExtensions: ["zip", "rar", "gz", "tgz"],
                elErrorContainer: "#errorBlock43"
                    // you can configure `msgErrorClass` and `msgInvalidFileExtension` as well
            });

        });

    </script>

    <!--start here-->
    <!--    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.8.1/bootstrap-table.min.js"></script>-->
    <%-- <script src="admin/js/bootstrap.min.js" type="text/javascript"></script> --%>
    <script src="admin/js/salvattore.min.js"></script>
    <script src="admin/js/fileinput.js"></script>
    <script src="admin/js/Chart.min.js"></script>
    <script src="admin/js/bootstrap-datepicker.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
	<script src="admin/js/bootstrap-select.min.js"></script>
	<script src="admin/js/jquery.flexslider-min.js"></script>
	<script src="admin/js/jquery.validate.min.js"></script>
	<script src="admin/js/clockpicker.js"></script>
	<script src="admin/js/jquery.bxslider.min.js"></script>
	<script src="admin/js/switchery.min.js"></script>
	  <script src="admin/js/bootstrap-notify.min.js"></script>
	<script src="admin/js/owl.carousel.min.js"></script>
    <script src="admin/js/main.js"></script>
	 <script src="admin/js/bootstrapValidator.min.js"></script>
	 <script type="text/javascript" src="admin/js/alertify.js"></script>
  <!-- footer  -->
    <footer class="footer">
        <span class="text-left" style="font-size: 12px;">
           <a href="http://touroxy.com" target="_blank">TourOxy</a> © 2017 Travel. 
        </span>
        <span class="float-xs-right" style="font-size: 12px;">
            Powered by : <a href="http://spysr.in" target="_blank">SpYsR</a>
        </span>
    </footer>
 <!-- footer end -->
 
<!-- header Account dropdown start-->
<div class="modal right fade" id="profile" role="dialog" aria-hidden="false" style="display: none; padding-right: 15px;margin-top:55px;">
					<div class="modal-dialog modal-lg" style="width:  320px;">
						<!-- Modal content-->
						<div class="modal-content" style="border: none;border-radius: 0px;">
							<div class="modal-header">
								<h4 class="modal-title text-left text-black">Account</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
									<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
								</button>
							</div>
							<div class="">
								 <h5 class="wallet-balance visible-xs" style="border-radius:0px;margin-top:0px;"><s:text name="tgi.label.wallet_balance" /> : <span class="walletAmount"></span></h5>
								<a href="agentDetails?id=<s:property value='%{#session.User.id}'/>" class="profile-sidebar-text"><i class="fa fa-user profile-sidebar-icon"></i> My Profile</a>
								<s:if test="%{((#session.User.userRoleId.isSuperUser()|| #session.Company.companyRole.isDistributor() || #session.Company.companyRole.isAgent()) && #session.User.userRoleId.isAdmin())}">
          						<a href="companyDetails?id=<s:property value='%{#session.Company.id}'/>" class="profile-sidebar-text"><i class="fa fa-building profile-sidebar-icon"></i>Your Company</a>
          						</s:if>
								 <a href="resetCompanyUserPassword?id=<s:property value='%{#session.User.id}'/>" class="profile-sidebar-text"><i class="fa fa-key profile-sidebar-icon"></i> Change Password</a>
								 <!-- <a href="#!" class="profile-sidebar-text"><i class="fa fa-bell profile-sidebar-icon"></i> Notification</a> -->
								 <!-- <a href="#!" class="profile-sidebar-text"><i class="fa fa-phone profile-sidebar-icon"></i> Contacts</a> -->
								 <!-- <a href="#!" class="profile-sidebar-text"><i class="fa fa-desktop profile-sidebar-icon"></i> My Devices</a> -->
								 <!-- <a href="#!" class="profile-sidebar-text"><i class="fa fa-cog profile-sidebar-icon"></i> Chanel Manager</a> -->
								 <a href="logout" class="profile-sidebar-text"><i class="fa fa-sign-out profile-sidebar-icon"></i> Log Out</a>
								 <div class="nav-account-section-header">Add More to your account</div>
							<s:if test="%{((#session.User.userRoleId.isSuperUser()|| #session.Company.companyRole.isDistributor()) && #session.User.userRoleId.isAdmin())}">			 
			                <a href="addCompany" class="profile-sidebar-add-text"> <i class="fa fa-plus profile-sidebar-icon"></i> Add New Company </a> 
			                </s:if>
			                <s:if test="%{((#session.User.userRoleId.isSuperUser()|| #session.Company.companyRole.isDistributor() || #session.Company.companyRole.isAgent()) && #session.User.userRoleId.isAdmin())}">
			                <a href="userRegister" class="profile-sidebar-add-text"> <i class="fa fa-plus profile-sidebar-icon"></i> Add New User </a> 
			                </s:if>
							</div>
						</div>
					</div>
				</div>
<!-- header Account dropdown End-->
 
 <!-- header Hotel dropdown start-->
<div class="modal right fade" id="hotels" role="dialog" aria-hidden="false" style="display: none; padding-right: 15px;margin-top:55px;">
					<div class="modal-dialog modal-lg" style="width:  320px;">
						<!-- Modal content-->
						<div class="modal-content" style="border: none;border-radius: 0px;">
							<div class="modal-header">
								<h4 class="modal-title text-left text-black">Hotels</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
									<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
								</button>
							</div>
							<div class="">
								 <a href="#!" class="hotel-sidebar-text"><img class="hotel-sidebar-img" src="admin/img/banner.png"> <span class="topnav-hotel-name"> Cunda Deniz Hotel  <br>  <small class=""> Property ID: 423022 </small></span>
								 </a>
								 <a href="#!" class="hotel-sidebar-text"><img class="hotel-sidebar-img" src="admin/img/banner.png"> <span class="topnav-hotel-name"> Cunda Deniz Hotel  <br>  <small class=""> Property ID: 423022 </small></span>
								 </a>
								 <a href="#!" class="hotel-sidebar-text"><img class="hotel-sidebar-img" src="admin/img/banner.png"> <span class="topnav-hotel-name"> Cunda Deniz Hotel  <br>  <small class=""> Property ID: 423022 </small></span>
								 </a>
								 <a href="#!" class="hotel-sidebar-text"><img class="hotel-sidebar-img" src="admin/img/banner.png"> <span class="topnav-hotel-name"> Cunda Deniz Hotel  <br>  <small class=""> Property ID: 423022 </small></span>
								 </a>
								 <a href="#!" class="hotel-sidebar-text"><img class="hotel-sidebar-img" src="admin/img/banner.png"> <span class="topnav-hotel-name"> Cunda Deniz Hotel  <br>  <small class=""> Property ID: 423022 </small></span>
								 </a>
								 <div class="nav-account-section-header">Add properties to your account</div>
								 
                <a href="#!" class="profile-sidebar-add-text"> <i class="fa fa-plus profile-sidebar-icon"></i> Add new property </a> 
                <a href="#!" class="profile-sidebar-add-text"> <i class="fa fa-plus profile-sidebar-icon"></i> Add Existing Properties </a> 
							</div>
						</div>
					</div>
				</div>
<!-- header Hotel dropdown End-->

<!-- header Help dropdown start-->
<div class="modal right fade" id="help" role="dialog" aria-hidden="false" style="display: none; padding-right: 15px;margin-top:55px;">
					<div class="modal-dialog modal-lg" style="width:  320px;">
						<!-- Modal content-->
						<div class="modal-content" style="border: none;border-radius: 0px;">
							<div class="modal-header">
								<h4 class="modal-title text-left text-black">Help</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
									<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
								</button>
							</div>
							<div class="">
								 <a href="#!" class="profile-sidebar-text"><i class="fa fa-key profile-sidebar-icon"></i> Change Password</a>
								 <a href="#!" class="profile-sidebar-text"><i class="fa fa-bell profile-sidebar-icon"></i> Notification</a>
								 <a href="#!" class="profile-sidebar-text"><i class="fa fa-phone profile-sidebar-icon"></i> Contacts</a>
								 <a href="#!" class="profile-sidebar-text"><i class="fa fa-desktop profile-sidebar-icon"></i> My Devices</a>
								 <a href="#!" class="profile-sidebar-text"><i class="fa fa-cog profile-sidebar-icon"></i> Chanel Manager</a>
								 <a href="#!" class="profile-sidebar-text"><i class="fa fa-sign-out profile-sidebar-icon"></i> Log Out</a>
								 <div class="nav-account-section-header">Add properties to your account</div>
								 
                <a href="#!" class="profile-sidebar-add-text"> <i class="fa fa-plus profile-sidebar-icon"></i> Add new property </a> 
                <a href="#!" class="profile-sidebar-add-text"> <i class="fa fa-plus profile-sidebar-icon"></i> Add Existing Properties </a> 
							</div>
						</div>
					</div>
				</div>
<!-- header Help dropdown End-->
 
 
 
 
    
     <div class="modal fade" id="alert_box" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
		<div class="modal-dialog">
		    <div class="modal-content">
		          <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
		     <h4 class="modal-title custom_align" id="alert_box_heading"><s:text name="tgi.txt.alert"/></h4>
		   </div>
		       <div class="modal-body">
		    
		    <div class="alert alert-success" id="alert_box_body"></div>
		     
		    </div>
		     <div class="modal-footer">
		     <button type="button" class="btn btn-success" data-dismiss="modal" data-target="#alert_box_ok" id="alert_box_ok"> <s:text name="tgi.txt.ok"/></button>
		     
        <%-- <button type="button" class="btn btn-success"  id="alert_box_ok"><span class="glyphicon glyphicon-ok-sign"></span> OK</button> --%>
      </div>
		      </div>
		</div>
</div>

 
<div class="modal fade" id="alert_box_info" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
		<div class="modal-dialog">
		    <div class="modal-content">
		          <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
		     <h4 class="modal-title custom_align" id="alert_box_heading"><s:text name="tgi.txt.alert"/></h4>
		   </div>
		       <div class="modal-body">
		    
		    <div class="alert alert-danger" id="alert_box_info_body"></div>
		     
		    </div>
		     <div class="modal-simple-footer">
		     <!-- <button type="button" class="btn btn-default" data-dismiss="modal" data-target="#alert_box_ok" id="alert_box_info_ok">Close</button> -->
		     
        <button type="button" class="btn btn-danger" data-dismiss="modal"  id="alert_box_info_ok"><span class="glyphicon glyphicon-ok-sign"></span> <s:text name="tgi.txt.ok"/></button>
      </div>
		      </div>
		</div>
</div> 


<div class="modal fade" id="alert_box_delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
		<div class="modal-dialog">
		    <div class="modal-content">
		          <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
		     <h4 class="modal-title custom_align" id="alert_box_heading"><s:text name="tgi.txt.alert"/></h4>
		   </div>
		       <div class="modal-body">
		    
		    <div class="alert alert-danger" id="alert_box_delete_body"></div>
		     
		    </div>
		     <div class="modal-simple-footer">
		     <button type="button" class="btn btn-default" data-dismiss="modal" data-target="#alert_box_delete_ok" id="alert_box_delete_cancel"><s:text name="tgi.label.no"/></button>
        	<button type="button" class="btn btn-danger"  id="alert_box_delete_ok"><span class="glyphicon glyphicon-ok-sign"></span>YES</button>
      </div>
		      </div>
		</div>
</div>
<!--  -->




    
    
    
 <div class="modal fade" id="alert_box_info" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
		<div class="modal-dialog">
		    <div class="modal-content">
		          <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
		     <h4 class="modal-title custom_align" id="alert_box_heading"><s:text name="tgi.txt.alert"/></h4>
		   </div>
		       <div class="modal-body">
		    
		    <div class="alert alert-danger" id="alert_box_info_body"></div>
		     
		    </div>
		     <div class="modal-simple-footer">
		     <!-- <button type="button" class="btn btn-default" data-dismiss="modal" data-target="#alert_box_ok" id="alert_box_info_ok">Close</button> -->
		     
        <button type="button" class="btn btn-success" data-dismiss="modal"  id="alert_box_info_ok"><span class="glyphicon glyphicon-ok-sign"></span>  <s:text name="tgi.txt.ok" /></button>
      </div>
		      </div>
		</div>
</div> 

<script>
function showModalPopUp(msg_success,code)
{
	if(code==="e")
		{
			$('#alert_box_info_body').removeClass("alert-success").removeClass("alert-info").removeClass("alert-danger").addClass("alert-danger");
		}
	if(code==="s")
		{
			$('#alert_box_info_body').removeClass("alert-success").removeClass("alert-info").removeClass("alert-danger").addClass("alert-success");
		}
	if(code==="i")
		{
			$('#alert_box_info_body').removeClass("alert-success").removeClass("alert-info").removeClass("alert-danger").addClass("alert-info");
		}
	if(code==="w")
		{
			$('#alert_box_info_body').removeClass("alert-success").removeClass("alert-info").removeClass("alert-danger").addClass("alert-warning");
		}
 $('#alert_box_info')
 .find('#alert_box_info_body').html(msg_success).end()
 .modal('show');
 }
 

function notifySuccess()
{
	$.notify({
		icon: 'glyphicon glyphicon-star',
		message: "You did a great job, Please wait ....",
		offset: 50
	},
	{
		type: "info",
	});
}
function notifySavingMsg()
{
	$.notify({
		icon: 'glyphicon glyphicon-star',
		message: "Saving Detail, Do not close this page...",
		offset: 20
	},
	{
		type: "info",
	});
}
function notifySaving()
{
	var notify = $.notify('<strong>Saving</strong> Do not close this page...', {
		allow_dismiss: false,
		showProgressbar: true
	});

	setTimeout(function() {
		notify.update({'type': 'success', 'message': '<strong>Success</strong> Your page has been saved!', 'progress': 25});
	}, 4000);
}
function createAnotherUpdateMsg(id, statusId,type)
{
	//$("#"+statusId).html(type+" Created Successfully.");
	$("#"+id).html('Create Another');
	$("#"+id).val('Create Another');
}
</script>
  
  
<script>
$('input[type=checkbox]').change(function ()
{
    if ($(this).prop("checked")) 
    {
		$(this).val(true);
    }
    else
    {
    	$(this).val(false);
    }
});
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

</script>