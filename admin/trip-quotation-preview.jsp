<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<link href="admin/css/jquery-ui.css" rel="stylesheet" type="text/css" />
 <style>
 p {
    margin: 0 0 0px;
}
.pnl1 {
    margin-top: 15px;
    border: 1px solid #eaeaea;
    background: whitesmoke;
    padding: 0px 0px 1px 10px;
    border-radius: 6px;
}
.panel-body {
    background: #eaeaea;
}
.item-list {
    padding: 10px;
}
 </style>

 <section class="wrapper container-fluid">
                <div class="row">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left">Trip Quotation Preview</h5>
                                 	<div class="set pull-right">
                                 	<a class="btn btn-default btn-xs" href="tourOrderList"><span class="pull-right">
										<span class="glyphicon glyphicon-step-backward"></span><s:text name="tgi.label.back" /></span></a>
								 	
								 	<a class="btn btn-success btn-xs" href="tourOrderList"><span class="pull-right">
										<i class="fa fa-envelope-o" aria-hidden="true"></i> Send Email</span></a>
								 	
                                     </div></div>


		<!-- Main content -->
		<section class="col-md-12">
				<div class="row">
				<div class="profile-timeline-card"> 
				<p class="pt-0 pb-2 mb-2">
					<span class="text-center"></span><span class="text-warning text-center"> ${tripQuotation.title}</span>
				</p>
								<hr>
								<p class="pt-0 pb-2 mb-2 mt-3">
										<span class="company-font-icon"><i class="fa fa-location-arrow"></i></span> <span> <b class="blue-grey-text">Trip Departure Details</b></span>
										<span class="pull-right">
										<a class="btn btn-sm btn-default" data-event="duplicate" data-toggle="modal" data-target="#edit-trip-quotation" >
										<img class="clippy" src="admin/img/svg/edit.svg" width="14"><span class="">&nbsp;Edit</span>
										</a>
										</span>
								</p> 
									<div class="no-table-css clearfix">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
									<th>Departure Date</th>
									<th>Check In Date</th>
									<th>Check Out Date</th>
									<th>Arrival Date</th>
									<th><s:text name="tgi.label.destiation_city" /></th>
									<th><s:text name="tgi.label.destination_country" /></th>
								</tr></thead>
									<tr>
									<td data-title="Departure Date">
									<c:if test="${tripQuotation != null && tripQuotation.departureDate != null}">
									${spysr:formatDate(tripQuotation.departureDate,'yyyy-MM-dd HH:mm', 'MMM/d/yyyy hh:mm a')}
									</c:if>
									</td>
									<td data-title="Check In Date">
									<c:if test="${tripQuotation != null && tripQuotation.startDate != null}">
									${spysr:formatDate(tripQuotation.startDate,'yyyy-MM-dd HH:mm', 'MMM/d/yyyy hh:mm a')}
									</c:if>
									</td>
									<td data-title="Check Out Date">
									<c:if test="${tripQuotation != null && tripQuotation.endDate != null}">
									${spysr:formatDate(tripQuotation.endDate,'yyyy-MM-dd HH:mm', 'MMM/d/yyyy hh:mm a')}
									</c:if>
									</td>
									<td data-title="Arrival Date">
									<c:if test="${tripQuotation != null && tripQuotation.arrivalDate != null}">
									${spysr:formatDate(tripQuotation.arrivalDate,'yyyy-MM-dd HH:mm', 'MMM/d/yyyy hh:mm a')}
									</c:if>
									</td>
										<td data-title="<s:text name="tgi.label.destiation_city" />">${tripQuotation.destinationFrom}</td>
										<td data-title="<s:text name="tgi.label.destination_country" />">${tripQuotation.destinationTo}</td>
									</tr></table></div>
									</div>
									<!-- edit trip quotation  -->
									</div>
								 
									
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-globe"></i></span> <span> <b class="blue-grey-text"><s:text name="tgi.label.tour_details" /> </b></span>
									</p>   
							<c:if test="${itineraryMap != null && itineraryMap.size()>0}">
								<div class="panel panel-default" style="padding-bottom: 0px;">
									<div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2"
												aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse"
												data-parent="#accordion" href="#collapse2"
												aria-expanded="true" aria-controls="collapseOne"> <i
												class="more-less glyphicon glyphicon-plus pull-right"></i> <s:text name="tgi.label.itinerary_description" />
											</a>
										</h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body" style="padding-top: 0px;">
											<c:forEach items="${itineraryMap}" var="itineraryObj" varStatus="status">
												<br>
												<div class="pull-right">
												<a class="btn btn-xs btn-primary" data-event="duplicate" data-toggle="modal" data-target="#edit_trip_quotation_itinerary_${status.count}">Edit</a>
												<a class="btn btn-xs btn-danger" data-itinerary-id="${itineraryObj.value.id}" onclick="javascritp:delteTripItinerary(this)">Delete</a>
												</div>
												<li><strong style="color: #c20a2f; font-weight: 700;">${itineraryObj.key} Day</strong></li>
												<li><b>${itineraryObj.value.title}</b></li>
												<li>${itineraryObj.value.description}</li>
												<li class="divider" style="border-bottom: 1px solid #cecece;"></li>
												<li class="mt-2"><b>Hotel :</b> ${itineraryObj.value.hotelName}</li>
												<li><b>City :</b> ${itineraryObj.value.hotelCity}</li>
												<li><b>Rating :</b> ${itineraryObj.value.hotelRating}</li>
											<!-- edit trip itinerary -->
											<div class="modal fade" id="edit_trip_quotation_itinerary_${status.count}" role="dialog">
													<div class="modal-dialog">
														<!-- Modal content-->
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title text-center">Edit Trip Quotation Itinerary</h4>
																<button type="button" class="close slds-modal__close" data-dismiss="modal">
																	<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
																</button>
															</div>
															<form id="updateTripQuotationItineraryForm_${status.count}" method="POST" class="bv-form">
																<input type="hidden" id="itinerary-id" name="id" value="${itineraryObj.value.id}" />
																<div class="modal-body">
																	<div class="row">
																		<div class="form-group">
																			<label class="col-md-2 col-form-label">Day <span id="day-to-add">${itineraryObj.key}</span>: </label>
																			<div class="col-md-10"><span data-toggle="tooltip" data-placement="top" title="Ininerary Title" >
																					<input type="text" class="form-control" name="title" id="title" autocomplete="off" value="${itineraryObj.value.title}">
																					</span>
																			</div>
																		</div>
																		<div class="col-md-12">
																			<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">&nbsp;</label>
																			<div class="controls">
																				<div class="form-group"><span data-toggle="tooltip" data-placement="top" title="Ininerary Description" >
																					<textarea class="form-control" name="description" id="description" autocomplete="off">${itineraryObj.value.description}</textarea>
																				</span></div>
																			</div>
																		</div>
																		<hr>
																		<div class="col-md-12">
																			<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Hotel Name</label>
																			<div class="controls">
																				<div class="form-group">
																					<input type="text" class="form-control" name="hotelName" id="hotel-name" autocomplete="off" value="${itineraryObj.value.hotelName}">
																				</div>
																			</div>
																		</div>
																		<div class="col-md-12">
																			<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Hotel City</label>
																			<div class="controls">
																				<div class="form-group">
																				<input type="text" class="form-control" name="hotelCity" id="hotel-city" autocomplete="off" value="${itineraryObj.value.hotelCity}">
																				</div>
																			</div>
																		</div>
																		<div class="col-md-12">
																			<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Rating</label>
																			<div class="controls">
																				<div class="form-group">
																				<input type="text" class="form-control" name="hotelRating" id="hotel-rating" autocomplete="off" value="${itineraryObj.value.hotelRating}">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
																		<button type="submit" class="btn btn-primary edit-trip-quotation-itinerary-btn" data-form-name`="updateTripQuotationItineraryForm_${status.count}" data-modal-id="edit_trip_quotation_itinerary_${status.count}">Save</button>
																	</div>
																</form>
															</div>
													
														</div>
													</div>
											</c:forEach>
										</div>
									</div>
								</div>
							</c:if>

							<c:if test="${quotationInclusionList != null && quotationInclusionList.size() >0}">
								<div class="panel panel-default">
									<div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapseOne" class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapseOne"> 
												<i class="more-less glyphicon glyphicon-plus pull-right"></i> Trip Inclusion
											</a>
										</h4>
									</div>
									<div id="collapse3" class="panel-collapse collapse"
										role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="long-description ">
												<c:forEach items="${quotationInclusionList}" var="inclusion" varStatus="status">
													<c:choose>
													<c:when test="${inclusion.title != null && inclusion.title != ''}">
													<div class="pnl1">
													<h4 style="color: #c20a2f; font-weight: 700;font-size:18px;" >${inclusion.title}</h4>
													</div>
													</c:when>
													<c:otherwise>
													
													</c:otherwise>
													</c:choose>
													<div class="item-list">
														<span class="" style="font-size:12px"><i class="fa fa-hand-o-right" style="font-size:12px;color: #8d1735;"></i><span style="font-size:16px">  ${inclusion.description}</span></span>
													</div>
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
							</c:if>
							</div></div> 
							
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-list-alt"></i></span> <span> <b class="blue-grey-text">Price & Rates</b></span>
										<span class="pull-right">
										<a class="btn btn-sm btn-default" data-event="duplicate" data-toggle="modal" data-target="#edit-trip-quotation-rate" >
										<img class="clippy" src="admin/img/svg/edit.svg" width="14"><span class="">&nbsp;Edit</span>
										</a>
										</span>
									</p> 
								<div class="table-responsive no-table-css clearfix mb-0">
								 <table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
								<th>Supplier Price</th>
								<th>Markup Price</th>
								<th>GST (%)</th>
								<th>Base Price</th>
								<th>Processing Fee (%)</th>								
								<th>Total Amount</th>								
								<th>Price Currency</th>								
								</tr></thead>
								<tr>
										<td data-title="<s:text name="tgi.label.total_passengers" />">${tripQuotation.supplierPrice}</td>
										<td data-title="<s:text name="tgi.label.base_amount" />">${tripQuotation.markup}</td>
										<td data-title="<s:text name="tgi.label.fee_amount" />">${tripQuotation.gst}</td>
										<td data-title="<s:text name="tgi.label.fee_amount" />">${tripQuotation.basePrice}</td>
										<td data-title="<s:text name="tgi.label.fee_amount" />">${tripQuotation.processingFee}</td>
										<td data-title="<s:text name="tgi.label.fee_amount" />">${tripQuotation.totalAmount}</td>
										<td data-title="<s:text name="tgi.label.total" />">INR</td>
									</tr>
								</table></div></div></div>
								<div class="row">
								<div class="profile-timeline-card">
								<p class="pt-0 pb-2 mb-2">
										<span class="company-font-icon"><i class="fa fa-list-alt"></i></span> <span> <b class="blue-grey-text">Cancellation & Refund Policy</b></span>
								</p> 
								<hr>
								<div class="table-responsive no-table-css clearfix mb-0">
								<table class="table table-bordered table-striped-column table-hover mb-0">
                                   <thead>
                                   <tr class="border-radius border-color">
								<th>SNo.</th>
								<th>Days</th>
								<th>Amount (%)</th>
								<th>Remarks</th>
								</tr></thead>
								<c:forEach items="${quotationCancellationPolicyList}" var="cancellationObj" varStatus="status">
								<tr>
										<td data-title="SNo">${status.count}</td>
										<td data-title="Days">${cancellationObj.cancellationDay}</td>
										<td data-title="Amount (%)">${cancellationObj.feeAmount}</td>
										<td data-title="Remarks">${cancellationObj.remarks}</td>
									</tr>
								</c:forEach>
								</table>
								</div>
								</div>
								</div>
								
		</section>
	</div></div></div></section>
<div class="models">
<!--  -->
<div class="modal fade" id="edit-trip-quotation" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title text-center">Edit Trip Quotation</h4>
							<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
							</button>
						</div>
						<form id="updateTripQuotationInfoForm" method="post" class="bv-form">
							<input type="hidden" id="info-id" name="id" value="${tripQuotation.id}" />
							<div class="modal-body">
								<div class="row">
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Day Info</label>
										<div class="controls">
											<div class="form-group" >
												<input type="text" name="title" id="title" class="form-control" value="${tripQuotation.title}">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Destination From</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" class="form-control" name="destinationFrom" id="" autocomplete="off" value="${tripQuotation.destinationFrom}">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Destination To</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" class="form-control" name="destinationTo" autocomplete="off" id="destinationTo" value="${tripQuotation.destinationTo}">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Departure Date :</label>
										<div class="controls">
											<div class="form-group">
											<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													<input type="text" class="form-control datetimepicker1"  name="departureDate" id="departureDate" autocomplete="off" value="${spysr:formatDate(tripQuotation.departureDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}" />
											</div></div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Check In Date :</label>
										<div class="controls">
											<div class="form-group">
											<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													<input type="text" class="form-control datetimepicker1"  name="startDate" id="startDate" autocomplete="off" value="${spysr:formatDate(tripQuotation.startDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}">
											</div></div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Check Out Date </label>
										<div class="controls">
											<div class="form-group">
											<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													<input type="text" class="form-control datetimepicker1"  name="endDate" id="endDate" autocomplete="off" value="${spysr:formatDate(tripQuotation.endDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}">
											</div></div>
										</div>
									</div>
									<div class="col-md-12">
										<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Arrival Date :</label>
										<div class="controls">
											<div class="form-group">
											<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													<input type="text" class="form-control datetimepicker1"  name="arrivalDate" id="arrivalDate" value="${spysr:formatDate(tripQuotation.arrivalDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}">
											</div></div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
								<button type="submit" class="btn btn-primary" id="update-trip-quotation-info-btn">Save</button>
							</div>
						</form>
					</div>

				</div>
			</div>
<!--  -->
<div class="modal fade" id="edit-trip-quotation-rate" role="dialog">
<div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title text-center">Edit Trip Quotation Rate</h4>
			<button type="button" class="close slds-modal__close" data-dismiss="modal">
				<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="15" alt="Copy to clipboard">
			</button>
		</div>
		<form id="updateTripQuotationRatesForm" method="post" class="bv-form">
			<input type="hidden" id="rate-id" name="id" value="${tripQuotation.id}" />
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Supplier Price</label>
						<div class="controls">
							<div class="form-group" >
								<input type="text" class="form-control" name="supplierPrice" id="supplier-price" autocomplete="off" value="${tripQuotation.supplierPrice}">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Markup Price</label>
						<div class="controls">
							<div class="form-group">
								<input type="text" class="form-control" name="markup" id="markup_price" autocomplete="off" value="${tripQuotation.markup}">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">GST (%)</label>
						<div class="controls">
							<div class="form-group">
								<input type="text" class="form-control" name="gst" id="gst-percentage" autocomplete="off" value="${tripQuotation.gst}">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Base Price</label>
						<div class="controls">
							<div class="form-group">
							<input type="text" class="form-control" name="basePrice" id="base-price" autocomplete="off" readonly="readonly" value="${tripQuotation.basePrice}">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Processing Fee (%)</label>
						<div class="controls">
							<div class="form-group">
							<input type="text" class="form-control" name="processingFee" id="processing-fee" autocomplete="off" value="${tripQuotation.processingFee}">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<label class="form-control-label" style="font-size: 13px; color: #0f1010;" for="formGroupInputSmall">Total Amount</label>
						<div class="controls">
							<div class="form-group">
							<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-inr" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="totalAmount" id="total-amount" autocomplete="off" readonly="readonly" value="${tripQuotation.supplierPrice}">
							</div></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
					<button type="submit" class="btn btn-primary" id="save-trip-quotation-rate-btn">Save</button>
				</div>
			</form>
		</div>

	</div>
</div>

</div>
	<script>
$(function () {
    $('.datetimepicker1').datetimepicker({
    	format: 'MM/DD/YYYY hh:mm A',
    	sideBySide : true,
    	
    }).on('dp.hide', function() {
        $(this).blur();
    });
 });
 
 /*-----------------------------*/
 $(document).ready(function(){
		$('#supplier-price, #markup_price, #gst-percentage , #processing-fee').keyup(function() {
			var suuplierPriceInput= $("#supplier-price").val();
			var markUpInput = $("#markup_price").val();
			var gstInput = $("#gst-percentage").val();
			var processingFeeInput = $("#processing-fee").val();
			
			var supplierPrice = parseFloat(suuplierPriceInput!=undefined && suuplierPriceInput!="" ?suuplierPriceInput:0);
			var markUp = parseFloat(markUpInput!=undefined && markUpInput!="" ?markUpInput:0);
			var gst = parseFloat(gstInput!=undefined && gstInput!="" ?gstInput:0);
			var processingFee = parseFloat(processingFeeInput!=undefined && processingFeeInput!="" ?processingFeeInput:0);
		
			
			var baseAmount = 0;
			var totalAmount = 0;
			console.log(supplierPrice);
			var markUpAmount=(supplierPrice+markUp);
			baseAmount = parseFloat((markUpAmount*gst)/100)+markUpAmount;
			totalAmount = parseFloat((baseAmount*processingFee)/100)+baseAmount;
			// display all values
			 $("#base-price").val(baseAmount);
			 $("#total-amount").val(totalAmount);
	});
});
 
 /*--------------------------------------------------------*/
 	$("#updateTripQuotationRatesForm").submit(function(e) {
			e.preventDefault();
			notifySuccess();
			$.ajax({
				url : "updateTripQuotationRate",
				type : "POST",
				dataType : 'json',
				data : $("#updateTripQuotationRatesForm").serialize(),
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						<%-- var url = '<%=request.getContextPath()%>/editTravelSalesLead';
						window.location.replace(url+'?id='+jsonData.json.id2);	 --%>
						setTimeout(location.reload.bind(location), 1000);
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message);}
					
				},
				error : function(request, status, error) {
					showModalPopUp("Trip quotation can not be updated.", "e");
				}
			});
		});
 
 	$("#updateTripQuotationInfoForm").submit(function(e) {
			e.preventDefault();
			notifySuccess();
			$.ajax({
				url : "updateTripQuotationDetail",
				type : "POST",
				dataType : 'json',
				data : $("#updateTripQuotationInfoForm").serialize(),
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						<%-- var url = '<%=request.getContextPath()%>/editTravelSalesLead';
						window.location.replace(url+'?id='+jsonData.json.id2);	 --%>
						setTimeout(location.reload.bind(location), 1000);
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message);}
					
				},
				error : function(request, status, error) {
					showModalPopUp("Trip quotation can not be updated.", "e");
				}
			});
		});
 	
 	$(".edit-trip-quotation-itinerary-btn").click(function(e) {
			e.preventDefault();
			notifySuccess();
			var formDeleteName = $(this).attr("data-form-name");
			var modalId = $(this).attr("data-modal-id");
			$.ajax({
				url : "updateQuotationItinerary",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						<%-- var url = '<%=request.getContextPath()%>/editTravelSalesLead';
						window.location.replace(url+'?id='+jsonData.json.id2);	 --%>
						setTimeout(location.reload.bind(location), 1000);
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message);}
				},
				error : function(request, status, error) {
					showModalPopUp("Trip quotation can not be updated.", "e");
				}
			});
		});
</script>
<script>
function delteTripItinerary(sel) {
	// confirm dialog
	 alertify.confirm("Are you sure delete this itinerary", function () {
		 var ItinId = $(sel).data("itinerary-id");
			$.ajax({
				url : "deleteTripQuotationItinerary?id="+ItinId,							
				type : "GET",
				dataType: 'json',
				success : function(jsonData) {
					if(jsonData.json.status == 'success'){
						alertify.success(jsonData.json.message);
						setTimeout(location.reload.bind(location), 1000);	
					}
	  				else if(jsonData.json.status == 'error'){
	  					alertify.error(jsonData.json.message, function(ev) {
	  					    alertify.error("Ok Try again");
	  					});
	  				}
				},
				error: function (request, status, error) {
					showModalPopUp("Somthing Wrong, please try again.","e");
				}
			});
	 }, function() {
		 alertify.error("You've clicked Cancel");
	 });
	
}

</script>