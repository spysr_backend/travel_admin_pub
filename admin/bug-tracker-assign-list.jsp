<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <link href="admin/css/inner-css/bug-tracker.css" rel="stylesheet" type="text/css">
<style>

</style>

   <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid"> 
                <div class="">
                    <div class="">
                        <div class="card1">
                        <div class="pnl">
                        <div class="hd clearfix">
					<h5 class="pull-left">My Bug</h5>
					<div class="set pull-right hidden-xs" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
				</div>
                                 <br>
                         		<form action="listTravelSalesLead" class="filter-form" id="resetform">
								<div class="filter_bar" id="filterDiv" style="display: none;">
								<div class="form-group">
								<div class="col-md-2 col-sm-6">
									<div class="form-group">
										<select name="createdBy" id="created-by" class="form-control input-sm input-sm">
											<option  selected="selected">Select Created By</option>
											<option value="all"><s:text name="tgi.label.all" /></option>
											<option value="my"><s:text name="tgi.label.ibe" /></option>
											<option value="distributor"><s:text name="tgi.label.admin" /></option>
										</select>
									</div>
								</div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="bookingStartDate" id="bookingStartDate" placeholder="Created Start Date...." class="form-control input-sm search-query date1 " />
								 </div></div>
							   
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="firstName" id="name-json" placeholder="Search By Name" class="form-control input-sm search-query" />
							   </div>
							   </div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="email" id="email" placeholder="Search By Email" class="form-control input-sm search-query" />
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<input type="text" name="phone" id="phone" placeholder="Search By Mobile" class="form-control input-sm search-query" />
								</div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="city" id="city-json" placeholder="Search By City" class="form-control input-sm search-query" />
								</div>
								</div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select name="country" id="country" class="form-control usst input-sm" data-live-search="true">
								    <option value="">Select Country</option>
									<c:forEach items="${countryList}" var="country">
										<option value="${country.countryCode}" >${country.countryName}</option>
									</c:forEach>
								</select> 
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="departureDateFlag" id="country-json" placeholder="Search By Departure Date..." class="form-control input-sm search-query" />
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select aria-hidden="true" name="leadSource" id="leadSource" class="form-control input-sm">
								    <option value="">Lead Source*</option>
									<option value="web">Website</option>
									<option value="socal">Socal Network</option>
									<option value="walkin">Walk-In</option>
									<option value="telephone">Telephone</option>
									<option value="b2b">B2B</option>
									<option value="justdial">Justdial</option>
									<option value="other">Others</option>
								</select>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select aria-hidden="true" name="callTime" id="callTime" class="form-control input-sm">
								  <option value="" selected="SELECTED">Best time to call</option>
									<option value="morning">Morning</option>
									<option value="afternoon">Afternoon</option>
									<option value="evening">Evening</option>
									<option value="anytime">Any time</option>
								</select>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select name="terminate" id="terminate" class="form-control input-sm">
								<option value="" selected="selected">Lead Terminate</option>
								<option value="1"><s:text name="tgi.label.yes" /></option>
                                <option value="0" ><s:text name="tgi.label.no" /></option>
                                </select>
								 </div></div>
							<div class="row">
							 <div class=""></div>
							   <div class="col-md-1">
								<div class="form-group">
								<button type="reset" class="btn btn-danger btn-sm btn-block" id="configreset">&nbsp;Clear&nbsp;</button>
							</div>
							</div>
								<div class="col-md-1">
								<div class="form-group">
								<button type="submit" class="btn btn-info btn-sm btn-block" value="Search" >Search</button>
							</div>
							</div>
							<input type="hidden" name="filterFlag" id="filterFlag">
							</div>
						</div></div>
				</form>
                            
                                   
<div class="dash-table">
			<div class="content mt-0 pt-0">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"  role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
                                        <thead>
                                        <tr class="table-sub-header">
                                        	<th colspan="15" class="noborder-xs">
                                        	<div class="pull-right" style="padding:4px;margin-top: -6px;margin-bottom: -10px;">   
                                        	<div class="btn-group"> 
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Travel Sales Lead">
											  <a class="btn btn-sm btn-default" href="addTravelSalesLead" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class="">&nbsp;New&nbsp;</span></a> 
											</div>
											<div class="dropdown pull-left" style="margin-right:5px;">
											  <a class="btn btn-sm btn-default " href="#" >&nbsp;Import&nbsp;</a> 
											</div>
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a  class="btn btn-sm btn-default filterBtn" data-toggle="dropdown"  style="margin-bottom: 2px;margin-right: 3px;" title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
										<tr class="border-radius border-color" role="row">
                                                   	<th></th>
                                                   	<th style="width: 50px;" data-priority="1">
															<div class="center">
															<div data-toggle="tooltip" data-placement="top" title="Select All">
															<div class="checkbox checkbox-default">
										                        <input id="master_check" data-action="all" type="checkbox" >
										                        <label for="master_check">&nbsp;</label>
										                    </div>
										                    </div>
										                    </div>
													</th>
                                                    <th class="column-max-width"> Type</th>
                                                    <th data-priority="2">Bug Id</th>
                                                    <th data-priority="2">Title</th>
                                                    <th class="">Type</th>
													<th class="">Priority</th>
													<th class="">Status</th>
                                                    <th data-priority="3">Created By</th>
                                                    <th data-priority="4">Assignee</th>
                                                    <th data-priority="6">Assigned To</th>
                                                    <th data-priority="5">Assigned At</th>
                                                    <th>Created</th>
                                                    <th>Updated</th>
                                                    <th class="column-max-width">Action</th>
                                                     <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
   											<s:if test="assignBugTrackerList.size > 0">
                                           <s:iterator value="assignBugTrackerList" status="rowstatus">
                                                <tr>
                                                <td scope="row" data-title="S.No"><div class="center"><s:property value="%{#rowstatus.count}" /></div></td>
                                                <td data-title="Select" class="select-checkbox">
																<div class="center">
																<div class="checkbox checkbox-default">
										                        <input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${id}" class="check_row">
										                        <label for="checkbox1_sd_${rowstatus.count}"></label>
										                   		</div>
										                   		</div>
														</td>
                                                    <td data-title="Select" class="">
													<c:choose>
													<c:when test="${bugType == 'Error' || bugType == 'Syntactic Error' || bugType == 'Calculation Error' || bugType == 'Logical Error' || bugType == 'Error handling errors' || bugType == 'Run time errors'}">
													<i class="fa fa-bug tooltips" data-toggle="tooltip" title="" data-original-title="${bugType}"></i>
													</c:when>
													<c:when test="${bugType == 'New Functionality' || bugType == 'New Work'}">
													<span data-toggle="tooltip" title="" data-original-title="${bugType}"> <img class="clippy" src="admin/img/svg/story.svg" height="16" width="16" alt="Story"></span>
													</c:when>
													<c:when test="${bugType == 'Task'}">
													<span data-toggle="tooltip" title="" data-original-title="${bugType}"> <img class="clippy" src="admin/img/svg/task.svg" height="16" width="16" alt="Story"></span>
													</c:when>
													<c:otherwise>
													<i class="fa fa-magic tooltips" data-toggle="tooltip" title="" data-original-title="${bugType}"></i>
													</c:otherwise>
													</c:choose>
													</td>
                                                    <td class="link"><a href="viewBugTracker?id=${id}">${referenceNo}</a></td>
                                                    <td class="link"><a href="viewBugTracker?id=${id}">${title}</a></td>
                                                    <td>${bugType}</td>
													<td>
													<c:choose>
													<c:when test="${level == 'Medium'}">
													<span class="label label-warning">${level}</span>
													</c:when>
													<c:when test="${level == 'Low'}">
													<span class="label label-success">${level}</span>
													</c:when>
													<c:otherwise>
													<span class="label label-danger">${level}</span>
													</c:otherwise>
													</c:choose>
													</td>
													<td>
													<c:choose>
													<c:when test="${status == 'New'}">
													<span class="bug-issue-status-max-width-medium bug-issue-status-info">${status}</span>
													</c:when>						
													<c:when test="${status == 'Pending'}">
													<span class="bug-issue-status-max-width-medium bug-issue-status-info">${status}</span>
													</c:when>						
													<c:when test="${status == 'WorkInProgress'}">
													<span class="bug-issue-status-max-width-medium bug-issue-status-info">${status}</span>
													</c:when>						
													<c:when test="${status == 'Pause'}">
													<span class="bug-issue-status-max-width-medium bug-issue-status-green">${status}</span>
													</c:when>						
													<c:when test="${status == 'Review'}">
													<span class="bug-issue-status-max-width-medium bug-issue-status-green">${status}</span>
													</c:when>						
													<c:when test="${status == 'Closed'}">
													<span class="bug-issue-status-max-width-medium bug-issue-status-green">${status}</span>
													</c:when>						
													<c:when test="${status == 'Verified'}">
													<span class="bug-issue-status-max-width-medium bug-issue-status-green">${status}</span>
													</c:when>						
													</c:choose>
													</td>
                                                    <td><span class="">
                                                    ${CreatedByName}
                                                    </span>
                                                    </td>
                                                    <td>${assignedByName}</td>
                                                    <td>${assignedToName}</td>
                                                     <td >
                                                     <c:choose>
														<c:when test="${assignDate!=null}">
																<s:date name="assignDate" format="dd/MMM/YYYY"/>
														</c:when>
														<c:otherwise>
														 NA
														</c:otherwise>
													</c:choose>
                                                     </td>
                                                    <td><s:date name="createdAt" format="dd/MMM/YYYY"/></td>
                                                    <td><s:date name="updatedAt" format="dd/MMM/YYYY"/></td>
                                        	        <td data-title="Action">
													<div class="dropdown pull-right" style="    margin-right: 20px;">
													  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
													  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
													  <ul class="dropdown-menu">
													    <li class="action"><a href="viewBugTracker?id=${id}">View Detail</a></li>
													    <li class="action"><a href="viewTestCases?bugId=${id}">View Test Case</a></li>
													    <li class="action">
													    <a href="#!" class="plus-icon" data-event="duplicate" data-toggle="modal" data-target="#assign_bug_tracker_${rowstatus.count}" data-bug-id="${bugTracker.id}">Assign To Others </a>
													    </li>
													    <c:if test="${session.User.userRoleId.techHead==true}">
													    </c:if>
													  </ul>
													</div>
													</td>  
													<td></td>
														 </tr>
												<!--Assign task to other Form  -->
	<div class="modal fade" id="assign_bug_tracker_${rowstatus.count}" role="dialog">
					<div class="modal-dialog modal-lg">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title text-center">Assign To Other</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="20" alt="Copy to clipboard">
								</button>
							</div>
							<form id="assignBugTrackerForm_${rowstatus.count}" method="post" class="bv-form">
								<div class="modal-body">
								 <div class="row mt-20">
								 	<c:choose>
								 	<c:when test="${session.User.userRoleId.techHead==true}">
								 	<div class="col-md-6">
											<label class="control-label text-left">Assign By</label>
												<div class="controls">
														<div class="form-group">
															<select name="bugTracker.assignedBy" id="assign-by" class="form-control">
															<option value="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="assign">
																		<option value="${assign.userId}" ${assign.userId == session.User.id ? 'selected': ''} >${assign.firstName} ${assign.lastName}</option>
															</c:forEach>
														</select> 
														</div>
												</div>
											</div>
								 	</c:when>
								 	<c:otherwise>
								 	<input type="hidden" name="bugTracker.assignedBy" value="<s:property value="%{#session.User.id}"/> ">
								 	</c:otherwise>
								 	</c:choose>
								 		<div class="<c:choose><c:when test="${session.User.userRoleId.techHead ==true}">col-md-6</c:when><c:otherwise>col-md-12</c:otherwise> </c:choose>">
											<label class="control-label text-left">Assign To</label>
												<div class="controls">
														<div class="form-group">
															<select name="bugTracker.assignTo" id="assign-to" class="form-control">
															<option value="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="assign">
																		<option value="${assign.userId}" ${assign.userId == assignedBy ? 'selected': ''}>${assign.firstName} ${assign.lastName}</option>
															</c:forEach>
															</select>
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="checkbox pull-right">
							                        <input id="checkbox1" type="checkbox" name="mailNotify" value="false">
							                        <label for="checkbox1">&nbsp;<span class="text-danger">Send notification email</span></label>
							                    </div>
						                    </div>
						                    <div class="col-md-12 mt-20">
						                    <p class="text-center"><span class="text-danger">*</span>The <strong>new owner</strong> will also become the owner of these records related to <strong><span class="is-owner"></span></strong> that are owned by you.</p>
						                    </div>
										</div>
							</div>
								<div class="modal-footer">
									<input type="hidden" name="bugTracker.id" id="bug-tracker-id" value="${id}">
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-success assign_to_bug" id="" data-modal-id="assign_bug_tracker_${rowstatus.count}" data-form-name="assignBugTrackerForm_${rowstatus.count}">Save</button>
								</div>
							</form>
						</div>

					</div>
				</div>
                                                </s:iterator>
                                            </s:if>
                                            </tbody>
											
                                        </table></div></div></div></div>
										</div>
                                    </div>
                            </div>
            </section>
        <!--ADMIN AREA ENDS-->
        

<!--AutoCompleteter  -->
<script>
// filter toggle script
$(document).ready(function(){$(".filterBtn").click(function(){$("#filterDiv").toggle(500)}),
$("div.easy-autocomplete").removeAttr("style"),$("#configreset").click(function(){$("#resetform")[0].reset()})}),
$(".filter-link").click(function(){var e=$(this).find("i").hasClass("fa-angle-up");$(".filter-link").find("i").removeClass("fa-angle-down"),
$(".filter-link").find("i").addClass("fa-angle-up"),e&&$(this).find("i").toggleClass("fa-angle-up fa-2x fa-angle-down fa-2x")});
//multiple select for company
/*--assign travel sales lead method--*/
 	$(document).ready(function() {
		$(".assign_travel_leadBtn").click(function(e) {
			e.preventDefault();
			var formDeleteName = $(this).attr("data-form-name");
			var modalId = $(this).attr("data-modal-id");
			var alertId = $(this).attr("data-alert-id");
			$.ajax({
				url : "saveTravelSalesLeadAssign",
				type : "POST",
				dataType : 'json',
				data : $("#" + formDeleteName).serialize(),
				success : function(jsonData) {
					if(jsonData.message.status == 'success'){
						$("#" + modalId).hide();
						alertify.success(jsonData.message.message);}
	  				else if(jsonData.message.status == 'input'){
	  					alertify.error(jsonData.message.message);}
	  				else if(jsonData.message.status == 'error'){
	  					alertify.error(jsonData.message.message);}
					
					//notify js
				},
				error : function(request, status, error) {
					showModalPopUp("Sales Lead   can not be updated.", "e");
				}
			});
		});

	});
/*##########################################################*/
	$(".assign_to_bug").click(function(e) {
		e.preventDefault();
		var formDeleteName = $(this).attr("data-form-name");
		var modalId = $(this).attr("data-modal-id");
		$.ajax({
			url : "updateBugTrackerOwner",
			type : "POST",
			dataType : 'json',
			data : $("#" + formDeleteName).serialize(),
			success : function(jsonData) {
				if(jsonData.message.status == 'success'){
					$("#" + modalId).hide();
					alertify.success(jsonData.message.message);
					}
  				else if(jsonData.message.status == 'error'){
  					$("#" + modalId).hide();
  					alertify.error(jsonData.message.message);}
				setTimeout(location.reload.bind(location), 1000);
			},
			error : function(request, status, error) {
				alertify.error("somthing wrong,please try again");
			}
		});
	}); 
 
/*##########################################################*/
//filter Autocompatator 
var CName={url:"getTravelSalesLeadJson.action?data=name",getValue:"name",list:{match:{enabled:!0}}};$("#name-json").easyAutocomplete(CName);
var Email={url:"getTravelSalesLeadJson.action?data=email",getValue:"email",list:{match:{enabled:!0}}};$("#email").easyAutocomplete(Email);
var Phone={url:"getTravelSalesLeadJson.action?data=phone",getValue:"phone",list:{match:{enabled:!0}}};$("#phone").easyAutocomplete(Phone);
var City={url:"getTravelSalesLeadJson.action?data=city",getValue:"city",list:{match:{enabled:!0}}};$("#city-json").easyAutocomplete(City);
var DFrom={url:"getTravelSalesLeadJson.action?data=destinationFrom",getValue:"destinationFrom",list:{match:{enabled:!0}}};$("#destination-from").easyAutocomplete(DFrom);
var DTo={url:"getTravelSalesLeadJson.action?data=destinationTo",getValue:"destinationTo",list:{match:{enabled:!0}}};$("#destination-to").easyAutocomplete(DTo);
			
/*--------------------------script for delete tour prices multiple-----------------------------*/
$('#delete_all_price').on("click", function(event){
	// confirm dialog
	 var allVals = [];
	 $(".check_row:checked").each(function() {  
		 allVals.push($(this).attr('data-id'));
	 });  
		 var check = confirm("Are you sure you want to delete selected data?");
		 if(check == true)
		 {
			 var join_selected_values = []; 
			 join_selected_values = allVals.join(",");
			 $.ajax({
						url : "archiveMultipleTravelLead?travelLeadIds="+join_selected_values,
						type : "POST",
						dataType: 'html',
						success : function(data) {
							alertify.success("You've clicked OK Your data have successfully deleted");
							setTimeout("window.location.reload();", 1000);
						},
						error: function (request, status, error) {
							showModalPopUp("Lead can not be deleted.","e");
						}
				}); 
		 }
		 else
		 {
			 return false;	 
		 }
});
</script>
<script type="text/javascript" src="admin/js/admin/multiple-archive.js"></script>

<script>
	$(document).ready(function() {
		var flag = true;
		$('#toggle-filter-bar').click('click', function() {
			$(".filter_bar").slideToggle("slow");
			$(this).removeClass();
			if (flag) {
				$('.collapsed').find('i').addClass('fa-angle-dawn');
			} else {
				$('.collapsed').find('i').addClass('fa-angle-up');
			}
			flag = !flag;
		}); 
	});

	
</script>
<script type="text/javascript">
$(".hide-filter").click(function(){
    $(".hide-filter").hide();
    $(".show-filter").show();
});

$(".show-filter").click(function(){
	$(".show-filter").hide();
    $(".hide-filter").show();
});

</script>