<%@ page language="java" isELIgnored="false"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script src="admin/js/jscolor.js"></script>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"admin_deals_insert_page";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<script type="text/javascript">
	function setImagePath()
	{
		$("#imageUrl").val($("#lefile").val());
	}
</script> 
<section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.add_homepage_deals" /></h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                            	<div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="admin_deals_fetch"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.deal_list" /> </a></li>
								</ul>
							</div>
						</div>
                               </div> 
                            </div>
                            <div class="hd clearfix">
                                <h5 class="pull-left"></h5>
                            </div>
					
                            <div class="cnt cnt-table">
                                <div class="no-table-css">
                                   <form action="admin_deals_insert.action" method="POST"
		enctype="multipart/form-data" class="form-horizontal">
	<button class="jscolor {valueElement:'bg-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:20px;">
		<s:text name="tgi.label.box_color" />
	</button>
	<input type="text" name="boxBgcolor" id="bg-chosen-color" value="A57DFF" style="width:80px;" />
                                    <table class="table table-form">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>&nbsp;</th>
                                               
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td>
                                                    <div class="field-name"><s:text name="tgi.label.category" /></div>
                                                </td>
												<td>
                                                   <select name="catagory" class="form-control" id="focusedInput" >
			<option value="flight"><s:text name="tgi.label.flight" /></option>
			<option value="hotel"><s:text name="tgi.label.hotel" /></option>
			<option value="car"><s:text name="tgi.label.car" /></option>
			<option value="tour"><s:text name="tgi.label.tour" /></option>
			<option value="cruise"><s:text name="tgi.label.cruise" /></option>
		</select>
							
                                                </td>
                                                												<td>
                                                    <div class="field-name"><s:text name="tgi.label.deals_type" /></div>
                                                </td>
                                                <td>
                                                   <select name="dealsType" class="form-control" >
			<option value="promodeal"><s:text name="tgi.label.Promo_Deals" /></option>
			<option value="bestdeal"><s:text name="tgi.label.Best_Deals" /></option>
			<option value="topdeal"><s:text name="tgi.label.Top_Deals" /></option>
			<option value="dailydeal"><s:text name="tgi.label.Daily_Deals" /></option>
			<option value="lastminutedeal"><s:text name="tgi.label.Last_Minute_Deals" /></option>
			<option value="staticlink"><s:text name="tgi.label.Static_Link" /></option>
		</select>
                                                </td>

                                                
                                            </tr>
                                            
                                            <tr>
											 <td>
                                                    <div class="field-name"><s:text name="tgi.label.title" /></div>
                                                </td>
                                                <td>
                                                   <input type="text" class="form-control input-sm" id=""
									name="title"   placeholder="" autocomplete="off" required>
                                                </td>
                                                 <th style="margin-left:0px;">
                                                 <select name="t_family" class="form-comtrol">
                                                 <option value=""><s:text name="tgi.label.Select_Font" /></option>
                                                 <option value="Arial,Helvetica Neue,Helvetica,sans-serif"><s:text name="tgi.label.arial_full_stack" /></option>
												<option value="Arial"><s:text name="tgi.label.arial" /></option>
												<option value="Helvetica Neue"><s:text name="tgi.label.helvetica_neue" /></option>
												<option value="Helvetica"><s:text name="tgi.label.helvetica" /></option>
												<option value="sans-serif"><s:text name="tgi.label.Sans-Serif" /></option>
                                                 </select>
                                                 <select name="t_weight" class="form-comtrol">
                                                 <option value=""><s:text name="tgi.label.Select_Weight" /></option>
												<option value="normal"><s:text name="tgi.label.normal" /></option>		
												<option value="bold"><s:text name="tgi.label.bold" /></option>
												<option value="bolder"><s:text name="tgi.label.bolder" /></option>
												<option value="lighter"><s:text name="tgi.label.lighter" /></option>
                                                 </select>
                                                 </th>
                                                <th>
                                                	
	<button class="jscolor {valueElement:'title-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:50px;">
	<s:text name="tgi.label.font_color" />
	</button>
	<input type="text" name="t_color" id="title-chosen-color" value="A57DFF" style="width:80px;" />
                                                </th>
												</tr>
												  <tr>
											<td>
                                                    <div class="field-name"><s:text name="tgi.label.price" /></div>
                                                </td>
                                                <td>
                                                   <input type="text" class="form-control input-sm"  name="price" id="price" />
                                                </td>
                                                 <th style="margin-left:0px;">
                                                 <select name="p_family" class="form-comtrol">
                                                 <option value=""><s:text name="tgi.label.Select_Font" /></option>
                                                 <option value="Arial,Helvetica Neue,Helvetica,sans-serif"><s:text name="tgi.label.arial_full_stack" /></option>
												<option value="Arial"><s:text name="tgi.label.arial" /></option>
												<option value="Helvetica Neue"><s:text name="tgi.label.helvetica_neue" /></option>
												<option value="Helvetica"><s:text name="tgi.label.helvetica" /></option>
												<option value="sans-serif"><s:text name="tgi.label.Sans-Serif" /></option>
                                                 </select>
                                                 <select name="p_weight" class="form-comtrol">
                                                 <option value=""><s:text name="tgi.label.Select_Weight" /></option>
												<option value="normal"><s:text name="tgi.label.normal" /></option>		
												<option value="bold"><s:text name="tgi.label.bold" /></option>
												<option value="bolder"><s:text name="tgi.label.bolder" /></option>
												<option value="lighter"><s:text name="tgi.label.lighter" /></option>
                                                 </select>
                                                 </th>
                                                <th>
                                                	
	<button class="jscolor {valueElement:'price-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:50px;">
		<s:text name="tgi.label.font_color" />
	</button>
	<input type="text" name="p_color" id="price-chosen-color" value="A57DFF" style="width:80px;" />
                                                </th>
												</tr>
                                            <tr>
											 <td>
                                                    <div class="field-name"><s:text name="tgi.label.currency" /></div>
                                                </td>
                                                 <td>
                                                  <!-- <input type="text" class="form-control input-sm" id="focusedInput" name="currency" /> -->
                                                  	<select class="form-control input-sm" name="currency">
								<option value="0" selected="selected"><s:text name="tgi.label.Select_Currency" /></option>
								<s:iterator value="countryList">
									<option value='<s:property value="cur_code"/>'><s:property value="cur_code" /></option>
								</s:iterator>
							</select>
                                                </td>
                                               
                                                                               <th style="margin-left:0px;">
                                                 <select name="c_family" class="form-comtrol">
                                                 <option value=""><s:text name="tgi.label.Select_Font" /></option>
                                                 <option value="Arial,Helvetica Neue,Helvetica,sans-serif"><s:text name="tgi.label.arial_full_stack" /></option>
												<option value="Arial"><s:text name="tgi.label.arial" /></option>
												<option value="Helvetica Neue"><s:text name="tgi.label.helvetica_neue" /></option>
												<option value="Helvetica"><s:text name="tgi.label.helvetica" /></option>
												<option value="sans-serif"><s:text name="tgi.label.Sans-Serif" /></option>
                                                 </select>
                                                 <select name="c_weight" class="form-comtrol">
                                                 <option value=""><s:text name="tgi.label.Select_Weight" /></option>
												<option value="normal"><s:text name="tgi.label.normal" /></option>		
												<option value="bold"><s:text name="tgi.label.bold" /></option>
												<option value="bolder"><s:text name="tgi.label.bolder" /></option>
												<option value="lighter"><s:text name="tgi.label.lighter" /></option>
                                                 </select>
                                                 </th>
                                                <th>
                                                	
	<button class="jscolor {valueElement:'currency-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:50px;">
		<s:text name="tgi.label.font_color" />
	</button>
	<input type="text" name="c_color" id="currency-chosen-color" value="A57DFF" style="width:80px;" />
                                                </th>
                                                </tr>
                                                <tr>
<td>
                                                    <div class="field-name"><s:text name="tgi.label.description" /></div>
                                                </td>
                                                 <td>
                                                  <input type="text"  class="form-control input-sm" id="focusedInput"name="description" />
                                                </td>
                                                                              <th style="margin-left:0px;">
                                                 <select name="d_family" class="form-comtrol">
                                                 <option value=""><s:text name="tgi.label.Select_Font" /></option>
                                                 <option value="Arial,Helvetica Neue,Helvetica,sans-serif"><s:text name="tgi.label.arial_full_stack" /></option>
												<option value="Arial"><s:text name="tgi.label.arial" /></option>
												<option value="Helvetica Neue"><s:text name="tgi.label.helvetica_neue" /></option>
												<option value="Helvetica"><s:text name="tgi.label.helvetica" /></option>
												<option value="sans-serif"><s:text name="tgi.label.Sans-Serif" /></option>
                                                 </select>
                                                 <select name="d_weight" class="form-comtrol">
                                                 <option value=""><s:text name="tgi.label.Select_Weight" /></option>
												<option value="normal"><s:text name="tgi.label.normal" /></option>		
												<option value="bold"><s:text name="tgi.label.bold" /></option>
												<option value="bolder"><s:text name="tgi.label.bolder" /></option>
												<option value="lighter"><s:text name="tgi.label.lighter" /></option>
                                                 </select>
                                                 </th>
                                                <th>
                                                	
	<button class="jscolor {valueElement:'description-chosen-color', onFineChange:'setTextColor(this)'}" style="margin-right:50px;">
		<s:text name="tgi.label.font_color" />
	</button>
	<input type="text" name="d_color" id="description-chosen-color" value="A57DFF" style="width:80px;"/>
                                                </th>
</tr>
                                            <tr> <td> 
											
      <div class=" field-name"><s:text name="tgi.label.upload_images" /></div></td>
	 <td> <div class="col-sm-12">

<input type="hidden" id="imageUrl" name="imageUrl">
<input id="lefile" type="file" style="display:none" name="upload" onchange="javascript:setImagePath()">
<div class="input-append">
<input id="photoCover" class="input-large" type="text">
<a class="btn" onclick="$('input[id=lefile]').click();" style="
    background-color: rgb(6, 111, 192);
    color: white;
"><s:text name="tgi.label.browse" /></a>
</div></div>
<script type="text/javascript">
$('input[id=lefile]').change(function() {
$('#photoCover').val($(this).val());
});
</script>
</td>
 <td>
                                                    <div class="field-name"><s:text name="tgi.label.image_caption" /></div>
                                                </td>
                                                 <td>
                                                  <input type="text"  class="form-control input-sm" id="focusedInput"name="imageCaption" />
                                                </td>												
</tr>
                                        </tbody>
                                    </table>
<div class="form-group table-btn">
                                        <div class="col-sm-10 col-sm-offset-2 ">
                                            <%-- <div class="pull-left"><button class="btn btn-danger" type="submit"><s:text name="tgi.label.clear_form" /></button></div> --%>
                                            <div class="pull-right">
                                                
                                                <button class="btn btn-default" type="reset"><s:text name="tgi.label.cancel" /></button>
                                                <button class="btn btn-primary " type="submit"><s:text name="tgi.label.add" /></button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
            
                      	
            			<s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>