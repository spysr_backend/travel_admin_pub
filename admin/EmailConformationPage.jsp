<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title> <s:text name="global.welcome"></s:text></title>
<link href="admin/css/bootstrap.css" rel="stylesheet">
 <link href="<s:text name="global.Appcss"></s:text>" rel="stylesheet" type="text/css" />
 
</head>
<body>
  <div class="wrapper">
      <header class="main-header">
        <div> 
            <a href="#" class="logo">
            <span class="logo-lg"><img src="<s:text name="global.ImagePath" ></s:text>" class="img-responsive"></span>
            </a>
        </div>
      </header>
      <article class="log-welcome">
        <h1><s:text name="tgi.label.welcome_to_tgi" /></h1>
        <p class="cont"><s:text name="tgi.label.your_email_verified" /></p>
        <p > Contact details:</p>
 ${message}
 
	<address>
            <s:text name="tgi.label.name" />: <a href="#"><s:property value="pname"></s:property></a><br/>
           <s:text name="tgi.label.email" />: <a href="#"><s:property value="pemail"></s:property></a><br/>
           <s:text name="tgi.label.phone" />: <a href="#"><s:property value="pphone"></s:property></a>
        
        </address>
 	</article>
      </div>
     <footer class="log-footer">
   <address>
            <s:text name="tgi.label.web" /> : <a href="#">#</a><br/>
           <s:text name="tgi.label.email" />: <a href="#">#</a> |
           <s:text name="tgi.label.phone" />: <a href="#" >#</a>
        
        </address>
       

</footer>
    
  </body>
 
</html>