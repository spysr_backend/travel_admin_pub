<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="dj" uri="/struts-dojo-tags"%>


<script type="text/javascript">
	$(function() {
		var totUrl = $(location).attr('href');
		var newUrl = totUrl.substr(0, totUrl.lastIndexOf('/') + 1);
		var finalUrl = newUrl + "goMyWallet";
		$('#alert_box_info_ok').click(function() {
			window.location.assign(finalUrl);
			$('#alert_box_info').hide();

		});
		$('#cancel').click(function() {
			$('#error-alert').hide();

		});
	});
</script>


<!--************************************
        MAIN ADMIN AREA
    ****************************************-->

<!--ADMIN AREA BEGINS-->




<div class="mt-5">
	<div class="container">
		<h4 class="text-center p-4">Add My Wallet</h4>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form action="addMyWallet" method="post" class="spy-form-horizontal"
					id="addWalletForm" name="myForm">
					<div class="form-group">
						<label for="email">Amount</label> <input type="text"
							class="form-control" name="agentWallet.walletBalance" id="amount"
							autocomplete="off" required> <input type="hidden"
							class="form-control" name="id"
							value="<s:property value="%{#session.User.id}"/>"> <input
							type="hidden" class="form-control" name="agentWallet.walletId"
							value="<s:property value="%{#session.User.agentWallet.walletId}"/>" />
					</div>
					<div class="form-group">
						<label for="pwd">Currency</label> <select
							class="form-control input-sm" name="agentWallet.currencyCode"
							id="currency" required>
							<option selected
								value="<s:property value="%{#session.User.agentWallet.currencyCode}"/>"><s:property
									value="%{#session.User.agentWallet.currencyCode}" /></option>
						</select>
					</div>
					<div class="form-group">
						<label for="pwd">Transaction Type </label> <select
							class="form-control input-sm" name="agentWallet.transactionType"
							id="agentWallet.transactionType" autocomplete="off" required>
							<option selected value="Credit"><s:text
									name="tgi.label.credit" /></option>
							<option value="Debit"><s:text name="tgi.label.debit" /></option>
						</select>
					</div>
					<input type="hidden" placeholder="Password" id="Password"
						name="Password">
					<button type="button" onclick="javascript:showPopUp();"
						class="btn btn-primary btn-block">Add Wallet</button>
				</form>
			</div>
		</div>
		<%-- <s:if test="hasActionErrors()">
			<div class="success-alert" style="display: none">
				<span class="fa fa-thumbs-o-up fa-1x"></span>
				<s:actionerror />
			</div>
		</s:if>
		<s:if test="hasActionMessages()">
			<div class="success-alert" style="display: none">
				<span class="fa fa-thumbs-o-up fa-1x"></span>
				<s:actionmessage />
			</div>
		</s:if> --%>
		<!--  -->
		<div class="modal fade" id="updateBoxWallet" tabindex="-1"
			role="dialog" aria-labelledby="edit" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content up-pass">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</button>
						<h4 class="modal-title custom_align text-center" id="Heading">
							<s:text name="tgi.label.enter_your_valid_passsword" />
						</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div class="input-group">
								<input type="password" placeholder="Password" id="Password_1"
									class="form-control input-sm" name="Password_1">
							</div>
						</div>
						<div class="modal-footer text-center">
							<button type="button"
								onclick="javascript:userWalletVerifyByPassword();"
								class="btn btn-success text-center">
								<span><s:text name="tgi.label.send" /></span> 
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<s:if test="hasActionErrors()">
	<div id="actionErrorInner" style="display:none">
		<s:actionerror />
	</div>
	<script src="admin/js/jquery.min.js"></script>
	<script src="admin/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#alert_box_info').modal({
				show : true,
				keyboard : false
			});
			$("#actionErrorInner").hide();
			$('#alert_box_info_body').text($("#actionErrorInner").text());
		});
	</script>
</s:if>
<s:if test="hasActionMessages()">
	<div id="actionMessageInner" style="display:none">
		<s:actionmessage />
	</div>
	<script src="admin/js/jquery.min.js"></script>
	<script src="admin/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#alert_box').modal({
				show : true,
				keyboard : false
			});
			$("#actionMessage").hide();
			$('#alert_box_body').text($("#actionMessageInner").text());
		});
	</script>
</s:if>

<script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
<script>
	function userWalletVerifyByPassword() {
		var password = $("#Password_1").val();
		console.debug(password);
		$("#Password").val(password);
		$("#addWalletForm").submit();
	}
	function showPopUp() {
		if ($("#amount").val() == null || $("#amount").val() == "") {
			alert("Enter amount")
		} else {
			$('#updateBoxWallet').modal({
				show : 'true'
			});
		}
	}
</script>
<!--ADMIN AREA ENDS-->