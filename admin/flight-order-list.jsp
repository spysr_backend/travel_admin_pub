<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>

            <section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                    <div class="card1">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.flight_order" /></h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                                 <div class="set pull-right">
                                    <div class="dropdown">
                                      <div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									<ul class="dropdown-menu dropdown-info pull-right align-left">
								<li class=""><a class="" href="flightReportList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.report_list" /> </a></li>
									<li class=""><a class="" href="flightOrderList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.order_list" /> </a></li>
									<li class=""><a class="" href="flightCommissionReportList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_report" /></a></li>
									<li class=""><a class="" href="flightCustomerInvoiceList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.custommer_invoice" /> </a></li>
									<li class=""><a class="" href="flightAgentInvoiceList"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<s:text name="tgi.label.agent_commision_invoice" /></a></li>
								</ul>
								</div></div></div>
								  <div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
								</div>
								<br/> 
							 	 <form action="flightOrderList" class="filter-form">
							    <div class="" id="filterDiv" style="display:none;" >
							    <div class="form-group">
							    <s:if test="%{#session.Company!=null}">
         						<s:if test="%{#session.Company.companyRole.isSuperUser() || #session.Company.companyRole.isDistributor()}">
                                 <div class="col-md-2">
                                <input type="hidden" id="HdncompanyTypeShowData" value="" />
                                <select name="companyTypeShowData" id="companyTypeShowData" class="form-control input-sm">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="my"><s:text name="tgi.label.my_self" /></option>
                                <s:if test="%{#session.Company!=null}">
         						<s:if test="%{#session.Company.companyRole.isSuperUser()}">
                                 <option value="distributor"><s:text name="tgi.label.distributor" /></option>
                                 </s:if>
                                </s:if>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div>
                                </s:if>
                                </s:if>
                                <div class="col-md-2 col-sm-6 form-group">
                                <input type="text" name="orderId" id="orderid-json"
												placeholder="Search By Order Id...."
												class="form-control search-query input-sm" />
                                </div>
                                 <div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="pnr" id="pnr-json"
												placeholder="Search By PNR...."
												class="form-control search-query input-sm" />
								</div>
                                <div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="airline" id="airline-json"
												placeholder="Airline...."
												class="form-control search-query input-sm" />
								</div>
                                <div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="bookingStartDate" id="bookingStartDate"
												placeholder="Start Booking Date...."
												class="form-control search-query date1 input-sm " />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="bookingEndDate" id="bookingEndDate"
												placeholder="End Booking Date...."
												class="form-control search-query date2 input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="travelStartDate" id="travelStartDate"
												placeholder="Start Travel  Date...."
												class="form-control search-query date3 input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="travelEndDate" id="travelEndDate"
												placeholder="End Travel Date...."
												class="form-control search-query date4 input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="firstName" id="firstName"
												placeholder="Passenger FirstName.."
												class="form-control search-query input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="lastName" id="lastName"
												placeholder="Passenger LastName..."
												class="form-control search-query input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="email" id="email"
												placeholder="Passenger Email..."
												class="form-control search-query input-sm" />
								</div>
								<div class="row">
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="mobile" id="mobile"
												placeholder="Passenger Mobile..."
												class="form-control search-query input-sm" />
								</div></div>
								
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="origin" id="origin"
												placeholder="Origin.."
												class="form-control search-query input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="destination" id="destination"
												placeholder="Destination..."
												class="form-control search-query input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="supplier" id="agency-json"
									placeholder="Supplier..." class="form-control search-query input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<input type="text" name="createdBy" id="agency-json"
									placeholder="Agency..." class="form-control search-query input-sm" />
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<select name="tripType" id="tripType" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_way" /></option>
                                <option value="ALL"><s:text name="tgi.label.all" /></option>
                             	<option value="R"><s:text name="tgi.label.ROUNDTRIP" /></option>
                                <option value="O"><s:text name="tgi.label.ONEWAY" /></option>
                                </select>
								</div>
								<div class="row">
								<div class="col-md-2 col-sm-6 form-group">
								<select name="paymentMode" id="paymentMode" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_payment_method" /></option>
                                <option value="ALL"><s:text name="tgi.label.all" /></option>
                             	<option value="CHECK"><s:text name="tgi.label.CHECK" /></option>
                                <option value="CREDIT_CARD"><s:text name="tgi.label.CREDIT_CARD" /></option>
                                <option value="DEBIT_CARD"><s:text name="tgi.label.DEBIT_CARD" /></option>
                                <option value="WALLET"><s:text name="tgi.label.WALLET" /></option>
                                </select>
								</div>
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<select name="paymentModeType" id="paymentModeType" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_payment_mode" /></option>
								<option value="ALL"><s:text name="tgi.label.all" /></option>
                                <option value="SQUAREUP" ><s:text name="tgi.label.squareUp" /></option>
                             	<option value="EXTERNALLINK_TEST"><s:text name="tgi.label.external" /></option>
                                <option value="AUTHORIZE"><s:text name="tgi.label.authorize" /></option>
                                <option value="WALLET"><s:text name="tgi.label.WALLET" /></option>
                                <option value="WIRECARD"><s:text name="tgi.label.wirecard" /></option>
                                <option value="STRIPE"><s:text name="tgi.label.stripe" /></option>
                                </select>
								</div>
								<div class="col-md-2 col-sm-6 form-group">
								<select name="paymentStatus" id="paymentStatus" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_payment_status" /></option>
								<option value="ALL"><s:text name="tgi.label.all" /></option>
                                <option value="PENDING" ><s:text name="tgi.label.PENDING" /></option>
                             	<option value="SUCCESS"><s:text name="tgi.label.SUCCESS" /></option>
                                <option value="FAILED"><s:text name="tgi.label.FAILED" /></option>
                                <option value="PROCESSING"><s:text name="tgi.label.PROCESSING" /></option>
                                <option value="DECLINED"><s:text name="tgi.label.DECLINED" /></option>
                                <option value="REFUND"><s:text name="tgi.label.REFUND" /></option>
                                </select>
								</div> 
								<div class="col-md-2 col-sm-6 form-group">
								<select name="bookingStatus" id="bookingStatus" class="form-control input-sm">
								<option value="" selected="selected"><s:text name="tgi.label.select_booking_status" /></option>
								<option value="ALL"><s:text name="tgi.label.all" /></option>
                                <option value="PENDING" ><s:text name="tgi.label.PENDING" /></option>
                             	<option value="CONFIRM"><s:text name="tgi.label.CONFIRM" /></option>
                                <option value="FAILED"><s:text name="tgi.label.FAILED" /></option>
                                <option value="PROCESSING"><s:text name="tgi.label.PROCESSING" /></option>
                                <option value="CANCELLED"><s:text name="tgi.label.CANCELLED" /></option>
                                </select>
								</div>
								<div class="col-md-1">
								<div class="form-group">
								<input type="reset" class="btn btn-danger btn-block" id="configreset" value="Clear">
								</div>
								</div>
									<div class="col-md-1">
									<div class="form-group">
									<input type="submit" class="btn btn-info btn-block" value="Search" />
								</div>
								</div>
                                </div></div>
                                </form> 
                            <input type="hidden"
						value="<s:property value="%{#session.Company.companyUserId}"/>"
						id="companyUserId"> <input type="hidden"
						value="<s:property value="%{#session.Company.eamil}"/>" id="email">
					<input type="hidden"
						value="<s:property value="%{#session.User.companyUserId}"/>"
						id="user_id">
									<%-- <s:if test="%{#session.Company!=null && #session.User!=null}">
						<s:if test="%{#session.User.userRoleId.isSuperUser()}"> --%>
                                <div class="cnt cnt-table">
                               <div class="dash-table no-table-css">

							<!-- testing -->
<div class="cnt cnt-table">
   	<div class="dash-table scroller">
			<div class="content mt-0 pt-0">
				<div id="example_wrapper" class="dataTables_wrapper">
										<table id="example" class="display dataTable responsive nowrap"
										role="grid" aria-describedby="example_info"
										style="width: 100%;" cellspacing="0">
                                        <!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
                                        <thead>
										<tr class="border-radius border-color" role="row">
                                                   <th data-priority="1"><s:text name="tgi.label.s.no" /></th>
                                                   <th data-priority="3"><s:text name="tgi.label.booking_date" /></th>
												   <th data-priority="2"><s:text name="tgi.label.orderid" /></th>
												   <th data-priority="4"><s:text name="tgi.label.pnr" /></th>
												   <th data-priority="5"><s:text name="tgi.label.airline" /></th>
												   <th data-priority="6"><s:text name="tgi.label.route" /></th>
										       <!--<th>MyMarkup</th>
												   <th>WholesalerMarkup</th>
												   <th>AgencyMarkup</th> -->
												   <th data-priority="7"><s:text name="tgi.label.passenger_name" /></th>
												   <th data-priority="8"><s:text name="tgi.label.total_amount" /></th>
											   	   <th data-priority="9"><s:text name="tgi.label.payment_status" /></th>
												   <th data-priority="10"><s:text name="tgi.label.booking_status" /></th>
											  <%-- <th><s:text name="tgi.label.supplier" /></th> --%>
												   <th data-priority="11"><s:text name="tgi.label.agency" /></th>
											       <th data-priority="12"><s:text name="tgi.label.action" /></th>
											       <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <s:iterator value="companyFlightOrderList"
													status="rowCount">
													<tr>
														<td data-title="S.No"><s:property value="%{#rowCount.count}" /></td>
														<td data-title="Booking Date"><s:property value="bookingDate" /></td>
														<td data-title="OrderId"><s:property value="orderId" /></td>
														<td data-title="PNR No"><s:property value="pnr" /></td>
														
														
														<td data-title="Airline"><s:property value="airline"/></td>
														
														<td data-title="Route"><s:property value="route" /></td>
														<%-- <td><s:property value="markUp" /></td>  
															<td><s:property value="distributorMarkup"/></td>
														<td><s:property value="childMarkup"/></td> --%>
														<%--  <td><s:property value="agentCommByRate" /></td> --%>
														<td data-title="Passenger Name"><s:property value="firstName" /> <s:property value="lastName" /></td>
														<td data-title="Total"><s:property value="finalPrice" /></td>
														<td data-title="Status	"><s:property value="paymentStatus" /></td>
														<td data-title="Status	"><s:property value="status" /></td>
														<%-- <td data-title="Supplier"><s:property value="apiProvider"/></td> --%>
														<td data-title="BookedBy"><s:property value="createdBy" /></td>

														<td data-title="Action">
															<p data-placement="top" title="edit">
																<a
																	href="showPassengerOrderDetails?id=<s:property value="id"/>&orderId=<s:property value="orderId"/>&companyId=<s:property value="companyId"/>"
																	class="btn btn-success btn-xs" data-toggle="modal">
																	<span class="glyphicon glyphicon-info-sign"></span> <s:text name="tgi.label.details" />
																</a>
															</p>
														</td>
														<td></td>
													</tr>
												</s:iterator>
                                            </tbody>
											
                                        </table></div></div></div></div></div>
										</div></div>
                                    </div>
                            </div>
                        </div>
                
            </section>
        <!--ADMIN AREA ENDS-->
        
		 <link href="admin/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
	   	 <script src="admin/js/jquery-ui.js" ></script>
         <script>
			$(document).ready(function() {
				var date1="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				var date2="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
				 spysr_date_custom_plugin('filter-form','date1','date2','mm/dd/yyyy','MMM DD YYYY',date1,date2);
				 
				 var date3="${spysr:formatDate(startDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					var date4="${spysr:formatDate(endDate,'yyyy-MM-dd', 'MMM dd yyyy')}";
					 spysr_date_custom_plugin('filter-form','date3','date4','mm/dd/yyyy','MMM DD YYYY',date3 ,date4);
			});
		</script>
	
	<script type="text/javascript">
$(function() {
   /*  $('#row_dim').hide();  */
    $('#user').change(function(){
    	//alert($('#user').val());
        if($('#user').val()== 'ALL') {
            $('#company_form-group').hide(); 
        } 
        else if($('#user').val() == '0') {
        	 $('#company_form-group').show(); 
          
       } 
        else {
            $('#company_form-group').hide(); 
        } 
    });
   
   
    $('#companyName').change(function(){
    	//alert($('#companyName').val());
        if($('#companyName').val() == 'ALL') {
            $('#user_form-group').hide(); 
        } else if($('#companyName').val() == '0') {
        	 $('#user_form-group').show(); 
           
        } 
        else{
        	 $('#user_form-group').hide(); 
        }
    });
   
   
   
   
});
 </script>
 
 	<script type="text/javascript" class="init">
	$(document).ready(function() {
			$('#filterBtn').click(function() {
		    $('#filterDiv').toggle();
		});

			$('div.easy-autocomplete').removeAttr('style');
	} );
	
	$('.faq-links').click(function() {
		var collapsed = $(this).find('i').hasClass('fa-compress');

		$('.faq-links').find('i').removeClass('fa-expand');

		$('.faq-links').find('i').addClass('fa-compress');
		if (collapsed)
			$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
	});
		</script>	
 
        <script>
     //order id
		var OrderId = {
			url: "getFlightReportsJson.action?data=orderId",
			getValue: "orderId",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#orderid-json").easyAutocomplete(OrderId);
	
	//pnr
		var PNR = {
			url: "getFlightReportsJson.action?data=pnr",
			getValue: "pnr",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#pnr-json").easyAutocomplete(PNR);
	
		
       //airine
		var Airline = {
			url: "getFlightReportsJson.action?data=airline",
			getValue: "airline",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#airline-json").easyAutocomplete(Airline);
		
       //origin
		var Origin = {
			url: "getFlightReportsJson.action?data=origin",
			getValue: "origin",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#origin").easyAutocomplete(Origin);
		
		var Destination = {
			url: "getFlightReportsJson.action?data=destination",
			getValue: "destination",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#destination").easyAutocomplete(Destination);
		
		var FirstName = {
			url: "getFlightReportsJson.action?data=firstName",
			getValue: "firstName",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#firstName").easyAutocomplete(FirstName);
		
		var LastName = {
			url: "getFlightReportsJson.action?data=lastName",
			getValue: "lastName",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#lastName").easyAutocomplete(LastName);
		
		var Email = {
			url: "getFlightReportsJson.action?data=email",
			getValue: "email",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#email").easyAutocomplete(Email);
		
		var Mobile = {
			url: "getFlightReportsJson.action?data=mobile",
			getValue: "mobile",
			
			list: {
				match: {
					enabled: true
				}
			}
		};
		
		$("#mobile").easyAutocomplete(Email);
			
		$(document).ready(function(){$(".filterBtn").click(function(){$("#filterDiv").toggle(500)}),
			 $("div.easy-autocomplete").removeAttr("style"),$("#configreset").click(function(){$("#resetform")[0].reset()})}),
			 $(".filter-link").click(function(){var e=$(this).find("i").hasClass("fa-angle-up");$(".filter-link").find("i").removeClass("fa-angle-down"),
			 $(".filter-link").find("i").addClass("fa-angle-up"),e&&$(this).find("i").toggleClass("fa-angle-up fa-2x fa-angle-down fa-2x")});
	</script>