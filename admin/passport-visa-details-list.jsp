  <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

       <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->
       
            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.passport_details" /></h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                               <%--  <div class="set pull-right">
                                    <div class="dropdown">
                                      <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <s:text name="tgi.label." />
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#"><s:text name="tgi.label.action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.another_action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.something_else_here" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.separated_link" /></a></li>
                                      </ul>
                                    </div>
                               </div> --%>
                            </div>
                            <div class="hd clearfix">
                                <h5 class="pull-left"></h5>
                            </div>
							<div class="cnt cnt-table">
							
                                <div class="cnt cnt-table">
											<div class="btn-group" data-toggle="buttons">
  <label class="btn btn-primary active">
    <input type="checkbox" checked autocomplete="off"><s:text name="tgi.label.excel" />
  </label>
  <label class="btn btn-primary">
    <input type="checkbox" autocomplete="off"><s:text name="tgi.label.pdf" />
  </label>
  <label class="btn btn-primary">
    <input type="checkbox" autocomplete="off"><s:text name="tgi.label.print" /> 
  </label>
</div>
                                             
                                       
                            
                                <div class="table-responsive no-table-css">
                                        <table class="table table-bordered table-striped-column table-hover">
                                            <thead>
                                                <tr class="border-radius border-color">
                                                    <th><s:text name="tgi.label.first_name" /></th>
                                                    <th><s:text name="tgi.label.last_name" /></th>
                                                    <th><s:text name="tgi.label.passport_number" /></th>
													<th><s:text name="tgi.label.passport_expiry_date" /></th>
                                                    
                                                </tr>
                                            </thead>
                                            	<tbody>


											<s:iterator value="#session.frequentflyerlist">

												<tr>
													<td data-title="First Name"><s:property value="firstName" /></td>
													<td data-title="Last Name"><s:property value="lastName" /></td>												
													<td data-title="PassportNumber"><s:property value="passportNo" /></td>
													<td data-title="PassportExpiryDate"><s:property value="passportExpiryDate" /></td>
													
													

												</tr>

											</s:iterator>

										</tbody>
                                                               
                                        </table>
                                    </div>
						
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        <!--ADMIN AREA ENDS-->

        
        
 	<%-- <script type="text/javascript">
		$(document).ready(
				function() {
					var table = $('#example').DataTable({
						lengthChange : true,
						"pagingType" : "full_numbers",
						"lengthMenu" : [ 10, 25, 50, 75, 100, 500 ],
						buttons : [ 'excel', 'pdf', 'print' ]
					});

					table.buttons().container().appendTo(
							'#example_wrapper .col-sm-6:eq(0)');

				});

		/*  $(function () {
		 	$('#example').DataTable({
		    	 "paging": true,
		         "lengthChange": true,
		        "searching": true,
		        "ordering": true,  
		           "info": true,
		         "autoWidth": false,  
		        "search": {
		      	    "regex": true,
		      	  }, 
		      	 
		      "pagingType": "full_numbers",
		      "lengthMenu": [10, 25, 50, 75, 100, 500 ],
		     
		      
		     });  
		  
		   });   */
	</script> --%>
		<script type="text/javascript">
$(function() {
   /*  $('#row_dim').hide();  */
    $('#user').change(function(){
    	//alert($('#user').val());
        if($('#user').val()== 'ALL') {
            $('#company_form-group').hide(); 
        } 
        else if($('#user').val() == '0') {
        	 $('#company_form-group').show(); 
          
       } 
        else {
            $('#company_form-group').hide(); 
        } 
    });
   
   
    $('#companyName').change(function(){
    	//alert($('#companyName').val());
        if($('#companyName').val() == 'ALL') {
            $('#user_form-group').hide(); 
        } else if($('#companyName').val() == '0') {
        	 $('#user_form-group').show(); 
           
        } 
        else{
        	 $('#user_form-group').hide(); 
        }
    });
   
   
   
   
});
 </script>