<%@ page language="java" isELIgnored="false"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
function deletePopupCountryInfo(id,version)
{
	$('#alert_box_delete').modal({
  	    show:true,
  	    keyboard: false
	    } );
	$('#alert_box_delete_body').text("Are your sure want to delete ?");
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	var finalUrl = newUrl+"admin_deals_delete.action?id="+id+"&version="+version;
	  $("#deleteItem").val(finalUrl);
}

$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"admin_deals_fetch";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	$('#alert_box_delete_ok').click(function() {
		window.location.assign($("#deleteItem").val()); 
			$('#alert_box_delete').hide();
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>


<title>Insert title here</title>
 <section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                              <h5 class="pull-left"><s:text name="tgi.label.all_deals" /></h5>
                              <!--   <div class="set pull-left">
                                    <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button>
                                </div> --> 
                                 <div class="set pull-right">
                                    <a id="add_button" class="btn btn-sm btn-success " href="admin_deals_insert_page"><i
							class="fa fa-plus fa-lg" aria-hidden="true"
							style="margin-right: 6px;"></i>Create<span class="none-xs">
								Deals</span></a>
                               </div> 
                            </div>
                            <script type="text/javascript">
                           $(document).ready(function()
                        		   {
                        	   if($("#categoryHdn").val() != null && $("#categoryHdn").val() != "")
                        		   {
							   $("#selectCategory").val($("#categoryHdn").val());
                        		   }
                        	   else
                        		   {
                        		   $("#selectCategory").val("all");
                        		   }
                        	   
							   $("#selectCategory").change(function()
                            		{
                            		$("#categoryHdn").val($("#selectCategory").val());
                            		$("#submitSearchDealsForm").submit();
                            		});
                           });
                            </script>
                            <script type="text/javascript">
                            $(document).ready(function()
                            	{
                            		$(".submitPublishForm").click(function()
                            		{
                            		 $("#id").val($(this).data("id"));
                            		 $("#key").val($(this).data("active"));
                            		 $("#displaySequenceHdn").val($("#setSequenceOrder_"+$(this).data("id")).val());
                            		 /* alert($(this).data("id"));
                            		 alert($(this).data("active"));
                            		 alert("setSequenceOrder_"+$(this).data("id")); */
                            		 $("#submitPublish").submit();
                            		});
                            	});
                            </script>
                            <div class="hd clearfix">
                                <select id="selectCategory">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="flight"><s:text name="tgi.label.flight" /></option>
                                <option value="hotel"><s:text name="tgi.label.hotel" /></option>
                                <option value="car"><s:text name="tgi.label.car" /></option>
                                <option value="tour"><s:text name="tgi.label.tour" /></option>
                                <option value="cruise"><s:text name="tgi.label.cruise" /></option>
                                </select>
                                </h5>
                            </div>
							<form action="search_dealsbycategory.action" id="submitSearchDealsForm">
							<input type="hidden" name="catagory" id="categoryHdn" value="${param.catagory}"/>
							</form>
                                <div class="cnt cnt-table">

					<div class="no-table-css">
						<table
							class="table table-bordered table-striped-column table-hover">
							<thead>
								<tr class="border-radius border-color">
									<th><s:text name="tgi.label.s.no" /></th>
									<th><s:text name="tgi.label.image" /></th>
									<th><s:text name="tgi.label.catagory" /></th>
									<th><s:text name="tgi.label.deal_type" /></th>
									<th><s:text name="tgi.label.title" /></th>

									<th><s:text name="tgi.label.price" /></th>
									<th>Currency</th>
									<th><s:text name="tgi.label.sequence" /></th>
									<th><s:text name="tgi.label.action" /></th>
								</tr>
							</thead>
							<s:iterator value="dealsList" status="i">
								<tbody>
									<tr>
										<td data-title="S.No">${i.count}</td>
										<td data-title="Image" scope="row"><img
											style="width: 100px; height: 100px;" id="modal"
											src="get_image_content?imgpath=${imageUrl}" class="postthumb"
											alt="undefined"> <%-- <img style="width:100px;height:100px;" id="modal" src="admin_deals_getimagebyid?id=${id}" class="postthumb" alt="undefined"> --%>

										</td>

										<td data-title="Catagory">${catagory}</td>
										<td data-title="Deal type">${dealsType}</td>
										<td data-title="Title">${title}</td>
										<td data-title="Currency">${currency}</td>
										<td data-title="Price">${price}</td>
										<td data-title="Action"><s:if test="active == false">
												<select id="setSequenceOrder_${id}">
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
												</select>
											</s:if> <s:else>${displaySequence}</s:else></td>

										<%-- <td><s:if test="active == true">
												<div class="btn btn-xs btn-danger submitPublishForm"
													id="submitPublishFormId" data-id="${id}"
													data-active="${active}">
													<s:text name="tgi.label.unpublish" />
												</div>
											</s:if> <s:else>
												<div class="btn btn-xs btn-success submitPublishForm"
													id="submitPublishFormId" data-id="${id}"
													data-active="${active}">
													<s:text name="tgi.label.publish" />
												</div>
											</s:else> <a href="admin_deals_preview.action?id=${id}"
											class="btn btn-xs btn-success"><s:text
													name="tgi.label.preview" /></a> <a
											href="admin_deals_edit.action?id=${id}"
											class="btn btn-xs btn-success"> <s:text
													name="tgi.label.edit" /></a> <a href="#"
											onclick="deletePopupCountryInfo('${id}','${version}')"
											class="btn btn-xs btn-success"><s:text
													name="tgi.label.delete" /></a> <input type="hidden"
											id="deleteItem"></td> --%>

										<td data-title="Action">
											<div class="btn-group">
												<a class="btn btn-xs btn-success dropdown-toggle"
													data-toggle="dropdown" style="padding: 3px 6px;"> <i
													class="fa fa-cog fa-lg" aria-hidden="true"></i>
												</a>
												<ul
													class="dropdown-menu dropdown-info pull-right align-left">
													<li class=""><s:if test="active == true">
															<div class="btn btn-xs  submitPublishForm"
																id="submitPublishFormId" data-id="${id}"
																data-active="${active}">
																&nbsp;&nbsp;<s:text name="tgi.label.unpublish" />
															</div>
														</s:if> <s:else>
															<div class="btn btn-xs  submitPublishForm"
																id="submitPublishFormId" data-id="${id}"
																data-active="${active}">
																&nbsp;&nbsp;<s:text name="tgi.label.publish" />
															</div>
														</s:else></li>
													<li class="divider"></li>
													<li class=""><a
														href="admin_deals_preview.action?id=${id}"
														class="btn btn-xs btnsuccess"><span class="glyphicon glyphicon-eye-open"></span> <s:text
																name="tgi.label.preview" /></a></li>
													<li class="divider"></li>
													<li class=""><a
														href="admin_deals_edit.action?id=${id}"
														class="btn btn-xs "><span class="glyphicon glyphicon-edit"></span> <s:text name="tgi.label.edit" /></a></li>
													<li class="divider"></li>
													<li class=""><a href="#"
														onclick="deletePopupCountryInfo('${id}','${version}')"
														class="btn btn-xs "><span class="glyphicon glyphicon-trash"></span> <s:text name="tgi.label.delete" /></a></li>
												</ul>
												<input type="hidden" id="deleteItem">
											</div>
										</td>
									</tr>


								</tbody>
							</s:iterator>
						</table>
					</div>

					<div class="">
								
								<div class="col-md-6">
								
<div class="" style="font-size=16px;margin-top:27px;">Showing 1 to 10 of 70 entries</div>
								</div>
								<div class="col-md-6">
                                    <ul class="pagination" style="float:right;">
									<li><a href="#"><s:text name="tgi.label.previous" /></a></a></li>
    <li class="disabled"><a href="#">&laquo;</a></li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">&raquo;</a></li>
	<li><a href="#"><s:text name="tgi.label.next" /></a></a></li>
</ul></div>


                                    <br><br>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
                
						<form action="admin_publish_deals.action" id="submitPublish">
<input type="hidden" name="id" id="id" value="${id}" />
<input type="hidden" name="key" id="key" value="${active}" />
<input type="hidden" name="displaySequence" id="displaySequenceHdn"/>
</form>



                  	   <s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'deleted')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>