
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="dj" uri="/struts-dojo-tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <link href="admin/css/inner-css/bug-tracker.css" rel="stylesheet" type="text/css">
 <style>
 
 </style>
 
 <section class="wrapper container-fluid"> 
	<div class=" ">
			<div class=" ">
	<section class="content-header">
		<h1>
			<i class="fa fa-bug"></i>Bug List
		</h1>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb mb-0 mt-0">
			<li class="set" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse" style="cursor: pointer;" href="#filters" aria-expanded="false">
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</li> 
				<li><a href="addBugTracker">Add Bug</a></li>
				<li  class="active">Bug List</li>
			</ol>
		</div>
	</section>
	<!-- Main content -->
					<div class="">  
							<div class="collapse filter-box" id="filters">
								<div class="well">
									<div class="clearfix">

										<!-- Filter of main info -->
										<div class=" filter-text clearfix">
											<div class="row row-minus"> 
												<div class="col-sm-2">
													<div class="form-group clearfix">
														<label class="control-label">BugTracker History</label>
														<div class="">
															<input type="hidden" id="reportTypeHidden"
																value="<s:property value="bugReportPage.flightReportFilter.reportType"/>">
															<select class="form-control" name="bugReportPage.flightReportFilter.reportType" id="reportType" required>
																<option value="1" selected="selected">My Bug History</option>
																<option value="0">All</option>
																
															</select>

														</div>
													</div>
												</div>
												
												<div class="col-sm-2">
													<div class="form-group clearfix">
														<label class="control-label">Assigned By</label>
														<div class="">
															<input type="text" class="form-control input-sm"
															 placeholder="assigned by" autocomplete="off" id="assignby" > 
							  								<input type="hidden" name="bugReportPage.assignedBy"    id="techHeadId" > 
								
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="form-group clearfix">
														<label class="control-label">Assigned To</label>
														<div class="">
															<input type="text" class="form-control input-sm" id="assignTo"  placeholder="assigned to" autocomplete="off">
								  							<input type="hidden" name="bugReportPage.assignTo" id="techSupportId"> 
														</div>
													</div>
												</div>
												<div class="filtr-btn col-sm-2 clearfix">
												<label class="control-label">&nbsp;</label>
												<button type="submit" class="btn btn-primary btn-block"
													name="bugReportPage.currentPageIndex"
													value="${bugReportPage.currentPageIndex}">Submit</button>
											</div>
											</div>
										</div>

										<div class="row row-minus">
											<div class="col-sm-6 clearfix cc-all">
												<a href=" " id="reset" class="text-right"><i
													class="fa fa-close"></i> Clear All</a>
											</div> 
										</div>
									</div>
								</div>
							</div>
						</div>
						
	<section class="col-md-12">
		<!-- Small boxes (Stat box) -->
		<div class="profile-timeline-card">
					<!--  report-search -->
					<input type="hidden" value="<s:property value="%{#session.Company.company_userid}"/>" id="companyUserId"> 
					<input type="hidden" value="<s:property value="%{#session.Company.Email}"/>" id="email">
					<input type="hidden" value="<s:property value="%{#session.User.company_userid}"/>" id="user_id">
					<input type="hidden" value="${bugReportPage.items}" id="buglist">
					<form action="goBugTrackerList" method="post" id="filterform">
							<input type="hidden" name="showType" value="<s:property value="showType"/>">
							<input type="hidden" id="companyType" value='<s:property value="filterCompanyType"/>'> 
							<input type="hidden" id="status" value='<s:property value="status"/>'> 
							<input type="hidden" value="<s:property value="%{#session.Company.company_userid}"/>" id="user_companyUserId" name="user_companyUserId">

					<div class="table-responsive dash-table">
					<table id="mytable1" class="table table-striped" style="white-space:nowrap;" 
						data-sort-name="name" data-sort-order="assc">
						<thead style="width:auto;padding: 0px; margin: 0px">
							<tr class="table-sub-header">
							<th class="p-0 pb-2">
                         	<label class="control-label text-left">Items Per Page </label> 
							<select class="form-control input-sm" name="bugReportPage.maxItems" id="maxItems" required onchange="this.form.submit()">
												<c:forEach var="maxItems" items="${bugReportPage.pageSizeQueue}">
													<c:choose>
														<c:when test="${bugReportPage.maxItems != null && bugReportPage.maxItems == maxItems}">
															<c:choose>
																<c:when test="${bugReportPage.maxItems == -1}">
																	<option value="${maxItems}" selected="selected">ALL</option>
																</c:when>

																<c:otherwise>
																	<option value="${maxItems}" selected="selected">${maxItems}</option>
																</c:otherwise>
															</c:choose>
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${maxItems == -1}">
																	<option value="${maxItems}">ALL</option>
																</c:when>

																<c:otherwise>
																	<option value="${maxItems}">${maxItems}</option>
																</c:otherwise>
															</c:choose>
														</c:otherwise>
													</c:choose>
												</c:forEach> 
											</select>
										</th>
                                        	<th colspan="15" class="noborder-xs">
                                        	<div class="pull-right" style="padding:4px;margin-top: -6px;margin-bottom: -10px;">   
                                        	<div class="btn-group">
											<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="New Travel Sales Lead">
											  <a class="btn btn-sm btn-default" href="addBugTracker" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class="">&nbsp;New&nbsp;</span></a> 
											</div>
											<div class="dropdown pull-left" style="margin-right:5px;">
											  <a class="btn btn-sm btn-default " href="#" >&nbsp;Import&nbsp;</a> 
											</div>
											<span class="line-btn">  | </span>
											<div class="dropdown pull-right" style="margin-left:5px;">
											  <a class="btn btn-sm btn-default collapsed filter-link filterBtn" id="" data-toggle="collapse" style="cursor: pointer;" href="#filters" aria-expanded="false" style="margin-bottom:2px;margin-right: 3px;" title="Show Filter Row">
											  <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;">
											  </a>  
											</div>
											<div class="dropdown pull-right" style="margin-left:5px;">
											<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadList.size()}" title="Please Select Row">
											  <button type="submit"  class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_row" style="margin-bottom: 2px;margin-right: 3px;" disabled="disabled">
											  <img class="clippy" src="admin/img/svg/cloud-data.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
											  </button>  
											</div>
											</div>
											  </div>
											</div> 
                                        	</th>
                                        </tr>
							<tr>
								<th class="th-primary column-max-width"> Type</th>
								<th class="th-primary column-max-width">S.No</th>
								<th class="th-primary col-md-1">Bug Id</th>
								<th class="th-primary ">Title</th>
								<th class="th-primary ">Url</th>
								<th class="th-primary">Priority</th>
								<th class="th-primary">Status</th>
								<th class="th-primary col-md-1">Assignee</th>
								<th class="th-primary col-md-1">Assigned To</th>
								<th class="th-primary col-md-1">Assigned At</th>
								<th class="th-primary col-md-1">Created</th>
								<th class="th-primary col-md-1">Updated</th>
								<th class="th-primary column-max-width">Action</th>
							</tr>
						</thead>
						
						  <tbody>
						<s:iterator value="bugReportPage.items" var="bugHistory" status="varStatus" >
						<tr>
						<td data-title="Select" class="">
						<c:choose>
						<c:when test="${bugType == 'Error' || bugType == 'Syntactic Error' || bugType == 'Calculation Error' || bugType == 'Logical Error' || bugType == 'Error handling errors' || bugType == 'Run time errors'}">
						<i class="fa fa-bug tooltips" data-toggle="tooltip" title="" data-original-title="${bugType}"></i>
						</c:when>
						<c:when test="${bugType == 'New Functionality' || bugType == 'New Work'}">
						<span data-toggle="tooltip" title="" data-original-title="${bugType}"> <img class="clippy" src="admin/img/svg/story.svg" height="16" width="16" alt="Story"></span>
						</c:when>
						<c:when test="${bugType == 'Task'}">
						<span data-toggle="tooltip" title="" data-original-title="${bugType}"> <img class="clippy" src="admin/img/svg/task.svg" height="16" width="16" alt="Story"></span>
						</c:when>
						<c:otherwise>
						<i class="fa fa-magic tooltips" data-toggle="tooltip" title="" data-original-title="${bugType}"></i>
						</c:otherwise>
						</c:choose>
						</td>
						<td>${varStatus.count}</td>
						<td >${referenceNo}</td>
						<td class="link"><a href="viewBugTracker?id=${bugTracker.id}"> ${title} </a></td>
						<td >
						<c:choose>
						<c:when test="${url != null}">
						${url}
						</c:when>
						<c:otherwise>
						None
						</c:otherwise>
						</c:choose>
						</td>
						<td>
						<c:choose>
						<c:when test="${level == 'Medium'}">
						<span class="label label-warning">${level}</span>
						</c:when>
						<c:when test="${level == 'Low'}">
						<span class="label label-success">${level}</span>
						</c:when>
						<c:otherwise>
						<span class="label label-danger">${level}</span>
						</c:otherwise>
						</c:choose>
						</td>
						<td>
						<c:choose>
						<c:when test="${status == 'New'}">
						<span class="bug-issue-status-max-width-medium bug-issue-status-info">${status}</span>
						</c:when>						
						<c:when test="${status == 'Pending'}">
						<span class="bug-issue-status-max-width-medium bug-issue-status-info">${status}</span>
						</c:when>						
						<c:when test="${status == 'WorkInProgress'}">
						<span class="bug-issue-status-max-width-medium bug-issue-status-info">${status}</span>
						</c:when>						
						<c:when test="${status == 'Pause'}">
						<span class="bug-issue-status-max-width-medium bug-issue-status-green">${status}</span>
						</c:when>						
						<c:when test="${status == 'Review'}">
						<span class="bug-issue-status-max-width-medium bug-issue-status-green">${status}</span>
						</c:when>						
						<c:when test="${status == 'Closed'}">
						<span class="bug-issue-status-max-width-medium bug-issue-status-green">${status}</span>
						</c:when>						
						<c:when test="${status == 'Verified'}">
						<span class="bug-issue-status-max-width-medium bug-issue-status-green">${status}</span>
						</c:when>						
						</c:choose>
						</td>
						<td>${assignedByName}</td>
						<td>${assignedToName}</td>
						<td>
						<c:choose>
							<c:when test="${assignDate!=null}">
									<s:date name="assignDate" format="dd/MMM/yyyy"/>
							</c:when>
							<c:otherwise>
							 NA
							</c:otherwise>
						</c:choose>
						</td>
						<td>${createDate}</td>
						<td>${updatedDate}</td>
								
						<td data-title="Action">
													<div class="dropdown pull-right" style="    margin-right: 20px;">
													  <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
													  <img class="clippy" src="admin/img/svg/caret-down.svg" width="10" alt="Caret Down"></button>
													  <ul class="dropdown-menu">
													    <li class="action"><a href="viewBugTracker?id=${bugTracker.id}">View Detail</a></li>
													    <li class="action"><a href="viewTestCases?bugId=${bugTracker.id}">View Test Case</a></li>
													    <li class="action">
													    <a href="#!" class="plus-icon" data-event="duplicate" data-toggle="modal" data-target="#assign_bug_tracker" data-bug-id="${bugTracker.id}" onclick="javascript:getBugTrackerData(this)">Assign To Others </a>
													    </li>
													    <c:if test="${session.User.userRoleId.techHead==true}">
													    <li class="action"><a href="#">Delete</a></li>
													    </c:if>
													  </ul>
													</div>
													</td> 
						</tr>
						 </s:iterator>
						 </tbody> 
						 </table> 
						 </div>
						  

								<table id="pagtable" class="pull-right">
									<tr id="tr">
										<span>Showing <s:property
												value="%{((bugReportPage.currentPageIndex - 1)*bugReportPage.maxItems)+1}" />
											to <s:property
												value="%{((bugReportPage.currentPageIndex*bugReportPage.maxItems) <= bugReportPage.availableItems)?(bugReportPage.currentPageIndex*bugReportPage.maxItems):bugReportPage.availableItems}" />
											of <s:property value="%{bugReportPage.availableItems}"/>
											items
										</span>

									</tr>
									<tr id="tr">

										<c:if test="${bugReportPage.currentPageIndex>1}">
											<td id="td"><button type="submit"
													name="bugReportPage.currentPageIndex" value="1"
													class="btn btn-primary">First</button></td>
											<td id="td"><button type="submit"
													name="bugReportPage.currentPageIndex"
													value="${bugReportPage.currentPageIndex - 1}"
													class="btn btn-primary">Prev</button></td>
										</c:if>

										<c:forEach
											begin="${(bugReportPage.currentPageIndex) > 1? (bugReportPage.currentPageIndex) : 1}"
											end="${ (bugReportPage.currentPageIndex + 4) <= bugReportPage.availablePages ? (bugReportPage.currentPageIndex + 4) :  bugReportPage.availablePages }"
											var="i">
											<td>
												<button type="submit"
													name="bugReportPage.currentPageIndex" value="${i}"
													class="btn btn-primary">
													<c:choose>
														<c:when test="${bugReportPage.currentPageIndex == i}">
															<u>${i}</u>
														</c:when>

														<c:otherwise>
									${i}								
								</c:otherwise>
													</c:choose>
												</button>
											</td>
											
											
											
											
										</c:forEach>
										<c:if
											test="${(bugReportPage.currentPageIndex + 4) < bugReportPage.availablePages}">
											<td id="td"><button type="submit"
													name="bugReportPage.currentPageIndex"
													value="${bugReportPage.currentPageIndex + 1}"
													class="btn btn-primary">Next</button></td>
											<td id="td"><button type="submit"
													name="bugReportPage.currentPageIndex"
													value="${bugReportPage.availablePages}"
													class="btn btn-primary">Last</button></td>
										</c:if>

									</tr>
								</table>
</form>
							</div>
<!--Assign task to other Form  -->
	<div class="modal fade" id="assign_bug_tracker" role="dialog">
					<div class="modal-dialog modal-lg">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title text-center">Assign To Other</h4>
								<button type="button" class="close slds-modal__close" data-dismiss="modal">
								<img class="clippy" src="admin/img/svg/cancel-bl.svg" width="20" alt="Copy to clipboard">
								</button>
							</div>
							<form id="assignBugTrackerForm" method="post" class="bv-form">
								<div class="modal-body">
								 <div class="row mt-20">
								 	<c:choose>
								 	<c:when test="${session.User.userRoleId.techHead==true}">
								 	<div class="col-md-6">
											<label class="control-label text-left">Assign By</label>
												<div class="controls">
														<div class="form-group">
															<select name="bugTracker.assignedBy" id="assign-by" class="form-control">
															<option value="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="assign">
																		<option value="${assign.userId}" ${assign.userId == session.User.id ? 'selected': ''} >${assign.firstName} ${assign.lastName}</option>
															</c:forEach>
														</select> 
														</div>
												</div>
											</div>
								 	</c:when>
								 	<c:otherwise>
								 	<input type="hidden" name="bugTracker.assignedBy" value="<s:property value="%{#session.User.id}"/> ">
								 	</c:otherwise>
								 	</c:choose>
								 		<div class="<c:choose><c:when test="${session.User.userRoleId.techHead ==true}">col-md-6</c:when><c:otherwise>col-md-12</c:otherwise> </c:choose>">
											<label class="control-label text-left">Assign To</label>
												<div class="controls">
														<div class="form-group">
															<select name="bugTracker.assignTo" id="assign-to" class="form-control">
															<option value="0" selected="selected">Select User</option>
															<c:forEach items="${userVos}" var="assign">
																		<option value="${assign.userId}">${assign.firstName} ${assign.lastName}</option>
															</c:forEach>
															</select>
														</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="checkbox pull-right">
							                        <input id="checkbox1" type="checkbox" name="mailNotify" value="false">
							                        <label for="checkbox1">&nbsp;<span class="text-danger">Send notification email</span></label>
							                    </div>
						                    </div>
						                    <div class="col-md-12 mt-20">
						                    <p class="text-center"><span class="text-danger">*</span>The <strong>new owner</strong> will also become the owner of these records related to <strong><span class="is-owner"></span></strong> that are owned by you.</p>
						                    </div>
										</div>
							</div>
								<div class="modal-footer">
									<input type="hidden" name="bugTracker.id" id="bug-tracker-id" value="">
									<button type="submit" class="btn btn-default" data-dismiss="modal" style="float: left;">Cancel</button>
									<button type="submit" class="btn btn-success" id="changeOwnerSubmitBtn" data-modal-id="assign_bug_tracker" data-form-name="assignBugTrackerForm">Save</button>
								</div>
							</form>
						</div>

					</div>
				</div>
	</section>
	</div></div>
	</section>
<script>
	$(document).ready(function() {
		$('#supplierName').focus(function() {
			$(this).val('');
		});

	});
	function getBugTrackerData(sel) {
		var bugId = $(sel).data("bug-id");
		$("#bug-tracker-id").val(bugId);
	}
	$("#changeOwnerSubmitBtn").click(function(e) {
		e.preventDefault();
		var formDeleteName = $(this).attr("data-form-name");
		var modalId = $(this).attr("data-modal-id");
		$.ajax({
			url : "updateBugTrackerOwner",
			type : "POST",
			dataType : 'json',
			data : $("#" + formDeleteName).serialize(),
			success : function(jsonData) {
				if(jsonData.message.status == 'success'){
					$("#" + modalId).hide();
					alertify.success(jsonData.message.message);
					}
  				else if(jsonData.message.status == 'error'){
  					$("#" + modalId).hide();
  					alertify.error(jsonData.message.message);}
				setTimeout(location.reload.bind(location), 1000);
			},
			error : function(request, status, error) {
				alertify.error("somthing wrong,please try again");
			}
		});
	});
/*--------------------------script for delete tour prices multiple-----------------------------*/
	$('#delete_all_row').on("click", function(event){
		// confirm dialog
		 var allVals = [];
		 $(".check_row:checked").each(function() {  
			 allVals.push($(this).attr('data-id'));
		 });  
			 var check = confirm("Are you sure you want to delete selected data?");
			 if(check == true)
			 {
				 var join_selected_values = []; 
				 join_selected_values = allVals.join(",");
				 $.ajax({
							url : "deleteMultipleBugTracker?bugTrackerIds="+join_selected_values,
							type : "POST",
							dataType: 'html',
							success : function(data) {
								alertify.success("You've clicked OK Your data have successfully deleted");
								setTimeout("window.location.reload();", 1000);
							},
							error: function (request, status, error) {
								showModalPopUp("Item can not be deleted.","e");
							}
					}); 
			 }
			 else
			 {
				 return false;	 
			 }
	});
	</script>
	<script type="text/javascript" src="admin/js/admin/multiple-delete.js"> </script>
	<script>
	$(document).ready(function(){$(".filterBtn").click(function(){$("#filterDiv").toggle(500)}),
		 $("div.easy-autocomplete").removeAttr("style"),$("#configreset").click(function(){$("#resetform")[0].reset()})}),
		 $(".filter-link").click(function(){var e=$(this).find("i").hasClass("fa-angle-up");$(".filter-link").find("i").removeClass("fa-angle-down"),
		 $(".filter-link").find("i").addClass("fa-angle-up"),e&&$(this).find("i").toggleClass("fa-angle-up fa-2x fa-angle-down fa-2x")});

	</script>
