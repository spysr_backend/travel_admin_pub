<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>


        <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->

            <section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.passport_details" /></h5>
                                <div class="set pull-left">
                                    <!-- <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                              <%--   <div class="set pull-right">
                                    <div class="dropdown">
                                      <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <s:text name="tgi.label.dropdown" />
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#"><s:text name="tgi.label.action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.another_action" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.something_else_here" /></a></li>
                                        <li><a href="#"><s:text name="tgi.label.saparated_links" /></a></li>
                                      </ul>
                                    </div>
                               </div> --%>
                            </div>
                            <div class="hd clearfix">
                                <h5 class="pull-left"></h5>
                            </div>
							<div class="cnt cnt-table">
							<div class="table-responsive">
<s:if
						test="%{#session.Company!=null && #session.User!=null && #session.User.userRoleId.superuser==1}">
	<form class="form-inline" action="FilterdFrequentFlyerList"
							method="post">
                                    <table class="table table-form">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <tr>

                                                <td>
                                                    <div class="field-name"><s:text name="tgi.label.first_name" /></div>
                                                </td>
                                                <td>
                                                   <input type="text" class="form-control input-sm" id="twodpd2"
										placeholder="FirstName" name="firstName" value='<s:property value="firstName"/>'>
                                                </td>
												<td>
                                                    <div class="field-name"><s:text name="tgi.label.last_name" /></div>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control input-sm" id="twodpd2"
										placeholder="LastName" name="lastName" value='<s:property value="lastName"/>'>
                                                </td>
                                            </tr>
                                            
                                            <tr>
											 <td>
                                                    <div class="field-name"><s:text name="tgi.label.frequent_flyerno" /></div>
                                                </td>
                                               <td>
                                                  	<input type="text" class="form-control input-sm" id="twodpd2"
										placeholder="FlyerNo" name="frequent_flyer_number" value='<s:property value="frequent_flyer_number"/>'>
									
                                                </td>
												<td>
                                                    <div class="field-name"><s:text name="tgi.label.frequent_flyer_airline" /></div>
                                                </td>
                                               <td>
                                                 <input type="text" class="form-control input-sm" id="twodpd2"
										placeholder="Airline" name="frequent_flyer_airline" value='<s:property value="frequent_flyer_airline"/>'>
                                                </td>

												<th>&nbsp;</th>
												<th>&nbsp;</th>
												</tr>
                                           
                                         </tbody>
										
                                    </table>
									<div class="form-group table-btn">
                                        <div class="col-sm-10 col-sm-offset-2 ">
                                            
											<div class="pull-right">
                                           <button class="btn btn-primary " type="submit"><s:text name="tgi.label.show" /></button>
                                            </div>
                                        </div>
                                    </div></form></s:if><br><br><br>
 </div></div>
							<br>
                                <div class="cnt cnt-table">
											<div class="btn-group" data-toggle="buttons">
  <label class="btn btn-primary active">
    <input type="checkbox" checked autocomplete="off"> <s:text name="tgi.label.excel" />
  </label>
  <label class="btn btn-primary">
    <input type="checkbox" autocomplete="off"> <s:text name="tgi.label.pdf" />
  </label>
  <label class="btn btn-primary">
    <input type="checkbox" autocomplete="off"> <s:text name="tgi.label.print" />
  </label>
</div>
                                             
                                       
                            
                                <div class="table-responsive no-table-css">
                                        <table class="table table-bordered table-striped-column table-hover">
                                            <thead>
                                                <tr class="border-radius border-color">
                                                    <th><s:text name="tgi.label.firstname" /></th>
                                                    <th><s:text name="tgi.label.lastname" /></th>
                                                    <th><s:text name="tgi.label.passportnumber" /></th>
                                                    <th><s:text name="tgi.label.passportexpirydate" /></th>
													<th><s:text name="tgi.label.frequentflyernumber" /></th>
													<th><s:text name="tgi.label.frequentflyerairline" /></th>
                                                </tr>
                                                	<s:iterator value="#session.frequentflyerlist" status="serial">
												<tr>
													<td data-title="S.No"><s:property value="%{#serial.count}" /></td>
													<td data-title="First Name"><s:property value="firstName" /></td>
													<td data-title="Last Name"><s:property value="lastName" /></td>																
												   <td data-title="Passport No"><s:property value="passportNo" /></td>
												   <td data-title="Passport Exp Date"><s:property value="passportExpiryDate" /></td>
													<td data-title="FrequentFlyerNumber"><s:property value="frequent_flyer_number" /></td>
													<td data-title="FrequentFlyerAirline"><s:property value="frequent_flyer_airline" /></td>	
												</tr>
											</s:iterator>
                                            </thead>
                                            <tbody>
                                                <tr class="no-records-found"><td colspan="9"><s:text name="tgi.label.no_matching_records_found" /></td></tr>
                                               
                                                  
												
                                            </tbody>
                                        </table>
                                    </div>
								
								<div class="row">
								
								<div class="col-md-6">
								
<div class="" style="font-size=16px;margin-top:27px;">Showing 1 to 10 of 70 entries</div>
								</div>
								<div class="col-md-6">
                                    <ul class="pagination" style="float:right;">
									<li><a href="#"><s:text name="tgi.label.previous" /></a></li>
    <li class="disabled"><a href="#">&laquo;</a></li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">&raquo;</a></li>
	<li><a href="#"><s:text name="tgi.label.next" /></a></li>
</ul></div>


                                    <br><br>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        <!--ADMIN AREA ENDS-->
