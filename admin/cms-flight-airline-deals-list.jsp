<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="/struts-tags" prefix="s" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<link href="admin/filter_resource/easy-autocomplete.min.css" rel="stylesheet"
	type="text/css">
<script src="admin/filter_resource/jquery.easy-autocomplete.min.js"
	type="text/javascript"></script>


	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/buttons.dataTables.min.css">
	 <link rel="stylesheet" type="text/css" href="admin/print-pdf-excel/print-resource/jquery.dataTables.min.css">
	<script type="text/javascript" async="" src="admin/print-pdf-excel/print-resource/ga.js.download"></script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/jquery.dataTables.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/dataTables.buttons.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.flash.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/jszip.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/pdfmake.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/vfs_fonts.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.html5.min.js.download">
	</script>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.print.min.js.download">
	</script>
	<%-- <script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/demo.js.download">
	</script> --%>
	<script type="text/javascript" language="javascript" src="admin/print-pdf-excel/print-resource/buttons.colVis.min.js">
	</script>
	<script type="text/javascript" class="init">
	$(document).ready(function() {
		var currentDate = (new Date).getTime();
		$('#example').DataTable( {
			dom: 'Bfrtip',
			 
		buttons: [
		            {
		                extend: 'copyHtml5',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                }
		            },
		            {
		                extend: 'excelHtml5',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            {
		                extend: 'pdfHtml5',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            {
		                extend: 'print',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            {
		                extend: 'csv',
		                exportOptions: {
		                	columns: "thead th:not(.noExport)"
		                },
		                filename: currentDate
		            },
		            'colvis'
		        ]
			        
		} );
		
	} );

		</script>   
<style>
#example_filter input
{
    height: 25px;
}
#example_wrapper .dt-buttons
{
	padding-top: 10px;
}

</style>

       
<script type="text/javascript">
function deletePopupCountryInfo(id,version)
{
	$('#alert_box_delete').modal({
  	    show:true,
  	    keyboard: false
	    } );
	$('#alert_box_delete_body').text("Are your sure want to delete ?");
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	var finalUrl = newUrl+"deleteMarkupList?markupId="+id+"&version="+version;
	  $("#deleteItem").val(finalUrl);
}

$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"markupList";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	$('#alert_box_delete_ok').click(function() {
		window.location.assign($("#deleteItem").val()); 
			$('#alert_box_delete').hide();
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
 
 <script type="text/javascript">
	$(document).ready(
			function() {

				var company_list = [];
				var agents_list = [];
				  var user_list = []; 
				 

				$.ajax({
					//Action Name
					url :"CompanyListUnderSuperUser",
					dataType : "json",
					data : {
					 parent_company_user_id : $("#companyUserId").val(),
						email : $("#email").val()
					},

					success : function(data, textStatus, jqXHR) {

						var items = data.records;
						for (var i = 0; i < data.records.length; i++) {
							company_list.push(data.records[i].companyname + ","
									+ data.records[i].company_userid);
						}
						console.log(company_list);
						//response(items);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus);
					}
				});

				$("#search").autocomplete(
						{

							source : function(request, response) {
								var matcher = new RegExp('^'
										+ $.ui.autocomplete
												.escapeRegex(request.term),
										"i");
								response($.grep(company_list, function(item) {
									return matcher.test(item);

								}));
							},
							select : function(event, ui) {
								var label = ui.item.label;
								var company_userid = ui.item.value;
								console.log(company_userid);
							if(company_userid!=null){ 
								$.ajax({
									//Action Name
									url : "UserListUnderCompany",
									dataType : "json",
									data : {
										 company_user_id : company_userid 
									},

									success : function(data, textStatus, jqXHR) {
										 user_list=[];
										console.log("--data---------"+data);
										
										var items = data.user_records;
										
										for (var i = 0; i < data.user_records.length; i++) {
											user_list.push(data.user_records[i].username + "("+data.user_records[i].company_userid+")"  + ","
													+ data.user_records[i].id);
										}
										console.log(user_list);
										userlist(user_list);
									},
									error : function(jqXHR, textStatus, errorThrown) {
										console.log(textStatus);
									}
								});
							}
							
							 
							 }
						});
				
				
				$.ajax({
					//Action Name
					url :"AgentsListUnderSuperUser",
					dataType : "json",
					data : {
					 parent_company_user_id : $("#companyUserId").val(),
						email : $("#email").val()
					},

					success : function(data, textStatus, jqXHR) {

						var items = data.agentList;
						for (var i = 0; i < data.agentList.length; i++) {
							agents_list.push(data.agentList[i].username + "("+data.agentList[i].company_userid+")"  + ","
									+ data.agentList[i].id);
						}
						console.log("------agents_list------"+agents_list);
						 user_list=agents_list;
							console.log("------user_list------"+user_list);
						 userlist(user_list);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus);
					}
				});
				
			  });
	
	 function userlist(userlist)
			{
		 if(userlist.length>0){
				$("#userIdSearch").autocomplete(
						{
		 				source : function(request, response) {
								var matcher = new RegExp('^'
										+ $.ui.autocomplete
												.escapeRegex(request.term),
										"i");
								response($.grep(userlist, function(item) {
									return matcher.test(item);

								}));
							}
						});	 
		 }
		  
	}
 </script>
 
     <!--************************************
        MAIN ADMIN AREA
    ****************************************-->

        <!--ADMIN AREA BEGINS-->

            <section class="wrapper container-fluid">

                <div class="">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left">Flight Deals From</h5>
                                <div class="set pull-left">
                                   <!--  <button class="fa fa-pencil" id="button"></button>
                                    <button class="fa fa-trash" id="expand"></button> -->
                                </div>
                                <div class="set pull-right">
                                  
                                  
                                    <a id="add_button" class="btn btn-sm btn-success " href="addMarkup"><i
							class="fa fa-plus fa-lg" aria-hidden="true"
							style="margin-right: 6px;"></i>Create<span class="none-xs">
								<s:text name="tgi.label.flight_markup" /></span></a>
                             
                               </div>
                                  <div class="set pull-right">
                               <div id="category-tabs">
									<button style="padding:4px;" class="btn btn-success" type="reset" id="filterBtn">
								<a class="collapsed faq-links" data-toggle="collapse"  href="#collapseFive" aria-expanded="false" >
								<i class="fa fa-compress "></i> <s:text name="tgi.label.filter" /></a></button>
								</div></div>
                               </div>
                                
                                <form action="flightMarkupList" class="filter-form">
                                <div class="clearfix" id="filterDiv" style="display:none;" >
                                <div class="form-group" style="margin-top:15px;">
                                 <div class="col-md-2">
                                <input type="hidden" id="HdncompanyTypeShowData" value="" />
                                <select name="companyTypeShowData" id="companyTypeShowData" class="form-control">
                                <option value="all" selected="selected"><s:text name="tgi.label.all" /></option>
                                <option value="my" selected="selected"><s:text name="tgi.label.my_self" /></option>
                                <option value="distributor"><s:text name="tgi.label.distributor" /></option>
                                <option value="agency"><s:text name="tgi.label.agency" /></option>
                                </select>
                                </div>
                                <div class="col-md-2">
                                <input type="text" name="name" id="markupname-json"
												placeholder="Markup Name...."
												class="form-control search-query" />
								</div>
								
								<div class="col-md-2">
								<input type="text" name="configname" id="configname-json"
												placeholder="Config Name...."
												class="form-control search-query" />
								</div>
                                 <div class="col-md-2">
                                <input type="text" name="airline" id="airline-json"
												placeholder="Airline...."
												class="form-control search-query" />
                                </div>
                                 <div class="col-md-2">
                                <input type="text" name="origin" id="origin-json"
												placeholder="Search By Origin...."
												class="form-control search-query" />
                                </div>
                                 <div class="col-md-2">
                                <input type="text" name="destination" id="destination-json"
												placeholder="Search By Destination...."
												class="form-control search-query" />
                                </div>
                                <div class="col-md-12">&nbsp;</div>
                               <div class="col-md-2">
                                <input type="text" name="arrvDate" id="datepicker_arrvDate"
												placeholder="Arrival Date...."
												class="form-control hasdatepicker" />
                                </div>
                                 <br><br>
                                <br>
                               <div class="col-md-2">
                                <input type="text" name="deptDate" id="datepicker_deptDate"
												placeholder="Departure Date...."
												class="form-control hasdatepicker" />
                                </div>
                               <div class="col-md-2">
                                <input type="text" name="promofareStartDate" id="promo_startDate"
												placeholder="PromoStarts...."
												class="form-control hasdatepicker" />
                                </div>
                                <div class="col-md-2">
								<input type="text" name="promofareEndDate" id="promo_endDate"
												placeholder="Promo Ends...."
												class="form-control search-query" />
								</div> 
                                <div class="col-md-2"><button type="submit" class="btn btn-info">
      							<span class="glyphicon glyphicon-search"></span><s:text name="tgi.label.search" />
    							</button></div>
    							</div>
    							</div>
                                </form>
									

							<input type="hidden" value="" id="companyUserId">
							<input type="hidden" value="yogesh@spysr.in" id="email">
							<input type="hidden" value="" id="user_id">
				
							  	 <div class="cnt cnt-table">
<div class="dash-table no-table-css">
		<div class="box ">
		<div class="fw-container">
		<div class="fw-body">
			<div class="content">
				<div id="example_wrapper" class="dataTables_wrapper table-responsive">
										<table id="example" class="display nowrap dataTable table-bordered " cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                                        <!-- <table id="mytable" class="table table-bordered table-striped-column table-hover"> -->
                                        <thead>
										<tr class="border-radius border-color" role="row">
                                                 <th><s:text name="tgi.label.sno" /></th>
												<th>Deal Page Number</th>
												<th>Airline Code</th>
												<th>Depart Range1</th>
												<th>Depart Range2</th>
												<th>Base Fare</th>
												<th>Tax</th>
												<th>Total Price</th>
												<th>Ticketing Date</th>
												<th><s:text name="tgi.label.action" /></th>
                                                </tr>
                                            </thead>
                                            <tbody>
						<s:if test="flightAirlineDealsList != null && flightAirlineDealsList.size >0">
						<s:iterator value="flightAirlineDealsList" status="i">
							<tr>
								<td data-title="S.No">${i.count}</td>
								<td data-title="Origin">${dealPageNumber}</td>
								<td data-title="Destination">${airlineCode}</td>
								<td data-title="Trip Type">${departRange1}</td>
								<td data-title="Trip Type">${departRange2}</td>
								<td data-title="Trip Type">${baseFare}</td>
								<td data-title="Trip Type">${tax}</td>
								<td data-title="Trip Type">${totalPrice}</td>
								<td data-title="Trip Type">${ticketingDate}</td>
								<td data-title="Action">
									<div class="btn-group">
										<a class="btn btn-xs btn-success dropdown-toggle"
											data-toggle="dropdown" style="padding: 3px 6px;"> <i
											class="fa fa-cog fa-lg" aria-hidden="true"></i>
										</a>
										<ul class="dropdown-menu dropdown-info pull-right align-left">
											<li class=""><a href="editFlightAirlineDeals.action?id=${id}"
												class="btn btn-xs "><span
													class="glyphicon glyphicon-edit"></span> <s:text
														name="tgi.label.edit" /></a></li>
											<li class="divider"></li>
											<li class=""><input type="hidden" id="deleteItem">
												<a href="deleteFlightAirlineDeals.action?id=${id}&version=${version}"	class="btn btn-xs "><span
													class="glyphicon glyphicon-trash"></span> Delete</a></li>
										</ul>
									</div>
								</td>
							</tr>
						</s:iterator>
					</s:if>
						</tbody>
                                        </table>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        
                                    </div>
								
							</div>
                            </div>
                        </div>
                    </div>
                    </div>
            </section>
        <!--ADMIN AREA ENDS-->
<script type="text/javascript" class="init">
$(document).ready(function() {
		$('#filterBtn').click(function() {
	    $('#filterDiv').toggle();
	});
		$('div.easy-autocomplete').removeAttr('style');
} );

$('.faq-links').click(function() {
	var collapsed = $(this).find('i').hasClass('fa-compress');

	$('.faq-links').find('i').removeClass('fa-expand');

	$('.faq-links').find('i').addClass('fa-compress');
	if (collapsed)
		$(this).find('i').toggleClass('fa-compress fa-2x fa-expand fa-2x')
});
	</script>	

 <%-- <script type="text/javascript">
      $(function () {
      
        $('#mytable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
             "info": true,
             "autoWidth": false,
             "search": {
          	    "regex": true,
          	  },
          "pagingType": "full_numbers",
          "lengthMenu": [10, 25, 50, 75, 100, 500 ],
          
            });
      });
    </script>
    
    <script>
    
    $(document).ready( function() {
        $('.ddd').dataTable( {
          "aLengthMenu": [[1, 5, 10, 20, -1], [1, 5, 10, 20, "All"]]
        } );
      } );
    </script>
   --%>  
    

 
<link href="admin/css/jquery-ui.css" rel="stylesheet" type="text/css"/>  
   <script src= https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js> </script>
	 <script>
 $(document).ready(function() 
 { 
	 $("#pickup-date").datepicker({
		 format: "dd-mm-yyyy"  
	 });
	 
	 $("#dropoff-date").datepicker({
		 format: "dd-mm-yyyy"  
	 });
	 
	 $("#promostart-date").datepicker({
		 format: "dd-mm-yyyy"  
	 });
	 
	 $("#promoend-date").datepicker({
		 format: "dd-mm-yyyy"  
	 });
});
	 
 </script> 

 
                   	   <s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'deleted')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
