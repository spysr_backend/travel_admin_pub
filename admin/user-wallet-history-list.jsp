<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
@media screen and (max-width:480px) {
	.detail-btn {
		margin-top: -27px;
		margin-left: 15px;
	}
}

.center {
	text-align: center;
}

a.collapsed {
	color: #02214c;
	text-decoration: none !important;
}

a.collapsed:hover {
	color: #337ab7;
	text-decoration: none !important;
}

.highlight {
	background-color: #F44336;
}

.highlight-light {
	background-color: #ff6559;
}

.line-btn {
	padding: 5px 0 0 5px;
	font-size: 20px;
	color: #9E9E9E;
}

.clippy {
	margin-bottom: 3px;
}

</style>

<!--************************************
        MAIN ADMIN AREA
    ****************************************-->

<!--ADMIN AREA BEGINS-->
<section class="wrapper container-fluid">
	<div class="row">
			<div class="card1">
				<div class="pnl">
					<div class="hd clearfix">
						<h5 class="pull-left"><img class="clippy" src="admin/img/svg/user.svg" width="25" alt="Wallet"> 
						<span class="link"><a href="userEdit?id=${userProfile.id}">${userProfile.firstName}</a></span> Wallet History : <small>Balance - <span class="follow-status-success"><i class="fa fa-inr" style="font-size:12px"></i> 
						<fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userProfile.userWallet.walletBalance}" /></span></small></h5>
						<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed" href="#!" ><span class="text-spysr">Filter (<span class="filter-count">0</span>)</span> </a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a href="getUserWalletTransactionHistory?userId=${userProfile.id}" class="collapsed " id="refresh-filter-form" style="cursor: pointer;"><span class="text-spysr" data-toggle="tooltip" data-placement="bottom" title="Refresh Filter"><img class="clippy" src="admin/img/svg/refresh.svg" width="18" alt="Copy to clipboard"></span></a>
					</div>
					<div class="set pull-right" style="margin-top:5px;font-size: 15px;">
						<a class="collapsed filter-link filterBtn" id="" data-toggle="collapse"  style="cursor: pointer;" data-toggle="collapse"  href="filterDiv" aria-expanded="false" >
						<span class="text-spysr"><span class="filter-text"></span> <img class="clippy" src="admin/img/svg/filter.svg" width="12" alt="Copy to clipboard" style="margin-bottom: 3px;"> Filter <i class="fa fa-angle-down"></i></span></a>
					</div>
					</div>
					<div class="row mt-20">
					<form action="listUserWalletHistoryFilter" class="filter-form" id="resetform" method="post">
								<div class="" id="filterDiv" style="display: none;">
								<div class="form-group">
								<div class="row">
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="createdDateRange" id="created_date_range" placeholder="Created At...." class="form-control input-sm search-query" />
								 </div></div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
							   <span data-toggle="tooltip" data-placement="top" title="Search By Credit Expiry Date">
								<input type="text" name="expiryDateFlag" id="created-at-to" placeholder="Expiry Date...." class="form-control input-sm search-query" />
								</span>
								 </div></div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="transactionNumber" id="transaction-json" placeholder="Search By Transaction No" class="form-control input-sm search-query" />
							   </div>
							   </div>
							   <div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<select class="form-control input-sm" name="walletType" id="wallet-type">
									 <option value="" selected="selected">Select Type</option>
									 <option value="oxyMoney">Oxy Money</option>
									 <option value="oxyBack">Oxy Back</option>
									 <option value="oxyCredit">Oxy Credit</option>
								</select>
							   </div>
							   </div>
								<div class="col-md-2 col-sm-6">
								<div class="form-group">
								<span data-toggle="tooltip" data-placement="top" title="Search By Action Type">
								<select class="form-control input-sm" name="actionType" id="wallet-type">
								 <option value="" selected="selected">Select Type</option>
								 <option value="FLIGHT_BOOKING">Flight Booking</option>
								 <option value="WALLET_ADD">Wallet Add</option>
								 <option value="HOTEL_BOOKING">Hotel Booking</option>
								 <option value="SIGNUP_REFER">Signup Refer</option>
								 <option value="FRIEND_REFER">Friend Refer</option>
								</select>
								</span>
								</div>
								</div>
								<div class="col-md-2 col-sm-6">
							   <div class="form-group">
								<input type="text" name="refferalCode" id="refferal-code" placeholder="Search By Reffral Code" class="form-control input-sm search-query" />
							   </div>
							   </div>
							   </div>
								   <div class="row">
									   <div class="col-md-10"></div>
									   <div class="col-md-1">
										<div class="form-group">
										<button type="reset" class="btn btn-danger btn-sm btn-block" id="configreset">&nbsp;Clear&nbsp;</button>
									</div>
									</div>
									<div class="col-md-1">
									<div class="form-group">
									<input type="hidden" name="filterFlag" id="filterFlag" value="true">
									<input type="hidden" name="userId" id="front-user-id" value="${userId}">
									<button type="submit" class="btn btn-info btn-sm btn-block" value="Search" >Search</button>
									</div>
								</div>
								</div>
						
						</div>
						</div>
				</form>
					</div>
					<div class="dash-table">
						<div class="content mt-0 pt-0">
							<div id="example_wrapper" class="dataTables_wrapper">
								<table id="example" class="display dataTable responsive nowrap" role="grid" aria-describedby="example_info" style="width: 100%;" cellspacing="0">
									<thead>
										<tr class="table-sub-header">
											<th colspan="6" class="noborder-xs">
												<div class="pull-left">
													<div class="btn-group">
														<div class="dropdown pull-left" style="margin-right: 5px;" data-toggle="tooltip" data-placement="top" title="Oxy Money Transactions History">
															<a class="btn btn-xs btn-default" href="listUserWalletHistoryFilter?walletType=oxyMoney&userId=${userId}">
															<label class="form-control-label">Oxy Money <span class="text-danger">( <i class="fa fa-inr" style="font-size:12px"></i>  <fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userProfile.userWallet.depositBalance}" />)</span></label></a>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Oxy Back Transactions History">
																<a class="btn btn-xs btn-default" href="listUserWalletHistoryFilter?walletType=oxyBack&userId=${userId}">
																	<label class="form-control-label">Oxy Back <span class="text-success">( <i class="fa fa-inr" style="font-size:12px"></i>  <fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userProfile.userWallet.cashbackBalance}" />)</span></label>
																</a>
															</div>
														</div>
														<div class="dropdown pull-left" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" title="Oxy Credit Transactions History">
																<a class="btn btn-xs btn-default" href="listUserWalletHistoryFilter?walletType=oxyCredit&userId=${userId}">
																	<label class="form-control-label">Oxy Credit <span class="text-primary">( <i class="fa fa-inr" style="font-size:12px"></i>  <fmt:formatNumber type = "number" maxFractionDigits = "3" value = "${userProfile.userWallet.creditBalance}" />)</span></label>
																</a>
															</div>
														</div>
													</div>
												</div>
											</th>
											<th colspan="5" class="noborder-xs">
												<div class="pull-right"
													style="padding: 4px; margin-top: -6px; margin-bottom: -10px;">
													<div class="btn-group">
														
														<div class="dropdown pull-left" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="Add Wallet Money">
														  <a class="btn btn-sm btn-default" href="add_user_wallet?userId=${userId}" ><img class="clippy" src="admin/img/svg/add1.svg" width="9"><span class=""> Wallet</span></a> 
														</div>
														<span class="line-btn"> | </span>
														<div class="dropdown pull-right" style="margin-left: 5px;">
															<a class="btn btn-sm btn-default filterBtn"
																data-toggle="dropdown"
																style="margin-bottom: 2px; margin-right: 3px;"
																title="Show Filter Row"> <img class="clippy"
																src="admin/img/svg/filter.svg" width="13"
																alt="Copy to clipboard" style="margin-bottom: 3px;">
															</a>
														</div>
														<%-- <div class="dropdown pull-right" style="margin-left: 5px;">
															<div class="" id="btn-title" data-toggle="tooltip" data-placement="top" data-size="${travelSalesLeadList.size()}" title="Please Select Row">
																<button type="submit" class="btn btn-sm btn-default" data-toggle="dropdown" id="delete_all_price" style="margin-bottom: 2px; margin-right: 3px;" disabled="disabled">
																	<img class="clippy" src="admin/img/svg/cloud-data.svg" width="15" alt="Copy to clipboard" style="margin-bottom: 3px;"> <span id="span-all-del">Archive</span>
																</button>
															</div>
														</div> --%>
													</div>
												</div>
											</th>
										</tr>
										<tr class="border-radius border-color" role="row">
											<th data-priority="1" style="width: 20px;"></th>
											<!-- <th style="width: 50px;" data-priority="2">
												<div class="center">
													<div data-toggle="tooltip" data-placement="top"
														title="Select All">
														<div class="checkbox checkbox-default">
															<input id="master_check" data-action="all"
																type="checkbox"> <label for="master_check">&nbsp;</label>
														</div>
													</div>
												</div>
											</th> -->
											<th data-priority="2"> Created At</th>
											<th data-priority="5">Action Type</th>
											<th data-priority="4"> Wallet Type</th>
											<th data-priority="6"> Transaction Id</th>
											<th data-priority="7"> Opening Balance (<i class="fa fa-inr" style="font-size:12px"></i>)</th>
											<th data-priority="8">Closing Balance (<i class="fa fa-inr" style="font-size:12px"></i>)</th>
											<th data-priority="3">Amount (<i class="fa fa-inr" style="font-size:12px"></i>)</th>
											<th data-priority="9" style="width: 125px">Transaction Type</th>
											<th data-priority="10">Expiry Date</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<s:if test="walletHistoryList.size > 0">
											<s:iterator value="walletHistoryList" status="rowstatus">
												<tr>
													<td scope="row" data-title="S.No"><div class="center"><s:property value="%{#rowstatus.count}" /></div></td>
													<%-- <td data-title="Select" class="select-checkbox">
														<div class="center">
															<div class="checkbox checkbox-default">
																<input id="checkbox1_sd_${rowstatus.count}" type="checkbox" value="false" data-id="${id}" class="check_row">
																<label for="checkbox1_sd_${rowstatus.count}"></label>
															</div>
														</div>
													</td> --%>
													<td data-title="Email"><c:choose><c:when test="${createdDate != null && createdDate != ''}">
													<s:date name="createdDate" format="dd MMM YYYY"/></c:when><c:otherwise><span class="">None</span></c:otherwise>
													</c:choose>
													</td>
													<td><c:choose><c:when test="${actionType != null}">${actionType}</c:when><c:otherwise>None</c:otherwise></c:choose></td>
													<td><span class="text-capitalize">
													<c:choose>
													<c:when test="${walletType == 'oxyMoney'}">
													<span class="text-danger">${walletType}</span>
													</c:when>
													<c:when test="${walletType == 'oxyBack'}">
													<span class="text-success">${walletType}</span>
													</c:when>
													<c:when test="${walletType == 'oxyCredit'}">
													<span class="text-primary">${walletType}</span>
													</c:when>
													</c:choose>
													</span></td>
													<td><c:choose> <c:when test="${transactionNumber != null}">${transactionNumber}</c:when><c:otherwise>N/A</c:otherwise></c:choose></td>
													<td><c:choose><c:when test="${openingBalance != null}"> <fmt:formatNumber value="${openingBalance}" maxFractionDigits="3"/></c:when><c:otherwise>None</c:otherwise></c:choose></td>
													<td><c:choose><c:when test="${closingBalance != null}"> <fmt:formatNumber value="${closingBalance}" maxFractionDigits="3"/></c:when><c:otherwise>None</c:otherwise></c:choose></td>
													<td><c:choose>
													<c:when test="${amount != null}">
													<c:choose><c:when test="${transactionType == 'Debit' }"><span class="text-danger" style="margin-left: -8px;">- <fmt:formatNumber value="${amount}" maxFractionDigits="3"/></span></c:when> 
													<c:when test="${transactionType == 'Credit' }"><span class="text-success" style="margin-left: -8px;">+ <fmt:formatNumber value="${amount}" maxFractionDigits="3"/></span></c:when> </c:choose>
													</c:when>
													<c:otherwise>None</c:otherwise></c:choose></td>
													<td><c:choose>
													<c:when test="${transactionType == 'Debit'}"><span class="oxy_status_bg_danger">${transactionType}</span></c:when>
													<c:when test="${transactionType == 'Credit'}"><span class="oxy_status_bg_success">${transactionType}</span></c:when>
													<c:otherwise>None</c:otherwise></c:choose></td>
													
													<td><c:choose><c:when test="${expiryDate != null}"><s:date name="expiryDate" format="dd/MMM/YYYY hh:mm a"/></c:when><c:otherwise>None</c:otherwise></c:choose></td>
													<td></td>
												</tr>
											</s:iterator>
										</s:if>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
					<div class="col-md-6">
				    <div class="notice notice-danger">
				        <strong>Oxy Money &nbsp;:</strong> Actual Money
				    </div>
					<div class="notice notice-success">
				        <strong>Oxy Back  &nbsp;&nbsp;&nbsp;&nbsp;:</strong> Cashback on Product
				    </div>
				    <div class="notice notice-info">
				        <strong>Oxy Credit &nbsp;:</strong> Points on Product (1 credit value = <i class="fa fa-inr" style="font-size:12px"></i> 1)
				    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script >
//filter Autocompatator 
var TNumber={url:"getUserWalletHistoryJson.action?data=transactionNumber&userId="+$("#front-user-id").val(),getValue:"transactionNumber",list:{match:{enabled:!0}}};$("#transaction-json").easyAutocomplete(TNumber);
var CName={url:"getUserWalletHistoryJson.action?data=refferalCode&userId="+$("#front-user-id").val(),getValue:"refferalCode",list:{match:{enabled:!0}}};$("#refferal-code").easyAutocomplete(CName);
</script>
<script type="text/javascript" src="admin/js/filter/filter-comman-js.js"></script>
<script type="text/javascript" src="admin/js/filter/front-user.js"></script>
<script type="text/javascript" src="admin/js/admin/multiple-archive.js"></script>