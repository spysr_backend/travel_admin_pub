<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
$(function() {
	var totUrl=$(location).attr('href');
	var newUrl=totUrl.substr(0,totUrl.lastIndexOf('/')+1);
	  var finalUrl = newUrl+"userEdit?id="+"${param.id}";
	$('#alert_box_info_ok').click(function() {
	 window.location.assign(finalUrl); 
		$('#alert_box_info').hide();
		
	});
	$('#alert_box_ok').click(function() {
		 window.location.assign(finalUrl); 
			$('#alert_box').hide();
			
		});
	  $('#cancel').click(function() {
		   $('#error-alert').hide();
			
		});  
 });
 </script>
<style>
.ms-parent.input-md {
    width: 100% !important;
}
</style>
	<s:if test="userProfile!=null">
		
		 <section class="wrapper container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pnl">
			<!-- Main content -->
				<div class="hd clearfix">
                                <h5 class="pull-left"><s:text name="tgi.label.edit_employee_profile" /></h5>
					  <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									<a class="btn btn-xs btn-success dropdown-toggle"
									data-toggle="dropdown" style="padding: 3px 6px;"> <i
									class="fa fa-cog fa-lg" aria-hidden="true"></i></a>
									
									<ul class="dropdown-menu dropdown-info pull-right align-left">
									<li class=""><a class="" href="userList"><span class="glyphicon glyphicon-list"></span> <s:text name="tgi.label.employee_list" /> </a></li>
										<li class="divider"></li>
									<li class=""> <a href="resetCompanyUserPassword?id=<s:property value="userProfile.id"/>"class="" data-toggle="modal">
														<span class="glyphicon glyphicon-log-in"></span> <s:text name="tgi.label.reset_password" />
														</a>  </li>
															<li class="divider"></li>
									<li class=""><a href="agentDetails?id=<s:property value="id"/>"
												class="btn  btn-xs" data-title="Update"> <span
												data-placement="top" class="fa fa-plus-circle"></span> View Profile
											</a></li>
								</ul>
								</div>
								</div>
                               	</div> 
				</div>

					<!-- left column -->
				
					 <s:if test="%{#session.Company!=null && #session.User!=null && #session.User.userRoleId.isSuperUser()}">
						 <s:if test="hasActionErrors()">

							<div class="succfully-updated clearfix" id="error-alert">

								<div class="col-sm-2">
									<i class="fa fa-check fa-3x"></i>
								</div>

								<div class="col-sm-10">

									<p>
										<s:actionerror />
									</p>

									<button type="button" id="cancel" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

								</div>

							</div>

						</s:if>

						<s:if test="hasActionMessages()">
							<div class="sccuss-full-updated" id="success-alert">
								<div class="succfully-updated clearfix">

									<div class="col-sm-2">
										<i class="fa fa-check fa-3x"></i>
									</div>

									<div class="col-sm-10">
										<s:actionmessage />
										<button type="button" id="success" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

									</div>

								</div>
							</div>
						</s:if>
						 </s:if>
						 
					<s:else>

			
			 <s:if test="hasActionErrors()">

							<div class="succfully-updated clearfix" id="error-alertelse">

								<div class="col-sm-2">
									<i class="fa fa-check fa-3x"></i>
								</div>

								<div class="col-sm-10">

									<p>
										<s:actionerror />
									</p>

									<button type="button" id="cancelelse" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

								</div>

							</div>

						</s:if>

						<s:if test="hasActionMessages()">
							<div class="sccuss-full-updated" id="success-alertelse">
								<div class="succfully-updated clearfix">

									<div class="col-sm-2">
										<i class="fa fa-check fa-3x"></i>
									</div>

									<div class="col-sm-10">
										<s:actionmessage />
										<button type="button" id="successelse" class="btn btn-primary"><s:text name="tgi.label.ok" /></button>

									</div>

								</div>
							</div>
						</s:if>		
					
					
					 </s:else>
				

					<!-- edit form column -->
					<div class="cnt cnt-table">
                    <section class="content">
                     <div class="row">          
					<div class=" col-sm-12  personal-info">
						<form class="form-horizontal" role="form" enctype="multipart/form-data" action="superUserProfileUpdate" id="user-update" method="post">
							
							<input type="hidden" name="id" value="<s:property value="userProfile.id"/>">
							<input type="hidden" name="userRoleId.roleid"	value="<s:property value="userRoleId.roleid"/>">
							
							 <div class="col-sm-4">
										<div class="col-sm-12 ">
						<div class="text-center">
						 <c:choose>
						 <c:when test="${userProfile.imagePath !=null}">
						  <img src="<s:url action='getImageByPath?imageId=%{userProfile.imagePath}'/>" 	class="avatar img-circle img-thumbnail" alt="profile image"/>
						 </c:when>
						 <c:otherwise>
						  <img src="admin/img/not-available.png" class="avatar img-circle img-thumbnail" alt="profile image"/>
						 </c:otherwise>
						 </c:choose>
						
							 
							<h6><s:text name="tgi.label.upload_a_different_photo" />...</h6>
							<div class="col-sm-6 ">
								<div id="fileinfo">

									<div id="fileError"></div><!-- required="required" -->


								</div>              
							</div>
						<%-- <input type="file" id="uploadimage" accept="image/*"
								 ng-file-select="onFileSelect($files)"
								class="text-center center-block well well-sm"  name="Imagepath" value="<s:property value="userProfile.Imagepath" />"> --%>
								
								<input type="file" style="width: 100%;" class="text-center center-block well well-sm"  name="uploadFile">
						</div>

					</div>
									</div>
									<div class="col-sm-8">
									
							<div class="form-group">
								<label for="Company" class="col-sm-2 control-label">
								Username 
								</label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<input type="text" class="form-control input-sm" id="username"
										name="userName"
										value="<s:property value="userProfile.userName"/>"
										placeholder="User ID " autocomplete="off" required> <input
										type="hidden" name="imagePath" ng-model="Imagepath"
										value="<s:property value="userProfile.imagePath"/>"></div></div>
								</div>
							</div>

							<div class="form-group">
								<label for="Website" class="col-sm-2 control-label"><s:text name="tgi.label.first_name" /></label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<input type="text" class="form-control input-sm"
										id="first-name" name="firstName"
										value="<s:property value="userProfile.firstName"/>"
										placeholder="First Name" autocomplete="off" required>
								</div></div></div>
							</div>

							<div class="form-group">
								<label for="Website" class="col-sm-2 control-label"><s:text name="tgi.label.last_name" /></label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<input type="text" class="form-control input-sm" id="last-name"
										name="lastName"
										value="<s:property value="userProfile.lastName"/>"
										placeholder="Last Name" autocomplete="off" required>
								</div></div></div>
							</div>
						<s:if test="%{#session.User.userRoleId.isSuperUser() || #session.Company.companyRole.isDistributor()}">
							<div class="form-group">
								<label for="Country" class="col-sm-2 control-label"><s:text name="tgi.label.emp_role" /></label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<select class="input-md" id="userroletype" name="userRoleTypeArray" multiple="multiple" >
												<!-- <option value="">Please select User Role </option> -->
														<option value="admin" selected><s:text name="tgi.label.admin" /></option>
														<option value="report"><s:text name="tgi.label.reports" /></option>
														<option value="order"><s:text name="tgi.label.order" /></option>
														<option value="cms"><s:text name="tgi.label.cms" /></option>
														<option value="crm"><s:text name="tgi.label.crm" /></option>
														<option value="developermode">Developer Mode</option>
														<option value="techsupport">Tech Support</option>
														<option value="techhead">Tech Head</option>
				
									</select>
											</div></div>
								</div>
							</div>
							<%-- <div class="form-group">
								<label for="Country" class="col-sm-2 control-label"><s:text name="tgi.label.status" /></label>
								<input type="hidden" value="<s:property value="%{#session.superUser_UserProfile.id}"/>" id="uniqueId">
								 <input type="hidden" value="<s:property value="%{#session.superUser_UserProfile.Status}"/>" id="status">

								<div class="col-sm-8">
									<select class="form-control input-sm" id="Status<s:property value="%{#session.superUser_UserProfile.id}"/>" name="Status" required>
										<option selected="selected" value="active"><s:text name="tgi.label.active" /></option>
										<option value="inactive"><s:text name="tgi.label.inactive" /></option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="Country" class="col-sm-2 control-label"><s:text name="tgi.label.lock" /> Status</label> 
								<input type="hidden" value="<s:property value="%{#session.superUser_UserProfile.Locked}"/>" id="lockstatus">
								<div class="col-sm-8">
									<select class="form-control input-sm" id="Locked<s:property value="%{#session.superUser_UserProfile.id}"/>" name="Locked" required>
										<option selected="selected" value="yes"><s:text name="tgi.label.yes" /></option>
										<option value="no"><s:text name="tgi.label.no" /></option>
									</select>
								</div>
							</div> --%>
							<div class="form-group">
								<label for="Website" class="col-sm-2 control-label">
									 <s:text name="tgi.label.login_attempts" /></label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<input type="text" disabled="disabled" class="form-control input-sm" name="attempt" id="attempt" value="<s:property value="userProfile.attempt"/>" placeholder="Password" autocomplete="off" required>
								</div></div></div>
							</div>
	</s:if>
							<div class="form-group">
								<label for="Email" class="col-sm-2 control-label"><s:text name="tgi.label.email" /></label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<input type="email" class="form-control input-sm" name="email" id="email" value="<s:property value="userProfile.email"/>" placeholder="Email" autocomplete="off" readonly="readonly"" >
								</div></div></div>
							</div>

							<div class="form-group">
								<label for="Address" class="col-sm-2 control-label"><s:text name="tgi.label.address"/></label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<textarea class="form-control input-sm" id="address"
										name="address" placeholder="Address" autocomplete="off"
										required><s:property
											value="userProfile.address" /></textarea>
								</div></div></div>
							</div>

							<div class="form-group">
								<label for="Country" class="col-sm-2 control-label"><s:text name="tgi.label.country"/></label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<select class="form-control input-sm" name="countryName"
										id="country" autocomplete="off" required>
										<option
											value="<s:property value="userProfile.countryName"/>"
											selected="selected"><s:property
												value="userProfile.countryName" /></option>

										<s:iterator value="CountryList">
											<option value="<s:property value="c_name"/>"><s:property
													value="c_name"></s:property></option>
										</s:iterator>
									</select></div></div>
								</div>
							</div>

							<div class="form-group">
								<label for="City" class="col-sm-2 control-label"><s:text name="tgi.label.language"/> <s:property value="userProfile.language"/> </label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<select class="form-control input-sm" name="language"
										id="language_id" autocomplete="off" required>
										<option
											value="<s:property value="userProfile.language"/>"
											selected="selected"><s:property
												value="userProfile.language" /></option>

										<s:iterator value="LanguageList">
											<option value="<s:property value="language"/>"><s:property
													value="language"></s:property></option>
										</s:iterator>

									</select></div></div>
								</div>
							</div>




							<div class="form-group">
								<label for="City" class="col-sm-2 control-label">City</label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<input type="text" class="form-control input-sm" name="city"
										id="city" placeholder="City" autocomplete="off" required
										value="<s:property value="userProfile.city"/>">
								</div></div></div>
							</div>

							<div class="form-group">
								<label for="telphone" class="col-sm-2 control-label"><s:text name="tgi.label.phone"/></label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<input type="tel" class="form-control input-sm" name="phone"
										id="telphone"
										value="<s:property value="userProfile.phone"/>"
										placeholder="8105979291" autocomplete="off" required>
								</div></div></div>
							</div>
							<div class="form-group">
								<label for="Description" class="col-sm-2 control-label"><s:text name="tgi.label.description"/></label>
								<div class="col-sm-8"><div class="form-group"><div class="col-md-12">
									<textarea class="form-control input-sm" id="description"
										name="description" placeholder="Description"
										autocomplete="off" required><s:property
											value="userProfile.description" /></textarea>
								</div></div></div>
							</div>

						<%-- 	<div class="form-group">
								<label for="Country" class="col-sm-2 control-label">
									<s:text name="tgi.label.security-question"/></label>
								<div class="col-sm-8">
									<select class="form-control input-sm" id="Question"
										name="Securityquestion" required>
										<option selected="selected"><s:property
												value="%{#session.superUser_UserProfile.Securityquestion}" /></option>
										<option><s:text name="tgi.label.what_was_your_childhood_nickname"/></option>
										<option><s:text name="tgi.label.what_is_the_name_of_your_favorite_childhood_friend"/></option>
										<option><s:text name="tgi.label.what_street_did_you_live_on_in_third_grade"/></option>
										<option><s:text name="tgi.label.what_is_your_oldest_sibling's_birthday_month_and_year"/></option>
										<option><s:text name="tgi.label.what_is_the_middle_name_of_your_oldest_child"/></option>
										<option><s:text name="tgi.label.what_is_your_oldest_sibling's_middle_name"/></option>
										<option><s:text name="tgi.label.what_school_did_you_attend_for_sixth_grade"/></option>
										<option><s:text name="tgi.label.what_was_the_name_of_your_first_stuffed_animal"/></option>
										<option><s:text name="tgi.label.What_is_your_maternal_grandmother's_maiden_name"/></option>
										<option><s:text name="tgi.label.In_what_town_was_your_first_job"/>
                                            </option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="Question" class="col-sm-2 control-label"><s:text nam="tgi.label.answer"/></label>

								<div class="col-sm-8">
									<input type="text" class="form-control input-sm" id="Answer"
										name="Securityanswer"
										value="<s:property value="%{#session.superUser_UserProfile.Securityanswer}"/>"
										placeholder="Answer" autocomplete="off" required>
								</div>
							</div> --%>

							<div class="form-group">
								<label class="col-md-3 control-label"></label>
								<div class="col-md-8">
									<input class="btn btn-primary" value="Save Changes"
										type="submit">
								</div>
							</div>
									
									</div>						
							</form>
							</div><!-- row -->

						</div>
</section></div>
					
					 </div></div>
					</div>
					</section>
				<!-- /.row -->
			<!-- /.content -->
	</s:if>

<!--bootstrap validater  -->
 <%--  <script type="text/javascript">
$(document).ready(function() {
$('#user-update')
.bootstrapValidator(
		{
			// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-remove',
				validating : 'fa fa-refresh'
			},
			fields : {
				Username : {
					message : 'Username CompanyName is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Username'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'Username  must be more than 3 and less than 20 characters long'
						} 
					}
				},
				userroletype : {
					message : 'Employee Role is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Employee Role '
						},
					}
				},
				Firstname : {
					message : 'Firstname is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Firstname'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'Firstname  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				Lastname : {
					message : 'Lastname is not valid',
					validators : {
						 notEmpty : {
							message : 'Select enter a Lastname'
						},
						stringLength : {
							min : 3,
							max : 20,
							
							message : 'Lastname  must be more than 3 and less than 20 characters long'
						} 
						
					}
				},
				Email : {
					message : 'Email is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Email'
						},
						
					}
				},
				password : {
					message : 'Password Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Password'
						},
						stringLength : {
							min : 6,
							max : 20,
							
							message : 'Password  must be more than 6 and less than 20 characters long'
						} 
						
					}
				},
				Address : {
					message : 'Address Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Address'
						},
						stringLength : {
							min : 3,
							max :100,
							
							message : 'Address  must be more than 6 and less than 100 characters long'
						} 
						
					}
				},
				City : {
					message : 'City Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a City'
						},
						stringLength : {
							min : 0,
							max :50,
							
							message : 'City  must be more than 2 and less than 50 characters long'
						} 
						
					}
				},
				Description : {
					message : 'Description Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Description'
						},
						stringLength : {
							min : 0,
							
							
							message : 'Description  must be more than 2 characters long'
						} 
						
					}
				},
				Securityanswer : {
					message : 'Answer Type is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Answer'
						},
						stringLength : {
							min : 0,
							
							
							message : 'Answer must be more than 2 characters long'
						} 
						
					}
				},
				Phone : {
					message : 'Phone is not valid',
					validators : {
						 notEmpty : {
							message : 'Please enter a Phone'
						},
						integer: {
	                        message: 'The value is not an integer'
	                    },
						stringLength : {
							min : 4,
							max :18,
							
							message : 'Phone  must be more than 2 and less than 15 characters long'
						} 
						
					}
				},
				Status : {
					message : 'Status is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Status'
						},
						
					}
				},
				Countryname : {
					message : 'Country is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Country'
						},
						
					}
				},
				Language : {
					message : 'Language is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Language'
						},
						
					}
				},
				Securityquestion : {
					message : 'Question is not valid',
					validators : {
						 notEmpty : {
							message : 'Please select a Question'
						},
						
					}
				},
	
			}
		}).on('error.form.bv', function(e) {
	// do something if you want to check error 
}).on('success.form.bv', function(e) {
	/* notifySuccess(); */
	showModalPopUp("Saving Details, Please wait ..","i");
	
}).on('status.field.bv', function(e, data) {
	if (data.bv.getSubmitButton()) {
		console.debug("button disabled ");
		data.bv.disableSubmitButtons(false);
	}
});
});
</script> --%>
	<!-- /.content-wrapper -->
	<%-- <script src="admin/js/app.js" type="text/javascript"></script> --%>
	

<script type="text/javascript">
	function checkPasswordMatch() {
	    var password = $("#password").val();
	    var confirmPassword = $("#confirmPwd").val();

	    if (password !=confirmPassword)
	        $("#error").html("Passwords are not match!");
	    
	    else{
	    	 
	    	 $("#error").html("");
	     
	     
	}
	}

	$(document).ready(function () {
	   $("#confirmPwd").keyup(checkPasswordMatch);
	});
/*------------------------------------------------------------*/
	$(document).ready(function() {
		$("#userroletype").multipleSelect();
		
		var arrayuserroleType = '${userroletypeView}'.split(',');
		console.debug(arrayuserroleType);
		$("#userroletype").multipleSelect("setSelects", arrayuserroleType);
		
		//$('div.ms-parent').setAttribute("style", "width: 100%;");
	});
	
</script>
	
	      	   <s:if test="message != null && message  != ''">
						 <script src="admin/js/jquery.min.js"></script>
				<script src="admin/js/bootstrap.min.js"></script>
						<c:choose>
						<c:when test="${fn:contains(message, 'successfully')}">
                	  <script>
                	  $(document).ready(function() 
                			  {
                	  $('#alert_box').modal({
	      	        	    show:true,
	      	        	    keyboard: false
	      	    	    } );
	      			  $('#alert_box_body').text('${param.message}');
                			  });
							</script>
						</c:when>
						<c:otherwise>
                	  <script>
                	  $(document).ready(function() 
                			  {
                		  $('#alert_box_info').modal({
  	      	        	    show:true,
  	      	        	    keyboard: false
  	      	    	    } );
  	      			  $('#alert_box_info_body').text("${param.message}");
                			  });
							</script>
						</c:otherwise>
						</c:choose>
						</s:if>
				