<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  <link href="admin/css/user-profile.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="admin/js/admin/panel-app.js"></script>
  
  <link href="admin/css/select/bootstrap-select.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="admin/js/select/bootstrap-select.min.js"></script>
     <style>
     .btn-spysr-blue:hover, .btn-spysr-blue:focus, .btn-spysr-blue.focus {
    color: #ffffff !important;
    text-decoration: none;
}
     </style>
   
  <script>
  /* checkbox button js */
  $(document).ready(function(){
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }
        // Initialization
        function init() {
            updateDisplay();
            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
  </script>
      <!--************  MAIN ADMIN AREA BEGINS *************-->
            <section class="wrapper container-fluid"> 
                <div class="row">
                <div class="pnl">
                <nav aria-label="breadcrumb">
				  <div class="hd clearfix own-bradcum mb-4">
				  			<div class="dropdown pull-right mt-10">
							   <a href="listTravelSalesMyLead" class="btn btn-xs btn-outline-danger dropdown-toggle" type="button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back To List</a>
							</div>
				    <h4 class=""><img class="clay" src="admin/img/svg/tourist.svg" width="26" alt="Higest"> Travel Sales My Lead<span class="text-danger">${message}</span></h4>    
				  </div>
				</nav>
				</div>
					<div class="col-md-12">
					<div class="container-detached">
						<div class="content-detached">

									<!-- Profile info -->
									<div class="panel panel-flat" style="border-top: 4px solid #02214c;">
										<div class="panel-heading" style="padding: 22px 15px;">
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li class="icon-inline"><a data-action="collapse" class="icon-a" href="#content-overview" data-toggle="collapse" aria-controls="content-overview"><img class="clippy" src="admin/img/svg/down-arrow.svg" width="12" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="reload" class="icon-a"><img class="clippy" src="admin/img/svg/reload.svg" width="12" alt="Copy to clipboard" ></a></li>
							                		<li class="icon-inline"><a data-action="close" class="icon-a"><img class="clippy" src="admin/img/svg/cross.svg" width="8" alt="Copy to clipboard" ></a></li>
							                	</ul>
						                	</div>
										</div>
 										<form class="from-horizontal" id="save_travel_sales_my_lead_form" enctype="multipart/form-data" method="post">
										<div class="panel-body" style="display: block;">
													<div class="col-md-6" style="border-right: 2px dotted #cecece;">
														<div class="col-md-12 form-group">
															<input type="text" name="title" value="" class="form-control" placeholder="Title">
														</div>
														<div class="col-md-6 form-group">
															<input type="text" name="firstName" value="" class="form-control" placeholder="First Name">
														</div>
														<div class="col-md-6 form-group">
															<input type="text" name="lastName" value="" class="form-control" placeholder="Last Name">
														</div>
														
														<div class="col-md-6 form-group">
															<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
															<input type="email" name="email" value="" class="form-control " placeholder="Email">
														</div>
														</div>
														<div class="col-md-6 form-group">
														<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-phone" aria-hidden="true"></i></span>
																<input type="text" name="mobile" value="" class="form-control" placeholder="Phone#2 (optional)">
														</div>
														</div>
														<div class="col-md-6 form-group">
															<input type="text" name="city" value="" class="form-control " placeholder="City">
														</div>
														<div class="col-md-6 form-group">
														<div data-toggle="tooltip" data-placement="top" title="Select Country">
								                            <select name="country" id="country" class="form-control selectpicker" data-live-search="true">
															    <option value="">Select Country</option>
																		<c:forEach items="${countryList}" var="country">
																			<option value="${country.countryName}" ${country.countryName == 'India' ? 'selected' : ''} >${country.countryName}</option>
																</c:forEach>
															</select> 
															</div>
														</div>
														<div class="col-md-6 form-group">
														<select aria-hidden="true" name="budget" id="budget" class="form-control selectpicker">
														   <option>Budget</option>
														   <option value="1">Economy (0 - 2 star)</option>
														   <option value="2">Standard (3 - 4 star)</option>
														   <option value="3">Luxury (5 star &amp; above)</option>
														</select>
														</div>
														<div class="col-md-6 form-group">
														<select aria-hidden="true" name="duration" id="duration" class="form-control selectpicker">
														  <option value="0">Duration*</option>
														  	<c:forEach items="${tourDurationMap}" var="duration">
																	<option value="${duration.key}">${duration.value}</option>
																</c:forEach>
														</select>
														</div>
														<div class="col-md-6 form-group">
														<div class="input-group"> <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
																<input type="text" name="departureDateFlag"  class="form-control" placeholder="Departure Date..." value="" id="departure-date">
														</div>
														</div>
														<div class="col-md-6 form-group">
														<div class="input-group"> <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
																<input type="text" name="arrivalDateFlag"  class="form-control" placeholder="Arrival Date..." value="" id="arrival-date">
														</div>
														</div>
														<div class="col-md-6 form-group">
														<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
																<input type="text" name="destinationFrom" id="destination-from" value="" class="form-control" placeholder="Destination From...">
														</div>
														</div>
														<div class="col-md-6 form-group">
														<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
																<input type="text" name="destinationTo" id="destination-to" value="" class="form-control" placeholder="Destination To...">
														</div>
														</div>
													</div>
													
													<div class="col-md-6">
													<div class="row" >
													<div class="row">
													<div class="col-md-6"><p class="text-danger"><span>Tour Services Want Customer *</span></p></div>
													<div class="col-md-6"></div>
													</div>
													<div class="row">
													<div class="col-md-2 form-group">
															<span class="button-checkbox">
														        <button type="button" class="btn" data-color="spysr-blue" id="tour-btn">Tour&nbsp;</button>
														        <input type="checkbox" name="tour" class="hidden" value="false" id="is-tour"/>
														    </span>
													</div>
													<div class="col-md-2 form-group">
															<span class="button-checkbox">
														        <button type="button" class="btn" data-color="spysr-blue" id="flight-btn">Flight</button>
														        <input type="checkbox" name="flight" class="hidden" value="false" id="is-flight"/>
														    </span>
													</div>
													<div class="col-md-2 form-group">
															<span class="button-checkbox">
														        <button type="button" class="btn" data-color="spysr-blue" id="hotel-btn">Hotel</button>
														        <input type="checkbox" name="hotel" class="hidden" value="false" id="is-hotel"/>
														    </span>
													</div>
													
													<div class="col-md-2 form-group">
															<span class="button-checkbox">
														        <button type="button" class="btn" data-color="spysr-blue" id="car-btn">&nbsp;Car&nbsp;&nbsp;</button>
														        <input type="checkbox" name="car" class="hidden" value="false" id="is-car" />
														    </span>
													</div>
													<div class="col-md-2 form-group">
															<span class="button-checkbox">
														        <button type="button" class="btn" data-color="spysr-blue" id="car-btn">&nbsp;Bus&nbsp;&nbsp;</button>
														        <input type="checkbox" name="bus" class="hidden" value="false" id="is-bus" />
														    </span>
													</div>
													<div class="col-md-2 form-group">
															<span class="button-checkbox">
														        <button type="button" class="btn" data-color="spysr-blue" id="car-btn">&nbsp;Cruise&nbsp;&nbsp;</button>
														        <input type="checkbox" name="cruise" class="hidden" value="false" id="is-car" />
														    </span>
													</div>
													</div>
													</div>
														<div class="col-md-3 form-group">
															<div data-toggle="tooltip" data-placement="top" title="How many rooms" >
															<select aria-hidden="true" name="noOfRoom" id="noOfRoom" class="form-control selectpicker input-sm">
															  <option value="0" selected="selected">Select Room</option>
															  <c:forEach var="i" begin="0" end="50" step="1">
																<option value="${i}" >${i}</option>
															</c:forEach>
															</select>
															</div>
															</div>
															<div class="col-md-3 form-group">
															<div data-toggle="tooltip" data-placement="top" title="Select Adult" >
																<div class="input-group"> <span class="input-group-addon" id="sizing-addon2">Adult</span>
																<select aria-hidden="true" name="adult" id="adult" class="form-control ">
																  <c:forEach var="i" begin="0" end="100" step="1">
																	<option value="${i}">${i}</option>
																</c:forEach>
																</select>
																</div>
																</div>
																</div>
															<div class="col-md-3 form-group">
															<div data-toggle="tooltip" data-placement="top" title="Select Kid's" >
																<div class="input-group"> <span class="input-group-addon" id="sizing-addon2">Kid's(5<8)</span>
																<select aria-hidden="true" name="kid" id="kid" class="form-control ">
																  <c:forEach var="i" begin="0" end="100" step="1">
																	<option value="${i}">${i}</option>
																</c:forEach>
																</select>
																</div>
															</div>
																</div>
															<div class="col-md-3 form-group">
															<div data-toggle="tooltip" data-placement="top" title="Select Infant" >
																<div class="input-group"> <span class="input-group-addon" id="sizing-addon2">Kid's(0<5)</span>
																<select aria-hidden="true" name="infant" id="infant" class="form-control ">
																  <c:forEach var="i" begin="0" end="5" step="1">
																	<option value="${i}">${i}</option>
																</c:forEach>
																</select>
																</div>
																</div>
																</div>
															<div class="col-md-6 form-group">
																<select aria-hidden="true" name="callTime" id="callTime" class="form-control selectpicker">
																  <option value="" selected="SELECTED">Best time to call</option>
																	<option value="morning">Morning</option>
																	<option value="afternoon">Afternoon</option>
																	<option value="evening">Evening</option>
																	<option value="anytime">Any time</option>
																</select>
															</div>
															<div class="col-md-6 form-group">
																<select aria-hidden="true" name="leadSource" id="lead-source" class="form-control selectpicker">
																    <option value="">Lead Source*</option>
																	<option value="advertisement">Advertisement</option>
																	<option value="customerEvent">Customer Event</option>
																	<option value="customerEvent" data-lead-source-type="employeeReferral">Employee Referral</option>
																	<option value="customerEvent">Google AdWords</option>
																	<option value="web">Website</option>
																	<option value="socal">Socal Network</option>
																	<option value="walkin">Walk-In</option>
																	<option value="telephone">Telephone</option>
																	<option value="b2b">B2B</option>
																	<option value="justdial">Justdial</option>
																	<option value="other">Others</option>
																</select>
															</div>
															<div class="col-md-12" style="display: none" id="employee-name">
															<div class="form-group">
															<span data-toggle="tooltip" data-placement="top" title="Employee Name" >
																<select name="employeeReferralId" id="employee-referral-id" class="form-control selectpicker" data-live-search="true">
																<option value="0" selected="selected">Select User</option>
																<c:forEach items="${userVos}" var="userOnj">
																			<option value="${userOnj.userId}">${userOnj.firstName} ${userOnj.lastName}</option>
																</c:forEach>
																</select>
																</span>
															</div>
															</div>
															<div class="col-md-12 form-group">
																<textarea name="requirement" rows="4" id="" class="form-control" placeholder="Requirements..."></textarea>
															</div>
															<div class="row">&nbsp;</div>
															<div class="row">&nbsp;</div>
															<div class="col-md-8"></div>
															<div class="col-md-4">
															<select class="selectpicker form-control" name="travelSalesMyLeadFollowUpData.leadStatus" data-style="btn-spysr-blue" data-header="Select a Lead Status">
															  <c:forEach items="${leadStatusMap}" var="lstatus">
																	<option value="${lstatus.key}" ${lstatus.key == 'New' ? 'selected': ''}>${lstatus.value}</option>
																</c:forEach>
															</select>
															</div>
													</div>
													<div class="col-md-12">&nbsp;</div>
													<div class="col-md-12" style="margin-top:50px">
													<div class="text-center">
									                     <button type="submit" class="btn btn-primary btn-labeled btn-labeled-left" id="save-travel-my-lead-btn"><span class="btn-label"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
									            	</div>
													</div>

										</div>
										</form>
									</div>
									<!-- /profile info -->

						</div>
						
					</div>
				</div>
				</div>
			</section>
        <!--ADMIN AREA ENDS-->
  <script>
/*-------------------------------------------------------------------------*/
    $(function () {
        $('#departure-date').datetimepicker({
        	format: 'MM/DD/YYYY',
        	
        }).on('dp.change', function() {
        	 $(this).data('DateTimePicker').hide();
        });

        $('#arrival-date').datetimepicker({
        	format: 'MM/DD/YYYY',
        }).on('dp.change', function() {
        	 $(this).data('DateTimePicker').hide();
        });
         $("#departure-date").on("dp.change", function (e) {
            $('#arrival-date').data("DateTimePicker").minDate(e.date);
        });
    });

/*-------------------------------------------------------------------------*/
$(document).ready(function() {
    $("#save_travel_sales_my_lead_form").bootstrapValidator({
        feedbackIcons: {
            valid: "fa fa-check",
            invalid: "fa fa-remove",
            validating: "fa fa-refresh"
        },
        fields:{name:{message:"Name is not valid",validators:{notEmpty:{message:"Name is required field"},
        	stringLength:{min:1,message:"Name must be more than 3 and less than 100 characters long"}}},
        	email:{validators:{notEmpty:{message:"Email is a required field"},emailAddress:{message:"The value is not a valid email address"}}},
        	city:{message:"City is not valid",validators:{notEmpty:{message:"City is a required field"}}},
        	country:{message:"Country is not valid",validators:{notEmpty:{message:"Country is a required field"}}},
        	phone:{message:"Phone is not valid",validators:{notEmpty:{message:"Phone is a required field"}}},
        	adult:{message:"Adult is not valid",validators:{notEmpty:{message:"Adult is a required field"}}}}
        
    }).on("error.form.bv", function(e) {}).on("success.form.bv", function(e) {
        showModalPopUp("Saving Details, Please wait ..", "i"),
            e.preventDefault(), $.ajax({
                url: "saveTravelSalesMyLead",
                type: "POST",
                dataType: "json",
                data: $("#save_travel_sales_my_lead_form").serialize(),
                success: function(jsonData) {
                	if(jsonData.message.status == 'success'){
	  					showModalPopUp(jsonData.message.message,"s");
	  					var url = '<%=request.getContextPath()%>/listTravelSalesMyLead';
						window.location.replace(url);	
                	}
	  				else if(jsonData.message.status == 'input'){
	  					showModalPopUp(jsonData.message.message,"w");}
	  				else if(jsonData.message.status == 'error'){
	  					showModalPopUp(jsonData.message.message,"e");}
                	
                        $(".save-travel-my-lead-btn").removeAttr("disabled"), console.debug("button disabled ");
                    var s = $(e.target);
                    s.data("bootstrapValidator");
                    s.bootstrapValidator("disableSubmitButtons", !1).bootstrapValidator("resetForm", !0)
                },
                error: function(e, a, s) {
                    showModalPopUp("Travel Lead can not be Saved.", "e")
                }
            })
    }).on("status.field.bv", function(e, a) {
        a.bv.getSubmitButton() && (console.debug("button disabled "), a.bv.disableSubmitButtons(!1))
    })
});
/* -----------------------------------------------------*/
/*-------------------------------------------------------------------------*/
$('#lead-source').on('change', function (e) {
    var optionSelected = $('#lead-source option:selected').data('lead-source-type');
   
    if(optionSelected == 'employeeReferral'){
    	$("#employee-name").show(500);
    }
    else
    {
    	$("#employee-name").hide(500);
    }
    
});
</script>
