<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/taglibs/spysr.tld" prefix="spysr"%>
<%@taglib prefix="dj" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style>
#notification_error {
    border: 1px solid #A25965;
    height: auto;
    width: auto;
    padding: 4px;
    background: #F8F0F1;
    text-align: center;
    -moz-border-radius: 5px;
}

#notification_ok {
    border: 1px #567397 solid;
    height: auto;
    width: auto;
    padding: 4px;
    background: #f5f9fd;
    text-align: center;
    -moz-border-radius: 5px;
}

.object_ok {
    border: 1px solid green;
    color: #333333;
}

.object_error {
    border: 1px solid #AC3962;
    color: #333333;
}
span.text-size {
    font-size: 18px;
    font-weight: 600;
}
.input-group .form-control {
    z-index: 0;
   }
   textarea.input-sm, select[multiple].input-sm {
    height: 30px;
}
.profile-timeline-card{
    border-left: 3px solid #FF9800;
}
</style>
<!--ADMIN AREA BEGINS-->
            <section class="wrapper container-fluid">
                    <div class="">
                        <div class="pnl">
                            <div class="hd clearfix">
                                <h5 class="pull-left"><img class="clay" src="admin/img/svg/document.svg" width="24" alt="quote"> Trip Quotation of Lead Number : <span class="text-danger">${tripQuotation.leadNumber}</span></h5>
                           <div class="set pull-right">
                                    <div class="dropdown">
									<div class="btn-group">
									
									<a class="btn btn-xs btn-primary" href="<c:choose><c:when test="${shortLeadNumber == 'LE' }">editTravelSalesLead?id=${leadId}</c:when><c:when test="${shortLeadNumber == 'ML' }">editTravelSalesMyLead?id=${leadId}</c:when></c:choose>">
									<i class="fa fa-arrow-left" aria-hidden="true"></i> Back To Lead </a>
									</div>
									</div>
                           </div> 
                          </div>
                        <form  id="saveTripQuotationFrom" method="post" name="saveTripQuotationFrom"  enctype="multipart/form-data">
                        	<input type="hidden" name=travelLeadNumber id="lead-number" value="${travelLeadNumber}">
                        	<input type="hidden" name=totalPassanger id="total-passanger" value="${tripQuotation.totalPassanger}">
							<div class="col-lg-12 col-md-12 ">
						
									<div class="profile-timeline-card" id="trip-quotation-info-tab">
										<p class="mb-2">
													<span class="company-font-icon"><img class="clippy" src="admin/img/svg/trip.svg" width="15"></span> <span> <b class="blue-grey-text">Trip Quotation</b></span>
										</p>
										<hr>
			                               <div class="row row-minus">
												<div class="col-md-12">
													<label class="form-control-label" for="prependedInput">Title</label>
													<div class="controls"><div class="form-group"><div class="">
															<input type="text" class="form-control" id="title" name="title" placeholder="Title" autocomplete="off" value="${tripQuotation.title}">
															<div id="status"></div>
													</div></div>
													</div>
												</div>
											</div>	     
			                               <div class="row row-minus">
												<div class="col-md-12">
													<label class="form-control-label" for="prependedInput">Description</label>
													<div class="controls"><div class="form-group"><div class="">
															<textarea  class="form-control" id="description" name="description" placeholder="" autocomplete="off"></textarea>
															<div id="status"></div>
													</div></div>
													</div>
												</div>
											</div>	     
			                               <div class="row row-minus">
												<div class="col-md-2">
													<label class="form-control-label" for="prependedInput">Destination From</label>
													<div class="controls"><div class="form-group"><div class="">
															<input type="text" class="form-control input-sm"  name="destinationFrom"  autocomplete="off" value="${tripQuotation.destinationFrom}"></div></div>
													</div>
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="prependedInput">Destination To</label>
													<div class="controls"><div class="form-group"><div class="">
															<input type="text" class="form-control input-sm"  name="destinationTo" id="destinationTo" autocomplete="off" value="${tripQuotation.destinationTo}"></div></div>
													</div>
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="prependedInput">Departure Date</label>
													<div class="controls"><div class="form-group">
													<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
															<input type="text" class="form-control input-sm"  name="departureDateFlag" id="departure-date" autocomplete="off" 
															value="${spysr:formatDate(tripQuotation.departureDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}"></div></div>
													</div>
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="prependedInput">Check In Date</label>
													<div class="controls"><div class="form-group">
													<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
															<input type="text" class="form-control input-sm"  name="startDateFlag" id="start-date" autocomplete="off" 
															value="${spysr:formatDate(tripQuotation.startDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}"></div></div>
													</div>
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="prependedInput">Check Out Date</label>
													<div class="controls"><div class="form-group">
													<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
															<input type="text" class="form-control input-sm"  name="endDateFlag" id="end-date" autocomplete="off" 
															value="${spysr:formatDate(tripQuotation.endDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}"></div></div>
													</div>
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="prependedInput">Arrival Date</label>
													<div class="controls"><div class="form-group">
													<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
															<input type="text" class="form-control input-sm"  name="arrivalDateFlag" id="arrival-date" autocomplete="off" 
															value="${spysr:formatDate(tripQuotation.arrivalDate,'yyyy-MM-dd HH:mm', 'MM/dd/yyyy hh:mm a')}"></div></div>
													</div>
												</div>
												</div>
				                                </div>
				                                
				                 <div class="profile-timeline-card" id="prices-rates-tab">
									<p class="mb-2">
										<span class="company-font-icon"><i class="fa fa-money" aria-hidden="true"></i></span> <span> <b class="blue-grey-text">Price & Rates</b></span>
									</p>
									<hr>
                     				<div class="row row-minus">
									<div class="col-md-2">
										<label class="form-control-label" for="prependedInput">Supplier Price</label>
										<div class="controls"><div class="form-group">
										<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-inr" aria-hidden="true"></i></span>
												<input type="text" class="form-control input-sm"  name="supplierPrice" id="supplier-price" autocomplete="off" placeholder="0.00"></div></div>
										</div>
									</div>
									<div class="col-md-2">
										<label class="form-control-label" for="prependedInput">Markup</label>
										<div class="controls"><div class="form-group">
										<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-inr" aria-hidden="true"></i></span>
												<input type="text" class="form-control input-sm"  name="markup" id="markup_price" autocomplete="off" placeholder="0.00"></div></div>
										</div>
									</div>
									<div class="col-md-1">
										<label class="form-control-label" for="prependedInput">GST (%)</label>
										<div class="controls"><div class="form-group">
										<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
												<input type="text" class="form-control input-sm"  name="gstPercentage" id="gst-percentage" autocomplete="off" placeholder="0.00"></div></div>
										</div>
									</div>
									<div class="col-md-1">
										<label class="form-control-label" for="prependedInput">GST Amount</label>
										<div class="controls"><div class="form-group">
												<input type="text" class="form-control input-sm"  name="gst" id="gst-amount" autocomplete="off" placeholder="0.00"></div>
										</div>
									</div>
									<div class="col-md-2">
										<label class="form-control-label" for="prependedInput">Base Price</label>
										<div class="controls"><div class="form-group">
										<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-inr" aria-hidden="true"></i></span>
												<input type="text" class="form-control input-sm"  id="base-price" autocomplete="off" placeholder="0.00" readonly="readonly"></div></div>
										</div>
									</div>
									<div class="col-md-1">
										<label class="form-control-label" for="prependedInput">Processing Fee (%)</label>
										<div class="controls"><div class="form-group">
										<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
												<input type="text" class="form-control input-sm"  name="processingFeePercentage" id="processing-fee-percentage" autocomplete="off" placeholder="0.00" value="3"></div>
												</div>
										</div>
									</div>
									<div class="col-md-1">
										<label class="form-control-label" for="prependedInput">Processing Fee </label>
										<div class="controls"><div class="form-group">
												<input type="text" class="form-control input-sm"  name="processingFee" id="processing-fee" autocomplete="off" placeholder="0.00" value="0"></div>
										</div>
									</div>
									<div class="col-md-2">
										<label class="form-control-label" for="prependedInput">Final Amount</label>
										<div class="controls"><div class="form-group">
										<div class="input-group"> <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-inr" aria-hidden="true"></i></span>
												<input type="text" class="form-control input-sm"  name="totalAmount" id="total-amount" autocomplete="off" placeholder="0.00" readonly="readonly"></div></div>
										</div>
									</div>
									</div>	
                                </div>
				                                
									<div class="profile-timeline-card" id="quotation-itinerary-tab">
									<p class="mb-2">
										<span class="company-font-icon"><img class="clippy" src="admin/img/svg/itinerary-b.svg" width="16" alt="itinerary"></span>
										 <span> <b class="blue-grey-text">Day Wise Itinerary <small>Duration :${tripQuotation.duration}</small></b></span>
										 <input type="hidden" name="duration" value="${tripQuotation.duration}">
									</p>
									<hr>
									<c:forEach var="day" begin="1" end="${tripQuotation.duration}" varStatus="count">
									<div class="form_box_block">
                     				<div class="row row-minus">
									<div class="form-group row">
				                      <label class="col-md-2 col-form-label"><span class="itin-day-count"> Day <span id="day-to-add">${day}</span>: </span></label>
				                      <input type="hidden" name="tripQuotationItineraryList[${count.count-1}].day" id="day" value="${day}">
				                      <div class="col-md-10">
				                        <input type="text" class="form-control input-sm" name="tripQuotationItineraryList[${count.count-1}].title" placeholder="Day Title" autocomplete="off" required="required">
				                      </div>
				                    </div>
				                    <div class="col-md-12">
										<div class="controls">
											<div class=""><div class="form-group">
												<textarea class="form-control " id="description" name="tripQuotationItineraryList[${count.count-1}].description"  autocomplete="off" rows="5" placeholder="Briefly describe..."></textarea>
												</div>
											</div>
										</div>
									</div>
									</div>
									<div class="row row-minus">
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput">Hotel on Day ${day}:</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" class="form-control input-sm" id="hotelName" name="tripQuotationItineraryList[${count.count-1}].hotelName"  autocomplete="off">
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput">&nbsp;</label>
										<div class="controls">
											<div class="form-group">
												<input type="text" class="form-control input-sm" id="hotelCity" name="tripQuotationItineraryList[${count.count-1}].hotelCity" placeholder="Hotel City"  autocomplete="off">
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<label class="form-control-label" for="appendedInput">&nbsp;</label>
										<div class="controls">
											<div class="form-group">
												<select class="form-control input-sm" name="tripQuotationItineraryList[${count.count-1}].hotelRating" id="hotel-rating" style="width: 100%;">
                                                        <option value="" selected="selected"> Select Rating</option>
                                                        <option value="1"> 1 Star</option>
                                                        <option value="2">2 Star</option>
                                                        <option value="3">3 Star</option>
                                                        <option value="4">4 Star</option>
                                                        <option value="5">5 Star</option>
                                                        <option value="5+">5+ Star</option>
                                                 </select>
											</div>
										</div>
									</div>
									</div>	     
                                	</div>
                                	</c:forEach>
                                	</div>
                                	
                                	<div class="profile-timeline-card" id="inclusion-exclusion-tab">
									<p class="mb-2">
										<span class="company-font-icon"><i class="fa fa-location-arrow"></i></span> <span> <b class="blue-grey-text">Inclusions</b></span>
									</p>
									<hr>
                     				<div class="row row-minus">
									<div class="col-md-11" id="inputsWrapperInclusions">
										<div class="row row-minus inputsWrapperInnerInclusions" > 
											<div class="col-md-5">
												<label class="form-control-label" for="appendedInput">Title</label>
												<div class="controls">
													<div class="form-group">
														<input type="text" class="form-control input-sm" id="title" name="tripQuotationInclusionsList[0].title"  autocomplete="off">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<label class="form-control-label" for="appendedInput">Description</label>
												<div class="controls">
													<div class="form-group">
														<textarea class="form-control input-sm" id="description input-sm" name="tripQuotationInclusionsList[0].description"  autocomplete="off" rows=""></textarea>
													</div>
												</div>
											</div>
											<div class="col-md-1 mt-4">
												<div class="pull-right">
													<!-- <div class="controls">
													<a class="btn btn-success removeBox" data-add-btn-id="addMoreBoxInclusions" ><i class="fa fa-plus"></i> Remove</a>
													</div> -->									
												</div>
											</div>	
										</div>
										</div>
										<div class="col-md-1 mt-4">
											<div class="pull-right">
											<div class="controls addMoreBox" >
											<a class="btn btn-sm btn-success addMoreBoxInner" id="addMoreBoxInclusions" data-inputs-wrapper-id="inputsWrapperInclusions" data-inputs-wrapper-inner-id="inputsWrapperInnerInclusions" 
											data-add-more-box="addMoreBox" data-remove-box="removeBox" data-add-more-box-inner="addMoreBoxInner" data-add-box-id="addMoreBoxInclusions" data-add-btn-txt="Add More"
											data-field-count=1 data-box-count=1 data-max-count=2 data-max-flag=false data-type="inclusions" ><img class="clippy" src="admin/img/svg/plus.svg" width="15"> &nbsp;Add</a>
											</div>									
											</div>
										</div>	   
									</div>  
									<p class="mt-3 mb-2">
										<span class="company-font-icon"><i class="fa fa-location-arrow"></i></span> <span> <b class="blue-grey-text">Exclusions</b></span>
									</p>
									<hr>
									<div class="row row-minus">
									<div class="col-md-11" id="inputsWrapperExclusions">
											<div class="row row-minus inputsWrapperInnerExclusions" > 
												<div class="col-md-5">
											<label class="form-control-label" for="appendedInput">Title</label>
											<div class="controls">
												<div class=""><div class="form-group"><div class="">
													<input type="text" class="form-control input-sm" id="title" name="tripQuotationExclusionsList[0].title"  autocomplete="off">
													
													</div></div>
												</div>
											</div>
											</div>
											<div class="col-md-6">
												<label class="form-control-label" for="appendedInput">Description</label>
												<div class="controls">
													<div class=""><div class="form-group">
														<textarea class="form-control input-sm" id="description" name="tripQuotationExclusionsList[0].description"  autocomplete="off" rows=""></textarea>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-1 mt-4">
												<div class="pull-right">
													<div class="controls">
													<!-- <a class="btn btn-success removeclass" ><i class="fa fa-plus"></i> Remove</a> -->
													</div>									
												</div>
											</div>	
										</div>
										</div>
										<div class="col-md-1 mt-4">
											<div class="pull-right">
											<div class="controls addMoreBox" >
											<a class="btn btn-sm btn-success addMoreBoxInner" id="addMoreBoxIncExclusions" data-inputs-wrapper-id ="inputsWrapperExclusions" data-inputs-wrapper-inner-id="inputsWrapperInnerExclusions" 
											data-add-more-box="addMoreBox" data-remove-box="removeBox" data-add-more-box-inner="addMoreBoxInner" data-add-box-id="addMoreBoxIncExclusions" data-add-btn-txt="Add More"
											data-field-count=1 data-box-count=1 data-max-count=2 data-max-flag=false data-type="exclusions" ><img class="clippy" src="admin/img/svg/plus.svg" width="15">&nbsp; Add</a>
											</div>	
											</div>
										</div>	   
									</div>  
                                </div>
                                
									<div class="profile-timeline-card" id="cancellation-policy-tab">
									<p class="mb-2">
										<span class="company-font-icon"><img class="clippy" src="admin/img/svg/policy.svg" width="14" alt="policy-o"></span> <span> <b class="blue-grey-text">Cancellation & Refund Policy</b></span>
									</p>
									<hr>
                     				<div class="row row-minus">
									<div class="col-md-11">
									<div class="row row-minus">
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Days</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input class="form-control input-sm" id="cancellation-day-1" name="tripQuotationCancellationPolicyList[0].cancellationDay" autocomplete="off" value="30" />
												</div>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Amount</label>
										<div class="controls">
											<div class="form-group">
												<div class="input-group">
												<input class="form-control input-sm" id="feeAmount" name="tripQuotationCancellationPolicyList[0].feeAmount" autocomplete="off" value="25" />
												<input type="hidden" name="feeType" id="feeType" value="%">
												<span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-7">
										<label class="form-control-label" for="appendedInput">Remarks</label>
										<div class="controls">
											<div class=""><div class="form-group "><div class="">
												<textarea class="form-control input-sm" id="remarks" name="tripQuotationCancellationPolicyList[0].remarks" autocomplete="off" rows="1">* 30 days or more before date of departure: 25% of total cost</textarea>
												</div>
											</div>
										</div>
									</div>
									</div>	
									</div>
									</div>  
									<div class="col-md-1"></div>   
                                </div>
                     			<div class="row row-minus">
                     				<div class="col-md-11">
                     				<div class="row row-minus">
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Days</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input class="form-control input-sm" id="cancellationDay" name="tripQuotationCancellationPolicyList[1].cancellationDay" autocomplete="off" value="29-20" />
												</div>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Amount</label>
										<div class="controls">
											<div class="form-group">
												<div class="input-group">
												<input class="form-control input-sm" id="feeAmount" name="tripQuotationCancellationPolicyList[1].feeAmount" autocomplete="off" value="50" />
												<input type="hidden" name="feeType" id="feeType" value="%">
												<span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-7">
										<label class="form-control-label" for="appendedInput">Remarks</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<textarea class="form-control input-sm" id="remarks" name="tripQuotationCancellationPolicyList[1].remarks" autocomplete="off" rows="1">* 29 - 20 days before date of departure: 50% of total cost</textarea>
												</div>
											</div>
										</div>
									</div>
									</div>	
									</div>     
                                </div>
                                <div class="col-md-1"></div>
                                </div>
                                 <div class="row row-minus">
									<div class="col-md-11" id="inputsWrapperCancellationPolicy">
                     				<div class="row row-minus">
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Days</label>
										<div class="controls">
											<div class=""><div class="form-group">
												<input class="form-control input-sm" id="cancellationDay" name="tripQuotationCancellationPolicyList[2].cancellationDay" autocomplete="off" value="19" />
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Amount</label>
										<div class="controls">
											<div class="form-group">
												<div class="input-group">
												<input class="form-control input-sm" id="feeAmount" name="tripQuotationCancellationPolicyList[2].feeAmount" autocomplete="off" value="100" />
												<input type="hidden" name="feeType" id="feeType" value="%">
												<span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-7">
										<label class="form-control-label" for="appendedInput">Remarks</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<textarea class="form-control input-sm" id="remarks" name="tripQuotationCancellationPolicyList[2].remarks" autocomplete="off" rows="1">* Less than 19 days before departure: 100% of total cost</textarea>
												</div>
											</div>
										</div>
									</div>
									</div>	     
                                </div>
                                </div>
                                <div class="col-md-1 mt-4">
											<div class="pull-right">
											<div class="controls addMoreBox" >
											<a class="btn btn-sm btn-success addMoreBoxInner" id="addMoreBoxCancellationPolicy" data-inputs-wrapper-id ="inputsWrapperCancellationPolicy" data-inputs-wrapper-inner-id="inputsWrapperInnerCancellationPolicy" 
											data-add-more-box="addMoreBox" data-remove-box="removeBox" data-add-more-box-inner="addMoreBoxInner" data-add-box-id="addMoreBoxCancellationPolicy" data-add-btn-txt="Add More"
											data-field-count=1 data-box-count=3 data-max-count=2 data-max-flag=false data-type="CancellationPolicy" ><img class="clippy" src="admin/img/svg/plus.svg" width="12">&nbsp; Add</a>
											</div>	
											</div>
										</div>	   
                        </div>
                        </div>
									
									<div class="profile-timeline-card" id="payment-policy-tab">
									<p class="mb-2">
										<span class="company-font-icon"><img class="clippy" src="admin/img/svg/policy.svg" width="14" alt="policy-o"></span> <span> <b class="blue-grey-text">Payment Policy</b></span>
									</p>
									<hr>
                     				<div class="row row-minus">
									<div class="col-md-11">
									<div class="row row-minus">
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Days</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input class="form-control input-sm" id="cancellationDay" name="tripQuotationPaymentPolicyList[0].day" autocomplete="off" value="None" />
												</div>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Amount</label>
										<div class="controls">
											<div class="form-group">
												<div class="input-group">
												<input class="form-control input-sm" id="feeAmount" name="tripQuotationPaymentPolicyList[0].feeAmount" autocomplete="off" value="40" />
												<input type="hidden" name="feeType" id="feeType" value="%">
												<span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-7">
										<label class="form-control-label" for="appendedInput">Remarks</label>
										<div class="controls">
											<div class=""><div class="form-group "><div class="">
												<textarea class="form-control input-sm" id="remarks" name="tripQuotationPaymentPolicyList[0].remarks" autocomplete="off" rows="1">40% payment to be paid in advance  at the time of booking being initiated.  </textarea>
												</div>
											</div>
										</div>
									</div>
									</div>	
									</div>
									</div>  
									<div class="col-md-1"></div>   
                                </div>
                     			<div class="row row-minus">
                     				<div class="col-md-11">
                     				<div class="row row-minus">
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Days</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<input class="form-control input-sm" id="cancellationDay" name="tripQuotationPaymentPolicyList[1].day" autocomplete="off" value="7" />
												</div>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Amount</label>
										<div class="controls">
											<div class="form-group">
												<div class="input-group">
												<input class="form-control input-sm" id="feeAmount" name="tripQuotationPaymentPolicyList[1].feeAmount" autocomplete="off" value="100" />
												<input type="hidden" name="feeType" id="feeType" value="%">
												<span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-7">
										<label class="form-control-label" for="appendedInput">Remarks</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<textarea class="form-control input-sm" id="remarks" name="tripQuotationPaymentPolicyList[1].remarks" autocomplete="off" rows="1">100% payment to be paid 7 days before departure.</textarea>
												</div>
											</div>
										</div>
									</div>
									</div>	
									</div>     
                                </div>
                                <div class="col-md-1"></div>
                                </div>
                                 <div class="row row-minus">
									<div class="col-md-11" id="inputsWrapperPaymentPolicy">
                     				<div class="row row-minus">
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Days</label>
										<div class="controls">
											<div class=""><div class="form-group">
												<input class="form-control input-sm" id="paynemt-day" name="tripQuotationPaymentPolicyList[2].day" autocomplete="off" value="None" />
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-2">
										<label class="form-control-label" for="appendedInput">Amount</label>
										<div class="controls">
											<div class="form-group">
												<div class="input-group">
												<input class="form-control input-sm" id="feeAmount" name="tripQuotationPaymentPolicyList[2].feeAmount" autocomplete="off" value="0" />
												<input type="hidden" name="feeType" id="feeType" value="%">
												<span class="input-group-addon" id="sizing-addon2"><b>%</b></span>
											</div>
										</div>
									</div>
									</div>	     
									<div class="col-md-7">
										<label class="form-control-label" for="appendedInput">Remarks</label>
										<div class="controls">
											<div class=""><div class="form-group"><div class="">
												<textarea class="form-control input-sm" id="remarks" name="tripQuotationPaymentPolicyList[2].remarks" autocomplete="off" rows="1">Failure in payment will be considered a no show , refund will be done or processed as per cancellation policy.</textarea>
												</div>
											</div>
										</div>
									</div>
									</div>	     
                                </div>
                                </div>
                                <!-- <div class="col-md-1 mt-4">
											<div class="pull-right">
											<div class="controls addMoreBox" >
											<a class="btn btn-sm btn-success addMoreBoxInner" id="addMoreBoxPaymentPolicy" data-inputs-wrapper-id ="inputsWrapperPaymentPolicy" data-inputs-wrapper-inner-id="inputsWrapperInnerPaymentPolicy" 
											data-add-more-box="addMoreBox" data-remove-box="removeBox" data-add-more-box-inner="addMoreBoxInner" data-add-box-id="addMoreBoxCancellationPolicy" data-add-btn-txt="Add More"
											data-field-count=1 data-box-count=3 data-max-count=2 data-max-flag=false data-type="PaymentPolicy" ><img class="clippy" src="admin/img/svg/plus.svg" width="12">&nbsp; Add</a>
											</div>	
											</div>
										</div>	 -->   
                        </div>
                        </div>
			                       
			                        <div class="profile-timeline-card mb-4" id="important-note-tab">
												<p class="mb-2">
													<span class="company-font-icon"><img class="clippy" src="admin/img/svg/note.svg" width="12"></span> <span> <b class="blue-grey-text">Important Note</b></span>
												</p>
												<hr>
			                     				<div class="row row-minus">
												<div class="col-md-12">
													<label class="form-control-label" for="prependedInput">&nbsp;</label>
													<div class="controls"><div class="form-group">
															<textarea class="form-control"  name=importantNote id="important-note" autocomplete="off"  rows="4">In case of natural calamities, it depends that how much refund we got from hotel/transportation and Airline. Accordingly we will refund the amount. TourOxy is not responsible for the things happen due to any natural calamities or any medical emergency. TourOxy will not be responsible for any loss in personal belongings of the customer during the journey or stay. We suggest guests to take care of their personal belongings. TourOxy always inform guests to check hotels & tour details prior booking, TourOxy will not be responsible for any further feedback from hotel or tour operator. TourOxy suggests to check hotel reviews and talk to driver or our partners in a responsible manner which help guest and TourOxy to operate our tours flawlessly without any issues. Every tour is solely depend on the itinerary and each day itinerary to be strictly followed by guests , TourOxy is not responsible for anything other than itinerary suggested and it must be followed to make sure smooth operations each day. TourOxy is not responsible for any traffic jams or delay being caused during your journey. We care for you and suggest verifying every detail before booking.</textarea>
													</div>
													</div>
												</div>
												</div>	
			                        </div>
                        </div>
                        <div class="bottom-fixed-footer-btn">
							<div class="form-actions">
								<div class="pull-left">
								<button type="reset" class="btn btn-default" id="resetTripQuotationBtn">Reset</button>
								</div>
								<div class="pull-right">
								<button type="submit" class="btn btn-success" id="save-trip-quotation-btn" style="padding: 6px 28px;">Save</button>
								</div>
							</div>
						</div>
                        </form>
                    </div>
                </div>
            </section>
<script>
									
$(document).ready(function() {
	$(".addMoreBoxInner").click(function (e) {
			var InputsWrapper   = $(this).data("inputs-wrapper-id");
			var InputsWrapperInner   = $(this).data("inputs-wrapper-inner-id"); 
			var AddButtonBoxInner       = $(this).data("add-more-box-inner"); 
			var AddButtonBox       = $(this).data("add-more-box"); 
			var RemoveFileBox       = $(this).data("remove-box"); 
			var AddMoreFileBoxId      = $(this).data("add-box-id");
			var type      = $(this).data("type");
			var addBtnText=$(this).data("add-btn-txt");
			var FieldCount = $(this).data("field-count");
			var x = parseInt($(this).data("box-count"));
			var MaxInputs = parseInt($(this).data("max-count")); 
			var MaxInputFlag = Boolean($(this).data("max-flag"));
			
	        if(x <= MaxInputs || !MaxInputFlag) 
	        {
	            FieldCount++; //text box added ncrement
	            var body= "";
	            if(type=="inclusions")
	            {
	            	body = getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,0,1,5,'Title','text','title'+x,'tripQuotationInclusionsList['+x+'].title','off',0,0,'','')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,1,1,6,'Description','textarea','description'+x,'tripQuotationInclusionsList['+x+'].description','off',1,0,'','');
	            }
	            if(type=="exclusions")
	            {
	            	body = getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,0,1,5,'Title','text','title'+x,'tripQuotationExclusionsList['+x+'].title','off',0,0,'','')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,1,1,6,'Description','textarea','description'+x,'tripQuotationExclusionsList['+x+'].description','off',1,0,'','');
	            }
	            if(type=="CancellationPolicy")
	            {
	            	body = getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,0,2,2,'Days','text','cancellationDay'+x,'tripQuotationCancellationPolicyList['+x+'].cancellationDay','off',0,0,'','')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,1,2,2,'Amount','text','feeAmount'+x,'tripQuotationCancellationPolicyList['+x+'].feeAmount','off',0,0,'Y','%')
	            		  +getInputBlock(InputsWrapper,InputsWrapperInner,AddMoreFileBoxId,x,2,2,7,'Remarks','textarea','remarks'+x,'tripQuotationCancellationPolicyList['+x+'].remarks','off',1,0,'','');
	            }
	            
	            $("#"+InputsWrapper).append(body);
	            x++; 
	            $("."+AddButtonBox).show();
	            $("."+AddButtonBoxInner).html(addBtnText);
	            if(x == MaxInputs+1 && MaxInputFlag) {
	            	 $("."+AddButtonBox).hide();
	            }
	            $(this).data("field-count",FieldCount);
	            $(this).data("box-count",x);
	        }
	        return false;
	});
	
	
	$("body").on("click",".removeBox", function(e){
			var addBtnId= $(this).data("add-btn-id");	
			var x = parseInt($("#"+addBtnId).data("box-count"));
			var InputsWrapperInner = $("#"+addBtnId).data("inputs-wrapper-inner-id");
			var AddButtonBoxInner       = $("#"+addBtnId).data("add-more-box-inner"); //"addMoreFileBox"; //Add button ID
			var AddButtonBox       = $("#"+addBtnId).data("add-more-box");
			var addBtnText=$(this).data("add-btn-txt");
			
	        if( x > 1 ) {
	        		var removeXid = x-1;
	        		 $("#"+InputsWrapperInner+removeXid).remove();
	                 x--;
	                 $("#"+addBtnId).data("box-count",x);
	                 $("."+AddButtonBox).show();
	                $("."+AddButtonBoxInner).html(addBtnText);
	        }
		return false;
	}) 

	});
function getInputBlock(wrapper, wrapperInner, addMoreFileBoxId, wrapperId,colCountStart ,colCountEnd,colMdCount,label,type, id, name, autocomplete, areaRow,areaCol,formInputFlag, formInputSymbol)
{
	var divRow='<div class="row row-minus '+wrapper+'" id="'+wrapperInner+wrapperId+'" >';
	var divCol='<div class="col-md-'+colMdCount+'">';
	var divLabel='<label class="form-control-label" for="appendedInput">'+label+'</label>';
	var divFormControl='<div class="controls">';
	
	var divFromGroup='<div class="form-group">';
	
	var divFormInput='<div class="input-group">';
	var divFormInputSpan='<span class="input-group-addon" id="sizing-addon2"><b>'+formInputSymbol+'</b></span>';
	
	var formInput='<input type="text" class="form-control " id="'+id+'" name="'+name+'"  autocomplete="'+autocomplete+'">';
	var formTextArea='<textarea class="form-control " id="'+id+'" name="'+name+'" autocomplete="'+autocomplete+'" rows="'+areaRow+'"></textarea>';
	var divCloseTag='</div>';
	var aClose='<div class="mt-4"><a href="#" class="btn-sm  btn-danger removeBox" data-add-btn-id="'+addMoreFileBoxId+'">Remove</a></div>';
	
	var start='';
	if(colCountStart==0)
		start =divRow+divCol+divLabel+divFormControl;
	else
		start = divCol+divLabel+divFormControl;
	
	if(formInputFlag=="")
			start=start+divFromGroup;
	else
			start=start+divFormInput;
	
	var body='';
	if(type=="text")
		body=formInput;
	if(formInputSymbol!="")
		body=body+divFormInputSpan;
	if(type=="textarea")
		body=formTextArea;
	var end = divCloseTag+divCloseTag+divCloseTag;
	if(colCountStart==colCountEnd)
	{
		end =end+aClose+divCloseTag;
	}
	return start+body+end;
}
</script>
<script>
$(function () {
    $('#start-date').datetimepicker({
    	format: 'MM/DD/YYYY hh:mm A',
    	sideBySide : false,
    	
    }).on('dp.change', function() {
    	 $(this).data('DateTimePicker').hide();
    });

    $('#end-date').datetimepicker({
    	format: 'MM/DD/YYYY hh:mm A',
    	sideBySide : false,
    }).on('dp.change', function() {
    	 $(this).data('DateTimePicker').hide();
    });
    
     $("#start-date").on("dp.change", function (e) {
        $('#end-date').data("DateTimePicker").minDate(e.date);
    });
});
$(function () {
    $('#departure-date').datetimepicker({
    	format: 'MM/DD/YYYY hh:mm A',
    	sideBySide : false,
    	
    }).on('dp.change', function() {
    	 $(this).data('DateTimePicker').hide();
    });

    $('#arrival-date').datetimepicker({
    	format: 'MM/DD/YYYY hh:mm A',
    	sideBySide : false,
    }).on('dp.change', function() {
    	 $(this).data('DateTimePicker').hide();
    });
    
     $("#departure-date").on("dp.change", function (e) {
        $('#arrival-date').data("DateTimePicker").minDate(e.date);
    });
});
 /*-----------------------------------------------------------------------------------------------------------------------*/
 $(document).ready(function(){
		$('#supplier-price, #markup_price, #gst-percentage , #processing-fee-percentage').keyup(function() {
			var suuplierPriceInput= $("#supplier-price").val();
			var markUpInput = $("#markup_price").val();
			var gstInput = $("#gst-percentage").val();
			var processingFeeInput = $("#processing-fee-percentage").val();
			
			var supplierPrice = parseFloat(suuplierPriceInput!=undefined && suuplierPriceInput!="" ?suuplierPriceInput:0);
			var markUp = parseFloat(markUpInput!=undefined && markUpInput!="" ?markUpInput:0);
			var gst = parseFloat(gstInput!=undefined && gstInput!="" ?gstInput:0);
			var processingFee = parseFloat(processingFeeInput!=undefined && processingFeeInput!="" ?processingFeeInput:0);
		
			
			var baseAmount = 0;
			var taxAmount = 0;
			var processingAmount = 0;
			var totalAmount = 0;
			console.log(supplierPrice);
			markUpAmount=(supplierPrice+markUp);
			taxAmount = parseFloat((markUpAmount*gst)/100);
			baseAmount = parseFloat((markUpAmount*gst)/100)+markUpAmount;
			processingAmount = parseFloat((baseAmount*processingFee)/100);
			totalAmount = parseFloat((baseAmount*processingFee)/100)+baseAmount;
			// display all values
			 $("#gst-amount").val(taxAmount);
			 $("#base-price").val(baseAmount);
			 $("#processing-fee").val(processingAmount);
			 $("#total-amount").val(totalAmount);
	});
});
 /*-----------------------------*/
 
 $(document).ready(function() {
    $("#saveTripQuotationFrom").bootstrapValidator({
        feedbackIcons: {
            valid: "fa fa-check",
            invalid: "fa fa-remove",
            validating: "fa fa-refresh"
        },
        fields:{title:{message:"Title is not valid",validators:{notEmpty:{message:"Title is required field"},
        	stringLength:{min:1,message:"Title must be more than 3 and less than 100 characters long"}}},
        	destinationFrom:{message:"Destination from is not valid",validators:{notEmpty:{message:"Destination from is a required field"}}},
        	destinationTo:{message:"Destination to is not valid",validators:{notEmpty:{message:"Destination to is a required field"}}},
        	supplierPrice:{message:"Phone is not valid",validators:{notEmpty:{message:"Supplier Price is a required field"}}},
        	},
        
    }).on("error.form.bv", function(e) {}).on("success.form.bv", function(e) {
    		notifySuccess();
            e.preventDefault(), 
            $.ajax({
                url: "saveTripQuotation",
                type: "POST",
                dataType: "json",
                data: $("#saveTripQuotationFrom").serialize(),
                success: function(jsonData) {
                	if(jsonData.message.status == 'success'){
	  					showModalPopUp(jsonData.message.message,"s");
	  					if(jsonData.message.number == 'LE'){
	  						var url = '<%=request.getContextPath()%>/editTravelSalesLead';
	  						window.location.replace(url+'?id='+jsonData.message.id);
	  					}
	  					else if(jsonData.message.number == 'ML'){
	  						var url = '<%=request.getContextPath()%>/editTravelSalesMyLead';
	  						window.location.replace(url+'?id='+jsonData.message.id);
	  					}
							
                	}
	  				else if(jsonData.message.status == 'input'){
	  					showModalPopUp(jsonData.message.message,"w");
	  					
	  					}
	  				else if(jsonData.message.status == 'error'){
	  					showModalPopUp(jsonData.message.message,"e");}
                	
                    $("#save-trip-quotation-btn").removeAttr("disabled"), console.debug("button disabled ");
                    var s = $(e.target);
                    s.data("bootstrapValidator");
                    s.bootstrapValidator("disableSubmitButtons", !1).bootstrapValidator("resetForm", !0)
                },
                error: function(e, a, s) {
                    showModalPopUp("Trip Quotation can not be Saved.", "e")
                }
            })
    }).on("status.field.bv", function(e, a) {
        a.bv.getSubmitButton() && (console.debug("button disabled "), a.bv.disableSubmitButtons(!1))
    })
});
</script>